package com.salesc2.data.network.api.response

data class ResponseTimeZone(
    val status: String,
    val msg: String,
    val `data`: Data
) {
    data class Data(
        val _id: String,
        val timezone: String
    )
}
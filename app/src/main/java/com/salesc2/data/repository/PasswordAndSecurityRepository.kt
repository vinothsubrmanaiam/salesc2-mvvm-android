package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestPasswordAndSecurity
import com.salesc2.data.network.api.response.ResponsePasswordAndSecurity
import com.salesc2.data.network.api.service.PasswordAndSecurityApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface PasswordAndSecurityRepository {
    suspend fun reqGetPwSec(
        requestPwSec: RequestPasswordAndSecurity,
        onSuccess: OnSuccess<ResponsePasswordAndSecurity>,
        onFailure: OnFailure<ResponsePasswordAndSecurity>

    )

    companion object Factory{
        fun create(api : PasswordAndSecurityApi) : PasswordAndSecurityRepository = PasswordAndSecurityRepositoryImpl(api)
    }
}

class PasswordAndSecurityRepositoryImpl(var api: PasswordAndSecurityApi) : PasswordAndSecurityRepository {
    override suspend fun reqGetPwSec(
        requestPwSec: RequestPasswordAndSecurity,
        onSuccess: OnSuccess<ResponsePasswordAndSecurity>,
        onFailure: OnFailure<ResponsePasswordAndSecurity>
    ) {


        withContext(Dispatchers.IO) {
            try {
                val response = api.reqPwSec(reqPwSec = requestPwSec)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status=="200") {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }
    }
}
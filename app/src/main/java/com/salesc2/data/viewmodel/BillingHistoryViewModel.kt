package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestBillingHistory
import com.salesc2.data.network.api.request.RequestBillingOverView
import com.salesc2.data.network.api.request.RequestMorePage
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseBillingHistory
import com.salesc2.data.network.api.response.ResponseBillingOverview
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.api.response.ResponseMorePage
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.BillingHistoryRepository
import com.salesc2.data.repository.BillingOverViewRepository
import com.salesc2.data.repository.EmployeeLoginRepository
import com.salesc2.data.repository.MorePageRepository

import kotlinx.coroutines.launch

class BillingHistoryViewModel(
    private val repository: BillingHistoryRepository,
    val context: Context
) : ViewModel() {
     fun reqBillingHistory(
         requestBillingHistory: RequestBillingHistory,
         onSuccess: OnSuccess<ResponseBillingHistory>,
         onFailure: OnFailure<ResponseBillingHistory>
    )
     {
         viewModelScope.launch {
             repository.reqBillingHistory(requestBillingHistory,onSuccess,onFailure)
         }
     }

}
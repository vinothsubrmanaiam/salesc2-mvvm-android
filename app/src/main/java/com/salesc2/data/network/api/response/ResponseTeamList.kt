package com.salesc2.data.network.api.response

data class ResponseTeamList(
    val levelsdata: List<Levelsdata>,
    val msg: String,
    val status: String
) {
    data class Levelsdata(
        val email: String,
        val id: String,
        val level_name: String,
        val level_type: String,
        val name: String,
        val phone: String,
        val profilePath: String,
        val role: String
    )
}
package com.salesc2.adapters

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.CalendarContract
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.database.AddTaskTimeline
import com.salesc2.database.AppDatabase
import com.salesc2.database.TaskStatusTable
import com.salesc2.fragment.DashboardAssigneeFilterFragment
import com.salesc2.fragment.EventDetailsFragment
import com.salesc2.fragment.TaskDetailsFragment
import com.salesc2.model.DashTasksModel
import com.salesc2.model.SelectStatusModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import me.thanel.swipeactionview.SwipeActionView
import me.thanel.swipeactionview.SwipeGestureListener
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class DashboardTasksAdapter() :RecyclerView.Adapter<DashboardTasksAdapter.MyViewHolder>() {
    private var dashTaskItems = ArrayList<DashTasksModel>()
    var context : Context? = null
    var dash_tab_click = String()
    var apiRequest = ApiRequest()
    var webService = WebService()
    var db :  AppDatabase?= null
    private var set_timezone = String()


    constructor(context: Context?, dashTaskItems: ArrayList<DashTasksModel>) : this(){
        this.context = context
        this.dashTaskItems = dashTaskItems
    }
   private var userId = String()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder{
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_dashboard_tasks, parent, false)
        dash_tab_click = context?.getSharedPref("dash_tab_click",context!!).toString()
         userId = context?.getSharedPref("userId",context!!).toString()
        set_timezone = context?.getSharedPref("set_timezone",context!!).toString()
        return MyViewHolder(view)
    }



    override fun getItemCount(): Int {
     return dashTaskItems.size
    }

    private fun capFirstLetter(text: String): String{
        if (text.length >=2){
            return text.substring(0, 1).toUpperCase(Locale.getDefault()) + text.substring(1)
        }
        return text
    }
    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

      val dashTasksModel : DashTasksModel = dashTaskItems[position]
        set_timezone = context?.getSharedPref("set_timezone",context!!).toString()

        try {
            holder.d_task_title.text = capFirstLetter(dashTasksModel.title)
        }catch (e:Exception){

        }
        //remove dot and dumy image if no data
try {

     if (dashTasksModel.type== "meeting" || dashTasksModel.type== "personal" && dashTasksModel.attendee_length.toInt()==0){
            holder.dot.visibility = View.GONE
            holder.d_emp_img.visibility = View.GONE
         holder.d_emp_name.visibility = View.GONE
        }else{
         holder.dot.visibility = View.VISIBLE
         holder.d_emp_img.visibility = View.VISIBLE
         holder.d_emp_name.visibility = View.VISIBLE
     }

}catch (e:Exception){

}

        try {
            if (dashTasksModel.type== "task" && dashTasksModel.taskStatus.equals("unassigned",ignoreCase = true)){
                holder.dot.visibility = View.GONE
                holder.d_emp_img.visibility = View.GONE
                holder.d_emp_name.visibility = View.GONE
            }else{
                holder.dot.visibility = View.VISIBLE
                holder.d_emp_img.visibility = View.VISIBLE
                holder.d_emp_name.visibility = View.VISIBLE
            }


        }catch (e:Exception){

        }


        //empty or null event address
        try {
          if (dashTasksModel.event_address == "null" || dashTasksModel.event_address == ""){
              holder.dash_event_location.visibility = View.GONE
          } else{
              holder.dash_event_location.visibility = View.VISIBLE
          }
        }catch (e:Exception){

        }

        try {
            val parser = SimpleDateFormat("yyyy-MM-dd HH:mm")
        val sdf_time = SimpleDateFormat("hh:mm a")
        val time_parse: Date = parser.parse(dashTasksModel.startDate)
        val timeToLocal = context?.dateFromUTC(context!!,time_parse)
        val startTime: String = sdf_time.format(timeToLocal)
        holder.d_task_dt.text = startTime.replace("am","AM").replace("pm","PM")


        var parserEnd = SimpleDateFormat("yyyy-MM-dd HH:mm")
        var sdf_timeEnd = SimpleDateFormat("hh:mm a")

        val time_parseEnd: Date = parserEnd.parse(dashTasksModel.endDate)
//        val edTimeToLocal = context?.dateFromUTC(context!!,time_parseEnd)
//        val startTimeEnd: String = sdf_timeEnd.format(time_parseEnd)


        val mills: Long = time_parse.time - time_parseEnd.time
        var hours: Long = mills / (1000 * 60 * 60)
        hours = if (hours < 0) -hours else hours
        var min = (mills / (1000 * 60) % 60).toInt()
        min = if (min < 0) -min else min

        if (hours > 0 && min > 0){
            holder.d_task_time.text = "$hours hr $min min"
        }else if(hours <= 0 && min > 0){
            holder.d_task_time.text = "$min min"
        }else if (hours > 0 && min <= 0){
            holder.d_task_time.text = "$hours hr"
        }
        }catch (e:Exception){

        }


        try {
            Picasso.get().load(dashTasksModel.assignTo_img).placeholder(R.drawable.ic_dummy_user_pic).into(holder.d_emp_img)
        }catch (e:Exception){

        }

        holder.d_emp_name.text = capFirstLetter(dashTasksModel.assignTo_name)

        if (dashTasksModel.isEmergency == "true"){
            holder.emergency_icon.visibility = View.VISIBLE
        }else{
            holder.emergency_icon.visibility = View.GONE
        }



            //OverDue
        if (dashTasksModel.status.equals("overdue",ignoreCase = true)){
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_red_bg_corner_shape) }?.let {
                holder.d_task_dt.setBackgroundDrawable(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                holder.d_task_dt.setTextColor(it)
            }
        }

        if (dashTasksModel.type == "task" &&
            dashTasksModel.taskStatus == "inprogress" ||
            dashTasksModel.taskStatus == "in progress" ||
            dashTasksModel.taskStatus == "In progress" || dashTasksModel.taskStatus == "In Progress"){
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_green_corner_shape) }?.let {
                holder.d_task_dt.setBackgroundDrawable(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                holder.d_task_dt.setTextColor(it)
            }

        }






        //new changes due to button click issue

        holder.right_swipe2_tv.setOnClickListener{
            holder.right_swipe2_layout.performClick()
        }
        holder.right_swipe2_layout.setOnClickListener{
            if (dash_tab_click == "taskOnClick"){
                   if (dashTasksModel.type == "task" && dashTasksModel.assignTo_id == userId) {
                try {
                    var uri: String =
                        "http://maps.google.com/maps?daddr=${dashTasksModel.lat},${dashTasksModel.lng}"
                    var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                    intent.setPackage("com.google.android.apps.maps")
                    context?.startActivity(intent)
                } catch (e: java.lang.Exception) {
                    print(e)
                }

            } else if ( dashTasksModel.lat != "null" && dashTasksModel.event_address != "null" && dashTasksModel.type == "meeting" ) {
                       try {
                           var uri: String =
                               "http://maps.google.com/maps?daddr=${dashTasksModel.lat},${dashTasksModel.lng}"
                           var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                           intent.setPackage("com.google.android.apps.maps")
                           context?.startActivity(intent)
                       } catch (e: java.lang.Exception) {
                           print(e)
                       }
                   }
            }
            else if (dash_tab_click == "meOnClick"){
                if (dashTasksModel.type == "meeting" || dashTasksModel.type == "personal"){
                    eventAcceptRejectApiCall(1,dashTasksModel._id)
                }else{
                    if (context?.isNetworkAvailable(context!!) == true) {
                        updateStatusApiCall("assigned", dashTasksModel._id)
                    }else{
                        CoroutineScope(Dispatchers.IO).launch {
                            /* if (dashTasksModel._id.length>5){
                                val taskst = db!!.taskStatusDao().getAll()
                                if (taskst?.contains("${dashTasksModel._id.toString()}")){
                                    db!!.taskStatusDao().updateTaskStatus("assigned",dashTasksModel._id)
                                }else{
                                    db!!.taskStatusDao().insertAll(0,dashTasksModel._id,"assigned")
                                }
                            }else
                            {*/
                            var jsonObjTimeline = JSONObject()
                            jsonObjTimeline.put("description", "Accepted the assignment")
                            jsonObjTimeline.put("type", 2)
                            jsonObjTimeline.put("createdBy", "")
                            jsonObjTimeline.put("assignedTo", dashTasksModel.assignTo_id)
                            jsonObjTimeline.put("createdAt", "")
                            jsonObjTimeline.put("updatedAt", "")


                            db = AppDatabase.getDatabaseClient(context!!)
                            db!!.salesDao().updateTaskStatus("assigned", dashTasksModel._id)
                            var timelineData = db!!.addTaskTimelineDao().getAll()
                            db!!.addTaskTimelineDao().updateTimelineType2(
                                jsonObjTimeline.toString(),
                                timelineData[position].timeline_id.toString()
                            )

//                        }
                        }
                    }
                }
            }


        }

        holder.right_swipe1_tv.setOnClickListener{
            holder.right_swipe1_layout.performClick()
        }
        holder.right_swipe1_layout.setOnClickListener{
            if (dashTasksModel.type == "task" && dashTasksModel.assignTo_id == userId) {
                try {
                    if (dashTasksModel.taskStatus == "Completed" || dashTasksModel.taskStatus == "Cancelled" || dashTasksModel.taskStatus == "Uncovered") {
                        context?.toast(dashTasksModel.taskStatus)
                    } else {

                        context?.setSharedPref(context!!, "select_status_name", "")
                        var mBottomSheetDialog: Dialog? =
                            context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) };
                        mBottomSheetDialog?.setContentView(R.layout.update_status_popup)
                        var addNote = mBottomSheetDialog?.findViewById<EditText>(R.id.add_note_et)
                        var cancelStatus =
                            mBottomSheetDialog?.findViewById<TextView>(R.id.cancel_status)
                        var updateStatus =
                            mBottomSheetDialog?.findViewById<TextView>(R.id.update_status)

                        var apply_lay =
                            mBottomSheetDialog?.findViewById<LinearLayout>(R.id.apply_lay)
                        var close: ImageView? = mBottomSheetDialog?.findViewById(R.id.close_img)
                        var updateStatusRecyclerView =
                            mBottomSheetDialog?.findViewById<RecyclerView>(R.id.updateStatus_recyclerView)
                        var selectStatusModel = ArrayList<SelectStatusModel>()

                        var list = getStatusArray(dashTasksModel.taskStatus)
                        for (i in 0 until list.size) {
                            selectStatusModel.add(SelectStatusModel(list[i]))
                        }
                        val adapter: RecyclerView.Adapter<*> =
                            SelectStatusAdapter(context, selectStatusModel)
                        val llm = LinearLayoutManager(context)
                        llm.orientation = LinearLayoutManager.VERTICAL
                        updateStatusRecyclerView?.layoutManager = llm
                        updateStatusRecyclerView?.adapter = adapter



                        apply_lay?.setOnClickListener {


                            var taskStatusNew =
                                context?.getSharedPref("select_status_name", context!!).toString()
                            if (!taskStatusNew.equals("")) {
                                var taskStatus = getStatus(taskStatusNew)

                                context?.progressBarShow(context!!)
                                var params = HashMap<String, String>()
                                params["taskStatus"] = taskStatus
                                params["_id"] = dashTasksModel._id
                                params["note"] = addNote?.text.toString()
                                params["assignedTo"] = dashTasksModel.assignTo_id
                                context?.let { it1 ->
                                    apiRequest.postRequestBodyWithHeaders(
                                        it1,
                                        webService.Update_status_url,
                                        params
                                    ) { response ->
                                        var status = response.getString("status")
                                        if (status == "200") {
                                            context?.setSharedPref(
                                                context!!,
                                                "select_status_name",
                                                ""
                                            )
                                            var msg = response.getString("msg")
                                            context?.progressBarDismiss(context!!)
                                            context?.toast(msg)
                                            (context as MainActivity).resetGraph(context as MainActivity)
                                            mBottomSheetDialog?.dismiss()
                                        } else {
                                            var msg = response.getString("msg")
                                            context?.toast(msg)
                                            context?.progressBarDismiss(context!!)
                                        }

                                    }
                                }
                            } else {
                                context?.toast("Kindly select any status")
                            }

                        }
                        cancelStatus?.setOnClickListener {
                            mBottomSheetDialog?.dismiss()
                        }
                        close?.setOnClickListener {
                            mBottomSheetDialog?.dismiss()
                        }




                        mBottomSheetDialog?.setCancelable(true)
                        mBottomSheetDialog?.window?.setLayout(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT
                        );
                        mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
                        mBottomSheetDialog?.show()
                    }

                } catch (e: java.lang.Exception) {

                }
            }

        }

        holder.left_swipe1_tv.setOnClickListener{
            //start view
            holder.left_swipe1_layout.performClick()
        }
        holder.left_swipe1_layout.setOnClickListener {
            if (dashTasksModel.type == "task" && dash_tab_click == "taskOnClick") {
                if (dashTasksModel.assignTo_id == userId && dash_tab_click == "taskOnClick") {
                    if (dashTasksModel.taskStatus == "Assigned" || dashTasksModel.taskStatus == "assigned") {
                        if (context?.isNetworkAvailable(context!!)==true) {
                            updateStatusApiCall("inprogress", dashTasksModel._id)
                        }
                        else{
                            CoroutineScope(Dispatchers.Main).launch {
                                if (dashTasksModel._id.length >5){
                                    db = AppDatabase.getDatabaseClient(context!!)
                                    db!!.salesDao().updateTaskStatus("inprogress",dashTasksModel._id)
                                    db!!.taskStatusDao().insertAll(TaskStatusTable(   0,dashTasksModel._id.toString(),"inprogress"))
                                    (context as MainActivity).callUpdateStatusDb(context as MainActivity)
                                }else{
                                    var jsonObjTimeline = JSONObject()
                                    jsonObjTimeline.put("description", "Started the task")
                                    jsonObjTimeline.put("type", 3)
                                    jsonObjTimeline.put("createdBy", "")
                                    jsonObjTimeline.put("assignedTo", dashTasksModel.assignTo_id)
                                    jsonObjTimeline.put("createdAt", "")
                                    jsonObjTimeline.put("updatedAt", "")

                                    db = AppDatabase.getDatabaseClient(context!!)
                                    db!!.salesDao().updateTaskStatus("inprogress",dashTasksModel._id)
                                    var timelineData = db!!.addTaskTimelineDao().getAll()
                                    db!!.addTaskTimelineDao().updateTimelineType3(jsonObjTimeline.toString(),timelineData[position].timeline_id.toString())
                                    (context as MainActivity).callUpdateStatusDb(context as MainActivity)
                                }
                        }
                        }

                    }
                    if (dashTasksModel.taskStatus == "inprogress" || dashTasksModel.taskStatus == "in progress" || dashTasksModel.taskStatus == "In progress" || dashTasksModel.taskStatus == "In Progress") {
                       if(context?.isNetworkAvailable(context!!)==true) {
                           updateStatusApiCall("completed", dashTasksModel._id)
                       }else{
                            CoroutineScope(Dispatchers.Main).launch {
                               if (dashTasksModel._id.length >5){
                                    db = AppDatabase.getDatabaseClient(context!!)
                                   var tsData = db!!.taskStatusDao().getAll()
                                    db!!.salesDao().updateTaskStatus("completed",dashTasksModel._id)

                                   if (tsData[0].dash_task_id?.contains(dashTasksModel._id) == true){
                                       db!!.taskStatusDao().updateTaskStatus("completed",dashTasksModel._id)
                                   }else{
                                       db!!.taskStatusDao().insertAll(TaskStatusTable(0,dashTasksModel._id,"completed"))
                                   }

                                    (context as MainActivity).callUpdateStatusDb(context as MainActivity)
                                }else{
                                   var jsonObjTimeline = JSONObject()
                                   jsonObjTimeline.put("description", "Accepted the assignment")
                                   jsonObjTimeline.put("type", 2)
                                   jsonObjTimeline.put("createdBy", "")
                                   jsonObjTimeline.put("assignedTo", dashTasksModel.assignTo_id)
                                   jsonObjTimeline.put("createdAt", "")
                                   jsonObjTimeline.put("updatedAt", "")


                                   db = AppDatabase.getDatabaseClient(context!!)
                                   var timelineData = db!!.addTaskTimelineDao().getAll()
                                   db!!.salesDao().updateTaskStatus("completed",dashTasksModel._id)

                                   db!!.addTaskTimelineDao().updateTimelineType2(jsonObjTimeline.toString(),timelineData[position].timeline_id.toString())
                                   (context as MainActivity).callUpdateStatusDb(context as MainActivity)
                               }


                        }
                       }
                    }

                }else{
                    context?.toast("Something went wrong")
                }
            }
          else if (dash_tab_click == "teamOnClick") {
                if (dashTasksModel.taskStatus == "Pending" || dashTasksModel.taskStatus == "pending" || dashTasksModel.taskStatus == "Pending") {
                    context?.setSharedPref(context!!,"reassign_dash_assignee","yes")
                    var bundle = Bundle()
                    bundle.putString("dash_startTime",dashTasksModel.startDate)
                    bundle.putString("dash_endTime",dashTasksModel.endDate)
//                        bundle.putString("task_account_id",dashTasksModel.a)
                    bundle.putString("task_detail_id",dashTasksModel._id)
                    context?.setSharedPref(context!!,"task_detail_id",dashTasksModel._id)
                    var fragment = DashboardAssigneeFilterFragment()
                    fragment.arguments = bundle
                    val activity = context as AppCompatActivity
                    val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.main_fragmet_container, fragment)
                    transaction.addToBackStack(null)
                    transaction.commit()
                } else if (dashTasksModel.taskStatus.equals("Pending",ignoreCase = true) && dashTasksModel.attendee_length == ""){
                    context?.setSharedPref(context!!,"reassign_dash_assignee","yes")
                    var bundle = Bundle()
                    bundle.putString("dash_startTime",dashTasksModel.startDate)
                    bundle.putString("dash_endTime",dashTasksModel.endDate)
                    bundle.putString("task_detail_id",dashTasksModel._id)
                    context?.setSharedPref(context!!,"task_detail_id",dashTasksModel._id)
                    var fragment = DashboardAssigneeFilterFragment()
                    fragment.arguments = bundle
                    val activity = context as AppCompatActivity
                    val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.main_fragmet_container, fragment)
                    transaction.addToBackStack(null)
                    transaction.commit()
                }
                if(dashTasksModel.taskStatus == "Unassigned" || dashTasksModel.taskStatus == "unassigned"){
                    context?.setSharedPref(context!!,"reassign_dash_assignee","yes")
                    var bundle = Bundle()
                    bundle.putString("dash_startTime",dashTasksModel.startDate)
                    bundle.putString("dash_endTime",dashTasksModel.endDate)
//                        bundle.putString("task_account_id",dashTasksModel.a)
                    bundle.putString("task_detail_id",dashTasksModel._id)
                    context?.setSharedPref(context!!,"task_detail_id",dashTasksModel._id)
                    var fragment = DashboardAssigneeFilterFragment()
                    fragment.arguments = bundle
                    val activity = context as AppCompatActivity
                    val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.main_fragmet_container, fragment)
                    transaction.addToBackStack(null)
                    transaction.commit()
                }
            }
           else if (dash_tab_click == "meOnClick"){
                if (dashTasksModel.type == "meeting" || dashTasksModel.type == "personal" ){
                    eventAcceptRejectApiCall(2,dashTasksModel._id)
                }else{
                    if (context?.isNetworkAvailable(context!!)==true) {
                        updateStatusApiCall("rejected", dashTasksModel._id)
                    }else{
                         CoroutineScope(Dispatchers.IO).launch {
                            var jsonObjTimeline = JSONObject()
                            jsonObjTimeline.put("description", "Rejected the task assignment")
                            jsonObjTimeline.put("type", 5)
                            jsonObjTimeline.put("createdBy", "")
                            jsonObjTimeline.put("assignedTo", dashTasksModel.assignTo_id)
                            jsonObjTimeline.put("createdAt", "")
                            jsonObjTimeline.put("updatedAt", "")


                            db = AppDatabase.getDatabaseClient(context!!)
                             var timelineData = db!!.addTaskTimelineDao().getAll()
                            db!!.salesDao().updateTaskStatus("rejected",dashTasksModel._id)
                             db!!.addTaskTimelineDao().updateTimelineType5(jsonObjTimeline.toString(),timelineData[position].timeline_id.toString())
                            /*addTaskTimelineDb("","","","","",dashTasksModel.assignTo_id,
                                "",dashTasksModel.startDate,dashTasksModel.endDate,"","assigned",
                                "",
                                set_timezone,"","","","",jsonObjTimeline.toString(),"",
                                "","","","","","","","")*/

                        }
                    }
                }
            }
        }

        holder.left_swipe2_tv.setOnClickListener{
            holder.left_swipe2_layout.performClick()
        }
        holder.left_swipe2_layout.setOnClickListener{
            if(dashTasksModel.type == "task") {
                if (dashTasksModel.assignTo_id == userId && dash_tab_click == "taskOnClick") {
                    if (dashTasksModel.taskStatus == "Assigned" || dashTasksModel.taskStatus == "assigned") {
                        try {
                            var intent: Intent = Intent(Intent.ACTION_INSERT);
                            intent.type = "vnd.android.cursor.item/event";

                            var cal: Calendar = Calendar.getInstance();
                            var startTime = "${dashTasksModel.startDate}"
                            var endTime = "${dashTasksModel.endDate}"
                            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime)
                            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime)
                            intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
                            intent.putExtra(CalendarContract.Events.TITLE, dashTasksModel.title)
                            intent.putExtra(
                                CalendarContract.Events.DESCRIPTION,
                                dashTasksModel.type
                            )
                            intent.putExtra(CalendarContract.Events.EVENT_LOCATION, "")
                            context?.startActivity(intent)
                        } catch (e: java.lang.Exception) {

                        }
                    }}}
           else if (dash_tab_click == "taskOnClick" && dashTasksModel.type == "meeting" || dashTasksModel.type == "personal"){
                try {
                    var startTimeInMilliseconds : Long = 0
                    var endTimeInMilliseconds : Long = 0
                    var startTime = "${dashTasksModel.startDate}"
                    var endTime = "${dashTasksModel.endDate}"
                    val reminderDayStart: String = startTime.toString()
                    val reminderDayEnd: String = endTime.toString()
                    val formatter =
                        SimpleDateFormat("yyyy-MM-dd HH:mm")
                    val SDate = formatter.parse(reminderDayStart)
                    val EDate = formatter.parse(reminderDayEnd)
                    startTimeInMilliseconds = SDate.time
                    endTimeInMilliseconds = EDate.time

                    val cr: ContentResolver = context!!.contentResolver
                    val values = ContentValues()
                    var intent: Intent = Intent(Intent.ACTION_INSERT);
                    intent.type = "vnd.android.cursor.item/event"
                    var cal: Calendar  = Calendar.getInstance();
//                    var startTime = "${dashTasksModel.startDate}"
//                    var endTime = "${dashTasksModel.endDate}"
                    intent.putExtra(CalendarContract.Events.DTSTART, startTimeInMilliseconds)
                    intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTimeInMilliseconds)
                    intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
                    intent.putExtra(CalendarContract.Events.TITLE, dashTasksModel.title)
                    intent.putExtra(CalendarContract.Events.DESCRIPTION, dashTasksModel.type)
                    intent.putExtra(CalendarContract.Events.EVENT_LOCATION, "")

                    context?.startActivity(intent)

                }catch (e:Exception){

                }
            }
        }



  if (dashTasksModel.type == "task" && dashTasksModel.assignTo_id == userId){
            holder.right_swipe1_layout.visibility = View.VISIBLE
            holder.right_swipe2_layout.visibility = View.VISIBLE

            holder.right_swipe1_tv.setOnClickListener{
                holder.right_swipe1_layout.performClick()
            }
            holder.right_swipe1_layout.setOnClickListener{

                try {
                    if (dashTasksModel.taskStatus == "Completed" || dashTasksModel.taskStatus  == "Cancelled" || dashTasksModel.taskStatus  == "Uncovered") {
                        context?.toast(dashTasksModel.taskStatus )
                    } else {

                        context?.setSharedPref(context!!,"select_status_name","")
                        var mBottomSheetDialog: Dialog? =
                            context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) };
                        mBottomSheetDialog?.setContentView(R.layout.update_status_popup)
                        var addNote = mBottomSheetDialog?.findViewById<EditText>(R.id.add_note_et)
                        var cancelStatus = mBottomSheetDialog?.findViewById<TextView>(R.id.cancel_status)
                        var updateStatus = mBottomSheetDialog?.findViewById<TextView>(R.id.update_status)

                        var  apply_lay= mBottomSheetDialog?.findViewById<LinearLayout>(R.id.apply_lay)
                        var close: ImageView? = mBottomSheetDialog?.findViewById(R.id.close_img)
                        var updateStatusRecyclerView =
                            mBottomSheetDialog?.findViewById<RecyclerView>(R.id.updateStatus_recyclerView)
                        var selectStatusModel = ArrayList<SelectStatusModel>()

                        var list = getStatusArray(dashTasksModel.taskStatus )
                        for (i in 0 until list.size) {
                            selectStatusModel.add(SelectStatusModel(list[i]))
                        }
                        val adapter: RecyclerView.Adapter<*> = SelectStatusAdapter(context, selectStatusModel)
                        val llm = LinearLayoutManager(context)
                        llm.orientation = LinearLayoutManager.VERTICAL
                        updateStatusRecyclerView?.layoutManager = llm
                        updateStatusRecyclerView?.adapter = adapter



                        apply_lay?.setOnClickListener {


                            var taskStatusNew = context?.getSharedPref("select_status_name",context!!).toString()
                            if(!taskStatusNew.equals(""))
                            {
                                var taskStatus = getStatus(taskStatusNew)

                                context?.progressBarShow(context!!)
                                var params = HashMap<String, String>()
                                params["taskStatus"] = taskStatus
                                params["_id"] = dashTasksModel._id
                                params["note"] = addNote?.text.toString()
                                params["assignedTo"] = dashTasksModel.assignTo_id
                                context?.let { it1 ->
                                    apiRequest.postRequestBodyWithHeaders(
                                        it1,
                                        webService.Update_status_url,
                                        params
                                    ) { response ->
                                        var status = response.getString("status")
                                        if (status == "200") {
                                            context?.setSharedPref(context!!,"select_status_name","")
                                            var msg = response.getString("msg")
                                            context?.progressBarDismiss(context!!)
                                            context?.toast(msg)
                                            (context as MainActivity).resetGraph(context as MainActivity)
                                            mBottomSheetDialog?.dismiss()
                                        } else {
                                            var msg = response.getString("msg")
                                            context?.toast(msg)
                                            context?.progressBarDismiss(context!!)
                                        }

                                    }
                                }
                            }
                            else
                            {
                                context?.toast("Kindly select any status")
                            }

                        }
                        cancelStatus?.setOnClickListener {
                            mBottomSheetDialog?.dismiss()
                        }
                        close?.setOnClickListener {
                            mBottomSheetDialog?.dismiss()
                        }




                        mBottomSheetDialog?.setCancelable(true)
                        mBottomSheetDialog?.window?.setLayout(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT
                        );
                        mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
                        mBottomSheetDialog?.show()
                    }

                }catch (e:java.lang.Exception){

                }
            }

            holder.right_swipe2_tv.setOnClickListener{
                holder.right_swipe2_layout.performClick()
            }
            holder.right_swipe2_layout.setOnClickListener{
                try {
                    var uri: String = "http://maps.google.com/maps?daddr=${dashTasksModel.lat},${dashTasksModel.lng}"
                    var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                    intent.setPackage("com.google.android.apps.maps")
                    context?.startActivity(intent)
                }catch (e:java.lang.Exception){
                    print(e)
                }

            }

        }
        else
        {
            holder.right_swipe1_layout.visibility = View.GONE
            holder.right_swipe2_layout.visibility = View.GONE
            holder.left_swipe1_layout.visibility = View.GONE
            holder.left_swipe2_layout.visibility = View.GONE
        }



        if(dashTasksModel.type == "task"){
             userId = context?.getSharedPref("userId",context!!).toString()
            if (dashTasksModel.assignTo_id == userId && dash_tab_click == "taskOnClick"){
                holder.left_swipe1_layout.visibility = View.VISIBLE
                holder.left_swipe2_layout.visibility = View.VISIBLE
                if (dashTasksModel.taskStatus == "Assigned" || dashTasksModel.taskStatus == "assigned"){
                    holder.left_swipe1_layout.visibility = View.VISIBLE
                    holder.left_swipe2_layout.visibility = View.VISIBLE
                    holder.left_swipe1_tv.text = "Start"
                    context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                        holder.left_swipe1_tv.setTextColor(it)
                    }
                    context?.let { ContextCompat.getDrawable(it,R.drawable.call_swipe_button_corner_green_bg) }?.let {
                        holder.left_swipe1_layout.setBackgroundDrawable(it)
                    }
                    holder.left_swipe1_tv.setCompoundDrawablesWithIntrinsicBounds(0,
                        R.drawable.ic_dash_start_icon, 0, 0)

                    holder.left_swipe1_tv.setOnClickListener{
                        //start view
                        holder.left_swipe1_layout.performClick()
                    }
                    holder.left_swipe1_layout.setOnClickListener{

                        if (context?.isNetworkAvailable(context!!)==true) {
                            updateStatusApiCall("inprogress",dashTasksModel._id)
                        }
                        else{
                            CoroutineScope(Dispatchers.Main).launch {
                                if (dashTasksModel._id.length >5){
                                    db = AppDatabase.getDatabaseClient(context!!)
                                    db!!.salesDao().updateTaskStatus("inprogress",dashTasksModel._id)
                                    db!!.taskStatusDao().insertAll(TaskStatusTable(   0,dashTasksModel._id.toString(),"inprogress"))
                                    context?.toast("Task Updated Successfully")
                                    (context as MainActivity).callUpdateStatusDb(context as MainActivity)

                                }else{
                                    var jsonObjTimeline = JSONObject()
                                    jsonObjTimeline.put("description", "Started the task")
                                    jsonObjTimeline.put("type", 3)
                                    jsonObjTimeline.put("createdBy", "")
                                    jsonObjTimeline.put("assignedTo", dashTasksModel.assignTo_id)
                                    jsonObjTimeline.put("createdAt", "")
                                    jsonObjTimeline.put("updatedAt", "")
                                    context?.toast("Task Updated Successfully")
                                    db = AppDatabase.getDatabaseClient(context!!)
                                    db!!.salesDao().updateTaskStatus("inprogress",dashTasksModel._id)
                                    var timelineData = db!!.addTaskTimelineDao().getAll()
                                    db!!.addTaskTimelineDao().updateTimelineType3(jsonObjTimeline.toString(),timelineData[position].timeline_id.toString())
                                    (context as MainActivity).callUpdateStatusDb(context as MainActivity)

                                }



                             /*   var jsonObjTimeline = JSONObject()
                                jsonObjTimeline.put("description", "Started the task")
                                jsonObjTimeline.put("type", 3)
                                jsonObjTimeline.put("createdBy", "")
                                jsonObjTimeline.put("assignedTo", dashTasksModel.assignTo_id)
                                jsonObjTimeline.put("createdAt", "")
                                jsonObjTimeline.put("updatedAt", "")


                                db = AppDatabase.getDatabaseClient(context!!)
                                var timelineData = db!!.addTaskTimelineDao().getAll()
                                db!!.dashBoardDao().updateTaskStatus("inprogress",dashTasksModel._id)
                                db!!.addTaskTimelineDao().updateTimelineType3(jsonObjTimeline.toString(),timelineData[position].timeline_id.toString())

                       */     }
                        }

                    }

                    holder.left_swipe2_tv.text = "Reminder"
                    context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                        holder.left_swipe2_tv.setTextColor(it)
                    }
                    context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_yellow_bg) }?.let {
                        holder.left_swipe2_layout.setBackgroundDrawable(it)
                    }
                    holder.left_swipe1_layout.visibility = View.VISIBLE
                    holder.left_swipe2_layout.visibility = View.VISIBLE
                    holder.left_swipe2_tv.setCompoundDrawablesWithIntrinsicBounds(0,
                        R.drawable.ic_dash_alarm_icon, 0, 0)

                    holder.left_swipe2_tv.setOnClickListener{
                        holder.left_swipe2_layout.performClick()
                    }
                    holder.left_swipe2_layout.setOnClickListener{
                        try {
                            var intent: Intent = Intent(Intent.ACTION_INSERT);
                            intent.type = "vnd.android.cursor.item/event";

                            var cal: Calendar  = Calendar.getInstance();
                            var startTime = "${dashTasksModel.startDate}"
                            var endTime = "${dashTasksModel.endDate}"
                            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime)
                            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime)
                            intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
                            intent.putExtra(CalendarContract.Events.TITLE, dashTasksModel.title)
                            intent.putExtra(CalendarContract.Events.DESCRIPTION, dashTasksModel.type)
                            intent.putExtra(CalendarContract.Events.EVENT_LOCATION, "")
                            context?.startActivity(intent)
                        }catch (e:java.lang.Exception){

                        }
                    }
                }
                 if (dashTasksModel.taskStatus == "inprogress" || dashTasksModel.taskStatus == "in progress" || dashTasksModel.taskStatus == "In progress" || dashTasksModel.taskStatus == "In Progress"){
                    holder.left_swipe1_layout.visibility = View.VISIBLE

                    holder.left_swipe1_tv.text = "Complete"
                    context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                        holder.left_swipe1_tv.setTextColor(it)
                    }
                    context?.let { ContextCompat.getDrawable(it,R.drawable.call_swipe_button_corner_green_bg) }?.let {
                        holder.left_swipe1_layout.setBackgroundDrawable(it)
                    }
                    holder.left_swipe1_tv.setCompoundDrawablesWithIntrinsicBounds(0,
                        R.drawable.ic_dash_complete_icon, 0, 0)
                    holder.left_swipe2_layout.visibility = View.GONE
                    holder.left_swipe1_tv.setOnClickListener{
                        holder.left_swipe1_layout.performClick()
                    }
                    holder.left_swipe1_layout.setOnClickListener{
                        if(context?.isNetworkAvailable(context!!)==true) {
                            updateStatusApiCall("completed", dashTasksModel._id)
                        }else{
                            CoroutineScope(Dispatchers.Main).launch {
                                if (dashTasksModel._id.length >5){
                                    db = AppDatabase.getDatabaseClient(context!!)
                                    var tsData = db!!.taskStatusDao().getAll()
                                    db!!.salesDao().updateTaskStatus("completed",dashTasksModel._id)
                                    if (tsData[0].dash_task_id?.contains(dashTasksModel._id) == true){
                                        db!!.taskStatusDao().updateTaskStatus("completed",dashTasksModel._id)
                                    }else{
                                        db!!.taskStatusDao().insertAll(TaskStatusTable(0,dashTasksModel._id,"completed"))
                                    }
                                    context?.toast("Task Updated Successfully")
                                    (context as MainActivity).callUpdateStatusDb(context as MainActivity)
                                }else{
                                    var jsonObjTimeline = JSONObject()
                                    jsonObjTimeline.put("description", "Accepted the assignment")
                                    jsonObjTimeline.put("type", 2)
                                    jsonObjTimeline.put("createdBy", "")
                                    jsonObjTimeline.put("assignedTo", dashTasksModel.assignTo_id)
                                    jsonObjTimeline.put("createdAt", "")
                                    jsonObjTimeline.put("updatedAt", "")

                                    context?.toast("Task Updated Successfully")
                                    db = AppDatabase.getDatabaseClient(context!!)
                                    var timelineData = db!!.addTaskTimelineDao().getAll()
                                    db!!.salesDao().updateTaskStatus("completed",dashTasksModel._id)

                                    db!!.addTaskTimelineDao().updateTimelineType2(jsonObjTimeline.toString(),timelineData[position].timeline_id.toString())
                                    (context as MainActivity).callUpdateStatusDb(context as MainActivity)
                                }
                        /*        var jsonObjTimeline = JSONObject()
                                jsonObjTimeline.put("description", "Accepted the assignment")
                                jsonObjTimeline.put("type", 2)
                                jsonObjTimeline.put("createdBy", "")
                                jsonObjTimeline.put("assignedTo", dashTasksModel.assignTo_id)
                                jsonObjTimeline.put("createdAt", "")
                                jsonObjTimeline.put("updatedAt", "")


                                db = AppDatabase.getDatabaseClient(context!!)

                                db!!.dashBoardDao().updateTaskStatus("completed",dashTasksModel._id)
                                var timelineData = db!!.addTaskTimelineDao().getAll()
                                db!!.addTaskTimelineDao().updateTimelineType2(jsonObjTimeline.toString(),timelineData[position].timeline_id.toString())
*/
                            }
                        }

                    }

                }
            }

            if (dash_tab_click == "teamOnClick"){
                if (dashTasksModel.taskStatus == "Pending" || dashTasksModel.taskStatus == "pending" || dashTasksModel.taskStatus == "Pending"){
                    holder.left_swipe2_layout.visibility = View.GONE
                    holder.left_swipe1_layout.visibility = View.VISIBLE
                    holder.right_swipe2_layout.visibility = View.GONE
                    holder.right_swipe1_layout.visibility = View.GONE
                    holder.left_swipe1_tv.text = "Reassign"

                    context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                        holder.left_swipe1_tv.setTextColor(it)
                    }
                    context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_yellow_bg) }?.let {
                        holder.left_swipe1_layout.setBackgroundDrawable(it)
                    }
                    holder.left_swipe1_tv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_reassign_icon, 0, 0)
                    holder.left_swipe1_tv.setOnClickListener{
                        holder.left_swipe1_layout.performClick()
                    }
                    holder.left_swipe1_layout.setOnClickListener{

                        context?.setSharedPref(context!!,"reassign_dash_assignee","yes")
                        var bundle = Bundle()
                        bundle.putString("dash_startTime",dashTasksModel.startDate)
                        bundle.putString("dash_endTime",dashTasksModel.endDate)
//                        bundle.putString("task_account_id",dashTasksModel.a)
                        bundle.putString("task_detail_id",dashTasksModel._id)
                        context?.setSharedPref(context!!,"task_detail_id",dashTasksModel._id)
                        var fragment = DashboardAssigneeFilterFragment()
                        fragment.arguments = bundle
                        val activity = context as AppCompatActivity
                        val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
                        transaction.replace(R.id.main_fragmet_container, fragment)
                        transaction.addToBackStack(null)
                        transaction.commit()
                    }
                }
               //new changes coz of no attendee
              else if (dashTasksModel.taskStatus.equals("Pending",ignoreCase = true) && dashTasksModel.attendee_length == ""){

                    holder.left_swipe2_layout.visibility = View.GONE
                    holder.left_swipe1_layout.visibility = View.VISIBLE
                    holder.right_swipe2_layout.visibility = View.GONE
                    holder.right_swipe1_layout.visibility = View.GONE
                    holder.left_swipe1_tv.text = "Assign"
                    context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                        holder.left_swipe1_tv.setTextColor(it)
                    }
                    context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_yellow_bg) }?.let {
                        holder.left_swipe1_layout.setBackgroundDrawable(it)
                    }
                    holder.left_swipe1_tv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_reassign_icon, 0, 0)

                    holder.left_swipe1_tv.setOnClickListener{
                        holder.left_swipe1_layout.performClick()
                    }
                    holder.left_swipe1_layout.setOnClickListener{
                        updateStatusApiCallAssignee("Pending",dashTasksModel._id,userId)

                        context?.setSharedPref(context!!,"reassign_dash_assignee","yes")
                        var bundle = Bundle()
                        bundle.putString("dash_startTime",dashTasksModel.startDate)
                        bundle.putString("dash_endTime",dashTasksModel.endDate)
//                        bundle.putString("task_account_id",dashTasksModel.a)
                        bundle.putString("task_detail_id",dashTasksModel._id)
                        context?.setSharedPref(context!!,"task_detail_id",dashTasksModel._id)
                        var fragment = DashboardAssigneeFilterFragment()
                        fragment.arguments = bundle
                        val activity = context as AppCompatActivity
                        val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
                        transaction.replace(R.id.main_fragmet_container, fragment)
                        transaction.addToBackStack(null)
                        transaction.commit()
                    }
                }





                if(dashTasksModel.taskStatus == "Unassigned" || dashTasksModel.taskStatus == "unassigned"){
                     userId = context?.getSharedPref("userId",context!!).toString()
                    holder.left_swipe2_layout.visibility = View.GONE
                    holder.left_swipe1_layout.visibility = View.VISIBLE
                    holder.right_swipe2_layout.visibility = View.GONE
                    holder.right_swipe1_layout.visibility = View.GONE
                    holder.left_swipe1_tv.text = "Assign"
                    context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                        holder.left_swipe1_tv.setTextColor(it)
                    }
                    context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_yellow_bg) }?.let {
                        holder.left_swipe1_layout.setBackgroundDrawable(it)
                    }
                    holder.left_swipe1_tv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_reassign_icon, 0, 0)

                    holder.left_swipe1_tv.setOnClickListener{
                        holder.left_swipe1_layout.performClick()
                    }
                    holder.left_swipe1_layout.setOnClickListener{

                         updateStatusApiCallAssignee("Pending",dashTasksModel._id,userId)
                        context?.setSharedPref(context!!,"reassign_dash_assignee","yes")
                        var bundle = Bundle()
                        bundle.putString("dash_startTime",dashTasksModel.startDate)
                        bundle.putString("dash_endTime",dashTasksModel.endDate)
                        bundle.putString("task_detail_id",dashTasksModel._id)
                        context?.setSharedPref(context!!,"task_detail_id",dashTasksModel._id)
                        var fragment = DashboardAssigneeFilterFragment()
                        fragment.arguments = bundle
                        val activity = context as AppCompatActivity
                        val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
                        transaction.replace(R.id.main_fragmet_container, fragment)
                        transaction.addToBackStack(null)
                        transaction.commit()
                    }
                }

//                if (dashTasksModel.taskStatus.equals("pending",ignoreCase = true) && dashTasksModel.assigned_toObj.isNull())

            }
        }
        if (dash_tab_click == "taskOnClick" && dashTasksModel.type == "meeting" || dashTasksModel.type == "personal"){
            holder.left_swipe1_layout.visibility = View.GONE
            holder.left_swipe2_layout.visibility = View.VISIBLE
            holder.right_swipe1_layout.visibility = View.GONE
            holder.right_swipe2_layout.visibility = View.GONE

            holder.left_swipe2_tv.setOnClickListener{
                holder.left_swipe2_layout.performClick()
            }
            holder.left_swipe2_layout.setOnClickListener{
                try {
                    var startTimeInMilliseconds : Long = 0
                    var endTimeInMilliseconds : Long = 0
                    var startTime = "${dashTasksModel.startDate}"
                    var endTime = "${dashTasksModel.endDate}"
                    val reminderDayStart: String = startTime.toString()
                    val reminderDayEnd: String = endTime.toString()
                    val formatter =
                        SimpleDateFormat("yyyy-MM-dd HH:mm")
                    val SDate = formatter.parse(reminderDayStart)
                    val EDate = formatter.parse(reminderDayEnd)
                    startTimeInMilliseconds = SDate.time
                    endTimeInMilliseconds = EDate.time


                    val cr: ContentResolver = context!!.contentResolver
                    val values = ContentValues()
                    var intent: Intent = Intent(Intent.ACTION_INSERT);
                    intent.type = "vnd.android.cursor.item/event"
                    var cal: Calendar  = Calendar.getInstance();
//                    var startTime = "${dashTasksModel.startDate}"
//                    var endTime = "${dashTasksModel.endDate}"
                    intent.putExtra(CalendarContract.Events.DTSTART, startTimeInMilliseconds)
                    intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTimeInMilliseconds)
                    intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)
                    intent.putExtra(CalendarContract.Events.TITLE, dashTasksModel.title)
                    intent.putExtra(CalendarContract.Events.DESCRIPTION, dashTasksModel.type)
                    intent.putExtra(CalendarContract.Events.EVENT_LOCATION, "")

                    context?.startActivity(intent)

                }catch (e:Exception){

                }
            }

            if (dashTasksModel.lat != "null" && dashTasksModel.event_address != "null" && dashTasksModel.type == "meeting" ) {
                holder.right_swipe1_layout.visibility = View.GONE
                holder.right_swipe2_layout.visibility = View.VISIBLE
                holder.right_swipe2_tv.setOnClickListener {
                    try {
                        var uri: String =
                            "http://maps.google.com/maps?daddr=${dashTasksModel.lat},${dashTasksModel.lng}"
                        var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                        intent.setPackage("com.google.android.apps.maps")
                        context?.startActivity(intent)
                    } catch (e: java.lang.Exception) {
                        print(e)
                    }
                }
                holder.right_swipe2_layout.setOnClickListener {
                    try {
                        var uri: String =
                            "http://maps.google.com/maps?daddr=${dashTasksModel.lat},${dashTasksModel.lng}"
                        var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                        intent.setPackage("com.google.android.apps.maps")
                        context?.startActivity(intent)
                    } catch (e: java.lang.Exception) {
                        print(e)
                    }
                }
            }


        }



         if (dash_tab_click == "meOnClick"){
            holder.left_swipe2_layout.visibility = View.GONE
            holder.left_swipe1_layout.visibility = View.VISIBLE
            holder.left_swipe1_tv.text = "Reject"
            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                holder.left_swipe1_tv.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_white_bg) }?.let {
                holder.left_swipe1_layout.setBackgroundDrawable(it)
            }
            holder.left_swipe1_tv.setCompoundDrawablesWithIntrinsicBounds(0,
                R.drawable.ic_reject_icon, 0, 0)

            holder.left_swipe1_tv.setOnClickListener{
                holder.left_swipe1_layout.performClick()
            }
            holder.left_swipe1_layout.setOnClickListener{

                if (dashTasksModel.type == "meeting" || dashTasksModel.type == "personal" ){
                    eventAcceptRejectApiCall(2,dashTasksModel._id)
                }else{
                    if (context?.isNetworkAvailable(context!!)==true) {
                        updateStatusApiCall("rejected", dashTasksModel._id)
                    }else{
                        CoroutineScope(Dispatchers.IO).launch {
                            var jsonObjTimeline = JSONObject()
                            jsonObjTimeline.put("description", "Rejected the task assignment")
                            jsonObjTimeline.put("type", 5)
                            jsonObjTimeline.put("createdBy", "")
                            jsonObjTimeline.put("assignedTo", dashTasksModel.assignTo_id)
                            jsonObjTimeline.put("createdAt", "")
                            jsonObjTimeline.put("updatedAt", "")


                            db = AppDatabase.getDatabaseClient(context!!)
                            var timelineData = db!!.addTaskTimelineDao().getAll()
                            db!!.salesDao().updateTaskStatus("rejected",dashTasksModel._id)
                            db!!.addTaskTimelineDao().updateTimelineType5(jsonObjTimeline.toString(),timelineData[position].timeline_id.toString())
                            /*addTaskTimelineDb("","","","","",dashTasksModel.assignTo_id,
                                "",dashTasksModel.startDate,dashTasksModel.endDate,"","assigned",
                                "",
                                set_timezone,"","","","",jsonObjTimeline.toString(),"",
                                "","","","","","","","")*/

                        }
                    }
                }

            }

            holder.right_swipe1_layout.visibility = View.GONE
            holder.right_swipe2_layout.visibility = View.VISIBLE
            holder.right_swipe2_tv.text = "Accept"
            context?.let { ContextCompat.getColor(it,R.color.green) }?.let {
                holder.right_swipe2_tv.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_white_bg) }?.let {
                holder.right_swipe2_layout.setBackgroundDrawable(it)
            }
            holder.right_swipe2_tv.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_accept_icon, 0, 0)

            holder.right_swipe2_tv.setOnClickListener{
                holder.right_swipe2_layout.performClick()
            }
            holder.right_swipe2_layout.setOnClickListener{
                if (dashTasksModel.type == "meeting" || dashTasksModel.type == "personal"){
                    eventAcceptRejectApiCall(1,dashTasksModel._id)
                }else{
                    if (context?.isNetworkAvailable(context!!) == true) {
                        updateStatusApiCall("assigned", dashTasksModel._id)
                    }else{
                        CoroutineScope(Dispatchers.IO).launch {
                            var jsonObjTimeline = JSONObject()
                            jsonObjTimeline.put("description", "Accepted the assignment")
                            jsonObjTimeline.put("type", 2)
                            jsonObjTimeline.put("createdBy", "")
                            jsonObjTimeline.put("assignedTo", dashTasksModel.assignTo_id)
                            jsonObjTimeline.put("createdAt", "")
                            jsonObjTimeline.put("updatedAt", "")


                            db = AppDatabase.getDatabaseClient(context!!)
                            var timelineData = db!!.addTaskTimelineDao().getAll()
                            db!!.salesDao().updateTaskStatus("assigned",dashTasksModel._id)
                            db!!.addTaskTimelineDao().updateTimelineType2(jsonObjTimeline.toString(),timelineData[position].timeline_id.toString())
                            /*addTaskTimelineDb("","","","","",dashTasksModel.assignTo_id,
                                "",dashTasksModel.startDate,dashTasksModel.endDate,"","assigned",
                                "",
                                set_timezone,"","","","",jsonObjTimeline.toString(),"",
                                "","","","","","","","")*/

                        }
                    }
                }

            }



        }



        holder.swipeView.swipeGestureListener = object : SwipeGestureListener {
            override fun onSwipedLeft(swipeActionView: SwipeActionView): Boolean {
                swipeActionView.animateToOriginalPosition(3000)
                return false
            }

            override fun onSwipedRight(swipeActionView: SwipeActionView): Boolean {
                swipeActionView.animateToOriginalPosition(3000)
                return false
            }
        }

        holder.d_task_title.setOnClickListener{
            holder.itemView.performClick()
        }
        holder.below_title_layout.setOnClickListener{
            holder.itemView.performClick()
        }
        holder.d_task_dt.setOnClickListener{
            holder.itemView.performClick()
        }
        holder.d_task_time.setOnClickListener{
            holder.itemView.performClick()
        }
        holder.d_emp_name.setOnClickListener{
            holder.itemView.performClick()
        }

        holder.swiping_layout.setOnClickListener{
            holder.itemView.performClick()
        }



        holder.itemView.setOnClickListener{
            context?.setSharedPref(context!!,"fromClass","Dash")
            if (dashTasksModel.type == "task"){
                context?.setSharedPref(context!!,"assignee_image",dashTasksModel.assignTo_img)
                context?.setSharedPref(context!!, "task_id", dashTasksModel._id)
                var bundle = Bundle()
                bundle.putString("task_id",dashTasksModel._id)
                bundle.putString("task_start_date",dashTasksModel.startDate)
                bundle.putString("task_end_date",dashTasksModel.endDate)
                bundle.putString("task_assignto_id",dashTasksModel.assignTo_id)
                bundle.putString("task_assignee_name",dashTasksModel.assignTo_name)
                context?.setSharedPref(context!!,"td_assignee_name",dashTasksModel.assignTo_name)
                var fragment = TaskDetailsFragment()
                fragment.arguments = bundle
                val activity = context as AppCompatActivity
                val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
                transaction.replace(R.id.main_fragmet_container, fragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }else if (dashTasksModel.type == "meeting"){
                var bundle = Bundle()
                bundle.putString("event_id",dashTasksModel._id)
                bundle.putString("event_lat",dashTasksModel.lat)
                bundle.putString("event_lng",dashTasksModel.lng)

                var fragment = EventDetailsFragment()
                fragment.arguments = bundle
                val activity = context as AppCompatActivity
                val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
                transaction.replace(R.id.main_fragmet_container, fragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }else if (dashTasksModel.type == "personal"){
                var bundle = Bundle()
                bundle.putString("event_id",dashTasksModel._id)
                bundle.putString("event_lat",dashTasksModel.lat)
                bundle.putString("event_lng",dashTasksModel.lng)

                var fragment = EventDetailsFragment()
                fragment.arguments = bundle
                val activity = context as AppCompatActivity
                val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
                transaction.replace(R.id.main_fragmet_container, fragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }
        }



        //task and event
        if (dashTasksModel.type == "task"){
            holder.event_type_layout.visibility = View.GONE
            holder.d_task_title.setPadding(8,8,8,8)
        }else if (dashTasksModel.type == "meeting"){
            holder.event_type_layout.visibility = View.VISIBLE
            context?.let { ContextCompat.getColor(it,R.color.dash_event_type_blue) }?.let {
                holder.dash_eventType.setTextColor(it)
            }
            holder.dash_eventType.text = dashTasksModel.event_eventType
            holder.dash_event_location.text = dashTasksModel.event_address
            if (dashTasksModel.attendee_length.toInt() == 0 || dashTasksModel.attendee_length == ""){
                holder.d_emp_name.text = dashTasksModel.organizerName

                try {
                    Picasso.get().load(dashTasksModel.organizerImage).placeholder(R.drawable.ic_dummy_user_pic).into(holder.d_emp_img)
                }catch (e:Exception){
                    print(e)
                }

            }else{
                if (dashTasksModel.attendee_length.toInt() >= 2){
                    holder.d_emp_name.text = "+${dashTasksModel.attendee_length.toInt() - 1} More"
                    context?.let { ContextCompat.getColor(it,R.color.dash_event_type_blue) }?.let {
                        holder.d_emp_name.setTextColor(it)
                    }
                }else{
                    holder.d_emp_name.text = capFirstLetter(dashTasksModel.attendee_name)

                }
                try {
                    Picasso.get().load(dashTasksModel.attendee_img).placeholder(R.drawable.ic_dummy_user_pic).into(holder.d_emp_img)
                }catch (e:Exception){
                    print(e)
                }

            }

        }

        else if (dashTasksModel.type == "personal"){
            holder.event_type_layout.visibility = View.VISIBLE
            holder.dash_event_location.visibility = View.GONE
            context?.let { ContextCompat.getColor(it,R.color.orange) }?.let {
                holder.dash_eventType.setTextColor(it)
            }
            holder.dash_eventType.text = dashTasksModel.event_eventType

            if (dashTasksModel.attendee_name == "" || dashTasksModel.attendee_name == "null" ||dashTasksModel.attendee_length.toInt() <= 0 || dashTasksModel.attendee_length == ""){
                holder.d_emp_name.text = dashTasksModel.organizerName
            }else{
                holder.d_emp_name.text = dashTasksModel.attendee_name
            }

            try {
                if (dashTasksModel.attendee_length.toInt() <= 0){
                    Picasso.get().load(dashTasksModel.organizerImage).placeholder(R.drawable.ic_dummy_user_pic).into(holder.d_emp_img)
                }else{
                    Picasso.get().load(dashTasksModel.attendee_img).placeholder(R.drawable.ic_dummy_user_pic).into(holder.d_emp_img)
                }

            }catch (e:Exception){
                print(e)
            }


        }


    }



    private fun eventAcceptRejectApiCall(type:Int,event_Id:String){
        val paramR = HashMap<String,Any>()
        paramR["type"] = type
        paramR["id"] = event_Id
        context?.let { it1 ->
            apiRequest.postRequestBodyWithHeadersAny(
                it1,
                webService.Event_accept_reject_url,
                paramR
            ) { response ->
                var status = response.getString("status")
                var msg = response.getString("msg")
                if (status == "200") {
                    if (dash_tab_click == "taskOnClick"){
                        (context as MainActivity).resetGraph(context as MainActivity)
                    }else if (dash_tab_click == "meOnClick"){
                        (context as MainActivity).resetMeOnClick(context as MainActivity)
                    }else if (dash_tab_click == "teamOnClick"){
                        (context as MainActivity).resetTeamOnClick(context as MainActivity)
                    }
                    context?.toast(msg)
                    context?.progressBarDismiss(context!!)

                } else {
                    context?.toast(msg)
                    context?.progressBarDismiss(context!!)
                }

            }
        }
    }


    fun updateStatusApiCall(status:String,taskId:String){
        var params = HashMap<String,Any>()
        params["taskStatus"]=status
        params["_id"] = taskId
        context?.let { it1 ->
            apiRequest.postRequestBodyWithHeadersAny(
                it1,
                webService.Update_status_url,
                params
            ) { response ->
                try {
                var status = response.getString("status")

                var msg = response.getString("msg")
                if (status == "200") {
                    if (dash_tab_click == "taskOnClick"){
                        (context as MainActivity).resetGraph(context as MainActivity)
                    }else if (dash_tab_click == "meOnClick"){
                        (context as MainActivity).resetMeOnClick(context as MainActivity)
                    }else if (dash_tab_click == "teamOnClick"){
                        (context as MainActivity).resetTeamOnClick(context as MainActivity)
                    }
                    context?.toast(msg)
                    context?.progressBarDismiss(context!!)

                } else {
                    context?.toast(msg)
                    context?.progressBarDismiss(context!!)
                }

            }catch (e:Exception){
                    context?.toast(e.toString())
                }
            }
        }
    }
    fun updateStatusApiCallAssignee(status:String,taskId:String , assignto : String){
        var params = HashMap<String,Any>()
        params["taskStatus"]=status
        params["_id"] = taskId
        params["assignedTo"] = assignto
        context?.let { it1 ->
            apiRequest.postRequestBodyWithHeadersAny(
                it1,
                webService.Update_status_url,
                params
            ) { response ->
                try {
                    var status = response.getString("status")

                    var msg = response.getString("msg")
                    if (status == "200") {
                        if (dash_tab_click == "taskOnClick"){
                            (context as MainActivity).resetGraph(context as MainActivity)
                        }else if (dash_tab_click == "meOnClick"){
                            (context as MainActivity).resetMeOnClick(context as MainActivity)
                        }else if (dash_tab_click == "teamOnClick"){
                            (context as MainActivity).resetTeamOnClick(context as MainActivity)
                        }
                        context?.toast(msg)
                        context?.progressBarDismiss(context!!)

                    } else {
                        context?.toast(msg)
                        context?.progressBarDismiss(context!!)
                    }

                }catch (e:Exception){
                    context?.toast(e.toString())
                }
            }
        }
    }

    private fun getStatusArray(Selected: String) :Array<String> {
        if (Selected.equals("In Progress",ignoreCase = true) || Selected.equals("InProgress",ignoreCase = true)) {
            return arrayOf("Completed","Cancelled")
        }
        else if (Selected.equals("Unassigned",ignoreCase = true)) {
            return arrayOf("Cancelled","Uncovered")
        }
        else if (Selected.equals( "Pending",ignoreCase = true)) {
            return arrayOf("Cancelled","Uncovered")
        }
        else if (Selected.equals ("Assigned" ,ignoreCase = true)) {
            return arrayOf("In progress","Cancelled","Uncovered")
        }
        return arrayOf("")
    }

    class MyViewHolder constructor(itemView : View) : RecyclerView.ViewHolder(itemView) {
        var d_task_title : TextView = itemView.findViewById(R.id.d_task_title)
        var d_task_dt : TextView = itemView.findViewById(R.id.d_task_dt)
        var d_task_time : TextView = itemView.findViewById(R.id.d_task_time)
        var d_emp_img : ImageView = itemView.findViewById(R.id.d_emp_img)
        var d_emp_name : TextView = itemView.findViewById(R.id.d_emp_name)
        var swipeView : SwipeActionView = itemView.findViewById(R.id.dash_tasks_swipe_view)
        var swiping_layout : CardView = itemView.findViewById(R.id.swiping_layout)

        var event_type_layout : ConstraintLayout = itemView.findViewById(R.id.event_type_layout)
        var dash_eventType : TextView = itemView.findViewById(R.id.dash_eventType)
        var dash_event_location : TextView = itemView.findViewById(R.id.dash_event_location)

        var left_swipe1_layout : ConstraintLayout = itemView.findViewById(R.id.left_swipe1_layout)
        var left_swipe1_tv : TextView = itemView.findViewById(R.id.left_swipe1_tv)
        var left_swipe2_layout : ConstraintLayout = itemView.findViewById(R.id.left_swipe2_layout)
        var left_swipe2_tv : TextView =    itemView.findViewById(R.id.left_swipe2_tv)

        var right_swipe1_layout : ConstraintLayout = itemView.findViewById(R.id.right_swipe1_layout)
        var right_swipe1_tv : TextView = itemView.findViewById(R.id.right_swipe1_tv)
        var right_swipe2_layout : ConstraintLayout = itemView.findViewById(R.id.right_swipe2_layout)
        var right_swipe2_tv : TextView =    itemView.findViewById(R.id.right_swipe2_tv)

        var emergency_icon : ImageView = itemView.findViewById(R.id.emergency_icon)

        var below_title_layout : ConstraintLayout = itemView.findViewById(R.id.below_title_layout)

        var dot : ImageView = itemView.findViewById(R.id.dot)

    }


    private fun getStatus(Selected: String) : String {
        if (Selected.equals("Inprogress",ignoreCase = true) || Selected.equals("In Progress",ignoreCase = true) || Selected.equals("In progress")) {
            return "inprogress"
        }
        else if (Selected == "Uncovered") {
            return "uncovered"
        }
        else if (Selected == "Cancelled" || Selected == "Canceled"){
            return "cancelled"
        }
        else if (Selected == "Completed") {
            return "completed"
        }
        else if (Selected == "Unassigned") {
            return "Unassigned"
        }
        else if (Selected == "Pending") {
            return "Pending"
        }
        else if (Selected == "Assigned" ||Selected.equals("Scheduled",ignoreCase = true)) {
            return "assigned"
        }
      return ""
    }


    //add task #task status timeline
    private fun addTaskTimelineDb(accID:String,deptID:String,serviceID:String,assignedBY:String,
                                  createdBY:String,assignedTO:String,reccurOption:String,startDATE:String,endDATE:String,
                                  taskDESC:String,taskSTATUS:String,prod:String,tz:String,createdAT:String,updateAT:String,
                                  t1:String,t2:String,t3:String,
                                  t4:String,t5:String,t6:String,t7:String,t8:String,t9:String,
                                  t10:String,t11:String){
        var cID =   context?.getSharedPref( "companyId", context!!).toString()
        CoroutineScope(Dispatchers.IO).launch {
            db = AppDatabase.getDatabaseClient(context!!)
            db!!.addTaskTimelineDao().insertAll(AddTaskTimeline(0,cID
                ,accID,deptID,serviceID,assignedBY
                ,createdBY,assignedTO,reccurOption,
                startDATE,endDATE,taskDESC,
                taskSTATUS,prod,tz,createdAT,updateAT,
                t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11));
        }
    }
}
package com.salesc2.data.network.api.response

data class ResponseBillingDetails(
    val `data`: List<Data>,
    val msg: String,
    val status: Int
) {
    data class Data(
        val _id: String,
        val billingContact: BillingContact,
        val price: Double,
        val status: String,
        val type: String
    ) {
        data class BillingContact(
            val __v: Int,
            val _id: String,
            val addresLine1: String,
            val addressLine2: String,
            val billingDetail: BillingDetail,
            val cardDetail: CardDetail,
            val city: String,
            val companyName: String,
            val country: String,
            val countryCode: String,
            val createdAt: String,
            val createdBy: String,
            val customerId: String,
            val email: String,
            val firstName: String,
            val invoiceCC: List<String>,
            val paymentMethodId: String,
            val phone: String,
            val state: String,
            val status: Int,
            val subscriptionId: String,
            val subscriptionSettingsId: String,
            val updatedAt: String,
            val zipCode: String
        ) {
            data class BillingDetail(
                val billingCycle: Int,
                val billingEndDate: String,
                val billingStartDate: String,
                val countOfActiveUsers: Int,
                val estimatedBill: Double,
                val estimatedQuantity: Int,
                val lastDate: String,
                val price: Double
            )

            data class CardDetail(
                val cardHolder: String,
                val expire: String,
                val lastDigit: Int
            )
        }
    }
}
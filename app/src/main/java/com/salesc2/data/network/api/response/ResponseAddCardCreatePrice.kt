package com.salesc2.data.network.api.response

data class ResponseAddCardCreatePrice(
    val `data`: Data,
    val status: Int
) {
    data class Data(
        val active: Boolean,
        val billing_scheme: String,
        val created: Int,
        val currency: String,
        val id: String,
        val livemode: Boolean,
        val lookup_key: Any,
        val metadata: Metadata,
        val nickname: Any,
        val `object`: String,
        val product: String,
        val recurring: Recurring,
        val tax_behavior: String,
        val tiers_mode: Any,
        val transform_quantity: Any,
        val type: String,
        val unit_amount: Int,
        val unit_amount_decimal: String
    ) {
        class Metadata

        data class Recurring(
            val aggregate_usage: String,
            val interval: String,
            val interval_count: Int,
            val trial_period_days: Any,
            val usage_type: String
        )
    }
}
package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestMorePage
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.api.response.ResponseMorePage
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.EmployeeLoginRepository
import com.salesc2.data.repository.MorePageRepository

import kotlinx.coroutines.launch

class MorePageViewModel(
    private val repository: MorePageRepository,
    val context: Context
) : ViewModel() {
     fun reqLogOut(
         requestMorePage: RequestMorePage,
         onSuccess: OnSuccess<ResponseMorePage>,
         onFailure: OnFailure<ResponseMorePage>
    )
     {
         viewModelScope.launch {
             repository.reqLogOut(requestMorePage,onSuccess,onFailure)
         }
     }

}
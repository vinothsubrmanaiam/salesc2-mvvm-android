package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.response.ResponseNotificationSettings
import com.salesc2.data.network.api.response.ResponseTeamList
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.NotificationSettingsRepository
import com.salesc2.data.repository.TeamListRepository
import kotlinx.coroutines.launch

class NotificationSettingsViewModel(
    private val notificationSettingsRepository: NotificationSettingsRepository,
    val context: Context
) : ViewModel() {

    fun reqNotificationSettings(
        onSuccess: OnSuccess<ResponseNotificationSettings>,
        onFailure: OnFailure<ResponseNotificationSettings>

    ){
        viewModelScope.launch {
            notificationSettingsRepository.reqNotificationSettings(onSuccess,onFailure)
        }
    }

}
package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseManageAccount
import com.salesc2.data.network.api.response.ResponseTeamList
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.ManageAccountRepository
import com.salesc2.data.repository.TeamListRepository

import kotlinx.coroutines.launch

class ManageAccountViewModel(
    private val teamListRepository: ManageAccountRepository,
    val context: Context
) : ViewModel() {

fun reqGetAccountDetails(

    onSuccess: OnSuccess<ResponseManageAccount>,
    onFailure: OnFailure<ResponseManageAccount>

){
    viewModelScope.launch {
        teamListRepository.reqTeamList(onSuccess,onFailure)
    }
}

}
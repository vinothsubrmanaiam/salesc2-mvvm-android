package com.salesc2.fragment

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.service.CustomTimePickerDialog
import com.salesc2.service.CustomTimePickerDialogNew
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*

class SelectEventDateTimeFragment:Fragment() {
    lateinit var mView: View
    lateinit var start_date : TextView
    lateinit var start_time : TextView
    lateinit var dt_tomorrow : TextView
    lateinit var dt_nextday1 : TextView
    lateinit var dt_nextday2 : TextView
    lateinit var dt_nextday3 : TextView

    lateinit var end_date : TextView
    lateinit var end_time : TextView
    lateinit var dt_30min : TextView
    lateinit var dt_1hr : TextView
    lateinit var dt_2hr : TextView
    lateinit var dt_3hr : TextView
    lateinit var dt_4hr : TextView

    lateinit var dt_add : Button
    private lateinit var close : ImageView

    private lateinit var goback : ImageView
    val myCalendar = Calendar.getInstance()
    private var setMyCal = Calendar.getInstance()
    private var setMyCalEnd = Calendar.getInstance()

    var dt : DateTimeFormatter? = null
    var now: LocalDateTime? = null
    private lateinit var dt_title_toolbar : TextView

    var stEventDate = String()
    var edEventDate = String()
    var stEventTime = String()
    var edEventTime = String()
    private var edit_event = String()

    private var eType_type= String()

    private var set_timezone = String()

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_select_date_time, container, false)
        start_date = mView.findViewById(R.id.start_date_tv)
        start_time = mView.findViewById(R.id.start_time)
        dt_tomorrow = mView.findViewById(R.id.dt_tomorrow)
        dt_nextday1 = mView.findViewById(R.id.dt_nextday1)
        dt_nextday2 = mView.findViewById(R.id.dt_nextday2)
        dt_nextday3 = mView.findViewById(R.id.dt_nextday3)

        end_date = mView.findViewById(R.id.end_date_tv)
        end_time = mView.findViewById(R.id.end_time)
        dt_30min = mView.findViewById(R.id.dt_30min)
        dt_1hr  = mView.findViewById(R.id.dt_1hr)
        dt_2hr  = mView.findViewById(R.id.dt_2hr)
        dt_3hr  = mView.findViewById(R.id.dt_3hr)
        dt_4hr  = mView.findViewById(R.id.dt_4hr)

        goback = mView.findViewById(R.id.select_dt_back_arrow)
        dt_add = mView.findViewById(R.id.dt_add)

        close = mView.findViewById(R.id.close_iv)
        dt_title_toolbar = mView.findViewById(R.id.dt_title_toolbar)
//        dt_title_toolbar.setText("Add Event")
        eType_type = context?.getSharedPref("eType_type",context!!).toString()
        stEventDate = context?.getSharedPref("event_start_date",context!!).toString()
        stEventTime = context?.getSharedPref("event_start_time",context!!).toString()
        edEventDate = context?.getSharedPref("event_end_date",context!!).toString()
        edEventTime = context?.getSharedPref("event_end_time",context!!).toString()

        set_timezone = context?.getSharedPref("set_timezone",context!!).toString()
        edit_event = context?.getSharedPref("edit_event",context!!).toString()

        if (eType_type == "1"){
            if (edit_event == "yes"){
                dt_title_toolbar.text = "Edit work event"
            }else{
                dt_title_toolbar.text = "Add work event"
            }

        }else{
            if (edit_event == "yes"){
                dt_title_toolbar.text = "Edit personal event"
            }else{
                dt_title_toolbar.text = "Add personal event"
            }

        }
        try {
            val dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy")
            now = if (set_timezone != "null"){
                LocalDateTime.now(ZoneId.of(set_timezone))
            }else{
                LocalDateTime.now()
            }
            start_date.text = dtf.format(now)
            end_date.text = start_date.text.toString()
/*
             dt = DateTimeFormatter.ofPattern("hh:mm a")
            start_time.text = dt?.format(now)
            end_time.text =dt?.format(now?.plusMinutes(60))*/

            val whateverDateYouWant = Date()
            val calendar = Calendar.getInstance()
            calendar.time = whateverDateYouWant

            val unroundedMinutes = calendar[Calendar.MINUTE]
            val mod = unroundedMinutes % 15
            calendar.add(Calendar.MINUTE, if (mod < 1) -mod else 15 - mod)
            val sdf = SimpleDateFormat("hh:mm a")
            val test = sdf.format(calendar.getTime())

            start_time.setText(test)
            var myFormatTime = SimpleDateFormat("hh:mm a")
            var strToDate = myFormatTime.parse(start_time.text.toString())
            var addMin = addMinutesToDate(strToDate, 60)
            var timeF = myFormatTime.format(addMin)
            end_time.setText(timeF)
        }catch (e:java.lang.Exception){

        }


        goBackAction()
        dtAction()
        closeAction()
        getCurrentAndMyTime()
        getStAndEdTime()
        if (stEventTime != "null"){
            start_time.text = stEventTime
        }
        if (edEventTime != "null"){
            end_time.text = edEventTime
        }
        if (edEventDate != "null"){
            end_date.text = edEventDate
        }
        if (stEventDate != "null"){
            start_date.text = stEventDate
        }
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private fun closeAction(){
        close.setOnClickListener{
            startActivity(Intent(context, MainActivity::class.java))
        }
    }
    private fun goBackAction(){
        goback.setOnClickListener(View.OnClickListener {
            fragmentManager?.popBackStack()
//            if (eType_type == "0"){
//                context?.loadFragment(context!!,SelectEventTypeFragment())
//            }else{
//                context?.loadFragment(context!!,SelectEventLocationFragment())
//            }
        })
    }

    fun addMinutesToDate(date: Date?, minutes: Int): Date? {
        val calendar = Calendar.getInstance()
        calendar.time = date
        calendar.add(Calendar.MINUTE, minutes)
        return calendar.time
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun dtAction(){
        start_date.setOnClickListener(View.OnClickListener {
            normalBg()
            var stYr = String()
            var stMonth = String()
            var stDay = String()
            try {
                val parser = SimpleDateFormat("MM/dd/yyyy")
                var parseSt = parser.parse(start_date.text.toString())
                var getYrFormat = SimpleDateFormat("yyyy")
                var getMonthFormat = SimpleDateFormat("MM")
                var getDayFormat = SimpleDateFormat("dd")

                stYr = getYrFormat.format(parseSt)
                stMonth = getMonthFormat.format(parseSt)
                stDay = getDayFormat.format(parseSt)
            }catch (e: Exception){

            }
            var date =
                DatePickerDialog.OnDateSetListener { date_picker, year, monthOfYear, dayOfMonth ->
                    setMyCal[Calendar.YEAR] = year
                    setMyCal[Calendar.MONTH] = monthOfYear
                    setMyCal[Calendar.DAY_OF_MONTH] = dayOfMonth
                    updateLabel()
                }
            var datePickerDialog: DatePickerDialog? = context?.let { it1 -> DatePickerDialog(it1, date,stYr.toInt(),stMonth.toInt()-1,stDay.toInt()) } //date is dateSetListener as per your code in question
            datePickerDialog?.updateDate(stYr.toInt(),stMonth.toInt()-1,stDay.toInt())
            datePickerDialog?.show()
        })
        end_date.setOnClickListener{
            var edYr = String()
            var edMonth = String()
            var edDay = String()
            try {
                val parserEnd = SimpleDateFormat("MM/dd/yyyy")
                var parseEnd = parserEnd.parse(end_date.text.toString())
                var getYrFormatEnd = SimpleDateFormat("yyyy")
                var getMonthFormatEnd = SimpleDateFormat("MM")
                var getDayFormatEnd = SimpleDateFormat("dd")

                edYr = getYrFormatEnd.format(parseEnd)
                edMonth = getMonthFormatEnd.format(parseEnd)
                edDay = getDayFormatEnd.format(parseEnd)
            }catch (e:Exception){

            }
            var dateEnd =
                DatePickerDialog.OnDateSetListener { date_pickerEnd, year, monthOfYear, dayOfMonth ->
                    setMyCalEnd[Calendar.YEAR] = year
                    setMyCalEnd[Calendar.MONTH] = monthOfYear
                    setMyCalEnd[Calendar.DAY_OF_MONTH] = dayOfMonth
                    updateLabelEndDate()
                }

            var datePickerDialogEnd: DatePickerDialog? = context?.let { it1 -> DatePickerDialog(it1, dateEnd,edYr.toInt(),edMonth.toInt()-1,edDay.toInt())} //date is dateSetListener as per your code in question
            datePickerDialogEnd?.updateDate(edYr.toInt(),edMonth.toInt()-1,edDay.toInt())

            datePickerDialogEnd?.show()
        }

        myCalendar.add(Calendar.DAY_OF_YEAR,1)
        var tomro : Date = myCalendar.time //In which you need put here yyyy-MM-dd
        sdf = SimpleDateFormat(myFormat, Locale.US)
        val dateFormat: DateFormat = SimpleDateFormat("MM/dd/yyyy")
        dt_tomorrow.setOnClickListener{
            tomroBg()
            start_date.text = dateFormat.format(tomro)
            end_date.text = start_date.text.toString()
        }

        myCalendar.add(Calendar.DAY_OF_YEAR,1)
        var nextday1 : Date = myCalendar.time
        dt_nextday1.text = getDayString(nextday1, Locale.US)
        dt_nextday1.setOnClickListener{
            nextDay1Bg()
            start_date.text = dateFormat.format(nextday1)
            end_date.text = start_date.text.toString()
        }

        myCalendar.add(Calendar.DAY_OF_YEAR,1)
        var nextday2 : Date = myCalendar.time
        dt_nextday2.text = getDayString(nextday2, Locale.US)

        dt_nextday2.setOnClickListener{
            nextDay2Bg()
//            myCalendar.add(Calendar.DAY_OF_YEAR,3)
            start_date.text = dateFormat.format(nextday2)
            end_date.text = start_date.text.toString()
        }
        myCalendar.add(Calendar.DAY_OF_YEAR,1)
        var nextday3 : Date = myCalendar.time
        dt_nextday3.text = getDayString(nextday3, Locale.US)
        dt_nextday3.setOnClickListener{
            nextDay3Bg()
//            myCalendar.add(Calendar.DAY_OF_YEAR,4)
            start_date.text = dateFormat.format(nextday3)
            end_date.text = start_date.text.toString()
        }




        //time
        var myFormatTime = SimpleDateFormat("hh:mm a")


        start_time.setOnClickListener{
            var mTimePicker:CustomTimePickerDialogNew? = null

            var strToDate = myFormatTime.parse(start_time.text.toString())
            var getHrF = SimpleDateFormat("HH")
            var getMnF = SimpleDateFormat("mm")
            var hr = getHrF.format(strToDate)
            var min = getMnF.format(strToDate)
            mTimePicker?.updateTime(hr.toInt(),min.toInt())
            mTimePicker = CustomTimePickerDialogNew(context!!, object: TimePickerDialog.OnTimeSetListener {
                override fun onTimeSet(timePicker:TimePicker, selectedHour:Int, selectedMinute:Int) {
                    mTimePicker?.updateTime(hr.toInt(),min.toInt())
                    var parser = SimpleDateFormat("HH:mm")
                    var parseTime = parser.parse("$selectedHour" + ":" + "$selectedMinute")

                    var startTime = myFormatTime.format(parseTime)
                    start_time.setText(startTime)
                    var strToDate = myFormatTime.parse(start_time.text.toString())
                    var addMin = addMinutesToDate(strToDate,60)
                    var endTimeF = myFormatTime.format(addMin)
                    end_time.setText(endTimeF)
                }
            }, hr.toInt(), min.toInt(), false)
            mTimePicker.setTitle("Select Time")
            mTimePicker?.updateTime(hr.toInt(),min.toInt())
            mTimePicker.show()
        }


        end_time.setOnClickListener{
            var mTimePickerEnd: CustomTimePickerDialogNew? = null
            var strToDate = myFormatTime.parse(end_time.text.toString())
            var getHrF = SimpleDateFormat("HH")
            var getMnF = SimpleDateFormat("mm")
            var hrEnd = getHrF.format(strToDate)
            var minEnd = getMnF.format(strToDate)
            val mcurrentTimeEnd = Calendar.getInstance()
            val hourEnd = mcurrentTimeEnd.get(Calendar.HOUR_OF_DAY)
            val minuteEnd = mcurrentTimeEnd.get(Calendar.MINUTE)

            mTimePickerEnd?.updateTime(hrEnd.toInt(),minEnd.toInt())
            mTimePickerEnd = CustomTimePickerDialogNew(context!!, object: TimePickerDialog.OnTimeSetListener {
                override fun onTimeSet(timePicker:TimePicker, selectedHour:Int, selectedMinute:Int) {
                    mTimePickerEnd?.updateTime(hrEnd.toInt(),minEnd.toInt())
                    var parser = SimpleDateFormat("HH:mm")
                    var parseTime = parser.parse("$selectedHour" + ":" + "$selectedMinute")

                    var endTime = myFormatTime.format(parseTime)
                    end_time.setText(endTime)
                }
            }, hrEnd.toInt(), minEnd.toInt(), false)
            mTimePickerEnd.setTitle("Select Time")
            mTimePickerEnd?.updateTime(hrEnd.toInt(),minEnd.toInt())
            mTimePickerEnd.show()
        }


        dt_30min.setOnClickListener{
            dt30minBg()
            var strToDate = myFormatTime.parse(start_time.text.toString())
            var addMin = addMinutesToDate(strToDate,30)
            var timeF = myFormatTime.format(addMin)
            end_time.text = timeF
        }

        dt_1hr.setOnClickListener{
            dt1hrBg()
            var strToDate = myFormatTime.parse(start_time.text.toString())
            var addMin = addMinutesToDate(strToDate,60)
            var timeF = myFormatTime.format(addMin)
            end_time.text = timeF
//            end_time.text =dt?.format(now?.plusMinutes(60))
        }
        dt_2hr.setOnClickListener{
            dt2hrBg()
            var strToDate = myFormatTime.parse(start_time.text.toString())
            var addMin = addMinutesToDate(strToDate,120)
            var timeF = myFormatTime.format(addMin)
            end_time.text = timeF
        }

        dt_3hr.setOnClickListener{
            dt3hrBg()
            var strToDate = myFormatTime.parse(start_time.text.toString())
            var addMin = addMinutesToDate(strToDate,180)
            var timeF = myFormatTime.format(addMin)
            end_time.text = timeF
        }

        dt_4hr.setOnClickListener{
            dt4hrBg()
            var strToDate = myFormatTime.parse(start_time.text.toString())
            var addMin = addMinutesToDate(strToDate,240)
            var timeF = myFormatTime.format(addMin)
            end_time.text = timeF
        }



        dt_add.setOnClickListener{
            getCurrentAndMyTime()
            getStAndEdTime()
            context?.setSharedPref(context!!,"event_dt_edited","edit_eDate")
            context?.setSharedPref(context!!,"event_start_date",start_date.text.toString())
            context?.setSharedPref(context!!,"event_start_time", start_time.text.toString())
            context?.setSharedPref(context!!,"event_end_time",end_time.text.toString())
            context?.setSharedPref(context!!,"event_end_date",end_date.text.toString())
            compareDateWithCurrentDate(currentDate,myDate)
        }

    }
    var myFormat = String()
    var sdf = SimpleDateFormat()
    private fun updateLabel(){
        myFormat = "MM/dd/yyyy" //In which you need put here yyyy-MM-dd
        sdf = SimpleDateFormat(myFormat, Locale.ENGLISH)
            start_date.text = sdf.format(setMyCal.time)
            end_date.text = start_date.text.toString()

    }
    private fun updateLabelEndDate(){
        myFormat = "MM/dd/yyyy" //In which you need put here yyyy-MM-dd
        sdf = SimpleDateFormat(myFormat, Locale.ENGLISH)
            end_date.text = sdf.format(setMyCalEnd.time)
    }

    private fun getDayString(date: Date?, locale: Locale?): String? {
        val formatter: DateFormat = SimpleDateFormat("EEEE", locale)
        return formatter.format(date)
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }


    private fun tomroBg(){
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            dt_tomorrow.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
            dt_tomorrow.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_nextday1.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_nextday1.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_nextday2.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_nextday2.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_nextday3.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_nextday3.setBackgroundDrawable(it)
        }
    }

    private fun nextDay1Bg(){
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_tomorrow.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_tomorrow.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            dt_nextday1.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
            dt_nextday1.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_nextday2.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_nextday2.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_nextday3.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_nextday3.setBackgroundDrawable(it)
        }
    }

    private fun nextDay2Bg(){
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_tomorrow.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_tomorrow.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            dt_nextday2.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
            dt_nextday2.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_nextday1.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_nextday1.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_nextday3.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_nextday3.setBackgroundDrawable(it)
        }
    }
    private fun nextDay3Bg(){
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_tomorrow.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_tomorrow.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            dt_nextday3.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
            dt_nextday3.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_nextday2.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_nextday2.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_nextday1.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_nextday1.setBackgroundDrawable(it)
        }
    }


    private fun dt30minBg(){
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_1hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_1hr.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            dt_30min.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
            dt_30min.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_2hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_2hr.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_3hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_3hr.setBackgroundDrawable(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_4hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_4hr.setBackgroundDrawable(it)
        }
    }

    private fun dt1hrBg(){
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_30min.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_30min.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            dt_1hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
            dt_1hr.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_2hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_2hr.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_3hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_3hr.setBackgroundDrawable(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_4hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_4hr.setBackgroundDrawable(it)
        }
    }

    private fun dt2hrBg(){
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_30min.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_30min.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            dt_2hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
            dt_2hr.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_1hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_1hr.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_3hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_3hr.setBackgroundDrawable(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_4hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_4hr.setBackgroundDrawable(it)
        }
    }

    private fun dt3hrBg(){
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_30min.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_30min.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            dt_3hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
            dt_3hr.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_2hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_2hr.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_1hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_1hr.setBackgroundDrawable(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_4hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_4hr.setBackgroundDrawable(it)
        }
    }

    private fun dt4hrBg(){
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_30min.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_30min.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            dt_4hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
            dt_4hr.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_2hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_2hr.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_3hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_3hr.setBackgroundDrawable(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_1hr.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_1hr.setBackgroundDrawable(it)
        }
    }



    private fun normalBg(){
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_tomorrow.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_tomorrow.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_nextday1.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_nextday1.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_nextday2.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_nextday2.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.mid_blue) }?.let {
            dt_nextday3.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_transparent_blue_bg) }?.let {
            dt_nextday3.setBackgroundDrawable(it)
        }
    }



    private var currentDate = Date()
    private var myDate = Date()
    private fun getCurrentAndMyTime(){
        try {
            var parser = SimpleDateFormat("MM/dd/yyyy hh:mm a")
            val parseDate = parser.parse("${start_date.text.toString()} ${start_time.text.toString()}")

            val myFormat = "dd/MM/yyyy hh:mm a"
            val sdf = SimpleDateFormat(myFormat)
            val stDateFormat = sdf.format(parseDate)

            myDate = stDateFormat.toDate()

            var now = if (set_timezone != "null"){
                LocalDateTime.now(ZoneId.of(set_timezone))
            }else{
                LocalDateTime.now()
            }
            var todayDtFormatNew: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm a")
            currentDate = todayDtFormatNew.format(now).toDate()


        }catch (e: java.lang.Exception){

        }
    }


    private fun compareDateWithCurrentDate(currentDate: Date, myDate: Date?) {
        getStAndEdTime()
        if (currentDate.after(myDate)) {

            context?.toast("Invalid date and time")
        }

        //before() will return true if and only if date1 is before date2
        if (currentDate.before(myDate)) {
             compareDateWithEndDate(tStDate,tEdDate)

        }

        //equals() returns true if both the dates are equal
        if (currentDate.equals(myDate)) {
             compareDateWithEndDate(tStDate,tEdDate)
        }

    }


    private var tStDate = Date()
    private var tEdDate = Date()
    private fun getStAndEdTime(){
        try {
            var parser = SimpleDateFormat("MM/dd/yyyy hh:mm a")
            val parseDateSt = parser.parse("${start_date.text.toString()} ${start_time.text.toString()}")
            val myFormat = "dd/MM/yyyy hh:mm a"
            val sdf = SimpleDateFormat(myFormat)
            val stDateFormat = sdf.format(parseDateSt)

            tStDate = stDateFormat.toDate()

            var parseDateEd = parser.parse("${end_date.text.toString()} ${end_time.text.toString()}")

            var edDateFormat = sdf.format(parseDateEd)
            tEdDate = edDateFormat.toDate()

        }catch (e: java.lang.Exception){

        }
    }


    private fun compareDateWithEndDate(currentDate: Date, myDate: Date?) {
        if (currentDate.after(myDate)) {
      context?.toast("Invalid end date and time")
        }

        //before() will return true if and only if date1 is before date2
        if (currentDate.before(myDate)) {

            var totalDuration = startEndTimeDurationCheck("${start_date.text.toString()} ${start_time.text.toString()}","${end_date.text.toString()} ${end_time.text.toString()}")
            if (totalDuration!! > 24){
                context?.toast("Selected duration is more than 24 hours, Please select valid duration.")
            }else{
                context?.loadFragment(context!!,AddEventFragment())
            }

        }

        //equals() returns true if both the dates are equal
        if (currentDate.equals(myDate)) {

            var totalDuration = startEndTimeDurationCheck("${start_date.text.toString()} ${start_time.text.toString()}","${end_date.text.toString()} ${end_time.text.toString()}")
            if (totalDuration!! > 24){
                context?.toast("Selected duration is more than 24 hours, Please select valid duration.")
            }else{
                context?.loadFragment(context!!,AddEventFragment())
            }
        }

    }


    var hours: Long? = null
    private fun startEndTimeDurationCheck(start: String,end:String) : Long?{
        try {
            var parser = SimpleDateFormat("MM/dd/yyyy hh:mm a")
            val time_parse: Date = parser.parse(start)

            var parserEnd = SimpleDateFormat("MM/dd/yyyy hh:mm a")
            val time_parseEnd: Date = parserEnd.parse(end)


            val mills: Long = time_parse.time - time_parseEnd.time
            hours = mills / (1000 * 60 * 60)
            hours = if (hours!! < 0) -hours!! else hours
            var min = (mills / (1000 * 60) % 60).toInt()
            min = if (min < 0) -min else min


        }catch (e:Exception){

        }
        return hours
    }


}
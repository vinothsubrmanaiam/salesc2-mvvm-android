package com.salesc2.adapters

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.github.ivbaranov.mli.MaterialLetterIcon
import com.salesc2.R
import com.salesc2.fragment.TeamMembersDetailsFragment
import com.salesc2.model.AccountlistModel
import com.salesc2.model.TeamMemberListModel
import com.salesc2.utils.ShowNoData
import com.salesc2.utils.capFirstLetter
import com.squareup.picasso.Picasso
import me.thanel.swipeactionview.SwipeActionView
import me.thanel.swipeactionview.SwipeDirection
import me.thanel.swipeactionview.SwipeGestureListener
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.ArrayList


class TeamMembersListAdapter(context: Context?,val showdata: ShowNoData, var teamItems: ArrayList<TeamMemberListModel>) :
    RecyclerView.Adapter<TeamMembersListAdapter.viewHolder>() ,Filterable{

     var teamMembersItems = ArrayList<TeamMemberListModel>()
    var context: Context? = null

    init {
        this.context = context
        teamMembersItems = teamItems

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_team_members_list, parent, false)
        return viewHolder(view)
    }



    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        var teamMemberListModel: TeamMemberListModel = teamMembersItems.get(position)
        holder.e_name.text = context?.capFirstLetter(teamMemberListModel?.e_name)
        if (teamMemberListModel.e_role.equals("Sales Rep",ignoreCase = true)){
            holder.role.text = context?.capFirstLetter("Representative")
        }else if (teamMemberListModel.e_role.equals("Team Lead",ignoreCase = true)){
            holder.role.text = context?.capFirstLetter("Regional Manager")
        }else{
            holder.role.text = context?.capFirstLetter(teamMemberListModel?.e_role)
        }

        holder.level_type.text =  context?.capFirstLetter(teamMemberListModel?.e_level_type)
        holder.level_name.text = context?.capFirstLetter(teamMemberListModel?.e_level_name)


        val initials = teamMemberListModel.e_name
            .split(' ')
            .mapNotNull { it.firstOrNull()?.toString() }
            .reduce { acc, s -> acc + s }



if (teamMemberListModel.e_profilePath != ""){
    holder.letterImage.visibility = View.GONE
    Picasso.get().load(teamMemberListModel.e_profilePath).placeholder(R.drawable.ic_dummy_user_pic)
        .into(holder.e_image)
}else{
    holder.letterImage.visibility = View.VISIBLE
    holder.letterImage.letter = initials
}



//        holder.call_layout.visibility = View.VISIBLE
//        holder.message_layout.visibility = View.VISIBLE
//        holder.email_layout.visibility = View.VISIBLE
//        swipeActionView.animateToOriginalPosition(10000)


        holder.swipeView_team.swipeGestureListener = object : SwipeGestureListener {
            override fun onSwipedLeft(swipeActionView: SwipeActionView): Boolean {
                holder.call_layout.visibility = View.VISIBLE
                holder.message_layout.visibility = View.VISIBLE
                holder.email_layout.visibility = View.VISIBLE
                holder.swipeView_team.animateInDirection(SwipeDirection.Left, false)
                return false
            }

            override fun onSwipedRight(swipeActionView: SwipeActionView): Boolean {
                holder.call_layout.visibility = View.VISIBLE
                holder.message_layout.visibility = View.VISIBLE
                holder.email_layout.visibility = View.VISIBLE
                holder.swipeView_team.animateInDirection(SwipeDirection.Right, false)
                return false
            }

        }


  holder.team_details_layout.setOnClickListener{
      holder.swiping_layout.performClick()
  }
holder.e_name.setOnClickListener{
    holder.swiping_layout.performClick()
}
        holder.role.setOnClickListener{
            holder.swiping_layout.performClick()
        }
        holder.level_name.setOnClickListener{
            holder.swiping_layout.performClick()
        }
        holder.level_type.setOnClickListener{
            holder.swiping_layout.performClick()
        }

        holder.swiping_layout.setOnClickListener {
            holder.call_layout.visibility = View.GONE
            holder.message_layout.visibility = View.GONE
            holder.email_layout.visibility = View.GONE
            var bundle = Bundle()
            var fragment = TeamMembersDetailsFragment()
            val activity = context as AppCompatActivity
            bundle.putString("id", teamMemberListModel.e_id)
            fragment.arguments = bundle
            activity.supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragmet_container, fragment).addToBackStack(null).commit()
        }


        holder.call_text.setOnClickListener{
            try {

                var callIntent: Intent =
                    Intent(Intent.ACTION_DIAL, Uri.parse("tel: ${teamMemberListModel.e_phone}"))
                context?.startActivity(callIntent)


            } catch (activityException: ActivityNotFoundException) {
                Log.e("Calling a Phone Number", "Call failed", activityException)
            }catch (e:Exception){

            }
        }
        holder.call_layout.setOnClickListener(View.OnClickListener {

            try {

                var callIntent: Intent =
                    Intent(Intent.ACTION_DIAL, Uri.parse("tel: ${teamMemberListModel.e_phone}"))
                context?.startActivity(callIntent)


            } catch (activityException: ActivityNotFoundException) {
                Log.e("Calling a Phone Number", "Call failed", activityException)
            }
            catch (e:Exception){

            }

        })

        holder.message_text.setOnClickListener{
            try {
                var smsIntent: Intent =
                    Intent(Intent.ACTION_VIEW, Uri.parse("sms:${teamMemberListModel.e_phone}"))
                context?.startActivity(smsIntent)
            }catch (e:Exception){
                print(e)
            }
        }
        holder.message_layout.setOnClickListener(View.OnClickListener {
            try {
                var smsIntent: Intent =
                    Intent(Intent.ACTION_VIEW, Uri.parse("sms:${teamMemberListModel.e_phone}"))
                context?.startActivity(smsIntent)
            }catch (e:Exception){
                print(e)
            }
        })

        holder.email_text.setOnClickListener{
            val TO = arrayOf(teamMemberListModel?.e_email.toString())
            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.data = Uri.parse("mailto:")
            emailIntent.type = "text/plain"
            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
            emailIntent.setPackage("com.google.android.gm")


            try {
                context?.startActivity(Intent.createChooser(emailIntent, "Send mail..."))
            } catch (ex: ActivityNotFoundException) {
            }catch (e:Exception){

            }

        }

        holder.email_layout.setOnClickListener(View.OnClickListener {


            val TO = arrayOf(teamMemberListModel?.e_email.toString())
            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.data = Uri.parse("mailto:")
            emailIntent.type = "text/plain"
            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
            emailIntent.setPackage("com.google.android.gm")


            try {
                context?.startActivity(Intent.createChooser(emailIntent, "Send mail..."))
            } catch (ex: ActivityNotFoundException) {
            }catch (e:Exception){

            }

        })


    }


    class viewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var e_name: TextView = itemView.findViewById(R.id.team_member_name)
        var role: TextView = itemView.findViewById(R.id.team_member_job_tv)
        var level_name: TextView = itemView.findViewById(R.id.team_member_country_tv)
        var level_type: TextView = itemView.findViewById(R.id.region_territory_tv)
        var e_image: ImageView = itemView.findViewById(R.id.team_member_iv)

        var swipeView_team: me.thanel.swipeactionview.SwipeActionView =
            itemView.findViewById(R.id.swipe_view_team_member_list)
        var email_layout: ConstraintLayout = itemView.findViewById(R.id.email_layout)
        var message_layout: ConstraintLayout = itemView.findViewById(R.id.message_layout)
        var call_layout: ConstraintLayout = itemView.findViewById(R.id.call_layout)

        var swiping_layout: ConstraintLayout = itemView.findViewById(R.id.team_swiping_layout)
        var team_details_layout : ConstraintLayout = itemView.findViewById(R.id.team_details_layout)
        var letterImage : MaterialLetterIcon = itemView.findViewById(R.id.letterImage)

        var email_text : TextView = itemView.findViewById(R.id.email_text)
        var message_text : TextView = itemView.findViewById(R.id.message_text)
        var call_text : TextView = itemView.findViewById(R.id.call_text)


    }
    override fun getItemCount(): Int {
        return teamMembersItems.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {

                    teamMembersItems = teamItems
                } else {
                    val resultList = java.util.ArrayList<TeamMemberListModel>()
                    for (row in teamItems) {
                        if (row.e_name.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))) {
                            showdata.hideNoData()
                            resultList.add(row)
                        }
                    }
                    teamMembersItems = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = teamMembersItems
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                teamMembersItems = results?.values as java.util.ArrayList<TeamMemberListModel>




                if (teamMembersItems.size==0) {
                    showdata.showNoData()

                }
                else
                {
                    showdata.hideNoData()
                    notifyDataSetChanged()

                }

            }

        }
    }

    }




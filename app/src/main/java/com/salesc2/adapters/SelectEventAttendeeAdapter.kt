package com.salesc2.adapters

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.fragment.AddEventFragment
import com.salesc2.model.AddAttendeesModel
import com.salesc2.model.AddTaskAssigneeModel
import com.salesc2.service.DatabaseHandler
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.setSharedPref
import com.salesc2.utils.toast
import com.squareup.picasso.Picasso


class SelectEventAttendeeAdapter() : RecyclerView.Adapter<SelectEventAttendeeAdapter.viewHolder>() {
    val selectedUserList = ArrayList<String>()//Creating an empty arraylist

    private lateinit var taskAssigneeItems: ArrayList<AddTaskAssigneeModel>
    var context: Context? = null
    var row_index = -1
    var eType_type = String()
    var checked: Boolean = true
    private var selected_eAttendee_position = String()
    var selectedEventIds = ArrayList<String>()
    var selectedListAttendee = String()
    var attendeeID = ArrayList<String>()
    private var userId = String()

    constructor(context: Context?, taskAssigneeItems: ArrayList<AddTaskAssigneeModel>) : this() {
        this.context = context
        this.taskAssigneeItems = taskAssigneeItems

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_add_task_assignee, parent, false)
        eType_type = context?.getSharedPref("eType_type", context!!).toString()
        userId = context?.getSharedPref("userId", context!!).toString()
        selected_eAttendee_position =
            context?.getSharedPref("selected_eAttendee_position", context!!).toString()
        selectedListAttendee =
            context?.getSharedPref("event_attendee_list_selected", context!!).toString()
        viewRecord()
        return viewHolder(view)
    }

    override fun getItemCount(): Int {
        return taskAssigneeItems.size
    }


    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        var addTaskAssigneeModel: AddTaskAssigneeModel = taskAssigneeItems[position]
        viewRecord()
        selected_eAttendee_position =
            context?.getSharedPref("selected_eAttendee_position", context!!).toString()
        selectedUserList.add(position.toString())
        if (addTaskAssigneeModel.assignee_id != "null") {
            holder.taskAssigneeName.text = addTaskAssigneeModel.assignee_name

        }
        try {
            Picasso.get().load(addTaskAssigneeModel.assignee_image)
                .placeholder(R.drawable.ic_dummy_user_pic).into(holder.taskAssigneeImage)
        } catch (e: Exception) {
            context?.toast(e.toString())
        }





        try {
            if (eType_type == "0") {
                if (selected_eAttendee_position != "null") {
                    row_index = selected_eAttendee_position.toInt()
                    if (row_index.equals(selected_eAttendee_position)) {

                    }
                }
            }

        } catch (e: Exception) {
            print(e)
        }


        var databaseHandler: DatabaseHandler? = null
        //calling the viewEmployee method of DatabaseHandler class to read the records
        var attendee: List<AddAttendeesModel>? = null

        databaseHandler = DatabaseHandler(context!!)
        attendee = databaseHandler?.viewAttendees()
        if (attendee!!.size!=0) {
            for (a in attendee!!) {
                if (addTaskAssigneeModel.assignee_name.equals(a.at_name)) {
                    holder.select_icon.visibility = View.VISIBLE
                    context?.setSharedPref(
                        context!!,
                        "event_attendee_list_selected",
                        a.at_id.toString().replace("[", "").replace("]", "")
                    )
                }

            }
        }



        selectedListAttendee =
            context?.getSharedPref("event_attendee_list_selected", context!!).toString()
        holder.itemView.setOnClickListener(View.OnClickListener {
            row_index = position
            notifyDataSetChanged()
            if (eType_type == "0")
            {
                val preferences: SharedPreferences =
                    PreferenceManager.getDefaultSharedPreferences(context)
                val editor: SharedPreferences.Editor = preferences.edit()
                editor.remove("selected_eAttendee_position")
                editor.commit()
                if (row_index == position) {

                    context?.setSharedPref(
                        context!!,
                        "selected_eAttendee_position",
                        position.toString()
                    )
                    context?.setSharedPref(
                        context!!,
                        "eAttendee_name",
                        addTaskAssigneeModel.assignee_name
                    )
                    context?.setSharedPref(
                        context!!,
                        "eAttendee_id",
                        addTaskAssigneeModel.assignee_id
                    )
                    context?.setSharedPref(
                        context!!,
                        "eAttendee_img",
                        addTaskAssigneeModel.assignee_image
                    )
                    holder.select_icon.visibility = View.VISIBLE

                    var fragment = AddEventFragment()

                    val activity = context as AppCompatActivity
                    activity.supportFragmentManager.beginTransaction()
                        .replace(R.id.main_fragmet_container, fragment).addToBackStack(null)
                        .commit()
                } else {
                    holder.select_icon.visibility = View.GONE
                }

            } else {
                if (row_index == position) {
                    selectedListAttendee =
                        context?.getSharedPref("event_attendee_list_selected", context!!).toString()
                    viewRecord()

                    if (selectedListAttendee.contains(addTaskAssigneeModel.assignee_id))
                    {
                        holder.select_icon.visibility = View.GONE
                        selectedEventIds.remove(addTaskAssigneeModel.assignee_id)

                        try {
                            val deleteId = addTaskAssigneeModel.assignee_id
                            val databaseHandler: DatabaseHandler = DatabaseHandler(context!!)
                            if (deleteId.trim() != "") {
                                //calling the deleteEmployee method of DatabaseHandler class to delete record
                                val status = databaseHandler.deleteAttendeeById(
                                    AddAttendeesModel(
                                        addTaskAssigneeModel.assignee_id, "", ""
                                    )
                                )
                                if (status > -1) {
                                }
                            } else {
                                Toast.makeText(
                                    context,
                                    "id or name or email cannot be blank",
                                    Toast.LENGTH_LONG
                                ).show()
                            }
                        } catch (e: Exception) {
                            print(e)
                        }

                        context?.setSharedPref(
                            context!!,
                            "event_attendee_list_selected",
                            selectedEventIds.toString().replace("[", "").replace("]", "")
                        )
                    } else {
                        holder.select_icon.visibility = View.VISIBLE
                        selectedEventIds.add(addTaskAssigneeModel.assignee_id)

                        //save record
                        val a_id = addTaskAssigneeModel.assignee_id
                        val a_name = addTaskAssigneeModel.assignee_name

                        val a_img = addTaskAssigneeModel.assignee_image



                        val databaseHandler: DatabaseHandler = DatabaseHandler(context!!)
                        if (a_name.trim() != "" && a_id.trim() != "") {
                            val status =
                                databaseHandler.addAttendees(AddAttendeesModel(a_id, a_name, a_img))
                            if (status > -1) {
                                var databaseHandler: DatabaseHandler? = null
                                var attendee: List<AddAttendeesModel>? = null
                                var multAttendeeId = String()
                                databaseHandler = DatabaseHandler(context!!)
                                attendee = databaseHandler?.viewAttendees()
                                val a_Id = Array<String>(attendee!!.size) { "" }
                                val a_Name = Array<String>(attendee!!.size) { "null" }
                                val a_img = Array<String>(attendee!!.size) { "null" }

                                var index = 0

                                for (a in attendee!!) {
                                    a_Id[index] = a.at_id
                                    a_Name[index] = a.at_name
                                    a_img[index] = a.at_img
                                    index++
                                    attendeeID.add("${a.at_id}")

                                }


                            }
                        }
                        context?.setSharedPref(
                            context!!,
                            "event_attendee_list_selected",
                            selectedEventIds.toString().replace("[", "").replace("]", "")
                        )
                    }
                }

            }


        })


        if (eType_type == "0") {
            if (row_index == position) {
                context?.setSharedPref(
                    context!!,
                    "selected_eAttendee_position",
                    position.toString()
                )
                context?.setSharedPref(
                    context!!,
                    "eAttendee_name",
                    addTaskAssigneeModel.assignee_name
                )
                context?.setSharedPref(context!!, "eAttendee_id", addTaskAssigneeModel.assignee_id)
                context?.setSharedPref(
                    context!!,
                    "eAttendee_img",
                    addTaskAssigneeModel.assignee_image
                )
                holder.select_icon.visibility = View.VISIBLE
            }


        }
    }


    //creating the instance of DatabaseHandler class
    var databaseHandler: DatabaseHandler? = null

    //calling the viewEmployee method of DatabaseHandler class to read the records
    var attendee: List<AddAttendeesModel>? = null
    var multAttendeeId = String()
    private fun viewRecord() {
        databaseHandler = DatabaseHandler(context!!)
        attendee = databaseHandler?.viewAttendees()
        val a_Id = Array<String>(attendee!!.size) { "" }
        val a_Name = Array<String>(attendee!!.size) { "null" }
        val a_img = Array<String>(attendee!!.size) { "null" }

        var index = 0

        for (a in attendee!!) {
            a_Id[index] = a.at_id
            a_Name[index] = a.at_name
            a_img[index] = a.at_img
            index++

            attendeeID.add("${a.at_id}")


        }

    }

    class viewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var taskAssigneeName: TextView = itemView.findViewById(R.id.task_assignee_name)
        var taskAssigneeImage: ImageView = itemView.findViewById(R.id.task_assignee_image)
        var assignee_layout: ConstraintLayout = itemView.findViewById(R.id.assignee_layout)
        var select_icon: ImageView = itemView.findViewById(R.id.select_icon)
        var select_checkBox: CheckBox = itemView.findViewById(R.id.select_checkBox)
    }
}
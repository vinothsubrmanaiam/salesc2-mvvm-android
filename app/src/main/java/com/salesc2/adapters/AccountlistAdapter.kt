package com.salesc2.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.database.DataSetObserver
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.fragment.AccountDetailsFragment
import com.salesc2.fragment.SelectAccountFragment
import com.salesc2.model.AccountlistModel
import com.salesc2.utils.ShowNoData
import com.salesc2.utils.loadFragment
import com.salesc2.utils.setSharedPref
import com.salesc2.utils.toast
import me.thanel.swipeactionview.SwipeActionView
import me.thanel.swipeactionview.SwipeDirection
import me.thanel.swipeactionview.SwipeGestureListener
import java.util.*


class AccountlistAdapter(context: Context?,val showdata: ShowNoData,  var acclist: ArrayList<AccountlistModel>) :
    RecyclerView.Adapter<AccountlistAdapter.viewHolder>(), Filterable {

    var account_list = ArrayList<AccountlistModel>()
    var context: Context? = null
    var resultCount:Int = 0

    init {
        this.context = context
        account_list = acclist
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.account_list, parent, false)
        return viewHolder(view)
    }


    override fun onBindViewHolder(holder: viewHolder, position: Int) {

        val accountlistModel: AccountlistModel = account_list.get(position)
        holder.accountname_tv?.text = accountlistModel.accountName
        holder.territory_tv?.text = accountlistModel.territory
        holder.swipeView.swipeGestureListener = object : SwipeGestureListener {
            override fun onSwipedLeft(swipeActionView: SwipeActionView): Boolean {
                holder.swipeView.animateInDirection(SwipeDirection.Left, false)
                return false
            }

            override fun onSwipedRight(swipeActionView: SwipeActionView): Boolean {
                holder.swipeView.animateInDirection(SwipeDirection.Right, false)
                return false

            }

        }


        holder.accountname_tv?.setOnClickListener{
            holder.main_layout.performClick()
        }
        holder.territory_tv?.setOnClickListener{
            holder.main_layout.performClick()
        }
        holder.main_layout.setOnClickListener {
            holder.getdirection_layout.visibility = View.GONE
            holder.addtask_layout.visibility = View.GONE

            val bundle = Bundle()
            val fragment = AccountDetailsFragment()
            val activity = context as AppCompatActivity
            bundle.putString("account_id", accountlistModel.id)
            context?.setSharedPref(context!!,"account_id",accountlistModel.id)
            fragment.arguments = bundle
            activity.supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragmet_container, fragment).addToBackStack(null).commit()
        }

        holder.getdirection_iv.setOnClickListener{
            holder.getdirection_layout.performClick()
        }
        holder.getdirection_tv.setOnClickListener{
            holder.getdirection_layout.performClick()
        }
        holder.getdirection_layout.setOnClickListener(View.OnClickListener {
            try {
                var uri: String = "http://maps.google.com/maps?daddr=${accountlistModel.lat},${accountlistModel.long}"
                var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                intent.setPackage("com.google.android.apps.maps")
                context?.startActivity(intent)
            }
            catch (e: Exception){
                print(e)
                context?.toast(e.toString())
            }
        })


        holder.addtask_iv.setOnClickListener{
            context?.loadFragment(context!!,SelectAccountFragment())
        }
        holder.addtask_tv.setOnClickListener{
            context?.loadFragment(context!!,SelectAccountFragment())
        }
        holder.addtask_layout.setOnClickListener{
            context?.loadFragment(context!!,SelectAccountFragment())
        }

    }


    class viewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var accountname_tv: TextView? = itemView.findViewById(R.id.accountname_tv)
        var territory_tv: TextView? = itemView.findViewById(R.id.territory_tv)
        var main_layout: ConstraintLayout = itemView.findViewById(R.id.main_layout)
        var swipeView: me.thanel.swipeactionview.SwipeActionView =
            itemView.findViewById(R.id.swipe_view_account_list)
        var getdirection_layout: ConstraintLayout = itemView.findViewById(R.id.getdirection_layout)
        var addtask_layout: ConstraintLayout = itemView.findViewById(R.id.addtask_layout)
        var getdirection_tv : TextView = itemView.findViewById(R.id.getdirection_tv)
        var getdirection_iv : ImageView = itemView.findViewById(R.id.getdirection_iv)

        var addtask_iv : ImageView = itemView.findViewById(R.id.addtask_iv)
        var addtask_tv : TextView = itemView.findViewById(R.id.addtask_tv)


    }


    override fun getItemCount(): Int {
        return account_list.size
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    account_list = acclist

                } else {
                    val resultList = ArrayList<AccountlistModel>()
                    for (row in acclist) {
                        if (row.accountName.toLowerCase(Locale.ROOT)
                                .contains(charSearch.toLowerCase(Locale.ROOT))
                        ) {

                            resultList.add(row)
                        }

                    }
                    account_list = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = account_list
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                account_list = results?.values as ArrayList<AccountlistModel>

                if (account_list.size==0) {
                    showdata.showNoData()

                }
                else
                {
                    showdata.hideNoData()
                    notifyDataSetChanged()
                    resultCount = results.count.toInt()
                }

            }



        }


    }


}

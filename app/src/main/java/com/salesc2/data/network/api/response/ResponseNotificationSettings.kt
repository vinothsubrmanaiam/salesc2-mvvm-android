package com.salesc2.data.network.api.response

data class ResponseNotificationSettings(
    val status: String,
    val msg: String,
    val `data`: Data
) {
    data class Data(
        val task: ArrayList<Task>,
        val event: List<Event>,
        val allNotification: List<AllNotification>
    ) {
        data class Task(
            val name: String,
            val notification: Boolean,
            val email: Boolean
        )

        data class Event(
            val name: String,
            val notification: Boolean,
            val email: Boolean
        )

        data class AllNotification(
            val name: String,
            val notification: Boolean,
            val email: Boolean
        )
    }
}
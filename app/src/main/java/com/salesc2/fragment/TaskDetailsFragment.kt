package com.salesc2.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.CalendarContract
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.database.AddNote
import com.salesc2.database.AppDatabase
import com.salesc2.database.SelectService
import com.salesc2.model.SelectAccountModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.set


class TaskDetailsFragment: Fragment(){

    private lateinit var viewPager: ViewPager
    private lateinit var tabLayout: TabLayout
    private lateinit var task_details_name: TextView
    private lateinit var taskDetails_assigneeToName : TextView
    private lateinit var task_details_status: TextView
    private lateinit var taskDetails_DateTime : TextView
    private lateinit var taskDetails_updateStatus: TextView
    private lateinit var taskDetails_changeAssignee:TextView
    private lateinit var enterNote : EditText
    private lateinit var add_note : TextView
    private lateinit var task_details_back_arrow:ImageView
    private lateinit var edit_task : ImageView

      private var  fromClass = String()
    var webService = WebService()
    var apiRequest = ApiRequest()
    lateinit var mView: View
    private var taskID = String()
    private var task_start_date = String()
    private var task_end_date = String()
    private var taskAssignToId = String()

    var tdiServiceName = String()
    var tdiAccountName = String()
    var tdiPhone = String()
    var tdiEmail = String()
    var tdiLoaction = String()
    var tdiLat = String()
    var tdiLon = String()
    var tdiDesc = String()

    var tdiServiceId = String()
    var tdiDeptName = String()
    var tdiDeptId = String()
    var tdiAssignedToName = String()

    var editSt_date = String()
    var editEd_date = String()
    var editSt_time = String()
    var editEd_time = String()

    var remStDate = String()
    var remEdDate = String()

    private var taskStartDate = String()
    private var taskEndDate = String()
    private var taskAccountID = String()
    private var changeAssignee = String()
    private var assignToId = String()
    val tdiBundle = Bundle()
    private var taskStatus = String()
    private lateinit var noteLayout:ConstraintLayout


    private var  assignToImgWithBase = String()

    private var update_api_call = String()

    private var set_timezone = String()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance=true
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_task_details,container,false)
        taskID = context?.getSharedPref("task_id",context!!).toString()
        task_start_date = arguments?.getString("task_start_date").toString()
        task_end_date = arguments?.getString("task_end_date").toString()
//      changeAssignee = arguments?.getString("task_assignee_name").toString()
        changeAssignee = context?.getSharedPref("td_assignee_name",context!!).toString()
//      assignToId = arguments?.getString("task_assignto_id_change").toString()
        assignToId = context?.getSharedPref("task_assignto_id_change",context!!).toString()
        update_api_call = context?.getSharedPref("update_api_call",context!!).toString()
        set_timezone = context?.getSharedPref("set_timezone",context!!).toString()
       /* context?.setSharedPref(context!!,"task_account_id",taskAccountID)
        context?.setSharedPref(context!!,"task_start_date",task_start_date)
        context?.setSharedPref(context!!,"task_end_date",task_end_date)
        context?.setSharedPref(context!!,"td_task_id",taskDetailsTaskId)*/

        fromClass = context?.getSharedPref("fromClass",context!!).toString()


        task_details_back_arrow= mView.findViewById(R.id.task_details_back_arrow)
        viewPager = mView.findViewById(R.id.taskDetails_ViewPager)
        tabLayout = mView.findViewById(R.id.taskDetails_TableLayout)
        task_details_name = mView.findViewById(R.id.task_details_name)
        taskDetails_assigneeToName = mView.findViewById(R.id.taskDetails_assigneeToName)
        task_details_status = mView.findViewById(R.id.task_details_status)
        taskDetails_DateTime = mView.findViewById(R.id.taskDetails_DateTime)
        taskDetails_updateStatus = mView.findViewById(R.id.taskDetails_updateStatus)
        taskDetails_changeAssignee = mView.findViewById(R.id.taskDetails_changeAssignee)
        enterNote = mView.findViewById(R.id.entertext_et)
        add_note = mView.findViewById(R.id.add_note_tv)
        noteLayout = mView.findViewById(R.id.noteLayout)
        edit_task = mView.findViewById(R.id.edit_task)


//        if (update_api_call == "yes"){
//            updateDetailsPage()
//        }else{

//        }
        if (context?.isNetworkAvailable(context!!) == true){
            taskDetailsApiCall()
        }else{
            showTaskOfflineData()
        }

        goBack()
        taskUpdateStatusAction()
        taskAssigneeChangeAction()
        editDateTimeAction()
        editTaskAction()

        //offline data// will update once api issue fixed
//        showTaskOfflineData()

        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE

        return mView
    }


    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
          context?.toast("isVisible")
        }
    }

    private fun goBack(){
        task_details_back_arrow.setOnClickListener{
//            startActivity(Intent(context,MainActivity::class.java))
if (fromClass == "Dash"){
    startActivity(Intent(context,MainActivity::class.java))
}else{
    fragmentManager?.popBackStack()
}

            /*var frag =
                activity?.supportFragmentManager?.findFragmentByTag("TaskDetailsFragment")

            if (frag is TaskDetailsFragment) {
                startActivity(Intent(context,MainActivity::class.java))
//                val fragment = Das()
//                val transaction: FragmentTransaction? =
//                    fragmentManager?.beginTransaction()
//                transaction?.replace(R.id.fragmet_container, fragment)
//                transaction?.addToBackStack(null)
//                transaction?.commit()
//                val a = activity
//                a?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

            } else{
                context?.progressBarDismiss(context!!)
                fragmentManager?.popBackStack()
            }*/

        }
    }

    private fun taskStatusBg(taskStatusText:String){
        if (taskStatusText.equals("Unassigned", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }
            task_details_status.text = context?.capFirstLetter(taskStatus)

        } else if (taskStatusText.equals("Completed", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.completed_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.completed_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }

            task_details_status.text = context?.capFirstLetter(taskStatus)
        } else if (taskStatusText.equals("Uncovered", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.uncovered_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.uncovered_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }
            task_details_status.text =context?.capFirstLetter(taskStatus)
        } else if (taskStatusText.equals("Cancelled", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.cancelled_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.cancelled_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }
            task_details_status.text = context?.capFirstLetter(taskStatus)
        } else if (taskStatusText.equals("In Progress", ignoreCase = true) ||taskStatusText.equals("inprogress", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.in_progress_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.in_progress_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }
            task_details_status.text = context?.capFirstLetter(taskStatus)
        } else if (taskStatusText.equals("Pending", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.pending_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }
            task_details_status.text = context?.capFirstLetter(taskStatus)
        } else if (taskStatusText.equals("Assigned", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.assigned_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }
            task_details_status.text = context?.capFirstLetter(taskStatus)
        }
    }



    private fun editTaskAction() {
        edit_task.setOnClickListener {
            val popup = PopupMenu(context, edit_task)
//            val inflater: MenuInflater = popup.menuInflater
//            inflater.inflate(R.menu.bottom_nav_popup_menu, popup.menu)
            popup.menu.add("  Add Reminder")
            popup.menu.add("  Edit")

            popup.show()
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
//                    context?.toast(item.title as String)
                    if (item.title == "  Edit"){
                        context?.setSharedPref(context!!,"edit_task","yes")

                        context?.setSharedPref(context!!,"edit_task_taskId",taskID)
                        context?.setSharedPref(context!!,"acc_filter_id",taskAccountID)
                        context?.setSharedPref(context!!,"acc_filter_name",tdiAccountName)
                        context?.setSharedPref(context!!,"dept_filter_id",tdiDeptId)
                        context?.setSharedPref(context!!,"dept_filter_name",tdiDeptName)
                        context?.setSharedPref(context!!,"service_filter_id",tdiServiceId)
                        context?.setSharedPref(context!!,"service_filter_name",tdiServiceName)
                        context?.setSharedPref(context!!,"start_date",editSt_date)
                        context?.setSharedPref(context!!,"start_time", editSt_time)
                        context?.setSharedPref(context!!,"end_time",editEd_time)
                        context?.setSharedPref(context!!,"end_date",editEd_date)
                        context?.setSharedPref(context!!,"assignee_name",tdiAssignedToName)
                        context?.setSharedPref(context!!,"assignee_image",assignToImgWithBase)
                        context?.setSharedPref(context!!,"assignee_id",taskAssignToId)
                        context?.loadFragment(context!!,AddTaskFragment())
                    }else{
                        var intent: Intent  = Intent(Intent.ACTION_INSERT);
                        intent.type = "vnd.android.cursor.item/event";

                        var cal: Calendar  = Calendar.getInstance();
                        var startTime = "$editSt_date $editSt_time"
                        var endTime = "$editEd_date $editEd_time"

                        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, remStDate)
                        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, remEdDate)
                        intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)

                        intent.putExtra(CalendarContract.Events.TITLE, task_details_name.text.toString())
                        intent.putExtra(CalendarContract.Events.DESCRIPTION, tdiDesc)
                        intent.putExtra(CalendarContract.Events.EVENT_LOCATION, tdiLoaction)
//                        intent.putExtra(CalendarContract.Events.RRULE, "FREQ=YEARLY");
                        startActivity(intent);
                    }
                    return true
                }
            })
        }
    }


    private fun addNoteApiCall(){
        add_note.setOnClickListener {
         if (context?.isNetworkAvailable(context!!) == true){
             add_note.isEnabled = false
             var note = enterNote.text.toString()
             if (note.trim() == "" || note.trim().isEmpty() || note.trim().isBlank()){
                 context?.toast("Please enter a note")
                 add_note.isEnabled = true
             }else {

                 var params = HashMap<String, String>()
                 params["task_id"] = taskID
                 params["notes"] = note
                 context?.let {
                     apiRequest.postRequestBodyWithHeaders(it, webService.Task_details_add_note_url, params) { response ->
                         try {
                             var status = response.getString("status")
                             var msg = response.getString("msg")
                             if (status == "200") {
                                 val inputMethodManager = context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                                 inputMethodManager.hideSoftInputFromWindow(view?.windowToken, 0)
                                 enterNote.text.clear()
                                 context?.toast(msg)
                                 taskDetailsViewPagerAction()
                                 add_note.isEnabled = true
                             } else {
                                 context?.toast(msg)
                                 add_note.isEnabled = true
                             }
                         } catch (e: Exception) {
                             print(e)
                             add_note.isEnabled = true
                         }

                     }
                 }
             }
         }else{
             CoroutineScope(Dispatchers.IO).launch {

                 db = AppDatabase.getDatabaseClient(requireContext())

                 db!!.addNoteDao().insertAll(
                     AddNote(
                         0,
                         taskID, enterNote.text.toString())
                 )
             }
         }
        }
    }

    private fun editDateTimeAction(){
       /* taskDetails_DateTime.setOnClickListener{
//            var bundle = Bundle()
//            bundle.putString("task_id",taskDetailsTaskId)
//            bundle.putString("task_st_date",taskStartDate)
//            bundle.putString("task_ed_date",taskEndDate)
            context?.setSharedPref(context!!,"start_date",editSt_date)
            context?.setSharedPref(context!!,"start_time", editSt_time)
            context?.setSharedPref(context!!,"end_time",editEd_time)
            context?.setSharedPref(context!!,"end_date",editEd_date)
            context?.setSharedPref(context!!,"edit_task_detail_date","yes")

            //replaceing Fragment
            var fragment = SelectDateTimeFragment()

            val activity = context as AppCompatActivity
            val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
            transaction.replace(R.id.main_fragmet_container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()

        }*/
    }


    private fun taskAssigneeChangeAction(){
        taskDetails_changeAssignee.setOnClickListener{
            if (taskStatus.equals("Inprogress",ignoreCase = true) || taskStatus.equals("In Progress",ignoreCase = true)){
                context?.toast("Task in progress, not able to reassign")
            }else{
                var accId = context?.getSharedPref("task_account_id",context!!).toString()
                var tStDate = context?.getSharedPref("task_start_date",context!!).toString()
                var tEdDate =  context?.getSharedPref("task_end_date",context!!).toString()
                context?.setSharedPref(context!!,"task_id",taskID)
                var bundle = Bundle()
                bundle.putString("task_start_date",tStDate)
                bundle.putString("task_end_date",tEdDate)
                bundle.putString("task_account_id",taskAccountID)
                bundle.putString("task_id",taskID)
                var fragment = TaskDetailsAssigneeChangeFragment()
                fragment.arguments = bundle
                val activity = context as AppCompatActivity
                val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
                transaction.replace(R.id.main_fragmet_container, fragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }

        }
    }
    var mBottomSheetDialog: Dialog? = null
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun taskUpdateStatusAction() {
        taskDetails_updateStatus.setOnClickListener {
            if (task_details_status.text.toString() == "Completed" || task_details_status.text.toString() == "completed" || task_details_status.text.toString() == "Cancelled" || task_details_status.text.toString() == "cancelled" || task_details_status.text.toString() == "Uncovered" || task_details_status.text.toString() == "uncovered") {
                context?.toast(task_details_status.text.toString())
            } else {
                var  popup = PopupMenu(context, taskDetails_updateStatus, R.style.PopupMenu)

                popup.gravity = Gravity.CENTER_HORIZONTAL

                popup.menuInflater.inflate(R.menu.popup_menu, popup.menu)
                if (task_details_status.text.toString().equals("In Progress",ignoreCase = true) || task_details_status.text.toString().equals("inprogress",ignoreCase = true)) {
                    popup.menu.add("        Completed")
                    popup.menu.add("        Cancelled")
                }
                else if (task_details_status.text.toString().equals("Unassigned",ignoreCase = true)) {
                    popup.menu.add("         Cancelled")
                    popup.menu.add("        Uncovered")
                }
                else if (task_details_status.text.toString().equals( "Pending",ignoreCase = true)) {
                    popup.menu.add("         Cancelled")
                    popup.menu.add("        Uncovered")
                    popup.menu.add("        Unassigned")
                }
                else if (task_details_status.text.toString().equals ("Assigned" ,ignoreCase = true)) {
                    popup.menu.add("         Unassigned")
                    popup.menu.add("          Cancelled")
                    popup.menu.add( "         Uncovered")
                    popup.menu.add( "        In Progress")
                }

                popup.setOnMenuItemClickListener(object:PopupMenu.OnMenuItemClickListener {
                    override fun onMenuItemClick(item: MenuItem):Boolean {
                    var taskStatus= getStatus(item.toString().trim().replace(" ",""))
                        var params = HashMap<String, String>()
                        params["taskStatus"] = taskStatus
                        params["_id"] = taskID
                        params["assignedTo"] = taskAssignToId
                        context?.let { it1 ->
                            apiRequest.postRequestBodyWithHeaders(
                                it1,
                                webService.Update_status_url,
                                params
                            ) { response ->
                                var status = response.getString("status")
                                var msg = response.getString("msg")
                                if (status == "200") {
                                    context?.toast(msg)
                                    context?.progressBarDismiss(context!!)
                                    context?.loadFragment(context!!,TaskDetailsFragment())

                                } else {
                                    context?.toast(msg)
                                    context?.progressBarDismiss(context!!)
                                }

                            }
                        }




                        return true
                    }
                })
                popup.show() //showing popup menu


                }


            }


        }

    private fun getStatus(Selected: String) : String {
        if (Selected.equals("Inprogress",ignoreCase = true) ||Selected.equals("In progress",ignoreCase = true)) {
            return "inprogress"
        }
        else if (Selected == "Uncovered") {
            return "uncovered"
        }
        else if (Selected == "Cancelled" || Selected == "Canceled"){
            return "cancelled"
        }
        else if (Selected == "Completed") {
            return "completed"
        }
        else if (Selected == "Unassigned") {
            return "Unassigned"
        }
        else if (Selected == "Pending") {
            return "Pending"
        }
        else if (Selected == "Assigned") {
            return "assigned"
        }
        return "inprogress"
    }

    private fun taskDetailsViewPagerAction(){

        var bundle = Bundle()
        bundle.putString("task_id",taskID)
        val taskDetailsTimelineFragment = TaskDetailsTimelineFragment()
        taskDetailsTimelineFragment.arguments = bundle

        tdiBundle.putString("tdi_service_name",tdiServiceName)
        tdiBundle.putString("tdi_account_name",tdiAccountName)
        tdiBundle.putString("tdi_phone",tdiPhone)
        tdiBundle.putString("tdi_email",tdiEmail)
        tdiBundle.putString("tdi_location_name",tdiLoaction)
        tdiBundle.putString("tdi_lat",tdiLat)
        tdiBundle.putString("tdi_lon",tdiLon)
        tdiBundle.putString("tdi_desc",tdiDesc)
        val taskDetailsInformationFragment = TaskDetailsInformationFragment()
        taskDetailsInformationFragment.arguments = tdiBundle

        val taskDetailsProductAndExpensesFragment = TaskDetailsProductAndExpensesFragment()
        taskDetailsProductAndExpensesFragment.arguments = bundle


        val myViewPageStateAdapter: MyViewPageStateAdapter = MyViewPageStateAdapter(activity!!.supportFragmentManager)
        viewPager.offscreenPageLimit
        myViewPageStateAdapter.addFragment(taskDetailsTimelineFragment, "Timeline")

        myViewPageStateAdapter.addFragment(taskDetailsInformationFragment, "Task Information")
        myViewPageStateAdapter.addFragment(taskDetailsProductAndExpensesFragment, "Product & Expenses")
        viewPager.adapter = myViewPageStateAdapter
        tabLayout.setupWithViewPager(viewPager, true)

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                viewPager.currentItem = tab!!.position
                val fm = activity?.supportFragmentManager
                val ft = fm!!.beginTransaction()
                val count = fm.backStackEntryCount
                if (count >= 1) {
                    //   activity?.supportFragmentManager!!.popBackStack()
                }

                ft.commit()
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
             if(tab?.isSelected?.equals(tab.position==0)!!){
                 noteLayout.visibility = View.VISIBLE
             }
             else{
                 noteLayout.visibility = View.GONE
             }
            }

        })

    }

    class MyViewPageStateAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
       private val fragmentList: MutableList<Fragment> = ArrayList<Fragment>()
       private val fragmentTitleList: MutableList<String> = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {

            return fragmentTitleList[position]
        }

        fun addFragment(fragment: Fragment, title: String) {
            fragmentList.add(fragment)
            fragmentTitleList.add(title)

        }
    }

    private fun taskDetailsApiCall(){

        val params = HashMap<String,String>()
            params["id"] = taskID
        context?.let {
            apiRequest.postRequestBodyWithHeaders(it,webService.Task_details_url,params){ response->


                try {
                var status = response.getString("status")
                    var msg = String()
                    if (response.has("msg")){
                         msg = response.getString("msg")
                    }

                if (status == "200"){
                    var dataObj = response.getJSONObject("data")
                    var _id = dataObj.getString("_id")
                    context?.setSharedPref(context!!,"task_id",_id)
                    taskDetailsViewPagerAction()
                    taskStatus = dataObj.getString("taskStatus")
                    taskStatusBg(taskStatus)
                    var startDate = dataObj.getString("startDate")
                    taskStartDate = startDate
                    var endDate = dataObj.getString("endDate")
                    taskEndDate = endDate
                    var taskDescription = dataObj.getString("taskDescription")
                    tdiDesc = taskDescription

                    try {
                        var parser = SimpleDateFormat("yyyy-MM-dd HH:mm")
                        val myFormatDate = "MMM dd, yyyy"
                        val sdf = SimpleDateFormat(myFormatDate)
                        val stDate = parser.parse(startDate)
                        val stDateUTC = context?.dateFromUTC(context!!,stDate)
                        val stDateFormat = sdf.format(stDateUTC)

                        val myFormatTime = "hh:mm a"
                        val sdfTime = SimpleDateFormat(myFormatTime)

                        val startTime =parser.parse(startDate)
                        val startTimeUTC = context?.dateFromUTC(context!!,startTime)
                        val stTimeFormat = sdfTime.format(startTimeUTC)
                        val endTime =parser.parse(endDate)
                        val endTimeUTC = context?.dateFromUTC(context!!,endTime)
                        val edTimeFormat = sdfTime.format(endTimeUTC)


                        remStDate = "$stDateFormat $stTimeFormat"
                        remEdDate = "$stDateFormat $edTimeFormat"
                        taskDetails_DateTime.text = "$stDateFormat at $stTimeFormat to $edTimeFormat".replace("am","AM").replace("pm","PM")

                        val dateFormat = "MM/dd/yyyy"
                        var sdf_date = SimpleDateFormat(dateFormat)
                        var stDateEdit = parser.parse(startDate)
                        var stDateEditToLocal = context?.dateFromUTC(context!!,stDateEdit)
                        editSt_date = sdf_date.format(stDateEditToLocal)
                        var edDateEdit = parser.parse(endDate)
                        var edDateEditToLocal = context?.dateFromUTC(context!!,edDateEdit)
                        editEd_date = sdf_date.format(edDateEditToLocal)
                        var stTimeEdit = parser.parse(startDate)
                        var stTimeToLocal = context?.dateFromUTC(context!!,stTimeEdit)
                        editSt_time = sdfTime.format(stTimeToLocal)
                        var edTimeEdit = parser.parse(endDate)
                        var edTimeToLocal = context?.dateFromUTC(context!!,edTimeEdit)
                        editEd_time = sdfTime.format(edTimeToLocal)

                    }catch (e:Exception){
                        print("td_date parse== $e")
                    }


                    var serviceObj = dataObj.getJSONObject("service")
                    var service_id = serviceObj.getString("_id")
                    tdiServiceId  = service_id
                    var service = serviceObj.getString("service")
                    tdiServiceName = service

                    var accountObj =dataObj.getJSONObject("account")
                    var account_id = accountObj.getString("_id")
                    taskAccountID = account_id
                    var accountName = accountObj.getString("accountName")
                    tdiAccountName = accountName

                    var addressObj = accountObj.getJSONObject("address")
                    var formatedAddress = addressObj.getString("formatedAddress")
                    tdiLoaction = formatedAddress
                    var city = addressObj.getString("city")
                    var state = addressObj.getString("state")
                    var zipCode = addressObj.getString("zipCode")

                    var locationObj = accountObj.getJSONObject("location")
                    var lat = locationObj.getString("lat")
                    var lng = locationObj.getString("lng")
                    tdiLat = lat
                    tdiLon = lng

                    var department_detailsObj = dataObj.getJSONObject("department_details")
                    var deptDetail_id = department_detailsObj.getString("_id")
                    tdiDeptId = deptDetail_id
                    var dept_name = department_detailsObj.getString("name")
                    tdiDeptName = dept_name


                    var department_contactsArray = dataObj.getJSONArray("department_contacts")
                    for (i in 0 until department_contactsArray.length()){
                        var department_contacts_Obj = department_contactsArray.getJSONObject(i)
                        var deptContacts_id = department_contacts_Obj.getString("_id")
                        var companyId = department_contacts_Obj.getString("companyId")
                        var departmentId = department_contacts_Obj.getString("departmentId")
                        var name = department_contacts_Obj.getString("name")

                        var phoneArray = department_contacts_Obj.getJSONArray("phone")
                        for (p in 0 until phoneArray.length()){
                            var phoneObj = phoneArray.getJSONObject(p)
                            var phone_id = phoneObj.getString("_id")
                            var phoneNo = phoneObj.getString("phoneNo")
                            tdiPhone = phoneNo
                            var phoneType = phoneObj.getString("phoneType")
                        }

                        var emailArray = department_contacts_Obj.getJSONArray("email")
                        for (e in 0 until emailArray.length()){
                            var emailObj = emailArray.getJSONObject(e)
                            var id_email = emailObj.getString("_id")
                            var emailid = emailObj.getString("emailid")
                            tdiEmail = emailid
                            var emailType =  emailObj.getString("emailType")
                        }

                        if(department_contacts_Obj.has("isPrimary")) {
                            var isPrimary = department_contacts_Obj.getString("isPrimary")
                        }
                        var addedBy = department_contacts_Obj.getString("addedBy")
                        var createdAt = department_contacts_Obj.getString("createdAt")
                        var updatedAt = department_contacts_Obj.getString("updatedAt")
                        var __v = department_contacts_Obj.getString("__v")

                    }

                    var assigned_to_id = dataObj.getString("assigned_to_id")
                    taskAssignToId = assigned_to_id
                    var assigned_to_name = dataObj.getString("assigned_to_name")
                    tdiAssignedToName = assigned_to_name
                    if (dataObj.has("assigned_to_image")){
                        var assigned_to_image = dataObj.getString("assigned_to_image")
                        assignToImgWithBase = webService.imageBasePath+assigned_to_image
                    }


                    if (changeAssignee == ""|| changeAssignee== "null") {
                        if (assigned_to_name != "null"){
                            taskDetails_assigneeToName.text = assigned_to_name
                        }else{
                            taskDetails_assigneeToName.text = ""
                            taskDetails_changeAssignee.text = "Assign"
                        }
                    }else {
                        if (changeAssignee != "null") {
                            taskDetails_assigneeToName.text = changeAssignee
                        }else{
                            taskDetails_assigneeToName.text = ""
                            taskDetails_changeAssignee.text = "Assign"
                        }
                    }


                    var taskName = dataObj.getString("taskName")
                    task_details_name.text = taskName
                    var conflict = dataObj.getString("conflict")
                    var conflictName = dataObj.getString("conflictName")

                    addNoteApiCall()
                    taskDetailsViewPagerAction()
                    if (update_api_call == "yes"){
                        updateDetailsPage()
                    }
                    val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
                    val editor : SharedPreferences.Editor = preferences.edit()
                    editor.remove("update_api_call")
                    editor.commit()

                }else{
                    taskDetailsViewPagerAction()
                    addNoteApiCall()
                    if (update_api_call == "yes"){
                        updateDetailsPage()
                    }

                    val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
                    val editor : SharedPreferences.Editor = preferences.edit()
                    editor.remove("update_api_call")
                    editor.commit()
                    context?.toast(msg)
                }

            }catch (e:Exception){
                    print("task detail exception== $e")
                    taskDetailsViewPagerAction()
                    addNoteApiCall()
                    if (update_api_call == "yes"){
                        updateDetailsPage()
                    }
                    val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
                    val editor : SharedPreferences.Editor = preferences.edit()
                    editor.remove("update_api_call")
                    editor.commit()
                    context?.toast(e.toString())

                }

            }
        }
    }



    private fun updateDetailsPage() {
        var params = HashMap<String, String>()
        params["_id"] = taskID
        params["taskStatus"] = "Pending"
            params["assignedTo"] = assignToId

        context?.let {
            apiRequest.postRequestBodyWithHeaders(
                it,
                webService.Update_status_url,
                params
            ) { response ->

                var status = response.getString("status")
                if (status == "200") {

                   context?.loadFragment(context!!,TaskDetailsFragment())
                }


            }
        }
    }


    private fun statusColorAndBg(Status:String){
        if (Status.equals("Unassigned", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_bg) }?.let {
               task_details_status.setBackgroundColor(it)
            }

        } else if (Status.equals("Completed", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.completed_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.completed_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }

        } else if (Status.equals("Uncovered", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.uncovered_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.uncovered_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }
//            val unwrappedDrawable = AppCompatResources.getDrawable(context!!, R.drawable.ic_dot

        } else if (Status.equals("Cancelled", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.cancelled_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.cancelled_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }

        } else if (Status.equals("In Progress", ignoreCase = true) || Status.equals("InProgress", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.in_progress_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.in_progress_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }

        } else if (Status.equals("Pending", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.pending_text) }?.let {
                task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }

        } else if (Status.equals("Assigned", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.assigned_text) }?.let {
              task_details_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_bg) }?.let {
                task_details_status.setBackgroundColor(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        context?.progressBarDismiss(context!!)
    }


    var db : AppDatabase? = null
    //offline mode
    private fun showTaskOfflineData(){
        CoroutineScope(Dispatchers.Main).launch {
            db = AppDatabase.getDatabaseClient(requireContext())
            val taskData = db!!.taskDao().getTaskDetails(taskID)
            Log.i("taskId=====",taskID)
            Log.i("taskData",taskData.toString())
            if (taskData.isNotEmpty()){
                taskDetailsViewPagerAction()
                var taskId = taskData[0].task_id
                var startDate = taskData[0].startDate
                var endDate = taskData[0].endDate
                try {
                    var parser = SimpleDateFormat("yyyy-MM-dd HH:mm")
                    val myFormatDate = "MMM dd, yyyy"
                    val sdf = SimpleDateFormat(myFormatDate)
                    val stDate = parser.parse(startDate)
                    val stDateUTC = context?.dateFromUTC(context!!,stDate)
                    val stDateFormat = sdf.format(stDateUTC)

                    val myFormatTime = "hh:mm a"
                    val sdfTime = SimpleDateFormat(myFormatTime)

                    val startTime =parser.parse(startDate)
                    val startTimeUTC = context?.dateFromUTC(context!!,startTime)
                    val stTimeFormat = sdfTime.format(startTimeUTC)
                    val endTime =parser.parse(endDate)
                    val endTimeUTC = context?.dateFromUTC(context!!,endTime)
                    val edTimeFormat = sdfTime.format(endTimeUTC)


                    remStDate = "$stDateFormat $stTimeFormat"
                    remEdDate = "$stDateFormat $edTimeFormat"
                    taskDetails_DateTime.text = "$stDateFormat at $stTimeFormat to $edTimeFormat".replace("am","AM").replace("pm","PM")

                    val dateFormat = "MM/dd/yyyy"
                    var sdf_date = SimpleDateFormat(dateFormat)
                    var stDateEdit = parser.parse(startDate)
                    var stDateEditToLocal = context?.dateFromUTC(context!!,stDateEdit)
                    editSt_date = sdf_date.format(stDateEditToLocal)
                    var edDateEdit = parser.parse(endDate)
                    var edDateEditToLocal = context?.dateFromUTC(context!!,edDateEdit)
                    editEd_date = sdf_date.format(edDateEditToLocal)
                    var stTimeEdit = parser.parse(startDate)
                    var stTimeToLocal = context?.dateFromUTC(context!!,stTimeEdit)
                    editSt_time = sdfTime.format(stTimeToLocal)
                    var edTimeEdit = parser.parse(endDate)
                    var edTimeToLocal = context?.dateFromUTC(context!!,edTimeEdit)
                    editEd_time = sdfTime.format(edTimeToLocal)

                }catch (e:Exception){
                    e.printStackTrace()
                }

                var taskName = taskData[0].taskName
                task_details_name.text = taskName

                var taskStatus = taskData[0].taskStatus
                taskStatusBg(taskStatus.toString())

                var deptName = taskData[0].departmentName
                var timelineArr = taskData[0].timeline
                context?.setSharedPref(context!!,"timeline_offline_data",timelineArr.toString())
                var deptContacts = taskData[0].department_contacts
                var products = taskData[0].products
                var expenses = taskData[0].expenses
                var lat = taskData[0].lat
                tdiLat = lat.toString()
                var lng = taskData[0].lng
                tdiLon = lng.toString()
                var assignToId = taskData[0].assignTo_id

                var assignToName = taskData[0].assignTo_name
                tdiAssignedToName = assignToName.toString()
                if (assignToName != "null" || assignToName != ""){
                    taskDetails_assigneeToName.text = assignToName
                }else{
                    taskDetails_assigneeToName.text = ""
                    taskDetails_changeAssignee.text = "Assign"
                }
                var service_id = taskData[0].service_id
                tdiServiceId = service_id.toString()
                var service_name = taskData[0].service_name
                tdiServiceName = service_name.toString()
                var account_id = taskData[0].account_id

                var account_name = taskData[0].account_name
                tdiAccountName = account_name.toString()
                var dept_id = taskData[0].dept_id
                tdiDeptId = dept_id.toString()
                var dept_name = taskData[0].dept_name
                tdiDeptName = dept_name.toString()
            }
        }
    }


}



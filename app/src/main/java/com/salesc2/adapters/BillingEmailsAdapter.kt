package com.salesc2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.BillingEmailsModel
import com.salesc2.model.BillingHistoryModel

class BillingEmailsAdapter() : RecyclerView.Adapter<BillingEmailsAdapter.MyViewHolder>() {
    private var billingEmailsItems = ArrayList<BillingEmailsModel>()
    var context : Context? = null

    constructor(context: Context?, billingEmailsItems: java.util.ArrayList<BillingEmailsModel>) : this(){
        this.context = context
        this.billingEmailsItems = billingEmailsItems

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_billing_emails, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return billingEmailsItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var billingEmailsModel : BillingEmailsModel = billingEmailsItems[position]
        holder.invoiceEmails.text = billingEmailsModel.emails
    }
    class MyViewHolder constructor(itemView:View) : RecyclerView.ViewHolder(itemView){
        var invoiceEmails : TextView = itemView.findViewById(R.id.invoiceEmails)
    }
}
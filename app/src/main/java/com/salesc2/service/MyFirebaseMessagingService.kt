package com.salesc2.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.salesc2.MainActivity
import com.salesc2.NotificationHandler
import com.salesc2.R
import com.salesc2.utils.SharePreferences
import com.salesc2.utils.setSharedPref
import java.util.*

class MyFirebaseMessagingService: FirebaseMessagingService() {

    var sharePreferences = SharePreferences()

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
//        try {
//            var mainActivity = MainActivity()
//            mainActivity.refreshDashboardAPIs()
//        }catch (e:Exception){
//
//        }
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ



        val data:Map<String,String> = remoteMessage.data
        val body:String? = data["body"]
        val title:String? = data["title"]
        val showPopup:String? = data["showPopup"]
        val messageFrom : String? = data["messageFrom"]
        val id : String? = data["id"]
        val type : String? = data["type"]
        val notificationType : String? = data["notificationType"]


        val click_action:String? = remoteMessage.notification!!.clickAction


        sendNotification(
            id.toString(),
            title.toString(),
            type.toString(),
            showPopup.toString(),
            notificationType.toString(),
            remoteMessage.getData().toString(),
            click_action.toString()
        )


    }

    override fun onNewToken(token: String) {

        sendRegistrationToServer(token)

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        val registrationComplete = Intent(Config.REGISTRATION_COMPLETE)
        registrationComplete.putExtra("token", token)
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete)

    }

    companion object {
        private const val TAG = "MyAndroidFCMService"
        private var type: String? = null
        var post_id: String? = null
        var post_message: String? = null
        var notificationManager: NotificationManager? = null
        const val Notification_ID = 102
    }

    private fun sendRegistrationToServer(token: String?) {
        // sending gcm token to server

        val database: FirebaseDatabase = Firebase.database
        val DatabaseReference: DatabaseReference = database.getReference("Tokens")
        DatabaseReference.setValue(token)
        this.setSharedPref(this@MyFirebaseMessagingService,"reg_device_token", token.toString())

    }

    private fun sendNotification(
        id: String,
        title : String,
        type: String,
        showPopup:String,
        notificationType:String,
        data:String,
        click_action:String) {
        /* var info = JSONObject(data)
         var aps:String = info.getString("aps")

         var apsDirectory = JSONObject(aps)

         var alert = apsDirectory.getString("alert")

 */


        val intent = Intent(click_action)
//        val intent = Intent(click_action)
        val bundle = Bundle()
        bundle.putString("id", id)
        bundle.putString("type", type)
        bundle.putString("showPopup", showPopup)
        bundle.putString("notificationType", notificationType)
        bundle.putString("data",data)
//        bundle.putString("")
        intent.putExtras(bundle)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val resultIntent = PendingIntent.getActivity(
                this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT
            )
            val largeIcon = BitmapFactory.decodeResource(
                this!!.resources,
                R.mipmap.ic_launcher_foreground
            )

// The user-visible description of the channel.
            val CHANNEL_ID = "my_channel_01" // The id of the channel.
            val notification_name: CharSequence = " Test" // The user-visible name of the channel.
            val notification_importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(CHANNEL_ID, notification_name, notification_importance)
            mChannel.setSound(null, null)
            // Create a notification and set the notification channel.
            val view = RemoteViews(packageName, R.layout.notification_collapsed)
            view.setTextViewText(
                R.id.notification_title,
                title
            )
//            view.setTextViewText(
//                R.id.notification_message,
//                message
//            )
            val notifyIntent = Intent(this, NotificationHandler::class.java)
            notifyIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            val pendingIntent = PendingIntent.getActivity(
                this,
                1,
                notifyIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )

            val notification: Notification =
                Notification.Builder(this)
                    .setLargeIcon(largeIcon)
                    .setCustomContentView(view)
                    .setCustomBigContentView(view)
                    .setContentTitle(title)
//                    .setContentText(message)
                    .setSmallIcon(R.drawable.ic_notify_icon)
                    .setChannelId(CHANNEL_ID).setContentIntent(resultIntent).setAutoCancel(true)
                    .build()
            notification.flags = Notification.FLAG_ONGOING_EVENT
            notificationManager =
                this!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            Objects.requireNonNull(notificationManager)
                ?.createNotificationChannel(mChannel)

// Issue the notification.
            notificationManager!!.notify(
                Notification_ID,
                notification
            )
        } else {
            val resultIntent = PendingIntent.getActivity(
                this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT
            )
            val notificationSoundURI = RingtoneManager.getDefaultUri(
                RingtoneManager.TYPE_NOTIFICATION
            )
            val mNotificationBuilder =
                NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_notify_icon)
                    .setContentTitle(title)
//                    .setStyle(
//                        NotificationCompat.BigTextStyle()
//                            .bigText(message)
//                    )
//                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(notificationSoundURI)
                    .setContentIntent(resultIntent)
            notificationManager =
                this!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            Objects.requireNonNull(notificationManager)
                ?.notify(0, mNotificationBuilder.build())
        }
    }
}

package com.salesc2.data.network.di

object Constants {
     var URL = "https://stage-api.salesc2.com/"

     val UserService = "https://stage-api.salesc2.com/ios-login/api/v1/"
     val TeamService = "https://stage-api.salesc2.com/ios-user/api/v1/"
     val TaskService = "https://stage-api.salesc2.com/ios-task/api/v1/"

     var Token = ""


}
package com.salesc2.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.lang.Exception

class EventMultipleAttendeeAdapter(private val context: Activity, private val a_id:Array<String>, private val a_name: Array<String>, private val a_img: Array<String>)
    : RecyclerView.Adapter<EventMultipleAttendeeAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_event_attendees, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return a_id.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        try {
          holder.attendee_name.text = a_name[position]
            Picasso.get().load(a_img[position]).into(holder.attendee_image)
        } catch (e: Exception) {
            print(e)
        }
        if (a_img[position] != "null" || a_img[position] != ""){
            holder.attendee_image.setBackgroundResource(R.drawable.ic_dummy_user_pic)
        }
    }

    class MyViewHolder constructor(itemView:View):RecyclerView.ViewHolder(itemView) {
        var attendee_image : CircleImageView = itemView.findViewById(R.id.attendee_image)
        var attendee_name : TextView = itemView.findViewById(R.id.attendee_name)
    }
}
package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestPasswordAndSecurity
import com.salesc2.data.network.api.response.ResponsePasswordAndSecurity
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.PasswordAndSecurityRepository
import kotlinx.coroutines.launch

class PasswordAndSecurityViewModel( private val repository: PasswordAndSecurityRepository, val context: Context
) : ViewModel() {
    fun reqPwSec(
        requestPwSec: RequestPasswordAndSecurity,
        onSuccess: OnSuccess<ResponsePasswordAndSecurity>,
        onFailure: OnFailure<ResponsePasswordAndSecurity>
    )
    {
        viewModelScope.launch {
            repository.reqGetPwSec(requestPwSec,onSuccess,onFailure)
        }
    }

}
package com.salesc2

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.salesc2.utils.progressBarDismiss
import com.salesc2.utils.progressBarShow
import com.salesc2.utils.toast
import java.security.KeyStore
import java.util.concurrent.Executor
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey


class FingerPrintAuthActivity : AppCompatActivity() {

    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo
    lateinit var biometricManager: BiometricManager
private lateinit var fingerProgress :ProgressBar
    var KEY_NAME = "User"

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fingerprint_auth)
        startActivity(Intent(this@FingerPrintAuthActivity,LoginActivity::class.java))

        println("test_activity"+"  "+"kscjasnsancnsn")
        fingerProgress = findViewById(R.id.fingerProgress)

        biometricManager = BiometricManager.from(this)
        executor = ContextCompat.getMainExecutor(this)
        checkBiometricStatus(biometricManager)
        biometricPrompt = BiometricPrompt(this, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(
                    errorCode: Int,
                    errString: CharSequence
                ) {
                    super.onAuthenticationError(errorCode, errString)
                    if(errorCode == BiometricPrompt.ERROR_NEGATIVE_BUTTON) {
                             // Because negative button says use application password
                        finishAffinity()
                    }
                    else if(errorCode == BiometricPrompt.ERROR_USER_CANCELED){
                        finishAffinity()
                    }
                    else if(errorCode == BiometricPrompt.ERROR_TIMEOUT){
                        finishAffinity()
                    }
                    else if(errorCode == BiometricPrompt.ERROR_LOCKOUT){
                        startActivity(Intent(this@FingerPrintAuthActivity,LoginActivity::class.java))
                    }


                    toast("Authentication: $errString")
                }

                override fun onAuthenticationSucceeded(
                    result: BiometricPrompt.AuthenticationResult
                ) {
                    super.onAuthenticationSucceeded(result)
//                    toast("Authentication succeeded!")
                    fingerProgress.visibility = View.VISIBLE
                    progressBarShow(this@FingerPrintAuthActivity)
                    goToHome()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    startActivity(Intent(this@FingerPrintAuthActivity,LoginActivity::class.java))
//                    toast("Authentication failed")
                }
            })

        promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle("Fingerprint Authentication")
//            .setSubtitle("Log in using your biometric credential")
//            .setNegativeButtonText("Use account password")
            .setDeviceCredentialAllowed(true)
            .setConfirmationRequired(false)
            .build()
        biometricPrompt.authenticate(promptInfo)

    }
    private fun checkBiometricStatus(biometricManager: BiometricManager){
        when (biometricManager.canAuthenticate()) {
            BiometricManager.BIOMETRIC_SUCCESS -> {
//              toast("App can authenticate using biometrics")
                Log.d("MY_APP_TAG", "App can authenticate using biometrics.")
            }
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE ->{
                toast("No biometric features available on this device.")
                Log.e("MY_APP_TAG", "No biometric features available on this device.")
                startActivity(Intent(this@FingerPrintAuthActivity,MainActivity::class.java))
            }

            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE ->{
                toast("Biometric features are currently unavailable.")
                Log.e("MY_APP_TAG", "Biometric features are currently unavailable.")
                startActivity(Intent(this@FingerPrintAuthActivity,MainActivity::class.java))
            }

            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED ->{
//                toast("The user hasn't associated any biometric credentials with their account.")
                Log.e("MY_APP_TAG", "The user hasn't associated " +
                        "any biometric credentials with their account.")
                startActivity(Intent(this@FingerPrintAuthActivity,MainActivity::class.java))
            }

        }
    }

    private fun goToHome(){
        startActivity(Intent(this, MainActivity::class.java))
        progressBarDismiss(this)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun generateSecretKey(keyGenParameterSpec: KeyGenParameterSpec) {
        val keyGenerator = KeyGenerator.getInstance(
            KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            keyGenerator.init(keyGenParameterSpec)
        }
        keyGenerator.generateKey()
    }

    private fun getSecretKey(): SecretKey {
        val keyStore = KeyStore.getInstance("AndroidKeyStore")

        // Before the keystore can be accessed, it must be loaded.
        keyStore.load(null)
        return keyStore.getKey(KEY_NAME, null) as SecretKey
    }

    private fun getCipher(): Cipher {
        return Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/"
                + KeyProperties.BLOCK_MODE_CBC + "/"
                + KeyProperties.ENCRYPTION_PADDING_PKCS7)

    }




}
package com.salesc2.fragment

import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.kaopiz.kprogresshud.KProgressHUD
import com.salesc2.R
import com.salesc2.adapters.TeamMembersListAdapter
import com.salesc2.model.TeamMemberListModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.ShowNoData
import com.salesc2.utils.alertDialog
import com.salesc2.utils.getSharedPref
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import java.util.*
import kotlin.collections.ArrayList


class TeamMembersListFragment : Fragment(), ShowNoData {

    lateinit var teamMembersListRecyclerView: RecyclerView
    lateinit var teamMemberListModel: ArrayList<TeamMemberListModel>
    lateinit var goBack : ImageView
    private lateinit var noDataFound_layout : ConstraintLayout

    lateinit var mView: View
    var apiRequest = ApiRequest()
    var webService = WebService()
    var token = String()
    private lateinit var teamMemberSearchView : SearchView
    lateinit var teamMembersListAdapter: TeamMembersListAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_team_members_list, container, false)
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        teamMembersListRecyclerView = mView.findViewById(R.id.team_members_list_rv)
        noDataFound_layout = mView.findViewById(R.id.noDataFound_layout)
        teamMembersListRecyclerView.layoutManager = LinearLayoutManager(teamMembersListRecyclerView.context)
        teamMembersListRecyclerView.setHasFixedSize(true)
        teamMembersListRecyclerView.isNestedScrollingEnabled = true
        teamMemberListModel = ArrayList()
        goBack = mView.findViewById(R.id.member_list_back_arrow)
        teamMemberSearchView = mView.findViewById(R.id.teamMemberSearchView)
        token = context?.let { activity?.getSharedPref("token", it) }.toString()
        TeamMembersListAsyncTask().execute()
        searchTeamMembers()
        goBack_Action()
        teamMembersListAdapter  = TeamMembersListAdapter(context, this,teamMemberListModel)
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private fun searchTeamMembers(){
        teamMemberSearchView.setOnQueryTextListener(object :androidx.appcompat.widget.SearchView.OnQueryTextListener, SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                if(newText.trim() == "")
                {
                    TeamMembersListAsyncTask().execute()
                }
                else{
                    teamMembersListAdapter.filter.filter(newText)
                }
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                teamMembersListAdapter.filter.filter(query)
                return false
            }


        })
    }

//    @OnTextChanged(R.id.feature_manager_search)
//    protected fun onTextChanged(text: CharSequence) {
//        filter(text.toString())
//    }

    private fun goBack_Action(){
        goBack.setOnClickListener(View.OnClickListener {
            fragmentManager?.popBackStack()
        })
    }

   private var progressHuD : KProgressHUD? = null
    inner class TeamMembersListAsyncTask : AsyncTask<String, String, String>() {

        override fun onPreExecute() {
            super.onPreExecute()
            progressHuD = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                //.setDetailsLabel("Downloading data")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .setBackgroundColor(57000000)
//                .show()
        }

        override fun doInBackground(vararg params: String?): String {
            val params = HashMap<String, String>()
            params.put("Authorization", "Bearer $token")
            context?.let {
                apiRequest.postRequestHeaders(
                    it,
                    webService.team_members_list_url,
                    params
                ) { response ->
                    var responseStatus = response.getString("status")
                    var msg = response.getString("msg")
                    if (responseStatus == "200") {
                        teamMemberListModel.clear()
                        teamMemberListModel = ArrayList()
                      var levelsdata = response.getJSONArray("levelsdata")
                        if (levelsdata.length() < 1){
                            try {
                                noDataFound_layout.visibility = View.VISIBLE
                                teamMembersListRecyclerView.visibility = View.VISIBLE
                            }catch (e: java.lang.Exception){
                                println(e)
                            }
                        }else{
                            try {
                                noDataFound_layout.visibility = View.GONE
                                teamMembersListRecyclerView.visibility = View.VISIBLE
                            }catch (e: java.lang.Exception){
                                println(e)
                            }
                        }
                        for (i in 0 until levelsdata.length())
                        {
                            var dataObj = levelsdata.getJSONObject(i)
                            var eid = dataObj.getString("id")
                            var name = dataObj.getString("name")
                            var role = dataObj.getString("role")
                            var email = dataObj.getString("email")
                            var phone = dataObj.getString("phone")
                            var profilePath = String()
                            if(dataObj.has("profilePath")) {
                                profilePath = dataObj.getString("profilePath")
                            }else{
                                profilePath=""
                            }
                            var imgWithBase = webService.imageBasePath+profilePath
                            var level_name = dataObj.getString("level_name")
                            var level_type = dataObj.getString("level_type")

                            teamMemberListModel.add(TeamMemberListModel(eid,name,role,level_name,level_type,imgWithBase,phone,email))
                            progressHuD?.dismiss()
                        }
                        progressHuD?.dismiss()
                    }else{
                        progressHuD?.dismiss()
                        activity?.alertDialog(context!!,"Alert",msg)
                    }
//                    setupTeamMembersListRecyclerView(teamMemberListModel)
                    teamMembersListAdapter  = TeamMembersListAdapter(context, this@TeamMembersListFragment,teamMemberListModel)
                    teamMembersListRecyclerView.adapter = teamMembersListAdapter

                }
            }

//            context?.let { activity?.progressBarDismiss(it) }
            return ""
        }

    }

    //    setup adapter
    private fun setupTeamMembersListRecyclerView(teamMemberListModel: ArrayList<TeamMemberListModel>) {
        teamMembersListAdapter  =
            TeamMembersListAdapter(
                context,this,
                teamMemberListModel
            )
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        teamMembersListRecyclerView.layoutManager = llm
        teamMembersListRecyclerView.adapter = teamMembersListAdapter

    }
    override fun onDestroy() {
        super.onDestroy()
        progressHuD?.dismiss()
    }

    override fun onDetach() {
        super.onDetach()
        progressHuD?.dismiss()
    }

    override fun showNoData() {
        try {
            noDataFound_layout.visibility = View.VISIBLE
            teamMembersListRecyclerView.visibility = View.GONE
        }catch (e: java.lang.Exception){
            println(e)
        }

    }

    override fun hideNoData() {
        try {
            noDataFound_layout.visibility = View.GONE
            teamMembersListRecyclerView.visibility = View.VISIBLE
        }catch (e: java.lang.Exception){
            println(e)
        }
    }
}
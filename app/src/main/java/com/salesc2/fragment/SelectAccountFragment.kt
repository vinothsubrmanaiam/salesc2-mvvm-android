package com.salesc2.fragment

import android.app.Dialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.adapters.SelectAccountAdapter
import com.salesc2.database.AppDatabase
import com.salesc2.database.SelectAccount
import com.salesc2.database.SelectService
import com.salesc2.model.SelectAccountModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.ShowNoData
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.isNetworkAvailable
import com.salesc2.utils.progressBarDismiss
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import kotlinx.coroutines.*
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


class SelectAccountFragment : Fragment(),ShowNoData {

    lateinit var accountSearch : SearchView
    lateinit var accountListRecyclerView: RecyclerView
    lateinit var selectAccountModel: ArrayList<SelectAccountModel>
    lateinit var selectAccountAdapter :  SelectAccountAdapter
    lateinit var goback : ImageView
    private lateinit var close : ImageView
    private lateinit var noDataFound_layout : ConstraintLayout
    var webService = WebService()
    var apiRequest = ApiRequest()
    var searchtext = String()
    private var db: AppDatabase? = null
    lateinit var mView: View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_select_account, container, false)
        accountSearch = mView.findViewById(R.id.selectAccount_SearchView)
        accountListRecyclerView = mView.findViewById(R.id.selectAccount_recyclerView)
        selectAccountModel = ArrayList()
        goback = mView.findViewById(R.id.select_account_back_arrow)
        close = mView.findViewById(R.id.close_iv)
        noDataFound_layout = mView.findViewById(R.id.noDataFound_layout)

        if ( requireContext().isNetworkAvailable(requireContext()))
        {
            lifecycleScope.launch {
                delay(100)
                SelectAccountAsyncTask().execute()
            }
        }
        else
        {
            showAccountData()

        }

        goBackAction()
        closeAction()
        searchAccount()
        accountSearch.clearFocus()
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private fun closeAction(){
        close.setOnClickListener{
            val dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.yes_or_no_alert_view)
            dialog.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
            val title : TextView = dialog.findViewById(R.id.alert_title)
            title.text = "Alert"
            val msg: TextView = dialog.findViewById(R.id.alert_msg)
            msg.text = "Do you want to cancel adding task?"
            val yes = dialog.findViewById<TextView>(R.id.alert_yes)
            yes.setOnClickListener(View.OnClickListener {
                context?.startActivity(Intent(context,MainActivity::class.java))
                dialog.dismiss()

            })
            val no = dialog.findViewById(R.id.alert_no) as TextView
            no.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }
    }
    fun goBackAction(){
        goback.setOnClickListener(View.OnClickListener {
           var edit_task = context?.getSharedPref("edit_task",context!!).toString()
            if (edit_task.equals("yes")) {
                fragmentManager?.popBackStack()
            }
            else{
                startActivity(Intent(context,MainActivity::class.java))
            }

        })
    }
    private fun searchAccount(){
//        accountSearch.setOnSearchClickListener(object : View.OnClickListener {
//            override fun onClick(v: View?) {
                accountSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextChange(newText: String): Boolean {
                        searchtext = newText
                        selectAccountModel = ArrayList()
                      //  SelectAccountAsyncTask().execute()
                        if (context?.isNetworkAvailable(context!!) == true){
                            selectAccountModel = ArrayList()
                              SelectAccountAsyncTask().execute()
                        }else{
                            if(newText.length!=0) {
                                if (newText?.length < 2) {
                                    showAccountData()
                                } else {
                                    selectAccountAdapter.filter.filter(newText)
                                }
                            }
                        }

                        return false
                    }

                    override fun onQueryTextSubmit(query: String): Boolean {
                        searchtext = query
                        selectAccountModel = ArrayList()
                       // SelectAccountAsyncTask().execute
                        if (context?.isNetworkAvailable(context!!) == true){
                            selectAccountModel = ArrayList()
                            SelectAccountAsyncTask().execute()
                        }
                        else{
                            selectAccountAdapter.filter.filter(query)
                        }

                        return false
                    }

                })
//            }
//        })


    }


    override fun onResume() {
        super.onResume()
        accountSearch.clearFocus()
//        selectAccountModel = ArrayList()
    }
    inner class SelectAccountAsyncTask(): AsyncTask<String,String,String>(){
        override fun onPreExecute() {
            selectAccountModel.clear()
            selectAccountModel = ArrayList()
//            if (selectAccountModel != null) {
//                selectAccountModel.clear()
//            } else {
//                selectAccountModel = ArrayList()
//            }
//            context?.progressBarShow(context!!)
        }
        override fun doInBackground(vararg params: String?): String {

            var params = HashMap<String, String>()
            params.put("search",searchtext)
            context?.let {
                apiRequest.postRequestBodyWithHeaders(it,webService.Select_account_list_url,params){ response->
                    selectAccountModel.clear()
                    var status = response.getString("status")
                    if (status == "200"){
                        var dataArray = response.getJSONArray("data")
                        if (dataArray.length() <=0){
                            noDataFound_layout.visibility = View.VISIBLE
                        }else{
                            noDataFound_layout.visibility = View.GONE
                        }
                        for (i in 0 until dataArray.length()){
                            var dataObj = dataArray.getJSONObject(i)
                            var _id = dataObj.getString("_id")
                            var name = dataObj.getString("name")
                            context?.progressBarDismiss(context!!)
                        selectAccountModel.add(SelectAccountModel(_id,name))
                        }
                        setupSelectAccountRecyclerView(selectAccountModel)
//                        searchAccount()
                    }else{
//                        searchAccount()
                        context?.progressBarDismiss(context!!)
                    }
                   /*if (selectAccountModel.size <=0){
                       noDataFound_layout.visibility = View.VISIBLE
                   }else{
                       noDataFound_layout.visibility = View.GONE
                   }*/
                }
            }
            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            context?.progressBarDismiss(context!!)
        }
    }

    //    setup adapter
    private fun setupSelectAccountRecyclerView(selectAccountModel: ArrayList<SelectAccountModel>) {
        println("account_id"+ " "+   "jasasdasvdsvda")
//        lifecycleScope.launch {
//            selectAccountModel = ArrayList()
//            delay(1500)
//            CoroutineScope(Dispatchers.IO).launch {
//
//
//
//                db = AppDatabase.getDatabaseClient(requireContext())
//                val accountData =    db!!.selectAccountDao().getAll()
//                for (n in 0 until accountData.size){
//
//                    println("account_id"+ " "+   accountData[n]._id)
//
//                    selectAccountModel.add(SelectAccountModel(accountData[n]._id!!,accountData[n].name!!))
//                }
//            }
           selectAccountAdapter = SelectAccountAdapter(context,this, selectAccountModel)
            val llm = LinearLayoutManager(context)
            llm.orientation = LinearLayoutManager.VERTICAL
            accountListRecyclerView.layoutManager = llm
            accountListRecyclerView.setHasFixedSize(true)
            accountListRecyclerView.adapter = selectAccountAdapter
        selectAccountAdapter.notifyDataSetChanged()
//        }

    }


    private fun showAccountData(){

        CoroutineScope(Dispatchers.Main).launch {
            selectAccountModel.clear()
            selectAccountModel = ArrayList()
            db = AppDatabase.getDatabaseClient(requireContext())
            val accountData = db!!.selectAccountNewDao().getAll()

            for (a in 0 until accountData.size){
                var id = accountData[a].account_id
                var name = accountData[a].name
                selectAccountModel.add(SelectAccountModel(id!!,name!!))
            }
            setupSelectAccountRecyclerView(selectAccountModel)
            Log.i("accountDataMain", accountData.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

    override fun showNoData() {
        noDataFound_layout.visibility = View.VISIBLE
    }

    override fun hideNoData() {
        noDataFound_layout.visibility = View.GONE
    }
}
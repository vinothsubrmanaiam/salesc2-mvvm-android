package com.salesc2.adapters

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.fragment.SelectDateTimeFragment
import com.salesc2.fragment.SelectServiceFragment
import com.salesc2.model.AccountlistModel
import com.salesc2.model.SelectServiceModel
import com.salesc2.utils.ShowNoData
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.setSharedPref
import java.util.*

class SelectServiceAdapter(context: Context?, val showdata: ShowNoData, var serviceList: ArrayList<SelectServiceModel>) : RecyclerView.Adapter<SelectServiceAdapter.viewHolder>() ,
    Filterable {

    var serviceItems= ArrayList<SelectServiceModel>()
    var service_filter_position = String()
    private  var row_index = -1
    var context: Context? = null
    var resultCount:Int = 0

    init {
        this.context = context
        serviceItems = serviceList
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_select_list, parent, false)
        service_filter_position = context?.getSharedPref("service_filter_position",context!!).toString()
        return viewHolder(view)
    }

    override fun getItemCount(): Int {
        return serviceItems.size
    }

    var checked = true
    override fun onBindViewHolder(holder: viewHolder, position: Int) {
       var selectServiceModel : SelectServiceModel = serviceItems[position]



        val cap = selectServiceModel.service_name.substring(0, 1).toUpperCase() + selectServiceModel.service_name.substring(1)
        holder.name.text=cap.toString()
        //holder.name.text = selectServiceModel.service_name
        service_filter_position = context?.getSharedPref("service_filter_position",context!!).toString()
       if (service_filter_position != "null"){
            row_index=service_filter_position.toInt()
            if (row_index.equals(service_filter_position)){
                holder.select_icon.visibility = View.VISIBLE
                context?.let { ContextCompat.getColor(it, R.color.offwhite2) }?.let {
                    holder.list_layout.setBackgroundColor(it)
                }
            }
        }


        holder.list_layout.setOnClickListener(View.OnClickListener {
            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("service_filter_position")
            editor.commit()
            if (checked) {
                row_index=position
                notifyDataSetChanged()
                checked = false
                holder.select_icon.visibility = View.VISIBLE
                context?.let { ContextCompat.getColor(it, R.color.offwhite2) }?.let {
                    holder.list_layout.setBackgroundColor(it)
                }
            }else{
                checked = true
            }

            context?.setSharedPref(context!!,"service_filter_id",selectServiceModel.service_id)
            context?.setSharedPref(context!!,"service_filter_name",selectServiceModel.service_name)

            var fragment = SelectDateTimeFragment()
            val activity = context as AppCompatActivity
            activity.supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragmet_container, fragment).addToBackStack(null).commit()

        })

        if(row_index==position){
            holder.select_icon.visibility = View.VISIBLE
            context?.setSharedPref(context!!,"service_filter_position",position.toString())

        }
        else
        {
            holder.select_icon.visibility = View.GONE
        }


    }
    class viewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView) {
        var name : TextView = itemView.findViewById(R.id.select_list_name)
        val list_layout : ConstraintLayout = itemView.findViewById(R.id.list_layout)
        var select_icon : ImageView = itemView.findViewById(R.id.select_icon)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    serviceItems = serviceList

                } else {
                    val resultList = ArrayList<SelectServiceModel>()
                    for (row in serviceList) {
                        if (row.service_name.toLowerCase(Locale.ROOT)
                                .contains(charSearch.toLowerCase(Locale.ROOT))
                        ) {

                            resultList.add(row)
                        }

                    }
                    serviceItems = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = serviceItems
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                serviceItems = results?.values as ArrayList<SelectServiceModel>

                if (serviceItems.size==0) {
                    showdata.showNoData()

                }
                else
                {
                    showdata.hideNoData()
                    notifyDataSetChanged()
                    resultCount = results.count.toInt()
                }

            }



        }
    }
}
package com.salesc2.data.network.api.request

import com.google.gson.annotations.SerializedName

data class RequestAddCardCreatePrice(
    @SerializedName("unit_amount") val unit_amount: String? = "",
    @SerializedName("billingCycle") val billingCycle: String? = "",
    @SerializedName("currency") val currency: String? = "",
    @SerializedName("productName") val productName: String? = ""
)

package com.salesc2.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TaskDao {
    @Query("SELECT * FROM Task")
    suspend fun getAll(): List<Task>

    @Insert
    suspend fun insertAll(vararg task: Task)

    @Query("DELETE FROM Task")
    fun deleteAll()

   /* @Query("SELECT * FROM Task WHERE task_id = task_id")
    fun getTaskDetails(task_id: String): List<Task>*/

    @Query("SELECT * FROM Task WHERE task_id = :item_name")
    fun getTaskDetails(item_name: String): List<Task>
}
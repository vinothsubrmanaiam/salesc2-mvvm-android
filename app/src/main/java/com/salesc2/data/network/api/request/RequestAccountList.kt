package com.salesc2.data.network.api.request

import com.google.gson.annotations.SerializedName

data class RequestAccountList(
    @SerializedName("search") val search: String? = ""
)

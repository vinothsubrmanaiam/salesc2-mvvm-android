package com.salesc2.data.network.api.service



import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseManageAccount
import com.salesc2.data.network.api.response.ResponseTeamList
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface ManageAccountApi {

    @POST("ios-user/api/v1/profile")
    suspend fun reqManageProfile(

    ): Response<ResponseManageAccount>
}
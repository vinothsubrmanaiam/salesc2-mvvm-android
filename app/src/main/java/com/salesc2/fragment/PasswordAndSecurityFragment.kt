package com.salesc2.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputEditText
import com.salesc2.R
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.loadFragment
import com.salesc2.utils.toast
import java.util.regex.Pattern

class PasswordAndSecurityFragment : Fragment() {

    private lateinit var oldPw : TextInputEditText
    private lateinit var newPw : TextInputEditText
    private lateinit var confirmPw :TextInputEditText

    private lateinit var ps_done_tv : TextView
    private lateinit var ps_back_arrow : ImageView

    private var apiRequest = ApiRequest()
    private var webService = WebService()
    private lateinit var mView: View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_password_security, container, false)
        oldPw = mView.findViewById(R.id.oldPw_TIET)
        newPw = mView.findViewById(R.id.newPw_TIET)
        confirmPw = mView.findViewById(R.id.confirmPw_TIET)
        ps_done_tv = mView.findViewById(R.id.ps_done_tv)
        ps_back_arrow = mView.findViewById(R.id.ps_back_arrow)
        doneAction()

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }
    private fun doneAction(){
        ps_done_tv.setOnClickListener{
            ps_done_tv.isEnabled = false
            validation()
        }

        ps_back_arrow.setOnClickListener{
            fragmentManager?.popBackStack()
        }
    }

    private fun validation(){
        if(oldPw.text.toString().isEmpty() || oldPw.text.toString().isBlank()){
            context?.toast("You should enter old password to continue")
            ps_done_tv.isEnabled = true
        }
        else if (newPw.text.toString().isEmpty() || newPw.text.toString().isBlank()){
            context?.toast("You should enter New password to continue")
            ps_done_tv.isEnabled = true
        }
        else if (newPw.text.toString().length < 8){
            context?.toast("New Password should contain minimum 8 character")
            ps_done_tv.isEnabled = true
        }
        else if (confirmPw.text.toString().isEmpty() || confirmPw.text.toString().isBlank()){
            context?.toast("You should enter confirm password to continue")
            ps_done_tv.isEnabled = true
        }
        else if (confirmPw.text.toString().length != newPw.text.toString().length){
            context?.toast("Confirm Password does not match with new password")
            ps_done_tv.isEnabled = true
        }
        else if (!isValidPasswordFormat(newPw.text.toString())){
            context?.toast("New password does not match constraints")
            ps_done_tv.isEnabled = true
        }
        else if (!isValidPasswordFormat(confirmPw.text.toString())){
            context?.toast("Confirm password does not match constraints")
            ps_done_tv.isEnabled = true
        }
        else{
                changePwApiCall()
        }

    }

    private fun changePwApiCall(){
        var old= oldPw.text.toString()
        var new = newPw.text.toString()
        var confirm = confirmPw.text.toString()
        val params = HashMap<String,Any>()
        params["oldPassword"] = old
        params["newPassword"] = new
        params["confirmPassword"] = confirm
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.change_password_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        context?.toast(msg)
                        oldPw.text?.clear()
                        newPw.text?.clear()
                        confirmPw.text?.clear()
                        context?.loadFragment(context!!,ManageAccountFragment())
                        ps_done_tv.isEnabled = true

                    }else{
                        context?.toast(msg)
                        ps_done_tv.isEnabled = true
                    }
                }catch (e:Exception){
                    context?.toast(e.toString())
                    ps_done_tv.isEnabled = true
                }
            }
        }
    }


  /*
   ^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\S+$).{4,}$
   ^                 # start-of-string
    (?=.*[0-9])       # a digit must occur at least once
    (?=.*[a-z])       # a lower case letter must occur at least once
    (?=.*[A-Z])       # an upper case letter must occur at least once
    (?=.*[@#$%^&+=])  # a special character must occur at least once you can replace with your special characters
    (?=\\S+$)          # no whitespace allowed in the entire string
    .{4,}             # anything, at least six places though
    $                 # end-of-string*/

    fun isValidPasswordFormat(password: String): Boolean {
        val passwordREGEX = Pattern.compile("^" +
                "(?=.*[0-9])" +         //at least 1 digit
                "(?=.*[a-z])" +         //at least 1 lower case letter
                "(?=.*[A-Z])" +         //at least 1 upper case letter
                "(?=.*[a-zA-Z])" +      //any letter
//                "(?=.*[@#$%^&+=])" +    //at least 1 special character
                "(?=\\S+$)" +           //no white spaces
                ".{8,}" +               //at least 8 characters
                "$");
        return passwordREGEX.matcher(password).matches()
    }

}

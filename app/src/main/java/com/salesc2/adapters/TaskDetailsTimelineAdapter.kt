package com.salesc2.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SimpleAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.TaskDetailsTimelineModel
import com.salesc2.model.TaskListModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.dateFromUTC
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TaskDetailsTimelineAdapter() : RecyclerView.Adapter<TaskDetailsTimelineAdapter.MyViewHolder>() {

    private lateinit var timelineItems: ArrayList<TaskDetailsTimelineModel>
    var context: Context? = null
    var webService = WebService()
    var apiRequest = ApiRequest()

    private var note = String()
    constructor(context: Context?, timelineItems: ArrayList<TaskDetailsTimelineModel>) : this(){
        this.context = context
        this.timelineItems = timelineItems

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.adapter_task_details_timeline, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
     return timelineItems.size
    }



    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
      var taskDetailsTimelineModel : TaskDetailsTimelineModel = timelineItems[position]
        try {

           note= taskDetailsTimelineModel.description
            holder.timeline_createdBy.text = taskDetailsTimelineModel.createdBy

            var assignedList = getStatusFromNumber(taskDetailsTimelineModel.type)

            if (taskDetailsTimelineModel.type == "1") {
                holder.task_assign_to.visibility= View.VISIBLE
                holder.timeline_assignedTo.text =
                    "${taskDetailsTimelineModel.assignedTo}"
            } else {
                holder.timeline_assignedTo.text = assignedList
                holder.task_assign_to.visibility= View.GONE
            }

            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm")
            val myFormat = SimpleDateFormat("MM/dd/yyyy hh:mm a")
            var parseDate = sdf.parse(taskDetailsTimelineModel.createdAt)
            val parseDateUTC = context?.dateFromUTC(context!!,parseDate)
            var dt = myFormat.format(parseDateUTC)
            holder.timeline_dateTime.text = dt

            if (holder.timeline_desc.text == "" || holder.timeline_desc.text == " "||holder.timeline_desc.text == null) {
                holder.timeline_desc.visibility = View.GONE
            } else {
                holder.timeline_desc.visibility = View.VISIBLE
            }
            val typeColor = getColorFromType(taskDetailsTimelineModel.type)
            holder.colorCode_tv.setBackgroundColor(Color.parseColor(typeColor))
            holder.timeline_desc.text = "${"${taskDetailsTimelineModel.description}"}"

        }catch (e:Exception){
            print(e)
        }
    }

    private fun getColorFromType(status : String) : String {
        if (status == "0") {
            return "#617FFF"
        }
        else if (status == "1") {
            return "#617FFF"
        }
        else if (status == "2") {
            return  "#21BF73"
        }
        else if (status == "3") {
            return "#69F3B1"
        }
        else if (status == "4") {
            return  "#21BF73"
        }
        else if (status == "5") {
            return  "#D24856"
        }
        else if (status == "6") {
            return  "#D24856"
        }
        else if (status == "7") {
            return "#617FFF"
        }
        else if (status == "8") {
            return  "#617FFF"
        }
        else if (status == "9") {
            return "#888888"
        }
        else if (status == "10") {
            return "#617FFF"
        }
        else if (status == "11") {
            return "#617FFF"
        }
        return  "#617FFF"
    }

    private fun getStatusFromNumber(status : String) : String {
        if (status == "0"){
         return "Task Created"
        }
        else if (status == "2") {
            return "Accepted the assignment"
        }
        else if (status == "3"){
            return "Started the task"
        }
        else if (status == "4"){
            return "Completed the task"
        }
        else if (status == "5") {
            return "Rejected the task assignment"
        }
        else if (status == "6") {
            return "Uncovered"
        }
        else if (status == "7") {
            return note
        }
        else if (status == "8") {
            return "Task unassigned"
        }
        else if (status == "9") {
            return "Cancelled"
        }
        else if (status == "10") {
            return "Rescheduled"
        }
        else if(status == "11"){
            return "Updated task"
        }
        return ""
    }
    class MyViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView){
        var timeline_createdBy : TextView = itemView.findViewById(R.id.taskDetails_timeline_createdBy)
        var timeline_dateTime : TextView = itemView.findViewById(R.id.taskDetails_timeline_dateTime)
        var timeline_assignedTo : TextView= itemView.findViewById(R.id.taskDetails_timeline_assignedTo)
        var timeline_desc : TextView = itemView.findViewById(R.id.taskDetails_timeline_desc)
        var colorCode_tv : TextView = itemView.findViewById(R.id.colorCode_tv)
        var task_assign_to : TextView = itemView.findViewById(R.id.task_assign_to)

    }

}
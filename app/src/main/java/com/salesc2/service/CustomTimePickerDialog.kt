package com.salesc2.service

import android.app.TimePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.widget.NumberPicker
import android.widget.TimePicker

open class CustomTimePickerDialog(context: Context, listener: OnTimeSetListener,
                                  hourOfDay:Int, minute:Int, is24HourView:Boolean):TimePickerDialog(context, TimePickerDialog.THEME_HOLO_LIGHT, null, hourOfDay, minute / TIME_PICKER_INTERVAL, is24HourView) {
    private var mTimePicker: TimePicker? = null
    private val mTimeSetListener:OnTimeSetListener
    init{
        mTimeSetListener = listener
    }
    override fun updateTime(hourOfDay:Int, minuteOfHour:Int) {
        mTimePicker?.currentHour = hourOfDay
        mTimePicker?.currentMinute = minuteOfHour / TIME_PICKER_INTERVAL
    }
    override fun onClick(dialog: DialogInterface, which:Int) {
        when (which) {
            BUTTON_POSITIVE -> if (mTimeSetListener != null)
            {
                mTimeSetListener.onTimeSet(mTimePicker, mTimePicker?.currentHour!!,
                    mTimePicker?.currentMinute!! * TIME_PICKER_INTERVAL)
            }
            BUTTON_NEGATIVE -> cancel()
        }
    }
    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        try
        {
            val classForid = Class.forName("com.android.internal.R\$id")
            val timePickerField = classForid.getField("timePicker")
            mTimePicker = findViewById(timePickerField.getInt(null)) as TimePicker
            val field = classForid.getField("minute")
            val minuteSpinner = mTimePicker!!
                .findViewById(field.getInt(null)) as NumberPicker
            minuteSpinner.setMinValue(0)
            minuteSpinner.setMaxValue((60 / TIME_PICKER_INTERVAL) - 1)
            var displayedValues = ArrayList<String>()
            var i = 0
            while (i < 60)
            {
                displayedValues.add(String.format("%02d", i))
                i += TIME_PICKER_INTERVAL
            }
            minuteSpinner.setDisplayedValues(displayedValues
                .toArray(arrayOfNulls<String>(displayedValues.size)))
        }
        catch (e:Exception) {
            e.printStackTrace()
        }
    }
    companion object {
        private val TIME_PICKER_INTERVAL = 15
    }
}
package net.simplifiedcoding.imageuploader

data class UploadResponse(
    val profilePath: String,
    val msg: String,
    val status: String
)
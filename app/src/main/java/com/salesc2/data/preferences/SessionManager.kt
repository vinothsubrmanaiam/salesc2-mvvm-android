package com.salesc2.data.preferences

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences

class SessionManager {

companion object
{
    fun saveSession(key: String?, value: String?, context: Context?) {
        if (context != null) {
            val editor = context.getSharedPreferences("KEY", Activity.MODE_PRIVATE).edit()
            editor.putString(key, value)
            editor.commit()
        }
    }

    fun getSession(key: String?, context: Context?): String? {
        return if (context != null) {
            val prefs = context.getSharedPreferences("KEY", Activity.MODE_PRIVATE)
            prefs.getString(key, "")
        } else {
            ""
        }
    }
}

}
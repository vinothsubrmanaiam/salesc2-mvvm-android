package com.salesc2.adapters

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.BillingCardListModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import java.text.SimpleDateFormat
import java.util.ArrayList

class BillingCardListAdapter(
    val context: Context,
    var billingCardListItems: ArrayList<BillingCardListModel>,var hideShowCardSelection: HideShowCardSelection)
    :RecyclerView.Adapter<BillingCardListAdapter.MyViewHolder>() {

    var apiRequest = ApiRequest()
    var webService = WebService()
    var billingId = String()
    var mBottomSheetDialog : Dialog? = null
    var row_index = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder{
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_card_list, parent, false)
        billingId = context?.getSharedPref("billingId",context!!).toString()

        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
       return billingCardListItems.size
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       var billingCardListModel: BillingCardListModel = billingCardListItems[position]
        try {
           holder.cardList_cardNumber.text = "XXXX      XXXX      XXXX      ${billingCardListModel.last4}"
            holder.cardList_cardHolder.text = billingCardListModel.name
            holder.cardList_expireDate.text = "${billingCardListModel.exp_month}/${billingCardListModel.exp_year}"
        }catch (e:Exception){

        }
        var  paymentMethodId = context?.getSharedPref("paymentMethodId",context!!).toString()


        holder.cardList_cardRemove.setOnClickListener{
            val dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.yes_or_no_alert_view)
            dialog.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
            val title : TextView = dialog.findViewById(R.id.alert_title)
            title.text = "Alert"
            val msg: TextView = dialog.findViewById(R.id.alert_msg)
            msg.text = "Do you want to remove card?"
            val yes = dialog.findViewById<TextView>(R.id.alert_yes)
            yes.setOnClickListener(View.OnClickListener {
                billingId = context?.getSharedPref("billingId",context!!).toString()
                val params = HashMap<String,Any>()
                params["billingId"] = billingId
                params["paymentMethodId"] = billingCardListModel.paymentMethodId
                context?.let { it1 ->
                    apiRequest.postRequestBodyWithHeadersAny(it1,webService.delete_customer_card_url,params){ response->
                        try {
                            var status = response.getString("status")
                            var msg = response.getString("msg")
                            if (status == "200"){
                                context?.toast(msg)
                                hideShowCardSelection.refreshCardList()
//                                (context as MainActivity).dismissBillingCardListPopUp(context as MainActivity)
                            }else{
                                context?.toast(msg)
                            }
                        }catch (e:Exception){

                        }

                    }
                }
                dialog.dismiss()

            })
            val no = dialog.findViewById(R.id.alert_no) as TextView
            no.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()

        }

        holder.cardList_cardEdit.setOnClickListener{
            context?.setSharedPref(context!!,"lastdigit_card",billingCardListModel.last4)
            context?.setSharedPref(context!!,"cardholder_card",billingCardListModel.name)
            context?.setSharedPref(context!!,"expire_card","${billingCardListModel.exp_month}/${billingCardListModel.exp_year}")
            context?.setSharedPref(context!!,"paymentMethodId",billingCardListModel.paymentMethodId)

            mBottomSheetDialog  =
                context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) };
            mBottomSheetDialog?.setContentView(R.layout.add_edit_card_layout)
            var nameOnCard : EditText? = mBottomSheetDialog?.findViewById(R.id.nameOnCard)
            var card_number : EditText? = mBottomSheetDialog?.findViewById(R.id.card_number)
            var Expire_date : EditText? = mBottomSheetDialog?.findViewById(R.id.Expire_date)
            var cvv : EditText? = mBottomSheetDialog?.findViewById(R.id.cvv)
            var confirmCard : Button? = mBottomSheetDialog?.findViewById(R.id.confirmCard)
            var edit_card_close : ImageView? = mBottomSheetDialog?.findViewById(R.id.edit_card_close)

            card_number?.setText("XXXX XXXX XXXX ${billingCardListModel.last4}")
            card_number?.isFocusable = false
            card_number?.isEnabled = false
            cvv?.setText("XXX")
            cvv?.isFocusable = false
            cvv?.isEnabled = false

            nameOnCard?.setText(billingCardListModel.name)
            Expire_date?.setText("${billingCardListModel.exp_month}/${billingCardListModel.exp_year}")

            var lastInput = ""
            Expire_date?.addTextChangedListener(object : TextWatcher {
                @SuppressLint("SetTextI18n")
                override fun afterTextChanged(s: Editable) {
                    try {
                        val input = s.toString()
                        if (s.length == 2 && !lastInput.endsWith("/"))
                        {
                            val month = Integer.parseInt(input)
                            if (month <= 12)
                            {
                                Expire_date?.setText(Expire_date.text.toString() + "/")
                                Expire_date?.clearFocus()
                                Expire_date?.requestFocus()
                                Expire_date?.post(Runnable {
                                    Expire_date?.setSelection(
                                        Expire_date?.getText().length
                                    )
                                })
                            }else{
                                context?.toast("Enter a valid month")
                            }
                            Expire_date?.clearFocus()
                            Expire_date?.requestFocus()
                        }
                        else if (s.length == 2 && lastInput.endsWith("/"))
                        {
                            val month = Integer.parseInt(input)
                            if (month <= 12)
                            {
                                Expire_date.setText(Expire_date.text.toString().substring(0, 1))
                                Expire_date.clearFocus()
                                Expire_date.post(Runnable {
                                    Expire_date.setSelection(
                                        Expire_date.getText().length
                                    )
                                })

                            }
                            else{
                                context?.toast("Enter a valid month")
                            }
                            Expire_date.clearFocus()
                            Expire_date.requestFocus()
                            Expire_date.post(Runnable {
                                Expire_date.setSelection(
                                    Expire_date.getText().length
                                )
                            })
                        }
                        lastInput = Expire_date.text.toString()
                    }catch (e:Exception){
                        print(e)
                    }

                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(p0: CharSequence?, start: Int, removed: Int, added: Int) {
                    /* if (start == 1 && start+added == 2 && p0?.contains('/') == false) {
                         Expire_date.setText(p0.toString() + "/")
                     } else if (start == 3 && start-removed == 2 && p0?.contains('/') == true) {
                         Expire_date.setText(p0.toString().replace("/", ""))
                     }*/
                }

            })

            edit_card_close?.setOnClickListener{
                mBottomSheetDialog?.dismiss()
            }

            confirmCard?.setOnClickListener{
                if (nameOnCard?.text.toString() == "" ||nameOnCard?.text.toString().trim().isBlank() ){
                    context?.toast("Please enter card holder name")
                }else if (Expire_date?.text.toString().trim().isBlank()){
                    context?.toast("Please enter the expiry date")
                }else{
                    try {
                        var sdf = SimpleDateFormat("MM/yyyy")
                        var monthF = SimpleDateFormat("MM")
                        var yrF = SimpleDateFormat("yyyy")
                        var parse =
                            sdf.parse(Expire_date?.text.toString())
                        var  expireMonthCard = monthF.format(parse)
                        var expireYrCard = yrF.format(parse)
                        editCardApiCall(billingId,nameOnCard?.text.toString(),expireMonthCard,expireYrCard,billingCardListModel.paymentMethodId)
                    }catch (e:Exception){

                    }
                }


            }

            mBottomSheetDialog?.setCancelable(true)
            mBottomSheetDialog?.window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT)
            mBottomSheetDialog?.window?.setGravity(Gravity.CENTER)
            mBottomSheetDialog?.show()
        }

        holder.selectCard.setOnClickListener{
            row_index=position
            notifyDataSetChanged()
            holder.selectCard.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_blue_circle))
            context?.setSharedPref(context!!,"lastdigit_card",billingCardListModel.last4)
            context?.setSharedPref(context!!,"cardholder_card",billingCardListModel.name)
            context?.setSharedPref(context!!,"expire_card","${billingCardListModel.exp_month}/${billingCardListModel.exp_year}")
            context?.setSharedPref(context!!,"paymentMethodId",billingCardListModel.paymentMethodId)

        }
        if(row_index==position){
            holder.selectCard.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_blue_circle))
            hideShowCardSelection.hideSelection()
        }
        else
        {
            holder.selectCard.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_grey_circle))
        }
        if (paymentMethodId == billingCardListModel.paymentMethodId){
            if (billingCardListModel.selection == "yes"){
                holder.selectCard.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_blue_circle))
                hideShowCardSelection.hideSelection()
            }
            context?.setSharedPref(context!!,"lastdigit_card",billingCardListModel.last4)
            context?.setSharedPref(context!!,"cardholder_card",billingCardListModel.name)
            context?.setSharedPref(context!!,"expire_card","${billingCardListModel.exp_month}/${billingCardListModel.exp_year}")
            context?.setSharedPref(context!!,"paymentMethodId",billingCardListModel.paymentMethodId)
        }


    }

    private fun editCardApiCall(billingId:String,cardHolder:String,exp_month:String,exp_year:String,paymentMethodId:String){
        val params = HashMap<String,Any>()
        params["billingId"] = billingId
        params["name"] = cardHolder
        params["exp_month"] = exp_month.toInt()
        params["exp_year"] = exp_year.toInt()
        params["paymentMethodId"] = paymentMethodId

        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.edit_card_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        context?.toast(msg)
                        context?.setSharedPref(context!!,"editCard","yes")
//                        context?.loadFragment(context!!, BillingDetailsFragment())
                        hideShowCardSelection.refreshCardList()
                        mBottomSheetDialog?.dismiss()
                    }else{
                        context?.toast(msg)
                    }
                }catch (e:Exception){

                }
            }
        }

    }


    class MyViewHolder constructor(itemView:View):RecyclerView.ViewHolder(itemView) {
      var selectCard :ImageView = itemView.findViewById(R.id.selectCard)
        var cardList_cardRemove : TextView = itemView.findViewById(R.id.cardList_cardRemove)
        var cardList_cardEdit : TextView = itemView.findViewById(R.id.cardList_cardEdit)
        var cardList_cardNumber : TextView = itemView.findViewById(R.id.cardList_cardNumber)
        var cardList_cardHolder : TextView = itemView.findViewById(R.id.cardList_cardHolder)
        var cardList_expireDate : TextView = itemView.findViewById(R.id.cardList_expireDate)

    }




}
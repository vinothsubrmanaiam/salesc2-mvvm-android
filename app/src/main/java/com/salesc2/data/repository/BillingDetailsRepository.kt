package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestBillingDetails
import com.salesc2.data.network.api.request.RequestMorePage
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseBillingCompanySubdetails
import com.salesc2.data.network.api.response.ResponseBillingDetails
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.api.response.ResponseMorePage
import com.salesc2.data.network.api.service.BillingDetailsApi
import com.salesc2.data.network.api.service.MorePageApi
import com.salesc2.data.network.api.service.UserLoginApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface BillingDetailsRepository {

    suspend fun reqBillingDetails(
        requestBillingDetails: RequestBillingDetails,
        onSuccess: OnSuccess<ResponseBillingDetails>,
        onFailure: OnFailure<ResponseBillingDetails>
    )

    suspend fun reqBillingCompanySubDetails(
        requestBillingDetails: RequestBillingDetails,
        onSuccess: OnSuccess<ResponseBillingCompanySubdetails>,
        onFailure: OnFailure<ResponseBillingCompanySubdetails>
    )


    companion object Factory {
        fun create(api: BillingDetailsApi): BillingDetailsRepository = BillingDetailsRepositoryImpl(api)
    }
}

private class BillingDetailsRepositoryImpl(private val api: BillingDetailsApi) : BillingDetailsRepository {
    override suspend fun reqBillingDetails(
        requestBillingDetails: RequestBillingDetails,
        onSuccess: OnSuccess<ResponseBillingDetails>,
        onFailure: OnFailure<ResponseBillingDetails>
    ) {
        withContext(Dispatchers.IO) {
            try {
                val response = api.reqBillingDetails(requestBillingDetails = requestBillingDetails)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status.equals("204")) {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }

    }

    override suspend fun reqBillingCompanySubDetails(
        requestBillingDetails: RequestBillingDetails,
        onSuccess: OnSuccess<ResponseBillingCompanySubdetails>,
        onFailure: OnFailure<ResponseBillingCompanySubdetails>
    ) {


        withContext(Dispatchers.IO) {
            try {
                val response = api.reqBillingCompanySubDetails(requestBillingDetails = requestBillingDetails)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status.equals("204")) {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }
    }

}

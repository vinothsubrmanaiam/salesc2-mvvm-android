package com.salesc2.data.network.api.response

data class ResponseEmployeeLogin(
    val `data`: Data,
    val msg: String,
    val status: String,
    val token: String
) {
    data class Data(
        val companyId: String,
        val email: String,
        val role: String,
        val username: String,
        val userId: String,
        val profilePath: String
        )
}
package com.salesc2.fragment

import android.app.Dialog
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.R
import com.salesc2.adapters.BillingCardListAdapter
import com.salesc2.adapters.BillingEmailsAdapter
import com.salesc2.adapters.BillingHistoryAdapter
import com.salesc2.data.network.api.request.RequestBillingOverView
import com.salesc2.data.viewmodel.BillingoverViewViewModel
import com.salesc2.data.viewmodel.MorePageViewModel
import com.salesc2.model.BillingCardListModel
import com.salesc2.model.BillingEmailsModel
import com.salesc2.model.BillingHistoryModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class BillingOverviewFragment : Fragment(),HideShowCardSelection{
    var hideShowRecyclerListCardSelection : HideShowRecyclerListCardSelection? = null
    private lateinit var BO_heading : TextView
    private lateinit var BO_viewEstimate : TextView
    private lateinit var BO_estimate_price : TextView
    private lateinit var BO_charge_date : TextView
    private lateinit var BO_viewBillDetails : TextView
    private lateinit var card_type : TextView
    private lateinit var card_num_dots : TextView
    private lateinit var cardHolder_name : TextView
    private lateinit var cardExpire_date : TextView
    private lateinit var billingPeriod : TextView
    private lateinit var BO_viewFullHistory : TextView
    private lateinit var billHistory_recyclerView : RecyclerView
    private lateinit var BO_manage_subscription : TextView
    private lateinit var contactImage : CircleImageView
    private lateinit var contactName : TextView
    private lateinit var contactEmail : TextView
    private lateinit var make_billing_contact : Button
    private lateinit var manage_contact : Button
    private lateinit var emails_recyclerView : RecyclerView
    private lateinit var billing_overview_back_arrow : ImageView
    private lateinit var noHistory : TextView
    private lateinit var sendCopyTV : TextView
    private lateinit var BO_image : CircleImageView

    private var subscriptionId = String()
    private var fromPage = String()
    private var billingId  = String()

    var billingEmailsModel = ArrayList<BillingEmailsModel>()
    var billingHistoryModel = ArrayList<BillingHistoryModel>()
    private var billingCardListModel = ArrayList<BillingCardListModel>()
    var apiRequest = ApiRequest()
    var webService = WebService()
    var showCardList = String()
    private val billingoverViewViewModel by viewModel<BillingoverViewViewModel>()

    private lateinit var mView : View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_billing_overview, container, false)
        subscriptionId = context?.getSharedPref("subscriptionId",context!!).toString()
        fromPage = context?.getSharedPref("fromPage",context!!).toString()
        billingId = context?.getSharedPref("billingId",context!!).toString()
        showCardList = context?.getSharedPref("showCardList",context!!).toString()
        BO_heading = mView.findViewById(R.id.BO_heading)
        BO_viewEstimate = mView.findViewById(R.id.BO_viewEstimate)
        BO_estimate_price = mView.findViewById(R.id.BO_estimate_price)
        BO_charge_date = mView.findViewById(R.id.BO_charge_date)
        BO_viewBillDetails = mView.findViewById(R.id.BO_viewBillDetails)
        card_type = mView.findViewById(R.id.card_type)
        card_num_dots = mView.findViewById(R.id.card_num_dots)
        cardHolder_name = mView.findViewById(R.id.cardHolder_name)
        cardExpire_date = mView.findViewById(R.id.cardExpire_date)
        billingPeriod = mView.findViewById(R.id.billingPeriod)
        BO_viewFullHistory = mView.findViewById(R.id.BO_viewFullHistory)
        billHistory_recyclerView = mView.findViewById(R.id.billHistory_recyclerView)
        BO_manage_subscription = mView.findViewById(R.id.BO_manage_subscription)
        contactImage = mView.findViewById(R.id.contactImage)
        contactName = mView.findViewById(R.id.contactName)
        contactEmail = mView.findViewById(R.id.contactEmail)
        make_billing_contact = mView.findViewById(R.id.make_billing_contact)
        manage_contact = mView.findViewById(R.id.manage_contact)
        emails_recyclerView = mView.findViewById(R.id.emails_recyclerView)
        billing_overview_back_arrow = mView.findViewById(R.id.billing_overview_back_arrow)
        noHistory = mView.findViewById(R.id.noHistory)
        sendCopyTV = mView.findViewById(R.id.sendCopyTV)
        BO_image = mView.findViewById(R.id.BO_image)
        billingEmailsModel = ArrayList()
        billingHistoryModel = ArrayList()
        billingCardListModel = ArrayList()
        if (fromPage == "DashBoard"){
            if (showCardList == "yes"){
                context?.setSharedPref(context!!,"showCardList","no")
                billingCardListingPopUp(billingId)
            }else{
                billingStatusApiCall()
            }
        }

        billingOverViewApiCall()
        if (fromPage != "DashBoard"){
            onClickActions()
        }

        companySubDetailsApiCall()
        companySubDetailsApiCallwithMVVM()
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private fun billingStatusApiCall(){
        val params = HashMap<String,Any>()
        context?.let {
            apiRequest.getRequestBodyWithHeaders(it,webService.billing_status_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){
                        var billStatus = response.getString("billStatus")
                        if (billStatus == "1"){
                            billingStatusPopUp("You are not subscribed to SalesC2. Please enable subscription to continue accessing the modules.",1)
                        }else if (billStatus == "2"){
                            billingStatusPopUp("Your payment for subscription is failed. Kindly add another card and retry payment.",2)
                        }else if (billStatus == "3"){
                            billingStatusPopUp("Payment is under process...",3)
                        }

                    }
                }catch (e: java.lang.Exception){
                    println(e)
                }
            }
        }
    }
    var mBottomSheetDialog : Dialog ? = null
    private fun billingStatusPopUp(Desc:String,status:Int){
        mBottomSheetDialog =
            context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) }
        mBottomSheetDialog?.setContentView(R.layout.popup_billing_status)
        var bDesc = mBottomSheetDialog?.findViewById<TextView>(R.id.bDesc)
        var viewBillingPage = mBottomSheetDialog?.findViewById<TextView>(R.id.viewBillingPage)
        var bClose: ImageView? = mBottomSheetDialog?.findViewById(R.id.bClose)

        bDesc?.text = Desc

        if (status == 1 || status == 2){
            viewBillingPage?.setText("Subscribe Now")
        }else if (status == 3){
            viewBillingPage?.setText("OK")
        }
        viewBillingPage?.setOnClickListener{
            if (status == 3){
                mBottomSheetDialog?.dismiss()
            }else{
                mBottomSheetDialog?.dismiss()
//                (context as MainActivity).billingCardListingPopUp(context as MainActivity,billingId)
                billingCardListingPopUp(billingId)
            }
        }

        bClose?.setOnClickListener{
            mBottomSheetDialog?.dismiss()
        }

        mBottomSheetDialog?.setCancelable(true)
        mBottomSheetDialog?.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT)
        mBottomSheetDialog?.window?.setGravity(Gravity.CENTER)
        mBottomSheetDialog?.show()
    }



    private var lastdigit_card = String()
    private var cardholder_card = String()
    private var expire_card = String()
    private var paymentMethodId = String()
    var cardList_recyclerView : RecyclerView? = null
    private var selectAddCard : ImageView? = null

    private fun billingCardListingPopUp(billingId: String){
        fromPage = context?.getSharedPref("fromPage",context!!).toString()
        billingCardListModel = ArrayList()
        mBottomSheetDialog =
            context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) };
        mBottomSheetDialog?.setContentView(R.layout.popup_card_list)
        cardList_recyclerView = mBottomSheetDialog?.findViewById<RecyclerView>(R.id.cardList_recyclerView)
        var cardList_continue = mBottomSheetDialog?.findViewById<TextView>(R.id.cardList_continue)
        var cardListClose: ImageView? = mBottomSheetDialog?.findViewById(R.id.cardListClose)
         selectAddCard   = mBottomSheetDialog?.findViewById(R.id.selectAddCard)!!
        getCardListApiCall(billingId,"yes")
        cardListClose?.setOnClickListener{
            mBottomSheetDialog?.dismiss()
        }
        selectAddCard?.setOnClickListener{
            getCardListApiCall(billingId,"no")
            selectAddCard?.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_blue_circle))
            context?.setSharedPref(context!!,"addCard","yes")
            hideShowRecyclerListCardSelection?.hideRecyclerSelection()
        }
        cardList_continue?.setOnClickListener{
            var  addCard = context?.getSharedPref("addCard",context!!).toString()

            if (addCard == "yes"){

                context?.setSharedPref(context!!,"bcFrom","BillingOverview")
                context?.loadFragment(context!!,AddCardFragment())
                mBottomSheetDialog?.dismiss()

            }else{
                if (fromPage == "DashBoard"){
                    reSubscribeApiCall()
                    mBottomSheetDialog?.dismiss()
                }else{
                    lastdigit_card = context?.getSharedPref("lastdigit_card",context!!).toString()
                    cardholder_card = context?.getSharedPref("cardholder_card",context!!).toString()
                    expire_card = context?.getSharedPref("expire_card",context!!).toString()
                    paymentMethodId = context?.getSharedPref("paymentMethodId",context!!).toString()
                    updateCardDetailsApiCall(billingId,cardholder_card,expire_card,lastdigit_card,paymentMethodId)
                    mBottomSheetDialog?.dismiss()
                }
            }


        }
        mBottomSheetDialog?.setCancelable(true)
        mBottomSheetDialog?.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT)
        mBottomSheetDialog?.window?.setGravity(Gravity.CENTER)
        mBottomSheetDialog?.show()
    }

    private fun getCardListApiCall(billingId: String,selection:String){
        val params = HashMap<String,Any>()
        params["billingId"] = billingId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.get_customer_card_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){
                        billingCardListModel.clear()
                        billingCardListModel = ArrayList()
                        var cardDetailsArr = response.getJSONArray("cardDetails")

                        for (i in 0 until cardDetailsArr.length()){
                            var cardDetailsObj = cardDetailsArr.getJSONObject(i)
                            var paymentMethodId = cardDetailsObj.getString("paymentMethodId")
                            var exp_year = cardDetailsObj.getString("exp_year")
                            var exp_month = cardDetailsObj.getString("exp_month")
                            var last4 = cardDetailsObj.getString("last4")
                            var name = cardDetailsObj.getString("name")
                            var brand = cardDetailsObj.getString("brand")
                            billingCardListModel.add(BillingCardListModel(paymentMethodId, exp_year, exp_month, last4, name, brand,selection))
                        }
                    }
                    setupBillingCardListRecyclerView(billingCardListModel)

                }catch (e:Exception){
                    println(e)
                }

            }
        }

    }
    private fun setupBillingCardListRecyclerView(billingCardListModel: ArrayList<BillingCardListModel>) {
        val adapter: RecyclerView.Adapter<*> = BillingCardListAdapter(context!!,billingCardListModel,this@BillingOverviewFragment)
        val llm = LinearLayoutManager(context!!)
        llm.orientation = LinearLayoutManager.VERTICAL
        cardList_recyclerView?.layoutManager = llm
        cardList_recyclerView?.adapter = adapter
    }

    private fun updateCardDetailsApiCall(billingId:String,cardHolder:String,expire:String,lastDigit:String,paymentMethodId:String){
        val params = HashMap<String,Any>()
        params["billingId"] = billingId
        params["cardHolder"] = cardHolder
        params["expire"] = expire
        params["lastDigit"] = lastDigit.toInt()
        params["paymentMethodId"] = paymentMethodId
        context?.let {
            apiRequest.putRequestBodyWithHeadersAny(it,webService.card_details_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        context?.toast(msg)
                        context?.loadFragment(context!!,BillingDetailsFragment())
                    }else{
                        context?.toast(msg)
                    }

                }catch (e:Exception){
                    println(e)
                }
            }
        }
    }

    private fun reSubscribeApiCall(){
        paymentMethodId = context?.getSharedPref("paymentMethodId",context!!).toString()
        var params = HashMap<String,Any>()
        params["type"] = 1
        params["paymentMethodId"] = paymentMethodId
        apiRequest.postRequestBodyWithHeadersAny(context!!,webService.re_subscribe_url,params){ response->
            try{
                var status = response.getString("status")
                var msg = response.getString("msg")
                if (status == "200"){
                    context?.toast(msg)
                    activity?.findViewById<RelativeLayout>(R.id.coordinatorlayout)?.visibility = View.VISIBLE
                    onClickActions()
                    val preferences: SharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context)
                    val editor: SharedPreferences.Editor = preferences.edit()
                    editor.remove("fromPage")
                    editor.commit()

                }
                else{
                    context?.toast(msg)
                }
            }catch (e:Exception){

            }
        }
    }



    override fun showSelection() {
        selectAddCard?.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_blue_circle))
    }

    override fun hideSelection() {
        selectAddCard?.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_grey_circle))
    }

    override fun refreshCardList() {
        getCardListApiCall(billingId,"yes")
    }


fun companySubDetailsApiCallwithMVVM(){
    billingoverViewViewModel.reqGetSubscription(RequestBillingOverView(
        subscriptionId = subscriptionId
    ),{
        println("Billing_overview"+" "+ it)

    },)
    {
        println("Billing_overview"+" "+ it)

    }
}


    private fun companySubDetailsApiCall(){
        val params = HashMap<String,Any>()
        params["subscriptionId"] = subscriptionId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.company_subscription_details,params){ response->

                println("Billing_overview"+" "+ webService.company_subscription_details)
                println("Billing_overview"+" "+ params)

                try {
                    println("Billing_overview"+" "+ response)

                    var status = response.getString("status")
                    if (status == "200"){
                        var dataobj = response.getJSONObject("data")
                        var name = dataobj.getString("name")
                        var logo = dataobj.getString("logo")
                        var type = dataobj.getString("type")
                        var level = dataobj.getString("level")
                        var role = dataobj.getString("role")
                        try {
                            Picasso.get().load(webService.imageBasePath+logo).placeholder(R.drawable.ic_bill_history).into(BO_image)
                        }catch (e:Exception){
                            println(e)
                        }
                        if (type == "1" || type == "3"){
                            BO_heading.text = name
                        }else{
                            BO_heading.text = name
                        }

                    }
                }catch (e:Exception){
                    println(e)
                }

            }
        }
        billingDetailsApiCall()
    }

    private fun onClickActions(){
        billing_overview_back_arrow.setOnClickListener{
           context?.loadFragment(context!!,MoreFragment())
        }
        BO_viewEstimate.setOnClickListener{
            context?.loadFragment(context!!,BillingEstimateFragment())
        }
        BO_viewBillDetails.setOnClickListener{
            context?.loadFragment(context!!,BillingDetailsFragment())
        }
        BO_viewFullHistory.setOnClickListener{
            context?.loadFragment(context!!,BillingHistoryFragment())
        }
        BO_manage_subscription.setOnClickListener{
            context?.loadFragment(context!!,BillingManageSubscriptionFragment())
        }
        manage_contact.setOnClickListener{
            context?.setSharedPref(context!!,"bcFrom","BillingOverview")
          context?.loadFragment(context!!,ConfigureBillingFragment())
        }
    }

    private fun billingOverViewApiCall(){
        var params = HashMap<String,Any>()
        params["subscriptionId"] = subscriptionId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.billing_overview_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){

                        var msg = response.getString("msg")
                        var dataArray = response.getJSONArray("data")
                        for (i in 0 until dataArray.length()){
                            var dataObj = dataArray.getJSONObject(i)
                            if (dataObj.has("_id")){
                                var _id = dataObj.getString("_id")
                            }


                            var invoiceCCArray = dataObj.getJSONArray("invoiceCC")
                            if (invoiceCCArray.length() <= 0){
                                sendCopyTV.visibility = View.GONE
                            }else{
                                sendCopyTV.visibility = View.VISIBLE
                            }
                            for (cc in 0 until invoiceCCArray.length()){
                                var ccObj = invoiceCCArray.getString(cc)
                                billingEmailsModel.add(BillingEmailsModel(ccObj.toString()))
                            }
                            setupBillingEmailsRecyclerView(billingEmailsModel)

                            var cardDetailObj = dataObj.getJSONObject("cardDetail")
                            var lastDigit = cardDetailObj.getString("lastDigit")
                            card_num_dots.text = "XXXX   XXXX   XXXX   $lastDigit"
                            var cardHolder = cardDetailObj.getString("cardHolder")
                            cardHolder_name.text = cardHolder
                            var expire = cardDetailObj.getString("expire")

                            cardExpire_date.text = "Exp $expire"

                            var billingDetailObj = dataObj.getJSONObject("billingDetail")
                            var price = billingDetailObj.getString("price")
                            var billingCycle = billingDetailObj.getString("billingCycle")
                            if (billingCycle == "1"){
                                billingPeriod.text = "Annual"
                            }else{
                                billingPeriod.text = "Monthly"
                            }
                            var estimatedBill = billingDetailObj.getString("estimatedBill")
                            BO_estimate_price.text = "USD $price"
                            var estimatedQuantity = billingDetailObj.getString("estimatedQuantity")
                            var countOfActiveUsers = billingDetailObj.getString("countOfActiveUsers")
                            var billingStartDate = billingDetailObj.getString("billingStartDate")
                            var billingEndDate = billingDetailObj.getString("billingEndDate")
                            var lastDate = billingDetailObj.getString("lastDate")
                            var sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                            var myFormat = SimpleDateFormat("MMM dd, yyyy")
                            var date = sdf.parse(billingEndDate)
                            var dateF = myFormat.format(date)
                            BO_charge_date.text = dateF


                            var paymentHistoryArray = dataObj.getJSONArray("paymentHistory")
                            if (paymentHistoryArray.length() <= 0){
                                noHistory.visibility = View.VISIBLE
                                billHistory_recyclerView.visibility = View.GONE
                            }else{
                                noHistory.visibility = View.GONE
                                billHistory_recyclerView.visibility = View.VISIBLE
                            }
                            for (p in 0 until paymentHistoryArray.length()){
                                if (p==0){
                                    billingHistoryModel.clear()
                                    billingHistoryModel =  ArrayList()
                                }
                                billHistory_recyclerView.visibility = View.VISIBLE
                                var paymentHistoryObj = paymentHistoryArray.getJSONObject(p)
                                var _id = paymentHistoryObj.getString("_id")
                                var amount = paymentHistoryObj.getString("amount")
                                var startDate = paymentHistoryObj.getString("startDate")
                                var createdAt = paymentHistoryObj.getString("createdAt")
                                billingHistoryModel.add(BillingHistoryModel(_id,amount,startDate,createdAt,"","","",""))
                            }



                            var contactObj = dataObj.getJSONObject("contact")
                            var firstName = contactObj.getString("firstName")
                            var lastName = ""
                            if (contactObj.has("lastName")){
                                lastName = contactObj.getString("lastName")
                            }

                            contactName.text = "$firstName $lastName"
                            var companyName = contactObj.getString("companyName")
//                        BO_heading.text = companyName
                            var phone = contactObj.getString("phone")
                            var email = contactObj.getString("email")
                            contactEmail.text = email

                        }
                        setupBillingHistoryRecyclerView(billingHistoryModel)
                    }


                }catch (e:Exception){
                    println(e)
                }
            }
        }

    }

    private fun setupBillingEmailsRecyclerView(billingEmailsModel: ArrayList<BillingEmailsModel>) {
        val adapter: RecyclerView.Adapter<*> = BillingEmailsAdapter(context, billingEmailsModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        emails_recyclerView.layoutManager = llm
        emails_recyclerView.adapter = adapter
    }
    private fun setupBillingHistoryRecyclerView(billingHistoryModel: ArrayList<BillingHistoryModel>) {
        val adapter: RecyclerView.Adapter<*> = BillingHistoryAdapter(context, billingHistoryModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        billHistory_recyclerView.layoutManager = llm
        billHistory_recyclerView.adapter = adapter
    }

    private fun billingDetailsApiCall(){
        val params = HashMap<String,Any>()
        params["subscriptionId"] = subscriptionId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.billing_details_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")

                    if (status == "200"){
                        var dataArr = response.getJSONArray("data")
                        for (i in 0 until dataArr.length()){
                            var dataObj = dataArr.getJSONObject(i)
                            var _id = dataObj.getString("_id")
                            var price = dataObj.getString("price")
                            var type = dataObj.getString("type")
                            var status = dataObj.getString("status")

                            var billingContactObj = dataObj.getJSONObject("billingContact")
                            var cardDetailObj = billingContactObj.getJSONObject("cardDetail")
                            var lastDigit = cardDetailObj.getString("lastDigit")
                            var cardHolder = cardDetailObj.getString("cardHolder")
                            var expire = cardDetailObj.getString("expire")

                            var billingDetailObj = billingContactObj.getJSONObject("billingDetail")
                            var b_price = billingDetailObj.getString("price")
                            var billingCycle = billingDetailObj.getString("billingCycle")

                            var estimatedBill = billingDetailObj.getString("estimatedBill")
                            var estimatedQuantity = billingDetailObj.getString("estimatedQuantity")
                            var countOfActiveUsers = billingDetailObj.getString("countOfActiveUsers")
                            var billingStartDate = billingDetailObj.getString("billingStartDate")
                            var billingEndDate = billingDetailObj.getString("billingEndDate")

                            var invoiceCCArr = billingContactObj.getJSONArray("invoiceCC")


                            var b_status = billingContactObj.getString("status")
                            var b_id = billingContactObj.getString("_id")

                            var firstName = billingContactObj.getString("firstName")
                            var lastName = ""
                            if (billingContactObj.has("lastName"))
                            {
                                lastName = billingContactObj.getString("lastName")
                            }

                            var countryCode = billingContactObj.getString("countryCode")
                            var phone = billingContactObj.getString("phone")
                            var email = billingContactObj.getString("email")

                            var companyName = billingContactObj.getString("companyName")
                            var subscriptionSettingsId = billingContactObj.getString("subscriptionSettingsId")
                            var createdBy = billingContactObj.getString("createdBy")
                            var customerId = billingContactObj.getString("customerId")
                            var paymentMethodId = billingContactObj.getString("paymentMethodId")
                            var subscriptionId = billingContactObj.getString("subscriptionId")
                            var addresLine1 = billingContactObj.getString("addresLine1")
                            var addressLine2 = ""
                            if (billingContactObj.has("addressLine2")){
                                addressLine2 = billingContactObj.getString("addressLine2")
                            }

                            var city = billingContactObj.getString("city")
                            var state = billingContactObj.getString("state")
                            var country = billingContactObj.getString("country")
                            var zipCode = billingContactObj.getString("zipCode")

                            context?.setSharedPref(context!!,"bc_fName",firstName)
                            context?.setSharedPref(context!!,"bc_lName",lastName)
                            context?.setSharedPref(context!!,"bc_phone",phone)
                            context?.setSharedPref(context!!,"bc_email",email)
                            context?.setSharedPref(context!!,"bc_company",companyName)
                            if(billingCycle == "0"){

                                context?.setSharedPref(context!!,"bc_b_period", "Monthly")

                            }else{
                                context?.setSharedPref(context!!,"bc_b_period", "Yearly")
                            }

                            context?.setSharedPref(context!!,"bc_address1",addresLine1)
                            context?.setSharedPref(context!!,"bc_address2",addressLine2)
                            context?.setSharedPref(context!!,"bc_country",country)
                            context?.setSharedPref(context!!,"bc_state",state)
                            context?.setSharedPref(context!!,"bc_city",city)
                            context?.setSharedPref(context!!,"bc_zip",zipCode)

                        }
                    }

                }catch (e:Exception){
                    println(e)
                }
            }
        }
    }
}
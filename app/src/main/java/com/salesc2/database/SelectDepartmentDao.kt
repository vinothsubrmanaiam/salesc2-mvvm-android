package com.salesc2.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface SelectDepartmentDao {
    @Query("SELECT * FROM SelectDepartment ORDER BY name ASC ")
    suspend fun getAll(): List<SelectDepartment>



    @Insert
    suspend fun insertAll(vararg selectDepartment: SelectDepartment)


    @Query("DELETE FROM SelectDepartment")
    fun deleteAll()
}
package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.EmployeeLoginRepository

import kotlinx.coroutines.launch

class UserLoginViewModel(
    private val repository: EmployeeLoginRepository,
    val context: Context
) : ViewModel() {
     fun reqEmployeeLogin(
         requestUserLogin: RequestUserLogin,
         onSuccess: OnSuccess<ResponseEmployeeLogin>,
         onFailure: OnFailure<ResponseEmployeeLogin>
    )
     {
         viewModelScope.launch {
             repository.reqEmployeeLogin(requestUserLogin,onSuccess,onFailure)
         }
     }

}
package com.salesc2.service

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper
import com.salesc2.model.AddAttendeesModel
import com.salesc2.model.AddProductsModel

//creating the database logic, extending the SQLiteOpenHelper base class
class DatabaseHandler(context: Context): SQLiteOpenHelper(context,DATABASE_NAME,null,DATABASE_VERSION) {
    companion object {
        private val DATABASE_VERSION = 3
        private val DATABASE_NAME = "AddTaskProducts"
        private val TABLE_PRODUCTS = "Products"
        private val KEY_ID = "id"
        private val KEY_NAME = "name"
        private val KEY_PRICE = "price"
        private val KEY_QTY = "quantity"

        private val TABLE_ATTENDEES = "Attendees"
        private val KEY_AT_ID = "attendee_id"
        private val KEY_AT_NAME = "attendee_name"
        private val KEY_AT_IMG = "attendee_img"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        //To change body of created functions use File | Settings | File Templates.
        //creating table with fields
        val CREATE_PRODUCTS_TABLE = ("CREATE TABLE " + TABLE_PRODUCTS + "("+ KEY_ID + " TEXT," + KEY_NAME + " TEXT," + KEY_PRICE + " TEXT," + KEY_QTY + " TEXT"+")")
        val CREATE_ATTENDEES_TABLE = ("CREATE TABLE " + TABLE_ATTENDEES + "("+ KEY_AT_ID + " TEXT,"+ KEY_AT_NAME + " TEXT," + KEY_AT_IMG + " TEXT"+")")
        db?.execSQL(CREATE_PRODUCTS_TABLE)
        db?.execSQL(CREATE_ATTENDEES_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
         //To change body of created functions use File | Settings | File Templates.
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS)
        db!!.execSQL("DROP TABLE IF EXISTS " + TABLE_ATTENDEES)
        onCreate(db)
    }


    //method to insert data
    fun addProducts(prod: AddProductsModel):Long{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_ID, prod.prod_id)
        contentValues.put(KEY_NAME, prod.prod_name) // EmpModelClass Name
        contentValues.put(KEY_PRICE,prod.prod_price )
        contentValues.put(KEY_QTY,prod.prod_qty )
        // Inserting Row
        val success = db.insert(TABLE_PRODUCTS, null, contentValues)
        //2nd argument is String containing nullColumnHack
        db.close() // Closing database connection
        return success
    }
    //method to read data
    fun viewProducts():List<AddProductsModel>{
        val empList:ArrayList<AddProductsModel> = ArrayList<AddProductsModel>()
        val selectQuery = "SELECT  * FROM $TABLE_PRODUCTS"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        var prodId: String
        var prodName: String
        var prodPrice: String
        var prodQty : String
        if (cursor.moveToFirst()) {
            do {
                prodId = cursor.getString(cursor.getColumnIndex("id"))
                prodName = cursor.getString(cursor.getColumnIndex("name"))
                prodPrice = cursor.getString(cursor.getColumnIndex("price"))
                prodQty = cursor.getString(cursor.getColumnIndex("quantity"))
                val emp= AddProductsModel(prod_id = prodId, prod_name = prodName, prod_price = prodPrice, prod_qty = prodQty)
                empList.add(emp)
            } while (cursor.moveToNext())
        }
        return empList
    }
//    //method to update data
//    fun updateEmployee(prod: AddProductsModel):Int{
//        val db = this.writableDatabase
//        val contentValues = ContentValues()
//        contentValues.put(KEY_ID, prod.prod_id)
//        contentValues.put(KEY_NAME, prod.prod_name) // EmpModelClass Name
//        contentValues.put(KEY_PRICE,prod.prod_price )
//        contentValues.put(KEY_QTY,prod.prod_qty )
//
//        // Updating Row
//        val success = db.update(TABLE_PRODUCTS, contentValues,"id="+prod.prod_id,null)
//        //2nd argument is String containing nullColumnHack
//        db.close() // Closing database connection
//        return success
//    }
//    //method to delete data
    fun deleteProducts(){
        val db = this.writableDatabase
        db.execSQL("delete from $TABLE_PRODUCTS")
        db.close() // Closing database connection
    }


    //Attendees
    //method to insert data
    fun addAttendees(at: AddAttendeesModel):Long{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_AT_ID,at.at_id )
        contentValues.put(KEY_AT_NAME, at.at_name) // EmpModelClass Name
        contentValues.put(KEY_AT_IMG,at.at_img )

        // Inserting Row
        val success = db.insert(TABLE_ATTENDEES, null, contentValues)
        //2nd argument is String containing nullColumnHack
        "delete from $TABLE_ATTENDEES where ${at.at_id} not in (select min(${at.at_id})\n" + "from $TABLE_ATTENDEES\n" +
                "group by $KEY_AT_ID,$KEY_AT_NAME,$KEY_AT_IMG)"
        db.close() // Closing database connection
        return success
    }

    //method to read data
    fun viewAttendees():List<AddAttendeesModel>{
        val atList:ArrayList<AddAttendeesModel> = ArrayList<AddAttendeesModel>()
        val selectQuery = "SELECT DISTINCT $KEY_AT_ID, $KEY_AT_NAME, $KEY_AT_IMG FROM $TABLE_ATTENDEES"
        val db = this.readableDatabase
        var cursor: Cursor? = null
        try{
            cursor = db.rawQuery(selectQuery, null)
        }catch (e: SQLiteException) {
            db.execSQL(selectQuery)
            return ArrayList()
        }
        var atId: String
        var atName: String
        var atImg: String

        if (cursor.moveToFirst()) {
            do {
                atId = cursor.getString(cursor.getColumnIndex("attendee_id"))
                atName = cursor.getString(cursor.getColumnIndex("attendee_name"))
                atImg = cursor.getString(cursor.getColumnIndex("attendee_img"))
                val attendee= AddAttendeesModel(at_id = atId,at_name = atName,at_img = atImg)
                atList.add(attendee)
            } while (cursor.moveToNext())
        }


        return atList
    }
    //method to delete data
    fun deleteAttendees(){
        val db = this.writableDatabase
        db.execSQL("delete from $TABLE_ATTENDEES")
        db.close() // Closing database connection
    }

    //method to delete data
    fun deleteAttendeeById(at: AddAttendeesModel) :Int{
        val db = this.writableDatabase
        val contentValues = ContentValues()
        contentValues.put(KEY_AT_ID, at.at_id)
        contentValues.put(KEY_AT_NAME, at.at_name)
        contentValues.put(KEY_AT_IMG, at.at_img) // EmpModelClass UserId
//        delete(at.at_id)
        // Deleting Row
        val success = db.delete(TABLE_ATTENDEES,"$KEY_AT_ID='" + at.at_id + "'",null)
//         db.execSQL("DELETE FROM " + TABLE_ATTENDEES + " WHERE " + KEY_AT_ID + "= '" + at.at_id + "'");
        //2nd argument is String containing nullColumnHack
        db.close() // Closing database connection
return success
    }
    fun delete(id: String) {
        val db = this.writableDatabase
        db.execSQL("delete from " + TABLE_ATTENDEES + " where $KEY_AT_ID='" + id + "'")
    }
}
package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestBillingOverView
import com.salesc2.data.network.api.request.RequestMorePage
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseBillingOverview
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.api.response.ResponseMorePage
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.BillingOverViewRepository
import com.salesc2.data.repository.EmployeeLoginRepository
import com.salesc2.data.repository.MorePageRepository

import kotlinx.coroutines.launch

class BillingoverViewViewModel(
    private val repository: BillingOverViewRepository,
    val context: Context
) : ViewModel() {
     fun reqGetSubscription(
         requestBillingOverView: RequestBillingOverView,
         onSuccess: OnSuccess<ResponseBillingOverview>,
         onFailure: OnFailure<ResponseBillingOverview>
    )
     {
         viewModelScope.launch {
             repository.reqGetSubscription(requestBillingOverView,onSuccess,onFailure)
         }
     }

}
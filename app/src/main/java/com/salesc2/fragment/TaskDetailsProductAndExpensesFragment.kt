package com.salesc2.fragment

import android.app.Dialog
import android.os.Bundle
import android.text.method.DigitsKeyListener
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.adapters.TaskDetailsExpensesAdapter
import com.salesc2.adapters.TaskDetailsProductAdapter
import com.salesc2.database.AppDatabase
import com.salesc2.model.TaskDetailsExpensesModel
import com.salesc2.model.TaskDetailsProductsModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject

class TaskDetailsProductAndExpensesFragment : Fragment(), RefreshProductAndExpensesTaskDetails {
    private lateinit var productDetails_recyclerView: RecyclerView
    private var taskDetailsProductModel = ArrayList<TaskDetailsProductsModel>()
    private lateinit var td_add_products: Button
    private lateinit var td_addExpense: TextView
    private lateinit var expensesDetails_recyclerView: RecyclerView
    private var taskDetailsExpensesModel = ArrayList<TaskDetailsExpensesModel>()
    var apiRequest = ApiRequest()
    var webService = WebService()
    private var taskId = String()
    var searchText = String()
    private var prodNameList = ArrayList<String>()
    lateinit var mView: View
    private var prodId = String()
    private var prodIdList = ArrayList<String>()

    var productIdArr = ArrayList<String>()
    private var productNameArr = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_task_details_product_expenses, container, false)
        taskId = context?.getSharedPref("task_id", context!!).toString()
        print("tasks_id==$taskId")
        context?.setSharedPref(context!!, "task_id_edit_prod", taskId)
        productDetails_recyclerView = mView.findViewById(R.id.td_productDetails_recyclerView)
        td_add_products = mView.findViewById(R.id.td_add_products_btn)
        td_addExpense = mView.findViewById(R.id.td_addExpenses_tv)
        expensesDetails_recyclerView = mView.findViewById(R.id.td_expensesDetails_recyclerView)
        taskDetailsExpensesModel = ArrayList()
        taskDetailsProductModel = ArrayList()
        prodNameList = ArrayList()
        prodIdList = ArrayList()

        if (context?.isNetworkAvailable(context!!)==true){
            productDetailsApiCall()
            expensesDetailsApiCall()
        }else{
            productExpenseOfflineData()
        }

        addProductsAction()
        addExpensesAction()
        addProductApiCall()

        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<ImageView>(R.id.app_icon_imageView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility =
            View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility =
            View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private var count = 1
    private fun addProductsAction() {
        td_add_products.setOnClickListener {
            val mBottomSheetDialog: Dialog? =
                context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) };
            mBottomSheetDialog?.setContentView(R.layout.task_details_add_products_popup)
            val product_name =
                mBottomSheetDialog?.findViewById<AutoCompleteTextView>(R.id.product_name_et)
            var product_price = mBottomSheetDialog?.findViewById<TextView>(R.id.product_price_et)
            var q_count = mBottomSheetDialog?.findViewById<TextView>(R.id.q_count)
            var close: ImageView? = mBottomSheetDialog?.findViewById(R.id.close_iv)
            var q_minus: ImageView? = mBottomSheetDialog?.findViewById(R.id.q_minus)
            var q_plus: ImageView? = mBottomSheetDialog?.findViewById(R.id.q_plus)
            var add_product_td: Button? = mBottomSheetDialog?.findViewById(R.id.add_product_td)
            product_price?.setKeyListener(DigitsKeyListener.getInstance(false, true))
            q_plus?.setOnClickListener {
                count++
                q_count?.text = count.toString()
            }
            q_minus?.setOnClickListener {
                if (q_count?.text.toString().toInt() > 1) {
                    count--
                }
                q_count?.text = count.toString()
            }


            val arrayAdapter = ArrayAdapter<String>(
                context!!,
                R.layout.support_simple_spinner_dropdown_item,
                prodNameList
            )
            product_name?.setAdapter(arrayAdapter)
            product_name?.threshold = 1
            product_name?.setOnClickListener {
                product_name?.showDropDown()

            }
            product_name?.setDropDownBackgroundDrawable(
                ContextCompat.getDrawable(
                    context!!,
                    R.drawable.corner_shape_white_bg
                )
            )
            product_name?.onFocusChangeListener = View.OnFocusChangeListener { view, b ->
//                view.setBackgroundColor(ContextCompat.getColor(context!!,R.color.white))
                if (b) {
                    // Display the suggestion dropdown on focus
                    product_name?.showDropDown()
                }
            }
            product_name?.onItemClickListener =
                AdapterView.OnItemClickListener { parent, view, position, id ->
                    val selectedItem = parent?.getItemAtPosition(position).toString()

                    product_name?.clearFocus()
                    val selectedItemId = parent?.getItemIdAtPosition(position)
                    prodId = prodIdList.get(selectedItemId!!.toInt())
                    if (prodId == "") {
                        Toast.makeText(context, "Closed.", Toast.LENGTH_SHORT).show()
                        prodId = ""
                    } else {
                        product_name?.setText(selectedItem)
                    }


                }

            mBottomSheetDialog?.setCancelable(true)
            mBottomSheetDialog?.window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            );
            mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
            mBottomSheetDialog?.show()

            add_product_td?.setOnClickListener {
                if (product_name?.text.toString().trim() == "" || product_name?.text.toString()
                        .trim().isEmpty() || product_name?.text.toString().trim().isBlank()
                ) {
                    context?.toast("Please enter product name")
                } else if (product_price?.text.toString()
                        .trim() == "" || product_price?.text.toString().trim()
                        .isEmpty() || product_price?.text.toString().trim().isBlank()
                ) {
                    context?.toast("Please enter product price")
                } else if (product_price?.text.toString().toDouble() < 0.1) {
                    context?.toast("Please enter a valid price")
                } else if (productIdArr.contains(prodId) || productNameArr.contains(product_name?.text.toString())) {
                    context?.toast("Product already exist")
                } else {
                    val roundOff =
                        Math.round(product_price?.text.toString().toDouble() * 100.0) / 100.0

                    if (!prodId.equals("")) {
                        var jsonObj = JSONObject()
                        jsonObj.put("item", product_name?.text.toString())
                        jsonObj.put("price", roundOff)
                        jsonObj.put("productId", prodId)
                        jsonObj.put("quantity", q_count?.text.toString())
                        var arrayOfParams = JSONArray()
                        arrayOfParams.put(jsonObj)
                        val params = HashMap<String, Any>()
                        params.put("id", taskId)
                        params.put("products", arrayOfParams)
                        apiRequest.postRequestBodyWithHeadersAny(
                            context!!,
                            webService.Task_product_add_edit_url,
                            params
                        ) { response ->
                            try {
                                var status = response.getString("status")
                                if (status == "202") {
                                    prodId = ""
                                    var msg = response.getString("msg")
                                    context?.toast(msg)
                                    productDetailsApiCall()
                                    mBottomSheetDialog?.dismiss()
                                } else {
                                    var msg = response.getString("msg")
                                    context?.toast(msg)
                                }
                            } catch (e: Exception) {

                            }
                        }
                    } else {
                        context?.toast("Kindly select the valid product")
                    }
                }


            }

            close?.setOnClickListener {
                mBottomSheetDialog?.dismiss()
            }

        }
    }

    private fun addProductApiCall() {
        var param = HashMap<String, String>()
        param.put("id", taskId)
        context?.let {
            apiRequest.postRequestBodyWithHeaders(
                it,
                webService.Add_products_url,
                param
            ) { response ->

                try {

                    var status = response.getString("status")
                    if (status == "200") {
                        prodIdList.clear()
                        prodIdList.clear()
                        prodIdList = ArrayList()
                        prodIdList = ArrayList()
                        var dataArray = response.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {
                            var dataObj = dataArray.getJSONObject(i)
                            var id = dataObj.getString("_id")
                            prodIdList.add(id)
                            var name = dataObj.getString("name")
                            prodNameList.add(name)

                            context?.progressBarDismiss(context!!)
                        }
                    } else {
                        context?.progressBarDismiss(context!!)
                    }
                } catch (e: java.lang.Exception) {
                    context?.toast(e.toString())
                }
                finally {
                    productDetailsApiCall()
                }
            }

        }
    }

    private fun productDetailsApiCall() {
        taskDetailsProductModel.clear()
        taskDetailsProductModel = ArrayList()
        productIdArr.clear()
        productNameArr.clear()
        val params = HashMap<String, String>()
        params["id"] = taskId
        context?.let {
            apiRequest.postRequestBodyWithHeaders(
                it,
                webService.Task_product_details_url,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")

                    if (status == "200") {

                        var dataArray = response.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {
                            var dataObj = dataArray.getJSONObject(i)
                            var prod_id = dataObj.getString("_id")
                            var price = dataObj.getString("price")
                            var quantity = dataObj.getString("quantity")
                            var name = dataObj.getString("name")
                            var productId = String()
                            if (dataObj.has("productId")){
                                productId = dataObj.getString("productId")
                            }
                            productIdArr.add(productId)
                            productNameArr.add(name)
                            taskDetailsProductModel.add(
                                TaskDetailsProductsModel(
                                    prod_id,
                                    name,
                                    productId,
                                    price,
                                    quantity
                                )
                            )
                        }

                        setupProductDetailsRecyclerView(taskDetailsProductModel)
                    } else if (status == "400") {
//                        taskDetailsProductModel.clear()
//                        taskDetailsProductModel = ArrayList()
                    }
//                    setupProductDetailsRecyclerView(taskDetailsProductModel)

                } catch (e: Exception) {
                    print(e)
                }

            }
        }
    }

    //    setup adapter
    private fun setupProductDetailsRecyclerView(taskDetailsProductsModel: ArrayList<TaskDetailsProductsModel>) {
        val adapter: RecyclerView.Adapter<*> = TaskDetailsProductAdapter(
            context!!, taskDetailsProductsModel, productIdArr, productNameArr, this)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        productDetails_recyclerView.layoutManager = llm
        productDetails_recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }


    private fun expensesDetailsApiCall() {
        taskDetailsExpensesModel.clear()
        taskDetailsExpensesModel = ArrayList()
        val params = HashMap<String, String>()
        params["task_id"] = taskId
        context?.let {
            apiRequest.postRequestBodyWithHeaders(
                it,
                webService.Task_expenses_details_url,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    if (status == "200") {
                        var dataArray = response.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {
                            var dataObj = dataArray.getJSONObject(i)
                            var _id = dataObj.getString("_id")
                            var employeeName = dataObj.getString("employeeName")
                            var description = dataObj.getString("description")
                            var amount = dataObj.getString("amount")
                            var time = dataObj.getString("time")
                            var employeeId = dataObj.getString("employeeId")
                            var expensesId = dataObj.getString("expensesId")

                            taskDetailsExpensesModel.add(
                                TaskDetailsExpensesModel(
                                    expensesId,
                                    employeeName,
                                    description,
                                    amount,
                                    time,
                                    employeeId
                                )
                            )
                        }
                        setupExpenseDetailsRecyclerView(taskDetailsExpensesModel)
                    }

                } catch (e: Exception) {
                    print(e)
                }

            }
        }
    }

    //    setup adapter
    private fun setupExpenseDetailsRecyclerView(taskDetailsExpensesModel: ArrayList<TaskDetailsExpensesModel>) {
        val adapter: RecyclerView.Adapter<*> =
            TaskDetailsExpensesAdapter(context!!, taskDetailsExpensesModel, this)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        expensesDetails_recyclerView.layoutManager = llm
        expensesDetails_recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()

    }


    private fun addExpensesAction() {
        td_addExpense.setOnClickListener {
            var mBottomSheetDialog: Dialog? =
                context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) };
            mBottomSheetDialog?.setContentView(R.layout.task_details_add_expenses_popup)
            var expense_name = mBottomSheetDialog?.findViewById<EditText>(R.id.product_name_et)
            var expense_price = mBottomSheetDialog?.findViewById<EditText>(R.id.product_price_et)
            var close: ImageView? = mBottomSheetDialog?.findViewById(R.id.close_iv)
            var addExpense: Button? = mBottomSheetDialog?.findViewById(R.id.add_expense)
            expense_price?.setKeyListener(DigitsKeyListener.getInstance(false, true))
            addExpense?.setOnClickListener {
                var desc = expense_name?.text.toString()
                var price = expense_price?.text.toString()
/* val value: String = expense_price!!.getText().toString()
val finalValue = value.toInt()
*/


                if (desc.trim().equals("") || desc.trim().isEmpty()) {
                    context?.toast("Please enter expense details")
                } else if (price.trim().equals("") || price.trim().isEmpty()) {
                    context?.toast("Please enter price")
                }


/* else if (finalValue > 1){

}*/
                else {
                    val n2Var: Double = expense_price!!.getText().toString().toDouble()
                    val roundOff = Math.round(n2Var * 100.0) / 100.0

                    var params = HashMap<String, Any>()
                    params["task_id"] = taskId
                    params["description"] = desc
                    params["amount"] = roundOff
                    params["expensesId"] = ""

                    context?.let {
                        apiRequest.postRequestBodyWithHeadersAny(
                            it,
                            webService.Task_expenses_add_edit_url,
                            params
                        ) { response ->
                            try {

                                var status = response.getString("status")
                                var msg = response.getString("msg")
                                if (status == "200") {
                                    mBottomSheetDialog?.dismiss()
                                    context?.toast(msg)
                                    taskDetailsExpensesModel.clear()
                                    expensesDetailsApiCall()
                                } else {
                                    context?.toast(msg)
                                }

                            } catch (e: java.lang.Exception) {
                                context?.toast(e.toString())
                            }
                        }
                    }
                }


            }



            mBottomSheetDialog?.setCancelable(true)
            mBottomSheetDialog?.window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            );
            mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
            mBottomSheetDialog?.show()

            close?.setOnClickListener {
                mBottomSheetDialog?.dismiss()
            }

        }
    }


    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

    override fun refreshProductAndExpenses() {
        productDetailsApiCall()
        expensesDetailsApiCall()
    }
    //
    var db: AppDatabase? = null
    //offline data
    private fun productExpenseOfflineData(){
        CoroutineScope(Dispatchers.IO).launch {
            db = AppDatabase.getDatabaseClient(requireContext())
            val taskData = db!!.taskDao().getTaskDetails(taskId)
            Log.i("taskIdProExp=====",taskId)
            Log.i("taskDataProExp",taskData.toString())
            try {
            if (taskData.size >= 1){
                var taskId = taskData[0].task_id
                var products = taskData[0].products
                Log.i("products== ",products.toString())
                var productsArray = JSONArray(taskData[0].products)
                for (i in 0 until productsArray.length()) {
                    var dataObj = productsArray.getJSONObject(i)
                    var prod_id = dataObj.getString("_id")
                    var price = dataObj.getString("price")
                    var quantity = dataObj.getString("quantity")
                    var name = dataObj.getString("name")
                    var productId = String()
                    if (dataObj.has("productId")){
                        productId = dataObj.getString("productId")
                    }
                    productIdArr.add(productId)
                    productNameArr.add(name)
                    taskDetailsProductModel.add(
                        TaskDetailsProductsModel(
                            prod_id,
                            name,
                            productId,
                            price,
                            quantity
                        )
                    )
                }

                setupProductDetailsRecyclerView(taskDetailsProductModel)




                var expenses = taskData[0].expenses
                Log.i("expenses== ",expenses.toString())
                var dataArray = JSONArray(taskData[0].expenses)
                for (i in 0 until dataArray.length()) {
                    var dataObj = dataArray.getJSONObject(i)
                    var _id = dataObj.getString("_id")
                    var employeeName = String()
                    if (dataObj.has("employeeName")){
                         employeeName = dataObj.getString("employeeName")
                    }
                    var description = String()
                    if (dataObj.has("description")){
                        description = dataObj.getString("description")
                    }

                    var amount = dataObj.getString("amount")
                    var time = dataObj.getString("time")
                    var employeeId = dataObj.getString("employeeId")
                    var expensesId = String()
                    if (dataObj.has("expensesId")){
                        expensesId = dataObj.getString("expensesId")
                    }


                    taskDetailsExpensesModel.add(
                        TaskDetailsExpensesModel(
                            expensesId,
                            employeeName,
                            description,
                            amount,
                            time,
                            employeeId
                        )
                    )
                }
                setupExpenseDetailsRecyclerView(taskDetailsExpensesModel)
            }
            }catch (e:Exception){
                e.printStackTrace()
            }
        }
    }
}
package com.salesc2.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.R
import com.salesc2.service.AlertDialogView
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.alertDialog
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.progressBarDismiss
import com.salesc2.utils.progressBarShow
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


class AccountDetailsFragment : Fragment() {

    lateinit var mView: View
    lateinit var viewPager: ViewPager
    lateinit var tabLayout: TabLayout
    lateinit var accountName_tv: TextView
    lateinit var address_tv: TextView
    lateinit var getdirection_tv: TextView
    var website: String = ""
    var description: String = ""
    var territory: String = ""
    var authToken = String()
    var lat = String()
    var lng = String()
    var webService = WebService()
    var apiRequest = ApiRequest()
    var alertDialogView = AlertDialogView()

    private var account_id = String()

    var toolbar_title_tv: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_account_details, container, false)
        account_id = context?.getSharedPref("account_id",context!!).toString()
        //initializing views
        initViews(mView)

        //calling account detail API
        accountDetail_url()

        /* //calling account's Department detail API
         accountDepartmentDetail_url()*/
        toolbar_title_tv?.setOnClickListener {
            fragmentManager?.popBackStack()
        }


        getdirection_tv.setOnClickListener {
            var uri: String = "http://maps.google.com/maps?daddr=${lat},${lng}"
            var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            intent.setPackage("com.google.android.apps.maps")
            context?.startActivity(intent)
        }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                viewPager.currentItem = tab!!.position
                val fm = activity?.supportFragmentManager
                val ft = fm!!.beginTransaction()
                val count = fm.backStackEntryCount
                if (count >= 1) {
                    //   activity?.supportFragmentManager!!.popBackStack()
                }
                ft.commit()
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {

            }

        })


        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    //calling Account Detail API
    private fun accountDetail_url() {
        context?.let { activity?.progressBarShow(it) }

//        var accountID = arguments?.getString("account_id").toString()

        val params = HashMap<String, String>()
        params.put("id", account_id)
        params.put("Authorization", "Bearer $authToken")
        context?.let {
            apiRequest.postRequestBodyWithHeaders(
                it,
                webService.Account_detail_url,
                params
            ) { response ->
                val status = response.getString("status")

                if (status == "200") {
                    val data = response.getString("data")

                    var dataArray = response.getJSONArray("data")
                    for (i in 0 until dataArray.length()) {

                        val jsonObject = dataArray.getJSONObject(i)

                        var _id = jsonObject.getString("_id")
                        var accountName = jsonObject.getString("accountName")

                        val address = jsonObject.getString("address")
                        val addressObject = JSONObject(address)
                        var formatedAddress = addressObject.getString("formatedAddress")
                        var city = addressObject.getString("city")
                        var state = addressObject.getString("state")
                        var zipCode = addressObject.getString("zipCode")


                        website = jsonObject.getString("website")

                        description = jsonObject.getString("description")

                        val location = jsonObject.getString("location")
                        val locationObject = JSONObject(location)
                        lat = locationObject.getString("lat")
                        lng = locationObject.getString("lng")

                        territory = jsonObject.getString("territory")
                        accountName_tv.text = accountName
                        address_tv.text = formatedAddress
                    }
                    activity?.progressBarDismiss(it)

                } else {
                    val msg = response.getString("msg")

                    activity?.alertDialog(requireContext(), "Alert", msg)
                }
                //Set Adapter for tabLayout
                setStatePageAdapter()
                context?.let { activity?.progressBarDismiss(it) }
            }
        }


    }



    private fun setStatePageAdapter() {

        val bundle = Bundle()
        bundle.putString("website", website)
        bundle.putString("description", description)
        bundle.putString("territory", territory)
        val fragment = AccountDetailInformationFragment()
        fragment.arguments = bundle
        val bundle1 = Bundle()
        var accountID = arguments?.getString("account_id").toString()

        bundle1.putString("account_id", account_id)

        val accountDetailDepartmentsFragment = AccountDetailDepartmentsFragment()
        accountDetailDepartmentsFragment.arguments = bundle1

        val accountDetailTimelineFragment = AccountDetailTimelineFragment()
        accountDetailTimelineFragment.arguments = bundle1

        val myViewPageStateAdapter: MyViewPageStateAdapter? =
            activity?.supportFragmentManager?.let { MyViewPageStateAdapter(it) }

        myViewPageStateAdapter?.addFragment(accountDetailDepartmentsFragment, "Departments")
        myViewPageStateAdapter?.addFragment(accountDetailTimelineFragment, "Timeline")
        myViewPageStateAdapter?.addFragment(fragment, "Account Information")
        viewPager.adapter = myViewPageStateAdapter
        tabLayout.setupWithViewPager(viewPager, true)
        context?.let { activity?.progressBarDismiss(it) }
    }

    private fun initViews(mView: View?) {
        authToken = context?.let { activity?.getSharedPref("token", it).toString() }.toString()
        viewPager = mView!!.findViewById<ViewPager>(R.id.viewPager)
        tabLayout = mView.findViewById<TabLayout>(R.id.tabLayout)
        address_tv = mView.findViewById(R.id.address_tv)
        accountName_tv = mView.findViewById(R.id.accountName_tv)
        getdirection_tv = mView.findViewById(R.id.getdirection_tv)
        toolbar_title_tv = mView.findViewById(R.id.toolbar_title_tv)

    }

    class MyViewPageStateAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        val fragmentList: MutableList<Fragment> = ArrayList<Fragment>()
        val fragmentTitleList: MutableList<String> = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return fragmentList.get(position)
        }

        override fun getCount(): Int {
            return fragmentList.size
        }

        override fun getPageTitle(position: Int): CharSequence? {

            return fragmentTitleList.get(position)
        }

        fun addFragment(fragment: Fragment, title: String) {
            fragmentList.add(fragment)
            fragmentTitleList.add(title)

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(requireContext())
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(requireContext())
    }


}

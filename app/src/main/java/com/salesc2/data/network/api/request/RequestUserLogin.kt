package com.salesc2.data.network.api.request

import com.google.gson.annotations.SerializedName

data class RequestUserLogin(
    @SerializedName("password") val password: String? = "",
    @SerializedName("device_token") val device_token: String? = "",
    @SerializedName("email") val email: String? = ""
)

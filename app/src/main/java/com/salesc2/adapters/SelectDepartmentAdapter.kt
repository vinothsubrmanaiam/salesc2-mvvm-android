package com.salesc2.adapters

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.fragment.SelectDepartmentFragment
import com.salesc2.fragment.SelectServiceFragment
import com.salesc2.model.SelectAccountModel
import com.salesc2.model.SelectDeptModel
import com.salesc2.model.SelectServiceModel
import com.salesc2.utils.ShowNoData
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.setSharedPref
import java.util.*

class SelectDepartmentAdapter(context: Context?, var showdata: ShowNoData, var deptList: ArrayList<SelectDeptModel>) : RecyclerView.Adapter<SelectDepartmentAdapter.viewHolder>(),
    Filterable {
     var deptItems= ArrayList<SelectDeptModel>()
    var context: Context? = null
    var dept_filter_position = String()
    private  var row_index = -1
    var resultCount:Int = 0
   init{
        this.context = context
        deptItems = deptList

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_select_list, parent, false)
        dept_filter_position = context?.getSharedPref("dept_filter_position",context!!).toString()
        return viewHolder(view)
    }

    override fun getItemCount(): Int {
        return deptItems.size
    }

    var checked = true
    override fun onBindViewHolder(holder: viewHolder, position: Int) {
       var selectDeptModel :  SelectDeptModel = deptItems[position]


        val cap = selectDeptModel.dept_name.substring(0, 1).toUpperCase() + selectDeptModel.dept_name.substring(1)
        holder.name.text=cap.toString()
        dept_filter_position = context?.getSharedPref("dept_filter_position",context!!).toString()

        if (dept_filter_position != "null"){
            row_index=dept_filter_position.toInt()
            if (row_index.equals(dept_filter_position)){
                holder.select_icon.visibility = View.VISIBLE
                context?.let { ContextCompat.getColor(it, R.color.offwhite2) }?.let {
                    holder.list_layout.setBackgroundColor(it)
                }
            }
        }


        holder.list_layout.setOnClickListener(View.OnClickListener {
            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("dept_filter_position")
            editor.commit()
            if (checked) {
                row_index=position
                notifyDataSetChanged()
                checked = false
                holder.select_icon.visibility = View.VISIBLE
                context?.let { ContextCompat.getColor(it, R.color.offwhite2) }?.let {
                    holder.list_layout.setBackgroundColor(it)
                }
            }else{
                checked = true
            }
            context?.setSharedPref(context!!,"dept_filter_id",selectDeptModel.dept_id)
            context?.setSharedPref(context!!,"dept_filter_name",selectDeptModel.dept_name)
            var bundle = Bundle()
            var fragment = SelectServiceFragment()
            bundle.putString("dept_id",selectDeptModel.dept_id)
            fragment.arguments = bundle
            val activity = context as AppCompatActivity
            activity.supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragmet_container, fragment).addToBackStack(null).commit()

        })

        if(row_index==position){
            holder.select_icon.visibility = View.VISIBLE
            context?.setSharedPref(context!!,"dept_filter_position",position.toString())

        }
        else
        {
            holder.select_icon.visibility = View.GONE
        }


    }
    class viewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView) {
        var name : TextView = itemView.findViewById(R.id.select_list_name)
        val list_layout : ConstraintLayout = itemView.findViewById(R.id.list_layout)
        var select_icon : ImageView = itemView.findViewById(R.id.select_icon)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    deptItems = deptList

                } else {
                    val resultList = ArrayList<SelectDeptModel>()
                    for (row in deptList) {
                        if (row.dept_name.toLowerCase(Locale.ROOT)
                                .contains(charSearch.toLowerCase(Locale.ROOT))
                        ) {

                            resultList.add(row)
                        }

                    }
                    deptItems = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = deptItems
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                deptItems = results?.values as ArrayList<SelectDeptModel>

                if (deptItems.size==0) {
                    showdata.showNoData()

                }
                else
                {
                    showdata.hideNoData()
                    notifyDataSetChanged()
                    resultCount = results.count.toInt()
                }

            }



        }
    }
}
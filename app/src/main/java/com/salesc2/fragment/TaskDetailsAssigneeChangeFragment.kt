package com.salesc2.fragment

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.adapters.AddTaskAssigneeAdapter
import com.salesc2.adapters.TaskDetailsAssigneeChangeAdapter
import com.salesc2.model.AddTaskAssigneeModel
import com.salesc2.model.ChangeTaskAssigneeModel
import com.salesc2.model.FilterAssigneeModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import java.lang.Exception

class TaskDetailsAssigneeChangeFragment: Fragment() {


    var webService = WebService()
    var apiRequest = ApiRequest()
    lateinit var assigneeSearch : SearchView
    lateinit var assigneeListRecyclerView: RecyclerView
    lateinit var goback : ImageView
    private lateinit var close : ImageView
    private lateinit var taskHead:TextView

    private var searchtext = String()


    private lateinit var filterAssigneeModel: ArrayList<ChangeTaskAssigneeModel>

    private var task_start_date = String()
    private var task_end_date = String()
    private var task_account_id = String()
    private var task_id = String()
    lateinit var mView: View

    var reassign_dash_assignee = String()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_add_task_assignee, container, false)
//        task_start_date = arguments?.getString("task_start_date").toString()
//        task_end_date = arguments?.getString("task_end_date").toString()
        task_start_date = context?.getSharedPref("task_start_date",context!!).toString()
        task_end_date = context?.getSharedPref("task_end_date",context!!).toString()
        task_account_id = arguments?.getString("task_account_id").toString()
        task_id = context?.getSharedPref("task_detail_id", context!!).toString()
        reassign_dash_assignee = context?.getSharedPref("reassign_dash_assignee",context!!).toString()
        assigneeSearch = mView.findViewById(R.id.task_assignee_SearchView)
        assigneeListRecyclerView = mView.findViewById(R.id.task_assignee_recyclerView)
        goback = mView.findViewById(R.id.task_assignee_back_arrow)
        taskHead= mView.findViewById(R.id.taskHead)
        taskHead.setText("Task Assignee")
        filterAssigneeModel = ArrayList()
        close = mView.findViewById(R.id.close_iv)
        goBackAction()
        searchAssignee()
        closeAction()
        AddTaskAssigneeAsyncTask().execute()

        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private fun closeAction(){
        close.setOnClickListener{
            context?.loadFragment(context!!,TaskDetailsFragment())
        }
    }
    private fun goBackAction(){
        goback.setOnClickListener(View.OnClickListener {
            context?.loadFragment(context!!,TaskDetailsFragment())
        })
    }
    private fun searchAssignee(){
        assigneeSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchtext = newText
                filterAssigneeModel.clear()
                AddTaskAssigneeAsyncTask().execute()
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchtext = query
                filterAssigneeModel.clear()
                AddTaskAssigneeAsyncTask().execute()

                return true
            }

            override fun equals(other: Any?): Boolean {
                return super.equals(other)

            }


        })
    }
    inner class AddTaskAssigneeAsyncTask(): AsyncTask<String, String, String>(){
        override fun onPreExecute() {
            super.onPreExecute()
            task_start_date = context?.getSharedPref("task_start_date",context!!).toString()
            task_end_date = context?.getSharedPref("task_end_date",context!!).toString()
//            context?.progressBarShow(context!!)
        }
        override fun doInBackground(vararg params: String?): String {
            task_start_date = context?.getSharedPref("task_start_date",context!!).toString()
            task_end_date = context?.getSharedPref("task_end_date",context!!).toString()
            var param = HashMap<String,String>()
            param["accountId"] = task_account_id
            if(task_start_date != "null"){
                param.put("StartDate",task_start_date)
            }else{
                param.put("StartDate","")
            }
            if(task_end_date != "null") {
                param.put("toDate", task_end_date)
            }
            else{
                param.put("toDate", "")
            }
            param["search"] = searchtext
            context?.let {
                apiRequest.postRequestBodyWithHeaders(it,webService.Add_task_assignee_url,param){ response->
                    try {
                    var status = response.getString("status")

                    if (status == "200"){
                        filterAssigneeModel.clear()
                        filterAssigneeModel = ArrayList()
                        var dataArray = response.getJSONArray("data")
                        for (i in 0 until dataArray.length()){
                            var dataObj = dataArray.getJSONObject(i)
                            var _id = dataObj.getString("_id")
                            var name = dataObj.getString("name")
                            var profilePath = String()
                            if (dataObj.has("profilePath")) {
                                profilePath = dataObj.getString("profilePath")
                            }else{
                                profilePath = ""
                            }
                            var imgWithBase = webService.imageBasePath+profilePath
                            filterAssigneeModel.add(ChangeTaskAssigneeModel(_id,imgWithBase,name,task_id))
                            context?.progressBarDismiss(context!!)
                        }
                        setupFilterAssigneeRecyclerView(filterAssigneeModel)
                    }else if (status == "400"){
                        var msg = response.getString("msg")
                        context?.toast(msg)
                        context?.progressBarDismiss(context!!)
                    }

                }catch (e:Exception){

                    }
                }
            }

            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            context?.progressBarDismiss(context!!)

        }

    }

    //    setup adapter
    private fun setupFilterAssigneeRecyclerView(filterAssigneeModel: ArrayList<ChangeTaskAssigneeModel>) {
        val adapter: RecyclerView.Adapter<*> = TaskDetailsAssigneeChangeAdapter(context, filterAssigneeModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        assigneeListRecyclerView.layoutManager = llm
        assigneeListRecyclerView.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

}
package com.salesc2.adapters

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.fragment.FilterTaskListFragment
import com.salesc2.fragment.SelectDepartmentFragment
import com.salesc2.model.SelectAccountModel
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.setSharedPref
import java.util.ArrayList

class FilterAccountAdapter() : RecyclerView.Adapter<FilterAccountAdapter.viewHolder>() {

    private lateinit var accountItems: ArrayList<SelectAccountModel>
    var context: Context? = null
  private var filter_account_position = String()
    constructor(context: Context?, accountItems: ArrayList<SelectAccountModel>) : this(){
        this.context = context
        this.accountItems = accountItems

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_select_list, parent, false)
        filter_account_position = context?.getSharedPref("filter_account_position",context!!).toString()
        return viewHolder(view)
    }

    override fun getItemCount(): Int {
        return accountItems.size
    }

    var row_index = -1
    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        var selectAccountModel : SelectAccountModel = accountItems[position]
        holder.name.text = selectAccountModel.acc_name

        filter_account_position = context?.getSharedPref("filter_account_position",context!!).toString()
        if (filter_account_position != "null"){
            row_index=filter_account_position.toInt()
            if (row_index.equals(filter_account_position)){
                context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                    holder.name.setTextColor(it)
                }
                context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
                    holder.name.setBackgroundDrawable(it)
                }
            }
        }
        holder.list_layout.setOnClickListener(View.OnClickListener {
            context?.setSharedPref(context!!,"filter_account_id",selectAccountModel.acc_id)
            context?.setSharedPref(context!!,"filter_account_name",selectAccountModel.acc_name)
            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("filter_account_position")
            editor.commit()
            row_index=position
            notifyDataSetChanged()

        })
        if(row_index==position){
            context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                holder.name.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
                holder.name.setBackgroundDrawable(it)
            }
            context?.setSharedPref(context!!,"filter_account_position",position.toString())

        }
        else
        {
            context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
                holder.name.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
                holder.name.setBackgroundColor(it)
            }

        }


    }
    class viewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView) {

        var name : TextView = itemView.findViewById(R.id.select_list_name)
        val list_layout : ConstraintLayout = itemView.findViewById(R.id.list_layout)
        var select_icon : ImageView = itemView.findViewById(R.id.select_icon)
    }
}
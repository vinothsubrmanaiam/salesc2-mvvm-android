package com.salesc2.data.network.api.response

data class ResponseDashBoard(
    val `data`: List<Data>,
    val msg: String,
    val pager: Pager,
    val status: Int
) {
    data class Data(
        val `data`: List<Data>,
        val date: String
    ) {
        data class Data(
            val _id: String,
            val account_address: String,
            val actualStartdate: String,
            val assigned_to: AssignedTo,
            val department: String,
            val endDate: String,
            val geomettry: Geomettry,
            val isEmergency: Boolean,
            val startDate: String,
            val status: String,
            val taskStatus: String,
            val title: String,
            val type: String
        ) {
            data class AssignedTo(
                val _id: String,
                val image: String,
                val name: String
            )

            data class Geomettry(
                val lat: Double,
                val lng: Double
            )
        }
    }

    data class Pager(
        val currentPage: Int,
        val endIndex: Int,
        val endPage: Int,
        val pageSize: Int,
        val pages: List<Int>,
        val startIndex: Int,
        val startPage: Int,
        val totalItems: Int,
        val totalPages: Int
    )
}
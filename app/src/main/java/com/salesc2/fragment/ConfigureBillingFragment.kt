@file:Suppress("Annotator")

package com.salesc2.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.R
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ConfigureBillingFragment : Fragment() {

    private lateinit var bConfig_name : TextView
    private lateinit var bc_fName : EditText
    private lateinit var bc_lName : EditText
    private lateinit var bc_phone : EditText
    private lateinit var bc_email : EditText
    private lateinit var bc_company : EditText
    private lateinit var bc_b_period : AutoCompleteTextView
    private lateinit var bc_address1 : EditText
    private lateinit var bc_address2 : EditText
    private lateinit var bc_country : AutoCompleteTextView
    private lateinit var bc_state : AutoCompleteTextView
    private lateinit var bc_city : AutoCompleteTextView
    private lateinit var bc_zip : EditText
    private lateinit var bc_confirm_proceed : Button
    private lateinit var bc_cancel : Button
    private lateinit var bConfig_img : CircleImageView
    private lateinit var billing_config_back_arrow : ImageView

    private var b_fName = String()
    private var b_lName = String()
    private var b_phone = String()
    private var b_email = String()
    private var b_company = String()
    private var b_b_period = String()
    private var b_address1 = String()
    private var b_address2 = String()
    private var b_country = String()
    private var b_state = String()
    private var b_city = String()
    private var b_zip = String()
    private var bcFrom = String()

    private var userEmail = String()
    private var username = String()

    private var subscriptionId = String()
    private var empPhone = String()
    private var empCompany = String()


    var webService = WebService()
    var apiRequest = ApiRequest()
    var countryArr = ArrayList<String>()
    var countryCodeArr = ArrayList<String>()
    var countryIdArr = ArrayList<String>()
    private var countryParentId = ""
    var stateArr = ArrayList<String>()
    var stateIdArr = ArrayList<String>()
    private var stateParentId = ""
    var cityArr = ArrayList<String>()


    var billingPeriodArr = ArrayList<String>()
    private var billingId = String()

    lateinit var mView:View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_configure_billing, container, false)
        subscriptionId = context?.getSharedPref("subscriptionId",context!!).toString()
        userEmail = context?.getSharedPref("email",context!!).toString()
        username = context?.getSharedPref("username",context!!).toString()
        empPhone = context?.getSharedPref("empPhone",context!!).toString()
        empCompany = context?.getSharedPref("empCompany",context!!).toString()


        bcFrom = context?.getSharedPref("bcFrom",context!!).toString()
        b_fName = context?.getSharedPref("bc_fName",context!!).toString()
        b_lName = context?.getSharedPref("bc_lName",context!!).toString()
        b_phone = context?.getSharedPref("bc_phone",context!!).toString()
        b_email = context?.getSharedPref("bc_email",context!!).toString()
        b_company = context?.getSharedPref("bc_company",context!!).toString()
        b_b_period = context?.getSharedPref("bc_b_period",context!!).toString()
        b_address1 = context?.getSharedPref("bc_address1",context!!).toString()
        b_address2 = context?.getSharedPref("bc_address2",context!!).toString()
        b_country = context?.getSharedPref("bc_country",context!!).toString()
        b_state = context?.getSharedPref("bc_state",context!!).toString()
        b_city = context?.getSharedPref("bc_city",context!!).toString()
        b_zip = context?.getSharedPref("bc_zip",context!!).toString()
        billingId = context?.getSharedPref("billingId",context!!).toString()


        bConfig_name = mView.findViewById(R.id.bConfig_name)
        bc_fName = mView.findViewById(R.id.bc_fName)
        bc_lName = mView.findViewById(R.id.bc_lName)
        bc_phone = mView.findViewById(R.id.bc_phone)
        bc_email = mView.findViewById(R.id.bc_email)
        bc_company = mView.findViewById(R.id.bc_company)
        bc_b_period = mView.findViewById(R.id.bc_b_period)
        bc_address1 = mView.findViewById(R.id.bc_address1)
        bc_address2 = mView.findViewById(R.id.bc_address2)
        bc_country = mView.findViewById(R.id.bc_country)
        bc_state = mView.findViewById(R.id.bc_state)
        bc_city = mView.findViewById(R.id.bc_city)
        bc_zip = mView.findViewById(R.id.bc_zip)
        bc_confirm_proceed = mView.findViewById(R.id.bc_confirm_proceed)
        bc_cancel = mView.findViewById(R.id.bc_cancel)
        bConfig_img = mView.findViewById(R.id.bConfig_img)
        billing_config_back_arrow = mView.findViewById(R.id.billing_config_back_arrow)

        billingPeriodArr = ArrayList()
        billingPeriodArr.add("Monthly")
        billingPeriodArr.add("Yearly")




        if (bcFrom == "BillingOverview" || bcFrom == "BillingDetails"){
            bc_fName.setText(username)
            bc_email.setText(userEmail)
            bc_fName.isFocusable = false
            bc_fName.isEnabled = false
            bc_email.isFocusable = false
            bc_email.isEnabled = false
            bc_phone.isFocusable = false
            bc_phone.isEnabled = false
            bc_b_period.isFocusable = false
            bc_b_period.isEnabled = false

            bc_phone.setText(b_phone)
            bc_b_period.setText(b_b_period)
            bc_company.setText(b_company)
            bc_address1.setText(b_address1)
            bc_address2.setText(b_address2)
            bc_country.setText(b_country)
            bc_state.setText(b_state)
            bc_city.setText(b_city)
            bc_zip.setText(b_zip)

        }else{
            bc_fName.setText(username)
            bc_email.setText(userEmail)
            bc_company.setText(empCompany)
            bc_phone.setText(empPhone)
//            bc_fName.isFocusable = false
//            bc_fName.isEnabled = false
            bc_email.isFocusable = false
            bc_email.isEnabled = false
            bc_b_period.setText("Monthly")
        }


        countryArr = ArrayList()
        countryCodeArr = ArrayList()
        countryIdArr = ArrayList()
        stateArr = ArrayList()
        stateIdArr = ArrayList()
        cityArr = ArrayList()

        bc_confirm_proceed.setOnClickListener{
            validation()
        }
        configureBillingApiCall()
        onClickActions()
        companySubDetailsApiCall()
        searchBillingPeriod()
        createCustomerApiCall()
        employeeDetailsApiCall()
        searchCountry()
        searchState()
        searchCity()
        countryListApiCall("")
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private fun onClickActions(){
        billing_config_back_arrow.setOnClickListener{
            fragmentManager?.popBackStack()
        }
        bc_cancel.setOnClickListener{
            if (bcFrom == "BillingOverview"){
                context?.loadFragment(context!!,BillingOverviewFragment())
            }else if (bcFrom == "BillingDetails"){
                context?.loadFragment(context!!,BillingDetailsFragment())
            }else{
                context?.loadFragment(context!!,MoreFragment())
            }

        }
    }
    private fun companySubDetailsApiCall(){
        val params = HashMap<String,Any>()
        params["subscriptionId"] = subscriptionId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.company_subscription_details,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){
                        var dataobj = response.getJSONObject("data")
                        var name = dataobj.getString("name")
                        var logo = dataobj.getString("logo")
                        var type = dataobj.getString("type")
                        var level = dataobj.getString("level")
                        var role = dataobj.getString("role")
                        try {
                            Picasso.get().load(webService.imageBasePath+logo).placeholder(R.drawable.ic_bill_history).into(bConfig_img)
                        }catch (e:Exception){
                            println(e)
                        }
                        if (type == "1" || type == "3"){
                            bConfig_name.text = name
                        }else{
                            bConfig_name.text = name
                        }

                    }
                }catch (e:Exception){
                    println(e)
                }

            }
        }
    }
    var searchTextCountry = String()
    private fun searchCountry(){
     /*   bc_country.afterTextChanged {s ->
           searchTextCountry = s
            countryParentId = ""
          countryListApiCall(s)
        }*/
        bc_country.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

                if(s!!.length >= 3)
                {
                    println("check_status"+""+"testing")
                    countryListApiCall(s.toString())
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {



            }
        })


        var selectedItem = String()
        bc_country.onItemClickListener = object : AdapterView.OnItemClickListener{
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                try {
                    selectedItem = parent?.getItemAtPosition(position).toString()
                    bc_country.setText(selectedItem)
                    val selectedItemId = parent?.getItemIdAtPosition(position)
                    var  countryCode = countryCodeArr.get(selectedItemId!!.toInt())
                    var countryId = countryIdArr.get(selectedItemId.toInt())
                    countryParentId = countryId
                    println("countryCodeAt== $countryCode")
                    println("countryId== $countryId")

                    context?.setSharedPref(context!!,"countryCode",countryCode)
                    bc_country.dismissDropDown()
                    bc_country.clearFocus()
                    bc_state.text.clear()
                    bc_city.text.clear()

                }catch (e:Exception){
                    println("countryItemSelectionException== ${e.toString()}")
                }


            }

        }

        bc_country.setOnDismissListener {

        }
    }
    private fun searchState(){
        bc_state.afterTextChanged {st->
            if (countryParentId == "null" || countryParentId == ""){
//                context?.toast("Please select country")
                bc_state.text.clear()
                bc_country.requestFocus()
                stateParentId = ""
            }else{
                stateParentId = ""
                stateListApiCall(st,countryParentId)
            }

        }


    }
    private fun searchCity(){
        bc_city.afterTextChanged { ct->
            if (stateParentId == "null" || stateParentId == ""){
//                context?.toast("Please select state")
                bc_city.text.clear()
                bc_state.requestFocus()
            }else{
                cityListApiCall(ct,stateParentId)
            }
        }

    }

    private fun searchBillingPeriod(){
        var arrayAdapter = ArrayAdapter<String>(context!!,R.layout.support_simple_spinner_dropdown_item,billingPeriodArr)
        bc_b_period.setAdapter(arrayAdapter)
//        bc_b_period.threshold = 1
        bc_b_period.dropDownAnchor = R.id.bc_b_period
        bc_b_period.setOnClickListener{
            bc_b_period.showDropDown()

        }
        bc_b_period.onFocusChangeListener = View.OnFocusChangeListener{
                view, b ->
            if(b){
                bc_b_period.showDropDown()
            }
        }
        var selectedItem = String()
        bc_b_period.onItemClickListener = object : AdapterView.OnItemClickListener{
            override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedItem = parent?.getItemAtPosition(position).toString()
                bc_b_period.setText(selectedItem,false)
                val selectedItemId = parent?.getItemIdAtPosition(position)

                bc_b_period.dismissDropDown()
                bc_b_period.clearFocus()

            }

        }

        bc_b_period.setOnDismissListener {

        }

    }

    private fun configureBillingApiCall(){
        var params = HashMap<String,Any>()

        context?.let {
            apiRequest.getRequestBodyWithHeaders(it,webService.configure_billing_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (response.has("msg")){
                        var msg = response.getString("msg")
                    }
                    if (status == "200"){
                        var dataObj = response.getJSONObject("data")
                        var showConfigure = dataObj.getString("showConfigure")
                        context?.setSharedPref(context!!,"showConfigure",showConfigure)
//                        if (showConfigure == "false"){
//                            context?.loadFragment(context!!,BillingOverviewFragment())
//                        }
                        var subscribeSettingsId = dataObj.getString("subscribeSettingsId")
                        context?.setSharedPref(context!!,"subscriptionId",subscribeSettingsId)
                        var billingId = dataObj.getString("billingId")
                        context?.setSharedPref(context!!,"billingId",billingId)
                        var pricePerUser = dataObj.getString("pricePerUser")
                        context?.setSharedPref(context!!,"unit_amount",pricePerUser)
                    }
                }catch (e:Exception){
                    println(e)
                }
            }
        }
    }

    private fun createCustomerApiCall(){
        var params = HashMap<String,Any>()
        params["email"]= userEmail

        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.create_customer_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (response.has("msg")){
                        var msg = response.getString("msg")
                    }

                    if (status == "200"){
                        var dataObj =  response.getJSONObject("data")
                        var id = dataObj.getString("id")
                        context?.setSharedPref(context!!,"billing_customer_id",id)
                        var objectd = dataObj.getString("object")
                        var address = dataObj.getString("address")
                        var balance = dataObj.getString("balance")
                        var created = dataObj.getString("created")
                        var email = dataObj.getString("email")
                    }
                }catch (e:Exception){
                    println(e)
                }
            }
        }
    }

    private fun validation(){
        bcFrom = context?.getSharedPref("bcFrom",context!!).toString()
        if (bc_fName.text.toString().trim().isEmpty()){
            context?.toast("Please enter first name")
            bc_fName.requestFocus()

        }else if (bc_phone.text.toString().trim().isEmpty() && bcFrom == "null"){
            context?.toast("Please enter phone number")
            bc_phone.requestFocus()

        }
        else if (bc_email.text.toString().trim().isEmpty()){
            context?.toast("Please enter email address")
            bc_email.requestFocus()
        }else if (bc_company.text.toString().trim().isEmpty()){
            context?.toast("Please enter company name")
            bc_company.requestFocus()
        }else if (bc_b_period.text.toString().trim().isEmpty()){
            context?.toast("Please enter billing period")
            bc_b_period.requestFocus()
        }else if (bc_address1.text.toString().trim().isEmpty()){
            context?.toast("Please enter address line 1")
            bc_address1.requestFocus()
        }else if (bc_country.text.toString().trim().isEmpty()){
            context?.toast("Please enter country name")
            bc_country.requestFocus()
        }else if (bc_state.text.toString().trim().isEmpty()){
            context?.toast("Please enter state name")
            bc_state.requestFocus()
        }else if (bc_city.text.toString().trim().isEmpty()){
            context?.toast("Please enter city name")
            bc_city.requestFocus()
        }else if (bc_zip.text.toString().trim().isEmpty()){
            context?.toast("Please enter zip code")
            bc_zip.requestFocus()
        }
        else if (!isValidMobileRegex(bc_phone.text.toString().trim().replace("-","").replace("+","")) && bcFrom == "null"){
        context?.toast("Please enter the valid phone number")
            bc_phone.requestFocus()
    }
        else{
            if (bcFrom == "BillingOverview" || bcFrom == "BillingDetails"){
                billingContactApiCall()
            }
           else{
                context?.setSharedPref(context!!,"bc_fName",bc_fName.text.toString())
                context?.setSharedPref(context!!,"bc_lName",bc_lName.text.toString())
                context?.setSharedPref(context!!,"bc_phone",bc_phone.text.toString())
                context?.setSharedPref(context!!,"bc_email",bc_email.text.toString())
                context?.setSharedPref(context!!,"bc_company",bc_company.text.toString())
                context?.setSharedPref(context!!,"bc_b_period",bc_b_period.text.toString())
                context?.setSharedPref(context!!,"bc_address1",bc_address1.text.toString())
                context?.setSharedPref(context!!,"bc_address2",bc_address2.text.toString())
                context?.setSharedPref(context!!,"bc_country",bc_country.text.toString())
                context?.setSharedPref(context!!,"bc_state",bc_state.text.toString())
                context?.setSharedPref(context!!,"bc_city",bc_city.text.toString())
                context?.setSharedPref(context!!,"bc_zip",bc_zip.text.toString())
                context?.loadFragment(context!!,AddCardFragment())
            }
        }
    }

    val REGEX_MOBILE = "^[4-9]{1}[0-9]{9}\$"
    private fun isValidMobileRegex(phone: String): Boolean {
        return phone.matches(REGEX_MOBILE.toRegex()) && phone.trim().length <= 10
    }
    private fun billingContactApiCall(){
        var countryCode = context?.getSharedPref("countryCode",context!!).toString()
        val params = HashMap<String,Any>()
        params["addressLine1"]= bc_address1.text.toString()
        params["addressLine2"] = bc_address2.text.toString()
        params["city"] = bc_city.text.toString()
        params["companyName"] = bc_company.text.toString()
        params["country"] = bc_country.text.toString()
        params["countryCode"] = countryCode
        params["state"] = bc_state.text.toString()
        params["subscriptionId"] = subscriptionId
        params["zipCode"] = bc_zip.text.toString()
        params["billingId"] = billingId

        context?.let {
            apiRequest.putRequestBodyWithHeadersAny(it,webService.billing_contact_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        context?.toast(msg)
                        if (bcFrom == "BillingOverview"){
                            context?.loadFragment(context!!,BillingOverviewFragment())
                        }else if (bcFrom == "BillingDetails"){
                            context?.loadFragment(context!!,BillingDetailsFragment())
                        }
                    }else{
                        context?.toast(msg)
                    }

                }catch (e:Exception){
                    println(e)
                }
            }
        }
    }

    /*private fun countryListApiCall(searchTxt :String){

     val params = HashMap<String,Any>()
        params["search"] = searchTxt
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.country_list_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200") {
                        countryArr.clear()
                        countryCodeArr.clear()
                        countryIdArr.clear()
                        var dataArr = response.getJSONArray("data")
                        for (c in 0 until dataArr.length()) {
                            var dataObj = dataArr.getJSONObject(c)
                            var _id = dataObj.getString("_id")
                            var name = dataObj.getString("name")
                            var code = dataObj.getString("code")
                            countryIdArr.add(_id)
                            countryCodeArr.add(code)
                            countryArr.add(name)
                        }

                    }

                }catch (e:Exception){
                    println(e)
                }
                try {
                    var arrayAdapter = ArrayAdapter<String>(context!!,R.layout.support_simple_spinner_dropdown_item,countryArr)
                    bc_country.setAdapter(arrayAdapter)
                    bc_country.threshold = 1
                    bc_country.setOnClickListener{
                        bc_country.showDropDown()
                    }
                    bc_country.onFocusChangeListener = View.OnFocusChangeListener{
                            view, b ->
                        if(b){
                            bc_country.showDropDown()
                        }
                    }
                    var selectedItem = String()
                    bc_country.onItemClickListener = object : AdapterView.OnItemClickListener{
                        override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                            try {
                                selectedItem = parent?.getItemAtPosition(position).toString()
                                bc_country.setText(selectedItem)
                                val selectedItemId = parent?.getItemIdAtPosition(position)
                                var  countryCode = countryCodeArr.get(selectedItemId!!.toInt())
                                var countryId = countryIdArr.get(selectedItemId.toInt())
                                countryParentId = countryId

                                context?.setSharedPref(context!!,"countryCode",countryCode)
                                bc_country.dismissDropDown()
                                bc_country.clearFocus()
                                bc_state.text.clear()
                                bc_city.text.clear()

                            }catch (e:Exception){
                                println("countryItemSelectionException== ${e.toString()}")
                            }


                        }

                    }

                    bc_country.setOnDismissListener {

                    }

                }catch (e:Exception){
                    println(e)
                }

            }


        }
    }*/
    private fun countryListApiCall(searchTxt: String)
    {

        val params = HashMap<String, Any>()
        params["search"] = searchTxt
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(
                it,
                webService.country_list_url,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    if (status == "200") {
                        countryArr.clear()
                        countryCodeArr.clear()
                        countryIdArr.clear()
                        var dataArr = response.getJSONArray("data")
                        for (c in 0 until dataArr.length()) {
                            var dataObj = dataArr.getJSONObject(c)
                            var _id = dataObj.getString("_id")
                            var name = dataObj.getString("name")
                            var code = dataObj.getString("code")
                            countryIdArr.add(_id)
                            countryCodeArr.add(code)
                            countryArr.add(name)
                        }

                    }

                } catch (e: Exception) {
                    println(e)
                } finally {
                    var arrayAdapter = ArrayAdapter<String>(
                        context!!,
                        R.layout.support_simple_spinner_dropdown_item,
                        countryArr
                    )
                    bc_country.setAdapter(arrayAdapter)
                    arrayAdapter.notifyDataSetChanged()
                }

/*try {
var arrayAdapter = ArrayAdapter<String>(context!!,R.layout.support_simple_spinner_dropdown_item,countryArr)
bc_country.setAdapter(arrayAdapter)
bc_country.threshold = 1
bc_country.setOnClickListener{
bc_country.showDropDown()
}
bc_country.onFocusChangeListener = View.OnFocusChangeListener{
view, b ->
if(b){
bc_country.showDropDown()
}
}
var selectedItem = String()
bc_country.onItemClickListener = object : AdapterView.OnItemClickListener{
override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
try {
selectedItem = parent?.getItemAtPosition(position).toString()
bc_country.setText(selectedItem)
val selectedItemId = parent?.getItemIdAtPosition(position)
var countryCode = countryCodeArr.get(selectedItemId!!.toInt())
var countryId = countryIdArr.get(selectedItemId.toInt())
countryParentId = countryId
println("countryCodeAt== $countryCode")
println("countryId== $countryId")

context?.setSharedPref(context!!,"countryCode",countryCode)
bc_country.dismissDropDown()
bc_country.clearFocus()
bc_state.text.clear()
bc_city.text.clear()

}catch (e:Exception){
println("countryItemSelectionException== ${e.toString()}")
}


}

}

bc_country.setOnDismissListener {

}

}catch (e:Exception){
println(e)
}*/

            }


        }
    }
    private fun stateListApiCall(searchTxt: String,parentId:String){
        val params = HashMap<String,Any>()
        params["search"] = searchTxt
        params["parentId"] = parentId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.state_list_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){
                        stateIdArr.clear()
                        stateArr.clear()
                        var dataArr = response.getJSONArray("data")
                        for (s in 0 until dataArr.length()){
                            var dataObj = dataArr.getJSONObject(s)
                            var _id = dataObj.getString("_id")
                            var name = dataObj.getString("name")
                            var code = dataObj.getString("code")
                            stateArr.add(name)
                            stateIdArr.add(_id)
                        }
                    }


                }catch (e:Exception){
                    println(e)
                }

                var arrayAdapter = ArrayAdapter<String>(context!!,R.layout.support_simple_spinner_dropdown_item,stateArr)
                bc_state.setAdapter(arrayAdapter)
                bc_state.threshold = 1
//                bc_state.dropDownAnchor = R.id.bc_state

                bc_state.setOnClickListener{
                    bc_state.showDropDown()
                }
                bc_state.onFocusChangeListener = View.OnFocusChangeListener{
                        view, b ->
                    if(b){
                        bc_state.showDropDown()
                    }
                }
                var selectedItem = String()
                bc_state.onItemClickListener = object : AdapterView.OnItemClickListener{
                    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                        try {
                            selectedItem = parent?.getItemAtPosition(position).toString()
                            bc_state.setText(selectedItem)
                            val selectedItemId = parent?.getItemIdAtPosition(position)
                            var  stateId = stateIdArr.get(selectedItemId!!.toInt())
                            stateParentId = stateId
                            bc_state.dismissDropDown()
                            bc_state.clearFocus()
                            bc_city.text.clear()
                        }catch (e:Exception){
                            println(e)
                        }


                    }

                }

                bc_state.setOnDismissListener {

                }

            }
        }
    }
    private fun cityListApiCall(searchTxt: String,parentId: String){
        val params = HashMap<String,Any>()
        params["search"] = searchTxt
        params["parentId"] = parentId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.city_list_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){
                        cityArr.clear()
                        var dataArr = response.getJSONArray("data")
                        for (c in 0 until dataArr.length()){
                            var dataObj = dataArr.getJSONObject(c)
                            var _id = dataObj.getString("_id")
                            var name = dataObj.getString("name")
                            cityArr.add(name)
                    }

                    }
                }catch (e:Exception){
                    println(e)
                }
                try {
                    var arrayAdapter = ArrayAdapter<String>(context!!,R.layout.support_simple_spinner_dropdown_item,cityArr)
                    bc_city.setAdapter(arrayAdapter)
                    bc_city.threshold = 1
                    bc_city.setOnClickListener{
                        bc_city.showDropDown()

                    }
                    bc_city.onFocusChangeListener = View.OnFocusChangeListener{
                            view, b ->
                        if(b){
                            bc_city.showDropDown()
                        }
                    }
                    var selectedItem = String()
                    bc_city.onItemClickListener = object : AdapterView.OnItemClickListener{
                        override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                            try {
                                selectedItem = parent?.getItemAtPosition(position).toString()
                                bc_city.setText(selectedItem)
                                val selectedItemId = parent?.getItemIdAtPosition(position)
                                bc_city.dismissDropDown()
                                bc_city.clearFocus()
                            }catch (e:Exception){
                                println(e)
                            }

                        }

                    }

                    bc_country.setOnDismissListener {

                    }
                }catch (e:Exception){
                    print(e)
                }

            }
        }
    }


    private fun employeeDetailsApiCall()
    {
        val params = HashMap<String,Any>()
        context?.let {
            apiRequest.getRequestBodyWithHeaders(it,webService.billing_employee_details_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){
                        var dataObj = response.getJSONObject("data")
                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")
                        var email = dataObj.getString("email")
                        var phone = dataObj.getString("phone")
                        var companyName = dataObj.getString("companyName")
                        context?.setSharedPref(context!!,"",name)
                        context?.setSharedPref(context!!,"",email)
                        context?.setSharedPref(context!!,"empPhone",phone)
                        context?.setSharedPref(context!!,"empCompany",companyName)
                        bc_phone.setText(phone)
                        bc_company.setText(companyName)
                        context?.setSharedPref(context!!,"",_id)
                    }

                }catch (e:Exception){
                    println(e)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

}
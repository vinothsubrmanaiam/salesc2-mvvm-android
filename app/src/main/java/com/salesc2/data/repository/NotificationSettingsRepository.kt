package com.salesc2.data.repository

import com.salesc2.data.network.api.response.ResponseNotificationSettings
import com.salesc2.data.network.api.response.ResponseTeamList
import com.salesc2.data.network.api.service.NotificationSettingsApi
import com.salesc2.data.network.api.service.TeamListApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

interface NotificationSettingsRepository {
    suspend fun reqNotificationSettings (
        onSuccess: OnSuccess<ResponseNotificationSettings>,
        onFailure: OnFailure<ResponseNotificationSettings>
    )

    companion object Factory{
        fun create(api : NotificationSettingsApi) : NotificationSettingsRepository = NotificationSettingsRepositoryImpl(api)
    }


}
class NotificationSettingsRepositoryImpl(val api: NotificationSettingsApi) : NotificationSettingsRepository {
    override suspend fun reqNotificationSettings (
        onSuccess: OnSuccess<ResponseNotificationSettings>,
        onFailure: OnFailure<ResponseNotificationSettings>
    ) {


        withContext(Dispatchers.IO)
        {
            try {

                val response = api.reqNotificationSettings()
                if(response.isSuccessful)
                {
                    response.body().let {
                        if(it?.status?.equals("200")!!)
                        {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it!!)
                            }
                        }
                        else
                        {
                            onFailure(it!!)
                        }
                    }
                }

            }catch (e: Exception)
            {

            }
        }
    }

}
package com.salesc2.data.network.api.request

import com.google.gson.annotations.SerializedName

data class RequestBillingSubScription(
    @SerializedName("subscriptionId") val subscriptionId: String? = ""
)

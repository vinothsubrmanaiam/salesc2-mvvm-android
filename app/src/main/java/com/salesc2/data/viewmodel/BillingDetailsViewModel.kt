package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestBillingDetails
import com.salesc2.data.network.api.request.RequestMorePage
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseBillingCompanySubdetails
import com.salesc2.data.network.api.response.ResponseBillingDetails
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.api.response.ResponseMorePage
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.BillingDetailsRepository
import com.salesc2.data.repository.EmployeeLoginRepository
import com.salesc2.data.repository.MorePageRepository

import kotlinx.coroutines.launch

class BillingDetailsViewModel(
    private val repository: BillingDetailsRepository,
    val context: Context
) : ViewModel() {
     fun reqBillingDetails(
         requestBillingDetails: RequestBillingDetails,
         onSuccess: OnSuccess<ResponseBillingDetails>,
         onFailure: OnFailure<ResponseBillingDetails>
    )
     {
         viewModelScope.launch {
             repository.reqBillingDetails(requestBillingDetails,onSuccess,onFailure)
         }
     }


    fun reqBillingCompanySubDetails(
        requestBillingDetails: RequestBillingDetails,
        onSuccess: OnSuccess<ResponseBillingCompanySubdetails>,
        onFailure: OnFailure<ResponseBillingCompanySubdetails>
    )
    {
        viewModelScope.launch {
            repository.reqBillingCompanySubDetails(requestBillingDetails,onSuccess,onFailure)
        }
    }

}
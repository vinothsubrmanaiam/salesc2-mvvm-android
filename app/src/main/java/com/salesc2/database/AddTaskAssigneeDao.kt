package com.salesc2.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface AddTaskAssigneeDao{
    @Query("SELECT * FROM AddTaskAssignee")
    suspend fun getAll(): List<AddTaskAssignee>

    @Insert
    suspend fun insertAll(vararg addTaskAssignee: AddTaskAssignee)

    @Query("DELETE FROM AddTaskAssignee")
    fun deleteAll()
}
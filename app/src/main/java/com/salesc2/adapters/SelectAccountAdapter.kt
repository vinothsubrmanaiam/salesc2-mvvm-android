package com.salesc2.adapters

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.fragment.SelectDepartmentFragment
import com.salesc2.model.SelectAccountModel
import com.salesc2.model.SelectDeptModel
import com.salesc2.utils.ShowNoData
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.setSharedPref
import java.util.*

class SelectAccountAdapter(context: Context?, var showdata: ShowNoData, var accList: ArrayList<SelectAccountModel>) : RecyclerView.Adapter<SelectAccountAdapter.viewHolder>(),Filterable {

    private lateinit var accountItems: ArrayList<SelectAccountModel>
    var context: Context? = null
    var acc_filter_position = String()
private  var row_index = -1
    var resultCount:Int = 0
    init{
        this.context = context
        accountItems = accList

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_select_list, parent, false)
        acc_filter_position = context?.getSharedPref("acc_filter_position",context!!).toString()
        return viewHolder(view)
    }

    override fun getItemCount(): Int {
       return accountItems.size
    }

    var checked = true
    override fun onBindViewHolder(holder: viewHolder, position: Int) {
       var selectAccountModel : SelectAccountModel = accountItems[position]

        val strArrayOBJ = selectAccountModel.acc_name.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val builder = StringBuilder()
        for (s in strArrayOBJ) {
            val cap = s.substring(0, 1).toUpperCase() + s.substring(1)
            builder.append("$cap ")
        }
        holder.name.text=builder.toString()

        acc_filter_position = context?.getSharedPref("acc_filter_position",context!!).toString()
        if (acc_filter_position != "null"){
            row_index=acc_filter_position.toInt()
            if (row_index.equals(acc_filter_position)){
                holder.select_icon.visibility = View.VISIBLE
                context?.let { ContextCompat.getColor(it, R.color.offwhite2) }?.let {
                    holder.list_layout.setBackgroundColor(it)
                }
            }
        }

        holder.list_layout.setOnClickListener(View.OnClickListener {
            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("acc_filter_position")
            editor.commit()
            if (checked) {
                row_index=position
                notifyDataSetChanged()
                checked = false
                holder.select_icon.visibility = View.VISIBLE
                context?.let { ContextCompat.getColor(it, R.color.offwhite2) }?.let {
                    holder.list_layout.setBackgroundColor(it)
                }
            }else{
                checked = true
            }

            context?.setSharedPref(context!!,"acc_filter_id",selectAccountModel.acc_id)
            context?.setSharedPref(context!!,"acc_filter_name",selectAccountModel.acc_name)

            var bundle = Bundle()
            var fragment = SelectDepartmentFragment()
            bundle.putString("account_id",selectAccountModel.acc_id)
            fragment.arguments = bundle
            val activity = context as AppCompatActivity
            activity.supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragmet_container, fragment).addToBackStack(null).commit()

        })

        if(row_index==position){
            holder.select_icon.visibility = View.VISIBLE
            context?.setSharedPref(context!!,"acc_filter_position",position.toString())

        }
        else
        {
            holder.select_icon.visibility = View.GONE
        }


    }



    class viewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView) {
        var name : TextView = itemView.findViewById(R.id.select_list_name)
        val list_layout : ConstraintLayout = itemView.findViewById(R.id.list_layout)
        var select_icon : ImageView = itemView.findViewById(R.id.select_icon)
    }
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    accountItems = accList

                } else {
                    val resultList = ArrayList<SelectAccountModel>()
                    for (row in accList) {
                        if (row.acc_name.toLowerCase(Locale.ROOT)
                                .contains(charSearch.toLowerCase(Locale.ROOT))
                        ) {

                            resultList.add(row)
                        }

                    }
                    accountItems = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = accountItems
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                accountItems = results?.values as ArrayList<SelectAccountModel>

                if (accountItems.size==0) {
                    showdata.showNoData()

                }
                else
                {
                    showdata.hideNoData()
                    notifyDataSetChanged()
                    resultCount = results.count.toInt()
                }

            }



        }
    }
}
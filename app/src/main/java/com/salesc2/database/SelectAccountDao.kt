package com.digi.store.util.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.salesc2.database.DashBoard
import com.salesc2.database.SelectAccount


@Dao
interface SelectAccountDao {

   @Query("SELECT * FROM SelectAccount")
    suspend fun getAll(): List<SelectAccount>



    @Insert
    suspend fun insertAll(vararg dashBoard: SelectAccount)


    @Query("DELETE FROM SelectAccount")
    fun deleteAll()


}

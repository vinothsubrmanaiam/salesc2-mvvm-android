package com.salesc2.data.network.api.response

data class ResponseBillingCompanySubdetails(
    val `data`: Data,
    val msg: String,
    val status: Int
) {
    data class Data(
        val level: Any,
        val logo: Any,
        val name: String,
        val role: String,
        val type: Int
    )
}
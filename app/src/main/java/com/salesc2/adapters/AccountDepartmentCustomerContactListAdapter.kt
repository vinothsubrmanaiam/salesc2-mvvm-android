package com.salesc2.adapters

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.adapters.AccountDepartmentCustomerContactListAdapter.viewHolder
import com.salesc2.model.AccountDepartmentPhoneModel
import java.util.*

class AccountDepartmentCustomerContactListAdapter() : RecyclerView.Adapter<viewHolder>() {

    private lateinit var accountphoneList: ArrayList<AccountDepartmentPhoneModel>
    var context: Context? = null

    constructor(context: Context?, accountphoneList: ArrayList<AccountDepartmentPhoneModel>) : this() {
        this.context = context
        this.accountphoneList = accountphoneList
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AccountDepartmentCustomerContactListAdapter.viewHolder {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.account_detail_dept_phoneno, parent, false)
        return AccountDepartmentCustomerContactListAdapter.viewHolder(view)
    }


     override fun onBindViewHolder(
        holder: viewHolder,
        position: Int
    ) {
         val accountDepartmentPhoneModel: AccountDepartmentPhoneModel =
             accountphoneList.get(position)
         holder.phone_tv?.text = accountDepartmentPhoneModel.phoneNo
         holder.phone_type_tv?.text = "(" + accountDepartmentPhoneModel.phoneType + ")"
         holder.call_layout?.setOnClickListener(View.OnClickListener {

             try {

                 var callIntent: Intent = Intent(
                     Intent.ACTION_DIAL,
                     Uri.parse("tel: ${accountDepartmentPhoneModel.phoneNo}")
                 )
                 context?.startActivity(callIntent)

             } catch (activityException: ActivityNotFoundException) {
                 Log.e("Calling a Phone Number", "Call failed", activityException)
             }

         })

     }

    override fun getItemCount(): Int {
        return accountphoneList.size
    }

    class viewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var phone_tv: TextView?  = itemView.findViewById(R.id.phone_tv)
        var phone_type_tv: TextView?  = itemView.findViewById(R.id.phone_type_tv)
        var call_layout: ConstraintLayout? = itemView.findViewById(R.id.call_layout)

    }
}
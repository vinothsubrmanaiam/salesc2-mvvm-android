package com.salesc2.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.salesc2.R
import com.salesc2.utils.progressBarDismiss

class AccountDetailInformationFragment : Fragment() {

    lateinit var mView: View
    lateinit var description_tv: TextView
    lateinit var website_tv: TextView
    lateinit var terrirory_tv: TextView

    var website: String = "NA"
    var description: String = "NA"
    var territory: String = "NA"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_account_detail_information, container, false)

        //initializing views
        initViews(mView)

        return mView
    }


    //initializing views
    private fun initViews(mView: View?) {

        terrirory_tv = mView!!.findViewById(R.id.terrirory_tv)
        website_tv = mView.findViewById(R.id.website_tv)
        description_tv = mView.findViewById(R.id.description_tv)


        website = arguments?.getString("website").toString()
        description = arguments?.getString("description").toString()
        territory = arguments?.getString("territory").toString()

        if (territory == ""){
            terrirory_tv.text = "NA"
        }else{
            terrirory_tv.text = territory
        }

        if (description == ""){
            description_tv.text = "NA"
        }else{
            description_tv.text = description
        }
        if (website == ""){
            website_tv.text = "NA"
        }else{
            website_tv.text = website
        }



        website_tv.setOnClickListener(View.OnClickListener {
            if (website != "") {
                val openURL = Intent(android.content.Intent.ACTION_VIEW)
                openURL.data = Uri.parse("http://" + website)
                startActivity(openURL)
            }
        })

    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(requireContext())
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(requireContext())
    }
}
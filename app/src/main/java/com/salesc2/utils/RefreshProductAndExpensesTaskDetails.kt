package com.salesc2.utils

interface RefreshProductAndExpensesTaskDetails {
    fun refreshProductAndExpenses()
}
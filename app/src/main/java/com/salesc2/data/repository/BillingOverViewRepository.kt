package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestBillingOverView
import com.salesc2.data.network.api.request.RequestMorePage
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseBillingOverview
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.api.response.ResponseMorePage
import com.salesc2.data.network.api.service.BillingOverViewApi
import com.salesc2.data.network.api.service.MorePageApi
import com.salesc2.data.network.api.service.UserLoginApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface BillingOverViewRepository {

    suspend fun reqGetSubscription(
        requestBillingOverView: RequestBillingOverView,
        onSuccess: OnSuccess<ResponseBillingOverview>,
        onFailure: OnFailure<ResponseBillingOverview>
    )

    companion object Factory {
        fun create(api: BillingOverViewApi): BillingOverViewRepository = BillingOverViewRepositoryImpl(api)
    }
}

private class BillingOverViewRepositoryImpl(private val api: BillingOverViewApi) : BillingOverViewRepository {
    override suspend fun reqGetSubscription(
        requestBillingOverView: RequestBillingOverView,
        onSuccess: OnSuccess<ResponseBillingOverview>,
        onFailure: OnFailure<ResponseBillingOverview>
    ) {
        withContext(Dispatchers.IO) {
            try {
                val response = api.reqGetSubscription(requestBillingOverView = requestBillingOverView)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status.equals("200")) {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }

    }

}

package com.salesc2.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.provider.MediaStore
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.github.ivbaranov.mli.MaterialLetterIcon
import com.github.siyamed.shapeimageview.RoundedImageView
import com.google.android.gms.location.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.textfield.TextInputEditText
import com.hr.data.network.di.ImageConstants
import com.hr.data.network.di.showDialogToPick
import com.salesc2.R
import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.repository.ManageAccountRepository
import com.salesc2.data.viewmodel.ManageAccountViewModel
import com.salesc2.data.viewmodel.UserLoginViewModel
import com.salesc2.service.ApiRequest

import com.salesc2.service.WebService
import com.salesc2.service.imageupload.ImageUploadAPI
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.squareup.picasso.Picasso
import net.simplifiedcoding.imageuploader.UploadRequestBody
import net.simplifiedcoding.imageuploader.UploadResponse
import net.simplifiedcoding.imageuploader.getFileName
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.*

import java.io.*
import java.util.*
import kotlin.collections.HashMap

class ManageProfileFragment : Fragment(), UploadRequestBody.UploadCallback {

    private lateinit var manage_account_back_arrow: ImageView
    private lateinit var manage_acc_done: TextView
    private lateinit var manage_account_image: RoundedImageView
    private lateinit var manage_account_letterImage: MaterialLetterIcon
    private lateinit var manage_account_job_tv: TextView
    private lateinit var manage_account_member_name: TextView

    private lateinit var manage_acc_address: TextInputEditText
    private lateinit var manage_acc_phone: TextInputEditText
    private lateinit var manage_acc_division: TextInputEditText
    private lateinit var manage_acc_area: TextInputEditText
    private lateinit var manage_acc_email: TextInputEditText
    private lateinit var manage_acc_region: TextInputEditText
    private lateinit var manage_acc_territory: TextInputEditText



    private lateinit var edit_image: ImageView
    private var selectedImageUri: Uri? = null
    private var userRole = String()
    private var userName = String()
    private var userImage = String()
    private var userDivision = String()
    private var userArea = String()
    private var userPhone = String()
    private var userAddress = String()
    private var userEmail = String()
    private var userRegion = String()
    private var userTerritory = String()

    private val GALLERY = 1
    private val CAMERA = 2
    var imageBase64 = String()

    private var apiRequest = ApiRequest()
    private var webService = WebService()
    lateinit var mView: View

    private var locationManager: LocationManager? = null
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    var PERMISSION_ID = 101
    private var lattitude = String()
    private var longitude = String()
    private val PLACE_PICKER_REQUEST = 5

    private val IMAGE_PICK_CODE = 999

    private val READ_EXTERNAL_STORAGE_REQUEST_CODE = 1001

    private var imageData: ByteArray? = null
    private val postURL: String =
        "https://ptsv2.com/t/54odo-1576291398/post" // remember to use your own api


    private val manageAccountViewModel by viewModel<ManageAccountViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_manage_profile, container, false)

        mFusedLocationClient =
            context?.let { LocationServices.getFusedLocationProviderClient(it) }!!
        // currentLocationName()
        locationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        manage_account_back_arrow = mView.findViewById(R.id.manage_account_back_arrow)
        manage_acc_done = mView.findViewById(R.id.manage_acc_done)
        manage_account_image = mView.findViewById(R.id.manage_account_iv)
        manage_account_letterImage = mView.findViewById(R.id.manage_account_letterImage)
        manage_account_job_tv = mView.findViewById(R.id.manage_account_job_tv)
        manage_account_member_name = mView.findViewById(R.id.manage_account_profile_name)
        manage_acc_address = mView.findViewById(R.id.manage_acc_address)
        manage_acc_phone = mView.findViewById(R.id.manage_acc_phone)
        manage_acc_division = mView.findViewById(R.id.manage_acc_division)
        manage_acc_area = mView.findViewById(R.id.manage_acc_area)
        manage_acc_email = mView.findViewById(R.id.manage_acc_email)
        manage_acc_region = mView.findViewById(R.id.manage_acc_region)
        manage_acc_territory = mView.findViewById(R.id.manage_acc_territory)

        edit_image = mView.findViewById(R.id.edit_image)

       profileDetailsApiCall()
        //profiledetailsApicallWithMvvm()
        onClickAction()

        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility =
            View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility =
            View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility =
            View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        getLastLocation()
        addressClick()
        return mView
    }

    private fun addressClick() {
        manage_acc_address.setOnClickListener {
            manage_acc_phone.clearFocus()
            placeAutoComplete()
        }
    }

    val REGEX_MOBILE = "^[4-9]{1}[0-9]{9}\$"
    private fun onClickAction() {
        manage_account_back_arrow.setOnClickListener {
            fragmentManager?.popBackStack()
        }
        manage_acc_done.setOnClickListener {
            //  context?.toast("Clicked done")
             manage_acc_done.isEnabled = false
              val imm: InputMethodManager =
                  activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
              imm.hideSoftInputFromWindow(view!!.windowToken, 0)
              if (manage_acc_phone.text.toString().trim().replace("-","").length < 10 || manage_acc_phone.text.toString().trim().length > 10){
                  context?.toast("Please enter the valid phone number")
                  manage_acc_done.isEnabled = true
              }else if (manage_acc_phone.text.toString().trim().isBlank()){
                  context?.toast("Please enter the valid phone number")
                  manage_acc_done.isEnabled = true
              } else if(!isValidMobile(manage_acc_phone.text.toString().trim().replace("-",""))){
                  context?.toast("Please enter the valid phone number")
                  manage_acc_done.isEnabled = true
              }else if (!isValidMobileRegex(manage_acc_phone.text.toString().trim().replace("-",""))){
                  context?.toast("Please enter the valid phone number")
                  manage_acc_done.isEnabled = true
              }
              else {
                  manage_acc_done.isEnabled = true
                  if (selectedImageUri == null) {
                      profileApiCall()
                  } else {
                      uploadImage_ret()
                  }

              }


        }
        edit_image.setOnClickListener {
            // showPictureDialog()
            onImagePicker()

        }


    }
    private fun onImagePicker() {
        if (checkRuntimePermission()) {
            this.showDialogToPick()
        } else {
            requestRuntimePermission()
        }
    }

    private fun checkRuntimePermission(): Boolean {
        val writeablePermission =
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
        if (writeablePermission != PackageManager.PERMISSION_GRANTED) {
            return false
        }
        return true
    }


    private fun requestRuntimePermission() {
        requestPermissions(
            arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA
            ), READ_EXTERNAL_STORAGE_REQUEST_CODE
        )
    }
    private fun isValidMobileRegex(phone: String): Boolean {
        return phone.matches(REGEX_MOBILE.toRegex()) && phone.trim().length == 10
    }

    private fun isValidMobile(mobile: String): Boolean {

        return !TextUtils.isEmpty(mobile) && Patterns.PHONE.matcher(mobile).matches()
    }

    private fun profileApiCall() {
        val params = HashMap<String,Any>()

        params["phone"] = manage_acc_phone.text.toString()
        params["profilePhoto"] = ""
        if (manage_acc_address.text.toString() != "NA"){
            params["address"] = manage_acc_address.text.toString()
            params["lat"] = lattitude
            params["lng"] = longitude
        }


        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.Profile_update_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        context?.toast(msg)
                        context?.loadFragment(context!!,ManageAccountFragment())
                    }else{
                        context?.toast(msg)
                    }
                }catch (e:Exception){
                    println(e)
                    context?.toast(e.toString())
                }

            }
        }


    }

    private fun showPictureDialog() {

        val pictureDialog = AlertDialog.Builder(context)
        pictureDialog.setTitle("Select Action")
        val pictureDialogItems = arrayOf("Gallery", "Camera")
        pictureDialog.setItems(
            pictureDialogItems
        ) { dialog, which ->
            when (which) {
                0 -> choosePhotoFromGallery()
                1 -> takePhotoFromCamera()
            }
        }
        pictureDialog.show()
    }

    private fun choosePhotoFromGallery() {
        if ((context?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            } !== PackageManager.PERMISSION_GRANTED)) {
            requestPermissions(
                arrayOf<String>(Manifest.permission.READ_EXTERNAL_STORAGE),
                2000
            )
        } else {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, IMAGE_PICK_CODE)
        }

    }

    private fun takePhotoFromCamera() {
        if (checkPermission()) {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, IMAGE_PICK_CODE)
        } else {
            requestPermission()
        }
    }

    //check camera permission
    private fun checkPermission(): Boolean {
        if ((context?.let {
                ContextCompat.checkSelfPermission(
                    it,
                    Manifest.permission.CAMERA
                )
            } !== PackageManager.PERMISSION_GRANTED)) {
            // Permission is not granted
            return false
        }
        return true
    }

    var PERMISSION_REQUEST_CODE = 10001
    private fun requestPermission() {
        activity?.let {
            ActivityCompat.requestPermissions(
                it,
                arrayOf<String>(Manifest.permission.CAMERA),
                PERMISSION_REQUEST_CODE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(context, "Permission Granted", Toast.LENGTH_SHORT).show()
                // main logic
            } else {
                Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if ((context?.let {
                            ContextCompat.checkSelfPermission(
                                it,
                                Manifest.permission.CAMERA
                            )
                        } !== PackageManager.PERMISSION_GRANTED)) {
                        showMessageOKCancel("You need to allow access permissions",
                            DialogInterface.OnClickListener { dialog, which ->
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermission()
                                }
                            })
                    }
                }
            }
        }
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }

    private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", null)
            .create()
            .show()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                val place = Autocomplete.getPlaceFromIntent(data!!);

                var lat = place.latLng?.latitude
                lattitude = lat.toString()
                var lng = place.latLng?.longitude
                longitude = lng.toString()
                manage_acc_address.setText("${place.address}")
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                var status = Autocomplete.getStatusFromIntent(data!!)
                Log.i("address", status.getStatusMessage().toString());
                print("addressPicker== ${status.statusMessage}")
                manage_acc_address.setText(userAddress)
            } else {
                manage_acc_address.setText(userAddress)
            }

        }
        if (data != null && resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                ImageConstants.GALLERY -> {
                    val selectedImage = data.data
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(
                            context?.contentResolver!!,
                            selectedImage
                        )
                    val result = getRealPathFromURI(data.data?.toString())
                    val file = File(result)

                    selectedImageUri = data?.data
                    manage_account_image!!.setImageBitmap(bitmap)

                }
                ImageConstants.CAMERA -> {
                    val bitmap = data.extras?.get("data") as Bitmap
                    selectedImageUri = getImageUri(requireContext(), bitmap)
                    manage_account_image!!.setImageBitmap(bitmap)
                }
            }
        }



    }


    fun getRealPathFromURI(contentURI: String?): String? {
        val contentUri = Uri.parse(contentURI)
        val cursor: Cursor? =
            activity?.contentResolver?.query(contentUri, null, null, null, null)
        return if (cursor == null) contentUri.path else {
            cursor.moveToFirst()
            val idx: Int = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            cursor.getString(idx)
        }
    }

    fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.PNG, 100, bytes)
        val path: String =
            MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "profile_Photo", "")
        return Uri.parse(path)
    }


    companion object {
        private val IMAGE_DIRECTORY = "/recent"
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                activity?.let {
                    mFusedLocationClient.lastLocation.addOnCompleteListener(it) { task ->
                        var location: Location? = task.result
                        if (location == null) {
                            requestNewLocationData()
                        } else {
                            lattitude = location.latitude.toString()
                            println("latt1_in_pl== $lattitude")
                            println("\n latttttttt")
                            println(location.latitude.toString())
                            longitude = location.longitude.toString()
                        }
                    }
                }
            } else {
                Toast.makeText(context, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    private fun checkPermissions(): Boolean {
        if (context?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            } == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        activity?.let {
            ActivityCompat.requestPermissions(
                it,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                PERMISSION_ID
            )
        }
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager =
            activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient =
            context?.let { LocationServices.getFusedLocationProviderClient(it) }!!
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            lattitude = mLastLocation.latitude.toString()
            println("latt === $lattitude")
            longitude = mLastLocation.longitude.toString()
            println("lon == $longitude")
        }
    }

    private fun placeAutoComplete() {

        if (!Places.isInitialized()) {
            Places.initialize(context!!, "AIzaSyD6L3A5wCPVO2mt3KeLTZ-fnl2kIEAs690", Locale.ENGLISH);
        }
        var fields = Arrays.asList(
            Place.Field.ID,
            Place.Field.NAME,
            Place.Field.LAT_LNG,
            Place.Field.ADDRESS
        )
        var intent =
            Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(context!!)
        startActivityForResult(intent, PLACE_PICKER_REQUEST)

    }

    fun profiledetailsApicallWithMvvm()
    {
        manageAccountViewModel.reqGetAccountDetails(
            {
                println("result_profile"+it.data)
            },{

            }
        )
    }

    private fun profileDetailsApiCall() {
        println("Manage_profile"+" "+ webService.Profile_update_url)

        var params = HashMap<String, Any>()
        context?.let {
            apiRequest.getRequestBodyWithHeaders(
                it,
                webService.Profile_update_url,
                params
            ) { response ->
                try {
                    println("Manage_profile"+" "+ response)
                    println("Manage_profile"+" "+ params)

                    var status = response.getString("status")
                    if (status == "200") {
                        var dataObj = response.getJSONObject("data")

                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")
                        var role = dataObj.getString("role")
                        var email = dataObj.getString("email")
                        var phone = dataObj.getString("phone")

                        var profilePath = String()
                        if (dataObj.has("profilePath")){
                            profilePath = dataObj.getString("profilePath")
                        }

                        var imgwithBase = webService.imageBasePath + profilePath
                        var address = "NA"
                        var division = "NA"
                        var area = "NA"
                        var region = "NA"
                        var territory = "NA"
                        if (dataObj.has("address")) {
                            address = dataObj.getString("address")
                        }
                        userAddress = address
//                        var comapanyName = dataObj.getString("comapanyName")
                        if (dataObj.has("division")) {
                            division = dataObj.getString("division")
                        }
                        if (dataObj.has("area")) {
                            area = dataObj.getString("area")
                        }

                        if (dataObj.has("region")) {
                            region = dataObj.getString("region")
                        }
                        if (dataObj.has("territory")) {
                            territory = dataObj.getString("territory")
                        }


                        var locationObj = dataObj.getJSONObject("location")
                        if (locationObj.has("lng")){
                            longitude = locationObj.getString("lng")
                        }
                       if (locationObj.has("lat")) {
                           lattitude = locationObj.getString("lat")
                       }

                        try {
                            Picasso.get().load(imgwithBase).placeholder(R.drawable.ic_dummy_user_pic).into(manage_account_image)
                        } catch (e: Exception) {
                            println(e)
                        }
                        if (role.equals("Sales Rep",ignoreCase = true)){
                            manage_account_job_tv.text  = "Representative"
                        }else if (role.equals("Team Lead",ignoreCase = true)){
                            manage_account_job_tv.text ="Regional Manager"
                        }else{
                            manage_account_job_tv.text = role
                        }
                        manage_account_member_name.text = name
                        manage_acc_address.setText(address)
                        manage_acc_phone.setText(phone)
                        manage_acc_division.setText(division)
                        manage_acc_area.setText(area)
                        manage_acc_email.setText(email)
                        manage_acc_region.setText(region)
                        manage_acc_territory.setText(territory)

                    } else {
                        context?.toast("Something went wrong, Try again later")
                    }
                } catch (e: java.lang.Exception) {
                    println(e)
//                    context?.toast(e.toString())
                }
            }
        }
    }

    override fun onProgressUpdate(percentage: Int) {
        println("Hi_testing" + " " + percentage.toString())
    }

    private fun uploadImage_ret() {


        val parcelFileDescriptor =
            requireActivity().contentResolver.openFileDescriptor(selectedImageUri!!, "r", null)
                ?: return

        val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
        val file = File(
            requireActivity().cacheDir,
            requireActivity().contentResolver.getFileName(selectedImageUri!!)
        )
        val outputStream = FileOutputStream(file)
        inputStream.copyTo(outputStream)
        var authToken = context?.getSharedPref("token", context!!).toString()
        val body = UploadRequestBody(file, "image", this)
        ImageUploadAPI().uploadImage( "Bearer $authToken",
            MultipartBody.Part.createFormData(
                "profilePhoto",
                file.name,
                body
            ),


            RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                manage_acc_phone.text.toString()
            ),
            RequestBody.create(
                "multipart/form-data".toMediaTypeOrNull(),
                manage_acc_address.text.toString()
            ),
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), lattitude),
            RequestBody.create("multipart/form-data".toMediaTypeOrNull(), longitude)


        ).enqueue(object : Callback<UploadResponse> {
            override fun onFailure(call: Call<UploadResponse>, t: Throwable) {
                context?.toast(t.message.toString())
            }

            override fun onResponse(
                call: Call<UploadResponse>,
                response: retrofit2.Response<UploadResponse>
            ) {
                response.body()?.let {
                    context?.toast(it.msg)
                    var imageWithBase = webService.imageBasePath + it.profilePath
                    requireActivity().setSharedPref(  requireActivity(), "profile_image", imageWithBase)
                }
            }
        })

    }
}
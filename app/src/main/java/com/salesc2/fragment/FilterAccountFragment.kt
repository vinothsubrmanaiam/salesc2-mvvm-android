package com.salesc2.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.adapters.FilterAccountAdapter
import com.salesc2.adapters.SelectAccountAdapter
import com.salesc2.model.SelectAccountModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.progressBarDismiss
import java.util.HashMap

//not in use
class FilterAccountFragment : Fragment() {

    var webService = WebService()
    var apiRequest = ApiRequest()
    lateinit var filterAccountSearch : SearchView
    lateinit var filterAccountListRecyclerView: RecyclerView
    lateinit var selectAccountModel: ArrayList<SelectAccountModel>

    private lateinit var mView : View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_filter_account, container, false)
        filterAccountSearch = mView.findViewById(R.id.filterAccount_SearchView)
        filterAccountListRecyclerView = mView.findViewById(R.id.filterAccount_recyclerView)
        selectAccountModel = ArrayList()
        filterAccountApiCall()
        searchAccount()

        activity?.findViewById<ImageView>(R.id.app_icon_imageView)?.visibility = View.GONE
        return mView
    }

    private var searchtext = String()
    private fun searchAccount() {
        filterAccountSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchtext = newText
                selectAccountModel.clear()
                filterAccountApiCall()
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchtext = query
                selectAccountModel.clear()
                filterAccountApiCall()

                return true
            }

            override fun equals(other: Any?): Boolean {
                return super.equals(other)

            }


        })
    }
    private fun filterAccountApiCall(){
        var params = HashMap<String, String>()
        params.put("search",searchtext)
        context?.let {
            apiRequest.postRequestBodyWithHeaders(it,webService.Select_account_list_url,params){ response->

                var status = response.getString("status")
                if (status == "200"){
                    var dataArray = response.getJSONArray("data")
                    for (i in 0 until dataArray.length()){
                        var dataObj = dataArray.getJSONObject(i)
                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")
                        selectAccountModel.add(SelectAccountModel(_id,name))
                        context?.progressBarDismiss(context!!)
                    }
                    setupFilterAccountRecyclerView(selectAccountModel)
                }else{
                    context?.progressBarDismiss(context!!)
                }
            }
        }
    }
    //    setup adapter
    private fun setupFilterAccountRecyclerView(selectAccountModel: ArrayList< SelectAccountModel>) {
        val adapter: RecyclerView.Adapter<*> = FilterAccountAdapter(context, selectAccountModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        filterAccountListRecyclerView.layoutManager = llm
        filterAccountListRecyclerView.adapter = adapter
    }


    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }
}
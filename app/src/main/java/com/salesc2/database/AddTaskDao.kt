package com.salesc2.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface AddTaskDao {
    @Query("SELECT * FROM AddTask")
    suspend fun getAll(): List<AddTask>

    @Insert
    suspend fun insertAll(vararg addTask: AddTask)

    @Query("DELETE FROM AddTask")
    fun deleteAll()

    @Query("DELETE FROM AddTask WHERE id = :id")
    fun deleteById(id: Int)
}
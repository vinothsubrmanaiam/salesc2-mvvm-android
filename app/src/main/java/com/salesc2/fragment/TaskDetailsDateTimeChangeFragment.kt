package com.salesc2.fragment

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import java.lang.Exception
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.HashMap

class TaskDetailsDateTimeChangeFragment:Fragment() {
    lateinit var mView: View
    lateinit var start_date : TextView
    lateinit var start_time : TextView
    lateinit var dt_tomorrow : TextView
    lateinit var dt_nextday1 : TextView
    lateinit var dt_nextday2 : TextView
    lateinit var dt_nextday3 : TextView

    lateinit var end_date : TextView
    lateinit var end_time : TextView
    lateinit var dt_30min : TextView
    lateinit var dt_1hr : TextView
    lateinit var dt_2hr : TextView
    lateinit var dt_3hr : TextView
    lateinit var dt_4hr : TextView

    lateinit var dt_add : Button
    private lateinit var close : ImageView

    private lateinit var goback : ImageView
    val myCalendar = Calendar.getInstance()

    var dt : DateTimeFormatter? = null
    var now: LocalDateTime? = null

    var webService = WebService()
    var apiRequest = ApiRequest()

    private var taskID = String()
    private lateinit var dt_title_toolbar:TextView
//    private var taskStDate = String()
//    private var taskEdDate  = String()
private var set_timezone = String()
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_select_date_time, container, false)
        taskID = arguments?.getString("task_id").toString()
        set_timezone = context?.getSharedPref("set_timezone",context!!).toString()

        start_date = mView.findViewById(R.id.start_date_tv)
        start_time = mView.findViewById(R.id.start_time)
        dt_tomorrow = mView.findViewById(R.id.dt_tomorrow)
        dt_nextday1 = mView.findViewById(R.id.dt_nextday1)
        dt_nextday2 = mView.findViewById(R.id.dt_nextday2)
        dt_nextday3 = mView.findViewById(R.id.dt_nextday3)

        end_date = mView.findViewById(R.id.end_date_tv)
        end_time = mView.findViewById(R.id.end_time)
        dt_30min = mView.findViewById(R.id.dt_30min)
        dt_1hr  = mView.findViewById(R.id.dt_1hr)
        dt_2hr  = mView.findViewById(R.id.dt_2hr)
        dt_3hr  = mView.findViewById(R.id.dt_3hr)
        dt_4hr  = mView.findViewById(R.id.dt_4hr)

        goback = mView.findViewById(R.id.select_dt_back_arrow)
        dt_add = mView.findViewById(R.id.dt_add)

        close = mView.findViewById(R.id.close_iv)
        dt_title_toolbar = mView.findViewById(R.id.dt_title_toolbar)
        dt_title_toolbar.setText("Task Details")


        val dtf: DateTimeFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy")
         now  = if (set_timezone != "null"){
            LocalDateTime.now(ZoneId.of(set_timezone))
        }else{
            LocalDateTime.now()
        }
        start_date.text = dtf.format(now)
        end_date.text = start_date.text.toString()

        dt = DateTimeFormatter.ofPattern("hh:mm a")
        start_time.text = dt?.format(now)
        end_time.text =dt?.format(now?.plusMinutes(60))






        goBackAction()
        dtAction()
        closeAction()
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private fun closeAction(){
        close.setOnClickListener{
            context?.loadFragment(context!!,TaskDetailsFragment())
        }
    }
    private fun goBackAction(){
        goback.setOnClickListener(View.OnClickListener {
         context?.loadFragment(context!!,TaskDetailsFragment())
        })
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun dtAction(){
        start_date.setOnClickListener(View.OnClickListener {
            var date =
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    myCalendar[Calendar.YEAR] = year
                    myCalendar[Calendar.MONTH] = monthOfYear
                    myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                    updateLabel()
                }
//            var datePickerDialog: DatePickerDialog? = context?.let { it1 -> DatePickerDialog(it1, date, Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH) } //date is dateSetListener as per your code in question
            var datePickerDialog: DatePickerDialog? = context?.let { it1 -> DatePickerDialog(it1, date,myCalendar.get(
                Calendar.YEAR
            ), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)) } //date is dateSetListener as per your code in question
//            datePickerDialog?.datePicker?.maxDate = System.currentTimeMillis()
            datePickerDialog?.show()
        })

        myCalendar.add(Calendar.DAY_OF_YEAR,1)
        var tomro : Date = myCalendar.time //In which you need put here yyyy-MM-dd
        sdf = SimpleDateFormat(myFormat, Locale.US)
        val dateFormat: DateFormat = SimpleDateFormat("MM/dd/yyyy")
        dt_tomorrow.setOnClickListener{
            start_date.text = dateFormat.format(tomro)
            end_date.text = start_date.text.toString()
        }

        myCalendar.add(Calendar.DAY_OF_YEAR,1)
        var nextday1 : Date = myCalendar.time
        dt_nextday1.text = getDayString(nextday1, Locale.US)
//        val dayOfWeek = myCalendar[Calendar.DAY_OF_WEEK]
//            dt_nextday1.text = dayOfWeek.toString()

        dt_nextday1.setOnClickListener{
            start_date.text = dateFormat.format(nextday1)
            end_date.text = start_date.text.toString()
        }

        myCalendar.add(Calendar.DAY_OF_YEAR,1)
        var nextday2 : Date = myCalendar.time
        dt_nextday2.text = getDayString(nextday2, Locale.US)

        dt_nextday2.setOnClickListener{
//            myCalendar.add(Calendar.DAY_OF_YEAR,3)
            start_date.text = dateFormat.format(nextday2)
            end_date.text = start_date.text.toString()
        }
        myCalendar.add(Calendar.DAY_OF_YEAR,1)
        var nextday3 : Date = myCalendar.time
        dt_nextday3.text = getDayString(nextday3, Locale.US)
        dt_nextday3.setOnClickListener{
//            myCalendar.add(Calendar.DAY_OF_YEAR,4)
            start_date.text = dateFormat.format(nextday3)
            end_date.text = start_date.text.toString()
        }




        //time
        val cal = Calendar.getInstance()
        var startTime1= String()
        start_time.setOnClickListener{
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                startTime1 = SimpleDateFormat("hh:mm a").format(cal.time)
                start_time.text = startTime1
                cal.add(Calendar.MINUTE, 60)
                end_time.text = SimpleDateFormat("hh:mm a").format(cal.time)
            }

            TimePickerDialog(context, timeSetListener, cal.get(Calendar.HOUR_OF_DAY), cal.get(
                Calendar.MINUTE), false).show()
        }


//        var dft  = DateTimeFormatter.ofPattern("hh:mm a").parse(start_time.text.toString())
//        val date =  SimpleDateFormat("hh:mm a").parse(start_time.text.toString())
//        val calendar = Calendar.getInstance()
//        calendar.time = date
//        calendar.add(Calendar.MINUTE, 30)
//        var dt30m = calendar.time
//        var newNow = SimpleDateFormat("hh:mm a").parse(start_time.text.toString()) as LocalDateTime


        dt_30min.setOnClickListener{
            end_time.text =dt?.format(now?.plusMinutes(30))
        }

        dt_1hr.setOnClickListener{
            end_time.text =dt?.format(now?.plusMinutes(60))
        }

        dt_2hr.setOnClickListener{
            end_time.text =dt?.format(now?.plusMinutes(120))
        }

        dt_3hr.setOnClickListener{
            end_time.text =dt?.format(now?.plusMinutes(180))
        }

        dt_4hr.setOnClickListener{
            end_time.text =dt?.format(now?.plusMinutes(240))
        }



        dt_add.setOnClickListener{
//            context?.setSharedPref(context!!,"start_date",start_date.text.toString())
//            context?.setSharedPref(context!!,"start_time", start_time.text.toString())
//            context?.setSharedPref(context!!,"end_time",end_time.text.toString())
//            context?.setSharedPref(context!!,"end_date",end_date.text.toString())
//            context?.loadFragment(context!!,AddTaskFragment())
            var stDate= String()
            var edDate = String()
try {
            var parser = SimpleDateFormat("MM/dd/yyyy hh:mm a")
            val myFormatDate = "yyyy-MM-dd hh:mm a"
            val sdf = SimpleDateFormat(myFormatDate)

            stDate = sdf.format(parser.parse("${start_date.text.toString()} ${start_time.text}"))
            edDate = sdf.format(parser.parse("${end_date.text} ${end_time.text}"))

}catch (e:Exception){
    println("datetime_exception=== $e")
}
            var params = HashMap<String,String>()
            if (taskID != "null") {
                params["task_id"] = taskID
            }else{
                params["task_id"] = context?.getSharedPref("task_detail_id",context!!).toString()
            }
            params["startDate"]=stDate.toString()
            params["endDate"]=edDate.toString()
            context?.let { it1 ->
                apiRequest.postRequestBodyWithHeaders(it1,webService.Task_details_edit_task_detail_url,params){response->
                    var status= response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        context?.toast(msg)
                        context?.loadFragment(context!!,TaskDetailsFragment())
                    }
                    else{
                        context?.toast(msg)
                    }

                }
            }

        }

    }
    var myFormat = String()
    var sdf = SimpleDateFormat()
    private fun updateLabel(){
        myFormat = "MM/dd/yyyy" //In which you need put here yyyy-MM-dd
        sdf = SimpleDateFormat(myFormat, Locale.US)
        start_date.text = sdf.format(myCalendar.time)
        end_date.text = start_date.text.toString()
    }

    private fun getDayString(date: Date?, locale: Locale?): String? {
        val formatter: DateFormat = SimpleDateFormat("EEEE", locale)
        return formatter.format(date)
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }
}
package com.salesc2.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationManager
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Looper
import android.provider.MediaStore
import android.provider.Settings
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.github.ivbaranov.mli.MaterialLetterIcon
import com.github.siyamed.shapeimageview.RoundedImageView
import com.google.android.gms.location.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.textfield.TextInputEditText
import com.salesc2.R
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.capFirstLetter
import com.salesc2.utils.loadFragment
import com.salesc2.utils.toast
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.squareup.picasso.Picasso
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.HashMap

@Suppress("IMPLICIT_BOXING_IN_IDENTITY_EQUALS")
class ManageAccountFragment : Fragment() {

    private lateinit var manage_account_back_arrow : ImageView
    private lateinit var manage_acc_done : TextView
    private lateinit var manage_account_image : RoundedImageView
    private lateinit var manage_account_letterImage :MaterialLetterIcon
    private lateinit var manage_account_job_tv : TextView
    private lateinit var manage_account_member_name : TextView

   private lateinit var manageProfile :TextView
    private lateinit var managePS : TextView
    private lateinit var manageNotify : TextView
    private lateinit var manageTimeZone : TextView
    private lateinit var edit_image : ImageView



    private var apiRequest = ApiRequest()
    private var webService = WebService()
    lateinit var mView : View



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_manage_account, container, false)

        manage_account_back_arrow = mView.findViewById(R.id.manage_account_back_arrow)
        manage_acc_done = mView.findViewById(R.id.manage_acc_done)
        manage_account_image = mView.findViewById(R.id.manage_account_iv)
        manage_account_letterImage = mView.findViewById(R.id.manage_account_letterImage)
        manage_account_job_tv = mView.findViewById(R.id.manage_account_job_tv)
        manage_account_member_name = mView.findViewById(R.id.manage_account_profile_name)

        edit_image = mView.findViewById(R.id.edit_image)
        manageProfile = mView.findViewById(R.id.manageProfile)
        managePS = mView.findViewById(R.id.managePS)
        manageNotify = mView.findViewById(R.id.manageNotify)
        manageTimeZone = mView.findViewById(R.id.manageTimeZone)
        profileDetailsApiCall()
        clickActions()
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE

        return mView
    }

    private fun clickActions(){
        manageProfile.setOnClickListener{
            context?.loadFragment(context!!,ManageProfileFragment())
        }
        managePS.setOnClickListener{
            context?.loadFragment(context!!,PasswordAndSecurityFragment())
        }
        manageNotify.setOnClickListener{
            context?.loadFragment(context!!,NotificationSettingsFragment())
        }
        manageTimeZone.setOnClickListener{
            context?.loadFragment(context!!,TimeZoneSettingFragment())
        }

        manage_account_back_arrow.setOnClickListener{
            fragmentManager?.popBackStack()
        }
        manage_acc_done.setOnClickListener{
           context?.loadFragment(context!!,MoreFragment())
        }
    }


    private fun profileDetailsApiCall(){
        var params = HashMap<String,Any>()
        context?.let {
            apiRequest.getRequestBodyWithHeaders(it,webService.Profile_update_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){
                        var dataObj = response.getJSONObject("data")

                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")
                        var role = dataObj.getString("role")
                        var email = dataObj.getString("email")
                        var phone = dataObj.getString("phone")
                        var imgwithBase = String()
                        if (dataObj.has("profilePath")){
                            var profilePath = dataObj.getString("profilePath")
                            imgwithBase = webService.imageBasePath+profilePath
                        }


                        var address = "NA"
                        var division = "NA"
                        var area = "NA"
                        var region = "NA"
                        var territory = "NA"
                        if (dataObj.has("address")){
                            address = dataObj.getString("address")
                        }

//                        var comapanyName = dataObj.getString("comapanyName")
                        if (dataObj.has("division")){
                            division = dataObj.getString("division")
                        }
                        if (dataObj.has("area")){
                            area = dataObj.getString("area")
                        }

                        if (dataObj.has("region")){
                            region = dataObj.getString("region")
                        }
                        if (dataObj.has("territory")){
                            territory = dataObj.getString("territory")
                        }



                        var locationObj = dataObj.getJSONObject("location")
                       var  longitude = locationObj.getString("lng")
                       var lattitude = locationObj.getString("lat")

                        try {
                            Picasso.get().load(imgwithBase).into(manage_account_image)
                        }catch (e:Exception){
                            println(e)
                        }
                        if (role.equals("Sales Rep",ignoreCase = true)){
                            manage_account_job_tv.text  = "Representative"
                        }else if (role.equals("Team Lead",ignoreCase = true)){
                            manage_account_job_tv.text ="Regional Manager"
                        }else{
                            manage_account_job_tv.text = role
                        }

                        manage_account_member_name.text = name
                    }
                    else{
                        context?.toast("Something went wrong, Try again later")
                    }
                }catch (e: java.lang.Exception){
                    println(e)
                    context?.toast(e.toString())
                }
            }
        }
    }
}
package com.salesc2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.github.siyamed.shapeimageview.RoundedImageView
import com.salesc2.R
import com.salesc2.model.TaskListModel
import com.salesc2.model.TaskSummaryModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import java.lang.Exception
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.Comparator
import kotlin.collections.ArrayList

class TaskSummaryAdapter() : RecyclerView.Adapter<TaskSummaryAdapter.MyViewHolder>() {
    private lateinit var taskSummaryItems: ArrayList<TaskSummaryModel>
    var context: Context? = null
    var webService = WebService()
    var apiRequest = ApiRequest()

    constructor(context: Context?, taskSummaryItems: ArrayList<TaskSummaryModel>) : this(){
        this.context = context
        this.taskSummaryItems = taskSummaryItems

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.adapter_task_summary, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
      return taskSummaryItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var listing =  sortList(taskSummaryItems)
      var taskSummaryModel : TaskSummaryModel = listing[position]

        if (taskSummaryModel.title.equals("completed",ignoreCase = true)){
            holder.ts_img_icon.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.ic_completed_ts) })
            context?.let { ContextCompat.getColor(it, R.color.ts_completed) }?.let { holder.ts_img_icon.setBackgroundColor(it) }
            context?.let { ContextCompat.getColor(it, R.color.ts_completed) }?.let { holder.ts_count.setTextColor(it) }
        }
        if (taskSummaryModel.title.equals("cancelled",ignoreCase = true) || taskSummaryModel.title.equals("canceled",ignoreCase = true)){
            holder.ts_img_icon.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.ic_cancelled_ts) })
            context?.let { ContextCompat.getColor(it, R.color.ts_cancelled) }?.let { holder.ts_img_icon.setBackgroundColor(it) }
            context?.let { ContextCompat.getColor(it, R.color.ts_cancelled) }?.let { holder.ts_count.setTextColor(it) }
        }
         if (taskSummaryModel.title.equals("unassigned",ignoreCase = true)){
             holder.ts_img_icon.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.ic_unassigned_ts) })
             context?.let { ContextCompat.getColor(it, R.color.ts_unassigned) }?.let { holder.ts_img_icon.setBackgroundColor(it) }
             context?.let { ContextCompat.getColor(it, R.color.ts_unassigned) }?.let { holder.ts_count.setTextColor(it) }
        }
         if (taskSummaryModel.title.equals("assigned",ignoreCase = true)){
             holder.ts_img_icon.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.ic_assigned_ts) })
             context?.let { ContextCompat.getColor(it, R.color.ts_assigned) }?.let { holder.ts_img_icon.setBackgroundColor(it) }
             context?.let { ContextCompat.getColor(it, R.color.ts_assigned) }?.let { holder.ts_count.setTextColor(it) }
        }
        if (taskSummaryModel.title.equals("inprogress",ignoreCase = true) ||taskSummaryModel.title.equals("in progress",ignoreCase = true)){
            holder.ts_img_icon.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.ic_inprogress_ts) })
            context?.let { ContextCompat.getColor(it, R.color.ts_inprogress) }?.let { holder.ts_img_icon.setBackgroundColor(it) }
            context?.let { ContextCompat.getColor(it, R.color.ts_inprogress) }?.let { holder.ts_count.setTextColor(it) }
        }
        if (taskSummaryModel.title.equals("uncovered",ignoreCase = true)){
            holder.ts_img_icon.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.ic_uncovered_ts) })
            context?.let { ContextCompat.getColor(it, R.color.ts_uncovered) }?.let { holder.ts_img_icon.setBackgroundColor(it) }
            context?.let { ContextCompat.getColor(it, R.color.ts_uncovered) }?.let { holder.ts_count.setTextColor(it) }
        }
        if (taskSummaryModel.title.equals("pending",ignoreCase = true)){
            holder.ts_img_icon.setImageDrawable(context?.let { ContextCompat.getDrawable(it,R.drawable.ic_pending_ts) })
            context?.let { ContextCompat.getColor(it, R.color.ts_pending) }?.let { holder.ts_img_icon.setBackgroundColor(it) }
            context?.let { ContextCompat.getColor(it, R.color.ts_pending) }?.let { holder.ts_count.setTextColor(it) }
        }





         var tsDesc = taskDescByStatus(taskSummaryModel.title,taskSummaryModel.count)
        try {
            if (taskSummaryModel.title.equals("pending",ignoreCase = true)){
                holder.ts_title.text = capitalize("Pending Acceptance")
            }else if (taskSummaryModel.title.equals("Unassigned",ignoreCase = true)){
                holder.ts_title.text = capitalize("Pending Assignment")
            }else{
                holder.ts_title.text = capitalize(taskSummaryModel.title)
            }
            holder.ts_desc.text = tsDesc
            holder.ts_count.text = taskSummaryModel.count
        }catch (e:Exception){

        }


    }
    fun taskDescByStatus(status:String,count:String) : String{
        if (status.equals("completed",ignoreCase = true)){
            return "$count Tasks Completed"
        }
        else if (status.equals("cancelled",ignoreCase = true) || status.equals("canceled",ignoreCase = true)){
            return "$count Tasks Cancelled"
        }
        else if (status.equals("unassigned",ignoreCase = true)){
            return "$count Tasks Unassigned"
        }
        else if (status.equals("assigned",ignoreCase = true)){
            return "$count Tasks Assigned "
        }
        else if (status.equals("inprogress",ignoreCase = true) ||status.equals("in progress",ignoreCase = true)){
            return "$count Tasks In Progress"
        }
        else if (status.equals("uncovered",ignoreCase = true)){
            return "$count Tasks Uncovered"
        }
        else if (status.equals("pending",ignoreCase = true)){
            return "$count Tasks Pending"
        }
        return ""
    }

    private fun capitalize(capString: String): String? {
        val capBuffer = StringBuffer()
        val capMatcher: Matcher =
            Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString)
        while (capMatcher.find()) { capMatcher.appendReplacement(capBuffer, capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase()) }
        return capMatcher.appendTail(capBuffer).toString()
    }

    class MyViewHolder constructor(itemView:View) : RecyclerView.ViewHolder(itemView){
        var ts_img_icon :RoundedImageView = itemView.findViewById(R.id.ts_img_icon)
        var ts_title : TextView = itemView.findViewById(R.id.ts_title)
        var ts_desc : TextView = itemView.findViewById(R.id.ts_desc)
        var ts_count : TextView = itemView.findViewById(R.id.ts_count)
    }

    //sorting list
    fun sortList(list:ArrayList<TaskSummaryModel>):ArrayList<TaskSummaryModel> {
        Collections.sort(list, object:Comparator<TaskSummaryModel> {
            override fun compare(teamMember1:TaskSummaryModel, teamMember2:TaskSummaryModel):Int {
                return teamMember1.title.compareTo(teamMember2.title)
            }
        })
        return list
    }

}
package com.salesc2.data.network.api.response

data class ResponseTaskSummary(
    val status: Int,
    val taskSummary: List<TaskSummary>
) {
    data class TaskSummary(
        val count: String,
        val status: String
    )
}
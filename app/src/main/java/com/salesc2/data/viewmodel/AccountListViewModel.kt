package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.AccountListRepository
import com.salesc2.data.repository.EmployeeLoginRepository

import kotlinx.coroutines.launch

class AccountListViewModel(
    private val repository: AccountListRepository,
    val context: Context
) : ViewModel() {
     fun reqGetAccountList(
         requestAccountList: RequestAccountList,
         onSuccess: OnSuccess<ResponseAccountList>,
         onFailure: OnFailure<ResponseAccountList>
    )
     {
         viewModelScope.launch {
             repository.reqGetAccountList(requestAccountList,onSuccess,onFailure)
         }
     }

}
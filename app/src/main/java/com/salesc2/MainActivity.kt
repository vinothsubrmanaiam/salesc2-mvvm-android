package com.salesc2

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.preference.PreferenceManager
import android.provider.Settings
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import cn.xm.weidongjian.popuphelper.PopupWindowHelper
import com.github.siyamed.shapeimageview.RoundedImageView
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.kaopiz.kprogresshud.KProgressHUD
import com.salesc2.adapters.*
import com.salesc2.database.*
import com.salesc2.fragment.*
import com.salesc2.model.*
import com.salesc2.service.ApiRequest
import com.salesc2.service.DatabaseHandler
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.data.Day
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_notification_list.*
import kotlinx.android.synthetic.main.popup_card_list.*
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class MainActivity : AppCompatActivity(), OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener, GoogleMap.OnMyLocationButtonClickListener,
    GoogleMap.OnMyLocationClickListener, ActivityCompat.OnRequestPermissionsResultCallback,
    ShowOption, HideShowCardSelection {
    private var db: AppDatabase? = null
    var bottomNavigationView: CustomBottomNavigationView? = null
    var toolbartitletv: TextView? = null
    var toolbar: Toolbar? = null
    var divider1: View? = null
    var toolbar_layout: ConstraintLayout? = null
    lateinit var addTask_appIcon: ImageView
    lateinit var nav_mid_image: ImageView
    lateinit var dashboard_location_icon: ImageView
    private lateinit var bell_main: ImageView

    private lateinit var dashboard_recyclerView: RecyclerView
    private var dashTaskModel = ArrayList<DashTasksModel>()

    private lateinit var collapsibleCalendar: CollapsibleCalendar
    var selectAddCard: ImageView? = null

    private lateinit var dash_filter: TextView
    private lateinit var dash_assignee_filter: ImageView
    private lateinit var dash_territory_filter: ImageView

    private var pendingTask = String()
    private var type = String()
    private var assignToId = String()
    private var territoryId = String()

    private var task_assignee_name = String()

    var apiRequest = ApiRequest()
    var webService = WebService()
    var taskCount = String()
    var map_sup_type: String = "0"
    private lateinit var dash_tasks_tab: TextView
    private lateinit var dash_tasks_tab_underline: View
    private lateinit var dash_p_accept_tab: TextView
    private lateinit var dash_p_accept_tab_underline: View
    private lateinit var dash_p_assign_tab: TextView
    private lateinit var dash_p_assign_tab_underline: View

    private lateinit var dash_date: TextView
    private var dashType = String()

    private var dateSelected = String()
    private lateinit var main_fragmet_container: FrameLayout

    private lateinit var no_task_layout: ConstraintLayout
    private lateinit var noData_note: TextView
    private lateinit var noTask_img: ImageView
    private lateinit var schedule_your_task_tv: TextView
    private lateinit var schedule_your_task_desc: TextView
    private lateinit var createNewTask: Button
    private lateinit var noTaskScrollView: ScrollView

    private var currentPage = String()
    private var lastPage = String()
    private var pageLoad: Int = 1
    private var isLoading = true
    private var previousTotal: Int = 0
    private var pageSize: Int = 10

    var dashSectionModel = ArrayList<DashSectionModel>()

    private lateinit var dash_user_img: CircleImageView
    private lateinit var dash_user_wish_name: TextView
    private lateinit var dash_user_tasks_count: TextView
    private lateinit var dash_user_wish_close: ImageView
    private lateinit var user_wish_layout: ConstraintLayout
    private lateinit var dashboard_calendar_icon: ImageView
    private lateinit var cal_bottom_layout: ConstraintLayout
    private var userImage = String()
    private var userName = String()

    private var dash_tab_click = String()
    private var tab_title = String()
    var show = false
    var userWishShow = String()
    var todayDate = String()

    var todays_task = String()
    var pending_task_for_me = String()
    var pending_task_for_team = String()
    private var deviceToken = String()
    private var executed = false
    var cmpNowDate = String()
    var notificationCount = String()
    private lateinit var mainProgressBar: ProgressBar
    private lateinit var notificationCount_tv: TextView
    var task_id = String()

    //notification
    private lateinit var notifcations_recyclerView: RecyclerView
    private var notificationListModel = ArrayList<NotificationListModel>()

    private lateinit var NclearAll_notifications: TextView
    private lateinit var Nclose_iv: ImageView
    private lateinit var Nmark_all_read: TextView
    private lateinit var noNotificationsLayout: ConstraintLayout

    private var NcurrentPage = String()
    private var NlastPage = String()
    private var NpageLoad: Int = 1
    private var NisLoading = true
    var NpreviousTotal: Int = 0
    private lateinit var notificationListLayout: ConstraintLayout

    //email and notification settings
    private lateinit var error_layout: ConstraintLayout

    private lateinit var task_head: TextView
    private lateinit var event_head: TextView
    private lateinit var uns_done_tv: TextView
    private lateinit var uns_back_arrow: ImageView
    private lateinit var tasksSetting_recyclerView: RecyclerView
    private var notificationSettingsTasksModel = ArrayList<NotificationSettingsTasksModel>()

    private lateinit var eventSetting_recyclerView: RecyclerView
    private var notificationSettingsEventsModel = ArrayList<NotificationSettingsEventsModel>()

    private lateinit var allNotifySetting_recyclerView: RecyclerView
    private var notificationSettingsAllNotificationModel =
        ArrayList<NotificationSettingsAllNotificationModel>()

    private var set_timezone = String()

    //for map activity
    private var lati = String()
    private var long = String()


    var lati_map = String()
    var long_map = String()
    var selectedFilterName = "All"
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    var PERMISSION_ID = 143
    var ISdataAvaliable: Boolean = false


    //maps
    private lateinit var map: GoogleMap


    var mapTaskList_recyclerView: RecyclerView? = null
    var mapTaskListModel = ArrayList<MapTaskListModel>()

    private val REQUEST_LOCATION_PERMISSION = 1

    var type_marker = String()
    var type1: Marker? = null
    var type2: Marker? = null
    var type0: Marker? = null

    private var eventAttendeesModel = ArrayList<EventAttendeesModel>()
    private lateinit var terriFilter_name: AutoCompleteTextView
    private var territoryList = ArrayList<String>()
    private var territoryIdList = ArrayList<String>()
    private var terriId = String()
    private lateinit var empImageMap: CircleImageView


    private var REQUEST_CODE_LOCATION = 1111


    private lateinit var mainNestedScroll: NestedScrollView

    private lateinit var coordinatorlayout: RelativeLayout

    private var isFiltered = ""

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainNestedScroll = findViewById(R.id.mainNestedScroll)
        coordinatorlayout = findViewById(R.id.coordinatorlayout)
        //maps
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        terriFilter_name = findViewById(R.id.terriFilter_name)
        empImageMap = findViewById(R.id.empImageMap)
        empImageMap.maxWidth = 40
        empImageMap.maxHeight = 40
        eventAttendeesModel = ArrayList()
        mapTaskListModel = ArrayList()
        territoryList = ArrayList()
        territoryList.add("All")
        territoryIdList = ArrayList()
        isPermissionGranted()
        territoryApiCall()
        territoryDropDown()

        val map_type_filter = resources.getStringArray(R.array.map_type_filter)

        // access the spinner
        val sp_map_type_filter = findViewById<Spinner>(R.id.sp_map_type_filter)
        if (sp_map_type_filter != null) {
            val adapter = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item, map_type_filter
            )
            sp_map_type_filter.adapter = adapter

            sp_map_type_filter.onItemSelectedListener = object :
                AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
                ) {
                    if (map_type_filter[position] == "User") {
                        map_sup_type = "0"
                    } else if (map_type_filter[position] == "All") {
                        map_sup_type = ""
                    } else if (map_type_filter[position] == "Events") {
                        map_sup_type = "1"
                    } else if (map_type_filter[position] == "Accounts") {
                        map_sup_type = "2"
                    }

                    if (!terriId.equals("")) {
                        var params = HashMap<String, Any>()
                        params["filter"] = true
                        params["territoryId"] = terriId
                        if (map_sup_type != "") {
                            params["typeFilter"] = map_sup_type.toInt()
                        }
                        isFiltered = "yes"
                        mapDataApiCall(params)
                    } else {
                        if (map_sup_type.equals("")) {
                            try {
                                val params = HashMap<String, Any>()
                                params["latitude"] = lati_map.toFloat()
                                params["longitude"] = long_map.toFloat()
                                params["radius"] = getMapsVisibleRadius()
                                mapDataApiCall(params)
                            } catch (e: Exception) {

                            }

                        } else {
                            try {
                                var params = HashMap<String, Any>()
//                                params["filter"] = true
                                //   params["territoryId"] = ""
                                params["latitude"] = lati_map.toFloat()
                                params["longitude"] = long_map.toFloat()
                                params["radius"] = getMapsVisibleRadius()
                                if (map_sup_type != "") {
                                    params["typeFilter"] = map_sup_type.toInt()
                                    isFiltered = "yes"
                                }

                                mapDataApiCall(params)
                            } catch (e: Exception) {

                            }


                        }

// params["typeFilter"] = map_sup_type

                    }


                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // write code to perform some action
                }
            }
        }


        notifcations_recyclerView = findViewById(R.id.notifcations_recyclerView)
        notificationListModel = ArrayList()
        NclearAll_notifications = findViewById(R.id.clearAll_notifications)
        Nclose_iv = findViewById(R.id.close_iv)
        Nmark_all_read = findViewById(R.id.mark_all_read)
        notificationListLayout = findViewById(R.id.notificationListLayout)
        noNotificationsLayout = findViewById(R.id.noNotificationsLayout)
        Nclose_iv.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        set_timezone = this.getSharedPref("set_timezone", this).toString()
//        TimeZone.setDefault(TimeZone.getTimeZone(set_timezone))

        userImage = getSharedPref("profile_image", this)
        userName = getSharedPref("username", this)
        dash_tab_click = getSharedPref("dash_tab_click", this)
        deviceToken = this.getSharedPref("reg_device_token", this)
        Log.i("deviceToken== ", deviceToken)
        task_id = this.getSharedPref("task_detail_id", this).toString()
        //status bar color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = ContextCompat.getColor(applicationContext, R.color.blacktext)
        }
        dashSectionModel = ArrayList()
        notificationListModel = ArrayList()

        noData_note = findViewById(R.id.noData_note)
        noTask_img = findViewById(R.id.noTask_img)
        schedule_your_task_tv = findViewById(R.id.schedule_your_task_tv)
        schedule_your_task_desc = findViewById(R.id.schedule_your_task_desc)
        noTaskScrollView = findViewById(R.id.noTaskScrollView)

        notificationCount_tv = findViewById(R.id.notificationCount_tv)
        mainProgressBar = findViewById(R.id.mainProgressBar)





        dash_user_img = findViewById(R.id.dash_user_img)
        dash_user_wish_name = findViewById(R.id.dash_user_wish_name)
        dash_user_tasks_count = findViewById(R.id.dash_user_tasks_count)
        dash_user_wish_close = findViewById(R.id.dash_user_wish_close)
        user_wish_layout = findViewById(R.id.user_wish_layout)
        dashboard_calendar_icon = findViewById(R.id.dashboard_calendar_icon)
        cal_bottom_layout = findViewById(R.id.cal_bottom_layout)




        tab_title = getSharedPref("dash_tab_click", this@MainActivity)
        territoryId = getSharedPref("dash_territory_id", this)
        assignToId = getSharedPref("dash_assignee_id", this)
        dash_date = findViewById(R.id.dash_date)

        createNewTask = findViewById(R.id.createNewTask)
        main_fragmet_container = findViewById(R.id.main_fragmet_container)
        dash_tasks_tab = findViewById(R.id.dash_tasks_tab)
        dash_tasks_tab_underline = findViewById(R.id.dash_tasks_tab_underline)
        dash_p_accept_tab = findViewById(R.id.dash_p_accept_tab)
        dash_p_accept_tab_underline = findViewById(R.id.dash_p_accept_tab_underline)
        dash_p_assign_tab = findViewById(R.id.dash_p_assign_tab)
        dash_p_assign_tab_underline = findViewById(R.id.dash_p_assign_tab_underline)

        no_task_layout = findViewById(R.id.no_task_layout)

        dash_filter = findViewById(R.id.dash_filter)
        dash_assignee_filter = findViewById(R.id.dash_assignee_filter)
        dash_territory_filter = findViewById(R.id.dash_territory_filter)

        dashboard_recyclerView = findViewById(R.id.dashboard_recyclerView)

        bell_main = findViewById(R.id.dashboard_notify_icon)
        nav_mid_image = findViewById(R.id.nav_mid_image)
        toolbar_layout = findViewById(R.id.toolbar_layout)
        toolbartitletv = findViewById(R.id.toolbar_title_tv)
        toolbar = findViewById(R.id.toolbar)
        divider1 = findViewById(R.id.divider1)
        bottomNavigationView = findViewById(R.id.navigationView)
        bottomNavigationView?.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        bottomNavigationView?.visibility = View.VISIBLE

        addTask_appIcon = findViewById(R.id.app_icon_imageView)

        collapsibleCalendar = findViewById(R.id.dashboard_calendarView)
        dashboard_location_icon = findViewById(R.id.dashboard_location_icon)
        main_fragmet_container.visibility = View.GONE


        //email and notification settings
        error_layout = findViewById(R.id.error_layout)

        uns_done_tv = findViewById(R.id.uns_done_tv)
        task_head = findViewById(R.id.task_head)
        event_head = findViewById(R.id.event_head)

        uns_back_arrow = findViewById(R.id.uns_back_arrow)
        tasksSetting_recyclerView = findViewById(R.id.tasksSetting_recyclerView)
        notificationSettingsTasksModel = ArrayList()
        eventSetting_recyclerView = findViewById(R.id.eventSetting_recyclerView)
        notificationSettingsEventsModel = ArrayList()

        allNotifySetting_recyclerView = findViewById(R.id.allNotifySetting_recyclerView)
        notificationSettingsAllNotificationModel = ArrayList()

        mFusedLocationClient = this.let { LocationServices.getFusedLocationProviderClient(it) }!!
        if (isNetworkAvailable(applicationContext)) {
           commonApiCalls()
        }



        if (assignToId != "null") {
            this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_filter_assignee_icon) }
                ?.let {
                    dash_assignee_filter.setImageDrawable(it)
                }

        } else {
            this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_assignee_grey) }?.let {
                dash_assignee_filter.setImageDrawable(it)
            }
        }
        if (territoryId != "null") {
            this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_territory_green) }?.let {
                dash_territory_filter.setImageDrawable(it)
            }
        } else {
            this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_filter_territory_icon) }
                ?.let {
                    dash_territory_filter.setImageDrawable(it)
                }
        }

        set_timezone = this.getSharedPref("set_timezone", this).toString()
        dashType = dash_filter.text.toString().toLowerCase()
        val dtf = SimpleDateFormat("dd/MM/yyyy hh:mm a")
        var now: LocalDateTime = if (set_timezone != "null") {
            LocalDateTime.now(ZoneId.of(set_timezone))
        } else {
            LocalDateTime.now()
        }

//        dateSelected = dtf.format(now)
        var todayDtFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        var todayDtFormatNew: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm a")
        cmpNowDate = todayDtFormat.format(now)
        var todayDt = todayDtFormatNew.format(now)
        var todayDtToDate = todayDt.toDate()
        var todayDtUTC = this.dateToUTC(this, todayDtToDate)
        var todayDtF = dtf.format(todayDtUTC)
        todayDate = todayDtF
        dateSelected = todayDtF
        var dt = DateTimeFormatter.ofPattern("MMM dd, yyyy")
        dash_date.text = "${dt.format(now)} "
        dashTaskModel.clear()
        if (dashType == "work") {
            dashBoardApiCall(dateSelected, "", "", "", "meeting")
        } else {
            dashBoardApiCall(dateSelected, "", "", "", dashType)
        }
        dashboardCountApiCall()
        getLastLocation()
        //getTime()
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            val permissions =
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
            // REQUEST_CODE_LOCATION should be defined on your app level
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_LOCATION)
        }
    }

    private fun commonApiCalls() {
        var offline_auto_pref = this.getSharedPref("isChecked_offline_auto", this).toString()
        var loggedInFirst = getSharedPref("LoggedInFirst", this).toString()
        if (loggedInFirst == "yes") {
            pageLoad = 1
            dashboardDataOffline()
            accountOfflineApiCall()
            serviceOfflineApiCall()
            productListOffline()
            taskDetailsOfflineApiCall()
            setSharedPref(this, "LoggedInFirst", "no")
        }
        if (offline_auto_pref == "yes") {
            getTime()
        }
        timeZoneApiCall()
        userWishWindow()
        addTaskAction()
        notificationOnClick()
        calendarView()

        filterAssigneeAndTerritory()
        tabAction()
        loadFragment(DashboardFragment())
        calHideShow()
        notificationCountApiCall()
        startLocation()
        //notificationList
        notificationListApiCall()
        clearAllNotificationOnClick()
        markAllReadAction()

        filterOnClick()

        //email and notification settings
        notificationSettingsApiCall()
        notificationSetActions()

        //offline
        addTaskOfflineApiCall()

    }

    private fun timeZoneApiCall() {
        var params = HashMap<String, Any>()
        this?.let {
            apiRequest.getRequestBodyWithHeaders(
                it,
                webService.Employee_timezone,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200") {
                        var dataObj = response.getJSONObject("data")
                        var _id = dataObj.getString("_id")
                        var timeZoneName = dataObj.getString("timezone")

                        if (timeZoneName != "") {
                            this.setSharedPref(this, "set_timezone", timeZoneName)
                        } else {
                            this.setSharedPref(this, "set_timezone", "null")
                        }


                    } else {
                        toast(msg)
                    }
                } catch (e: Exception) {

                }
            }
        }
    }


    var clicked = true
    private fun calHideShow() {
        dashboard_calendar_icon.setOnClickListener {
            if (clicked) {
                collapsibleCalendar.visibility = View.GONE
                cal_bottom_layout.visibility = View.GONE
//                dashboard_calendar_icon.setColorFilter(R.color.grey)
                clicked = false
            } else {
                clicked = true
                collapsibleCalendar.visibility = View.VISIBLE
                cal_bottom_layout.visibility = View.VISIBLE
//                dashboard_calendar_icon.setColorFilter(R.color.white)

            }
        }
    }


    fun startLocation() {

        if (LocationUpdate.ISLocationUpdate.equals("True")) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dashboard_location_icon.setImageDrawable(
                    getResources().getDrawable(
                        R.drawable.ic_icon_material_location_off,
                        getApplicationContext().getTheme()
                    )
                );
            } else {
                dashboard_location_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_material_location_off));
            }

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                dashboard_location_icon.setImageDrawable(
                    getResources().getDrawable(
                        R.drawable.ic_location_white,
                        getApplicationContext().getTheme()
                    )
                );
            } else {
                dashboard_location_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_white));
            }
        }


        dashboard_location_icon.setOnClickListener {

            if (LocationUpdate.ISLocationUpdate.equals("True")) {
                LocationUpdate.ISLocationUpdate = "False"
                val intent = Intent(this, LocationUpdate::class.java)
                stopService(intent)

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dashboard_location_icon.setImageDrawable(
                        getResources().getDrawable(
                            R.drawable.ic_location_white,
                            getApplicationContext().getTheme()
                        )
                    );
                } else {
                    dashboard_location_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_white));
                }
            } else {

                val isChecked = this.getSharedPref("isshow_locationdialog", this)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    dashboard_location_icon.setImageDrawable(
                        getResources().getDrawable(
                            R.drawable.ic_icon_material_location_off,
                            getApplicationContext().getTheme()
                        )
                    );
                } else {
                    dashboard_location_icon.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_material_location_off));
                }

                if (!isChecked.equals("true")) {
                    locationDialogshow(this@MainActivity)

                } else {
                    val intent = Intent(this, LocationUpdate::class.java)
                    startService(intent)

                }

            }

        }

    }

    fun locationDialogshow(context: Context) {
        val locationdialog = Dialog(context)
        locationdialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        locationdialog.setCancelable(false)
        locationdialog.setContentView(R.layout.show_location_dialog)
        locationdialog.window?.setBackgroundDrawableResource(R.drawable.alert_dialog_view_bg)
        val location_switch = locationdialog.findViewById(R.id.location_switch) as SwitchCompat
        val cancelbtn = locationdialog.findViewById(R.id.cancelbtn) as TextView
        val okbtn = locationdialog.findViewById(R.id.okbtn) as TextView
        val isChecked = context?.getSharedPref("isshow_locationdialog", context!!)

        if (isChecked.equals("true")) {
            location_switch.setChecked(true);

        } else {
            location_switch.setChecked(false);
        }
        location_switch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                context?.setSharedPref(context!!, "isshow_locationdialog", "true")


            } else {
                context?.setSharedPref(context!!, "isshow_locationdialog", "false")
            }
        }
        okbtn.setOnClickListener {


            val intent = Intent(context, LocationUpdate::class.java)
            startService(intent)

            locationdialog.dismiss()

        }

        cancelbtn.setOnClickListener {
            locationdialog.dismiss()

        }
        locationdialog.show()
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager: ActivityManager =
            getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.getClassName()) {
                return true
            }
        }
        return false
    }

    fun resetGraph(context: MainActivity) {
        tab_title = getSharedPref("dash_tab_click", this@MainActivity)
        dashSectionModel.clear()
        dashTaskModel.clear()
        pageLoad = 1
        pageSize = 10
        territoryId = getSharedPref("dash_territory_id", this)
        assignToId = getSharedPref("dash_assignee_id", this)
        dashBoardApiCall(dateSelected, "", "", "", "all")
        dashboardCountApiCall()
    }

    fun updateDetailsPage(context: MainActivity) {
        task_id = this.getSharedPref("task_detail_id", this).toString()
        assignToId = getSharedPref("dash_assignee_id", this)
        var params = HashMap<String, String>()
        params["_id"] = task_id
        params["taskStatus"] = "Pending"
        params["assignedTo"] = assignToId

        this.let {
            apiRequest.postRequestBodyWithHeaders(
                it,
                webService.Update_status_url,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200") {
                        dash_p_assign_tab.performClick()
                        context?.toast(msg)
                    } else {
                        context?.toast(msg)
                    }
                } catch (e: java.lang.Exception) {

                }


            }
        }
    }

    fun resetAssigneeOnClick(context: MainActivity) {
        noTaskScrollView.visibility = View.GONE
        no_task_layout.visibility = View.GONE
        main_fragmet_container.visibility = View.GONE
        dashboard_recyclerView.visibility = View.VISIBLE
        tab_title = getSharedPref("dash_tab_click", this@MainActivity)
        val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.remove("dash_territory_id")
        editor.commit()
        dashSectionModel.clear()
        dashTaskModel.clear()
        pageLoad = 1
        pageSize = 10
        territoryId = getSharedPref("dash_territory_id", this)
        assignToId = getSharedPref("dash_assignee_id", this)

        when (tab_title) {
            "taskOnClick" -> {
                dashBoardApiCall(dateSelected, assignToId, "", "", "all")
            }
            "meOnClick" -> {
                dashBoardApiCall(dateSelected, assignToId, "", "me", "all")
            }
            "teamOnClick" -> {
                dashBoardApiCall(dateSelected, assignToId, "", "team", "")
            }
        }
        dashboardCountApiCall()

        if (assignToId != "null") {
            this.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_filter_assignee_icon) }
                ?.let {
                    dash_assignee_filter.setImageDrawable(it)
                }

        } else {
            this.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_assignee_grey) }?.let {
                dash_assignee_filter.setImageDrawable(it)
            }
        }
        if (territoryId != "null") {
            this.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_territory_green) }?.let {
                dash_territory_filter.setImageDrawable(it)
            }
        } else {
            this.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_filter_territory_icon) }
                ?.let {
                    dash_territory_filter.setImageDrawable(it)
                }
        }
    }

    fun resetTerritoryOnClick(context: MainActivity) {
        noTaskScrollView.visibility = View.GONE
        no_task_layout.visibility = View.GONE
        main_fragmet_container.visibility = View.GONE
        dashboard_recyclerView.visibility = View.VISIBLE
        tab_title = getSharedPref("dash_tab_click", this@MainActivity)
        val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.remove("dash_assignee_id")
        editor.commit()
        dashSectionModel.clear()
        dashTaskModel.clear()
        pageLoad = 1
        pageSize = 10
        territoryId = getSharedPref("dash_territory_id", this)
        assignToId = getSharedPref("dash_assignee_id", this)
        if (tab_title == "taskOnClick") {
            dashBoardApiCall(dateSelected, "", territoryId, "", "all")
        } else if (tab_title == "meOnClick") {
            dashBoardApiCall(dateSelected, "", territoryId, "me", "all")
        } else if (tab_title == "teamOnClick") {
            dashBoardApiCall(dateSelected, "", territoryId, "team", "")
        }

        dashboardCountApiCall()
        if (assignToId != "null") {
            this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_filter_assignee_icon) }
                ?.let {
                    dash_assignee_filter.setImageDrawable(it)
                }

        } else {
            this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_assignee_grey) }?.let {
                dash_assignee_filter.setImageDrawable(it)
            }
        }
        if (territoryId != "null") {
            this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_territory_green) }?.let {
                dash_territory_filter.setImageDrawable(it)
            }
        } else {
            this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_filter_territory_icon) }
                ?.let {
                    dash_territory_filter.setImageDrawable(it)
                }
        }
    }

    fun resetMeOnClick(context: MainActivity) {
        dashSectionModel.clear()
        dashTaskModel.clear()
        pageLoad = 1
        pageSize = 10
        dash_p_accept_tab.performClick()
    }

    fun resetTeamOnClick(context: MainActivity) {
        dashSectionModel.clear()
        dashTaskModel.clear()
        pageLoad = 1
        pageSize = 10
        dash_p_assign_tab.performClick()
    }

    fun refreshDashboardAPIs() {
        dashboardCountApiCall()
        notificationCountApiCall()
    }

    fun showNoDataFound(context: MainActivity) {
        this.toast("No data found")
        no_task_layout?.visibility = View.VISIBLE
        noTaskScrollView?.visibility = View.VISIBLE
        noData_note?.text = "No data found"
        noTask_img?.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.no_task_img))
        schedule_your_task_tv?.visibility = View.GONE
        schedule_your_task_desc?.visibility = View.GONE
    }

    fun timeZoneUpdateRefresh(context: MainActivity) {
        var dashTyp = dash_filter.text.toString().toLowerCase()
        dashboardCountApiCall()
        notificationCountApiCall()
        pageLoad = 1
        pageSize = 10
        when (tab_title) {
            "taskOnClick" -> {
                if (dashTyp == "work") {
                    dashBoardApiCall(dateSelected, "", "", "", "meeting")
                } else {
                    dashBoardApiCall(dateSelected, "", "", "", dashTyp)
                }

            }
            "meOnClick" -> {
                dashBoardApiCall(dateSelected, "", "", "me", "all")
            }
            "teamOnClick" -> {
                dashBoardApiCall(dateSelected, "", "", "team", "")
            }
        }
    }

    fun allNotifyRefresh(context: MainActivity) {
        notificationSettingsApiCall()
    }

    private fun userWishWindow() {
        try {
            Picasso.get().load(userImage).placeholder(R.drawable.profile1).into(dash_user_img)
//             dash_user_tasks_count.text = "You have ${taskCount} tasks for the day"
        } catch (e: Exception) {

        }
        try {
            val c = Calendar.getInstance()
            val timeOfDay = c[Calendar.HOUR_OF_DAY]

            if (timeOfDay >= 0 && timeOfDay < 12) {
//                Toast.makeText(this, "Good Morning", Toast.LENGTH_SHORT).show()
                dash_user_wish_name.text = "Good Morning $userName"
            } else if (timeOfDay >= 12 && timeOfDay < 16) {
//                Toast.makeText(this, "Good Afternoon", Toast.LENGTH_SHORT).show()
                dash_user_wish_name.text = "Good Afternoon $userName"
            } else if (timeOfDay >= 16 && timeOfDay < 24) {
//                Toast.makeText(this, "Good Evening", Toast.LENGTH_SHORT).show()
                dash_user_wish_name.text = "Good Evening $userName"
            }
//            else if (timeOfDay >= 21 && timeOfDay < 24) {
//                Toast.makeText(this, "Good Night", Toast.LENGTH_SHORT).show()
//                dash_user_wish_name.text = "Good Morning $userName"
//            }
        } catch (e: Exception) {

        }


        dash_user_wish_close.setOnClickListener {
            user_wish_layout.visibility = View.GONE
            this.setSharedPref(this, "user_wish_show", "no")
            userWishShow = this.getSharedPref("user_wish_show", this)
        }
//        userWishShow = this.getSharedPref("user_wish_show",this)
//          if(userWishShow == "no"){
//            user_wish_layout.visibility = View.GONE
//        }else{
//            user_wish_layout.visibility = View.VISIBLE
//        }
    }


    private fun tabAction() {
        dash_tasks_tab.setOnClickListener {
            dashboard_recyclerView.visibility = View.VISIBLE
            main_fragmet_container.visibility = View.GONE
            this.setSharedPref(this, "dash_tab_click", "taskOnClick")
            no_task_layout.visibility = View.GONE
            noTaskScrollView.visibility = View.GONE
            dash_tasks_tab_underline.visibility = View.VISIBLE
            dash_p_accept_tab_underline.visibility = View.GONE
            dash_p_assign_tab_underline.visibility = View.GONE
            main_fragmet_container.visibility = View.GONE
            dash_filter.visibility = View.VISIBLE
            dashTaskModel.clear()
            dashSectionModel.clear()
            dashType = dash_filter.text.toString().toLowerCase()
            pageLoad = 1
            pageSize = 10
            val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.remove("dash_assignee_id")
            editor.remove("dash_territory_id")
            editor.commit()

            territoryId = getSharedPref("dash_territory_id", this)
            assignToId = getSharedPref("dash_assignee_id", this)
            if (assignToId != "null") {
                this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_filter_assignee_icon) }
                    ?.let {
                        dash_assignee_filter.setImageDrawable(it)
                    }

            } else {
                this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_assignee_grey) }?.let {
                    dash_assignee_filter.setImageDrawable(it)
                }
            }
            if (territoryId != "null") {
                this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_territory_green) }
                    ?.let {
                        dash_territory_filter.setImageDrawable(it)
                    }
            } else {
                this?.let {
                    ContextCompat.getDrawable(
                        it,
                        R.drawable.ic_dash_filter_territory_icon
                    )
                }
                    ?.let {
                        dash_territory_filter.setImageDrawable(it)
                    }
            }
            if (dashType == "work") {
                dashBoardApiCall(dateSelected, "", "", "", "meeting")
            } else {
                dashBoardApiCall(dateSelected, "", "", "", dashType)
            }


            refreshDashboardAPIs()

        }
        dash_p_accept_tab.setOnClickListener {
            println("this_is_testing"+" wdqwdqwdd")


            if (isNetworkAvailable(this)){
            dashboard_recyclerView.visibility = View.VISIBLE
            main_fragmet_container.visibility = View.GONE
            this.setSharedPref(this, "dash_tab_click", "meOnClick")

            noTaskScrollView.visibility = View.GONE
            no_task_layout.visibility = View.GONE
            dash_tasks_tab_underline.visibility = View.GONE
            dash_p_accept_tab_underline.visibility = View.VISIBLE
            dash_p_assign_tab_underline.visibility = View.GONE
            main_fragmet_container.visibility = View.GONE
            dash_filter.visibility = View.GONE
            dashTaskModel.clear()
            dashSectionModel.clear()
            pageLoad = 1
            pageSize = 10

            val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.remove("dash_assignee_id")
            editor.remove("dash_territory_id")
            editor.commit()
            territoryId = getSharedPref("dash_territory_id", this)
            assignToId = getSharedPref("dash_assignee_id", this)
            if (assignToId != "null") {
                this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_filter_assignee_icon) }
                    ?.let {
                        dash_assignee_filter.setImageDrawable(it)
                    }

            } else {
                this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_assignee_grey) }?.let {
                    dash_assignee_filter.setImageDrawable(it)
                }
            }
            if (territoryId != "null") {
                this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_territory_green) }
                    ?.let {
                        dash_territory_filter.setImageDrawable(it)
                    }
            } else {
                this?.let {
                    ContextCompat.getDrawable(
                        it,
                        R.drawable.ic_dash_filter_territory_icon
                    )
                }
                    ?.let {
                        dash_territory_filter.setImageDrawable(it)
                    }
            }

            dashBoardApiCall(dateSelected, "", "", "me", "")
            refreshDashboardAPIs()

        }else{
            toast("No internt connection")
        }}
        dash_p_assign_tab.setOnClickListener {


            println("this_is_testing"+" wdqwdqwdd")

          if (this.isNetworkAvailable(this)){
            dashboard_recyclerView.visibility = View.VISIBLE
            main_fragmet_container.visibility = View.GONE
            this.setSharedPref(this, "dash_tab_click", "teamOnClick")

            noTaskScrollView.visibility = View.GONE
            no_task_layout.visibility = View.GONE
            dash_tasks_tab_underline.visibility = View.GONE
            dash_p_accept_tab_underline.visibility = View.GONE
            dash_p_assign_tab_underline.visibility = View.VISIBLE
            main_fragmet_container.visibility = View.GONE
            dash_filter.visibility = View.GONE
            dashTaskModel.clear()
            dashSectionModel.clear()
            pageLoad = 1
            pageSize = 10

            val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.remove("dash_assignee_id")
            editor.remove("dash_territory_id")

            editor.commit()
            territoryId = getSharedPref("dash_territory_id", this)
            assignToId = getSharedPref("dash_assignee_id", this)
            if (assignToId != "null") {
                this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_filter_assignee_icon) }
                    ?.let {
                        dash_assignee_filter.setImageDrawable(it)
                    }

            } else {
                this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_assignee_grey) }?.let {
                    dash_assignee_filter.setImageDrawable(it)
                }
            }
            if (territoryId != "null") {
                this?.let { ContextCompat.getDrawable(it, R.drawable.ic_dash_territory_green) }
                    ?.let {
                        dash_territory_filter.setImageDrawable(it)
                    }
            } else {
                this?.let {
                    ContextCompat.getDrawable(
                        it,
                        R.drawable.ic_dash_filter_territory_icon
                    )
                }
                    ?.let {
                        dash_territory_filter.setImageDrawable(it)
                    }
            }
            dashBoardApiCall(dateSelected, "", "", "team", "")
            refreshDashboardAPIs()
        }else{
            toast("No internet connection")
        }}


    }

    var itemIdFilter = String()

    @RequiresApi(Build.VERSION_CODES.O)
    private fun filterOnClick() {
//        var view : View = findViewById(R.id.MY_MENU_ITEM_ID)
        dash_filter.setOnClickListener {
            itemIdFilter = getSharedPref("filter_item_id", this)
            main_fragmet_container.visibility = View.GONE
            var popup = PopupMenu(this, dash_filter, R.style.PopupMenu)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                popup.gravity = Gravity.CENTER_HORIZONTAL
            }
            popup.menuInflater.inflate(R.menu.popup_menu, popup.menu)
            popup.menu.add("All")
            popup.menu.add("Task")
            popup.menu.add("Work")
            popup.menu.add("Personal")
            try {
                if (dash_filter.text.toString() == "All") {
                    val spanString =
                        SpannableString(popup.menu.getItem(0).title.toString())
                    spanString.setSpan(
                        ForegroundColorSpan(Color.GREEN),
                        0,
                        spanString.length,
                        0
                    ) //fix the color to white

//               item.setTitle(spanString)
                    popup.menu.getItem(0).title = spanString
                }
                if (dash_filter.text.toString() == "Task") {
                    val spanString =
                        SpannableString(popup.menu.getItem(1).getTitle().toString())
                    spanString.setSpan(
                        ForegroundColorSpan(Color.GREEN),
                        0,
                        spanString.length,
                        0
                    ) //fix the color to white

//               item.setTitle(spanString)
                    popup.menu.getItem(1).title = spanString
                }
                if (dash_filter.text.toString() == "Work") {
                    val spanString =
                        SpannableString(popup.menu.getItem(2).getTitle().toString())
                    spanString.setSpan(
                        ForegroundColorSpan(Color.GREEN),
                        0,
                        spanString.length,
                        0
                    ) //fix the color to white

//               item.setTitle(spanString)
                    popup.menu.getItem(2).title = spanString
                }
                if (dash_filter.text.toString() == "Personal") {
                    val spanString =
                        SpannableString(popup.menu.getItem(3).getTitle().toString())
                    spanString.setSpan(
                        ForegroundColorSpan(Color.GREEN),
                        0,
                        spanString.length,
                        0
                    ) //fix the color to white

//               item.setTitle(spanString)
                    popup.menu.getItem(3).title = spanString
                }
            } catch (e: Exception) {

            }

            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
                    dash_filter.text = item.title.toString()
                    if (item.title.toString() == "Work") {
                        type = "meeting"
                        setSharedPref(this@MainActivity, "all_filter_dash", "Meeting")
                        setSharedPref(this@MainActivity, "filter_item_id", item.itemId.toString())
                    } else {
                        type = dash_filter.text.toString().toLowerCase()
                        setSharedPref(this@MainActivity, "all_filter_dash", item.title.toString())
                        setSharedPref(this@MainActivity, "filter_item_id", item.itemId.toString())
                    }

                    dashTaskModel.clear()
                    dashSectionModel.clear()
                    pageLoad = 1
                    pageSize = 10
                    dash_tasks_tab.performClick()
                    return false
                }
            })
            popup.show() //showing popup menu

        }
    }

    var d_accId = String()
    var d_stTime = String()
    var d_edTime = String()

    private fun filterAssigneeAndTerritory() {
        dash_assignee_filter.setOnClickListener {
            var bundle = Bundle()
            bundle.putString("dash_accountId", d_accId)
            bundle.putString("dash_startTime", d_stTime)
            bundle.putString("dash_endTime", d_edTime)
            var fragment = DashboardAssigneeFilterFragment()
            fragment.arguments = bundle
            val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.main_fragmet_container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()

        }
        dash_territory_filter.setOnClickListener {
            val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
            val editor: SharedPreferences.Editor = preferences.edit()
            editor.remove("dash_filter_assignee_position")
            editor.commit()
            loadFragment(DashboardTerritoryFilterFragment())
        }
    }

    var cmpCalDate = String()
    private fun calendarView() {
        collapsibleCalendar.setCalendarListener(object : CollapsibleCalendar.CalendarListener {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onDaySelect() {

                setSharedPref(this@MainActivity, "dash_tab_click", "taskOnClick")
                no_task_layout.visibility = View.GONE
                noTaskScrollView.visibility = View.GONE
                dash_tasks_tab_underline.visibility = View.VISIBLE
                dash_p_accept_tab_underline.visibility = View.GONE
                dash_p_assign_tab_underline.visibility = View.GONE
                main_fragmet_container.visibility = View.GONE
                dash_filter.visibility = View.VISIBLE
                dashTaskModel.clear()
                dashSectionModel.clear()


                val dtf = SimpleDateFormat("dd/MM/yyyy hh:mm a")
                var now = if (set_timezone != "null") {
                    LocalDateTime.now(ZoneId.of(set_timezone))
                } else {
                    LocalDateTime.now()
                }
//        dateSelected = dtf.format(now)
                var todayDtFormatNew: DateTimeFormatter = DateTimeFormatter.ofPattern("hh:mm a")
                var currentTime = todayDtFormatNew.format(now)

                val day: Day = collapsibleCalendar.selectedDay!!

                var dateSelected1 = "${day.day}/${(day.month + 1)}/${day.year} $currentTime"
                var sdf = SimpleDateFormat("dd/MM/yyyy hh:mm a")
                //date
                var format = SimpleDateFormat("dd/MM/yyyy hh:mm a")
                val month_parse: Date = sdf.parse(dateSelected1)
                val month_date: String = format.format(month_parse)
                var StringToDate = month_date.toDate()
                var dateToUTC = dateToUTC(this@MainActivity, StringToDate)
                var dateF = format.format(dateToUTC)
                dateSelected = dateF

                var _format = SimpleDateFormat("dd/MM/yyyy")
                val _parse: Date = sdf.parse(dateSelected1)
                cmpCalDate = _format.format(_parse)
//                dateSelected = cmpCalDate

                var dateToDisp = "${day.day}-${(day.month + 1)}-${day.year}"


                var dt = SimpleDateFormat("MMM dd, yyyy")

                dash_date.text = "$dateToDisp"

                pageLoad = 1
                pageSize = 10
                dashType = dash_filter.text.toString().toLowerCase()
                if (dashType == "work") {
                    dashBoardApiCall(dateSelected, "", "", "", "meeting")
                } else {
                    dashBoardApiCall(dateSelected, "", "", "", dashType)
                }


//                toast(dateSelected)
                this@MainActivity.setSharedPref(
                    this@MainActivity,
                    "dashboard_selected_date",
                    dateSelected
                )

            }

            override fun onItemClick(view: View) {
//                view.setBackgroundResource(R.drawable.corner_shape_white_bg)
            }

            override fun onClickListener() {
                collapsibleCalendar.setBackgroundResource(R.drawable.corner_shape_white_bg)
            }

            override fun onDataUpdate() {
            }

            override fun onDayChanged() {
            }

            override fun onMonthChange() {

            }

            override fun onWeekChange(i: Int) {
            }
        })
    }


    @RequiresApi(Build.VERSION_CODES.O)
//var listdata = ArrayList<String>()
    private var progressHuD: KProgressHUD? = null


    @RequiresApi(Build.VERSION_CODES.O)
    private fun dashBoardApiCall(
        date: String,
        assignTo: String,
        territory: String,
        pendingTask: String,
        type: String
    ) {
        if (isNetworkAvailable(applicationContext)) {

            println("param" + " taskoffline" + "testing")

//offline work

            CoroutineScope(Dispatchers.Main).launch {

                db = AppDatabase.getDatabaseClient(applicationContext)
                println("param" + " taskoffline" + "testing 1")

// db!!.dashBoardDao().deleteAll()
                val data = db!!.addTaskDao().getAll()
                println("param" + " taskoffline" + "record" + data.size)

                if (data.size != 0) {

                    CoroutineScope(Dispatchers.Main).launch {
                        db = AppDatabase.getDatabaseClient(this@MainActivity)
                        val accountData = db!!.addTaskDao().getAll()
//                        db!!.dashBoardDateAddDao().deleteAll()

                        for (n in 0 until accountData.size) {
                            var param = HashMap<String, Any>()
                            param.put("accountId", accountData[n].accountId.toString())
                            param.put("departmentId", accountData[n].departmentId.toString())
                            param.put("serviceId", accountData[n].serviceId.toString())
                            param.put("assignedTo", accountData[n].assignedTo.toString())
                            param.put("startDate", accountData[n].startDate.toString())
                            param.put("endDate", accountData[n].endDate.toString())
                            param.put("taskDescription", accountData[n].taskDescription.toString())
                            param.put("taskStatus", accountData[n].taskStatus.toString())
                            /*  if (accountData[n].products.length <= 0){
                                  param.put("products", "")
                              }else{
                                  param.put("products", accountData[n].products.toString())
                              }*/
                            param.put("products", "")

                            param.put("priority", accountData[n].priority.toString().toBoolean())
                            println("param" + " taskoffline" + "testing 3")

                            println("param" + " taskoffline " + param)
                            this?.let {
                                apiRequest.postRequestBodyWithHeadersAny(
                                    this@MainActivity,
                                    webService.Add_task_url,
                                    param
                                ) { response ->
                                    var status = response.getString("status")
                                    var msg = response.getString("msg")
                                    if (status == "200") {
                                        Log.i("added offline task=", accountData[n].id.toString())
                                        db!!.addTaskDao().deleteById(accountData[n].id)
                                    } else if (status == "500") {

                                    } else {

                                    }
                                }
                            }
                        }
                        lifecycleScope.launch {
                            taskStatusUpdateCall()
                            delay(2000)
                            dashBoardOnlineApiCall(date, assignTo, territory, pendingTask, type)
                        }
                    }

                    ISdataAvaliable = true
                } else {

                    dashBoardOnlineApiCall(date, assignTo, territory, pendingTask, type)
                    ISdataAvaliable = false
                }
            }

            println("param" + " taskoffline" + "testing 2")


        } else {
            mainProgressBar.visibility = View.GONE
            progressHuD?.dismiss()
            setupDashTasksRecyclerView()
        }


    }


    private fun dashBoardOnlineApiCall(
        date: String,
        assignTo: String,
        territory: String,
        pendingTask: String,
        type: String
    ) {
        dash_tab_click = getSharedPref("dash_tab_click", this)
        mainProgressBar.visibility = View.VISIBLE
        if (pageLoad == 1) {
            dashSectionModel.clear()
            dashTaskModel.clear()
            CoroutineScope(Dispatchers.IO).launch {
                db = AppDatabase.getDatabaseClient(applicationContext)
                db!!.salesDao().deleteAll()
                db!!.dashBoardDateAddDao().deleteAll()

            }
        }


        val cal = Calendar.getInstance()
        var tz: TimeZone = cal.timeZone
        progressHuD = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            //.setDetailsLabel("Downloading data")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .setBackgroundColor(57000000)

        var params = HashMap<String, Any>()
        params["date"] = date
        if (assignTo == "null") {
            assignTo == ""
            params["assignedTo"] = ""
        } else {
            params["assignedTo"] = assignTo
        }
        params["territoryId"] = territory
        params["pending_tasks"] = pendingTask
        params["page"] = pageLoad
        params["pageSize"] = pageSize
        params["type"] = type
        set_timezone = this.getSharedPref("set_timezone", this).toString()
        if (set_timezone != "null") {
            params["timezone"] = set_timezone
        } else {
            params["timezone"] = tz.id
        }
        if (cmpNowDate == cmpCalDate) {
            params["pending"] = true
        } else if (dateSelected == todayDate) {
            params["pending"] = true
        }
        println("Url_dashboard"+" "+   webService.Dashboard_url)
        println("Url_dashboard_params"+" "+   params)
        this.let {
            apiRequest.postRequestBodyWithHeadersAny(
                this,
                webService.Dashboard_url,
                params
            ) { response ->
                try {
                    println("Url_dashboard_response"+" "+   response)

                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200") {
                        var dataArray = response.getJSONArray("data")
                        println("Response_data_dash_array" + "" + dataArray)

                        for (i in 0 until dataArray.length())
                        {
                            //                            dashTaskModel.clear()
                            dashTaskModel = ArrayList()
                            var dataObj = dataArray.getJSONObject(i)

                            var date = dataObj.getString("date")
                            CoroutineScope(Dispatchers.Main).launch {
                                db = AppDatabase.getDatabaseClient(applicationContext)
                                db!!.dashBoardDateAddDao().insertAll(DashBoardDateAdd(0, date));
                            }
                            var dataArray1 = dataObj.getJSONArray("data")

                            for (j in 0 until dataArray1.length()) {
                                var dataObj1 = dataArray1.getJSONObject(j)
                                var _id = String()
                                if (dataObj1.has("_id")) {
                                    _id = dataObj1.getString("_id")
                                }
                                var taskStatus = String()
                                if (dataObj1.has("taskStatus")) {
                                    taskStatus = dataObj1.getString("taskStatus")
                                }
                                var title = String()
                                if (dataObj1.has("title")) {
                                    title = dataObj1.getString("title")
                                }

                                var account_address = String()
                                if (dataObj1.has("account_address")) {
                                    account_address = dataObj1.getString("account_address")
                                }

                                var geomettryObj = JSONObject()
                                if (dataObj1.has("geomettry")) {
                                    geomettryObj = dataObj1.getJSONObject("geomettry")
                                }

                                var lat = String()
                                var lng = String()
                                if (geomettryObj.has("lat")) {
                                    lat = geomettryObj.getString("lat")
                                }
                                if (geomettryObj.has("lng")) {
                                    lng = geomettryObj.getString("lng")
                                }

                                var isEmergency = dataObj1.getString("isEmergency")

                                var assigned_toObj = JSONObject()
                                var assignee_id = String()
                                var assignee_name = String()
                                var imgWithBase = String()
                                if (dataObj1.has("assigned_to")) {
                                    assigned_toObj = dataObj1.getJSONObject("assigned_to")

                                    if (assigned_toObj.has("_id")) {
                                        assignee_id = assigned_toObj.getString("_id")
                                    }
                                    if (assigned_toObj.has("name")) {
                                        assignee_name = assigned_toObj.getString("name")
                                    }
                                    var assignee_image = String()
                                    if (assigned_toObj.has("image")) {
                                        assignee_image = assigned_toObj.getString("image")
                                    }

                                    imgWithBase = webService.imageBasePath + assignee_image
                                }

                                var department = String()
                                if (dataObj1.has("department")) {
                                    department = dataObj1.getString("department")
                                }
                                var status = String()
                                if (dataObj1.has("status")) {
                                    status = dataObj1.getString("status")
                                }
                                var startDate = dataObj1.getString("startDate")
                                d_stTime = startDate
                                var endDate = dataObj1.getString("endDate")
                                d_edTime = endDate

                                //for event
                                var eventType = String()
                                var eventCategory = String()
                                var address = String()
                                var description = String()
                                var attendeesArr = JSONArray()
                                if (dataObj1.has("eventType")) {
                                    eventType = dataObj1.getString("eventType")
                                }
                                if (dataObj1.has("eventCategory")) {
                                    eventCategory = dataObj1.getString("eventCategory")
                                }
                                if (dataObj1.has("address")) {
                                    address = dataObj1.getString("address")
                                }
                                if (dataObj1.has("description")) {
                                    description = dataObj1.getString("description")
                                }
                                var attendee_id = String()
                                var attendee_name = String()
                                var attendee_imgWithBase = String()
                                var attendee_length = String()
                                if (dataObj1.has("attendees")) {
                                    attendeesArr = dataObj1.getJSONArray("attendees")
                                    attendee_length = attendeesArr.length().toString()
                                    for (a in 0 until attendeesArr.length()) {
                                        var attendeeObj = attendeesArr.getJSONObject(a)
                                        attendee_id = attendeeObj.getString("_id")
                                        attendee_name = attendeeObj.getString("name")
                                        var attendee_image = String()

                                        if (attendeeObj.has("image")) {
                                            attendee_image = attendeeObj.getString("image")
                                        }

                                        attendee_imgWithBase =
                                            webService.imageBasePath + attendee_image

                                    }
                                }

                                var organizerId = String()
                                var organizerName = String()
                                var organizerImage = String()
                                if (dataObj1.has("organizerId")) {
                                    var organizerIdArray = dataObj1.getJSONArray("organizerId")
                                    for (org in 0 until organizerIdArray.length()) {
                                        var organizerObj = organizerIdArray.getJSONObject(org)
                                        organizerId = organizerObj.getString("_id")
                                        organizerName = organizerObj.getString("name")
                                        var profilePath = organizerObj.getString("profilePath")
                                        organizerImage = webService.imageBasePath + profilePath
                                    }
                                }
                                var type = dataObj1.getString("type")


                                if (response.has("pager")) {
                                    var pagerObj = response.getJSONObject("pager")
                                    var totalItems = pagerObj.getString("totalItems")
                                    var currentP = pagerObj.getString("currentPage")
                                    try {
                                        pageSize = pagerObj.getString("pageSize").toInt()
                                    } catch (e: java.lang.Exception) {
                                        print(e)
                                    }


                                    var totalPages = pagerObj.getString("totalPages")
                                    currentPage = pagerObj.getString("startPage")
                                    lastPage = pagerObj.getString("endPage")
                                    Log.i("lastPage== ", lastPage)
                                    var startIndex = pagerObj.getString("startIndex")
                                    var endIndex = pagerObj.getString("endIndex")
                                }


                                dashTaskModel.add(
                                    DashTasksModel(

                                        date,
                                        _id,
                                        taskStatus,
                                        title,
                                        account_address,
                                        lat,
                                        lng,
                                        isEmergency,
                                        assignee_id,
                                        assignee_name,
                                        imgWithBase,
                                        department,
                                        status,
                                        startDate,
                                        endDate,
                                        type,
                                        eventType,
                                        eventCategory,
                                        address,
                                        description,

                                        attendee_name,
                                        attendee_imgWithBase,
                                        attendee_id,
                                        attendee_length,
                                        organizerId,
                                        organizerName,
                                        organizerImage

                                    )
                                )
                                ///offline update
                                CoroutineScope(Dispatchers.Main).launch {

                                    db = AppDatabase.getDatabaseClient(applicationContext)

                                    db!!.salesDao().insertAll(
                                        DashBoard(
                                            0,

                                            date,
                                            _id,
                                            taskStatus,
                                            title,
                                            account_address,
                                            lat,
                                            lng,
                                            isEmergency,
                                            assignee_id,
                                            assignee_name,
                                            imgWithBase,
                                            department,
                                            status,
                                            startDate,
                                            endDate,
                                            type,
                                            eventType,
                                            eventCategory,
                                            address,
                                            description,
                                            attendee_name,
                                            attendee_imgWithBase,
                                            attendee_id,
                                            attendee_length,
                                            organizerId,
                                            organizerName,
                                            organizerImage

                                        )
                                    )
                                }
                                progressHuD?.dismiss()
                            }
                            dashSectionModel.add(DashSectionModel(date, dashTaskModel))
                            progressHuD?.dismiss()
                        }
                        setupDashTasksOnlineRecyclerView(dashSectionModel)
                    } else if (status == "403") {
                        mainProgressBar.visibility = View.GONE
                        toast(msg)
                    } else if (status == "401") {
                        mainProgressBar.visibility = View.GONE
                        progressHuD?.dismiss()
                        noTaskScrollView.visibility = View.VISIBLE
                        no_task_layout.visibility = View.VISIBLE
                        dashboard_recyclerView.visibility = View.GONE
                        main_fragmet_container.visibility = View.GONE
                        if (dashTaskModel.size == 0) {
                            if (dash_tab_click == "taskOnClick") {
                                noData_note.text = "No Upcoming Tasks"
                                noTask_img.setImageDrawable(
                                    ContextCompat.getDrawable(
                                        this,
                                        R.drawable.no_task_img
                                    )
                                )
                                schedule_your_task_desc.visibility = View.GONE
                                schedule_your_task_tv.visibility = View.GONE
                                schedule_your_task_tv.text = "Schedule Your Task"
                                schedule_your_task_desc.text =
                                    "Manage your task schedule easily and efficiently"
                                createNewTask.visibility = View.GONE
                                no_task_layout.visibility = View.VISIBLE
                                noTaskScrollView.visibility = View.VISIBLE
                            } else if (dash_tab_click == "meOnClick") {
                                noData_note.text = "No Tasks/Events Pending Acceptance"
                                noTask_img.setImageDrawable(
                                    ContextCompat.getDrawable(
                                        this,
                                        R.drawable.ic_me_no_task_icon
                                    )
                                )
                                schedule_your_task_tv.text = "Add your task with SalesC2"
                                schedule_your_task_desc.text =
                                    "Manage your task schedule easily and efficiently"
                                createNewTask.visibility = View.GONE
                                no_task_layout.visibility = View.VISIBLE
                                noTaskScrollView.visibility = View.VISIBLE

                                schedule_your_task_desc.visibility = View.GONE
                                schedule_your_task_tv.visibility = View.GONE
                            } else if (dash_tab_click == "teamOnClick") {
                                noData_note.text = "No Tasks Pending Assignment"
                                noTask_img.setImageDrawable(
                                    ContextCompat.getDrawable(
                                        this,
                                        R.drawable.ic_team_no_task_icon
                                    )
                                )
                                schedule_your_task_tv.text = "Add your task with SalesC2"
                                schedule_your_task_desc.text =
                                    "Manage your task schedule easily and efficiently"
                                createNewTask.visibility = View.GONE
                                no_task_layout.visibility = View.VISIBLE
                                noTaskScrollView.visibility = View.VISIBLE

                                schedule_your_task_desc.visibility = View.GONE
                                schedule_your_task_tv.visibility = View.GONE
                            } else {
                                noData_note.text = "No Upcoming Tasks"
                                noTask_img.setImageDrawable(
                                    ContextCompat.getDrawable(
                                        this,
                                        R.drawable.no_task_img
                                    )
                                )
                                schedule_your_task_tv.text = "Schedule Your Task"
                                schedule_your_task_desc.text =
                                    "Manage your task schedule easily and efficiently"
                                createNewTask.visibility = View.GONE
                                no_task_layout.visibility = View.VISIBLE
                                noTaskScrollView.visibility = View.VISIBLE

                                schedule_your_task_desc.visibility = View.GONE
                                schedule_your_task_tv.visibility = View.GONE
                            }

                        }
                    } else {
                        mainProgressBar.visibility = View.GONE
                        progressHuD?.dismiss()
                        toast(msg)
                    }

                } catch (e: Exception) {
                    mainProgressBar.visibility = View.GONE
                    progressHuD?.dismiss()
                    toast(e.toString())
                }
            }
        }
    }

    private fun setupDashTasksOnlineRecyclerView(dashSectionModel: ArrayList<DashSectionModel>) {
        runOnUiThread {
            noTaskScrollView.visibility = View.GONE
            mainProgressBar.visibility = View.GONE
            no_task_layout.visibility = View.GONE
            main_fragmet_container.visibility = View.GONE
            dashboard_recyclerView.visibility = View.VISIBLE


            val adapter: RecyclerView.Adapter<*> =
                MainRecyclerAdapter(this@MainActivity, dashSectionModel)
            val llm = LinearLayoutManager(this@MainActivity)
            llm.orientation = LinearLayoutManager.VERTICAL
            dashboard_recyclerView.layoutManager = llm
            dashboard_recyclerView.adapter = adapter
            dashboard_recyclerView.setHasFixedSize(true)
            adapter.notifyDataSetChanged()

            dashboard_recyclerView.addOnScrollListener(object :
                RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(
                    recyclerView: RecyclerView,
                    newState: Int
                ) {
                    super.onScrollStateChanged(recyclerView, newState)
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    val visibleItemCount = llm.childCount
                    val totalItemCount = llm.itemCount
                    val firstVisibleItem = llm.findFirstVisibleItemPosition()


                    if (isLoading) {
                        if (totalItemCount > previousTotal) {
                            previousTotal = totalItemCount
//                        var i = currentPage.toInt()
                            pageLoad++
                            isLoading = false
                            Log.v("...", "Last Item Wow !")

                        }
                    }
                    if (!isLoading && (firstVisibleItem + visibleItemCount) >= totalItemCount && pageLoad != lastPage.toInt() + 1) {
                        isLoading = true
                        if (dash_tab_click == "taskOnClick") {
                            dashBoardApiCall(dateSelected, "", "", "", "all")
                        } else if (dash_tab_click == "meOnClick") {
                            dashBoardApiCall(dateSelected, "", "", "me", "")
                        } else if (dash_tab_click == "teamOnClick") {
                            dashBoardApiCall(dateSelected, "", "", "team", "")
                        }
                    }
                }


            })

        }

    }


    //    setup adapter
    private fun setupDashTasksRecyclerView() {
        dashTaskModel.clear()
        dashSectionModel.clear()

        lifecycleScope.launch {

            delay(500)

            CoroutineScope(Dispatchers.Main).launch {
                db = AppDatabase.getDatabaseClient(applicationContext)
                val dashData = db!!.salesDao().getAll()
                val dateData = db!!.dashBoardDateAddDao().fetchDistinctData()

                Log.i("dateData", dateData.toString())
                for (k in 0 until dateData.size) {

                    //dashTaskModel.clear()
                    dashTaskModel = ArrayList()

                    for (n in 0 until dashData.size) {

                        if (dateData[k]?.data!!.equals(dashData[n]?.date.toString())) {
                            Log.i("dateInside=", dashData[n]?.date.toString())
                            dashTaskModel.add(
                                DashTasksModel(
                                    dashData[n].date.toString(),
                                    dashData[n]._id.toString(),
                                    dashData[n].taskStatus.toString(),
                                    dashData[n].title.toString(),
                                    dashData[n].account_address.toString(),
                                    dashData[n].lat.toString(),
                                    dashData[n].lng.toString(),
                                    dashData[n].isEmergency.toString(),
                                    dashData[n].assignTo_id.toString(),
                                    dashData[n].assignTo_name.toString(),
                                    dashData[n].assignTo_img.toString(),
                                    dashData[n].department.toString(),
                                    dashData[n].status.toString(),
                                    dashData[n].startDate.toString(),
                                    dashData[n].endDate.toString(),
                                    dashData[n].type.toString(),
                                    dashData[n].event_eventType.toString(),
                                    dashData[n].event_eventCategory.toString(),
                                    dashData[n].event_address.toString(),
                                    dashData[n].event_description.toString(),
                                    dashData[n].attendee_name.toString(),
                                    dashData[n].attendee_img.toString(),
                                    dashData[n].attendee_id.toString(),
                                    dashData[n].attendee_length.toString(),
                                    dashData[n].organizerId.toString(),
                                    dashData[n].organizerName.toString(),
                                    dashData[n].organizerImage.toString()

                                )
                            )
                        }
                    }

                    dashSectionModel.add(
                        DashSectionModel(
                            dateData[k]?.data.toString(),
                            dashTaskModel
                        )
                    )

                }
                runOnUiThread {
                    noTaskScrollView.visibility = View.GONE
                    mainProgressBar.visibility = View.GONE
                    no_task_layout.visibility = View.GONE
                    main_fragmet_container.visibility = View.GONE
                    dashboard_recyclerView.visibility = View.VISIBLE


                    val adapter: RecyclerView.Adapter<*> =
                        MainRecyclerAdapter(this@MainActivity, dashSectionModel)
                    val llm = LinearLayoutManager(this@MainActivity)
                    llm.orientation = LinearLayoutManager.VERTICAL
                    dashboard_recyclerView.layoutManager = llm
                    dashboard_recyclerView.adapter = adapter
                    dashboard_recyclerView.setHasFixedSize(true)
                    adapter.notifyDataSetChanged()

                   /* dashboard_recyclerView.addOnScrollListener(object :
                        RecyclerView.OnScrollListener() {
                        override fun onScrollStateChanged(
                            recyclerView: RecyclerView,
                            newState: Int
                        ) {
                            super.onScrollStateChanged(recyclerView, newState)
                        }

                        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                            super.onScrolled(recyclerView, dx, dy)

                            val visibleItemCount = llm.childCount
                            val totalItemCount = llm.itemCount
                            val firstVisibleItem = llm.findFirstVisibleItemPosition()


                            if (isLoading) {
                                if (totalItemCount > previousTotal) {
                                    previousTotal = totalItemCount
//                        var i = currentPage.toInt()
                                    pageLoad++
                                    isLoading = false
                                    Log.v("...", "Last Item Wow !")

                                }
                            }
                            *//* if (!isLoading && (firstVisibleItem + visibleItemCount) >= totalItemCount && pageLoad != lastPage.toInt()+1) {
                                 isLoading = true
                                 if (dash_tab_click == "taskOnClick"){
                                     dashBoardApiCall(dateSelected,"","","","all")
                                 }else if (dash_tab_click == "meOnClick"){
                                     dashBoardApiCall(dateSelected,"","","me","")
                                 }else if(dash_tab_click == "teamOnClick"){
                                     dashBoardApiCall(dateSelected,"","","team","")
                                 }
                             }*//*
                        }


                    })*/

                }


            }


        }


    }

    private fun notificationOnClick() {
        bell_main.setOnClickListener {
            notificationListModel.clear()
            notificationListApiCall()
            refreshDashboardAPIs()
            loadFragment(NotificationsListFragment())
//            startActivity(Intent(this,NotificationListActivity::class.java))
        }

    }


    private fun addTaskAction() {
        addTask_appIcon.setOnClickListener(View.OnClickListener {
            loadFragment(SelectAccountFragment())

        })
    }


    private fun dashboardCountApiCall() {
        val cal = Calendar.getInstance()
        var tz: TimeZone = cal.timeZone
        var params = HashMap<String, Any>()
        params["date"] = todayDate
        if (set_timezone != "null") {
            params["timezone"] = set_timezone
        } else {
            params["timezone"] = tz.id
        }
        this.apiRequest.postRequestBodyWithHeadersAny(
            this,
            webService.Dashboard_count_url,
            params
        ) { response ->
            try {
                var status = response.getString("status")
                if (status == "200") {
                    var dataObj = response.getJSONObject("data")
                    todays_task = dataObj.getString("todays_task")
                    pending_task_for_me = dataObj.getString("pending_task_for_me")
                    pending_task_for_team = dataObj.getString("pending_task_for_team")
                    dash_user_tasks_count.text = "You have ${todays_task} tasks for the day"

                    if (pending_task_for_me.toInt() >= 1) {

                        dash_p_accept_tab.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            R.drawable.ic_dash_p_acceptance_icon,
                            R.drawable.ic_dot_icon_small,
                            0
                        )
                    }
                    if (pending_task_for_team.toInt() >= 1) {
//                        dash_p_assign_tab.compoundDrawablePadding = 0
                        dash_p_assign_tab.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            R.drawable.ic_dash_p_assignment_icon,
                            R.drawable.ic_dot_icon_small,
                            0
                        )
                    }


                } else if (status == "400") {
                    var msg = response.getString("msg")
                    toast(msg)
                }

            } catch (e: Exception) {
                print(e)
            }

        }
    }

    private fun notificationCountApiCall() {
        val params = HashMap<String, Any>()
        apiRequest.getRequestBodyWithHeaders(
            this,
            webService.Notification_count_url,
            params
        ) { response ->
            try {
                var status = response.getString("status")
                if (status == "200") {
                    notificationCount = response.getString("count")
                    notificationCount_tv.setText(notificationCount)
                } else if (status == "403") {
                    clearPrefAndNavigateToLogin()
                }
            } catch (e: Exception) {

                toast("Notification count error: ${e.toString()} ")
            }
        }
    }

    private fun clearPrefAndNavigateToLogin() {
        val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        val editor: SharedPreferences.Editor = preferences.edit()

        editor.remove("filter_account_id")
        editor.remove("filter_assignee_id")
        editor.remove("filter_status_name")
        editor.remove("filter_start_dt")
        editor.remove("filter_end_dt")
        editor.remove("filter_territory_id")
        //filter items
        editor.remove("status_position")
        editor.remove("filter_assignee_position")
        editor.remove("filter_account_position")
        editor.remove("filter_territory_position")

        editor.remove("eAttendee_name")
        editor.remove("eAttendee_id")
        editor.remove("eAttendee_img")
        //add task
        editor.remove("assignee_name")
        editor.remove("assignee_id")
        editor.remove("assignee_image")

        //add event title and desc
        editor.remove("eTitle")
        editor.remove("eDesc")

        //dashboard prefs
        editor.remove("dash_territory_id")
        editor.remove("dash_assignee_id")

        //add task desc
        editor.remove("tDesc")
        //territory filter toolbar
        editor.remove("territory_toolbar")

        //add task filter status //show tick mark
        editor.remove("acc_filter_position")
        editor.remove("dept_filter_position")
        editor.remove("service_filter_position")

        //edit event type
        editor.remove("edit_add_event")
        //event attendee position
        editor.remove("selected_eAttendee_position")
        editor.remove("selected_eType_position")

        //dash assignee filter position
        editor.remove("dash_filter_assignee_position")
        editor.remove("dash_filter_territory_position")

        //edit task
        editor.remove("edit_task")

        //event location lat lng and address
        editor.remove("eLocation_name")
        editor.remove("latitude")
        editor.remove("longitude")

        //reassign in dash list p_assign
        editor.remove("reassign_dash_assignee")

        //edit date time in edit task
        editor.remove("task_dt_edited")

        //notification reassigne pos nd task id
        editor.remove("notify_re_assignee_position")
        editor.remove("task_id_notify")

        //edit event
        editor.remove("edit_event")


        //task start end date
        editor.remove("start_date")
        editor.remove("end_date")
        editor.remove("end_time")
        editor.remove("start_time")


        //event start end date
        editor.remove("event_start_date")
        editor.remove("event_start_time")
        editor.remove("event_end_date")
        editor.remove("event_end_time")

        //task detail assignee change
        editor.remove("update_api_call")


        //taskdetails assigne adapter
        editor.remove("task_details_assignee_position")

        //addtask assignee
        editor.remove("addTasK_assignee_position")

        //td and ed priority switch handle
        editor.remove("isChecked_task_priority")
        editor.remove("isChecked_event_priority")

        //task details date edit
        editor.remove("edit_task_detail_date")

        editor.remove("event_attendee_list_selected")

        editor.remove("token")
//add task edit attendee tick issue
        editor.remove("edit_task_assignee_id")
        editor.remove("fromClass")
        editor.commit()

        val databaseHandler: DatabaseHandler = DatabaseHandler(this)
        databaseHandler.deleteProducts()
        databaseHandler.deleteAttendees()

        startActivity(Intent(this, LoginActivity::class.java))
    }

    //bottom navigation iteam
    private var onNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_dashboard -> {
                    refreshDashboardAPIs()
                    val preferences: SharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(this)
                    val editor: SharedPreferences.Editor = preferences.edit()
                    editor.remove("eAttendee_name")
                    editor.remove("eAttendee_id")
                    editor.remove("eAttendee_img")
                    //add task
                    editor.remove("assignee_name")
                    editor.remove("assignee_id")
                    editor.remove("assignee_image")

//                    //filter prefs
//                    editor.remove("filter_account_id")
//                    editor.remove("filter_assignee_id")
//                    editor.remove("filter_status_name")
//                    editor.remove("filter_start_dt")
//                    editor.remove("filter_end_dt")
//                    editor.remove("filter_territory_id")

//                    //filter items
//                    editor.remove("status_position")
//                    editor.remove("filter_assignee_position")
//                    editor.remove("filter_account_position")
//                    editor.remove("filter_territory_position")

                    //add event title and desc
                    editor.remove("eTitle")
                    editor.remove("eDesc")


                    //dashboard prefs
//                    editor.remove("dash_tab_click")
                    editor.remove("dash_territory_id")
                    editor.remove("dash_assignee_id")

                    //add task desc
                    editor.remove("tDesc")
                    //territory filter toolbar
                    editor.remove("territory_toolbar")

                    //add task filter status //show tick mark
                    editor.remove("acc_filter_position")
                    editor.remove("dept_filter_position")
                    editor.remove("service_filter_position")

                    //edit event type
                    editor.remove("edit_add_event")
                    //event attendee position
                    editor.remove("selected_eAttendee_position")
                    editor.remove("selected_eType_position")

                    //dash assignee filter position
                    editor.remove("dash_filter_assignee_position")
                    editor.remove("dash_filter_territory_position")

                    //edit task
                    editor.remove("edit_task")

                    //event location lat lng and address
                    editor.remove("eLocation_name")
                    editor.remove("latitude")
                    editor.remove("longitude")

                    //reassign in dash list p_assign
                    editor.remove("reassign_dash_assignee")

                    //edit date time in edit task
                    editor.remove("task_dt_edited")

                    //notification reassigne pos nd task id
                    editor.remove("notify_re_assignee_position")
                    editor.remove("task_id_notify")

                    //edit event
                    editor.remove("edit_event")


                    //task start end date
                    editor.remove("start_date")
                    editor.remove("end_date")
                    editor.remove("end_time")
                    editor.remove("start_time")


                    //event start end date
                    editor.remove("event_start_date")
                    editor.remove("event_start_time")
                    editor.remove("event_end_date")
                    editor.remove("event_end_time")

                    //task detail assignee change
                    editor.remove("update_api_call")


                    //taskdetails assigne adapter
                    editor.remove("task_details_assignee_position")

                    //addtask assignee
                    editor.remove("addTasK_assignee_position")

                    //td and ed priority switch handle
                    editor.remove("isChecked_task_priority")
                    editor.remove("isChecked_event_priority")

                    //task details date edit
                    editor.remove("edit_task_detail_date")
                    editor.remove("event_attendee_list_selected")

                    //add task edit attendee tick issue
                    editor.remove("edit_task_assignee_id")

                    //payment
                    editor.remove("fromPage")
                    editor.remove("editCard")
                    editor.remove("bcFrom")

                    editor.remove("fromClass")
                    editor.remove("fromPage")
                    editor.commit()

                    val databaseHandler: DatabaseHandler = DatabaseHandler(this)
                    databaseHandler.deleteProducts()
                    databaseHandler.deleteAttendees()

                    territoryId = getSharedPref("dash_territory_id", this)
                    assignToId = getSharedPref("dash_assignee_id", this)

                    if (assignToId != "null") {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_filter_assignee_icon
                            )
                        }?.let {
                            dash_assignee_filter.setImageDrawable(it)
                        }

                    } else {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_assignee_grey
                            )
                        }?.let {
                            dash_assignee_filter.setImageDrawable(it)
                        }
                    }
                    if (territoryId != "null") {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_territory_green
                            )
                        }?.let {
                            dash_territory_filter.setImageDrawable(it)
                        }
                    } else {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_filter_territory_icon
                            )
                        }
                            ?.let {
                                dash_territory_filter.setImageDrawable(it)
                            }
                    }


//                    loadFragment(DashboardFragment())
                    supportFragmentManager.beginTransaction().replace(
                        R.id.main_fragmet_container,
                        DashboardFragment(),
                        "DashboardFragment"
                    ).addToBackStack("DashboardFragment").commit()


                    return@OnNavigationItemSelectedListener true

                }
                R.id.navigation_team -> {
                    val preferences: SharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(this)
                    val editor: SharedPreferences.Editor = preferences.edit()
                    editor.remove("eAttendee_name")
                    editor.remove("eAttendee_id")
                    editor.remove("eAttendee_img")
                    //add task
                    editor.remove("assignee_name")
                    editor.remove("assignee_id")
                    editor.remove("assignee_image")

//                    //filter prefs
//                    editor.remove("filter_account_id")
//                    editor.remove("filter_assignee_id")
//                    editor.remove("filter_status_name")
//                    editor.remove("filter_start_dt")
//                    editor.remove("filter_end_dt")
//                    editor.remove("filter_territory_id")
//                    //filter items
//                    editor.remove("status_position")
//                    editor.remove("filter_assignee_position")
//                    editor.remove("filter_account_position")
//                    editor.remove("filter_territory_position")

                    editor.remove("eTitle")
                    editor.remove("eDesc")

                    //dashboard prefs
//                    editor.remove("dash_tab_click")
                    editor.remove("dash_territory_id")
                    editor.remove("dash_assignee_id")

                    //add task desc
                    editor.remove("tDesc")

                    //territory filter toolbar
                    editor.remove("territory_toolbar")

                    //add task filter status //show tick mark
                    editor.remove("acc_filter_position")
                    editor.remove("dept_filter_position")
                    editor.remove("service_filter_position")


                    //edit event type
                    editor.remove("edit_add_event")

                    //event attendee position
                    editor.remove("selected_eAttendee_position")
                    editor.remove("selected_eType_position")

                    //dash assignee filter position
                    editor.remove("dash_filter_assignee_position")
                    editor.remove("dash_filter_territory_position")

                    //edit task
                    editor.remove("edit_task")

                    //event location lat lng and address
                    editor.remove("eLocation_name")
                    editor.remove("latitude")
                    editor.remove("longitude")

                    //reassign in dash list p_assign
                    editor.remove("reassign_dash_assignee")

                    //edit date time in edit task
                    editor.remove("task_dt_edited")

                    //notification reassigne pos nd task id
                    editor.remove("notify_re_assignee_position")
                    editor.remove("task_id_notify")
//edit event
                    editor.remove("edit_event")

                    //task start end date
                    editor.remove("start_date")
                    editor.remove("end_date")
                    editor.remove("end_time")
                    editor.remove("start_time")

                    //event start end date
                    editor.remove("event_start_date")
                    editor.remove("event_start_time")
                    editor.remove("event_end_date")
                    editor.remove("event_end_time")


                    //task detail assignee change
                    editor.remove("update_api_call")
                    //taskdetails assigne adapter
                    editor.remove("task_details_assignee_position")

                    //addtask assignee
                    editor.remove("addTasK_assignee_position")

//td and ed priority switch handle
                    editor.remove("isChecked_task_priority")
                    editor.remove("isChecked_event_priority")

                    //task details date edit
                    editor.remove("edit_task_detail_date")

                    editor.remove("event_attendee_list_selected")

                    //add task edit attendee tick issue
                    editor.remove("edit_task_assignee_id")

                    editor.remove("fromClass")

                    editor.remove("fromPage")
                    editor.commit()
                    val databaseHandler: DatabaseHandler = DatabaseHandler(this)
                    databaseHandler.deleteProducts()
                    databaseHandler.deleteAttendees()

                    loadFragment(TeamMembersListFragment())


                    if (assignToId != "null") {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_filter_assignee_icon
                            )
                        }?.let {
                            dash_assignee_filter.setImageDrawable(it)
                        }

                    } else {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_assignee_grey
                            )
                        }?.let {
                            dash_assignee_filter.setImageDrawable(it)
                        }
                    }
                    if (territoryId != "null") {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_territory_green
                            )
                        }?.let {
                            dash_territory_filter.setImageDrawable(it)
                        }
                    } else {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_filter_territory_icon
                            )
                        }
                            ?.let {
                                dash_territory_filter.setImageDrawable(it)
                            }
                    }

                    return@OnNavigationItemSelectedListener true

                }
                R.id.navigation_mid -> {
                    val preferences: SharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(this)
                    val editor: SharedPreferences.Editor = preferences.edit()
                    editor.remove("eAttendee_name")
                    editor.remove("eAttendee_id")
                    editor.remove("eAttendee_img")
                    //add task
                    editor.remove("assignee_name")
                    editor.remove("assignee_id")
                    editor.remove("assignee_image")

//                    //filter prefs
//                    editor.remove("filter_account_id")
//                    editor.remove("filter_assignee_id")
//                    editor.remove("filter_status_name")
//                    editor.remove("filter_start_dt")
//                    editor.remove("filter_end_dt")
//                    editor.remove("filter_territory_id")
//                    //filter items
//                    editor.remove("status_position")
//                    editor.remove("filter_assignee_position")
//                    editor.remove("filter_account_position")
//                    editor.remove("filter_territory_position")

                    editor.remove("eTitle")
                    editor.remove("eDesc")

                    //dashboard prefs
//                    editor.remove("dash_tab_click")
                    editor.remove("dash_territory_id")
                    editor.remove("dash_assignee_id")

                    //add task desc
                    editor.remove("tDesc")
                    //territory filter toolbar
                    editor.remove("territory_toolbar")

                    //add task filter status //show tick mark
                    editor.remove("acc_filter_position")
                    editor.remove("dept_filter_position")
                    editor.remove("service_filter_position")


                    //edit event type
                    editor.remove("edit_add_event")

                    //event attendee position
                    editor.remove("selected_eAttendee_position")
                    editor.remove("selected_eType_position")

                    //dash assignee filter position
                    editor.remove("dash_filter_assignee_position")
                    editor.remove("dash_filter_territory_position")

                    //edit task
                    editor.remove("edit_task")

                    //event location lat lng and address
                    editor.remove("eLocation_name")
                    editor.remove("latitude")
                    editor.remove("longitude")

                    //reassign in dash list p_assign
                    editor.remove("reassign_dash_assignee")

                    //edit date time in edit task
                    editor.remove("task_dt_edited")

                    //notification reassigne pos nd task id
                    editor.remove("notify_re_assignee_position")
                    editor.remove("task_id_notify")

                    //edit event
                    editor.remove("edit_event")

                    //task start end date
                    editor.remove("start_date")
                    editor.remove("end_date")
                    editor.remove("end_time")
                    editor.remove("start_time")

                    //event start end date
                    editor.remove("event_start_date")
                    editor.remove("event_start_time")
                    editor.remove("event_end_date")
                    editor.remove("event_end_time")

                    //task detail assignee change
                    editor.remove("update_api_call")

                    //taskdetails assigne adapter
                    editor.remove("task_details_assignee_position")

                    //addtask assignee
                    editor.remove("addTasK_assignee_position")

                    //td and ed priority switch handle
                    editor.remove("isChecked_task_priority")
                    editor.remove("isChecked_event_priority")

                    //task details date edit
                    editor.remove("edit_task_detail_date")

                    editor.remove("event_attendee_list_selected")

                    //add task edit attendee tick issue
                    editor.remove("edit_task_assignee_id")

                    editor.remove("fromClass")
                    editor.remove("fromPage")
                    editor.commit()
                    val databaseHandler: DatabaseHandler = DatabaseHandler(this)
                    databaseHandler.deleteProducts()
                    databaseHandler.deleteAttendees()


                    val popupWindowHelper: PopupWindowHelper
                    val popView: View
                    popView = LayoutInflater.from(this).inflate(R.layout.nav_mid_popup, null)
                    popupWindowHelper = PopupWindowHelper(popView)
                    popupWindowHelper.showFromBottom(bottomNavigationView)
                    var popup_newTask = popView?.findViewById<TextView>(R.id.popup_newTask)
                    var popup_meeting = popView?.findViewById<TextView>(R.id.popup_meeting)
                    var popup_personal = popView?.findViewById<TextView>(R.id.popup_personal)
                    var popCardView: CardView? = popView?.findViewById<CardView>(R.id.popCardView)
                    popCardView?.setOnClickListener {
                        popupWindowHelper.dismiss()
                    }
                    popup_newTask?.setOnClickListener {
                        popupWindowHelper.dismiss()
                        loadFragment(SelectAccountFragment())
                    }
                    popup_meeting?.setOnClickListener {
                        popupWindowHelper.dismiss()
                        loadFragment(SelectEventTypeFragment())
                        this.setSharedPref(this, "eType_type", "1")
                    }
                    popup_personal?.setOnClickListener {
                        popupWindowHelper.dismiss()
                        loadFragment(SelectEventTypeFragment())
                        this.setSharedPref(this, "eType_type", "0")

                    }




                    if (assignToId != "null") {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_filter_assignee_icon
                            )
                        }?.let {
                            dash_assignee_filter.setImageDrawable(it)
                        }

                    } else {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_assignee_grey
                            )
                        }?.let {
                            dash_assignee_filter.setImageDrawable(it)
                        }
                    }
                    if (territoryId != "null") {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_territory_green
                            )
                        }?.let {
                            dash_territory_filter.setImageDrawable(it)
                        }
                    } else {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_filter_territory_icon
                            )
                        }
                            ?.let {
                                dash_territory_filter.setImageDrawable(it)
                            }
                    }

                    return@OnNavigationItemSelectedListener true
                }

                R.id.navigation_tasks -> {
                    val preferences: SharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(this)
                    val editor: SharedPreferences.Editor = preferences.edit()
                    editor.remove("eAttendee_name")
                    editor.remove("eAttendee_id")
                    editor.remove("eAttendee_img")
                    //add task
                    editor.remove("assignee_name")
                    editor.remove("assignee_id")
                    editor.remove("assignee_image")
//                    //filter prefs
//                    editor.remove("filter_account_id")
//                    editor.remove("filter_assignee_id")
//                    editor.remove("filter_status_name")
//                    editor.remove("filter_start_dt")
//                    editor.remove("filter_end_dt")
//                    editor.remove("filter_territory_id")
//                    //filter items
//                    editor.remove("status_position")
//                    editor.remove("filter_assignee_position")
//                    editor.remove("filter_account_position")
//                    editor.remove("filter_territory_position")

                    editor.remove("eTitle")
                    editor.remove("eDesc")

                    //dashboard prefs
//                    editor.remove("dash_tab_click")
                    editor.remove("dash_territory_id")
                    editor.remove("dash_assignee_id")

                    //add task desc
                    editor.remove("tDesc")
                    //territory filter toolbar
                    editor.remove("territory_toolbar")

                    //add task filter status //show tick mark
                    editor.remove("acc_filter_position")
                    editor.remove("dept_filter_position")
                    editor.remove("service_filter_position")


                    //edit event type
                    editor.remove("edit_add_event")

                    //event attendee position
                    editor.remove("selected_eAttendee_position")
                    editor.remove("selected_eType_position")

                    //dash assignee filter position
                    editor.remove("dash_filter_assignee_position")
                    editor.remove("dash_filter_territory_position")

                    //edit task
                    editor.remove("edit_task")

                    //event location lat lng and address
                    editor.remove("eLocation_name")
                    editor.remove("latitude")
                    editor.remove("longitude")

                    //reassign in dash list p_assign
                    editor.remove("reassign_dash_assignee")

                    //edit date time in edit task
                    editor.remove("task_dt_edited")

                    //notification reassigne pos nd task id
                    editor.remove("notify_re_assignee_position")
                    editor.remove("task_id_notify")

                    //edit event
                    editor.remove("edit_event")

                    //task start end date
                    editor.remove("start_date")
                    editor.remove("end_date")
                    editor.remove("end_time")
                    editor.remove("start_time")

                    //event start end date
                    editor.remove("event_start_date")
                    editor.remove("event_start_time")
                    editor.remove("event_end_date")
                    editor.remove("event_end_time")
                    //task detail assignee change
                    editor.remove("update_api_call")

                    //taskdetails assigne adapter
                    editor.remove("task_details_assignee_position")

                    //addtask assignee
                    editor.remove("addTasK_assignee_position")

                    //td and ed priority switch handle
                    editor.remove("isChecked_task_priority")
                    editor.remove("isChecked_event_priority")
                    //task details date edit
                    editor.remove("edit_task_detail_date")

                    editor.remove("event_attendee_list_selected")

                    //add task edit attendee tick issue
                    editor.remove("edit_task_assignee_id")

                    editor.remove("fromClass")
                    editor.remove("fromPage")
                    editor.commit()
                    val databaseHandler: DatabaseHandler = DatabaseHandler(this)
                    databaseHandler.deleteProducts()
                    databaseHandler.deleteAttendees()
                    loadFragment(MapsFragment())




                    if (assignToId != "null") {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_filter_assignee_icon
                            )
                        }?.let {
                            dash_assignee_filter.setImageDrawable(it)
                        }

                    } else {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_assignee_grey
                            )
                        }?.let {
                            dash_assignee_filter.setImageDrawable(it)
                        }
                    }
                    if (territoryId != "null") {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_territory_green
                            )
                        }?.let {
                            dash_territory_filter.setImageDrawable(it)
                        }
                    } else {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_filter_territory_icon
                            )
                        }
                            ?.let {
                                dash_territory_filter.setImageDrawable(it)
                            }
                    }

                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_more -> {
                    val preferences: SharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(this)
                    val editor: SharedPreferences.Editor = preferences.edit()
                    editor.remove("eAttendee_name")
                    editor.remove("eAttendee_id")
                    editor.remove("eAttendee_img")

                    //add task
                    editor.remove("assignee_name")
                    editor.remove("assignee_id")
                    editor.remove("assignee_image")

//                    //filter prefs
//                    editor.remove("filter_account_id")
//                    editor.remove("filter_assignee_id")
//                    editor.remove("filter_status_name")
//                    editor.remove("filter_start_dt")
//                    editor.remove("filter_end_dt")
//                    editor.remove("filter_territory_id")
//                    //filter items
//                    editor.remove("status_position")
//                    editor.remove("filter_assignee_position")
//                    editor.remove("filter_account_position")
//                    editor.remove("filter_territory_position")

                    editor.remove("eTitle")
                    editor.remove("eDesc")

                    //dashboard prefs
//                    editor.remove("dash_tab_click")
                    editor.remove("dash_territory_id")
                    editor.remove("dash_assignee_id")

                    //add task desc
                    editor.remove("tDesc")
                    //territory filter toolbar
                    editor.remove("territory_toolbar")

                    //add task filter status //show tick mark
                    editor.remove("acc_filter_position")
                    editor.remove("dept_filter_position")
                    editor.remove("service_filter_position")


                    //edit event type
                    editor.remove("edit_add_event")

                    //event attendee position
                    editor.remove("selected_eAttendee_position")
                    editor.remove("selected_eType_position")

                    //dash assignee filter position
                    editor.remove("dash_filter_assignee_position")
                    editor.remove("dash_filter_territory_position")

                    //edit task
                    editor.remove("edit_task")

                    //event location lat lng and address
                    editor.remove("eLocation_name")
                    editor.remove("latitude")
                    editor.remove("longitude")

                    //reassign in dash list p_assign
                    editor.remove("reassign_dash_assignee")

                    //edit date time in edit task
                    editor.remove("task_dt_edited")

                    //notification reassigne pos nd task id
                    editor.remove("notify_re_assignee_position")
                    editor.remove("task_id_notify")


                    //edit event
                    editor.remove("edit_event")

                    //task start end date
                    editor.remove("start_date")
                    editor.remove("end_date")
                    editor.remove("end_time")
                    editor.remove("start_time")

                    //event start end date
                    editor.remove("event_start_date")
                    editor.remove("event_start_time")
                    editor.remove("event_end_date")
                    editor.remove("event_end_time")

                    //task detail assignee change
                    editor.remove("update_api_call")

                    //taskdetails assigne adapter
                    editor.remove("task_details_assignee_position")

                    //addtask assignee
                    editor.remove("addTasK_assignee_position")

                    //td and ed priority switch handle
                    editor.remove("isChecked_task_priority")
                    editor.remove("isChecked_event_priority")

                    //task details date edit
                    editor.remove("edit_task_detail_date")

                    editor.remove("event_attendee_list_selected")
                    //add task edit attendee tick issue
                    editor.remove("edit_task_assignee_id")

                    editor.remove("fromClass")

                    editor.remove("fromPage")

                    editor.commit()
                    val databaseHandler: DatabaseHandler = DatabaseHandler(this)
                    databaseHandler.deleteProducts()
                    databaseHandler.deleteAttendees()
                    loadFragment(MoreFragment())



                    if (assignToId != "null") {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_filter_assignee_icon
                            )
                        }?.let {
                            dash_assignee_filter.setImageDrawable(it)
                        }

                    } else {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_assignee_grey
                            )
                        }?.let {
                            dash_assignee_filter.setImageDrawable(it)
                        }
                    }
                    if (territoryId != "null") {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_territory_green
                            )
                        }?.let {
                            dash_territory_filter.setImageDrawable(it)
                        }
                    } else {
                        this?.let {
                            ContextCompat.getDrawable(
                                it,
                                R.drawable.ic_dash_filter_territory_icon
                            )
                        }
                            ?.let {
                                dash_territory_filter.setImageDrawable(it)
                            }
                    }

                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    //use to load fragment
    private fun loadFragment(fragment: Fragment): Boolean {
        // load fragment
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        }
        //replaceing Fragment
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.main_fragmet_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
        return true

    }

    override fun onBackPressed() {


        if (Common.className.equals("DashboardFragment")) {
            var dialog = Dialog(this)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
            dialog?.setContentView(R.layout.custom_yes_or_no_alert)
            dialog?.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
            val body = dialog?.findViewById(R.id.headtitle) as TextView
            body.text = "Alert"
            val msg1 = dialog?.findViewById(R.id.message) as TextView
            msg1.text = "Do you want to exit?"
            var dialogCancel = dialog?.findViewById(R.id.cancel) as TextView
            dialogCancel.setOnClickListener(View.OnClickListener {
                dialog.dismiss()
            })
            var dialogButton = dialog.findViewById(R.id.ok) as TextView
            dialogButton.setOnClickListener(View.OnClickListener {
                this.finishAffinity()
                dialog.dismiss()
            })
            dialog.show()
        } else {
            if (supportFragmentManager.backStackEntryCount > 1) {
                supportFragmentManager.popBackStack()
            } else if (supportFragmentManager.backStackEntryCount == 1) {
                startActivity(Intent(this, MainActivity::class.java))
                /*  val fragmentList: List<*> =
                      supportFragmentManager.fragments*/
                /*for (f in fragmentList) {
                    if (f is DashboardFragment) {
                        var dialog = Dialog(this)
                        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog?.setCancelable(false)
                        dialog?.setContentView(R.layout.custom_yes_or_no_alert)
                        dialog?.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
                        val body = dialog?.findViewById(R.id.headtitle) as TextView
                        body.text = "Alert"
                        val msg1 = dialog?.findViewById(R.id.message) as TextView
                        msg1.text = "Do you want to exit?"
                        var dialogCancel = dialog?.findViewById(R.id.cancel) as TextView
                        dialogCancel.setOnClickListener(View.OnClickListener {
                            dialog.dismiss()
                        })
                        var dialogButton = dialog.findViewById(R.id.ok) as TextView
                        dialogButton.setOnClickListener(View.OnClickListener {
                            this.finishAffinity()
                            dialog.dismiss()
                        })
                        dialog.show()
                    }
                    else {
                        startActivity(Intent(this, MainActivity::class.java))
                    }
                }*/
            } else if (supportFragmentManager.backStackEntryCount == 2) {
                startActivity(Intent(this, MainActivity::class.java))
            }
        }
//      super.onBackPressed()
        /**/

    }


    override fun onResume() {
        super.onResume()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onDestroy() {
        super.onDestroy()
        progressHuD?.dismiss()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        progressHuD?.dismiss()
    }


    fun String.toDate(
        dateFormat: String = "dd/MM/yyyy hh:mm a",
        timeZone: TimeZone = TimeZone.getDefault()
    ): Date {
        val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
        parser.timeZone = timeZone
        return parser.parse(this)
    }


    /**
     * notification list
     */

    fun resetNotificationLists(context: MainActivity) {
        notificationListModel.clear()
        notificationListApiCall()
    }

    private fun markAllReadAction() {
        Nmark_all_read.setOnClickListener {
            val dialog = this?.let { it1 -> Dialog(it1) }
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
            dialog?.setContentView(R.layout.custom_yes_or_no_alert)
            dialog?.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
            val body = dialog?.findViewById(R.id.headtitle) as TextView
            body.text = "Alert"
            val msg1 = dialog?.findViewById(R.id.message) as TextView
            msg1.text = "Do you want to mark all notifications as read"
            val dialogCancel = dialog?.findViewById(R.id.cancel) as TextView
            dialogCancel.setOnClickListener {
                dialog.dismiss()
            }
            val dialogButton = dialog.findViewById(R.id.ok) as TextView
            dialogButton.setOnClickListener {
                markAllReadApiCall()
                dialog?.dismiss()
            }
            dialog?.show()
        }
    }


    private fun markAllReadApiCall() {
        var params = HashMap<String, Any>()
        params["type"] = "read"
        this?.let {
            apiRequest.postRequestBodyWithHeadersAny(
                it,
                webService.All_notification_url,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200") {
                        this?.toast(msg)
                        notificationListModel.clear()
                        notificationListApiCall()
                    } else {
                        this?.toast(msg)
                    }
                } catch (e: java.lang.Exception) {

                }

            }
        }
    }


    private fun notificationListApiCall() {
        set_timezone = this.getSharedPref("set_timezone", this).toString()
        val cal = Calendar.getInstance()
        var tz: TimeZone = cal.timeZone


        val params = HashMap<String, Any>()
        params["nextpage"] = ""
        params["pagesize"] = "15"

        if (set_timezone != "null") {
            params["timeZone"] = set_timezone
        } else {
            params["timeZone"] = tz.id
        }
        this?.let {
            apiRequest.postRequestBodyWithHeadersAny(
                it,
                webService.Get_notification_list_url,
                params
            ) { response ->
                try {

                    notificationListModel.clear()
                    notificationListModel = ArrayList()
                    var status = response.getString("status")
                    if (status == "200") {
                        var dataArray = response.getJSONArray("data")
                        if (dataArray.length() < 1) {
//                            showNoDataFound()
                            noNotificationsLayout.visibility = View.VISIBLE
                        } else {
                            noNotificationsLayout.visibility = View.GONE
                        }
                        for (i in 0 until dataArray.length()) {
                            var dataObj = dataArray.getJSONObject(i)
                            var _id = dataObj.getString("_id")
                            var read = dataObj.getString("read")
                            var refId = dataObj.getString("refId")
                            var type = dataObj.getString("type")
                            var createdAt = dataObj.getString("createdAt")
                            var showPopup = dataObj.getString("showPopup")
                            var title = dataObj.getString("title")
                            var description = dataObj.getString("description")

                            var paginationObj = response.getJSONObject("pagination")

                            var totalItems = paginationObj.getString("totalItems")
                            NcurrentPage = paginationObj.getString("currentPage")
                            NlastPage = paginationObj.getString("endPage")

                            var count = response.getString("count")

                            notificationListModel.add(
                                NotificationListModel(
                                    _id,
                                    read,
                                    refId,
                                    type,
                                    createdAt,
                                    showPopup,
                                    title,
                                    description
                                )
                            )
                        }
                        setupNotificationListRecyclerView(notificationListModel)
                    }
                } catch (e: java.lang.Exception) {

                }
            }
        }

    }

    //    setup adapter
    private fun setupNotificationListRecyclerView(notificationListModel: ArrayList<NotificationListModel>) {
        val adapter: RecyclerView.Adapter<*> = NotificationsListAdapter(this, notificationListModel)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        notifcations_recyclerView.layoutManager = llm
        notifcations_recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
        notifcations_recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
//
//                if (adapterVideoList.itemCount != 0) {
//                    val lastVisibleItemPosition =
//                        llm.findLastCompletelyVisibleItemPosition()
//                    if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == videoListingRecyclerView?.adapter?.itemCount!! - 1) {
//                        progressBAR.visibility = View.VISIBLE
//                        if ( pageLoad != lastPage.toInt()+1) {
//                            VideoListPageLoadApiTask().execute()
//                            pageLoad++
//                        }else {
//                            progressBAR.visibility = View.GONE
//                        }
//                    }
//                }

                val visibleItemCount = llm.childCount
                val totalItemCount = llm.itemCount
                val firstVisibleItem = llm.findFirstVisibleItemPosition()


                if (isLoading) {
                    if (totalItemCount > previousTotal) {
                        previousTotal = totalItemCount
//                        var i = currentPage.toInt()
                        NpageLoad++
                        isLoading = false


                    }
                }
                if (!isLoading && (firstVisibleItem + visibleItemCount) >= totalItemCount && NpageLoad != NlastPage.toInt()) {
                    isLoading = true

                    notificationListPageloadApiCall()
                }
            }


        })

    }


    private fun notificationListPageloadApiCall() {
        val params = HashMap<String, Any>()
        params["nextpage"] = NpageLoad
        params["pagesize"] = "10"
        this?.let {
            apiRequest.postRequestBodyWithHeadersAny(
                it,
                webService.Get_notification_list_url,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    if (status == "200") {
                        var dataArray = response.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {
                            var dataObj = dataArray.getJSONObject(i)
                            var _id = dataObj.getString("_id")
                            var read = dataObj.getString("read")
                            var refId = dataObj.getString("refId")
                            var type = dataObj.getString("type")
                            var createdAt = dataObj.getString("createdAt")
                            var showPopup = dataObj.getString("showPopup")
                            var title = dataObj.getString("title")
                            var description = dataObj.getString("description")

//                            var paginationObj = dataObj.getJSONObject("pagination")
//
//                            var totalItems = paginationObj.getString("totalItems")
//                            currentPage = paginationObj.getString("currentPage")
//                            lastPage = paginationObj.getString("endPage")
//
//                            var count = dataObj.getString("count")

                            notificationListModel.add(
                                NotificationListModel(
                                    _id,
                                    read,
                                    refId,
                                    type,
                                    createdAt,
                                    showPopup,
                                    title,
                                    description
                                )
                            )
                        }
                        setupNotificationListRecyclerView(notificationListModel)
                    }
                } catch (e: java.lang.Exception) {

                }
            }
        }

    }

    private fun clearAllNotificationOnClick() {
        NclearAll_notifications.setOnClickListener {
            val dialog = this?.let { it1 -> Dialog(it1) }
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
            dialog?.setContentView(R.layout.custom_yes_or_no_alert)
            dialog?.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
            val body = dialog?.findViewById(R.id.headtitle) as TextView
            body.text = "Alert"
            val msg1 = dialog?.findViewById(R.id.message) as TextView
            msg1.text = "Do you want to clear all notifications"
            val dialogCancel = dialog?.findViewById(R.id.cancel) as TextView
            dialogCancel.setOnClickListener {
                dialog.dismiss()
            }
            val dialogButton = dialog.findViewById(R.id.ok) as TextView
            dialogButton.setOnClickListener {
                clearAllNotificationsApiCall()
                dialog?.dismiss()
            }
            dialog?.show()
        }
    }

    private fun clearAllNotificationsApiCall() {
        var params = HashMap<String, Any>()
        params["type"] = "clear"
        this?.let {
            apiRequest.postRequestBodyWithHeadersAny(
                it,
                webService.All_notification_url,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200") {
                        this?.toast(msg)
                        notificationListModel.clear()
                        notificationListApiCall()
                    } else {
                        this?.toast(msg)
                    }
                } catch (e: java.lang.Exception) {


                }

            }
        }
    }


    /**
     * notification and email settings
     */

    private fun notificationSetActions() {
        uns_back_arrow.setOnClickListener {
            supportFragmentManager.popBackStack()
        }

        uns_done_tv.setOnClickListener {
            notificationSettingsApiCall()
            supportFragmentManager.popBackStack()
        }

    }

    private fun notificationSettingsApiCall() {
        var params = HashMap<String, Any>()
        this.let {
            apiRequest.getRequestBodyWithHeaders(
                it,
                webService.user_notification_settings,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200") {
                        notificationSettingsTasksModel.clear()
                        notificationSettingsEventsModel.clear()
                        notificationSettingsAllNotificationModel.clear()
                        error_layout.visibility = View.GONE
                        event_head.visibility = View.VISIBLE
                        task_head.visibility = View.VISIBLE
                        var dataObj = response.getJSONObject("data")

                        var taskArray = dataObj.getJSONArray("task")
                        for (t in 0 until taskArray.length()) {

                            var taskObj = taskArray.getJSONObject(t)
                            var tName = taskObj.getString("name")
                            var tNotification = taskObj.getString("notification")
                            var tEmail = taskObj.getString("email")
                            notificationSettingsTasksModel.add(
                                NotificationSettingsTasksModel(
                                    tName,
                                    tNotification.toBoolean(),
                                    tEmail.toBoolean()
                                )
                            )
                        }
                        setupTaskSettingsRecyclerView(notificationSettingsTasksModel)

                        var eventArray = dataObj.getJSONArray("event")
                        for (e in 0 until eventArray.length()) {
                            var eventObj = eventArray.getJSONObject(e)
                            var eName = eventObj.getString("name")
                            var eNotification = eventObj.getString("notification")
                            var eEmail = eventObj.getString("email")
                            notificationSettingsEventsModel.add(
                                NotificationSettingsEventsModel(
                                    eName,
                                    eNotification.toBoolean(),
                                    eEmail.toBoolean()
                                )
                            )
                        }
                        setupEventSettingsRecyclerView(notificationSettingsEventsModel)
                        var allNotificationArray = dataObj.getJSONArray("allNotification")
                        for (n in 0 until allNotificationArray.length()) {
                            var allNotifyObj = allNotificationArray.getJSONObject(n)
                            var anName = allNotifyObj.getString("name")
                            var anNotification = allNotifyObj.getString("notification")
                            var anEmail = allNotifyObj.getString("email")
                            notificationSettingsAllNotificationModel.add(
                                NotificationSettingsAllNotificationModel(
                                    anName,
                                    anNotification.toBoolean(),
                                    anEmail.toBoolean()
                                )
                            )
                        }
                        setupAllNotifySettingsRecyclerView(notificationSettingsAllNotificationModel)
                    } else if (status == "500") {
//                        this?.toast(msg)

                        error_layout.visibility = View.VISIBLE
                        event_head.visibility = View.GONE
                        task_head.visibility = View.GONE
                    } else if (status == "400") {
//                        this?.toast(msg)

                        error_layout.visibility = View.VISIBLE
                        event_head.visibility = View.GONE
                        task_head.visibility = View.GONE
                    } else {
                        error_layout.visibility = View.VISIBLE
                        event_head.visibility = View.GONE
                        task_head.visibility = View.GONE
                    }
                } catch (e: Exception) {
//                    this?.toast(e.toString())

                    error_layout.visibility = View.VISIBLE
                    event_head.visibility = View.GONE
                    task_head.visibility = View.GONE
                }
            }
        }
    }

    //    setup adapter
    private fun setupTaskSettingsRecyclerView(notificationSettingsTasksModel: ArrayList<NotificationSettingsTasksModel>) {
        val adapter: RecyclerView.Adapter<*> =
            NotificationSettingsTaskAdapter(this, notificationSettingsTasksModel)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        tasksSetting_recyclerView.layoutManager = llm
        tasksSetting_recyclerView.adapter = adapter
    }

    private fun setupEventSettingsRecyclerView(notificationSettingsEventsModel: ArrayList<NotificationSettingsEventsModel>) {
        val adapter: RecyclerView.Adapter<*> =
            NotificationSettingsEventAdapter(this, notificationSettingsEventsModel)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        eventSetting_recyclerView.layoutManager = llm
        eventSetting_recyclerView.adapter = adapter
    }

    private fun setupAllNotifySettingsRecyclerView(notificationSettingsAllNotificationModel: ArrayList<NotificationSettingsAllNotificationModel>) {
        val adapter: RecyclerView.Adapter<*> = NotificationSettingsAllNotificationsAdapter(
            this,
            notificationSettingsAllNotificationModel
        )
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        allNotifySetting_recyclerView.layoutManager = llm
        allNotifySetting_recyclerView.adapter = adapter
    }


    /**
     * get lat lon
     */

    @SuppressLint("MissingPermission")
    fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        lati = location.latitude.toString()

                        long = location.longitude.toString()
                        setSharedPref(this@MainActivity, "current_lat", lati)
                        setSharedPref(this@MainActivity, "current_lon", long)

                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }


    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            PERMISSION_ID
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                enableMyLocation()
            }
        }
        if (requestCode == REQUEST_CODE_LOCATION && grantResults.isNotEmpty()
            && grantResults[0] != PackageManager.PERMISSION_GRANTED
        ) {
            throw RuntimeException("Location services are required in order to " + "connect to a reader.")
        }
    }

    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            lati = mLastLocation.latitude.toString()

            long = mLastLocation.longitude.toString()


            setSharedPref(this@MainActivity, "current_lat", lati)
            setSharedPref(this@MainActivity, "current_lon", long)
            try {
                val params = HashMap<String, Any>()
                params["latitude"] = lati.toDouble()
                params["longitude"] = long.toDouble()
                params["radius"] = getMapsVisibleRadius()
                mapDataApiCall(params)
            } catch (e: Exception) {

            }
        }
    }


    /**
     * Maps
     */
    private fun territoryDropDown() {

        terriFilter_name.setOnClickListener {

            try {
                val arrayAdapter = ArrayAdapter<String>(
                    this,
                    R.layout.support_simple_spinner_dropdown_item,
                    territoryList
                )
                terriFilter_name.dropDownWidth = ViewGroup.LayoutParams.WRAP_CONTENT
//                arrayAdapter.setDropDownViewResource( R.layout.dropdown_textview)
                terriFilter_name.setThreshold(Integer.MAX_VALUE);
                terriFilter_name?.setAdapter(arrayAdapter)
                terriFilter_name.showDropDown()
                terriFilter_name?.threshold = 1
                terriFilter_name?.setOnClickListener {
                    terriFilter_name?.showDropDown()


                }

//            terriFilter_name?.setDropDownBackgroundDrawable(ContextCompat.getDrawable(context!!,R.drawable.corner_shape_white_bg))
                terriFilter_name?.onFocusChangeListener = View.OnFocusChangeListener { view, b ->
//                view.setBackgroundColor(ContextCompat.getColor(context!!,R.color.white))
                    if (b) {
                        // Display the suggestion dropdown on focus
                        terriFilter_name?.showDropDown()
                    }
                }
                terriFilter_name.onItemClickListener =
                    AdapterView.OnItemClickListener { parent, view, position, id ->
                        val selectedItem = parent?.getItemAtPosition(position).toString()
                        terriFilter_name?.setText(" Territory : $selectedItem ", false)
                        selectedFilterName = selectedItem
                        terriFilter_name.clearFocus()
                        hideSoftKeyboard(this)




                        try {
                            if (selectedItem == "All") {

                                lati_map = getSharedPref("current_lat", this).toString()
                                long_map = getSharedPref("current_lon", this).toString()
                                map.clear()
                                type0?.remove()
                                type1?.remove()
                                type2?.remove()
                                val params = HashMap<String, Any>()
                                params["latitude"] = lati.toFloat()
                                params["longitude"] = long.toFloat()
                                params["radius"] = getMapsVisibleRadius()
                                if (map_sup_type != "") {
                                    isFiltered = "yes"
                                    params["typeFilter"] = map_sup_type.toInt()
                                }

                                mapDataApiCall(params)
                            } else {

                                map.clear()
                                type0?.remove()
                                type1?.remove()
                                type2?.remove()
                                val selectedItemId = parent?.getItemIdAtPosition(position)
                                terriId = territoryIdList.get(selectedItemId!!.toInt() - 1)
                                var params = HashMap<String, Any>()
                                params["filter"] = true
                                params["territoryId"] = terriId

                                isFiltered = "yes"
                                if (map_sup_type != "") {
                                    params["typeFilter"] = map_sup_type.toInt()
                                }
                                mapDataApiCall(params)

                            }
                        } catch (e: java.lang.Exception) {

                        }


                    }

            } catch (e: java.lang.Exception) {

            }
        }
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    fun hideSoftKeyboard(mActivity: Activity) {
        // Check if no view has focus:
        val view = mActivity.currentFocus
        if (view != null) {
            val inputManager =
                mActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun territoryApiCall() {
        var params = HashMap<String, Any>()
        this?.let {
            apiRequest.postRequestBodyWithHeadersAny(
                it,
                webService.Map_territory_url,
                params
            ) { response ->
                try {

                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200") {
                        var dataArray = response.getJSONArray("data")

                        for (d in 0 until dataArray.length()) {
                            var dataObj = dataArray.getJSONObject(d)
                            var _id = dataObj.getString("_id")
                            territoryIdList.add(_id)
                            var name = dataObj.getString("name")
                            territoryList.add(name)
                        }
                    }
                } catch (e: Exception) {

                }
            }
        }
    }


    private fun isPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun enableMyLocation() {
        if (isPermissionGranted()) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            map.isMyLocationEnabled = true
        } else {
            this.let {
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION_PERMISSION
                )
            }
        }
    }


    private fun mapDataApiCall(params: HashMap<String, Any>) {
        mainProgressBar.visibility = View.VISIBLE
        this.let {
            apiRequest.postRequestBodyWithHeadersAny(
                it,
                webService.Map_data_url,
                params
            ) { response ->
                try {
                    val eventIcon = resources.getDrawable(R.drawable.ic_marker_app_icon_blue, null)
                    val taskRedIcon = resources.getDrawable(R.drawable.ic_marker_h_red, null)
                    val taskGreenIcon = resources.getDrawable(R.drawable.ic_marker_h_green, null)
                    val taskGreyIcon = resources.getDrawable(R.drawable.ic_marker_h_grey, null)
                    var employeeIcon = resources.getDrawable(R.drawable.profile_normal_zoom, null)

                    var status = response.getString("status")
                    if (status == "200") {
                        map.clear()
                        var dataArray = response.getJSONArray("data")

                        if (dataArray.length() == 0) {
                            map.clear()
                            type0?.remove()
                            type1?.remove()
                            type2?.remove()
                        }
                        if (dataArray.length() >= 1) {

                            for (m in 0 until dataArray.length()) {
                                var dataObj = dataArray.getJSONObject(m)
                                var title = ""
                                if (dataObj.has("title")) {
                                    title = dataObj.getString("title")
                                }

                                var description = ""
                                if (dataObj.has("description")) {
                                    description = dataObj.getString("description")
                                }
                                var address = ""
                                if (dataObj.has("address")) {
                                    address = dataObj.getString("address")
                                }

                                var id = dataObj.getString("id")
                                var name = dataObj.getString("name")

                                var locationsObj = dataObj.getJSONObject("locations")
                                var latitude = locationsObj.getString("lat")
                                var longitude = locationsObj.getString("lng")

                                type_marker = dataObj.getString("type")

                                var isEmergency = String()
                                if (dataObj.has("isEmergency")) {
                                    isEmergency = dataObj.getString("isEmergency")
                                }
                                var taskStatus = String()
                                if (dataObj.has("taskStatus")) {
                                    taskStatus = dataObj.getString("taskStatus")
                                }

                                var employeeId = String()
                                if (dataObj.has("employeeId")) {
                                    employeeId = dataObj.getString("employeeId")
                                }
                                var empName = String()
                                if (dataObj.has("name")) {
                                    empName = dataObj.getString("name")
                                }
                                var empImgWithBase = String()
                                var profilepath = String()
                                if (dataObj.has("profilepath")) {
                                    profilepath = dataObj.getString("profilepath")
                                    empImgWithBase = webService.imageBasePath + profilepath
                                }


                                if (type_marker == "1" && latitude != "null") {
                                    if (taskStatus.equals(
                                            "inprogress",
                                            ignoreCase = true
                                        ) || taskStatus.equals(
                                            "in progress",
                                            ignoreCase = true
                                        ) || taskStatus.equals("completed", ignoreCase = true)
                                    ) {
                                        type1 = map.addMarker(
                                            MarkerOptions()
                                                .position(
                                                    LatLng(
                                                        latitude.toDouble(),
                                                        longitude.toDouble()
                                                    )
                                                )
                                                .snippet(id)
                                                .icon(
                                                    BitmapDescriptorFactory.fromBitmap(
                                                        drawableToBitmap(taskGreenIcon)
                                                    )
                                                )
                                        )
                                        type1?.tag = 1
                                    } else if (taskStatus.equals(
                                            "overdue",
                                            ignoreCase = true
                                        ) || taskStatus.equals("uncovered", ignoreCase = true)
                                        && latitude != "null"
                                    ) {
                                        type1 = map.addMarker(
                                            MarkerOptions()
                                                .position(
                                                    LatLng(
                                                        latitude.toDouble(),
                                                        longitude.toDouble()
                                                    )
                                                )
                                                .snippet(id)
                                                .icon(
                                                    BitmapDescriptorFactory.fromBitmap(
                                                        drawableToBitmap(taskRedIcon)
                                                    )
                                                )
                                        )
                                        type1?.tag = 1
                                    } else {
                                        if (latitude != "null") {
                                            type1 = map.addMarker(
                                                MarkerOptions()
                                                    .position(
                                                        LatLng(
                                                            latitude.toDouble(),
                                                            longitude.toDouble()
                                                        )
                                                    )
                                                    .snippet(id)
                                                    .icon(
                                                        BitmapDescriptorFactory.fromBitmap(
                                                            drawableToBitmap(taskGreyIcon)
                                                        )
                                                    )
                                            )
                                            type1?.tag = 1
                                        }

                                    }


                                    type1?.id


                                }

                                if (type_marker == "2" && latitude != "null") {
                                    type2 = map.addMarker(
                                        MarkerOptions().position(
                                            LatLng(
                                                latitude.toDouble(),
                                                longitude.toDouble()
                                            )
                                        )
                                            .snippet(id)
                                            .icon(
                                                BitmapDescriptorFactory.fromBitmap(
                                                    drawableToBitmap(eventIcon)
                                                )
                                            )
                                    )
                                    type2?.tag = 2

                                    type2?.id

                                }
                                if (type_marker == "0" && latitude != "null") {
                                    Picasso.get().load(empImgWithBase)
                                        .placeholder(R.drawable.profile_normal_zoom)
                                        .into(empImageMap)
                                    val drawable = empImageMap.drawable as BitmapDrawable
//                            empIcon = drawable.bitmap

                                    var empIcon =
                                        Bitmap.createScaledBitmap(drawable.bitmap, 60, 60, true)

                                    type0 = map.addMarker(
                                        MarkerOptions()
                                            .position(
                                                LatLng(
                                                    latitude.toDouble(),
                                                    longitude.toDouble()
                                                )
                                            )
                                            .snippet(employeeId)
                                            .icon(
                                                BitmapDescriptorFactory.fromBitmap(
                                                    getCroppedBitmap(empIcon)
                                                )
                                            )
                                    )
                                    type0?.tag = 0

                                    type0?.id

                                }
                                if (m == 0) {
                                    if (isFiltered == "yes" && latitude != "null") {
                                        val lati_map: Double = latitude.toDouble()
                                        val long_map: Double = longitude.toDouble()
                                        try {
                                            map.moveCamera(
                                                CameraUpdateFactory.newLatLngZoom(
                                                    LatLng(
                                                        lati_map,
                                                        long_map
                                                    ), 10f
                                                )
                                            )
                                            isFiltered = ""
                                        } catch (e: java.lang.Exception) {

                                        }
                                        isFiltered = ""
                                    }
                                }


                            }
                        }


                        mainProgressBar.visibility = View.GONE
                    }
                } catch (e: Exception) {

                    mainProgressBar.visibility = View.GONE
                }
            }
        }
    }


    private fun getMapsVisibleRadius(): Double {
        val visibleRegion = map.projection.visibleRegion
        val diagonalDistance = FloatArray(1)
        val farLeft = visibleRegion.farLeft
        val nearRight = visibleRegion.nearRight
        Location.distanceBetween(
            farLeft.latitude,
            farLeft.longitude,
            nearRight.latitude,
            nearRight.longitude,
            diagonalDistance
        )
        return (diagonalDistance[0] / 2).toDouble()
    }


    private fun getCroppedBitmap(bitmap: Bitmap): Bitmap? {
        val output = Bitmap.createBitmap(
            bitmap.width,
            bitmap.height, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(output)
        val color = -0xbdbdbe
        val paint = Paint()
        val rect = Rect(0, 0, bitmap.width, bitmap.height)
        paint.isAntiAlias = true
        canvas.drawARGB(0, 0, 0, 0)
        paint.color = color
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(
            (bitmap.width / 2).toFloat(), (bitmap.height / 2).toFloat(),
            (bitmap.width / 2).toFloat(), paint
        )
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(bitmap, rect, rect, paint)
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        MapsInitializer.initialize(this)
        map.isBuildingsEnabled = true
        map.isIndoorEnabled = true
        map.isTrafficEnabled = true
        val mUiSettings = map.uiSettings
        mUiSettings.isZoomControlsEnabled = false
        mUiSettings.isCompassEnabled = true
        mUiSettings.isMyLocationButtonEnabled = true
        mUiSettings.isScrollGesturesEnabled = true
        mUiSettings.isZoomGesturesEnabled = true
        mUiSettings.isTiltGesturesEnabled = true
        mUiSettings.isRotateGesturesEnabled = true
        mUiSettings.isMapToolbarEnabled = false
//        map.setPadding(0,1600,0,0)
        enableMyLocation()


        try {
            var lati_map1: Double = getSharedPref("current_lat", this).toDouble()
            var long_map1: Double = getSharedPref("current_lon", this).toDouble()

            map.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        lati_map1,
                        long_map1
                    ), 10f
                )
            )
            isFiltered = ""
            getMapsVisibleRadius()
            val params = HashMap<String, Any>()
            params["latitude"] = lati_map1.toDouble()
            params["longitude"] = long_map1.toDouble()
            if (getMapsVisibleRadius() == 0.0) {
                params["radius"] = 1000.0000
            } else {
                params["radius"] = getMapsVisibleRadius()
            }

            var getRedius = getMapsVisibleRadius()
            mapDataApiCall(params)
        } catch (e: java.lang.Exception) {

        }

        map.setOnCameraChangeListener(object : GoogleMap.OnCameraChangeListener {
            override fun onCameraChange(position: CameraPosition?) {
                lati_map = getSharedPref("current_lat", this@MainActivity).toString()
                long_map = getSharedPref("current_lon", this@MainActivity).toString()


                if (selectedFilterName == "All") {
                    lati_map = getSharedPref("current_lat", this@MainActivity).toString()
                    long_map = getSharedPref("current_lon", this@MainActivity).toString()
                    val params = HashMap<String, Any>()
                    params["latitude"] = lati_map.toFloat()
                    params["longitude"] = long_map.toFloat()
                    params["radius"] = getMapsVisibleRadius()
                    if (map_sup_type != "") {
                        params["typeFilter"] = map_sup_type
                    }
// params["typeFilter"] = map_sup_type
                    mapDataApiCall(params)
                } else {
                    var params = HashMap<String, Any>()
                    params["filter"] = true
                    params["territoryId"] = terriId
                    if (map_sup_type != "") {
                        params["typeFilter"] = map_sup_type
                    }
                    mapDataApiCall(params)


                }

                /* val bounds: LatLngBounds = map.getProjection().getVisibleRegion().latLngBounds
                 val northeast = bounds.northeast
                 val southwest = bounds.southwest
                 val text: CharSequence = "ne:$northeast sw:$southwest"
                 val duration = Toast.LENGTH_SHORT
                 val toast = Toast.makeText(context, text, duration)


                 toast.show()*/
            }
        })




        try {
            val mapFragment =
                supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
            val mapView: View? = mapFragment?.getView()
            val locationButton = (mapView?.findViewById<View>("1".toInt())
                ?.getParent() as View).findViewById<View>("2".toInt())
            val rlp =
                locationButton.layoutParams as RelativeLayout.LayoutParams
// position on right bottom
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
            rlp.setMargins(0, 0, 30, 30)
        } catch (e: java.lang.Exception) {

        }

        map.setOnMarkerClickListener(this)
        map.setOnMyLocationButtonClickListener(this)
        map.setOnMyLocationClickListener(this)


    }


    /** Called when the user clicks a marker.  */
    override fun onMarkerClick(marker: Marker): Boolean {

        if (marker.tag == 0) {
            try {
                empDetailsPopUp(marker.snippet.toString())
            } catch (e: Exception) {

            }
        } else if (marker.tag == 1) {
            try {
                taskListPopUp(marker.snippet.toString())

            } catch (e: Exception) {

            }

        } else if (marker.tag == 2) {
            try {
                eventDetailsPopUp(marker.snippet.toString())
            } catch (e: Exception) {

            }

        }




        return false
    }


    override fun onMyLocationButtonClick(): Boolean {
//        Toast.makeText(context, "MyLocation button clicked", Toast.LENGTH_SHORT).show()
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).

        return false
    }

    override fun onMyLocationClick(location: Location) {
//        Toast.makeText(context, "Current location:\n$location", Toast.LENGTH_LONG).show()
    }


    private fun drawableToBitmap(drawable: Drawable): Bitmap {
        if (drawable is BitmapDrawable) {
            return (drawable as BitmapDrawable).getBitmap()
        }
        val bitmap = Bitmap.createBitmap(
            drawable.getIntrinsicWidth(),
            drawable.getIntrinsicHeight(),
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight())
        drawable.draw(canvas)
        return bitmap
    }


    private fun taskListApiCall(t_id: String) {

        val params = HashMap<String, Any>()
        params["id"] = t_id

        this.let {
            apiRequest.postRequestBodyWithHeadersAny(
                it,
                webService.Map_account_details_url,
                params
            ) { response ->
                try {

                    var status = response.getString("status")
                    var msg = response.getString("msg")

                    if (status == "200") {
                        mapTaskListModel.clear()
                        mapTaskListModel = ArrayList()
                        var dataArray = response.getJSONArray("data")

                        if (dataArray.length() == 0) {
                            map_taskNoDataFound_layout?.visibility = View.VISIBLE
                            mapGetDirectionLayout?.visibility = View.GONE
                            mapTaskList_recyclerView?.visibility = View.GONE
                            firstItemLayout?.visibility = View.GONE
                        } else {
                            map_taskNoDataFound_layout?.visibility = View.GONE
                            mapGetDirectionLayout?.visibility = View.VISIBLE
                            mapTaskList_recyclerView?.visibility = View.VISIBLE
                            firstItemLayout?.visibility = View.VISIBLE
                        }

                        for (a in 0 until dataArray.length()) {
                            var dataObj = dataArray.getJSONObject(a)
                            var _id = dataObj.getString("_id")
                            var taskStatus = dataObj.getString("taskStatus")
                            var startDate = dataObj.getString("startDate")
                            var endDate = dataObj.getString("endDate")
                            var t_status = dataObj.getString("status")
                            var taskName = dataObj.getString("taskName")
                            var geometryObj = dataObj.getJSONObject("geometry")
                            var lat = geometryObj.getString("lat")
                            var lng = geometryObj.getString("lng")

                            var assigned_toObj = dataObj.getJSONObject("assigned_to")
                            var assigneToId = String()
                            var assignToName = String()
                            if (assigned_toObj.has("_id")) {
                                assigneToId = assigned_toObj.getString("_id")
                            }
                            if (assigned_toObj.has("name")) {
                                assignToName = assigned_toObj.getString("name")
                            }

                            var accountName = dataObj.getString("accountName")
                            var isEmergency = dataObj.getString("isEmergency")
                            var type = dataObj.getString("type")


                            if (a == 0) {
                                mapGetDirectionTV?.setOnClickListener {
                                    try {
                                        var uri: String =
                                            "http://maps.google.com/maps?daddr=${lat.toDouble()},${lng.toDouble()}"
                                        var intent: Intent =
                                            Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                                        intent.setPackage("com.google.android.apps.maps")
                                        this.startActivity(intent)
                                    } catch (e: Exception) {
                                        print(e)
                                        this.toast(e.toString())
                                    }
                                }
                                mapGetDirectionLayout?.setOnClickListener {
                                    try {
                                        var uri: String =
                                            "http://maps.google.com/maps?daddr=${lat.toDouble()},${lng.toDouble()}"
                                        var intent: Intent =
                                            Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                                        intent.setPackage("com.google.android.apps.maps")
                                        this.startActivity(intent)
                                    } catch (e: Exception) {
                                        print(e)
                                        this.toast(e.toString())
                                    }
                                }
                                map_task_account_name_popUp?.text = this.capFirstLetter(accountName)
                                map_task_name_popUp?.text = capFirstLetter(taskName)
                                map_task_status_tv_popUp?.text = capFirstLetter(taskStatus)
                                map_task_assignTo_name_popUp?.text = capFirstLetter(assignToName)
                                try {
                                    otherTasksTV?.visibility = View.GONE
                                    var sdf: SimpleDateFormat =
                                        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                                    var myFormat = SimpleDateFormat("hh:mm a")
                                    val time_parse: Date = sdf.parse(startDate)
                                    val timeParseLocal = dateFromUTC(this, time_parse)
                                    val startTime: String = myFormat.format(timeParseLocal)
                                    map_task_time_popUp?.text =
                                        startTime.replace("am", "AM").replace("pm", "PM")
                                } catch (e: java.lang.Exception) {
                                    print(e)
                                }
                            } else {
                                otherTasksTV?.visibility = View.VISIBLE
                                mapTaskListModel.add(
                                    MapTaskListModel(
                                        _id,
                                        taskStatus,
                                        startDate,
                                        endDate,
                                        t_status,
                                        taskName,
                                        lat,
                                        lng,
                                        accountName,
                                        isEmergency,
                                        type,
                                        assignToName
                                    )
                                )

                            }
                        }
                        setupMapTaskListRecyclerView(mapTaskListModel)
                    }

                } catch (e: Exception) {

                }
            }
        }
    }

    fun closeMapsPopUp(context: MainActivity) {
        mBottomSheetDialog?.dismiss()
    }


    var map_task_account_name_popUp: TextView? = null
    var map_task_status_tv_popUp: TextView? = null
    var map_task_name_popUp: TextView? = null
    var map_task_time_popUp: TextView? = null
    var map_task_assignTo_name_popUp: TextView? = null
    var mapGetDirectionLayout: ConstraintLayout? = null
    var mapGetDirectionTV: TextView? = null
    var otherTasksTV: TextView? = null
    var map_taskNoDataFound_layout: ConstraintLayout? = null
    var firstItemLayout: ConstraintLayout? = null
    var mapTaskPopUp_close: ImageView? = null

    var mBottomSheetDialog: Dialog? = null
    private fun taskListPopUp(taskId: String) {
        mBottomSheetDialog =
            this.let { it1 -> Dialog(it1, com.salesc2.R.style.MaterialDialogSheet) };
        mBottomSheetDialog?.setContentView(com.salesc2.R.layout.popup_map_tasklist)

        map_task_account_name_popUp =
            mBottomSheetDialog?.findViewById(com.salesc2.R.id.map_task_account_name_popUp)
        map_task_status_tv_popUp =
            mBottomSheetDialog?.findViewById(com.salesc2.R.id.map_task_status_tv_popUp)
        map_task_name_popUp = mBottomSheetDialog?.findViewById(com.salesc2.R.id.map_task_name_popUp)
        map_task_time_popUp = mBottomSheetDialog?.findViewById(com.salesc2.R.id.map_task_time_popUp)
        map_task_assignTo_name_popUp =
            mBottomSheetDialog?.findViewById(com.salesc2.R.id.map_task_assignTo_name_popUp)
        mapGetDirectionLayout =
            mBottomSheetDialog?.findViewById(com.salesc2.R.id.mapGetDirectionLayout)
        mapGetDirectionTV = mBottomSheetDialog?.findViewById(com.salesc2.R.id.mapGetDirectionTV)
        otherTasksTV = mBottomSheetDialog?.findViewById(R.id.otherTasksTV)
        map_taskNoDataFound_layout =
            mBottomSheetDialog?.findViewById(R.id.map_taskNoDataFound_layout)
        firstItemLayout = mBottomSheetDialog?.findViewById(R.id.firstItemLayout)
        mapTaskList_recyclerView =
            mBottomSheetDialog?.findViewById<RecyclerView>(com.salesc2.R.id.mapTaskList_recyclerView)
        mapTaskPopUp_close = mBottomSheetDialog?.findViewById(R.id.mapTaskPopUp_close)
        taskListApiCall(taskId)


        mapTaskPopUp_close?.setOnClickListener {
            mBottomSheetDialog?.dismiss()
        }
        mBottomSheetDialog?.setCancelable(true)
        mBottomSheetDialog?.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        );
        mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
        mBottomSheetDialog?.show()


    }

    private fun setupMapTaskListRecyclerView(mapTaskListModel: ArrayList<MapTaskListModel>) {
        val adapter: RecyclerView.Adapter<*> = MapTaskListAdapter(this, mapTaskListModel)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        mapTaskList_recyclerView?.layoutManager = llm
        mapTaskList_recyclerView?.adapter = adapter
    }


    var map_event_name_popUp: TextView? = null
    var map_event_dateTime_popup: TextView? = null
    var descTV: TextView? = null
    var map_event_desc_popup: TextView? = null
    var attendeeTV: TextView? = null
    var map_event_attendee_recyclerView: RecyclerView? = null
    var mapEventGetDirectionLayout: ConstraintLayout? = null
    var mapEventGetDirectionTV: TextView? = null
    var mapEventPopUp_close: ImageView? = null

    private fun eventDetailsPopUp(eventId: String) {
        val mBottomSheetDialog: Dialog? =
            this.let { it1 -> Dialog(it1, com.salesc2.R.style.MaterialDialogSheet) };
        mBottomSheetDialog?.setContentView(com.salesc2.R.layout.popup_map_event_details)

        map_event_name_popUp =
            mBottomSheetDialog?.findViewById(com.salesc2.R.id.map_event_name_popUp)
        map_event_dateTime_popup =
            mBottomSheetDialog?.findViewById(com.salesc2.R.id.map_event_dateTime_popup)
        descTV = mBottomSheetDialog?.findViewById(com.salesc2.R.id.descTV)
        map_event_desc_popup =
            mBottomSheetDialog?.findViewById(com.salesc2.R.id.map_event_desc_popup)
        attendeeTV = mBottomSheetDialog?.findViewById(com.salesc2.R.id.attendeeTV)
        mapEventGetDirectionLayout =
            mBottomSheetDialog?.findViewById(com.salesc2.R.id.mapEventGetDirectionLayout)
        mapEventGetDirectionTV =
            mBottomSheetDialog?.findViewById(com.salesc2.R.id.mapEventGetDirectionTV)
        mapEventPopUp_close = mBottomSheetDialog?.findViewById(R.id.mapEventPopUp_close)
        map_event_attendee_recyclerView =
            mBottomSheetDialog?.findViewById<RecyclerView>(com.salesc2.R.id.map_event_attendee_recyclerView)
        mapEventDetailsApiCall(eventId)
        mapEventPopUp_close?.setOnClickListener {
            mBottomSheetDialog?.dismiss()
        }
        mBottomSheetDialog?.setCancelable(true)
        mBottomSheetDialog?.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
        mBottomSheetDialog?.show()
    }


    private fun mapEventDetailsApiCall(eventID: String) {
        var params = HashMap<String, Any>()
        params["id"] = eventID
        this.let {
            apiRequest.postRequestBodyWithHeadersAny(
                it,
                webService.Map_event_details_url,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")

                    if (status == "200") {
                        eventAttendeesModel.clear()
                        var dataObj = response.getJSONObject("data")
                        var _id = dataObj.getString("_id")
                        var isEmergency = dataObj.getString("isEmergency")
                        var title = dataObj.getString("title")
                        map_event_name_popUp?.text = title
                        if (dataObj.has("description")) {
                            map_event_desc_popup?.visibility = View.VISIBLE
                            descTV?.visibility = View.VISIBLE
                            var description = dataObj.getString("description")
                            map_event_desc_popup?.text = description
                        } else {
                            map_event_desc_popup?.visibility = View.GONE
                            descTV?.visibility = View.GONE
                        }


                        var startDate = dataObj.getString("startDate")
                        var endDate = dataObj.getString("endDate")

                        try {
                            var parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                            val myFormatDate = "MMM dd, yyyy"
                            val sdf = SimpleDateFormat(myFormatDate)
//                        sdf.timeZone = TimeZone.getTimeZone(tz.displayName)
//                        val stDate = sdf.format(parser.parse(startDate))
                            val stDate = parser.parse(startDate)
                            var stDtToLocal = dateFromUTC(this, stDate)
                            var stDateF = sdf.format(stDtToLocal)
                            val myFormatTime = "hh:mm a"
                            val sdfTime = SimpleDateFormat(myFormatTime)
//                        sdfTime.timeZone = TimeZone.getTimeZone(tz.displayName)
                            val startTime = sdfTime.format(stDtToLocal)
                            val endTime = parser.parse(endDate)
                            var edTimeToLocal = dateFromUTC(this, endTime)
                            var endTimeF = sdfTime.format(edTimeToLocal)
                            map_event_dateTime_popup?.text =
                                "$stDateF at $startTime to $endTimeF".replace("am", "AM")
                                    .replace("pm", "PM")
                        } catch (e: java.lang.Exception) {

                        }

                        var attendee_id = String()
                        var attendeeName = String()
                        var attendeeImage = String()
                        var attendeesArray = dataObj.getJSONArray("attendees")

                        if (attendeesArray.length() > 1) {
                            for (a in 0 until attendeesArray.length()) {
                                var attendeeObj = attendeesArray.getJSONObject(a)
                                if (attendeeObj.has("_id")) {
                                    attendee_id = attendeeObj.getString("_id")
                                }

                                if (attendeeObj.has("name")) {
                                    attendeeName = attendeeObj.getString("name")
                                }

                                if (attendeeObj.has("profilePath")) {
                                    attendeeImage = attendeeObj.getString("profilePath")
                                }

                                var imgWithBase = webService.imageBasePath + attendeeImage
                                eventAttendeesModel.add(
                                    EventAttendeesModel(
                                        attendeeName,
                                        attendee_id,
                                        imgWithBase, ""
                                    )
                                )
                            }
                            setupMapEventAttendeeRecyclerView(eventAttendeesModel)
                        } else {
                            attendeeTV?.text = "Created By"
                            if (dataObj.has("createdBy")) {
                                var createdByObj = dataObj.getJSONObject("createdBy")
                                var createdBy_id = createdByObj.getString("_id")
                                var createdByName = createdByObj.getString("name")
                                var createdByImage = createdByObj.getString("profilePath")
                                var imgWithBase = webService.imageBasePath + createdByImage
                                eventAttendeesModel.add(
                                    EventAttendeesModel(
                                        createdByName,
                                        createdBy_id,
                                        imgWithBase,
                                        ""
                                    )
                                )
                            }
                            setupMapEventAttendeeRecyclerView(eventAttendeesModel)
                        }
//                        setupMapEventAttendeeRecyclerView(eventAttendeesModel)

                        var address = dataObj.getString("address")


                        var type = dataObj.getString("type")
                        var eventType = dataObj.getString("eventType")
                        var eventtypeName = dataObj.getString("eventtypeName")

                        var locationsObj = dataObj.getJSONObject("locations")
                        var lat = locationsObj.getString("lat")
                        var lng = locationsObj.getString("lng")

                        mapEventGetDirectionLayout?.setOnClickListener {
                            try {
                                var uri: String =
                                    "http://maps.google.com/maps?daddr=${lat.toDouble()},${lng.toDouble()}"
                                var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                                intent.setPackage("com.google.android.apps.maps")
                                this.startActivity(intent)
                            } catch (e: Exception) {
                                print(e)
                                this.toast(e.toString())
                            }
                        }
                        mapEventGetDirectionTV?.setOnClickListener {
                            try {
                                var uri: String =
                                    "http://maps.google.com/maps?daddr=${lat.toDouble()},${lng.toDouble()}"
                                var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                                intent.setPackage("com.google.android.apps.maps")
                                this.startActivity(intent)
                            } catch (e: Exception) {
                                print(e)
                                this.toast(e.toString())
                            }
                        }
                    } else {
                        this.toast(msg)
                    }
                } catch (e: Exception) {

                }
            }
        }
    }

    //    setup adapter
    private fun setupMapEventAttendeeRecyclerView(eventAttendeesModel: ArrayList<EventAttendeesModel>) {
        val adapter: RecyclerView.Adapter<*> =
            EventDetailsAttendeesAdapter(this, eventAttendeesModel)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.HORIZONTAL
        map_event_attendee_recyclerView?.layoutManager = llm
        map_event_attendee_recyclerView?.adapter = adapter
    }


    private fun mapEmployeeDetailsApiCall(empID: String) {
        var params = HashMap<String, Any>()
        params["id"] = empID

        this.let {
            apiRequest.postRequestBodyWithHeadersAny(
                it,
                webService.Map_employee_details_url,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")

                    if (status == "200") {
                        empTopLayout?.visibility = View.VISIBLE
                        var dataObj = response.getJSONObject("data")

                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")
                        map_emp_name?.text = capFirstLetter(name)
                        var type = dataObj.getString("type")
                        var role = dataObj.getString("role")
                        if (role.equals("Sales Rep", ignoreCase = true)) {
                            map_emp_job?.text = this?.capFirstLetter("Representative")
                        } else if (role.equals("Team Lead", ignoreCase = true)) {
                            map_emp_job?.text = this?.capFirstLetter("Regional Manager")
                        } else {
                            map_emp_job?.text = this?.capFirstLetter(role)
                        }
                        map_emp_job?.text = capFirstLetter(role)
                        var levelName = dataObj.getString("levelName")
                        map_emp_territory?.text = levelName
                        var level = dataObj.getString("level")
                        var email = dataObj.getString("email")
                        map_emp_email?.text = email
                        var phone = dataObj.getString("phone")
                        map_emp_phone?.text = phone
                        var profilePath = dataObj.getString("profilePath")
                        var imgWithBase = webService.imageBasePath + profilePath
                        try {
                            Picasso.get().load(imgWithBase)
                                .placeholder(R.drawable.ic_dummy_user_pic).into(map_emp_image)
                        } catch (e: Exception) {

                        }

                        var taskArray = dataObj.getJSONArray("task")
                        if (taskArray.length() >= 1) {
                            otherTaskScheduled?.visibility = View.VISIBLE
                        }
                        for (t in 0 until taskArray.length()) {
                            var taskObj = taskArray.getJSONObject(t)
                            var t_id = taskObj.getString("_id")
                            var taskStatus = taskObj.getString("taskStatus")
                            var taskName = taskObj.getString("taskName")
                            var account_address = taskObj.getString("account_address")

                            var geometryObj = taskObj.getJSONObject("geometry")
                            var lat = geometryObj.getString("lat")
                            var lng = geometryObj.getString("lng")

                            var department = taskObj.getString("department")
                            var isEmergency = taskObj.getString("isEmergency")
                            var startDate = taskObj.getString("startDate")
                            var endDate = taskObj.getString("endDate")
                            mapTaskListModel.add(
                                MapTaskListModel(
                                    t_id,
                                    taskStatus,
                                    startDate,
                                    endDate,
                                    "",
                                    taskName,
                                    lat,
                                    lng,
                                    department,
                                    isEmergency,
                                    "",
                                    name
                                )
                            )
                        }
                        setupMapEmpTaskListRecyclerView(mapTaskListModel)
                    } else {
                        toast(msg)
                    }

                } catch (e: Exception) {

                }
            }
        }
    }


    var map_emp_image: RoundedImageView? = null
    var map_emp_job: TextView? = null
    var map_emp_name: TextView? = null
    var map_emp_email: TextView? = null
    var map_emp_phone: TextView? = null
    var map_emp_territory: TextView? = null
    var map_emp_all_filter: TextView? = null
    var mapEmpTaskList_recyclerView: RecyclerView? = null
    var emp_image_layout: ConstraintLayout? = null
    var emp_dash_image: ImageView? = null
    var mapEmpLayout: ConstraintLayout? = null
    var mapEmpPopUp_close: ImageView? = null
    var empTopLayout: ConstraintLayout? = null
    var otherTaskScheduled: TextView? = null
    private fun empDetailsPopUp(empID: String) {
        mBottomSheetDialog = this.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) };
        mBottomSheetDialog?.setContentView(R.layout.popup_map_employee_details)

        map_emp_image = mBottomSheetDialog?.findViewById(R.id.map_emp_image)
        map_emp_job = mBottomSheetDialog?.findViewById(R.id.map_emp_job)
        map_emp_name = mBottomSheetDialog?.findViewById(R.id.map_emp_name)
        map_emp_email = mBottomSheetDialog?.findViewById(R.id.map_emp_email)
        map_emp_phone = mBottomSheetDialog?.findViewById(R.id.map_emp_phone)
        map_emp_territory = mBottomSheetDialog?.findViewById(com.salesc2.R.id.map_emp_territory)
        map_emp_all_filter = mBottomSheetDialog?.findViewById(com.salesc2.R.id.map_emp_all_filter)
        mapEmpTaskList_recyclerView =
            mBottomSheetDialog?.findViewById(com.salesc2.R.id.mapEmpTaskList_recyclerView)
        emp_image_layout = mBottomSheetDialog?.findViewById(R.id.emp_image_layout)
        emp_dash_image = mBottomSheetDialog?.findViewById(R.id.emp_dash_image)
        mapEmpLayout = mBottomSheetDialog?.findViewById(R.id.mapEmpLayout)
        mapEmpPopUp_close = mBottomSheetDialog?.findViewById(R.id.mapEmpPopUp_close)
        empTopLayout = mBottomSheetDialog?.findViewById(R.id.empTopLayout)
        otherTaskScheduled = mBottomSheetDialog?.findViewById(R.id.otherTaskScheduled)
        mapEmpPopUp_close?.setOnClickListener {
            mBottomSheetDialog?.dismiss()
        }
        emp_image_layout?.setOnClickListener {
            mBottomSheetDialog?.dismiss()
        }
        emp_dash_image?.setOnClickListener {
            mBottomSheetDialog?.dismiss()
        }
        mapEmployeeDetailsApiCall(empID)



        map_emp_phone?.setOnClickListener {
            try {

                var callIntent: Intent =
                    Intent(Intent.ACTION_DIAL, Uri.parse("tel: ${map_emp_phone?.text}"))
                this.startActivity(callIntent)


            } catch (activityException: ActivityNotFoundException) {

            } catch (e: Exception) {

            }
        }
        map_emp_email?.setOnClickListener {


            val TO = arrayOf(map_emp_email?.text.toString())
            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.data = Uri.parse("mailto:")
            emailIntent.type = "text/plain"
            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
// emailIntent.putExtra(Intent.EXTRA_CC, CC);
            // emailIntent.putExtra(Intent.EXTRA_CC, CC);
//            emailIntent.putExtra(Intent.EXTRA_SUBJECT,)

            emailIntent.setPackage("com.google.android.gm")


            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."))
            } catch (ex: ActivityNotFoundException) {
            } catch (e: Exception) {

            }


        }
        mBottomSheetDialog?.setCancelable(true)
        mBottomSheetDialog?.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
        mBottomSheetDialog?.show()
    }


    private fun setupMapEmpTaskListRecyclerView(mapTaskListModel: ArrayList<MapTaskListModel>) {
        val adapter: RecyclerView.Adapter<*> = MapTaskListAdapter(this, mapTaskListModel)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        mapEmpTaskList_recyclerView?.layoutManager = llm
        mapEmpTaskList_recyclerView?.adapter = adapter
    }


    /**
     * payment
     */
    var paymentMethodId = String()
    var billingCardListModel = ArrayList<BillingCardListModel>()
    var cardList_recyclerView: RecyclerView? = null
    var fromPage = String()
    fun billingCardListingPopUp(context: MainActivity, billingId: String) {
        fromPage = getSharedPref("fromPage", this)
        billingCardListModel = ArrayList()
        mBottomSheetDialog =
            this?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) };
        mBottomSheetDialog?.setContentView(R.layout.popup_card_list)
        cardList_recyclerView =
            mBottomSheetDialog?.findViewById<RecyclerView>(R.id.cardList_recyclerView)
        var cardList_continue = mBottomSheetDialog?.findViewById<TextView>(R.id.cardList_continue)
        var cardListClose: ImageView? = mBottomSheetDialog?.findViewById(R.id.cardListClose)
        selectAddCard = mBottomSheetDialog?.findViewById(R.id.selectAddCard)!!
        getCardListApiCall(billingId, "yes")
        cardListClose?.setOnClickListener {
            mBottomSheetDialog?.dismiss()
        }
        selectAddCard?.setOnClickListener {
            selectAddCard?.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_blue_circle
                )
            )
            setSharedPref(this, "addCard", "yes")
            getCardListApiCall(billingId, "no")
//             (cardList_recyclerView!!.adapter as BillingCardListAdapter).hideData()
        }
        cardList_continue?.setOnClickListener {
            var addCard = getSharedPref("addCard", this).toString()
            if (addCard == "yes") {
//                 setSharedPref(this,"editCard","yes")
                loadFragment(AddCardFragment())
                mBottomSheetDialog?.dismiss()
            } else {
                if (fromPage == "DashBoard") {
                    reSubscribeApiCall()
                    mBottomSheetDialog?.dismiss()
                } else {
                    mBottomSheetDialog?.dismiss()
                }
            }
        }

        mBottomSheetDialog?.setCancelable(true)
        mBottomSheetDialog?.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        mBottomSheetDialog?.window?.setGravity(Gravity.CENTER)
        mBottomSheetDialog?.show()
    }

    private fun getCardListApiCall(billingId: String, selection: String) {
        val params = HashMap<String, Any>()
        params["billingId"] = billingId
        this?.let {
            apiRequest.postRequestBodyWithHeadersAny(
                it,
                webService.get_customer_card_url,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    if (status == "200") {
                        billingCardListModel.clear()
                        billingCardListModel = ArrayList()
                        var cardDetailsArr = response.getJSONArray("cardDetails")

                        for (i in 0 until cardDetailsArr.length()) {
                            var cardDetailsObj = cardDetailsArr.getJSONObject(i)
                            var paymentMethodId = cardDetailsObj.getString("paymentMethodId")
                            var exp_year = cardDetailsObj.getString("exp_year")
                            var exp_month = cardDetailsObj.getString("exp_month")
                            var last4 = cardDetailsObj.getString("last4")
                            var name = cardDetailsObj.getString("name")
                            var brand = cardDetailsObj.getString("brand")
                            billingCardListModel.add(
                                BillingCardListModel(
                                    paymentMethodId,
                                    exp_year,
                                    exp_month,
                                    last4,
                                    name,
                                    brand,
                                    selection
                                )
                            )
                        }
                    }
                    setupBillingCardListRecyclerView(billingCardListModel)

                } catch (e: Exception) {

                }

            }
        }

    }

    private fun setupBillingCardListRecyclerView(
        billingCardListModel: ArrayList<BillingCardListModel>


    ) {
        val adapter: RecyclerView.Adapter<*> =
            BillingCardListAdapter(this, billingCardListModel, this)
        val llm = LinearLayoutManager(this)
        llm.orientation = LinearLayoutManager.VERTICAL
        cardList_recyclerView?.layoutManager = llm
        cardList_recyclerView?.adapter = adapter
    }

    fun dismissBillingCardListPopUp(context: MainActivity) {
        mBottomSheetDialog?.dismiss()
    }

    private fun reSubscribeApiCall() {
        paymentMethodId = this.getSharedPref("paymentMethodId", this)
        var params = HashMap<String, Any>()
        params["type"] = 1
        params["paymentMethodId"] = paymentMethodId
        apiRequest.postRequestBodyWithHeadersAny(
            this,
            webService.re_subscribe_url,
            params
        ) { response ->
            try {
                var status = response.getString("status")
                var msg = response.getString("msg")
                if (status == "200") {
                    val preferences: SharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(this)
                    val editor: SharedPreferences.Editor = preferences.edit()
                    editor.remove("fromPage")
                    editor.commit()
                    this.toast(msg)
                    loadFragment(BillingOverviewFragment())
                    coordinatorlayout.visibility = View.VISIBLE
                } else {
                    this.toast(msg)
                }
            } catch (e: Exception) {

            }
        }
    }

    private fun updateCardDetailsApiCall(
        billingId: String,
        cardHolder: String,
        expire: String,
        lastDigit: String,
        paymentMethodId: String
    ) {
        val params = HashMap<String, Any>()
        params["billingId"] = billingId
        params["cardHolder"] = cardHolder
        params["expire"] = expire
        params["lastDigit"] = lastDigit
        params["paymentMethodId"] = paymentMethodId
        apiRequest.putRequestBodyWithHeadersAny(
            this,
            webService.card_details_url,
            params
        ) { response ->
            try {

            } catch (e: Exception) {

            }
        }
    }

    override fun showOption(
        contexts: Context,
        selectCard: ImageView
    ) {


/*
        if (selected){
            selected = false
            holder.selectCard.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_blue_circle))
        }else{
            selected = true
            holder.selectCard.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_grey_circle))
        }*/
        selectCard.setImageDrawable(
            ContextCompat.getDrawable(
                contexts!!,
                R.drawable.ic_blue_circle
            )
        )
        selectAddCard!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_grey_circle))

    }

    override fun showSelection() {
        selectAddCard!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_blue_circle))
    }

    override fun hideSelection() {
        selectAddCard!!.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_grey_circle))
    }

    override fun refreshCardList() {

    }


    /**
     * Phase 2 :Offline work
     * By Thungesh
     */

    //Account Api Call

    //new Account API
    private var roomDB: AppDatabase? = null


    private fun accountOfflineApiCall() {
        val params = HashMap<String, Any>()
        apiRequest.getRequestBodyWithHeaders(
            this,
            webService.account_offline_url,
            params
        ) { response ->
            CoroutineScope(Dispatchers.IO).launch {
                roomDB = AppDatabase.getDatabaseClient(this@MainActivity)
                roomDB!!.selectAccountNewDao().deleteAll()
                roomDB!!.addTaskAssigneeDao().deleteAll()

            }
            try {
                var status = response.getString("status")
                if (status == "200") {
                    var dataArray = response.getJSONArray("accounts")


                    var employeesArray = response.getJSONArray("employees")
                    for (j in 0 until employeesArray.length()) {
                        var dataObj = employeesArray.getJSONObject(j)
                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")
                        var profilePath = dataObj.getString("profilePath")
                        println("employeesArray" + " " + _id + " " + name + " " + profilePath)

                        CoroutineScope(Dispatchers.IO).launch {
                            roomDB = AppDatabase.getDatabaseClient(this@MainActivity)
                            roomDB!!.addTaskAssigneeDao().insertAll(
                                AddTaskAssignee(0, _id, name, profilePath)
                            )
                        }

                    }



                    for (i in 0 until dataArray.length()) {

                        var dataObj = dataArray.getJSONObject(i)
                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")

                        CoroutineScope(Dispatchers.IO).launch {
                            roomDB = AppDatabase.getDatabaseClient(this@MainActivity)
                            roomDB!!.selectAccountNewDao().insertAll(
                                SelectAccountNew(
                                    0,
                                    _id,
                                    name,
                                    dataObj.getJSONArray("departments").toString()
                                )
                            )
                        }


                    }
                    println("response account=" + response);
                } else if (status == "403") {

                }
            } catch (e: Exception) {

                toast("Notification count error: ${e.toString()} ")
            }
        }
    }

    private fun serviceOfflineApiCall() {
        var params = java.util.HashMap<String, String>()
        params.put("search", "")
        this.let {
            apiRequest.postRequestBodyWithHeaders(
                it,
                webService.Select_service_list_url,
                params
            ) { response ->
                CoroutineScope(Dispatchers.IO).launch {
                    roomDB = AppDatabase.getDatabaseClient(this@MainActivity)
                    roomDB!!.selectServiceDao().deleteAll()
                }
                println("select_service_filter== $response")
                var status = response.getString("status")
                if (status == "200") {

                    var dataArray = response.getJSONArray("data")
                    for (i in 0 until dataArray.length()) {
                        var dataObj = dataArray.getJSONObject(i)
                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")


                        CoroutineScope(Dispatchers.IO).launch {

                            roomDB = AppDatabase.getDatabaseClient(this@MainActivity)

                            roomDB!!.selectServiceDao().insertAll(
                                SelectService(
                                    0,
                                    _id, name
                                )
                            )
                        }
                    }
                } else {
                    this?.progressBarDismiss(this!!)
                }

            }
        }
    }

    //product list
    private fun productListOffline() {
        var param = HashMap<String, String>()
        param.put("search", "")
        this?.let {
            apiRequest.postRequestBodyWithHeaders(
                it,
                webService.Add_products_url,
                param
            ) { response ->
                try {
                    var status = response.getString("status")
                    if (status == "200") {
                        CoroutineScope(Dispatchers.IO).launch {
                            roomDB = AppDatabase.getDatabaseClient(this@MainActivity)
                            roomDB!!.productListDao().deleteAll()
                        }
                        var dataArray = response.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {
                            var dataObj = dataArray.getJSONObject(i)
                            var _id = dataObj.getString("_id")
                            var name = dataObj.getString("name")

                            CoroutineScope(Dispatchers.IO).launch {

                                roomDB = AppDatabase.getDatabaseClient(this@MainActivity)

                                roomDB!!.productListDao().insertAll(
                                    ProductList(
                                        0,
                                        _id, name
                                    )
                                )
                            }
                        }
                    } else {

                    }
                } catch (e: Exception) {

                }
            }
        }
    }

    private fun addTaskOfflineApiCall() {
        CoroutineScope(Dispatchers.Main).launch {
            db = AppDatabase.getDatabaseClient(applicationContext)
            var timelineData = db!!.addTaskTimelineDao().getAll()
            var jsonArrTimeLine = JSONArray()
            if (timelineData.size != 0) {
                if (timelineData[0].type1 != "") {
                    jsonArrTimeLine.put(timelineData[0].type1)
                }
                if (timelineData[0].type2 != "") {
                    jsonArrTimeLine.put(timelineData[0].type2)
                }
                if (timelineData[0].type3 != "") {
                    jsonArrTimeLine.put(timelineData[0].type3)
                }
                if (timelineData[0].type4 != "") {
                    jsonArrTimeLine.put(timelineData[0].type4)
                }
                if (timelineData[0].type5 != "") {
                    jsonArrTimeLine.put(timelineData[0].type5)
                }
                if (timelineData[0].type6 != "") {
                    jsonArrTimeLine.put(timelineData[0].type6)
                }
                if (timelineData[0].type7 != "") {
                    jsonArrTimeLine.put(timelineData[0].type7)
                }
                if (timelineData[0].type8 != "") {
                    jsonArrTimeLine.put(timelineData[0].type8)
                }
                if (timelineData[0].type9 != "") {
                    jsonArrTimeLine.put(timelineData[0].type9)
                }
                if (timelineData[0].type10 != "") {
                    jsonArrTimeLine.put(timelineData[0].type10)
                }
                if (timelineData[0].type11 != "") {
                    jsonArrTimeLine.put(timelineData[0].type11)
                }
            }
            var params = HashMap<String, Any>()
            for (t in 0 until timelineData.size) {
                var cID = getSharedPref("companyId", this@MainActivity)
                params["companyId"] = cID
                params["accountId"] = timelineData[t].accountId.toString()
                params["departmentId"] = timelineData[t].departmentId.toString()
                params["serviceId"] = timelineData[t].serviceId.toString()
                params["assignedBy"] = timelineData[t].assignedBy.toString()
                params["createdBy"] = timelineData[t].createdBy.toString()
                params["assignedTo"] = timelineData[t].assignedTo.toString()
                params["recurringOption"] = timelineData[t].recurringOption.toString()
                params["startDate"] = timelineData[t].startDate.toString()
                params["endDate"] = timelineData[t].endDate.toString()
                params["taskDescription"] = timelineData[t].taskDescription.toString()
                params["taskStatus"] = timelineData[t].taskStatus.toString()
                params["products"] = timelineData[t].products.toString()
                params["timezone"] = timelineData[t].timezone.toString()
                params["createdAt"] = timelineData[t].createdAt.toString()
                params["updatedAt"] = timelineData[t].updatedAt.toString()
                params["timelines"] = jsonArrTimeLine.toString()
                apiRequest.postRequestBodyWithHeadersAny(
                    this@MainActivity,
                    webService.add_task_offline_url,
                    params
                ) { response ->
                    Log.i("addTaskOfflineResponse= ", response.toString())
                    db = AppDatabase.getDatabaseClient(applicationContext)
                    db!!.addTaskTimelineDao().deleteById(timelineData[t].timeline_id)

                }

            }

        }


    }

    private fun dashboardDataOffline() {
        lifecycleScope.launch {
            delay(1000)
            whileOnline(dateSelected, "", "", "", "all")
        }
    }

    fun callUpdateStatusDb(context: MainActivity) {
        finish()
        startActivity(intent)
        setupDashTasksRecyclerView()
    }

    private fun whileOnline(date: String,
        assignTo: String,
        territory: String,
        pendingTask: String,
        type: String) {
        dash_tab_click = getSharedPref("dash_tab_click", this)
        mainProgressBar.visibility = View.GONE
        if (isNetworkAvailable(this)) {
            dash_tab_click = getSharedPref("dash_tab_click", this)
            mainProgressBar.visibility = View.GONE
            if (pageLoad == 1) {
//                dashSectionModel.clear()
//                dashTaskModel.clear()
                CoroutineScope(Dispatchers.Main).launch {
                    db = AppDatabase.getDatabaseClient(applicationContext)
                    db!!.salesDao().deleteAll()
                    db!!.dashBoardDateAddDao().deleteAll()

                }
            }
            val cal = Calendar.getInstance()
            var tz: TimeZone = cal.timeZone


            var params = HashMap<String, Any>()
            params["date"] = date
            if (assignTo == "null") {
                assignTo == ""
                params["assignedTo"] = ""
            } else {
                params["assignedTo"] = assignTo
            }

            params["territoryId"] = territory
            params["pending_tasks"] = pendingTask
            params["page"] = pageLoad
            params["pageSize"] = pageSize
            params["type"] = type
            set_timezone = this.getSharedPref("set_timezone", this).toString()
            if (set_timezone != "null") {
                params["timezone"] = set_timezone
            } else {
                params["timezone"] = tz.id
            }


            if (cmpNowDate == cmpCalDate) {
                params["pending"] = true
            } else if (dateSelected == todayDate) {
                params["pending"] = true
            }

            this.let {
                apiRequest.postRequestBodyWithHeadersAny(
                    this,
                    webService.Dashboard_url,
                    params
                ) { response ->
                    try {
                        var status = response.getString("status")
                        var msg = response.getString("msg")
                        if (status == "200") {
                            Log.i("dash_resp", response.toString())
                            var dataArray = response.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
//                            dashTaskModel.clear()
                                dashTaskModel = ArrayList()
                                var dataObj = dataArray.getJSONObject(i)

                                var date = dataObj.getString("date")
                                CoroutineScope(Dispatchers.Main).launch {
                                    db = AppDatabase.getDatabaseClient(applicationContext)

                                    db!!.dashBoardDateAddDao().insertAll(DashBoardDateAdd(0, date));
                                }


                                var dataArray1 = dataObj.getJSONArray("data")

                                for (j in 0 until dataArray1.length()) {
                                    var dataObj1 = dataArray1.getJSONObject(j)
                                    var _id = String()
                                    if (dataObj1.has("_id")) {
                                        _id = dataObj1.getString("_id")
                                    }
                                    var taskStatus = String()
                                    if (dataObj1.has("taskStatus")) {
                                        taskStatus = dataObj1.getString("taskStatus")
                                    }
                                    var title = String()
                                    if (dataObj1.has("title")) {
                                        title = dataObj1.getString("title")
                                    }

                                    var account_address = String()
                                    if (dataObj1.has("account_address")) {
                                        account_address = dataObj1.getString("account_address")
                                    }

                                    var geomettryObj = JSONObject()
                                    if (dataObj1.has("geomettry")) {
                                        geomettryObj = dataObj1.getJSONObject("geomettry")
                                    }

                                    var lat = String()
                                    var lng = String()
                                    if (geomettryObj.has("lat")) {
                                        lat = geomettryObj.getString("lat")
                                    }
                                    if (geomettryObj.has("lng")) {
                                        lng = geomettryObj.getString("lng")
                                    }

                                    var isEmergency = dataObj1.getString("isEmergency")

                                    var assigned_toObj = JSONObject()
                                    var assignee_id = String()
                                    var assignee_name = String()
                                    var imgWithBase = String()
                                    if (dataObj1.has("assigned_to")) {
                                        assigned_toObj = dataObj1.getJSONObject("assigned_to")

                                        if (assigned_toObj.has("_id")) {
                                            assignee_id = assigned_toObj.getString("_id")
                                        }
                                        if (assigned_toObj.has("name")) {
                                            assignee_name = assigned_toObj.getString("name")
                                        }
                                        var assignee_image = String()
                                        if (assigned_toObj.has("image")) {
                                            assignee_image = assigned_toObj.getString("image")
                                        }

                                        imgWithBase = webService.imageBasePath + assignee_image
                                    }

                                    var department = String()
                                    if (dataObj1.has("department")) {
                                        department = dataObj1.getString("department")
                                    }
                                    var status = String()
                                    if (dataObj1.has("status")) {
                                        status = dataObj1.getString("status")
                                    }
                                    var startDate = dataObj1.getString("startDate")
                                    d_stTime = startDate
                                    var endDate = dataObj1.getString("endDate")
                                    d_edTime = endDate

                                    //for event
                                    var eventType = String()
                                    var eventCategory = String()
                                    var address = String()
                                    var description = String()
                                    var attendeesArr = JSONArray()
                                    if (dataObj1.has("eventType")) {
                                        eventType = dataObj1.getString("eventType")
                                    }
                                    if (dataObj1.has("eventCategory")) {
                                        eventCategory = dataObj1.getString("eventCategory")
                                    }
                                    if (dataObj1.has("address")) {
                                        address = dataObj1.getString("address")
                                    }
                                    if (dataObj1.has("description")) {
                                        description = dataObj1.getString("description")
                                    }
                                    var attendee_id = String()
                                    var attendee_name = String()
                                    var attendee_imgWithBase = String()
                                    var attendee_length = String()
                                    if (dataObj1.has("attendees")) {
                                        attendeesArr = dataObj1.getJSONArray("attendees")
                                        attendee_length = attendeesArr.length().toString()
                                        for (a in 0 until attendeesArr.length()) {
                                            var attendeeObj = attendeesArr.getJSONObject(a)
                                            attendee_id = attendeeObj.getString("_id")
                                            attendee_name = attendeeObj.getString("name")
                                            var attendee_image = String()

                                            if (attendeeObj.has("image")) {
                                                attendee_image = attendeeObj.getString("image")
                                            }

                                            attendee_imgWithBase =
                                                webService.imageBasePath + attendee_image

                                        }
                                    }

                                    var organizerId = String()
                                    var organizerName = String()
                                    var organizerImage = String()
                                    if (dataObj1.has("organizerId")) {
                                        var organizerIdArray = dataObj1.getJSONArray("organizerId")
                                        for (org in 0 until organizerIdArray.length()) {
                                            var organizerObj = organizerIdArray.getJSONObject(org)
                                            organizerId = organizerObj.getString("_id")
                                            organizerName = organizerObj.getString("name")
                                            var profilePath = organizerObj.getString("profilePath")
                                            organizerImage = webService.imageBasePath + profilePath
                                        }
                                    }
                                    var type = dataObj1.getString("type")


                                    if (response.has("pager")) {
                                        var pagerObj = response.getJSONObject("pager")
                                        var totalItems = pagerObj.getString("totalItems")
                                        var currentP = pagerObj.getString("currentPage")
                                        try {
                                            pageSize = pagerObj.getString("pageSize").toInt()
                                        } catch (e: java.lang.Exception) {
                                            print(e)
                                        }


                                        var totalPages = pagerObj.getString("totalPages")
                                        currentPage = pagerObj.getString("startPage")
                                        lastPage = pagerObj.getString("endPage")
                                        var startIndex = pagerObj.getString("startIndex")
                                        var endIndex = pagerObj.getString("endIndex")
                                    }

                                    CoroutineScope(Dispatchers.Main).launch {

                                        db = AppDatabase.getDatabaseClient(applicationContext)

                                        db!!.salesDao().insertAll(
                                            DashBoard(
                                                0,
                                                date,
                                                _id,
                                                taskStatus,
                                                title,
                                                account_address,
                                                lat,
                                                lng,
                                                isEmergency,
                                                assignee_id,
                                                assignee_name,
                                                imgWithBase,
                                                department,
                                                status,
                                                startDate,
                                                endDate,
                                                type,
                                                eventType,
                                                eventCategory,
                                                address,
                                                description,
                                                attendee_name,
                                                attendee_imgWithBase,
                                                attendee_id,
                                                attendee_length,
                                                organizerId,
                                                organizerName,
                                                organizerImage

                                            )
                                        )
                                    }
                                    progressHuD?.dismiss()
                                }
                                progressHuD?.dismiss()
                            }
                            setupDashTasksOfflineRecyclerView()
                            progressHuD?.dismiss()
                        } else if (status == "403") {
                            mainProgressBar.visibility = View.GONE

                        } else {
                            mainProgressBar.visibility = View.GONE
                            progressHuD?.dismiss()

                        }

                    } catch (e: Exception) {
                        mainProgressBar.visibility = View.GONE
                        progressHuD?.dismiss()
                    }
                }
            }
        }

    }

    fun callOfflineApi(context: MainActivity) {
        ///offline api call
        pageLoad = 1
        dashboardDataOffline()
        accountOfflineApiCall()
        serviceOfflineApiCall()
        productListOffline()
        taskDetailsOfflineApiCall()
//        addTaskOfflineApiCall()
    }

    fun taskStatusUpdateCall() {
        CoroutineScope(Dispatchers.Main).launch {
            db = AppDatabase.getDatabaseClient(applicationContext)
            var tsData = db!!.taskStatusDao().getAll()
            for (s in 0 until tsData.size) {
                var status = tsData[s].t_status
                var tID = tsData[s].dash_task_id
                var params = HashMap<String, Any>()
                params["taskStatus"] = status.toString()
                params["_id"] = tID.toString()
                apiRequest.postRequestBodyWithHeadersAny(
                    this@MainActivity,
                    webService.Update_status_url,
                    params
                ) { response ->
                    try {
                        var status = response.getString("status")
                        var msg = response.getString("msg")
                        if (status == "200") {
                            db!!.taskStatusDao().deleteById(tsData[s].id)
                        }

                    } catch (e: Exception) {
                        toast(e.toString())
                    }
                }
            }
        }
    }

    private fun taskDetailsOfflineApiCall() {
        var params = HashMap<String, Any>()
        apiRequest.getRequestBodyWithHeaders(
            this,
            webService.all_task_offline_url,
            params
        ) { response ->
            Log.i("all_task_offline_resp== ", response.toString())
            try {
                var status = response.getString("status")
                if (status == "200") {
                    CoroutineScope(Dispatchers.Main).launch {
                        db = AppDatabase.getDatabaseClient(applicationContext)
                        db!!.taskDao().deleteAll()
                    }
                    var resultArray = response.getJSONArray("result")
                    for (r in 0 until resultArray.length()) {
                        var resultObj = resultArray.getJSONObject(r)
                        var _id = resultObj.getString("_id")
                        var timelineArr = resultObj.getJSONArray("timeline")
                        var startDate = resultObj.getString("startDate")
                        var endDate = resultObj.getString("endDate")
                        var productsArr = resultObj.getJSONArray("products")
                        var taskStatus = resultObj.getString("taskStatus")
                        var taskName = resultObj.getString("taskName")

                        var locationObj = resultObj.getJSONObject("location")
                        var lat = locationObj.getString("lat")
                        var lng = locationObj.getString("lng")

                        var assignedToObj = resultObj.getJSONObject("assignedTo")
                        var assignToId = assignedToObj.getString("_id")
                        var assignToName = assignedToObj.getString("name")

                        var departmentName = resultObj.getString("departmentName")

                        var department_contactsArr = resultObj.getJSONArray("department_contacts")
                        /* for (d in 0 until department_contactsArr.length()){
                             var dContactsObj = department_contactsArr.getJSONObject(d)

                         }*/

                        var expensesArr = resultObj.getJSONArray("expenses")
                        CoroutineScope(Dispatchers.Main).launch {
                            db = AppDatabase.getDatabaseClient(applicationContext)
                            db!!.taskDao().insertAll(
                                Task(
                                    0,
                                    _id,
                                    startDate,
                                    endDate,
                                    taskStatus,
                                    taskName,
                                    lat,
                                    lng,
                                    timelineArr.toString(),
                                    assignToId,
                                    assignToName,
                                    departmentName,
                                    department_contactsArr.toString(),
                                    expensesArr.toString(),
                                    productsArr.toString(),
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    ""
                                )
                            )
                        }

                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        /* CoroutineScope(Dispatchers.Main).launch {
             db = AppDatabase.getDatabaseClient(applicationContext)
             var alltaskDb = db!!.taskDao().getAll()
             Log.i("alltaskDb", alltaskDb.toString())

         }*/
    }


    fun getTime() {


        val pre_time = this.getSharedPref("pre_date", this)

        println("gfhfgjthhfhf" + pre_time)
        if (pre_time != "null") {

            val toyBornTime = getSharedPref("pre_date", this)


            val dateFormat = SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss"
            )

            val value = try {
                val oldDate = dateFormat.parse(toyBornTime)
                System.out.println(oldDate)



                val currentDate = Date()
                val diff = currentDate.time - oldDate.time
                val seconds = diff / 1000
                val minutes = seconds / 60
                val hours = minutes / 60
                val days = hours / 24
                if (minutes > 15) {
                    println("Difference" + " " + " testing" + " " + "1")

                    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                    val currentDateandTime = sdf.format(Date())
                    setSharedPref(this@MainActivity, "pre_date", currentDateandTime.toString())
                    if (isNetworkAvailable(applicationContext)) {
                        pageLoad = 1
                        dashboardDataOffline()
                        accountOfflineApiCall()
                        serviceOfflineApiCall()
                        productListOffline()
                        taskDetailsOfflineApiCall()
                    }
                } else {
                    println("Difference" + " " + " testing" + " " + "2")
                }

                Log.e(
                    "Difference: ", " minutes: " + minutes
                )


                // Log.e("toyBornTime", "" + toyBornTime);
            } catch (e: ParseException) {
                e.printStackTrace()
            }
        } else {

            var offline_auto_pref = this.getSharedPref("isChecked_offline_auto", this).toString()
            if (offline_auto_pref == "yes") {
                val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                val currentDateandTime = sdf.format(Date())
                setSharedPref(this@MainActivity, "pre_date", currentDateandTime.toString())
                if (isNetworkAvailable(applicationContext)) {
                    pageLoad = 1
                    dashboardDataOffline()
                    accountOfflineApiCall()
                    serviceOfflineApiCall()
                    productListOffline()
                    taskDetailsOfflineApiCall()
                }
            }

        }


    }

    private fun setupDashTasksOfflineRecyclerView() {
        runOnUiThread {
            val adapter: RecyclerView.Adapter<*> =
                MainRecyclerAdapter(this@MainActivity, dashSectionModel)
            val llm = LinearLayoutManager(this@MainActivity)
            llm.orientation = LinearLayoutManager.VERTICAL
            dashboard_recyclerView.layoutManager = llm
            dashboard_recyclerView.adapter = adapter
            dashboard_recyclerView.setHasFixedSize(true)
            adapter.notifyDataSetChanged()

            dashboard_recyclerView.addOnScrollListener(object :
                RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(
                    recyclerView: RecyclerView,
                    newState: Int
                ) {
                    super.onScrollStateChanged(recyclerView, newState)
                }

                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)

                    val visibleItemCount = llm.childCount
                    val totalItemCount = llm.itemCount
                    val firstVisibleItem = llm.findFirstVisibleItemPosition()


                    if (isLoading) {
                        if (totalItemCount > previousTotal) {
                            previousTotal = totalItemCount
//                        var i = currentPage.toInt()
                            pageLoad++
                            isLoading = false
                            Log.v("...", "Last Item Wow !")

                        }
                    }
                    if (!isLoading && (firstVisibleItem + visibleItemCount) >= totalItemCount && pageLoad != lastPage.toInt() + 1) {
                        isLoading = true
                        if (dash_tab_click == "taskOnClick") {
                            dashBoardApiCall(dateSelected, "", "", "", "all")
                        }
                    }
                }


            })

        }

    }

}


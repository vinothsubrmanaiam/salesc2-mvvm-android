package com.salesc2.model

import com.google.gson.JsonArray
import com.google.gson.JsonObject
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable
import java.sql.Struct

data class LoginActivityModel(var email: String, var password: String)


data class AccountlistModel(
    var id: String,
    var accountName: String,
    var territory: String,
    var lat: String,
    var long: String
) : Serializable



data class EventAttendeesModel(var attendee_name:String,
                           var attendee_id:String,
                           var attendee_img:String,var type: String)

/*data class AccountTimeLinelistModel( var e_id: String,
                                var e_name : String,
                                var e_role:String,
                                var e_level_name: String,
                                var e_level_type: String,
                                var e_profilePath : String, var e_phone: String, var e_email: String): Serializable*/



data class TeamMemberListModel(
    var e_id: String,
    var e_name: String,
    var e_role: String,
    var e_level_name: String,
    var e_level_type: String,
    var e_profilePath: String, var e_phone: String, var e_email: String
) : Serializable

data class UpcomingTasksModel(
    var taskDate: String,
    var taskName: String
) : Serializable

data class AddTaskAssigneeModel(var assignee_id: String,
                                var assignee_image: String,
                                var assignee_name: String,
                                var isSelected:Boolean = false):Serializable {}


data class SelectAccountModel(var acc_id: String,
                              var acc_name: String)

data class SelectDeptModel(var dept_id:String,
                           var dept_name:String)

data class  SelectServiceModel(var service_id: String,
                               var service_name: String)

data class TaskListModel(var task_id: String,
                         var task_name: String,
                         var task_department:String,
                         var task_status:String,
                         var task_assigneeTo:String,
                         var task_assigneeTo_id: String,
                         var task_startDate:String,
                         var task_endDate:String,var task_lat:String,var task_lng:String)

data class AddProductsModel( var prod_id:String,
                             var prod_name:String,
                            var prod_price:String,
                            var prod_qty:String)

data class TaskProductsModel( var prod_id:Array<String>,
                             var prod_name:Array<String>,
                             var prod_price:Array<String>,
                             var prod_qty:Array<String>)

data class FilterListModel(var filter_name: String)

data class FilterAssigneeModel(var assignee_id: String,
                                var assignee_name: String)

data class FilterTerritoryModel(var territory_id:String,
                                var territory_name:String)
data class FilterStatusModel(var filterStatus_name:String)
data class SelectStatusModel(var ss_name:String)

data class TDUpdateStatusModel(var us_name:String,var us_id:String,var us_assignetoId:String)

data class TaskDetailsTimelineModel(var _id:String,
                                    var type:String,
                                    var description:String,
                                    var createdAt:String,
                                    var createdBy:String,
                                    var createdById:String,
                                    var assignedTo:String,
                                    var assignedToId:String)

data class TaskDetailsProductsModel(var prod_id:String,
                                    var prod_name:String,
                                    var productId : String,
                            var prod_price:String,
                            var prod_qty:String)

data class TaskDetailsExpensesModel(var expense_id: String,
                                    var employeeName:String,
                                    var description:String,
                                    var amount:String,
                                    var time:String,
                                    var employeeId:String)


data class ChangeTaskAssigneeModel(var assignee_id: String,
                                var assignee_image: String,
                                var assignee_name: String,
                                   var assignee_taskId:String)


data class SelectEventTypeModel(var et_id:String,
                                var et_type:String,
                                var et_name:String):Serializable


data class AddAttendeesModel(var at_id:String,var at_name:String,var at_img:String)


data class NotificationListModel(var n_id:String,
                                 var n_read:String,
                                 var n_refId:String,
                                 var n_type:String,
                                 var n_createdAt:String,
                                 var n_showPopup:String,
                                 var n_title:String,
                                 var n_description:String)

/*
data class AccountDepartmentDetailModel(
    var dept_id: String,
    var departmentName: String,
    var contactname: String,
    var phone_id: String,
    var phoneNo: String,
    var phoneType: String,
    var email_id: String,
    var emailid: String,
    var emailType: String
) : Serializable
*/
data class AccountDepartmentPhoneModel(
    var phone_id: String,
    var phoneNo: String,
    var phoneType: String
) : Serializable

data class AccountDepartmentEmailModel(
    var emailid: String,
    var email_id: String,
    var emailType: String
) : Serializable

data class AccountDepartmentContactModel(
    var contactname: String,
    var accountDepartmentPhoneModel: ArrayList<AccountDepartmentPhoneModel>,
    var accountDepartmentEmailModel: ArrayList<AccountDepartmentEmailModel>
) : Serializable

data class AccountDepartmentDetailModel(
    var dept_id: String,
    var departmentName: String,
    var accountDepartmentContactModel: ArrayList<AccountDepartmentContactModel>
) : Serializable


data class AccountTimeLinelistModel(
    var _id: String,
    var timeline_id: String,
    var timestamp: String,
    var taskname: String,
    var status: String,
    var assignee_id: String,
    var assignee_name: String

) : Serializable




data class DashTasksModel(
                          var date:String,var _id:String,
                          var taskStatus:String,
                          var title:String,
                          var account_address:String,
                          var lat:String,
                          var lng:String,var isEmergency:String,
                          var assignTo_id:String,var assignTo_name:String,
                          var assignTo_img:String,var department:String,
                          var status:String,var startDate:String,
                          var endDate:String,var type:String,
                          var event_eventType:String,var event_eventCategory:String,
                          var event_address:String, var event_description:String,
                      var attendee_name:String,
                          var attendee_img:String,var attendee_id:String ,var attendee_length:String,
                          var organizerId :String, var organizerName: String,
                          var organizerImage:String
):Serializable



data class DashSectionModel(var date: String,var items:ArrayList<DashTasksModel>)

data class TaskSummaryModel(var title : String, var count:String)

data class NotificationSettingsTasksModel(var name:String,
                                          var notification:Boolean,
                                          var email:Boolean)

data class NotificationSettingsEventsModel(var name:String,
                                           var notification:Boolean,
                                           var email:Boolean)

data class NotificationSettingsAllNotificationModel(var name:String,
                                                    var notification:Boolean,
                                                    var email:Boolean)

data class MapTaskListModel(var _id : String, var taskStatus:String,
                            var startDate:String, var endDate: String,
                            var status:String, var taskName:String,var lat:String,
                            var lng: String,var accountName:String,var isEmergency:String,
                            var type:String,var assignTo_name:String):Serializable

data class BillingHistoryModel(var _id : String,var amount:String,
                               var startDate:String,var createdAt:String,
                               var invoiceNumber:String,var status :String,
                               var subscription:String,var billingCycle:String)

data class BillingCardListModel(var paymentMethodId:String,var exp_year: String,
                                var exp_month : String,
                                var last4:String,var name: String,
                                var brand: String,var selection:String)

data class BillingEmailsModel(var emails:String)
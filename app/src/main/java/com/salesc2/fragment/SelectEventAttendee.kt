package com.salesc2.fragment

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.adapters.SelectEventAttendeeAdapter
import com.salesc2.model.AddTaskAssigneeModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.DatabaseHandler
import com.salesc2.service.WebService
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.loadFragment
import com.salesc2.utils.progressBarDismiss
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar

class SelectEventAttendee:Fragment() {
    var webService = WebService()
    var apiRequest = ApiRequest()
    lateinit var assigneeSearch: SearchView
    lateinit var assigneeListRecyclerView: RecyclerView
    lateinit var addTaskAssigneeModel: ArrayList<AddTaskAssigneeModel>
    lateinit var goback: ImageView
    private lateinit var close: ImageView
    private lateinit var attendee_add: Button

    private var searchtext = String()

    private var eType_type = String()




    lateinit var mView: View
    private lateinit var taskHead : TextView
   private var userId = String()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_add_task_assignee, container, false)
        eType_type = context?.getSharedPref("eType_type",context!!).toString()
        userId = context?.getSharedPref("userId",context!!).toString()
        assigneeSearch = mView.findViewById(R.id.task_assignee_SearchView)
        assigneeListRecyclerView = mView.findViewById(R.id.task_assignee_recyclerView)
        goback = mView.findViewById(R.id.task_assignee_back_arrow)
        taskHead = mView.findViewById(R.id.taskHead)
        taskHead.text = "Add Attendees"
        addTaskAssigneeModel = ArrayList()

        close = mView.findViewById(R.id.close_iv)
        attendee_add = mView.findViewById(R.id.attendee_add)
        if(eType_type == "0"){
          attendee_add.visibility = View.GONE
        }else if (eType_type == "1"){
            attendee_add.visibility = View.VISIBLE
        }else{
            attendee_add.visibility = View.GONE
        }
        goBackAction()
        searchAssignee()
        closeAction()
        AddTaskAssigneeAsyncTask().execute()
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private fun closeAction(){
        close.setOnClickListener{
            val databaseHandler : DatabaseHandler = DatabaseHandler(context!!)
            databaseHandler.deleteAttendees()
            context?.loadFragment(context!!,AddEventFragment())
        }
        attendee_add.setOnClickListener{
            context?.loadFragment(context!!,AddEventFragment())
        }
    }
    private fun goBackAction(){
        goback.setOnClickListener(View.OnClickListener {
            fragmentManager?.popBackStack()
        })

    }
    private fun searchAssignee(){
        assigneeSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchtext = newText
                AddTaskAssigneeAsyncTask().execute()
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchtext = query
                AddTaskAssigneeAsyncTask().execute()

                return true
            }

            override fun equals(other: Any?): Boolean {
                return super.equals(other)

            }



        })
    }
    inner class AddTaskAssigneeAsyncTask(): AsyncTask<String, String, String>(){
        override fun onPreExecute() {
            super.onPreExecute()
            addTaskAssigneeModel = ArrayList()
//            context?.progressBarShow(context!!)
        }
        override fun doInBackground(vararg params: String?): String {
            var param = HashMap<String, String>()
//            param.put("accountId", "")
            param.put("StartDate", "")
            param.put("toDate", "")
            param.put("search", searchtext)
            context?.let {
                apiRequest.postRequestBodyWithHeaders(
                    it,
                    webService.Add_task_assignee_url,
                    param
                ) { response ->
                    try {
                        var status = response.getString("status")
                        if (status == "200") {
                            addTaskAssigneeModel.clear()
                            var dataArray = response.getJSONArray("data")
                            for (i in 0 until dataArray.length()) {
                                var dataObj = dataArray.getJSONObject(i)
                                var _id = dataObj.getString("_id")
                                var name = dataObj.getString("name")
                                var profilePath = String()
                                if (dataObj.has("profilePath")) {
                                    profilePath = dataObj.getString("profilePath")
                                } else {
                                    profilePath = ""
                                }
                                var imgWithBase = webService.imageBasePath + profilePath

                                if (userId != _id){
                                    addTaskAssigneeModel.add(AddTaskAssigneeModel(_id, imgWithBase, name))
                                }


                                context?.progressBarDismiss(context!!)
                            }
                                setupEventAttendeeRecyclerView(addTaskAssigneeModel)
                        } else {
                            context?.progressBarDismiss(context!!)
                        }

                    } catch (e: Exception) {

                    }
                }


            }
            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            context?.progressBarDismiss(context!!)

        }

    }

    //    setup adapter
    private fun setupEventAttendeeRecyclerView(addTaskAssigneeModel: ArrayList<AddTaskAssigneeModel>) {
        val adapter: RecyclerView.Adapter<*> = SelectEventAttendeeAdapter(context, addTaskAssigneeModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        assigneeListRecyclerView.layoutManager = llm
        assigneeListRecyclerView.adapter = adapter
        assigneeListRecyclerView.setHasFixedSize(true)
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

}
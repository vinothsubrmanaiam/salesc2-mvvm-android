package com.salesc2.data.network.api.request

import com.google.gson.annotations.SerializedName

data class RequestDashBoard(
    @SerializedName("date") val date: String? = "",
    @SerializedName("timezone") val timezone: String? = "",
    @SerializedName("pending") val pending: String? = "",
    @SerializedName("territoryId") val territoryId: String? = "",
    @SerializedName("pending_tasks") val pending_tasks: String? = "",
    @SerializedName("pageSize") val pageSize: String? = "",
    @SerializedName("page") val page: String? = "",
    @SerializedName("type") val type: String? = "",
    @SerializedName("assignedTo") val assignedTo: String? = ""
)

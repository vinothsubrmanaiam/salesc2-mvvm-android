package com.salesc2.data.network.api.request

import com.google.gson.annotations.SerializedName

data class RequestPasswordAndSecurity(@SerializedName("oldPassword") val oldPassword:String?= "",
                                      @SerializedName("newPassword") val newPassword:String?= "",
                                      @SerializedName("confirmPassword") val confirmPassword:String?= ""
)
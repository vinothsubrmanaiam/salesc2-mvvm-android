package com.salesc2.service

class WebService {


    val Token =
        "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmMDQyNDU5NTVkZDdmMjQ4YzlkMzg5MSIsIm5hbWUiOiJSYWphIiwiY29tcGFueUlkIjoiNWVlOWNjZjBlMDNlMGEzZDBmMzA0YzQ1Iiwicm9sZSI6InNhbGVzIHJlcCIsImVtYWlsIjoicmFqYS5zQGZvcnR1bmVzb2Z0aXQuY29tIiwiaWF0IjoxNTk0NjI5MDk1LCJleHAiOjE2MjYxNjUwOTV9.mEQXDu19gp1Vl9ch7z99_HvdRihxoIawyLaJpOdkqdQ"

    /* static let UserService = "http://3.219.91.255:9001/api/v1/"
      static let TeamService = "http://3.219.91.255:9002/api/v1/"
      static let AccountService = "http://3.219.91.255:9003/api/v1/"
      static let TaskService = "http://3.219.91.255:9004/api/v1/"
      static let EventService = "http://3.219.91.255:9005/api/v1/"*/

   val imageBasePath = "https://salesc2userprofile.s3.amazonaws.com/"

/*<<<<<<< HEAD
    val Base_Url = "http://3.219.91.255:9001/api/v1/"
    val Base_Url1 = "http://3.219.91.255:9002/api/v1/"
    val Base_Url2 = "http://3.219.91.255:9003/api/v1/"

    val employee_login_url = Base_Url + "employee"
    val employee_logout_url = Base_Url + "employee/logout"
    val reset_password_url = Base_Url + "employee/resetpassword"
    val Account_list_url = Base_Url2 + "account/1/10"
    val Account_detail_url = Base_Url2 + "accountDetail"
    val Account_departmentdetail_url = Base_Url2 + "departmentDetail"
    val Account_timelinedetail_url = Base_Url2 + "timelineDetail"

    //   val Account_list_url = Base_Url2 + "account/1/10"
    //  val Account_detail_url = Base_Url2 + "accountDetail"
    val team_members_list_url = Base_Url1 + "userlist"
    val team_members_details_url = Base_Url1 + "user"
=======*/

// Dev URLs
   /*
    val typeservice = "Dev"
    val UserService = "http://3.219.91.255:9001/api/v1/"
    val TeamService = "http://3.219.91.255:9002/api/v1/"
    val AccountService = "http://3.219.91.255:9003/api/v1/"
    val TaskService = "http://3.219.91.255:9004/api/v1/"
    val EventService = "http://3.219.91.255:9005/api/v1/"
val BillingService = "http://3.219.91.255:8019/api/v1/"
val LocationService = "http://3.219.91.255:8015/api/v1/"

    val BillingConfigure = "https://3.219.91.255:8019/user/api/v1/"
    val CompanyService = "https://3.219.91.255:8019/company/api/v1/"
    var  clientSecretKey = "sk_test_51HaR0gA9n98XqrBDMAoTErl3t0Mi7U76jRBngidGc87m8z6O9RzJoBc0H8rePChyijWnyYalJhmtr155aXkSaVAA00SOfzyTXO"

*/


    var currentFrag : String = ""

 //Stage URLs
    val typeservice = "Stage"
    val UserService = "https://stage-api.salesc2.com/ios-login/api/v1/"
 val TeamService = "https://stage-api.salesc2.com/ios-user/api/v1/"
 val AccountService = " https://stage-api.salesc2.com/ios-account/api/v1/"
 val TaskService = "https://stage-api.salesc2.com/ios-task/api/v1/"
 val EventService =  "https://stage-api.salesc2.com/ios-event/api/v1/"
    val BillingService = "https://stage-api.salesc2.com/subscription/api/v1/"
     val CompanyService = "https://stage-api.salesc2.com/company/api/v1/"
    val BillingConfigure = "https://stage-api.salesc2.com/user/api/v1/"
    val LocationService = "https://stage-api.salesc2.com/location/api/v1/"
    var  clientSecretKey = "sk_test_51HaR0gA9n98XqrBDMAoTErl3t0Mi7U76jRBngidGc87m8z6O9RzJoBc0H8rePChyijWnyYalJhmtr155aXkSaVAA00SOfzyTXO"

// https://stage-api.salesc2.com/ios-registration



    ///Production URL
    /*  val typeservice = "Production"
  val UserService = "https://api.salesc2.com/ios-login/api/v1/"
    val TeamService = "https://api.salesc2.com/ios-user/api/v1/"
   val AccountService = "https://api.salesc2.com/ios-account/api/v1/"
   val TaskService = "https://api.salesc2.com/ios-task/api/v1/"
    val EventService = "https://api.salesc2.com/ios-event/api/v1/"
    val LocationService = "https://api.salesc2.com/location/api/v1/"
    val BillingService = "https://api.salesc2.com/subscription/api/v1/"
    val CompanyService = "https://api.salesc2.com/company/api/v1/"
    val BillingConfigure = "https://api.salesc2.com/user/api/v1/"*/
    //var clientSecretKey = "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"







    //userStatic 9001
    val employee_login_url = UserService + "employee"
    val employee_logout_url = UserService + "employee/logout"
    val reset_password_url = UserService + "employee/resetpassword"

    //TeamService 9002
    val team_members_list_url = TeamService + "userlist"
    val team_members_details_url = TeamService + "user"
    val Profile_update_url = TeamService+"profile"
    val user_notification_settings = TeamService+"employee/notificationSettings"
    val change_password_url = TeamService+"employee/changePassword"
    var Employee_timezone = TeamService+"employee/timezone"


    //AccountService 9003
    val Account_list_url = AccountService + "account"
    val Account_detail_url = AccountService + "accountDetail"
    val Account_departmentdetail_url = AccountService + "departmentDetail"
    val Account_timelinedetail_url = AccountService + "timelineDetail"

    //TaskService 9004
    val Select_account_list_url = TaskService+"accountFilter"
    val Select_dept_list_url = TaskService + "departmentFilter"
    val Select_service_list_url = TaskService + "servicefilter"
    val Add_task_url = TaskService+"addtask"
    val Add_task_assignee_url = TaskService+"employeefilter"
    val Add_products_url = TaskService+"productfilter"
    val Update_status_url = TaskService+"updateStatus"
    val Territory_filter_url  = TaskService+"territory_filter"
    val Task_list_url = TaskService+"get_task"
    val Task_details_url = TaskService+"task_details"
    val Task_details_timeline_url = TaskService+"task_timeline_details"
    val Task_product_details_url = TaskService+"task_product_details"
    val Task_product_add_edit_url = TaskService+"update_product"
    val Task_expenses_details_url = TaskService+"expenses_details"
    val Task_expenses_add_edit_url = TaskService+"expenses_edit"
    val Task_details_edit_task_detail_url = TaskService+"edit_task_details"
    val Task_details_add_note_url = TaskService+"add_note"
    val Edit_task_url = TaskService+"edit_task"

    val remome_product_api = TaskService+"task/removeProduct"


    val Dashboard_url = TaskService+"dashboard"
 val Dashboard_count_url = TaskService+"dashboard/counts"

 val Get_notification_list_url = TaskService+"notifications/get_notifications"
 val Single_notification_url = TaskService+"notifications/edit_single_notifications"
 val All_notification_url = TaskService+"notifications/edit_all_notifications"
 val Task_notification_url = TaskService+"task_notification"
    val Notification_count_url = TaskService +"notifications/get_notification_count"

 val TaskSummary_url= TaskService+"taskSummary"

    var Map_data_url = TaskService+"map/mapDatas"
    val Map_territory_url = TaskService+"dashboard_level_filter"
    val Map_account_details_url = TaskService+"map/taskDetail"
    val Map_event_details_url = TaskService+"map/eventDetail"
    val Map_employee_details_url = TaskService+"map/employeeDetail"



 //Event service 9005
     val Event_details_url = EventService+"eventDetail"
     val Event_cancel_url = EventService+"cancelEvent"
     val Event_type_list_url = EventService+"eventTypeList"
     val Event_add_url = EventService+"addEvent"
    val Edit_event_url = EventService+"eventDetail"

 val Event_notification_url = EventService+"event_notification"
 val Event_accept_reject_url = EventService+"eventAcceptReject"



    val configure_billing_url = BillingService+"billing_configure"
    val create_customer_url = BillingService+ "create-customer"
    val billing_setup_store_url = BillingService+"billingSetup/store"
    val billing_overview_url = BillingService+"billing/overview"
    val stripe_url = "https://api.stripe.com/v1/payment_methods"
    val create_price_url = BillingService+"create-price"
    val billing_estimate_url = BillingService+"billing_estimate"
    val billing_history_url = BillingService+"billing/history"
    val cancel_subscription_url = BillingService+"subscription/cancel"
    val billing_details_url = BillingService+"billingDetails"
    val edit_card_url = BillingService+"edit_customer_card"
    var switch_billing_cycle_url = BillingService+"billing/switchBillingCycle"
    var update_invoice_url = BillingService+"billing/updateInvoice"
    var show_billing_url = BillingService+"show_billing"
    var billing_contact_url = BillingService+"billing/billingContact"
    var billing_status_url = BillingService+"employee/billingStatus"
    var re_subscribe_url = BillingService+"reSubscribe"
    var card_details_url = BillingService+"billing/cardDetail"
    var delete_customer_card_url = BillingService+"delete_customer_card"
    var get_customer_card_url = BillingService+"get_customer_cards"

        //dev
//    val company_subscription_details = "http://3.219.91.255:8004/api/v1/company/subscription/details"
//    val billing_employee_details_url = "http://3.219.91.255:8006/api/v1/employee/billing/configure"


    val billing_employee_details_url = BillingConfigure+"employee/billing/configure"
    val company_subscription_details = CompanyService+"company/subscription/details"
    val location_update = LocationService+"location/updateLocation"


    val country_list_url = "http://3.219.91.255:8015/api/v1/getCountry"
    val state_list_url = "http://3.219.91.255:8015/api/v1/getState"
    val city_list_url = "http://3.219.91.255:8015/api/v1/getcity"


    /**
     * offline Api
     */
    //dev
//    val account_offline_url = "http://3.219.91.255:9004/api/v1/accounts"
//    var all_task_offline_url = "http://3.219.91.255:9004/api/v1/get_all_tasks"
//    val add_task_offline_url = "http://3.219.91.255:9004/api/v1/addTaskOffline"

    val account_offline_url = TaskService+"accounts"
    var all_task_offline_url = TaskService+"get_all_tasks"
    val add_task_offline_url = TaskService+"addTaskOffline"


}
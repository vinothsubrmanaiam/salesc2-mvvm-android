package com.salesc2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.AccountDepartmentContactModel
import com.salesc2.model.AccountDepartmentEmailModel
import com.salesc2.model.AccountDepartmentPhoneModel
import com.salesc2.utils.capFirstLetter
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class AccountDepartmentCustomerListAdapter() :
    RecyclerView.Adapter<AccountDepartmentCustomerListAdapter.viewHolder>() {

    private lateinit var accountContactList: ArrayList<AccountDepartmentContactModel>
    var context: Context? = null

    constructor(context: Context?, accountContactList: ArrayList<AccountDepartmentContactModel>) : this() {
        this.context = context
        this.accountContactList = accountContactList
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AccountDepartmentCustomerListAdapter.viewHolder {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.account_detail_dept_customename, parent, false)
        return AccountDepartmentCustomerListAdapter.viewHolder(view)
    }



    override fun getItemCount(): Int {
        return accountContactList.size
    }

    override fun onBindViewHolder(
        holder: AccountDepartmentCustomerListAdapter.viewHolder,
        position: Int
    ) {
        //holder.itemView.tag = position
        val accountDepartmentContactModel: AccountDepartmentContactModel =
            accountContactList.get(position)
        holder.contactname_tv?.text = context?.capFirstLetter(accountDepartmentContactModel.contactname)

        for (i in 0 until accountContactList.size) {
            /* var accountDepartmentPhoneModel = accountDepartmentContactModel.accountDepartmentPhoneModel[i]*/
            var phoneSize = accountDepartmentContactModel.accountDepartmentPhoneModel.size
            val accountDepartmentPhoneList = ArrayList<AccountDepartmentPhoneModel>()
            for (j in 0 until phoneSize) {
                var accountDepartmentPhoneModel =
                    accountDepartmentContactModel.accountDepartmentPhoneModel[j]

                var phone_id = accountDepartmentPhoneModel.phone_id
                var phoneNo = accountDepartmentPhoneModel.phoneNo
                var phoneType = accountDepartmentPhoneModel.phoneType

                accountDepartmentPhoneList.add(
                    AccountDepartmentPhoneModel(
                        phone_id,
                        phoneNo,
                        phoneType
                    )
                )

                //setup adapter
                val adapter: AccountDepartmentCustomerContactListAdapter =
                    AccountDepartmentCustomerContactListAdapter(
                        context,
                        accountDepartmentPhoneList
                    )
                val llm = LinearLayoutManager(context)
                llm.orientation = LinearLayoutManager.VERTICAL
                holder.phoneno_rv?.layoutManager = llm
                holder.phoneno_rv?.adapter = adapter
            }

            var emailSize = accountDepartmentContactModel.accountDepartmentEmailModel.size
            for (j in 0 until emailSize) {
                val accountDepartmentEmailList = ArrayList<AccountDepartmentEmailModel>()

                var accountDepartmentEmailModel =
                    accountDepartmentContactModel.accountDepartmentEmailModel[j]

                var email_id = accountDepartmentEmailModel.email_id
                var emailid = accountDepartmentEmailModel.emailid
                var emailType = accountDepartmentEmailModel.emailType
                accountDepartmentEmailList.add(
                    AccountDepartmentEmailModel(
                        emailid,
                        email_id,
                        emailType
                    )
                )

                //setup adapter
                val adapter: AccountDepartmentCustomerEmailListAdapter =
                    AccountDepartmentCustomerEmailListAdapter(
                        context,
                        accountDepartmentEmailList
                    )
                val llm = LinearLayoutManager(context)
                llm.orientation = LinearLayoutManager.VERTICAL
                holder.email_rv?.layoutManager = llm
                holder.email_rv?.adapter = adapter
            }

        }

    }

    class viewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var contactname_tv: TextView? = itemView.findViewById(R.id.person_name_tv)
        var phoneno_rv: RecyclerView? = itemView.findViewById(R.id.phoneno_rv)
        var email_rv: RecyclerView? = itemView.findViewById(R.id.email_rv)

    }
    private fun capitalize(capString: String): String? {
        val capBuffer = StringBuffer()
        val capMatcher: Matcher =
            Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString)
        while (capMatcher.find()) {
            capMatcher.appendReplacement(
                capBuffer,
                capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase()
            )
        }
        return capMatcher.appendTail(capBuffer).toString()
    }

}

//
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import androidx.recyclerview.widget.RecyclerView
//import com.salesc2.R
//import com.salesc2.model.AccountDepartmentContactModel
//import com.salesc2.model.AccountDepartmentDetailModel
//import kotlinx.android.synthetic.main.account_detail_dept_customename.view.*
//
//
//class AccountDepartmentCustomerListAdapter(
//    val accountDepartmentDetailModel: AccountDepartmentDetailModel
//
//) : RecyclerView.Adapter<ViewHolder>() {
//
//    override fun getItemCount(): Int {
//        return accountDepartmentDetailModel.accountDepartmentContactModel.size
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.person_name_tv?.text = accountDepartmentDetailModel.accountDepartmentContactModel.get(position).contactname
//    }
//
//    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
//        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.account_detail_dept_customename, parent, false))
//    }
//}
//
//class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
//    val person_name_tv = view.person_name_tv
//}


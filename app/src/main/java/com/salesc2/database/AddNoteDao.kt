package com.salesc2.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface AddNoteDao {
    @Query("SELECT * FROM ProductList")
    suspend fun getAll(): List<AddNote>

    @Insert
    suspend fun insertAll(vararg addNote: AddNote)

    @Query("DELETE FROM AddNote ")
    fun deleteAll()
}
package com.salesc2.service

import android.app.Dialog
import android.content.Context
import android.view.Window
import android.widget.TextView
import com.salesc2.R

class AlertDialogView {
    fun showDialog(context: Context, title: String, msg: String) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.alert_dialog_view)
        dialog.window?.setBackgroundDrawableResource(R.drawable.alert_dialog_view_bg)
        val body = dialog.findViewById(R.id.dialogtitle) as TextView
        body.text = title
        val msg1 = dialog.findViewById(R.id.text_dialog) as TextView
        msg1.text = msg
        val dialogButton = dialog.findViewById(R.id.cancel_btn_dialog) as TextView
        dialogButton.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }
}
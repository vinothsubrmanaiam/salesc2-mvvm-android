package com.salesc2.data.network.di

import HttpClientManager
import com.salesc2.data.network.api.service.*
import com.salesc2.data.repository.*
import com.salesc2.data.viewmodel.*

import createApi
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val NETWORKING_MODULE = module {
    single { HttpClientManager.newInstance(androidContext()) }
    single { get<HttpClientManager>().createApi<UserLoginApi>() }
    single { get<HttpClientManager>().createApi<TeamListApi>()}
    single { get<HttpClientManager>().createApi<DashBoardAPI>() }
    single { get<HttpClientManager>().createApi<AccountListApi>() }
    single { get<HttpClientManager>().createApi<DepartmentListApi>() }
    single { get<HttpClientManager>().createApi<ManageAccountApi>() }
    single { get<HttpClientManager>().createApi<TaskSummaryApi>() }
    single { get<HttpClientManager>().createApi<MorePageApi>() }
    single { get<HttpClientManager>().createApi<PasswordAndSecurityApi>()}
    single { get<HttpClientManager>().createApi<NotificationSettingsApi>()}
    single { get<HttpClientManager>().createApi<TimeZoneApi>()}
    single { get<HttpClientManager>().createApi<UpdateTimeZoneApi>()}
    single { get<HttpClientManager>().createApi<BillingOverViewApi>()}
    single { get<HttpClientManager>().createApi<BillingDetailsApi>()}
    single { get<HttpClientManager>().createApi<BillingHistoryApi>()}
    single { get<HttpClientManager>().createApi<BillingSubScriptionApi>()}

}

val REPOSITORY_MODULE = module {
    single { EmployeeLoginRepository.create(get()) }
    single { TeamListRepository.create(get () )  }
    single { DashBoardRepository.create(get()) }
    single { AccountListRepository.create(get()) }
    single { DepartmentListRepository.create(get()) }
    single { ManageAccountRepository.create(get()) }
    single { TaskSummaryRepository.create(get()) }
    single { MorePageRepository.create(get()) }
    single { PasswordAndSecurityRepository.create(get())}
    single { NotificationSettingsRepository.create(get())}
    single { TimeZoneRepository.create(get())}
    single { UpdateTimeZoneRepository.create(get())}
    single { BillingOverViewRepository.create(get())}
    single { BillingDetailsRepository.create(get())}
    single { BillingHistoryRepository.create(get())}
    single { BillingSubScriptionRepository.create(get())}


}

val VIEW_MODEL_MODULE = module {
    single { UserLoginViewModel(get(), androidContext()) }
    single { TeamListViewModel(get(),androidContext()) }
    single { DashBoardViewModel(get(),androidContext()) }
    single { AccountListViewModel(get(),androidContext()) }
    single { DepartmentListViewModel(get(),androidContext()) }
    single { ManageAccountViewModel(get(),androidContext()) }
    single { TaskSummaryViewModel(get(),androidContext()) }
    single { MorePageViewModel(get(),androidContext()) }
    single { NotificationSettingsViewModel(get(),androidContext()) }
    single { TimeZoneViewModel(get(),androidContext()) }
    single { UpdateTimeZoneViewModel(get(),androidContext()) }
    single { BillingoverViewViewModel(get(),androidContext()) }
    single { BillingDetailsViewModel(get(),androidContext()) }
    single { BillingHistoryViewModel(get(),androidContext()) }
    single { BillingSubscriptionViewModel(get(),androidContext()) }

}
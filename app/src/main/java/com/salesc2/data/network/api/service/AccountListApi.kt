package com.salesc2.data.network.api.service



import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseTeamList
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AccountListApi {

    @POST("ios-task/api/v1/accountFilter")
    suspend fun reqGetAccountList(
        @Body requestAccountList: RequestAccountList
    ): Response<ResponseAccountList>
}
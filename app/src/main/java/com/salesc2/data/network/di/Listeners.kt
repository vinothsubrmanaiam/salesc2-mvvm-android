package com.salesc2.data.network.di

typealias OnSuccess<R> = (R) -> Unit
typealias OnFailure<R> = (R) -> Unit
typealias onException<R> = (R) -> Unit
package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestBillingSubScription
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseBillingSubscription
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.AccountListRepository
import com.salesc2.data.repository.BillingSubScriptionRepository
import com.salesc2.data.repository.EmployeeLoginRepository

import kotlinx.coroutines.launch

class BillingSubscriptionViewModel(
    private val repository: BillingSubScriptionRepository,
    val context: Context
) : ViewModel() {
     fun reqSubscriptionDetails(
         requestBillingSubScription: RequestBillingSubScription,
         onSuccess: OnSuccess<ResponseBillingSubscription>,
         onFailure: OnFailure<ResponseBillingSubscription>

     )
     {
         viewModelScope.launch {
             repository.reqSubscriptionDetails(requestBillingSubScription,onSuccess,onFailure)
         }
     }

}
package com.salesc2.utils

interface HideShowCardSelection {
    fun showSelection()
    fun hideSelection()
    fun refreshCardList()
}
package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestPasswordAndSecurity
import com.salesc2.data.network.api.request.RequestUpdateTimeZone
import com.salesc2.data.network.api.response.ResponsePasswordAndSecurity
import com.salesc2.data.network.api.response.ResponseUpdateTimeZone
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.PasswordAndSecurityRepository
import com.salesc2.data.repository.UpdateTimeZoneRepository
import kotlinx.coroutines.launch

class UpdateTimeZoneViewModel(private val repository: UpdateTimeZoneRepository, val context: Context
) : ViewModel() {
    fun reqUpdateTZ(
        request: RequestUpdateTimeZone,
        onSuccess: OnSuccess<ResponseUpdateTimeZone>,
        onFailure: OnFailure<ResponseUpdateTimeZone>
    )
    {
        viewModelScope.launch {
            repository.reqUpdateTZ(request,onSuccess,onFailure)
        }
    }

}
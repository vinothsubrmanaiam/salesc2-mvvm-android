package com.salesc2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ProductList(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "prod_id") val prod_id: String?,
    @ColumnInfo(name = "prod_name") val prod_name: String?
    )
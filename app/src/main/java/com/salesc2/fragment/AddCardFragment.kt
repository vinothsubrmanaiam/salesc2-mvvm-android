package com.salesc2.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import com.android.volley.*
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.StringRequest
import com.kaopiz.kprogresshud.KProgressHUD
import com.salesc2.R
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.stripe.android.view.CardInputWidget
import mostafa.ma.saleh.gmail.com.editcredit.EditCredit
import org.json.JSONObject
import java.text.SimpleDateFormat


class AddCardFragment : Fragment() {

    private lateinit var nameOnCard : EditText
    private lateinit var card_number : EditCredit
    private lateinit var cvv : EditText
    private lateinit var Expire_date : EditText
    private lateinit var confirmCard :Button
    private lateinit var addEventToolbarHead : TextView
    private lateinit var add_card_back_arrow : ImageView
    private lateinit var cardInputWidget : CardInputWidget

    private var bc_fName = String()
    private var bc_lName = String()
    private var bc_phone = String()
    private var bc_email = String()
    private var bc_company = String()
    private var bc_b_period = String()
    private var bc_address1 = String()
    private var bc_address2 = String()
    private var bc_country = String()
    private var bc_state = String()
    private var bc_city = String()
    private var bc_zip = String()
    private var billing_customer_id =  String()
    private var subscriptionId = String()
    private var unit_amount = String()


    private var expireMonthCard = String()
    private var expireYrCard = String()
    private var cvvCard = String()
    private var nameCard = String()
    private var numberCard = String()

    var webService = WebService()
    var apiRequest = ApiRequest()
    var editCard = String()
    private var bd_lastdigit_card = String()
    private var bd_cardholder_card = String()
    private var bd_expire_card = String()
    private var billingId = String()
    private var paymentMethodId = String()
    private var countryCode = String()
//    var  clientSecretKey = "sk_test_51HaR0gA9n98XqrBDMAoTErl3t0Mi7U76jRBngidGc87m8z6O9RzJoBc0H8rePChyijWnyYalJhmtr155aXkSaVAA00SOfzyTXO"
//var clientSecretKey = "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"
    var addCard = String()
    private var fromPage = String()
    private lateinit var mView : View
    @SuppressLint("ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_add_card, container, false)
        bc_fName = context?.getSharedPref("bc_fName",context!!).toString()
        bc_lName = context?.getSharedPref("bc_lName",context!!).toString()
        bc_phone = context?.getSharedPref("bc_phone",context!!).toString()
        bc_email = context?.getSharedPref("bc_email",context!!).toString()
        bc_company = context?.getSharedPref("bc_company",context!!).toString()
        bc_b_period = context?.getSharedPref("bc_b_period",context!!).toString()
        bc_address1 = context?.getSharedPref("bc_address1",context!!).toString()
        bc_address2 = context?.getSharedPref("bc_address2",context!!).toString()
        bc_country = context?.getSharedPref("bc_country",context!!).toString()
        bc_state = context?.getSharedPref("bc_state",context!!).toString()
        bc_city = context?.getSharedPref("bc_city",context!!).toString()
        bc_zip = context?.getSharedPref("bc_zip",context!!).toString()
        billing_customer_id = context?.getSharedPref("billing_customer_id",context!!).toString()
        subscriptionId = context?.getSharedPref("subscriptionId",context!!).toString()
        unit_amount = context?.getSharedPref("unit_amount",context!!).toString()
        editCard = context?.getSharedPref("editCard",context!!).toString()
        bd_lastdigit_card = context?.getSharedPref("lastdigit_card",context!!).toString()
        bd_cardholder_card = context?.getSharedPref("cardholder_card",context!!).toString()
        bd_expire_card = context?.getSharedPref("expire_card",context!!).toString()
        billingId = context?.getSharedPref("billingId",context!!).toString()
        paymentMethodId = context?.getSharedPref("paymentMethodId",context!!).toString()
        countryCode = context?.getSharedPref("countryCode",context!!).toString()
        addCard = context?.getSharedPref("addCard",context!!).toString()

        fromPage = context?.getSharedPref("fromPage",context!!).toString()
        nameOnCard = mView.findViewById(R.id.nameOnCard)
        card_number = mView.findViewById(R.id.card_number)
        cvv = mView.findViewById(R.id.cvv)
        Expire_date = mView.findViewById(R.id.Expire_date)
        confirmCard = mView.findViewById(R.id.confirmCard)
        addEventToolbarHead = mView.findViewById(R.id.addEventToolbarHead)
        add_card_back_arrow = mView.findViewById(R.id.add_card_back_arrow)
        cardInputWidget = mView.findViewById(R.id.cardInputWidget)
        card_number.maxEms = 16
        cardInputWidget.postalCodeEnabled = false
        cardInputWidget.card?.number
        cardInputWidget.card?.cvc
        cardInputWidget.card?.expYear
        cardInputWidget.card?.expMonth

        nameOnCard.requestFocus()
        var lastInput = ""
        Expire_date.addTextChangedListener(object : TextWatcher {
            @SuppressLint("SetTextI18n")
            override fun afterTextChanged(s: Editable) {
                try {
                    val input = s.toString()
                    if (s.length == 2 && !lastInput.endsWith("/"))
                    {
                        val month = Integer.parseInt(input)
                        if (month <= 12)
                        {
                            Expire_date.setText(Expire_date.text.toString() + "/")
                            Expire_date.clearFocus()
                            Expire_date.requestFocus()
                            Expire_date.post(Runnable {
                                Expire_date.setSelection(
                                    Expire_date.getText().length
                                )
                            })
                        }else{
                            context?.toast("Enter a valid month")
                        }
                        Expire_date.clearFocus()
                        Expire_date.requestFocus()
                    }
                    else if (s.length == 2 && lastInput.endsWith("/"))
                    {
                        val month = Integer.parseInt(input)
                        if (month <= 12)
                        {
                            Expire_date.setText(Expire_date.text.toString().substring(0, 1))
                            Expire_date.clearFocus()
                            Expire_date.post(Runnable {
                                Expire_date.setSelection(
                                    Expire_date.getText().length
                                )
                            })

                        }
                        else{
                            context?.toast("Enter a valid month")
                        }
                        Expire_date.clearFocus()
                        Expire_date.requestFocus()
                        Expire_date.post(Runnable {
                            Expire_date.setSelection(
                                Expire_date.getText().length
                            )
                        })
                    }
                    lastInput = Expire_date.text.toString()
                }catch (e:Exception){
                    print(e)
                }

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, start: Int, removed: Int, added: Int) {
               /* if (start == 1 && start+added == 2 && p0?.contains('/') == false) {
                    Expire_date.setText(p0.toString() + "/")
                } else if (start == 3 && start-removed == 2 && p0?.contains('/') == true) {
                    Expire_date.setText(p0.toString().replace("/", ""))
                }*/
            }

        })

        
        add_card_back_arrow.setOnClickListener{
            fragmentManager?.popBackStack()
        }

        fun EditText.placeCursorToEnd() {
            this.setSelection(this.text.length)
        }
/*if (editCard == "yes"){
    card_number.isEnabled = false
    card_number.isFocusable = false
    card_number.setText("XXXXXXXXXXXX$bd_lastdigit_card")
    nameOnCard.setText(bd_cardholder_card)
    Expire_date.setText(bd_expire_card)
    addEventToolbarHead.text = "Edit Card"
    cvv.isEnabled = false
    cvv.isFocusable = false
    cvv.setText("XXX")
}*/

        onclickAction()
        createPriceApiCall()


        return mView
    }
    var progressHuD : KProgressHUD? = null
private fun validation(){
   try {
       if (cardInputWidget.card?.expYear.toString() != "null" && cardInputWidget.card?.expMonth.toString() != "null") {
           var sdf = SimpleDateFormat("MM/yyyy")
           var monthF = SimpleDateFormat("MM")
           var yrF = SimpleDateFormat("yyyy")
           var parse = sdf.parse("${cardInputWidget.card?.expMonth}/${cardInputWidget.card?.expYear}")
           expireMonthCard = monthF.format(parse)
           expireYrCard = yrF.format(parse)
       }

   }catch (e:Exception){

   }
    numberCard = cardInputWidget.card?.number.toString()
    nameCard = nameOnCard.text.toString()
    cvvCard = cardInputWidget.card?.cvc.toString()
    if (nameCard.isEmpty() || nameCard.isBlank()){
        context?.toast("Please enter card holder name")
        nameOnCard.requestFocus()
    }else if (numberCard.isEmpty() || numberCard.isBlank() || numberCard == "null"){
        context?.toast("Please enter the valid card details")
    }
    else if (cardInputWidget.card?.expMonth.toString() == "null" ||cardInputWidget.card?.expYear.toString() == "null"){
        context?.toast("Please enter the expiry date")
    }
    else if (cardInputWidget.card?.cvc.toString() == "null"){
        context?.toast("Please enter the cvc")
    }else if (cardInputWidget.card?.cvc.toString().length < 3 || cardInputWidget.card?.cvc.toString().length > 3 ){
        context?.toast("Please enter the valid cvc")
    }
    else if (cardInputWidget.card?.cvc?.contains(".")!! || cardInputWidget.card?.cvc?.contains(",")!!){
        context?.toast("Please enter the valid cvc")
    }
   /* else if (Expire_date.text.toString().length <7){
        context?.toast("Please enter the valid expiry date")
    }else if (expireMonthCard.toInt() < 1 ||expireMonthCard.toInt() > 12 ){
        context?.toast("Please enter the valid expiry month")
    }else if (expireYrCard.toInt() < 2021){
        context?.toast("Please enter the valid expiry year")
    }
    else if (cvvCard.isEmpty() || cvvCard.isBlank()){
        context?.toast("Please enter the cvv")
    } else if (cvvCard.length < 3 || cvvCard.toInt() == 0){
        context?.toast("Please enter the valid cvv")
    }*/
    else{
        numberCard = cardInputWidget.card?.number.toString()
        nameCard = nameOnCard.text.toString()
        cvvCard = cardInputWidget.card?.cvc.toString()
        try {
            progressHuD= KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                //.setDetailsLabel("Downloading data")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .setBackgroundColor(57000000)
                .show()
            var sdf = SimpleDateFormat("MM/yyyy")
            var monthF = SimpleDateFormat("MM")
            var yrF = SimpleDateFormat("yyyy")
            var parse = sdf.parse("${cardInputWidget.card?.expMonth}/${cardInputWidget.card?.expYear}")
            expireMonthCard = monthF.format(parse)
            expireYrCard = yrF.format(parse)
            if (editCard == "yes"){
                editCardApiCall(billingId,nameOnCard.text.toString(),expireMonthCard,expireYrCard,paymentMethodId)
            }else{
                stripePaymentMethodApiCall()
            }

        }catch (e:Exception){

        }
    }
}



    private fun onclickAction(){
        confirmCard.setOnClickListener{
            cardInputWidget.card?.number
            cardInputWidget.card?.cvc
            cardInputWidget.card?.expYear
            cardInputWidget.card?.expMonth
            validation()
        }
    }


    private fun stripePaymentMethodApiCall(){
        var params = HashMap<String,Any>()
        params["type"] = "card"
        params["card[number]"] = numberCard
        params["card[exp_month]"] = expireMonthCard
        params["card[exp_year]"] = expireYrCard
        params["card[cvc]"] = cvvCard
        params["billing_details[name]"] = nameOnCard.text.toString()

        context?.let {
            postRequestBodyWithHeadersPayment(it,webService.stripe_url,params){ response->
                try {
                    var p_id = response.getString("id")
                    var payment_method_id = p_id
                    context?.setSharedPref(context!!,"paymentMethodId",p_id)

                    var cardObj = response.getJSONObject("card")
                    var exp_month = cardObj.getString("exp_month")
                    var exp_year = cardObj.getString("exp_year")
                    var last4 = cardObj.getString("last4")
                    var funding = cardObj.getString("funding")

                    if (addCard == "yes"){

                        updateCardDetailsApiCall(billingId,nameOnCard.text.toString(),"$exp_month/$exp_year",last4,p_id)
                    }else{
                        billingSetUpApiCall(payment_method_id,last4,nameOnCard.text.toString(),"$exp_month/$exp_year")
                    }



                }catch (e:Exception){
                    progressHuD?.dismiss()

                }

            }
        }

    }



    private fun postRequestBodyWithHeadersPayment(context: Context, path: String, param: HashMap<String, Any>, completion: (JSONObject) -> Unit) {
        var mStringRequest: StringRequest

        val cache = DiskBasedCache(context.cacheDir, 1024 * 1024)
        val network = BasicNetwork(HurlStack())


        mStringRequest = object : StringRequest(
            Method.POST,path,
            Response.Listener<String> { response->
                if (response != null) {
                    var info = JSONObject(response)
                    completion(info)
                }
            },
            Response.ErrorListener { volleyError ->
                Log.i("This is the error", "Error :$volleyError")
                var message= String()
                if (volleyError is NetworkError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ServerError)
                {
                    message = "The server could not be found. Please try again after some time!!"
                }
                else if (volleyError is AuthFailureError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ParseError)
                {
                    message = "Parsing error! Please try again after some time!!"
                }
                else if (volleyError is NoConnectionError)
                {
                    message = "Failed to connect server. Please try again after some time"
                }
                else if (volleyError is TimeoutError)
                {
                    message = "Connection TimeOut! Please check your internet connection."
                }
              //  context.alertDialog(context,"Alert",message)

            }) {
            override fun getBodyContentType(): String {
                return "application/x-www-form-urlencoded"
            }
            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                var authToken = context.getSharedPref("token",context).toString()
                var header = HashMap<String,String>()
                header.put("Authorization","Bearer ${webService.clientSecretKey}")
                return header
            }
            @Throws(AuthFailureError::class)
            override fun getParams(): MutableMap<String, String>? {
                return param as MutableMap<String,String>
            }
        }
        val mRequestQueue = RequestQueue(cache, network).apply {
            start()
        }
        mRequestQueue.add(mStringRequest)
    }

    private fun createPriceApiCall(){
        var bCycle:Int? = null
        if (bc_b_period == "Monthly"){
            bCycle = 0
        }else{
            bCycle = 1
        }
        var params = HashMap<String,Any>()
        params["unit_amount"] = unit_amount.toDouble()
        params["billingCycle"] = bCycle
        params["currency"]="usd"
        params["productName"] = "Professional plan"

        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.create_price_url,params){ response->

                println("Add_card"+" "+webService.create_price_url)
                println("Add_card"+" "+response)
                try {
                    var status = response.getString("status")
                    if (response.has("msg")){
                        var msg = response.getString("msg")
                    }
                    if (status == "200"){
                        var dataObj = response.getJSONObject("data")
                        var id = dataObj.getString("id")
                        context?.setSharedPref(context!!,"priceId",id)
                    }

                }catch (e:Exception){

                }
            }
        }
    }



    private fun billingSetUpApiCall(paymentMethodId:String,lastDigit:String,cardHolder:String,expire:String){
        var bCycle:Int? = null
        if (bc_b_period == "Monthly"){
            bCycle = 0
        }else{
            bCycle = 1
        }
        var priceId = context?.getSharedPref("priceId",context!!).toString()
        var params = HashMap<String,Any>()
        params["firstName"] = bc_fName
        if (bc_lName != ""){
            params["lastName"] = bc_lName
        }
        params["phone"] = bc_phone.replace("+","").replace("-","")
        params["email"] = bc_email
        params["period"] = bCycle
        params["subscriptionId"] = subscriptionId
        params["companyName"] = bc_company
        params["addresLine1"] = bc_address1
        if (bc_address2 != ""){
            params["addressLine2"] = bc_address2
        }
        params["city"] = bc_city
        params["state"] = bc_state
        params["country"] = bc_country
        params["countryCode"] = countryCode
        params["zipCode"] = bc_zip
        params["customerId"] = billing_customer_id
        params["paymentMethodId"] = paymentMethodId
        params["priceId"] = priceId
        params["lastDigit"] = lastDigit.toInt()
        params["cardHolder"] = cardHolder
        params["expire"] = expire

        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.billing_setup_store_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        progressHuD?.dismiss()
                        context?.toast(msg)
                        context?.loadFragment(context!!,BillingOverviewFragment())
                    }else{
                        context?.toast(msg)
                        progressHuD?.dismiss()
                    }
                }catch (e:Exception){
                    progressHuD?.dismiss()

                }
            }
        }

    }


    private fun editCardApiCall(billingId:String,cardHolder:String,exp_month:String,exp_year:String,paymentMethodId:String){
        val params = HashMap<String,Any>()
        params["billingId"] = billingId
        params["name"] = cardHolder
        params["exp_month"] = exp_month.toInt()
        params["exp_year"] = exp_year.toInt()
        params["paymentMethodId"] = paymentMethodId

        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.edit_card_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        progressHuD?.dismiss()
                        context?.toast(msg)
                        activity?.findViewById<RelativeLayout>(R.id.coordinatorlayout)?.visibility = View.VISIBLE
                        context?.setSharedPref(context!!,"editCard","yes")
                        if (fromPage == "Dashboard"){
                            context?.loadFragment(context!!,BillingOverviewFragment())
                        }
                        else{
                            progressHuD?.dismiss()
                            context?.loadFragment(context!!,BillingDetailsFragment())
                        }
                    }else{
                        progressHuD?.dismiss()
                        context?.toast(msg)
                    }
                }catch (e:Exception){
                    progressHuD?.dismiss()

                }
            }
        }

    }


    private fun updateCardDetailsApiCall(billingId:String,cardHolder:String,expire:String,lastDigit:String,paymentMethodId:String){
        val params = HashMap<String,Any>()
        params["billingId"] = billingId
        params["cardHolder"] = cardHolder
        params["expire"] = expire
        params["lastDigit"] = lastDigit.toInt()
        params["paymentMethodId"] = paymentMethodId
        context?.let {
            apiRequest.putRequestBodyWithHeadersAny(it,webService.card_details_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        context?.setSharedPref(context!!,"addCard","no")
                        progressHuD?.dismiss()
                        activity?.findViewById<RelativeLayout>(R.id.coordinatorlayout)?.visibility = View.VISIBLE
                        if (fromPage == "DashBoard"){
                            context?.loadFragment(context!!,BillingOverviewFragment())
                            context?.setSharedPref(context!!,"showCardList","yes")
                        }
                        else{
                            context?.loadFragment(context!!,BillingDetailsFragment())
                        }
                        context?.toast(msg)
                    }else{
                        progressHuD?.dismiss()
                        context?.toast(msg)
                    }
                }catch (e:Exception){
                    progressHuD?.dismiss()

                }
            }
        }
    }
}
package com.salesc2.adapters

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.fragment.EventDetailsFragment
import com.salesc2.fragment.NotificationReassignFragment
import com.salesc2.fragment.TaskDetailsFragment
import com.salesc2.model.NotificationListModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.squareup.picasso.Picasso
import me.thanel.swipeactionview.SwipeActionView
import me.thanel.swipeactionview.SwipeGestureListener
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap

class NotificationsListAdapter():RecyclerView.Adapter<NotificationsListAdapter.MyViewHolder>() {
    private lateinit var notificationItems: ArrayList<NotificationListModel>
    var context: Context? = null
    var webService = WebService()
    var apiRequest = ApiRequest()

    constructor(context: Context?, notificationItems: ArrayList<NotificationListModel>) : this(){
        this.context = context
        this.notificationItems = notificationItems
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_notification_list, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return notificationItems.size
    }

    @SuppressLint("SimpleDateFormat")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
    var notificationListModel : NotificationListModel = notificationItems[position]
//        holder.delete_notify_layout.visibility = View.GONE
        if (notificationListModel.n_title != "null"){
            holder.notify_title.text = notificationListModel.n_title
        }
        if (notificationListModel.n_description != "null") {
            holder.notify_details.visibility = View.VISIBLE
            holder.notify_details.text = notificationListModel.n_description
        }else{
            holder.notify_details.visibility = View.GONE
        }
        try {
            var parser = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val pastDate: Date = parser.parse(notificationListModel.n_createdAt)
            val dateToLocal = context?.dateFromUTC(context!!,pastDate)
            val past = dateToLocal

            val time_format = SimpleDateFormat("hh:mm a")
            val time_parse: Date = parser.parse((notificationListModel.n_createdAt))
            var time_now: String = time_format.format(time_parse)
            val now = Date()

            val seconds: Long =
                TimeUnit.MILLISECONDS.toSeconds(now.time - past!!.time)
            val minutes: Long =
                TimeUnit.MILLISECONDS.toMinutes(now.time - past!!.time)
            val hours: Long =
                TimeUnit.MILLISECONDS.toHours(now.time - past!!.time)
            val days: Long =
                TimeUnit.MILLISECONDS.toDays(now.time - past!!.time)


            when {
                seconds < 60 -> {
                    holder.notify_date.text = "$seconds sec ago"
                }
                minutes < 60 -> {
                    holder.notify_date.text = "$minutes min ago"
                }
                hours < 24 -> {
                    holder.notify_date.text = "$hours hour ago"
                }
                days < 1 -> {
                    //                              prTime = "Yesterday $time_now"
                    holder.notify_date.text = "$days day ago"
                }

                days >= 1 -> {
                    if (days > 365) {
                        holder.notify_date.text = ((days / 365).toString()) + " year ago"
                    } else if (days > 30) {
                        holder.notify_date.text = (days / 30).toString() + " month ago"
                    } else if (days > 7) {
                        holder.notify_date.text = (days / 7).toString() + " week ago"
                    } else {
                        holder.notify_date.text = "$days days ago"
                    }
                }
            }

            if (notificationListModel.n_read == "false"|| notificationListModel.n_read.equals(false)){
                context?.let { ContextCompat.getColor(it,R.color.transparent_blue) }?.let {
                    holder.notify_cardview.setBackgroundColor(it)
                }

            }else{
                 context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                    holder.notify_cardview.setBackgroundColor(it)
                }
            }


        }catch (e:Exception){
            print(e)
        }


        holder.notify_swipeLayout.swipeGestureListener = object : SwipeGestureListener {
            override fun onSwipedLeft(swipeActionView: SwipeActionView): Boolean {
                holder.delete_notify_layout.visibility = View.VISIBLE
                holder.deleteImg.isClickable = true
                holder.delete_notify_layout.isClickable = true

                return false
            }

            override fun onSwipedRight(swipeActionView: SwipeActionView): Boolean {
                holder.delete_notify_layout.visibility = View.VISIBLE
                holder.deleteImg.isClickable = true
                holder.delete_notify_layout.isClickable = true
                return false
            }
        }

        holder.deleteImg.setOnClickListener {
            holder.delete_notify_layout.performClick()
        }
        holder.delete_notify_layout.setOnClickListener{

            val dialog = context?.let { it1 -> Dialog(it1) }
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
            dialog?.setContentView(R.layout.custom_yes_or_no_alert)
            dialog?.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
            val body = dialog?.findViewById(R.id.headtitle) as TextView
            body.text = "Alert"
            val msg1 = dialog?.findViewById(R.id.message) as TextView
            msg1.text = "Do you want to delete this notification"
            val dialogCancel = dialog?.findViewById(R.id.cancel) as TextView
            dialogCancel.setOnClickListener{
                dialog.dismiss()
            }
            val dialogButton = dialog.findViewById(R.id.ok) as TextView
            dialogButton.setOnClickListener {
                val params = HashMap<String,Any>()
                params["id"] =   notificationListModel.n_id
                params["type"] = "clear"
                context?.let { it1 ->
                    apiRequest.postRequestBodyWithHeadersAny(it1,webService.Single_notification_url,params){ response->
                        try {
                            var status = response.getString("status")
                            var msg = response.getString("msg")
                            if (status == "200"){
                                context?.toast(msg)
                                (context as MainActivity).resetNotificationLists(context as MainActivity)
                            } else{
                                context?.toast(msg)
                            }
                        }catch (e:Exception){
                            print(e)
                        }

                    }
                }
                dialog?.dismiss()
            }
            dialog?.show()

        }

        holder.notify_swipeLayout.setOnClickListener{
            holder.notify_cardview.performClick()
        }
        holder.notify_title.setOnClickListener{
            holder.notify_cardview.performClick()
        }
        holder.notify_date.setOnClickListener{
            holder.notify_cardview.performClick()
        }
        holder.notify_details.setOnClickListener{
            holder.notify_cardview.performClick()
        }
        holder.notify_cardview.setOnClickListener {

            if (notificationListModel.n_type == "0") {
                if (notificationListModel.n_showPopup.equals(true) || notificationListModel.n_showPopup == "true") {
                    singleRead(notificationListModel.n_id)
                    acceptRejectPopUp(notificationListModel.n_refId)
                } else {
                    singleRead(notificationListModel.n_id)
                    context?.setSharedPref(context!!,"task_id",notificationListModel.n_refId)
                    var bundle = Bundle()
                    bundle.putString("task_id", notificationListModel.n_refId)
                    var fragment = TaskDetailsFragment()
                    fragment.arguments = bundle
                    val activity = context as AppCompatActivity

                    val transaction: FragmentTransaction =
                        activity.supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.main_fragmet_container, fragment)
                    transaction.addToBackStack(null)
                    transaction.commit()
                }

            }else if (notificationListModel.n_type == "1"){
                if (notificationListModel.n_showPopup.equals(true) || notificationListModel.n_showPopup == "true") {
                    singleRead(notificationListModel.n_id)
                    acceptRejectPopUpEvent(notificationListModel.n_refId)
                } else {
                    singleRead(notificationListModel.n_id)
                    var bundle = Bundle()
                    bundle.putString("event_id", notificationListModel.n_refId)

                    var fragment = EventDetailsFragment()
                    fragment.arguments = bundle
                    val activity = context as AppCompatActivity
                    val transaction: FragmentTransaction =
                        activity.supportFragmentManager.beginTransaction()
                    transaction.replace(R.id.main_fragmet_container, fragment)
                    transaction.addToBackStack(null)
                    transaction.commit()
                }
            }
        }

    }


    private fun acceptRejectPopUp(taskId:String){
        var pp_id = String()
        var pp_priority = String()
        var pp_startDate = String()
        var pp_endDate = String()
        var pp_assignedTo = String()
        var pp_taskName = String()
        var pp_conflictName = String()
        var pp_conflict = String()
        val params = HashMap<String,Any>()
        params["id"]=taskId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.Task_notification_url,params){ response->
                try {
                    var status = response.getString("status")

                    if (status == "200") {
                        var dataObj = response.getJSONObject("data")

                        pp_id = dataObj.getString("_id")
                        pp_priority = dataObj.getString("priority")
                        pp_startDate = dataObj.getString("startDate")
                        pp_endDate = dataObj.getString("endDate")
                        pp_assignedTo = dataObj.getString("assignedTo")
                        pp_taskName = dataObj.getString("taskName")
                        pp_conflictName = dataObj.getString("conflictName")
                        pp_conflict = dataObj.getString("conflict")

                        //bottom popup
                        var mBottomSheetDialog: Dialog? =
                            context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) }
                        mBottomSheetDialog?.setContentView(R.layout.accept_reject_popup)
                        var accept = mBottomSheetDialog?.findViewById<Button>(R.id.accept_btn)
                        var reject = mBottomSheetDialog?.findViewById<Button>(R.id.reject_btn)
                        var close: ImageView? = mBottomSheetDialog?.findViewById(R.id.close_popup)
                        var notify_popup_title : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_title)
                        var notify_popup_name : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_name)
                        var notify_popup_datetime : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_datetime)
                        var notify_popup_image : ImageView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_image)
                        var notify_popup_emergency_tv : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_emergency_tv)
                         var notify_popup_emergency_alert : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_emergency_alert)
                        if (pp_priority == "true")
                        {
                            notify_popup_emergency_tv?.visibility = View.VISIBLE
                            Picasso.get().load(R.drawable.task_emergancy_img).into(notify_popup_image)
                            if (pp_conflict == "true") {
                                notify_popup_emergency_alert?.visibility = View.VISIBLE
                                notify_popup_emergency_alert?.text = pp_conflictName
                            }
                        }else{
                            notify_popup_emergency_tv?.visibility = View.GONE
                            notify_popup_emergency_alert?.visibility = View.GONE
                        }

                        notify_popup_title?.text = "Task Assignment"
                        notify_popup_name?.text = context?.capFirstLetter(pp_taskName)

                        try {
                            var parser = SimpleDateFormat("yyyy-MM-dd HH:mm")
                            val cal = Calendar.getInstance()
                            var tz:TimeZone = cal.timeZone
                            val myFormatDate = "MMM dd, yyyy"
                            val sdf = SimpleDateFormat(myFormatDate)
                            sdf.timeZone = TimeZone.getTimeZone(tz.displayName)
                            val stDate = sdf.format(parser.parse(pp_startDate))
                            val myFormatTime = "hh:mm a"
                            val sdfTime = SimpleDateFormat(myFormatTime)
                            sdfTime.timeZone = TimeZone.getTimeZone(tz.displayName)
                            val startTime =sdfTime.format(parser.parse(pp_startDate))
                            val endTime = sdfTime.format(parser.parse(pp_endDate))
                            notify_popup_datetime?.text = "$stDate at $startTime to $endTime".replace("am","AM").replace("pm","PM")


                        }catch (e:Exception){
                            print("td_date parse== $e")
                        }

                        mBottomSheetDialog?.setCancelable(true)
                        mBottomSheetDialog?.window?.setLayout(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT
                        );
                        mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
                        mBottomSheetDialog?.show()

                        close?.setOnClickListener{
                            mBottomSheetDialog?.dismiss()
                        }

                        reject?.setOnClickListener{
                            val paramR = HashMap<String,Any>()
                            paramR["_id"] = taskId
                            paramR["taskStatus"] = "rejected"
                            context?.let { it1 ->
                                apiRequest.postRequestBodyWithHeadersAny(
                                    it1,
                                    webService.Update_status_url,
                                    paramR
                                ) { response ->

                                    var status = response.getString("status")
                                    var msg = response.getString("msg")
                                    if (status == "200") {
                                        context?.toast(msg)
                                        context?.progressBarDismiss(context!!)
                                        mBottomSheetDialog?.dismiss()
                                        (context as MainActivity).resetNotificationLists(context as MainActivity)

                                    } else {
                                        context?.toast(msg)
                                        context?.progressBarDismiss(context!!)
                                    }

                                }
                            }
                        }

                        accept?.setOnClickListener{
                            if (pp_conflict.equals("true") || pp_conflict.equals(true)){
                                notify_popup_title?.text = "Conflict Found"
                                accept?.setText("Reassign/Reschedule")
                                reject?.setText("Skip")
                                notify_popup_name?.text = pp_conflictName
                                Picasso.get().load(R.drawable.task_conflict_img).into(notify_popup_image)

                                reject?.setOnClickListener{
                                    mBottomSheetDialog?.dismiss()

                                }
                                accept?.setOnClickListener{
                                    mBottomSheetDialog?.dismiss()
                                    context?.setSharedPref(context!!,"task_id_notify",taskId)
                                    var fragment = NotificationReassignFragment()

                                    val activity = context as AppCompatActivity

                                    val transaction: FragmentTransaction =
                                        activity.supportFragmentManager.beginTransaction()
                                    transaction.replace(R.id.main_fragmet_container, fragment)
                                    transaction.addToBackStack(null)
                                    transaction.commit()


                                }


                            }else{
                                val param = HashMap<String,Any>()
                                param["_id"] = taskId
                                param["taskStatus"] = "assigned"
                                context?.let { it1 ->
                                    apiRequest.postRequestBodyWithHeadersAny(
                                        it1,
                                        webService.Update_status_url,
                                        param
                                    ) { response ->

                                        var status = response.getString("status")
                                        var msg = response.getString("msg")
                                        if (status == "200") {
                                            context?.toast(msg)
                                            context?.progressBarDismiss(context!!)
                                            mBottomSheetDialog?.dismiss()
                                            (context as MainActivity).resetNotificationLists(context as MainActivity)
                                        } else {
                                            context?.toast(msg)
                                            context?.progressBarDismiss(context!!)
                                        }

                                    }
                                }
                            }
                        }




                    }else{
                        var msg = response.getString("msg")
                      context?.toast(msg)

                    }
                }catch (e:Exception){

                }
            }
        }






    }


    private fun acceptRejectPopUpEvent(eventId:String){
        var pp_id = String()
        var pp_priority = String()
        var pp_startDate = String()
        var pp_endDate = String()
        var pp_assignedTo = String()
        var pp_conflictName = String()
        var pp_conflict = String()
        var pp_title = String()
        var pp_desc = String()
        var pp_conflictType = String()
        var pp_eventType = String()
        var pp_eventType_type = String()
        var pp_conflictId = String()
        val params = HashMap<String,Any>()
        params["id"] = eventId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.Event_notification_url,params){ response->
                try {
                    var status = response.getString("status")

                    if (status == "200") {
                        var dataObj = response.getJSONObject("data")

                        pp_id = dataObj.getString("_id")
                        pp_title = dataObj.getString("title")
                        if (dataObj.has("description")){
                            pp_desc = dataObj.getString("description")
                        }

                        pp_priority = dataObj.getString("priority")
                        pp_startDate = dataObj.getString("startDate")
                        pp_endDate = dataObj.getString("endDate")
                        pp_conflictType = dataObj.getString("conflictType")
                        pp_eventType = dataObj.getString("eventType")
                        pp_eventType_type = dataObj.getString("eventType_type")
                        pp_conflictId = dataObj.getString("conflictId")
                        pp_conflictName = dataObj.getString("conflictName")
                        pp_conflict = dataObj.getString("conflict")

                        //bottom popup
                        var mBottomSheetDialog: Dialog? =
                            context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) }
                        mBottomSheetDialog?.setContentView(R.layout.accept_reject_popup)
                        var accept = mBottomSheetDialog?.findViewById<Button>(R.id.accept_btn)
                        var reject = mBottomSheetDialog?.findViewById<Button>(R.id.reject_btn)
                        var close: ImageView? = mBottomSheetDialog?.findViewById(R.id.close_popup)
                        var notify_popup_title : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_title)
                        var notify_popup_name : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_name)
                        var notify_popup_datetime : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_datetime)
                        var notify_popup_image : ImageView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_image)
                        var notify_popup_emergency_tv : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_emergency_tv)
                        var notify_popup_emergency_alert : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_emergency_alert)
                        if (pp_priority == "true")
                        {
                            notify_popup_emergency_tv?.visibility = View.VISIBLE
                            Picasso.get().load(R.drawable.task_emergancy_img).into(notify_popup_image)
                            if (pp_conflict == "true") {
                                notify_popup_emergency_alert?.visibility = View.VISIBLE
                                notify_popup_emergency_alert?.text = context?.capFirstLetter(pp_conflictName)
                            }
                        }else{
                            notify_popup_emergency_tv?.visibility = View.GONE
                            notify_popup_emergency_alert?.visibility = View.GONE
                        }

                        notify_popup_title?.text = pp_eventType.toUpperCase()
                        notify_popup_name?.text = context?.capFirstLetter(pp_title)


                        try {
                            var parser = SimpleDateFormat("yyyy-MM-dd HH:mm")
                            val cal = Calendar.getInstance()
                            var tz:TimeZone = cal.timeZone
                            val myFormatDate = "MMM dd, yyyy"
                            val sdf = SimpleDateFormat(myFormatDate)
                            sdf.timeZone = TimeZone.getTimeZone(tz.displayName)
                            val stDate = sdf.format(parser.parse(pp_startDate))
                            val myFormatTime = "hh:mm a"
                            val sdfTime = SimpleDateFormat(myFormatTime)
                            sdfTime.timeZone = TimeZone.getTimeZone(tz.displayName)
                            val startTime =sdfTime.format(parser.parse(pp_startDate))
                            val endTime = sdfTime.format(parser.parse(pp_endDate))
                            notify_popup_datetime?.text = "$stDate at $startTime to $endTime".replace("am","AM").replace("pm","PM")


                        }catch (e:Exception){
                            print("td_date parse== $e")
                        }


                        mBottomSheetDialog?.setCancelable(true)
                        mBottomSheetDialog?.window?.setLayout(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT
                        );
                        mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
                        mBottomSheetDialog?.show()

                        close?.setOnClickListener{
                            mBottomSheetDialog?.dismiss()
                        }

                        reject?.setOnClickListener{
                            val paramR = HashMap<String,Any>()
                            paramR["type"] = 2
                            paramR["id"] = eventId
                            context?.let { it1 ->
                                apiRequest.postRequestBodyWithHeadersAny(
                                    it1,
                                    webService.Event_accept_reject_url,
                                    paramR
                                ) { response ->

                                    var status = response.getString("status")
                                    var msg = response.getString("msg")
                                    if (status == "200") {
                                        context?.toast(msg)
                                        context?.progressBarDismiss(context!!)
                                        mBottomSheetDialog?.dismiss()
                                        (context as MainActivity).resetNotificationLists(context as MainActivity)

                                    } else {
                                        context?.toast(msg)
                                        context?.progressBarDismiss(context!!)
                                    }

                                }
                            }
                        }

                        accept?.setOnClickListener{
                            if (pp_conflict.equals("true") || pp_conflict.equals(true)){
                                notify_popup_title?.text = "Conflict Found"
                                accept?.setText("Reassign/Reschedule")
                                reject?.setText("Skip")
                                notify_popup_name?.text = pp_conflictName
                                Picasso.get().load(R.drawable.task_conflict_img).into(notify_popup_image)

                                reject?.setOnClickListener{
                                    mBottomSheetDialog?.dismiss()
                                }
                                accept?.setOnClickListener{
                                    mBottomSheetDialog?.dismiss()
                                }


                            }else{
                                val param = HashMap<String,Any>()
                                param["type"] = 1
                                param["id"] = eventId
                                context?.let { it1 ->
                                    apiRequest.postRequestBodyWithHeadersAny(
                                        it1,
                                        webService.Event_accept_reject_url,
                                        param
                                    ) { response ->

                                        var status = response.getString("status")
                                        var msg = response.getString("msg")
                                        if (status == "200") {
                                            context?.toast(msg)
                                            context?.progressBarDismiss(context!!)
                                            mBottomSheetDialog?.dismiss()
                                            (context as MainActivity).resetNotificationLists(context as MainActivity)
                                        } else {
                                            context?.toast(msg)
                                            context?.progressBarDismiss(context!!)
                                        }

                                    }
                                }
                            }
                        }




                    }else{
                        var msg = response.getString("msg")
                        context?.toast(msg)
                    }
                }catch (e:Exception){

                }
            }
        }

    }
private fun singleRead(id:String) {
    val params = HashMap<String, Any>()
    params["id"] = id
    params["type"] = "read"
    context?.let { it1 ->
        apiRequest.postRequestBodyWithHeadersAny(
            it1,
            webService.Single_notification_url,
            params
        ) { response ->
            try {
                var status = response.getString("status")
                var msg = response.getString("msg")
                if (status == "200") {

                    (context as MainActivity).resetNotificationLists(context as MainActivity)
//                                context?.toast(msg)
                } else {
//                                context?.toast(msg)

                }
            } catch (e: Exception) {
                print(e)
            }

        }
    }
}


    class MyViewHolder constructor(itemView:View):RecyclerView.ViewHolder(itemView) {
        var notify_title : TextView = itemView.findViewById(R.id.notify_title)
        var notify_date : TextView = itemView.findViewById(R.id.notify_date)
        var notify_details : TextView = itemView.findViewById(R.id.notify_details)
        var notify_swipeLayout : SwipeActionView = itemView.findViewById(R.id.notify_swipeLayout)
        var delete_notify_layout : ConstraintLayout = itemView.findViewById(R.id.delete_notify_layout)
        var notify_cardview : CardView = itemView.findViewById(R.id.notify_cardview)
        var deleteImg : ImageView = itemView.findViewById(R.id.deleteImg)
    }



    fun String.toDate(dateFormat: String = "dd/MM/yyyy hh:mm a", timeZone: TimeZone = TimeZone.getDefault()): Date {
        val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
        parser.timeZone = timeZone
        return parser.parse(this)
    }
}
package com.salesc2.data.network.api.service



import com.salesc2.data.network.api.request.RequestMorePage
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.api.response.ResponseMorePage
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface MorePageApi {

    @POST("ios-login/api/v1/employee/logout")
    suspend fun reqMorePage(
        @Body requestMorePage: RequestMorePage
    ): Response<ResponseMorePage>
}
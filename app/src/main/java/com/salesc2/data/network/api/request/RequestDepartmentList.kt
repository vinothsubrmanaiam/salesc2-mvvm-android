package com.salesc2.data.network.api.request

import com.google.gson.annotations.SerializedName

data class RequestDepartmentList(
    @SerializedName("search") val search: String? = "",
    @SerializedName("accountId") val accountId: String? = ""
)

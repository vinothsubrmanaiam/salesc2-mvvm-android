package com.salesc2.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.adapters.TaskProductsListAdapter
import com.salesc2.database.*
import com.salesc2.model.AddProductsModel
import com.salesc2.model.ProductsModel
import com.salesc2.model.TaskProductsModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.DatabaseHandler
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class AddTaskFragment : Fragment() {

    var webService = WebService()
    var apiRequest = ApiRequest()
    lateinit var mView: View

    var accountId = String()
    var accountName = String()
    var deptId = String()
    var deptName = String()
    var serviceId = String()
    var serviceName = String()
    var startDate = String()
    var startTime = String()
    var endDate = String()
    var endTime = String()
    var assignee = String()
    var assigneeId = String()
    var assigneeImg = String()

    lateinit var add_products: Button

    lateinit var taskHead: TextView
    lateinit var taskDateTime: TextView
    lateinit var assigneeImage: CircleImageView
    lateinit var assigneeName: TextView
    lateinit var addTask: Button
    lateinit var offline_addTask: Button

    lateinit var addTask_description: EditText
    private lateinit var prioritySwitch: Switch
    private lateinit var cancel_addTask: Button
    lateinit var emergency_text: TextView
    lateinit var add_task_back_arrow: ImageView
    private lateinit var assignee_change_layout: ConstraintLayout

    lateinit var addProductsRecyclerView: RecyclerView
    var addProductsModel = ArrayList<AddProductsModel>()
    var priority: Boolean = false

    var productsModel = ArrayList<ProductsModel>()

    var taskProductsModel = ArrayList<TaskProductsModel>()
    var prodName = String()
    var prodPrice = String()
    var prodQty = String()
    var arrayList = ArrayList<String>()
    var products = ArrayList<ProductsModel>()
    private var db: AppDatabase? = null


    private var edit_task = String()
    private var edit_task_taskId = String()

    private lateinit var addTaskHead: TextView

    private var task_dt_edited = String()

    private var isChecked_task_priority = String()
    var TIME = 3 * 1000.toLong()

    override fun onResume() {
        super.onResume()

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_add_task, container, false)
        accountId = context?.getSharedPref("acc_filter_id", context!!).toString()
        accountName = context?.getSharedPref("acc_filter_name", context!!).toString()
        deptId = context?.getSharedPref("dept_filter_id", context!!).toString()
        deptName = context?.getSharedPref("dept_filter_name", context!!).toString()
        serviceId = context?.getSharedPref("service_filter_id", context!!).toString()
        serviceName = context?.getSharedPref("service_filter_name", context!!).toString()
        startDate = context?.getSharedPref("start_date", context!!).toString()
        endDate = context?.getSharedPref("end_date", context!!).toString()
        startTime = context?.getSharedPref("start_time", context!!).toString()
        endTime = context?.getSharedPref("end_time", context!!).toString()

        edit_task = context?.getSharedPref("edit_task", context!!).toString()
        edit_task_taskId = context?.getSharedPref("edit_task_taskId", context!!).toString()

//        assignee = arguments?.getString("assignee_name").toString()
//        assigneeId = arguments?.getString("assignee_id").toString()
//        assigneeImg = arguments?.getString("assignee_image").toString()

        assignee = context?.getSharedPref("assignee_name", context!!).toString()
        assigneeId = context?.getSharedPref("assignee_id", context!!).toString()
        assigneeImg = context?.getSharedPref("assignee_image", context!!).toString()

        prodName = arguments?.getString("prod_name").toString()
        prodPrice = arguments?.getString("prod_price").toString()
        prodQty = arguments?.getString("prod_qty").toString()


        isChecked_task_priority =
            context?.getSharedPref("isChecked_task_priority", context!!).toString()

        addTaskHead = mView.findViewById(R.id.addTaskHead)

        assigneeImage = mView.findViewById(R.id.assignee_image)
        assigneeName = mView.findViewById(R.id.assignee_name)
        taskHead = mView.findViewById(R.id.add_task_head_tv)
        taskDateTime = mView.findViewById(R.id.addTask_date_time_tv)
        addTask = mView.findViewById(R.id.addTask_btn)
        offline_addTask = mView.findViewById(R.id.offline_addTask)
        addTask_description = mView.findViewById(R.id.addTask_description_et)
        prioritySwitch = mView.findViewById(R.id.priority_switch)
        cancel_addTask = mView.findViewById(R.id.cancel_addTask)
        emergency_text = mView.findViewById(R.id.emergency_text)
        add_task_back_arrow = mView.findViewById(R.id.add_task_back_arrow)
        assignee_change_layout = mView.findViewById(R.id.assignee_change_layout)
        addProductsRecyclerView = mView.findViewById(R.id.add_products_recyclerView)
        addProductsModel = ArrayList()
        productsModel = ArrayList()
        products = ArrayList()
        taskProductsModel = ArrayList()
        taskHead.text = "$serviceName at $accountName ($deptName)"
        task_dt_edited = context?.getSharedPref("task_dt_edited", context!!).toString()


        ///check internet
        if (context?.isNetworkAvailable(context!!) == true) {
            //send data to server
        } else {
            //store in local
        }




        if (isChecked_task_priority == "yes") {
            prioritySwitch.isChecked = true
            priority = true
        }

        if (edit_task == "yes") {
            addTaskHead.text = "Edit Task"
            context?.setSharedPref(context!!, "edit_task_assignee_id", assigneeId)
            if (task_dt_edited != "edit_tDate") {
                try {
                    var parser = SimpleDateFormat("MM/dd/yyyy")

                    val myFormat = "MMM dd, yyyy"
                    val sdf = SimpleDateFormat(myFormat)
                    val stDate = sdf.format(parser.parse(startDate))
                    taskDateTime.text =
                        "$stDate at $startTime To $endTime".replace("am", "AM").replace("pm", "PM")
                } catch (e: Exception) {

                }
            } else {
                try {
                    var parser = SimpleDateFormat("MM/dd/yyyy")

                    val myFormat = "MMM dd, yyyy"
                    val sdf = SimpleDateFormat(myFormat)

                    val stDate = sdf.format(parser.parse(startDate))
                    taskDateTime.text =
                        "$stDate at $startTime To $endTime".replace("am", "AM").replace("pm", "PM")

                } catch (e: Exception) {

                }
            }


        } else {
            try {
                var parser = SimpleDateFormat("MM/dd/yyyy")
                val myFormat = "MMM dd, yyyy"
                val sdf = SimpleDateFormat(myFormat)
                val stDate = sdf.format(parser.parse(startDate))
                taskDateTime.text =
                    "$stDate at $startTime To $endTime".replace("am", "AM").replace("pm", "PM")

            } catch (e: Exception) {

            }
        }




        if (assignee != "null") {
            assigneeName.text = "$assignee"
        } else {
            assigneeName.text = "Unassigned"
        }
        try {
            Picasso.get().load(assigneeImg).placeholder(R.drawable.ic_dummy_user_pic)
                .into(assigneeImage)
        } catch (e: Exception) {

        }



        try {
            var parser = SimpleDateFormat("MM/dd/yyyy hh:mm a")
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val stDate = parser.parse("$startDate $startTime")
            var stDateUTC = context?.dateToUTC(context!!, stDate)
            val stDateFormat = sdf.format(stDateUTC)
            val edDate = parser.parse("$endDate $endTime")
            var edDateUTC = context?.dateToUTC(context!!, edDate)
            val edDateFormat = sdf.format(edDateUTC)

            context?.setSharedPref(context!!, "task_start_date", stDateFormat)
            context?.setSharedPref(context!!, "task_end_date", edDateFormat)
        } catch (e: Exception) {

        }





        add_products = mView.findViewById(R.id.add_products_btn)
//        getCurrentAndMyTime()
        addProducts()
        addTaskAssignee()
        addTaskAction()
        taskDateTimeEditAction()
        cancelTaskAction()
        viewRecord()
        descSave()
        offlinetaskUpdate()
        prioritySwitchAction()





        if (edit_task == "yes") {
            addTask.text = "Update"
        }
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<ImageView>(R.id.app_icon_imageView)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility =
            View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private fun offlinetaskUpdate() {


        offline_addTask.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {


                db = AppDatabase.getDatabaseClient(requireContext())
                val accountData = db!!.addTaskDao().getAll()
                for (n in 0 until accountData.size) {


                    var param = HashMap<String, Any>()
                    param.put("accountId", accountData[n].accountId.toString())
                    param.put("departmentId", accountData[n].departmentId.toString())
                    param.put("serviceId", accountData[n].serviceId.toString())
                    param.put("assignedTo","")
                    param.put("startDate", accountData[n].startDate.toString())
                    param.put("endDate", accountData[n].endDate.toString())
                    param.put("taskDescription", accountData[n].taskDescription.toString())
                    param.put("taskStatus", accountData[n].taskStatus.toString())
                    param.put("products", productArray)
                    param.put("priority",false)

                    println("param"+"" + param)
                    context?.let {
                        apiRequest.postRequestBodyWithHeadersAny(
                            it,
                            webService.Add_task_url,
                            param
                        ) { response ->
                            var status = response.getString("status")
                            var msg = response.getString("msg")
                            if (status == "200") {

                                context?.toast(msg)
                                startActivity(Intent(context, MainActivity::class.java))
                                val preferences: SharedPreferences =
                                    PreferenceManager.getDefaultSharedPreferences(context)
                                val editor: SharedPreferences.Editor = preferences.edit()
                                editor.remove("assignee_name")
                                editor.remove("assignee_id")
                                editor.remove("assignee_image")
                                editor.commit()
                                addTask.isEnabled = true
                                //clear sql data
                                val databaseHandler: DatabaseHandler = DatabaseHandler(context!!)
                                databaseHandler.deleteProducts()
                            } else if (status == "500") {
                                context?.toast("Something went wrong")
                                addTask.isEnabled = true
                            } else {
                                addTask.isEnabled = true
                                context?.toast(msg)
                            }
                        }
                    }
                }
            }
        }
    }


    private fun descSave() {
        val LAST_TEXT_DESC = "tDesc"
        val prefDesc = PreferenceManager.getDefaultSharedPreferences(context)
        addTask_description.setText(prefDesc.getString(LAST_TEXT_DESC, ""))
        addTask_description.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable) {
                prefDesc.edit().putString(LAST_TEXT_DESC, s.toString()).commit()
            }
        })
    }

    var productArray = ArrayList<JSONObject>()

    val jsonObject = JSONObject()
    var jsonArray = JSONArray()
    val jsonProducts = JSONObject()
    private fun viewRecord() {
        //creating the instance of DatabaseHandler class
        val databaseHandler: DatabaseHandler = DatabaseHandler(context!!)
        //calling the viewEmployee method of DatabaseHandler class to read the records
        val prod: List<AddProductsModel> = databaseHandler.viewProducts()
        val prodArrayId = Array<String>(prod.size) { "" }
        val prodArrayName = Array<String>(prod.size) { "null" }
        val prodArrayPrice = Array<String>(prod.size) { "null" }
        val prodArrayQty = Array<String>(prod.size) { "null" }
        var index = 0

        for (e in prod) {
            prodArrayId[index] = e.prod_id
            prodArrayName[index] = e.prod_name
            prodArrayPrice[index] = e.prod_price
            prodArrayQty[index] = e.prod_qty
            index++
            jsonObject.put("item", e.prod_name)
            jsonObject.put("price", e.prod_price)
            jsonObject.put("productId", e.prod_id)
            jsonObject.put("quantity", e.prod_qty)
            jsonArray.put(jsonObject)
            productArray.add(jsonObject)
        }

        jsonProducts.put("products", jsonArray)

        val myListAdapter = TaskProductsListAdapter(
            context as Activity,
            prodArrayId,
            prodArrayName,
            prodArrayPrice,
            prodArrayQty
        )
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        addProductsRecyclerView.layoutManager = llm
        addProductsRecyclerView.adapter = myListAdapter


    }


    private fun cancelTaskAction() {
        add_task_back_arrow.setOnClickListener {
            fragmentManager?.popBackStack()
        }
        cancel_addTask.setOnClickListener {
            cancel_addTask.isEnabled = false
            val dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.yes_or_no_alert_view)
            dialog.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
            val title: TextView = dialog.findViewById(R.id.alert_title)
            title.text = "Alert"
            val msg: TextView = dialog.findViewById(R.id.alert_msg)
            msg.text = "Do you want to cancel adding task?"
            val yes = dialog.findViewById<TextView>(R.id.alert_yes)
            yes.setOnClickListener(View.OnClickListener {
                val preferences: SharedPreferences =
                    PreferenceManager.getDefaultSharedPreferences(context)
                val editor: SharedPreferences.Editor = preferences.edit()
                editor.remove("assignee_name")
                editor.remove("assignee_id")
                editor.remove("assignee_image")
                editor.remove("tDesc")
                editor.commit()
                val databaseHandler: DatabaseHandler = DatabaseHandler(context!!)
                databaseHandler.deleteProducts()
                context?.startActivity(Intent(context, MainActivity::class.java))
                dialog.dismiss()
                cancel_addTask.isEnabled = true

            })
            val no = dialog.findViewById(R.id.alert_no) as TextView
            no.setOnClickListener {
                dialog.dismiss()
                cancel_addTask.isEnabled = true
            }
            dialog.show()

        }
    }

    private fun prioritySwitchAction() {
        prioritySwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                context?.setSharedPref(context!!, "isChecked_task_priority", "yes")
                context?.let { ContextCompat.getColor(it, R.color.colorAccent) }?.let {
                    emergency_text.setTextColor(it)
                }
                priority = true
            } else {
                priority = false
                val preferences: SharedPreferences =
                    PreferenceManager.getDefaultSharedPreferences(context)
                val editor: SharedPreferences.Editor = preferences.edit()
                editor.remove("isChecked_task_priority")
                editor.commit()
                context?.let { ContextCompat.getColor(it, R.color.emergency_text_color) }?.let {
                    emergency_text.setTextColor(it)
                }
            }
        }

    }

    private fun taskDateTimeEditAction() {
        taskDateTime.setOnClickListener {
            startDate = context?.getSharedPref("start_date", context!!).toString()
            endDate = context?.getSharedPref("end_date", context!!).toString()
            startTime = context?.getSharedPref("start_time", context!!).toString()
            endTime = context?.getSharedPref("end_time", context!!).toString()

            context?.loadFragment(context!!, SelectDateTimeFragment())

        }
    }

var createdAtTime =  String()
    var updatedAtTime = String()
    private fun addTaskAction() {
        addTask.setOnClickListener {
        createdAtTime = LocalDateTime.now().toString()
            updatedAtTime = LocalDateTime.now().toString()

            it.setEnabled(false)
            Handler().postDelayed(Runnable {
                it.setEnabled(true)
                addTask.isEnabled = true
            }, TIME)
            addTask.isEnabled = false
            if (edit_task == "yes") {
                editTaskApiCall()

            } else {

                if ( requireContext().isNetworkAvailable(requireContext()))
                {

                    AddTaskAsyncTask().execute()
                }
                else
                {
                    AddTaskDb()


                }
                 //
              //  println("Testing_add_task")/
            }


        }
    }

    private fun AddTaskDb() {
        assigneeId = context?.getSharedPref("assignee_id", context!!).toString()
       var set_timezone = context?.getSharedPref("set_timezone", context!!).toString()
        var assignedBy = context?.getSharedPref("userId", context!!).toString()
        val cal = Calendar.getInstance()
        var tz: TimeZone = cal.timeZone
        if (set_timezone=="null"){
            set_timezone = tz.toString()
        }
        var tDesc = addTask_description.text.toString()
        var jsonObjTimeline = JSONObject()
        jsonObjTimeline.put("description", "Task Created")
        jsonObjTimeline.put("type", 1)
        jsonObjTimeline.put("createdBy", assignedBy)
        jsonObjTimeline.put("assignedTo", assigneeId)
        jsonObjTimeline.put("createdAt", createdAtTime)
        jsonObjTimeline.put("updatedAt", updatedAtTime)


        /*"description": "String",
        "type": "1",
        "createdBy": "60e6dc3a438dd8d8e69756de",
        "assignedTo": "60e6dc3a438dd8d8e69756de",
        "createdAt": "2021-07-08T11:06:34.045+00:00",
        "updatedAt": "2021-07-08T11:06:34.045+00:00"*/

     var parser = SimpleDateFormat("MM/dd/yyyy hh:mm a")
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm a")
        val stDate = parser.parse("$startDate $startTime")
        var stDateUTC = context?.dateToUTC(context!!, stDate)
        val stDateFormat = sdf.format(stDateUTC)
        val edDate = parser.parse("$endDate $endTime")
        var edDateUTC = context?.dateToUTC(context!!, edDate)
        val edDateFormat = sdf.format(edDateUTC)
//2020-8-14 10:32 PM
        var desc = addTask_description.text.toString()

        CoroutineScope(Dispatchers.Main).launch {

            db = AppDatabase.getDatabaseClient(requireContext())
            val getName = db!!.dashBoardDateAddDao().showDateList(stDateFormat)


            if (getName.size == 0) {
                db!!.dashBoardDateAddDao().insertAll( DashBoardDateAdd(0,stDateFormat));

            }

            db!!.addTaskDao().insertAll(
                AddTask(
                    0,
                    accountId,
                    deptId,
                    serviceId,
                    assigneeId,
                    stDateFormat,
                    edDateFormat,
                    desc,
                    "Pending",
                    productArray.toString(),
                    priority.toString()
                )
            )


            db!!.salesDao().insertAll(
                DashBoard(
                    0,
                    stDateFormat,
                    "",
                    "Pending",
                    serviceName+" in "+accountName,
                    "NA",
                    "",
                    "",
                    priority.toString(),
                    assigneeId,
                    assigneeName.text.toString(),
                    assigneeImg,
                    deptId,
                   "",
                    stDateFormat,
                    edDateFormat,
                    "task",
                    "",
                    "",
                    "",
                    "",
                 "" ,
                    assigneeImg,
                    assigneeId,
                    "1",
                    "",
                    "",
                    ""

                )
            )

            addTaskTimelineDb(accountId,deptId,serviceId,assignedBy,assignedBy,assigneeId,
            "",startDate,endDate,tDesc,"Pending",productArray.toString(),
            set_timezone,createdAtTime,updatedAtTime,jsonObjTimeline.toString(),"","","",
            "","","","","","","")

            context?.toast("Task inserted successfully")
            startActivity(Intent(context, MainActivity::class.java))

        }
    }

    private fun addTaskAssignee() {
        assignee_change_layout.setOnClickListener {
            assigneeName.performClick()
        }
        assigneeName.setOnClickListener {
            try {
                var parser = SimpleDateFormat("MM/dd/yyyy hh:mm a")
                val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                val stDate = parser.parse("$startDate $startTime")
                var stDateUTC = context?.dateToUTC(context!!, stDate)
                val stDateFormat = sdf.format(stDateUTC)
                val edDate = parser.parse("$endDate $endTime")
                var edDateUTC = context?.dateToUTC(context!!, edDate)
                val edDateFormat = sdf.format(edDateUTC)

                context?.setSharedPref(context!!, "task_start_date", stDateFormat)
                context?.setSharedPref(context!!, "task_end_date", edDateFormat)
            } catch (e: Exception) {

            }
            context?.loadFragment(context!!, AddTaskAssigneeFragment())
//            context?.loadFragment(context!!,AddTaskAssigneeFragment())
        }
    }

    private fun addProducts() {

        add_products.setOnClickListener {
            context?.loadFragment(context!!, AddProductFragment())
        }
    }


    inner class AddTaskAsyncTask() : AsyncTask<String, String, String>() {
        @SuppressLint("WrongThread")
        override fun doInBackground(vararg params: String?): String {
            if (assigneeId == "null") {
                assigneeId = ""
            }
            var parser = SimpleDateFormat("MM/dd/yyyy hh:mm a")
            val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm a")
            val stDate = parser.parse("$startDate $startTime")
            var stDateUTC = context?.dateToUTC(context!!, stDate)
            val stDateFormat = sdf.format(stDateUTC)
            val edDate = parser.parse("$endDate $endTime")
            var edDateUTC = context?.dateToUTC(context!!, edDate)
            val edDateFormat = sdf.format(edDateUTC)
//2020-8-14 10:32 PM
            var desc = addTask_description.text.toString()

            var param = HashMap<String, Any>()
            if (productArray == null) {
                param.put("accountId", accountId)
                param.put("departmentId", deptId)
                param.put("serviceId", serviceId)
                param.put("assignedTo", assigneeId)
                param.put("startDate", stDateFormat)
                param.put("endDate", edDateFormat)
                param.put("taskDescription", desc)
                param.put("taskStatus", "Pending")
                param.put("products", productArray)
                param.put("priority", priority.toString())

            } else {
                param.put("accountId", accountId)
                param.put("departmentId", deptId)
                param.put("serviceId", serviceId)
                param.put("assignedTo", assigneeId)
                param.put("startDate", stDateFormat)
                param.put("endDate", edDateFormat)
                param.put("taskDescription", desc)
                param.put("taskStatus", "Pending")
                param.put("products", productArray)
                param.put("priority", priority.toString())
            }
            println("param"+"" + param)

            Log.i("paramAddTask",param.toString())

            context?.let {
                apiRequest.postRequestBodyWithHeadersAny(
                    it,
                    webService.Add_task_url,
                    param
                ) { response ->
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200") {
                        context?.toast(msg)
                        val preferences: SharedPreferences =
                            PreferenceManager.getDefaultSharedPreferences(context)
                        val editor: SharedPreferences.Editor = preferences.edit()
                        editor.remove("assignee_name")
                        editor.remove("assignee_id")
                        editor.remove("assignee_image")
                        editor.commit()
                        addTask.isEnabled = true
                        //clear sql data
                        val databaseHandler: DatabaseHandler = DatabaseHandler(context!!)
                        databaseHandler.deleteProducts()
                        startActivity(Intent(context, MainActivity::class.java))
                    } else if (status == "500") {
                        context?.toast("Something went wrong")
                        addTask.isEnabled = true
                    } else {
                        addTask.isEnabled = true
                        context?.toast(msg)
                    }
                }
            }

            return ""
        }

    }


    private fun editTaskApiCall() {
        accountId = context?.getSharedPref("acc_filter_id", context!!).toString()
        accountName = context?.getSharedPref("acc_filter_name", context!!).toString()
        deptId = context?.getSharedPref("dept_filter_id", context!!).toString()
        deptName = context?.getSharedPref("dept_filter_name", context!!).toString()
        serviceId = context?.getSharedPref("service_filter_id", context!!).toString()
        serviceName = context?.getSharedPref("service_filter_name", context!!).toString()
        startDate = context?.getSharedPref("start_date", context!!).toString()
        endDate = context?.getSharedPref("end_date", context!!).toString()
        startTime = context?.getSharedPref("start_time", context!!).toString()
        endTime = context?.getSharedPref("end_time", context!!).toString()

        edit_task = context?.getSharedPref("edit_task", context!!).toString()
        edit_task_taskId = context?.getSharedPref("edit_task_taskId", context!!).toString()


        assignee = context?.getSharedPref("assignee_name", context!!).toString()
        assigneeId = context?.getSharedPref("assignee_id", context!!).toString()
        assigneeImg = context?.getSharedPref("assignee_image", context!!).toString()




        if (assigneeId == "null") {
            assigneeId = ""
        }
        var parser = SimpleDateFormat("MM/dd/yyyy hh:mm a")
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm a")
        val stDate = parser.parse("$startDate $startTime")
        var stDateUTC = context?.dateToUTC(context!!, stDate)
        val stDateFormat = sdf.format(stDateUTC)
        val edDate = parser.parse("$endDate $endTime")
        var edDateUTC = context?.dateToUTC(context!!, edDate)
        val edDateFormat = sdf.format(edDateUTC)


//2020-8-14 10:32 PM
        var desc = addTask_description.text.toString()

        var param = HashMap<String, Any>()
        if (productArray == null) {
            param["id"] = edit_task_taskId
            param.put("accountId", accountId)
            param.put("departmentId", deptId)
            param.put("serviceId", serviceId)
            param.put("assignedTo", assigneeId)
            param.put("startDate", stDateFormat)
            param.put("endDate", edDateFormat)
            param.put("taskDescription", desc)
            param.put("taskStatus", "Pending")
            param.put("products", productArray)
            param.put("priority", priority.toString())
        } else {
            param["id"] = edit_task_taskId
            param.put("accountId", accountId)
            param.put("departmentId", deptId)
            param.put("serviceId", serviceId)
            if (assigneeId != "") {
                param.put("assignedTo", assigneeId)
            }
            param.put("startDate", stDateFormat)
            param.put("endDate", edDateFormat)
            param.put("taskDescription", desc)
            param.put("taskStatus", "Pending")
            param.put("products", productArray)
            param.put("priority", priority)
        }

        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(
                it,
                webService.Edit_task_url,
                param
            ) { response ->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200") {

                        context?.toast(msg)
                        startActivity(Intent(context, MainActivity::class.java))
                        val preferences: SharedPreferences =
                            PreferenceManager.getDefaultSharedPreferences(context)
                        val editor: SharedPreferences.Editor = preferences.edit()
                        editor.remove("assignee_name")
                        editor.remove("assignee_id")
                        editor.remove("assignee_image")
                        editor.remove("edit_task")
                        editor.commit()
                        addTask.isEnabled = true
                        //clear sql data
                        val databaseHandler: DatabaseHandler = DatabaseHandler(context!!)
                        databaseHandler.deleteProducts()
                    } else if (status == "500") {
                        context?.toast("Something went wrong")
                        addTask.isEnabled = true
                    } else {
                        context?.toast(msg)
                        addTask.isEnabled = true
                    }

                } catch (e: java.lang.Exception) {
                    context?.toast(e.toString())
                } finally {
                    addTask.isEnabled = true
                }
            }
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

    //add task #task created timeline
    private fun addTaskTimelineDb(accID:String,deptID:String,serviceID:String,assignedBY:String,
    createdBY:String,assignedTO:String,reccurOption:String,startDATE:String,endDATE:String,
    taskDESC:String,taskSTATUS:String,prod:String,tz:String,createdAT:String,updateAT:String,
                                  t1:String,t2:String,t3:String,
                                  t4:String,t5:String,t6:String,t7:String,t8:String,t9:String,
                                  t10:String,t11:String){
        var cID =   context?.getSharedPref( "companyId", context!!).toString()
        CoroutineScope(Dispatchers.IO).launch {
            db = AppDatabase.getDatabaseClient(requireContext())
            db!!.addTaskTimelineDao().insertAll(AddTaskTimeline(0,cID
            ,accID,deptID,serviceID,assignedBY
            ,createdBY,assignedTO,reccurOption,
            startDATE,endDATE,taskDESC,
            taskSTATUS,prod,tz,createdAT,updateAT,
            t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11));
        }
    }

}
package com.salesc2.fragment

import android.app.Dialog
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.util.PatternsCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.salesc2.R
import com.salesc2.adapters.BillingCardListAdapter
import com.salesc2.data.network.api.request.RequestBillingDetails
import com.salesc2.data.viewmodel.BillingDetailsViewModel
import com.salesc2.data.viewmodel.MorePageViewModel
import com.salesc2.model.BillingCardListModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import org.koin.androidx.viewmodel.ext.android.viewModel


class BillingDetailsFragment:Fragment(),HideShowCardSelection {
    var hideShowRecyclerListCardSelection : HideShowRecyclerListCardSelection? = null

    private var keywordList = mutableListOf<String>()

    private lateinit var billing_details_back_arrow : ImageView
    private lateinit var card_type : TextView
    private lateinit var bd_card_edit : TextView
    private lateinit var card_num_dots : TextView
    private lateinit var cardHolder_name : TextView
    private lateinit var cardExpire_date : TextView
    private lateinit var billingPeriod : TextView
    private lateinit var billingPeriodSwitch : TextView
    private lateinit var bd_tech_contactImage : CircleImageView
    private lateinit var bd_tech_contactName : TextView
    private lateinit var bd_tech_contactEmail : TextView
    private lateinit var imageLoc_bd : CircleImageView
    private lateinit var bd_loc_name : TextView
    private lateinit var bd_area_name : TextView
    private lateinit var bd_editDetails : TextView
    private lateinit var bd_company_name : TextView
    private lateinit var bd_company_address : TextView
    private lateinit var bd_vat_gst_in : TextView
    private lateinit var bd_manage_contact : TextView
    private lateinit var bd_invoice_emails : EditText
    private lateinit var etKeywords : EditText
    private lateinit var bd_saveEmails : Button
    private lateinit var submit_email : Button

    private lateinit var cgTag : ChipGroup
    private var editCard = String()

    private var billingCardListModel = ArrayList<BillingCardListModel>()
    var selectAddCard : ImageView? = null
    private var billingId = String()
    private var subscriptionId = String()
    private var webService = WebService()
   private var apiRequest = ApiRequest()
    private var pid = String()
    private val billingDetailsViewModel by viewModel<BillingDetailsViewModel>()

    var emailsArray = ArrayList<String>()
    private lateinit var mView:View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_billing_details, container, false)
        billingId = context?.getSharedPref("billingId",context!!).toString()
        subscriptionId = context?.getSharedPref("subscriptionId",context!!).toString()
        editCard = context?.getSharedPref("editCard",context!!).toString()
        billing_details_back_arrow = mView.findViewById(R.id.billing_details_back_arrow)
        card_type = mView.findViewById(R.id.card_type)
        bd_card_edit = mView.findViewById(R.id.bd_card_edit)
        card_num_dots = mView.findViewById(R.id.card_num_dots)
        cardHolder_name = mView.findViewById(R.id.cardHolder_name)
        cardExpire_date = mView.findViewById(R.id.cardExpire_date)
        billingPeriod = mView.findViewById(R.id.billingPeriod)
        billingPeriodSwitch = mView.findViewById(R.id.billingPeriodSwitch)
        bd_tech_contactImage = mView.findViewById(R.id.bd_tech_contactImage)
        bd_tech_contactName = mView.findViewById(R.id.bd_tech_contactName)
        bd_tech_contactEmail = mView.findViewById(R.id.bd_tech_contactEmail)
        imageLoc_bd = mView.findViewById(R.id.imageLoc_bd)
        bd_loc_name = mView.findViewById(R.id.bd_loc_name)
        bd_area_name = mView.findViewById(R.id.bd_area_name)
        bd_editDetails = mView.findViewById(R.id.bd_editDetails)
        bd_company_name = mView.findViewById(R.id.bd_company_name)
        bd_company_address = mView.findViewById(R.id.bd_company_address)
        bd_vat_gst_in = mView.findViewById(R.id.bd_vat_gst_in)
        bd_manage_contact = mView.findViewById(R.id.bd_manage_contact)
        bd_invoice_emails = mView.findViewById(R.id.bd_invoice_emails)

        etKeywords = mView.findViewById(R.id.etKeywords)
        bd_saveEmails = mView.findViewById(R.id.bd_saveEmails)
        submit_email  = mView.findViewById(R.id.submit_email)
        cgTag  = mView.findViewById(R.id.cgTag)
        billing_details_back_arrow = mView.findViewById(R.id.billing_details_back_arrow)
        billingCardListModel = ArrayList()
        emailsArray = ArrayList()
//        if (editCard == "yes"){
//            (context as MainActivity).billingCardListingPopUp(context as MainActivity,billingId)
//        }
        billingDetailsApiCall()
        billingDetailsApiCallwithMVVM()
        companySubDetailsApiCall()
        companySubDetailsApiCallwithmvvm()
        onClickActions()


        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor : SharedPreferences.Editor = preferences.edit()
        editor.remove("editCard")
        editor.remove("addCard")
        editor.commit()
        return mView
    }

    private fun onClickActions(){
        billing_details_back_arrow.setOnClickListener{
          context?.loadFragment(context!!,BillingOverviewFragment())
        }
        bd_card_edit.setOnClickListener{
            context?.setSharedPref(context!!,"paymentMethodId",pid)
            context?.setSharedPref(context!!,"fromPage","BillingDetails")
          billingCardListingPopUp(billingId)
        }
        billingPeriodSwitch.setOnClickListener{
            switchBillingCycleApiCall()
        }
        bd_saveEmails.setOnClickListener{
            updateInvoiceCcEmailsApiCall()
        }

        submit_email.setOnClickListener{
            //updateInvoiceCcEmailsApiCall()
            addChipToGroup(bd_invoice_emails.text.toString())
        }

        bd_invoice_emails.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action === KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                if (PatternsCompat.EMAIL_ADDRESS.matcher(bd_invoice_emails.text.toString()).matches().equals(true)){
//                    emailsArray.add(bd_invoice_emails.text.toString())
                    addChipToGroup(bd_invoice_emails.text.toString())

                }else{
                    context?.toast("Enter a valid email")
                }

                return@OnKeyListener true
            }
            false
        })
        bd_manage_contact.setOnClickListener{
            context?.setSharedPref(context!!,"bcFrom","BillingDetails")
            context?.loadFragment(context!!,ConfigureBillingFragment())
        }
        bd_editDetails.setOnClickListener{
            context?.setSharedPref(context!!,"bcFrom","BillingDetails")
            context?.loadFragment(context!!,ConfigureBillingFragment())
        }

    }



    private fun updateInvoiceCcEmailsApiCall(){
        if (emailsArray.size <1){
            context?.toast("Please add email")
        }else{
            var params = HashMap<String,Any>()
            params["billingId"] = billingId
            params["invoiceCC"] = emailsArray
            context?.let {
                apiRequest.postRequestBodyWithHeadersAny(it,webService.update_invoice_url,params){ response->
                    try {
                        var status = response.getString("status")
                        var msg = response.getString("msg")
                        if (status == "200"){
                            context?.toast(msg)
                            bd_invoice_emails.text.clear()
                            cgTag.removeAllViews()
                            keywordList.clear()
                        }else{
                            context?.toast(msg)
                        }


                    }catch (e:Exception){

                    }
                }
            }
        }

    }

    private fun addChipToGroup(keyword: String) {
        if (keyword.contains("#")) {
            val parts = keyword.split(",")
            val removedEmpty = parts.filter { it != "" }
            removedEmpty.forEach {
                val removedComma = it.removeSuffix(",")
                if (keywordList.size < 5) {
                    keywordList.add(removedComma)
                    createChips(removedComma)


                } else
                    activity?.toast("testing")
            }
        } else {
            keywordList.add(keyword)
            emailsArray.add(keyword)
            createChips(keyword)
        }
    }
    private fun createChips(keyWords: String) {
        val chip = Chip(context)
        chip.text = "$keyWords"
        chip.isCloseIconVisible = true
        chip.isClickable = true
        chip.isCheckable = false
        cgTag.addView(chip as View)
        chip.setOnCloseIconClickListener {
            val new = chip.text.removePrefix("#")
            keywordList.remove(new)
            cgTag.removeView(chip as View)
            emailsArray.remove(keyWords)

        }
        bd_invoice_emails.setText("")
    }



    private fun switchBillingCycleApiCall(){
        var billingCycle = context?.getSharedPref("billingCycle",context!!).toString()

        val params = HashMap<String,Any>()
        params["billingId"] = billingId
        params["subscriptionId"] = subscriptionId
        if (billingCycle == "0"){
            params["billingCycle"] = 1

        }else{
            params["billingCycle"] = 0

        }

        context?.let {
            apiRequest.putRequestBodyWithHeadersAny(it,webService.switch_billing_cycle_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        var billingCycle = context?.getSharedPref("billingCycle",context!!).toString()
                        context?.toast(msg)
                        if(billingCycle == "0"){
                            billingPeriod.text = "Monthly"
                            billingPeriodSwitch.text = "Switch to Annual"
                            context?.setSharedPref(context!!,"billingCycle","0")
                        }else{
                            billingPeriod.text = "Annual"
                            billingPeriodSwitch.text = "Switch to Monthly"
                            context?.setSharedPref(context!!,"billingCycle","1")
                        }
                    }else{
                        context?.toast(msg)
                    }
                }catch (e:Exception){
                }
            }
        }
    }
fun companySubDetailsApiCallwithmvvm()
{
    billingDetailsViewModel.reqBillingCompanySubDetails(RequestBillingDetails(
        subscriptionId = subscriptionId
    ),{
        println("Billing_details_new"+" "+it)
    },{

    })
}

    private fun companySubDetailsApiCall(){
        val params = HashMap<String,Any>()
        params["subscriptionId"] = subscriptionId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.company_subscription_details,params){ response->
                try {

                    println("Billing_details_new"+" "+webService.company_subscription_details)

                    var status = response.getString("status")
                    if (status == "200"){
                        println("Billing_details_new"+" "+response)

                        var dataobj = response.getJSONObject("data")
                        var name = dataobj.getString("name")
                        var logo = dataobj.getString("logo")
                        var type = dataobj.getString("type")
                        var level = dataobj.getString("level")
                        var role = dataobj.getString("role")
                        try {
                            Picasso.get().load(webService.imageBasePath+logo).placeholder(R.drawable.ic_be_location).into(imageLoc_bd)
                        }catch (e:Exception){

                        }
                        if (type == "1" || type == "3"){
                            bd_loc_name.text = name
                            if (role.equals("Sales Rep",ignoreCase = true)){
                                bd_area_name.text  = "Representative"
                            }else if (role.equals("Team Lead",ignoreCase = true)){
                                bd_area_name.text ="Regional Manager"
                            }else{
                                bd_area_name.text = role
                            }
                        }else{
                            bd_loc_name.text = name
                            bd_area_name.text = level
                        }

                    }
                }catch (e:Exception){

                }

            }
        }
    }

fun billingDetailsApiCallwithMVVM()
{
    billingDetailsViewModel.reqBillingDetails(RequestBillingDetails(
        subscriptionId = subscriptionId
    ),{
        println("Billing_details"+" "+it)

    },{
        println("Billing_details"+" "+it)

    })
}
    private fun billingDetailsApiCall(){
      val params = HashMap<String,Any>()
        params["subscriptionId"] = subscriptionId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.billing_details_url,params){ response->

                println("Billing_details"+" "+webService.billing_details_url)
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")

                    if (status == "200"){
                        println("Billing_details"+" "+response)

                        var dataArr = response.getJSONArray("data")
                        for (i in 0 until dataArr.length()){
                            var dataObj = dataArr.getJSONObject(i)
                            var _id = dataObj.getString("_id")
                            var price = dataObj.getString("price")
                            var type = dataObj.getString("type")
                            var status = dataObj.getString("status")

                            var billingContactObj = dataObj.getJSONObject("billingContact")
                            var cardDetailObj = billingContactObj.getJSONObject("cardDetail")
                            var lastDigit = cardDetailObj.getString("lastDigit")
                            card_num_dots.text = "XXXX   XXXX   XXXX   $lastDigit"
                            context?.setSharedPref(context!!,"lastdigit_card",lastDigit)
                            var cardHolder = cardDetailObj.getString("cardHolder")
                            cardHolder_name.text = cardHolder
                            context?.setSharedPref(context!!,"cardholder_card",cardHolder)
                            var expire = cardDetailObj.getString("expire")
                            cardExpire_date.text = "Exp $expire"
                            context?.setSharedPref(context!!,"expire_card",cardHolder)

                            var billingDetailObj = billingContactObj.getJSONObject("billingDetail")
                            var b_price = billingDetailObj.getString("price")
                            var billingCycle = billingDetailObj.getString("billingCycle")
                            context?.setSharedPref(context!!,"billingCycle",billingCycle)
                            if(billingCycle == "0"){
                                billingPeriod.text = "Monthly"
                                billingPeriodSwitch.text = "Switch to Annual"
                            }else{
                                billingPeriod.text = "Annual"
                                billingPeriodSwitch.text = "Switch to Monthly"
                            }
                            var estimatedBill = billingDetailObj.getString("estimatedBill")
                            var estimatedQuantity = billingDetailObj.getString("estimatedQuantity")
                            var countOfActiveUsers = billingDetailObj.getString("countOfActiveUsers")
                            var billingStartDate = billingDetailObj.getString("billingStartDate")
                            var billingEndDate = billingDetailObj.getString("billingEndDate")

                            var invoiceCCArr = billingContactObj.getJSONArray("invoiceCC")


                            var b_status = billingContactObj.getString("status")
                            var b_id = billingContactObj.getString("_id")
                            context?.setSharedPref(context!!,"billingId",b_id)
                            var firstName = billingContactObj.getString("firstName")
                            var lastName = ""
                            if (billingContactObj.has("lastName"))
                            {
                                 lastName = billingContactObj.getString("lastName")
                            }
                            bd_tech_contactName.text = "$firstName $lastName"
                            var countryCode = billingContactObj.getString("countryCode")
                            var phone = billingContactObj.getString("phone")
                            var email = billingContactObj.getString("email")
                            bd_tech_contactEmail.text = email
                            var companyName = billingContactObj.getString("companyName")
                            bd_company_name.text = companyName
                            var subscriptionSettingsId = billingContactObj.getString("subscriptionSettingsId")
                            var createdBy = billingContactObj.getString("createdBy")
                            var customerId = billingContactObj.getString("customerId")
                            var paymentMethodId = billingContactObj.getString("paymentMethodId")
                            context?.setSharedPref(context!!,"paymentMethodId",paymentMethodId)
                            pid= paymentMethodId
                            var subscriptionId = billingContactObj.getString("subscriptionId")
                            var addresLine1 = billingContactObj.getString("addresLine1")
                            var addressLine2 = ""
                                if (billingContactObj.has("addressLine2")){
                                addressLine2 = billingContactObj.getString("addressLine2")
                            }

                            var city = billingContactObj.getString("city")
                            var state = billingContactObj.getString("state")
                            var country = billingContactObj.getString("country")
                            var zipCode = billingContactObj.getString("zipCode")
                            bd_company_address.text ="${context?.capFirstLetter(addresLine1)} ${context?.capFirstLetter(addressLine2)}, ${context?.capFirstLetter(city)}, ${context?.capFirstLetter(state)}, ${context?.capFirstLetter(country)} $zipCode"
                            var createdAt = billingContactObj.getString("createdAt")
                            var updatedAt = billingContactObj.getString("updatedAt")
                            var __v = billingContactObj.getString("__v")
                            bd_vat_gst_in.text = __v


                            context?.setSharedPref(context!!,"bc_fName",firstName)
                            context?.setSharedPref(context!!,"bc_lName",lastName)
                            context?.setSharedPref(context!!,"bc_phone",phone)
                            context?.setSharedPref(context!!,"bc_email",email)
                            context?.setSharedPref(context!!,"bc_company",companyName)
                            context?.setSharedPref(context!!,"bc_b_period", billingPeriod.text.toString())
                            context?.setSharedPref(context!!,"bc_address1",addresLine1)
                            context?.setSharedPref(context!!,"bc_address2",addressLine2)
                            context?.setSharedPref(context!!,"bc_country",country)
                            context?.setSharedPref(context!!,"bc_state",state)
                            context?.setSharedPref(context!!,"bc_city",city)
                            context?.setSharedPref(context!!,"bc_zip",zipCode)

                        }
                    }

                }catch (e:Exception){

                }
            }
        }
    }


    private var lastdigit_card = String()
    private var cardholder_card = String()
    private var expire_card = String()
    private var paymentMethodId = String()
    var cardList_recyclerView : RecyclerView? = null
    var fromPage = String()
   private var mBottomSheetDialog : Dialog? = null
    private fun billingCardListingPopUp(billingId: String){
        fromPage = context?.getSharedPref("fromPage",context!!).toString()
        billingCardListModel = ArrayList()
        mBottomSheetDialog =
            context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) };
        mBottomSheetDialog?.setContentView(R.layout.popup_card_list)
        cardList_recyclerView = mBottomSheetDialog?.findViewById<RecyclerView>(R.id.cardList_recyclerView)
        var cardList_continue = mBottomSheetDialog?.findViewById<TextView>(R.id.cardList_continue)
        var cardListClose: ImageView? = mBottomSheetDialog?.findViewById(R.id.cardListClose)
        selectAddCard   = mBottomSheetDialog?.findViewById(R.id.selectAddCard)!!
        getCardListApiCall(billingId,"yes")
        cardListClose?.setOnClickListener{
            mBottomSheetDialog?.dismiss()
        }
        selectAddCard?.setOnClickListener{
            getCardListApiCall(billingId,"no")
            selectAddCard?.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_blue_circle))
            context?.setSharedPref(context!!,"addCard","yes")
            hideShowRecyclerListCardSelection?.hideRecyclerSelection()
        }
        cardList_continue?.setOnClickListener{
            var  addCard = context?.getSharedPref("addCard",context!!).toString()

            if (addCard == "yes"){

                context?.loadFragment(context!!,AddCardFragment())
                mBottomSheetDialog?.dismiss()
            }else{
                if (fromPage == "DashBoard"){
                    reSubscribeApiCall()
                    mBottomSheetDialog?.dismiss()
                }else{
                    lastdigit_card = context?.getSharedPref("lastdigit_card",context!!).toString()
                    cardholder_card = context?.getSharedPref("cardholder_card",context!!).toString()
                    expire_card = context?.getSharedPref("expire_card",context!!).toString()
                    paymentMethodId = context?.getSharedPref("paymentMethodId",context!!).toString()
                    updateCardDetailsApiCall(billingId,cardholder_card,expire_card,lastdigit_card,paymentMethodId)
                    mBottomSheetDialog?.dismiss()
                }
            }


        }
        mBottomSheetDialog?.setCancelable(true)
        mBottomSheetDialog?.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT)
        mBottomSheetDialog?.window?.setGravity(Gravity.CENTER)
        mBottomSheetDialog?.show()
    }

    private fun getCardListApiCall(billingId: String,selection:String){
        val params = HashMap<String,Any>()
        params["billingId"] = billingId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.get_customer_card_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){
                        billingCardListModel.clear()
                        billingCardListModel = ArrayList()
                        var cardDetailsArr = response.getJSONArray("cardDetails")

                        for (i in 0 until cardDetailsArr.length()){
                            var cardDetailsObj = cardDetailsArr.getJSONObject(i)
                            var paymentMethodId = cardDetailsObj.getString("paymentMethodId")
                            var exp_year = cardDetailsObj.getString("exp_year")
                            var exp_month = cardDetailsObj.getString("exp_month")
                            var last4 = cardDetailsObj.getString("last4")
                            var name = cardDetailsObj.getString("name")
                            var brand = cardDetailsObj.getString("brand")
                            billingCardListModel.add(BillingCardListModel(paymentMethodId, exp_year, exp_month, last4, name, brand,selection))
                        }
                    }
                    setupBillingCardListRecyclerView(billingCardListModel)

                }catch (e:Exception){

                }

            }
        }

    }
    private fun setupBillingCardListRecyclerView(billingCardListModel: ArrayList<BillingCardListModel>) {
        val adapter: RecyclerView.Adapter<*> = BillingCardListAdapter(context!!,billingCardListModel,this)
        val llm = LinearLayoutManager(context!!)
        llm.orientation = LinearLayoutManager.VERTICAL
        cardList_recyclerView?.layoutManager = llm
        cardList_recyclerView?.adapter = adapter
    }

    private fun updateCardDetailsApiCall(billingId:String,cardHolder:String,expire:String,lastDigit:String,paymentMethodId:String){
        val params = HashMap<String,Any>()
        params["billingId"] = billingId
        params["cardHolder"] = cardHolder
        params["expire"] = expire
        params["lastDigit"] = lastDigit.toInt()
        params["paymentMethodId"] = paymentMethodId
        context?.let {
            apiRequest.putRequestBodyWithHeadersAny(it,webService.card_details_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        context?.toast(msg)
                        context?.loadFragment(context!!,BillingDetailsFragment())
                    }else{
                        context?.toast(msg)
                    }

                }catch (e:Exception){

                }
            }
        }
    }

    override fun showSelection() {
        selectAddCard?.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_blue_circle))
    }

    override fun hideSelection() {
        selectAddCard?.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_grey_circle))
    }

    override fun refreshCardList() {
        getCardListApiCall(billingId,"yes")
    }

    private fun reSubscribeApiCall(){
        paymentMethodId = context?.getSharedPref("paymentMethodId",context!!).toString()
        var params = HashMap<String,Any>()
        params["type"] = 1
        params["paymentMethodId"] = paymentMethodId
        apiRequest.postRequestBodyWithHeadersAny(context!!,webService.re_subscribe_url,params){ response->
            try{
                var status = response.getString("status")
                var msg = response.getString("msg")
                if (status == "200"){
                    context?.toast(msg)
                    activity?.findViewById<RelativeLayout>(R.id.coordinatorlayout)?.visibility = View.VISIBLE
                }
                else{
                    context?.toast(msg)
                }
            }catch (e:Exception){

            }
        }
    }
}
package com.salesc2.data.network.api.service



import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestBillingDetails
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseBillingCompanySubdetails
import com.salesc2.data.network.api.response.ResponseBillingDetails
import com.salesc2.data.network.api.response.ResponseTeamList
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface BillingDetailsApi {

    @POST("subscription/api/v1/billingDetails")
    suspend fun reqBillingDetails(
        @Body requestBillingDetails: RequestBillingDetails
    ): Response<ResponseBillingDetails>

    @POST("company/api/v1/company/subscription/details")
    suspend fun reqBillingCompanySubDetails(
        @Body requestBillingDetails: RequestBillingDetails
    ): Response<ResponseBillingCompanySubdetails>
}
package com.salesc2.adapters

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.fragment.AddTaskFragment
import com.salesc2.fragment.SelectDepartmentFragment
import com.salesc2.model.AddTaskAssigneeModel
import com.salesc2.model.SelectAccountModel
import com.salesc2.model.TeamMemberListModel
import com.salesc2.utils.ShowNoData
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.setSharedPref
import com.squareup.picasso.Picasso
import org.w3c.dom.Text
import java.util.*

class AddTaskAssigneeAdapter(context: Context?, var showdata: ShowNoData, var taskAssigneeList: ArrayList<AddTaskAssigneeModel>) : RecyclerView.Adapter<AddTaskAssigneeAdapter.viewHolder>(),Filterable {

    private lateinit var taskAssigneeItems: ArrayList<AddTaskAssigneeModel>
    var context: Context? = null
 private  var row_index = -1
    var resultCount:Int = 0
    private var addTasK_assignee_position = String()
    init{
        this.context = context
        taskAssigneeItems = taskAssigneeList

    }
    var edit_task_assignee_id = String()
      private var  edit_task = String()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_add_task_assignee, parent, false)
        addTasK_assignee_position = context?.getSharedPref("addTasK_assignee_position",context!!).toString()
        edit_task_assignee_id = context?.getSharedPref("edit_task_assignee_id",context!!).toString()
        edit_task = context?.getSharedPref("edit_task",context!!).toString()
        return viewHolder(view)
    }

    override fun getItemCount(): Int {
       return taskAssigneeItems.size
    }

    var checked = true
    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        var addTaskAssigneeModel : AddTaskAssigneeModel = taskAssigneeItems[position]
        holder.taskAssigneeName.text = addTaskAssigneeModel.assignee_name
        addTasK_assignee_position = context?.getSharedPref("addTasK_assignee_position",context!!).toString()

        if (!addTaskAssigneeModel.assignee_image.equals("")){
            Picasso.get().load(addTaskAssigneeModel.assignee_image)
                .placeholder(R.drawable.ic_dummy_user_pic).into(holder.taskAssigneeImage)
        }
        else{
            holder.taskAssigneeImage.setBackgroundResource(R.drawable.ic_dummy_user_pic)
        }
        if (addTasK_assignee_position != "null"){
            row_index=addTasK_assignee_position.toInt()
            if (row_index.equals(addTasK_assignee_position)){
                holder.select_icon.visibility= View.VISIBLE
            }
        }

        if (edit_task_assignee_id.equals(addTaskAssigneeModel.assignee_id) && edit_task == "yes"){
            context?.setSharedPref(context!!,"addTasK_assignee_position",position.toString())
            addTasK_assignee_position = context?.getSharedPref("addTasK_assignee_position",context!!).toString()
            if (addTasK_assignee_position != "null"){
                row_index=addTasK_assignee_position.toInt()
                if (row_index.equals(addTasK_assignee_position)){
                    holder.select_icon.visibility= View.VISIBLE
                }
            }
        }
        holder.itemView.setOnClickListener(View.OnClickListener {

            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("addTasK_assignee_position")
            editor.commit()
            row_index=position
            notifyDataSetChanged()


            context?.setSharedPref(context!!,"assignee_name",addTaskAssigneeModel.assignee_name)
            context?.setSharedPref(context!!,"assignee_image",addTaskAssigneeModel.assignee_image)
            context?.setSharedPref(context!!,"assignee_id",addTaskAssigneeModel.assignee_id)

            var fragment = AddTaskFragment()
            val activity = context as AppCompatActivity
            activity.supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragmet_container, fragment).addToBackStack(null).commit()

        })
        if(row_index==position){
            context?.setSharedPref(context!!,"addTasK_assignee_position",position.toString())
            holder.select_icon.visibility= View.VISIBLE

        }
        else
        {
            holder.select_icon.visibility= View.GONE
            context?.let { ContextCompat.getColor(it, R.color.filter_left_menu_bg) }?.let {
                holder.taskAssigneeName.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.light_grey) }?.let {
                holder.taskAssigneeName.setBackgroundColor(it)
            }

        }

    }
    class viewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView) {
       var taskAssigneeName : TextView = itemView.findViewById(R.id.task_assignee_name)
        var taskAssigneeImage : ImageView = itemView.findViewById(R.id.task_assignee_image)
        var assignee_layout : ConstraintLayout = itemView.findViewById(R.id.assignee_layout)
        var select_icon : ImageView = itemView.findViewById(R.id.select_icon)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    taskAssigneeItems = taskAssigneeList

                } else {
                    val resultList = ArrayList<AddTaskAssigneeModel>()
                    for (row in taskAssigneeList) {
                        if (row.assignee_name.toLowerCase(Locale.ROOT)
                                .contains(charSearch.toLowerCase(Locale.ROOT))
                        ) {

                            resultList.add(row)
                        }

                    }
                    taskAssigneeItems = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = taskAssigneeItems
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                taskAssigneeItems = results?.values as ArrayList<AddTaskAssigneeModel>

                if (taskAssigneeItems.size==0) {
                    showdata.showNoData()

                }
                else
                {
                    showdata.hideNoData()
                    notifyDataSetChanged()
                    resultCount = results.count.toInt()
                }

            }



        }
    }
}
package com.salesc2.data.network.api.service

import com.salesc2.data.network.api.response.ResponseTimeZone
import retrofit2.Response
import retrofit2.http.GET

interface TimeZoneApi {
    @GET("ios-user/api/v1/employee/timezone")
    suspend fun reqTimeZone(): Response<ResponseTimeZone>
}
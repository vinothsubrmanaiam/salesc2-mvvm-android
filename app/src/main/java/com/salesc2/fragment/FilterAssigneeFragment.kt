package com.salesc2.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.adapters.AddTaskAssigneeAdapter
import com.salesc2.adapters.FilterAssigneeAdapter
import com.salesc2.model.AddTaskAssigneeModel
import com.salesc2.model.FilterAssigneeModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.progressBarDismiss

//not in use
class FilterAssigneeFragment: Fragment() {

    var webService = WebService()
    var apiRequest = ApiRequest()
   private lateinit var assigneeSearch : SearchView
   private lateinit var assigneeListRecyclerView: RecyclerView
   private lateinit var filterAssigneeModel: ArrayList<FilterAssigneeModel>
    private lateinit var mView : View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_filter_assignee, container, false)
        assigneeSearch = mView.findViewById(R.id.filter_assignee_SearchView)
        assigneeListRecyclerView = mView.findViewById(R.id.filter_assignee_recyclerView)
        filterAssigneeModel = ArrayList()
        filterAssigneeApiCall()
        searchAssignee()
        activity?.findViewById<ImageView>(R.id.app_icon_imageView)?.visibility = View.GONE
        return mView
    }
    var searchtext = String()
    private fun searchAssignee(){
        assigneeSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchtext = newText
                filterAssigneeApiCall()
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchtext = query
                filterAssigneeApiCall()

                return true
            }

            override fun equals(other: Any?): Boolean {
                return super.equals(other)

            }


        })
    }

    private fun filterAssigneeApiCall(){
        var param = HashMap<String,String>()
//        param.put("accountId",accId)
//        param.put("StartDate","")
//        param.put("toDate","")
        param.put("search",searchtext)
        context?.let {
            apiRequest.postRequestBodyWithHeaders(it,webService.Add_task_assignee_url,param){ response->
                println("AddTaskAssigneeAsyncTask== $response")

                var status = response.getString("status")
                if (status == "200"){
                    var dataArray = response.getJSONArray("data")
                    for (i in 0 until dataArray.length()){
                        var dataObj = dataArray.getJSONObject(i)
                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")

//                        var profilePath = dataObj.getString("profilePath")
//                        var imgWithBase = webService.imageBasePath+profilePath
                        //                            context?.setSharedPref(context!!,"assignee_filter_id",_id)
//                            context?.setSharedPref(context!!,"assignee_filter_name",name)
                        filterAssigneeModel.add(FilterAssigneeModel(_id,name))
                        context?.progressBarDismiss(context!!)
                    }
                    setupFilterAssigneeRecyclerView(filterAssigneeModel)
                }else{
                    context?.progressBarDismiss(context!!)
                }

            }
        }

    }
    //    setup adapter
    private fun setupFilterAssigneeRecyclerView(filterAssigneeModel: ArrayList<FilterAssigneeModel>) {
        val adapter: RecyclerView.Adapter<*> = FilterAssigneeAdapter(context, filterAssigneeModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        assigneeListRecyclerView.layoutManager = llm
        assigneeListRecyclerView.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }
}
package com.salesc2

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.fragment.EventDetailsFragment
import com.salesc2.fragment.NotificationReassignFragment
import com.salesc2.fragment.TaskDetailsFragment
import com.salesc2.service.ApiRequest
import com.salesc2.service.MyFirebaseMessagingService
import com.salesc2.service.WebService
import com.salesc2.utils.capFirstLetter
import com.salesc2.utils.progressBarDismiss
import com.salesc2.utils.setSharedPref
import com.salesc2.utils.toast
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*

class NotificationHandler : AppCompatActivity() {



    var apiRequest = ApiRequest()
    var webService = WebService()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      setContentView(R.layout.activity_notification_handler)
        findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        findViewById<View>(R.id.divider1)?.visibility = View.GONE
       findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
       findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
       findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        clearNotification()
        val bundle:Bundle? = intent.extras
        var data:String?= bundle!!.getString("data")
        val title: String?=bundle!!.getString("title")
        val id: String?=bundle!!.getString("id")
        val type: String?=bundle!!.getString("type")
        val showPopup: String?=bundle!!.getString("showPopup")
        val notificationType: String?=bundle!!.getString("notificationType")

        val bundle1:Bundle? = intent.extras
        if (data==null) {
            //bundle must contain all info sent in "data" field of the notification

            val title: String?=bundle1!!.getString("title")
            val id: String?=bundle1!!.getString("id")
            val type: String?=bundle1!!.getString("type")
            val showPopup: String?=bundle1!!.getString("showPopup")
            val notificationType: String?=bundle1!!.getString("notificationType")

            NavigateToFragment(id,showPopup, notificationType,type)
        }
        else{

            data = data!!.substring(1, data!!.length - 1) //remove curly brackets
            val keyValuePairs =
                data!!.split(",").toTypedArray() //split the string to creat key-value pairs

            val map: MutableMap<String, String> = HashMap()

            for (pair in keyValuePairs)  //iterate over the pairs
            {
                val entry =
                    pair.split("=").toTypedArray() //split the pairs to get key and value
                map[entry[0].trim { it <= ' ' }] =
                    entry[1].trim { it <= ' ' } //add them to the hashmap and trim whitespaces
            }
            for ((k, v) in map) {

            }
            var type:String? = map.get("type")
            var id:String? = map.get("id")
            var showPopup:String? = map.get("showPopup")
            var notificationType:String? = map.get("notificationType")
            NavigateToFragment(id,showPopup,notificationType,type)
        }




    }

    private fun NavigateToFragment(id: String?, showPopup: String?,notificationType: String?,type:String?) {

        if (notificationType == "0"){
            if(showPopup == "true" || showPopup!!.equals(true)){
                acceptRejectPopUp(id!!)
            }else{
                val bundle = Bundle()
                bundle.putString("task_id",id)
                bundle.putString("",showPopup)
                val fragment = TaskDetailsFragment()
                fragment.arguments = bundle
                val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
                transaction.replace(R.id.notification_fragmet_container, fragment,"TaskDetailsFragment")
                transaction.addToBackStack("TaskDetailsFragment")
                transaction.commit()
            }

        }else if (notificationType == "1"){
            if(showPopup == "true" || showPopup!!.equals(true)){
                acceptRejectPopUpEvent(id!!)
            }else {
                val bundle = Bundle()
                bundle.putString("event_id", id)
                bundle.putString("", showPopup)
                val fragment = EventDetailsFragment()
                fragment.arguments = bundle
                val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
                transaction.replace(
                    R.id.notification_fragmet_container,
                    fragment,
                    "EventDetailsFragment"
                )
                transaction.addToBackStack("EventDetailsFragment")
                transaction.commit()
            }
        }
    }

    private fun clearNotification() {
        if (MyFirebaseMessagingService.notificationManager != null) {
            Objects.requireNonNull(MyFirebaseMessagingService.notificationManager)?.cancel(MyFirebaseMessagingService.Notification_ID)
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
    }

    override fun onBackPressed() {
        finish()
    }





    private fun acceptRejectPopUp(taskId:String){
        var pp_id = String()
        var pp_priority = String()
        var pp_startDate = String()
        var pp_endDate = String()
        var pp_assignedTo = String()
        var pp_taskName = String()
        var pp_conflictName = String()
        var pp_conflict = String()
        val params = HashMap<String,Any>()
        params["id"]=taskId
        this?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.Task_notification_url,params){ response->
                try {
                    var status = response.getString("status")

                    if (status == "200") {
                        var dataObj = response.getJSONObject("data")

                        pp_id = dataObj.getString("_id")
                        pp_priority = dataObj.getString("priority")
                        pp_startDate = dataObj.getString("startDate")
                        pp_endDate = dataObj.getString("endDate")
                        pp_assignedTo = dataObj.getString("assignedTo")
                        pp_taskName = dataObj.getString("taskName")
                        pp_conflictName = dataObj.getString("conflictName")
                        pp_conflict = dataObj.getString("conflict")

                        //bottom popup
                        var mBottomSheetDialog: Dialog? =
                            this?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) }
                        mBottomSheetDialog?.setContentView(R.layout.accept_reject_popup)
                        var accept = mBottomSheetDialog?.findViewById<Button>(R.id.accept_btn)
                        var reject = mBottomSheetDialog?.findViewById<Button>(R.id.reject_btn)
                        var close: ImageView? = mBottomSheetDialog?.findViewById(R.id.close_popup)
                        var notify_popup_title : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_title)
                        var notify_popup_name : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_name)
                        var notify_popup_datetime : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_datetime)
                        var notify_popup_image : ImageView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_image)
                        var notify_popup_emergency_tv : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_emergency_tv)
                        var notify_popup_emergency_alert : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_emergency_alert)
                        if (pp_priority == "true")
                        {
                            notify_popup_emergency_tv?.visibility = View.VISIBLE
                            Picasso.get().load(R.drawable.task_emergancy_img).into(notify_popup_image)
                            if (pp_conflict == "true") {
                                notify_popup_emergency_alert?.visibility = View.VISIBLE
                                notify_popup_emergency_alert?.text = pp_conflictName
                            }
                        }else{
                            notify_popup_emergency_tv?.visibility = View.GONE
                            notify_popup_emergency_alert?.visibility = View.GONE
                        }

                        notify_popup_title?.text = "Task Assignment"
                        notify_popup_name?.text = this?.capFirstLetter(pp_taskName)

                        try {
                            var parser = SimpleDateFormat("yyyy-MM-dd HH:mm")
                            val cal = Calendar.getInstance()
                            var tz:TimeZone = cal.timeZone
                            val myFormatDate = "MMM dd, yyyy"
                            val sdf = SimpleDateFormat(myFormatDate)
                            sdf.timeZone = TimeZone.getTimeZone(tz.displayName)
                            val stDate = sdf.format(parser.parse(pp_startDate))
                            val myFormatTime = "hh:mm a"
                            val sdfTime = SimpleDateFormat(myFormatTime)
                            sdfTime.timeZone = TimeZone.getTimeZone(tz.displayName)
                            val startTime =sdfTime.format(parser.parse(pp_startDate))
                            val endTime = sdfTime.format(parser.parse(pp_endDate))
                            notify_popup_datetime?.text = "$stDate at $startTime to $endTime".replace("am","AM").replace("pm","PM")


                        }catch (e:Exception){
                            print("td_date parse== $e")
                        }
//                        notify_popup_datetime?.text = pp_startDate


                        mBottomSheetDialog?.setCancelable(true)
                        mBottomSheetDialog?.window?.setLayout(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT
                        );
                        mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
                        mBottomSheetDialog?.show()

                        close?.setOnClickListener{
                            mBottomSheetDialog?.dismiss()
                            startActivity(Intent(this,MainActivity::class.java))
                        }

                        reject?.setOnClickListener{
                            val paramR = HashMap<String,Any>()
                            paramR["_id"] = taskId
                            paramR["taskStatus"] = "rejected"
                            this?.let { it1 ->
                                apiRequest.postRequestBodyWithHeadersAny(
                                    it1,
                                    webService.Update_status_url,
                                    paramR
                                ) { response ->
                                    var status = response.getString("status")
                                    var msg = response.getString("msg")
                                    if (status == "200") {
                                        this?.toast(msg)
                                        this?.progressBarDismiss(this)
                                        mBottomSheetDialog?.dismiss()
                                        startActivity(Intent(this,MainActivity::class.java))
//                                        (this as MainActivity).resetNotificationLists(this as MainActivity)

                                    } else {
                                        this?.toast(msg)
                                        this?.progressBarDismiss(this)
                                    }

                                }
                            }
                        }

                        accept?.setOnClickListener{
                            if (pp_conflict.equals("true") || pp_conflict.equals(true)){
                                notify_popup_title?.text = "Conflict Found"
                                accept?.setText("Reassign/Reschedule")
                                reject?.setText("Skip")
                                notify_popup_name?.text = pp_conflictName
                                Picasso.get().load(R.drawable.task_conflict_img).into(notify_popup_image)

                                reject?.setOnClickListener{
                                    mBottomSheetDialog?.dismiss()
                                    startActivity(Intent(this,MainActivity::class.java))
                                }
                                accept?.setOnClickListener{
                                    mBottomSheetDialog?.dismiss()
                                    this.setSharedPref(this,"task_id_notify",taskId)
                                    var fragment = NotificationReassignFragment()


                                    val transaction: FragmentTransaction =
                                        supportFragmentManager.beginTransaction()
                                    transaction.replace(R.id.notification_fragmet_container, fragment)
                                    transaction.addToBackStack(null)
                                    transaction.commit()
                                }


                            }else{
                                val param = HashMap<String,Any>()
                                param["_id"] = taskId
                                param["taskStatus"] = "assigned"
                                this?.let { it1 ->
                                    apiRequest.postRequestBodyWithHeadersAny(
                                        it1,
                                        webService.Update_status_url,
                                        param
                                    ) { response ->
                                        var status = response.getString("status")
                                        var msg = response.getString("msg")
                                        if (status == "200") {
                                            this?.toast(msg)
                                            this?.progressBarDismiss(this)
                                            mBottomSheetDialog?.dismiss()
                                            startActivity(Intent(this,MainActivity::class.java))
//                                            (context as MainActivity).resetNotificationLists(context as MainActivity)
                                        } else {
                                            this?.toast(msg)
                                            this?.progressBarDismiss(this)
                                        }

                                    }
                                }
                            }
                        }




                    }else{
                        var msg = response.getString("msg")
                        this?.toast(msg)

                    }
                }catch (e:Exception){

                }
            }
        }

    }


    private fun acceptRejectPopUpEvent(eventId:String){
        var pp_id = String()
        var pp_priority = String()
        var pp_startDate = String()
        var pp_endDate = String()
        var pp_assignedTo = String()
        var pp_conflictName = String()
        var pp_conflict = String()
        var pp_title = String()
        var pp_desc = String()
        var pp_conflictType = String()
        var pp_eventType = String()
        var pp_eventType_type = String()
        var pp_conflictId = String()
        val params = HashMap<String,Any>()
        params["id"] = eventId
        this?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.Event_notification_url,params){ response->
                try {
                    var status = response.getString("status")

                    if (status == "200") {
                        var dataObj = response.getJSONObject("data")

                        pp_id = dataObj.getString("_id")
                        pp_title = dataObj.getString("title")
                        pp_desc = dataObj.getString("description")
                        pp_priority = dataObj.getString("priority")
                        pp_startDate = dataObj.getString("startDate")
                        pp_endDate = dataObj.getString("endDate")
                        pp_conflictType = dataObj.getString("conflictType")
                        pp_eventType = dataObj.getString("eventType")
                        pp_eventType_type = dataObj.getString("eventType_type")
                        pp_conflictId = dataObj.getString("conflictId")
                        pp_conflictName = dataObj.getString("conflictName")
                        pp_conflict = dataObj.getString("conflict")

                        //bottom popup
                        var mBottomSheetDialog: Dialog? =
                            this?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) }
                        mBottomSheetDialog?.setContentView(R.layout.accept_reject_popup)
                        var accept = mBottomSheetDialog?.findViewById<Button>(R.id.accept_btn)
                        var reject = mBottomSheetDialog?.findViewById<Button>(R.id.reject_btn)
                        var close: ImageView? = mBottomSheetDialog?.findViewById(R.id.close_popup)
                        var notify_popup_title : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_title)
                        var notify_popup_name : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_name)
                        var notify_popup_datetime : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_datetime)
                        var notify_popup_image : ImageView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_image)
                        var notify_popup_emergency_tv : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_emergency_tv)
                        var notify_popup_emergency_alert : TextView? = mBottomSheetDialog?.findViewById(R.id.notify_popup_emergency_alert)
                        if (pp_priority == "true")
                        {
                            notify_popup_emergency_tv?.visibility = View.VISIBLE
                            Picasso.get().load(R.drawable.task_emergancy_img).into(notify_popup_image)
                            if (pp_conflict == "true") {
                                notify_popup_emergency_alert?.visibility = View.VISIBLE
                                notify_popup_emergency_alert?.text = this?.capFirstLetter(pp_conflictName)
                            }
                        }else{
                            notify_popup_emergency_tv?.visibility = View.GONE
                            notify_popup_emergency_alert?.visibility = View.GONE
                        }

                        notify_popup_title?.text = pp_eventType.toUpperCase()
                        notify_popup_name?.text = this?.capFirstLetter(pp_title)


                        try {
                            var parser = SimpleDateFormat("yyyy-MM-dd HH:mm")
                            val cal = Calendar.getInstance()
                            var tz:TimeZone = cal.timeZone

                            val myFormatDate = "MMM dd, yyyy"
                            val sdf = SimpleDateFormat(myFormatDate)
                            sdf.timeZone = TimeZone.getTimeZone(tz.displayName)
                            val stDate = sdf.format(parser.parse(pp_startDate))
                            val myFormatTime = "hh:mm a"
                            val sdfTime = SimpleDateFormat(myFormatTime)
                            sdfTime.timeZone = TimeZone.getTimeZone(tz.displayName)
                            val startTime =sdfTime.format(parser.parse(pp_startDate))
                            val endTime = sdfTime.format(parser.parse(pp_endDate))
                            notify_popup_datetime?.text = "$stDate at $startTime to $endTime".replace("am","AM").replace("pm","PM")


                        }catch (e:Exception){

                        }
//                        notify_popup_datetime?.text = pp_startDate


                        mBottomSheetDialog?.setCancelable(true)
                        mBottomSheetDialog?.window?.setLayout(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT
                        );
                        mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
                        mBottomSheetDialog?.show()

                        close?.setOnClickListener{
                            mBottomSheetDialog?.dismiss()
                            startActivity(Intent(this,MainActivity::class.java))

                        }

                        reject?.setOnClickListener{
                            val paramR = HashMap<String,Any>()
                            paramR["type"] = 2
                            paramR["id"] = eventId
                            this?.let { it1 ->
                                apiRequest.postRequestBodyWithHeadersAny(
                                    it1,
                                    webService.Event_accept_reject_url,
                                    paramR
                                ) { response ->
                                    var status = response.getString("status")
                                    var msg = response.getString("msg")
                                    if (status == "200") {
                                        this?.toast(msg)
//                                        context?.progressBarDismiss(context!!)
                                        mBottomSheetDialog?.dismiss()
                                        startActivity(Intent(this,MainActivity::class.java))
//                                        (context as MainActivity).resetNotificationLists(context as MainActivity)

                                    } else {
                                        this?.toast(msg)
//                                        context?.progressBarDismiss(context!!)
                                    }

                                }
                            }
                        }

                        accept?.setOnClickListener{
                            if (pp_conflict.equals("true") || pp_conflict.equals(true)){
                                notify_popup_title?.text = "Conflict Found"
                                accept?.setText("Reassign/Reschedule")
                                reject?.setText("Skip")
                                notify_popup_name?.text = pp_conflictName
                                Picasso.get().load(R.drawable.task_conflict_img).into(notify_popup_image)

                                reject?.setOnClickListener{
                                    mBottomSheetDialog?.dismiss()
                                    startActivity(Intent(this,MainActivity::class.java))
                                }
                                accept?.setOnClickListener{
                                    mBottomSheetDialog?.dismiss()
                                    startActivity(Intent(this,MainActivity::class.java))
                                }


                            }else{
                                val param = HashMap<String,Any>()
                                param["type"] = 1
                                param["id"] = eventId
                                this?.let { it1 ->
                                    apiRequest.postRequestBodyWithHeadersAny(
                                        it1,
                                        webService.Event_accept_reject_url,
                                        param
                                    ) { response ->
                                        var status = response.getString("status")
                                        var msg = response.getString("msg")
                                        if (status == "200") {
                                            this?.toast(msg)
//                                            context?.progressBarDismiss(context!!)
                                            mBottomSheetDialog?.dismiss()
                                            startActivity(Intent(this,MainActivity::class.java))
//                                            (context as MainActivity).resetNotificationLists(context as MainActivity)
                                        } else {
                                            this?.toast(msg)
//                                            this?.progressBarDismiss(context!!)
                                        }

                                    }
                                }
                            }
                        }




                    }else{
                        var msg = response.getString("msg")
                        this?.toast(msg)
                    }
                }catch (e:Exception){

                }
            }
        }

    }




}

package com.salesc2.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TaskStatusDao {
    @Query("SELECT * FROM TaskStatusTable")
    suspend fun getAll(): List<TaskStatusTable>

    @Insert
    suspend fun insertAll(vararg taskStatusTable: TaskStatusTable)

    @Query("DELETE FROM TaskStatusTable")
    fun deleteAll()

    @Query("UPDATE TaskStatusTable SET t_status=:status WHERE  dash_task_id= :t_id")
    fun updateTaskStatus(status: String?, t_id: String)

    @Query("DELETE FROM TaskStatusTable WHERE id = :sid")
    fun deleteById(sid: Int)
}
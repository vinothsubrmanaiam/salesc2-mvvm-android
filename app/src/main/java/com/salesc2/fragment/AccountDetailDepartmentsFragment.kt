package com.salesc2.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.adapters.AccountDetailDepartmentListAdapter
import com.salesc2.model.AccountDepartmentContactModel
import com.salesc2.model.AccountDepartmentDetailModel
import com.salesc2.model.AccountDepartmentEmailModel
import com.salesc2.model.AccountDepartmentPhoneModel
import com.salesc2.service.AlertDialogView
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.alertDialog
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.progressBarDismiss
import java.util.*
import kotlin.collections.ArrayList

class AccountDetailDepartmentsFragment : Fragment() {


    lateinit var mView: View
    var webService = WebService()
    var apiRequest = ApiRequest()
    var alertDialogView = AlertDialogView()
    val accountDepartmentDetailModel = ArrayList<AccountDepartmentDetailModel>()
    var accountDetailDepartmentListRecyclerView: RecyclerView? = null

    var departmentName = String()
    var dept_id = String()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_account_detail_departments, container, false)

        //initializing views
        initViews(mView)

        //calling account's Department detail API
        accountDepartmentDetail_url()


        return mView
    }

    //initializing views
    private fun initViews(mView: View?) {

        //account department list Recycler view
        accountDetailDepartmentListRecyclerView = mView?.findViewById(R.id.accounts_departments_rv)
        // accountDetailDepartmentListRecyclerView?.setHasFixedSize(true)
        // accountDetailDepartmentListRecyclerView?.isNestedScrollingEnabled = false
    }

    //Calling Department Detail
    private fun accountDepartmentDetail_url() {
        // context?.let { activity?.progressBarShow(it) }
        var authToken = String()
        authToken = context?.let { activity?.getSharedPref("token", it).toString() }.toString()

        if (accountDepartmentDetailModel != null) {
            accountDepartmentDetailModel.clear()
        }


        var accountID = String()
        accountID = arguments?.getString("account_id").toString()

        val params = HashMap<String, String>()//""5efd9f151087d90007986f93""
        params.put("id", accountID)

        context?.let {
            apiRequest.postRequestBodyWithHeaders(
                it,
                webService.Account_departmentdetail_url,
                params
            ) { response ->
                val status = response.getString("status")
                if (status == "200") {
                    val data = response.getString("data")

                    var dataArray = response.getJSONArray("data")

                    for (i in 0 until dataArray.length()) {

                        val jsonObject = dataArray.getJSONObject(i)
                        dept_id = jsonObject.getString("_id")
                        departmentName = jsonObject.getString("departmentName")


                        var contactArray = jsonObject.getJSONArray("contact")

                        val accountDepartmentContactModel =
                            ArrayList<AccountDepartmentContactModel>()

                        for (i in 0 until contactArray.length()) {
                            var contactname = String()
                            val contactJsonObject = contactArray.getJSONObject(i)

                            contactname = contactJsonObject.getString("name")

                            var phoneArray = contactJsonObject.getJSONArray("phone")
                            val accountDepartmentPhoneModel =
                                ArrayList<AccountDepartmentPhoneModel>()

                            for (i in 0 until phoneArray.length()) {
                                var phone_id = String()
                                var phoneNo = String()
                                var phoneType = String()
                                val phoneJsonObject = phoneArray.getJSONObject(i)
                                phone_id = phoneJsonObject.getString("_id")
                                phoneNo = phoneJsonObject.getString("phoneNo")
                                phoneType = phoneJsonObject.getString("phoneType")

                                accountDepartmentPhoneModel.add(
                                    AccountDepartmentPhoneModel(
                                        phone_id,
                                        phoneNo,
                                        phoneType
                                    )
                                )
                            }

                            var emailArray = contactJsonObject.getJSONArray("email")
                            val accountDepartmentEmailModel =
                                ArrayList<AccountDepartmentEmailModel>()

                            for (i in 0 until emailArray.length()) {
                                var email_id = String()
                                var emailid = String()
                                var emailType = String()
                                val emailJsonObject = emailArray.getJSONObject(i)
                                email_id = emailJsonObject.getString("_id")
                                emailid = emailJsonObject.getString("emailid")
                                emailType = emailJsonObject.getString("emailType")

                                accountDepartmentEmailModel.add(
                                    AccountDepartmentEmailModel(
                                        emailid,
                                        email_id,
                                        emailType
                                    )
                                )
                            }

                            accountDepartmentContactModel.add(
                                AccountDepartmentContactModel(
                                    contactname,
                                    accountDepartmentPhoneModel,
                                    accountDepartmentEmailModel
                                )
                            )
                        }

                        accountDepartmentDetailModel.add(
                            AccountDepartmentDetailModel(
                                dept_id,
                                departmentName,
                                accountDepartmentContactModel
                            )
                        )


                    }
                    context?.let { activity?.progressBarDismiss(it) }
                    setupAccountDepartmentDetailRecyclerView(accountDepartmentDetailModel)
                } else {
                    val msg = response.getString("msg")
                    activity?.alertDialog(requireContext(), "Alert", msg)
                }

            }


        }


    }


    //    setup adapter
    private fun setupAccountDepartmentDetailRecyclerView(accountDepartmentDetailModel: ArrayList<com.salesc2.model.AccountDepartmentDetailModel>) {
        val adapter: RecyclerView.Adapter<*> =
            AccountDetailDepartmentListAdapter(
                context,
                accountDepartmentDetailModel
            )
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        accountDetailDepartmentListRecyclerView?.layoutManager = llm
        accountDetailDepartmentListRecyclerView?.adapter = adapter
        context?.let { activity?.progressBarDismiss(it) }
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(requireContext())
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(requireContext())
    }

}




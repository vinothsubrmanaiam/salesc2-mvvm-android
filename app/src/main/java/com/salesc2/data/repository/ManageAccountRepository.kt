package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseManageAccount
import com.salesc2.data.network.api.response.ResponseTeamList
import com.salesc2.data.network.api.service.ManageAccountApi
import com.salesc2.data.network.api.service.TeamListApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

interface ManageAccountRepository
{

    suspend fun reqTeamList (

        onSuccess: OnSuccess<ResponseManageAccount>,
        onFailure: OnFailure<ResponseManageAccount>
    )

    companion object Factory{
        fun create(api : ManageAccountApi) : ManageAccountRepository = ManageAccountRepositoryImpl(api)
    }


}
class ManageAccountRepositoryImpl(val api: ManageAccountApi) : ManageAccountRepository {
    override suspend fun reqTeamList(

        onSuccess: OnSuccess<ResponseManageAccount>,
        onFailure: OnFailure<ResponseManageAccount>
    ) {


        withContext(Dispatchers.IO)
        {
            try {

                val response = api.reqManageProfile()
                if(response.isSuccessful)
                {
                    response.body().let {
                        if(it?.status.equals("200"))
                        {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it!!)
                            }
                        }
                        else
                        {
                            onFailure(it!!)
                        }
                    }
                }

            }catch (e: Exception)
            {

            }
        }
    }

}
package com.salesc2.data.repository

import com.salesc2.data.network.api.response.ResponseTeamList
import com.salesc2.data.network.api.service.TeamListApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

interface TeamListRepository
{

    suspend fun reqTeamList (
        onSuccess: OnSuccess<ResponseTeamList>,
        onFailure: OnFailure<ResponseTeamList>
    )

    companion object Factory{
        fun create(api : TeamListApi) : TeamListRepository = TeamListRepositoryImpl(api)
    }


}
class TeamListRepositoryImpl(val api: TeamListApi) : TeamListRepository {
    override suspend fun reqTeamList(
        onSuccess: OnSuccess<ResponseTeamList>,
        onFailure: OnFailure<ResponseTeamList>
    ) {


        withContext(Dispatchers.IO)
        {
            try {

                val response = api.reqTeamLogin()
                if(response.isSuccessful)
                {
                    response.body().let {
                        if(it?.status.equals("200"))
                        {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it!!)
                            }
                        }
                        else
                        {
                            onFailure(it!!)
                        }
                    }
                }

            }catch (e: Exception)
            {

            }
        }
    }

}
package com.salesc2.fragment

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.util.PatternsCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.adapters.EventAttendeesAdapter
import com.salesc2.adapters.EventMultipleAttendeeAdapter
import com.salesc2.model.AddAttendeesModel
import com.salesc2.model.EventAttendeesModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.DatabaseHandler
import com.salesc2.service.WebService
import com.salesc2.utils.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class AddEventFragment: Fragment() {
    private lateinit var addEvent_title: EditText
    private lateinit var addEvent_eventType: TextView
    private lateinit var addEvent_eventLoc:TextView
    private lateinit var addEvent_eventDT:TextView
    private lateinit var addEvent_eventPrioritySwitch:Switch
    private lateinit var addEvent_eventPriorityText:TextView
    private lateinit var addEvent_desc:EditText
    private lateinit var addEvent_btn:Button
    private lateinit var cancel_addEvent : Button
    private var  priority : Boolean =  false
    private lateinit var add_event_back_arrow :ImageView
    private lateinit var addEvent_attendees:TextView
    private lateinit var eventLoc_layout : ConstraintLayout
    private lateinit var addEvent_recyclerView : RecyclerView

    private var eventAttendeesModel = ArrayList<EventAttendeesModel>()

    private var eType_name = String()
    private var eType_id = String()
    private var eLocation_name = String()
    private var event_start_date= String()
    private var event_start_time = String()
    private var event_end_date = String()
    private var event_end_time = String()
    private var eType_type = String()
    private var event_lat = String()
    private var event_lon = String()
    private var attendee_id = String()
    private var attendee_img = String()
    private var attendee_name = String()

    private var titleSaved = String()
    private var descSaved = String()

    var apiRequest = ApiRequest()
    var webService = WebService()
    private var userRole = String()

    private var event_dt_edited = String()
    private var event_title_edit = String()
    private var edit_event  = String()
    lateinit var mView: View

    private lateinit var addEventToolbarHead : TextView

    private var event_desc_edit =  String()
    private lateinit var eventAttendees_layout :ConstraintLayout

    private var isChecked_event_priority = String()

    private  var event_id = String()
    private lateinit var cgTag : ChipGroup
    private lateinit var bd_invoice_emails : EditText
    private var keywordList = mutableListOf<String>()
    var TIME = 3 * 1000.toLong()
    var stringArrayListEmails = ArrayList<String>()

    private var set_timezone = String()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mView = inflater.inflate(R.layout.fragment_add_event,container,false)

        event_desc_edit = context?.getSharedPref("event_desc_edit",context!!).toString()
        event_title_edit = context?.getSharedPref("event_title_edit",context!!).toString()
        edit_event = context?.getSharedPref("edit_event",context!!).toString()
        eType_name = context?.getSharedPref("eType_name", context!!).toString()
        eType_id = context?.getSharedPref("eType_id",context!!).toString()
        eLocation_name = context?.getSharedPref("eLocation_name",context!!).toString()
        event_start_date = context?.getSharedPref("event_start_date",context!!).toString()
        event_start_time = context?.getSharedPref("event_start_time",context!!).toString()
        event_end_date = context?.getSharedPref("event_end_date",context!!).toString()
        event_end_time = context?.getSharedPref("event_end_time",context!!).toString()

        eType_type = context?.getSharedPref("eType_type",context!!).toString()

        event_lat = context?.getSharedPref("latitude", context!!).toString()
        event_lon = context?.getSharedPref("longitude", context!!).toString()

        attendee_name =  context?.getSharedPref("eAttendee_name",context!!).toString()
        attendee_id = context?.getSharedPref("eAttendee_id",context!!).toString()
        attendee_img = context?.getSharedPref("eAttendee_img",context!!).toString()

        event_id = context?.getSharedPref("event_id",context!!).toString()
        isChecked_event_priority = context?.getSharedPref("isChecked_event_priority",context!!).toString()

        set_timezone = context?.getSharedPref("set_timezone",context!!).toString()

        addEvent_title = mView.findViewById(R.id.addEvent_title)
        addEvent_eventType = mView.findViewById(R.id.addEvent_eventType)
        addEvent_eventLoc = mView.findViewById(R.id.addEvent_eventLoc)
        addEvent_eventDT = mView.findViewById(R.id.addEvent_eventDT)
        addEvent_eventPrioritySwitch = mView.findViewById(R.id.addEvent_eventPrioritySwitch)
        addEvent_eventPriorityText = mView.findViewById(R.id.addEvent_eventPriorityText)
        addEvent_desc = mView.findViewById(R.id.addEvent_desc)
        addEvent_btn = mView.findViewById(R.id.addEvent_btn)
        cancel_addEvent = mView.findViewById(R.id.cancel_addEvent)
        add_event_back_arrow = mView.findViewById(R.id.add_event_back_arrow)
        addEvent_attendees = mView.findViewById(R.id.addEvent_attendees)
        eventLoc_layout = mView.findViewById(R.id.eventLoc_layout)
        addEvent_recyclerView = mView.findViewById(R.id.addEvent_recyclerView)
        eventAttendeesModel = ArrayList()
        addEventToolbarHead = mView.findViewById(R.id.addEventToolbarHead)
        bd_invoice_emails = mView.findViewById(R.id.bd_invoice_emails)
        cgTag  = mView.findViewById(R.id.cgTag)

        if (isChecked_event_priority == "yes"){
            addEvent_eventPrioritySwitch.isChecked = true
            priority =true
        }
        if(attendee_id == "null"){
            attendee_id = ""
        }
        event_dt_edited = context?.getSharedPref("event_dt_edited",context!!).toString()

        userRole = context?.getSharedPref( "role", context!!).toString()

        eventAttendees_layout = mView.findViewById(R.id.eventAttendees_layout)

        if (userRole == "Representative" && eType_type == "0"){
            eventAttendees_layout.visibility = View.GONE
        }else{
            eventAttendees_layout.visibility = View.VISIBLE
        }


        bd_invoice_emails.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (event.action === KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                if (PatternsCompat.EMAIL_ADDRESS.matcher(bd_invoice_emails.text.toString()).matches().equals(true)){
                    addChipToGroup(bd_invoice_emails.text.toString())
                    addEvent_desc.clearFocus()
                    bd_invoice_emails.requestFocus()

                }else{
                    context?.toast("Enter a valid email")
                }

                return@OnKeyListener true
            }
            false
        })



        add_event_back_arrow.setOnClickListener{
//            context?.loadFragment(context!!,SelectEventDateTimeFragment())
            fragmentManager?.popBackStack()
        }

if (eType_type == "0") {
    if (attendee_name == "null" || attendee_name == "") {
        addEvent_recyclerView.visibility = View.GONE
    } else {
        addEvent_recyclerView.visibility = View.VISIBLE
    }
}

        addEvent_eventType.text = eType_name

        if (eType_type == "1"){
        if (eLocation_name == "" || eLocation_name == "null"){
            addEvent_eventLoc.text = "NA"
            eventLoc_layout.visibility =  View.VISIBLE
            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("latitude")
            editor.remove("longitude")
            editor.commit()
            }else{
            addEvent_eventLoc.text = eLocation_name
            eventLoc_layout.visibility =  View.VISIBLE
        }
        }

        if (eType_type == "0"){
            eventLoc_layout.visibility =  View.GONE
        }
        try {
            var parser = SimpleDateFormat("MM/dd/yyyy")
            val myFormatDate = "MMM dd"
            val sdfDate = SimpleDateFormat(myFormatDate)
            val stDate = sdfDate.format(parser.parse("$event_start_date"))

                addEvent_eventDT.text = "$stDate at $event_start_time to $event_end_time".replace("am","AM").replace("pm","PM")



        }catch (e:java.lang.Exception){

        }


        addOrCancelEventButtonAction()
        prioritySwitchAction()
        addAttendeesAction()
        editAddEvent()
//        getCurrentAndMyTime()

        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor : SharedPreferences.Editor = preferences.edit()
        editor.remove("edit_add_event")
        editor.commit()

        if (eType_type == "0") {
            eventAttendeesModel.add(EventAttendeesModel(attendee_name, attendee_id, attendee_img,""))
            setupEventAttendeeRecyclerView(eventAttendeesModel)
        }else{
            viewRecord()
        }
        editTextSave()

        if (edit_event =="yes"){
            if (eType_type == "1"){
                addEventToolbarHead.text = "Edit work event"
            }else{
                addEventToolbarHead.text = "Edit personal event"
            }
            addEvent_btn.text = "Update"
            if (event_desc_edit == "null" || event_desc_edit == "Null"){
                addEvent_desc.setText("")
            }else{
                addEvent_desc.setText(event_desc_edit)
            }
            addEvent_title.setText(event_title_edit)
            if (eLocation_name == "" || eLocation_name == "null") {
                addEvent_eventLoc.text = "NA"
            }

        }else{
            if (eType_type == "1"){
                addEventToolbarHead.text = "Add work event"
                addEvent_btn.setText("Add work event")
            }else{
                addEventToolbarHead.text = "Add personal event"
                addEvent_btn.setText("Add personal event")
            }
        }
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        return mView
    }
    private fun addChipToGroup(keyword: String) {
        if (keyword.contains("#")) {
            val parts = keyword.split(",")
            val removedEmpty = parts.filter { it != "" }
            removedEmpty.forEach {
                val removedComma = it.removeSuffix(",")
                if (keywordList.size < 5) {
                    keywordList.add(removedComma)
                    createChips(removedComma)
                } else
                    activity?.toast("testing")
            }
        } else {
            keywordList.add(keyword)
            createChips(keyword)
            stringArrayListEmails.add(keyword)
            addEvent_desc.clearFocus()
            bd_invoice_emails.requestFocus()
            bd_invoice_emails.performClick()
        }
    }
    private fun createChips(keyWords: String) {
        val chip = Chip(context)
        chip.text = "$keyWords"
        chip.isCloseIconVisible = true
        chip.isClickable = true
        chip.isCheckable = false
        cgTag.addView(chip as View)
        chip.setOnCloseIconClickListener {
            val new = chip.text.removePrefix("#")
            keywordList.remove(new)
            stringArrayListEmails.remove(new)
            cgTag.removeView(chip as View)


        }
        bd_invoice_emails.text.clear()
        addEvent_desc.clearFocus()
        bd_invoice_emails.requestFocus()
        bd_invoice_emails.performClick()
    }


    private fun editTextSave(){
        val LAST_TEXT = "eTitle"
        val pref = PreferenceManager.getDefaultSharedPreferences(context)
        addEvent_title.setText(pref.getString(LAST_TEXT, ""))
        addEvent_title.addTextChangedListener(object: TextWatcher {
          override  fun beforeTextChanged(s:CharSequence, start:Int, count:Int, after:Int) {
            }
         override   fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
            }
           override fun afterTextChanged(s: Editable) {
                pref.edit().putString(LAST_TEXT, s.toString()).commit()
            }
        })

        val LAST_TEXT_DESC = "eDesc"
        val prefDesc = PreferenceManager.getDefaultSharedPreferences(context)
        addEvent_desc.setText(prefDesc.getString(LAST_TEXT_DESC, ""))
        addEvent_desc.addTextChangedListener(object: TextWatcher {
            override  fun beforeTextChanged(s:CharSequence, start:Int, count:Int, after:Int) {
            }
            override   fun onTextChanged(s:CharSequence, start:Int, before:Int, count:Int) {
            }
            override fun afterTextChanged(s: Editable) {
                prefDesc.edit().putString(LAST_TEXT_DESC, s.toString()).commit()
            }
        })
    }






    var attendeeArrayId :Array<String>? = null
    //creating the instance of DatabaseHandler class
    var databaseHandler: DatabaseHandler? = null
    //calling the viewEmployee method of DatabaseHandler class to read the records
    var attendee: List<AddAttendeesModel>? = null

    var stringArrayListMeeting = ArrayList<String>()
    private fun viewRecord(){
        databaseHandler = DatabaseHandler(context!!)
        attendee = databaseHandler?.viewAttendees()
//        //creating the instance of DatabaseHandler class
//        val databaseHandler: DatabaseHandler = DatabaseHandler(context!!)
//        //calling the viewEmployee method of DatabaseHandler class to read the records
//        val attendee: List<AddAttendeesModel> = databaseHandler.viewAttendees()
        val a_Id = Array<String>(attendee!!.size){""}
        val a_Name = Array<String>(attendee!!.size){"null"}
        val a_img = Array<String>(attendee!!.size){"null"}

        var index = 0

        for(a in attendee!!){
            a_Id[index] = a.at_id
            a_Name[index] = a.at_name
            a_img[index] = a.at_img
            index++

            stringArrayListMeeting.add("${a.at_id}")
        }
        val myListAdapter = EventMultipleAttendeeAdapter(context as Activity,a_Id,a_Name,a_img)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.HORIZONTAL
        addEvent_recyclerView.layoutManager = llm
        addEvent_recyclerView.adapter = myListAdapter

    }



    private fun editAddEvent(){
        addEvent_eventDT.setOnClickListener{



            context?.loadFragment(context!!,SelectEventDateTimeFragment())
        }
        addEvent_eventType.setOnClickListener{
            context?.setSharedPref(context!!,"edit_add_event","101")
            context?.loadFragment(context!!,SelectEventTypeFragment())
        }
        addEvent_eventLoc.setOnClickListener{
            context?.setSharedPref(context!!,"edit_add_event","101")
            context?.loadFragment(context!!,SelectEventLocationFragment())
        }

    }

    private fun addAttendeesAction(){
        addEvent_attendees.setOnClickListener{
            event_start_date = context?.getSharedPref("event_start_date",context!!).toString()
            event_start_time = context?.getSharedPref("event_start_time",context!!).toString()
            event_end_date = context?.getSharedPref("event_end_date",context!!).toString()
            event_end_time = context?.getSharedPref("event_end_time",context!!).toString()
            context?.loadFragment(context!!,SelectEventAttendee())
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun addOrCancelEventButtonAction(){
        addEvent_btn.setOnClickListener{
            addEvent_btn.isEnabled = false



            Handler().postDelayed(Runnable {
                it.setEnabled(true)
                addEvent_btn.isEnabled = true
            }, TIME)

            edit_event = context?.getSharedPref("edit_event",context!!).toString()
            if(eType_type == "0") {
                if (addEvent_title.text.toString() == "" || addEvent_title.text.toString().isBlank()) {
                    context?.toast("Please enter event title")
                   // addEvent_btn.isEnabled = true
                }
                else{

                    if (edit_event =="yes"){
                        editEventDetailsApiCall()
                     //   addEvent_btn.isEnabled = true
                    }else{
                        addEventApiCall()
                      //  addEvent_btn.isEnabled = true
                    }



                }
            } else if (eType_type == "1"){
                if (addEvent_title.text.toString() == "" || addEvent_title.text.toString().isBlank()) {
                    context?.toast("Please enter event title")
                  //  addEvent_btn.isEnabled = true
                }
                else{

                    if (edit_event =="yes"){
                        editEventDetailsApiCall()
                      //  addEvent_btn.isEnabled = true
                    }else{
                        addEventApiCall()
                        //addEvent_btn.isEnabled = true
                    }

                }
            }


        }
        cancel_addEvent.setOnClickListener{

            val dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.yes_or_no_alert_view)
            dialog.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
            val title : TextView = dialog.findViewById(R.id.alert_title)
            title.text = "Alert"
            val msg: TextView = dialog.findViewById(R.id.alert_msg)
            if(edit_event =="yes"){
                msg.text = "Do you want to cancel editing event?"
            }else{
                msg.text = "Do you want to cancel add event?"
            }

            val yes = dialog.findViewById<TextView>(R.id.alert_yes)
            yes.setOnClickListener(View.OnClickListener {
                if(edit_event =="yes"){
                    addEvent_title.text.clear()
                    addEvent_desc.text.clear()
                    startActivity(Intent(context,MainActivity::class.java))
                    dialog.dismiss()
                }else{
                    val databaseHandler : DatabaseHandler = DatabaseHandler(context!!)
                    databaseHandler.deleteAttendees()
                    val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
                    val editor : SharedPreferences.Editor = preferences.edit()
                    editor.remove("eAttendee_name")
                    editor.remove("eAttendee_id")
                    editor.remove("eAttendee_img")
                    editor.remove("edit_add_event")
                    editor.remove("eTitle")
                    editor.remove("eDesc")
                    editor.commit()
                    addEvent_title.text.clear()
                    addEvent_desc.text.clear()
                    startActivity(Intent(context,MainActivity::class.java))
                    dialog.dismiss()
                }
            })
            val no = dialog.findViewById(R.id.alert_no) as TextView
            no.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()

        }
    }

    private fun prioritySwitchAction(){
       /* var PREF_SWITCH_PUSH = "switch"
        var SHARED_PREFS = "PREF"
        val sharedPreferences = context?.getSharedPreferences(SHARED_PREFS, MODE_PRIVATE)
        val editor = sharedPreferences?.edit()
        addEvent_eventPrioritySwitch.isChecked =
            sharedPreferences?.getBoolean(PREF_SWITCH_PUSH, false)!! //false default }
*/

        addEvent_eventPrioritySwitch.setOnCheckedChangeListener{buttonView, isChecked ->
            if (isChecked){
                context?.setSharedPref(context!!,"isChecked_event_priority","yes")
//                addEvent_eventPrioritySwitch.isChecked = true
                context?.let { ContextCompat.getColor(it,R.color.colorAccent) }?.let {
                    addEvent_eventPriorityText.setTextColor(it)
                }
                priority = true
//                editor?.putBoolean(PREF_SWITCH_PUSH, true)
            }else{
//                addEvent_eventPrioritySwitch.isChecked = false
                priority= false
                val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
                val editor : SharedPreferences.Editor = preferences.edit()
                editor.remove("isChecked_event_priority")
                editor.commit()
//                editor?.putBoolean(PREF_SWITCH_PUSH, false)
                context?.let { ContextCompat.getColor(it,R.color.emergency_text_color) }?.let {
                    addEvent_eventPriorityText.setTextColor(it)
                }

            }
        }
    }

    private fun addEventApiCall(){
        event_lat = context?.getSharedPref("latitude", context!!).toString()
        event_lon = context?.getSharedPref("longitude", context!!).toString()

        var parser = SimpleDateFormat("MM/dd/yyyy hh:mm a")
        val myFormat = "yyyy-MM-dd hh:mm a"
        val sdf = SimpleDateFormat(myFormat)
        val stDate = parser.parse("$event_start_date $event_start_time")
        val stDateUTC = context?.dateToUTC(context!!,stDate)
        val stDateFormat = sdf.format(stDateUTC)
        val edDate = parser.parse("$event_end_date $event_end_time")
        val edDateUTC = context?.dateToUTC(context!!,edDate)
        val edDateFormat = sdf.format(edDateUTC)
        var stringArrayListPersonal = ArrayList<String>()
        if (attendee_id != ""){
            stringArrayListPersonal.add("$attendee_id")
        }


        var eventLocLatLon = ArrayList<Double>()
        try {
            eventLocLatLon.add(event_lat.toDouble())
            eventLocLatLon.add(event_lon.toDouble())
        }catch (e:Exception){

        }




        var params = HashMap<String,Any>()

var titleEvent = addEvent_title.text.toString()
        var descEvent =  addEvent_desc.text.toString()
        if (eType_type == "1") {
            params["title"] = titleEvent
            if (descEvent != ""){
                params["description"] = descEvent
            }
//            if (stringArrayListMeeting.size >= 1){
                params["attendees"] = stringArrayListMeeting
//            }

            params["isEmergency"] = priority
            params["startDate"] = stDateFormat
            params["endDate"] =  edDateFormat
            params["eventType_type"] = eType_type!!.toInt()
            params["location"] = eventLocLatLon
            params["eventType"] = eType_id
            if(eLocation_name != ""){
                params["address"] = eLocation_name.toString()
            }

            if (stringArrayListEmails.size >= 1){
                params["ccEmails"] = stringArrayListEmails
            }
        }
        else if (eType_type == "0"){
            params["title"] = titleEvent
            if (descEvent != ""){
                params["description"] = descEvent
            }
            params["attendees"] = stringArrayListPersonal
            params["isEmergency"] = priority
            params["startDate"] = stDateFormat
            params["endDate"] = edDateFormat
            params["eventType_type"] = eType_type!!.toInt()
            params["eventType"] = eType_id.toString()
            params["location"] = eventLocLatLon
            if(eLocation_name != ""){
                params["address"] = eLocation_name.toString()
            }
            if (stringArrayListEmails.size >= 1){
                params["ccEmails"] = stringArrayListEmails
            }
        }


        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.Event_add_url,params){response->
                try {
                var status = response.getString("status")
                var msg = response.getString("msg")
                if (status == "200"){
//                    context?.alertDialog(context!!,"Success",msg)
                    context?.toast(msg)
                    val databaseHandler : DatabaseHandler = DatabaseHandler(context!!)
                    databaseHandler.deleteAttendees()
                    val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
                    val editor : SharedPreferences.Editor = preferences.edit()
                    editor.remove("eAttendee_name")
                    editor.remove("eAttendee_id")
                    editor.remove("eAttendee_img")
                    editor.remove("edit_add_event")
                    editor.remove("eTitle")
                    editor.remove("eDesc")
                    editor.commit()
                    addEvent_title.text.clear()
                    addEvent_desc.text.clear()
                    startActivity(Intent(context,MainActivity::class.java))
                }
                else{
                    context?.toast(msg)
                }

            }catch (e:Exception){

                    context?.toast(e.toString())
                }
            }
        }
    }

    //    setup adapter
    private fun setupEventAttendeeRecyclerView(eventAttendeesModel: ArrayList<EventAttendeesModel>) {
        val adapter: RecyclerView.Adapter<*> = EventAttendeesAdapter(context, eventAttendeesModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        addEvent_recyclerView.layoutManager = llm
        addEvent_recyclerView.adapter = adapter
    }


    fun dateToUTC(date: Date): Date? {
        return Date(date.time - Calendar.getInstance().timeZone.getOffset(date.time))
    }



    private fun editEventDetailsApiCall(){
        event_lat = context?.getSharedPref("latitude", context!!).toString()
        event_lon = context?.getSharedPref("longitude", context!!).toString()

        var parser = SimpleDateFormat("MM/dd/yyyy hh:mm a")
        val myFormat = "yyyy-MM-dd hh:mm a"
        val sdf = SimpleDateFormat(myFormat)
        val stDate = parser.parse("$event_start_date $event_start_time")
        val stDateLocal = context?.dateToUTC(context!!,stDate)
        val stDateFormat = sdf.format(stDateLocal)
        val edDate = parser.parse("$event_end_date $event_end_time")
        val edDateLocal = context?.dateToUTC(context!!,edDate)
        val edDateFormat = sdf.format(edDateLocal)
        var stringArrayListPersonal = ArrayList<String>()
        if (attendee_id != ""){
            stringArrayListPersonal.add("$attendee_id")
        }


        var eventLocLatLon = ArrayList<Double>()
        try {
            eventLocLatLon.add(event_lat.toDouble())
            eventLocLatLon.add(event_lon.toDouble())
        }catch (e:Exception){

        }

        var stringArrayListEmails = ArrayList<String>()
        stringArrayListEmails.add("")

        var titleEvent = addEvent_title.text.toString()
        var descEvent =  addEvent_desc.text.toString()
        val params = HashMap<String,Any>()
        if (eType_type == "1") {
            params["title"] = titleEvent
            if (descEvent != ""){
                params["description"] = descEvent
            }
//            if (stringArrayListMeeting.size >= 1){
            params["attendees"] = stringArrayListMeeting
//            }

            params["isEmergency"] = priority
            params["startDate"] = stDateFormat
            params["endDate"] =  edDateFormat
            params["eventType_type"] = eType_type!!.toInt()
            params["location"] = eventLocLatLon
            params["eventType"] = eType_id
            if(eLocation_name != ""){
                params["address"] = eLocation_name.toString()
            }

            if (stringArrayListEmails.size >= 1){
                params["ccEmails"] = stringArrayListEmails
            }
        }
        else if (eType_type == "0"){
            params["title"] = titleEvent
            if (descEvent != ""){
                params["description"] = descEvent
            }
            params["attendees"] = stringArrayListPersonal
            params["isEmergency"] = priority
            params["startDate"] = stDateFormat
            params["endDate"] = edDateFormat
            params["eventType_type"] = eType_type!!.toInt()
            params["eventType"] = eType_id.toString()
            params["location"] = eventLocLatLon
            if(eLocation_name != ""){
                params["address"] = eLocation_name.toString()
            }
            if (stringArrayListEmails.size >= 1){
                params["ccEmails"] = stringArrayListEmails
            }
        }
        context?.let {
            apiRequest.putRequestBodyWithHeadersAny(it,webService.Edit_event_url+"/$event_id",params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
//                    context?.alertDialog(context!!,"Success",msg)
                        context?.toast(msg)
                        val databaseHandler : DatabaseHandler = DatabaseHandler(context!!)
                        databaseHandler.deleteAttendees()
                        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
                        val editor : SharedPreferences.Editor = preferences.edit()
                        editor.remove("eAttendee_name")
                        editor.remove("eAttendee_id")
                        editor.remove("eAttendee_img")
                        editor.remove("edit_add_event")
                        editor.remove("eTitle")
                        editor.remove("eDesc")

                        editor.commit()
                        addEvent_title.text.clear()
                        addEvent_desc.text.clear()
                        startActivity(Intent(context,MainActivity::class.java))
                    }
                    else{
                        context?.toast(msg)
                    }

                }catch (e:Exception){

                    context?.toast(e.toString())
                }
            }
        }
    }


}
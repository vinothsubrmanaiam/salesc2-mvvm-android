package com.salesc2.adapters

import android.app.Dialog
import android.content.Context
import android.text.method.DigitsKeyListener
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.TaskDetailsExpensesModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import java.text.SimpleDateFormat
import java.util.*

class TaskDetailsExpensesAdapter(var context: Context,var expensesItems: ArrayList<TaskDetailsExpensesModel>,var refreshProductAndExpensesTaskDetails: RefreshProductAndExpensesTaskDetails) : RecyclerView.Adapter<TaskDetailsExpensesAdapter.MyViewHolder>() {

    var apiRequest = ApiRequest()
    var webService = WebService()
    var task_id_edit_prod = String()
   init {
        this.context = context
        this.expensesItems = expensesItems

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_task_details_expenses, parent, false)
        task_id_edit_prod = context?.getSharedPref("task_id_edit_prod", context!!).toString()
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return expensesItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var taskDetailsExpensesModel: TaskDetailsExpensesModel = expensesItems[position]
        task_id_edit_prod = context?.getSharedPref("task_id_edit_prod", context!!).toString()
     try {
        holder.expense_name.text = context.capFirstLetter(taskDetailsExpensesModel.description)
         holder.expense_price.text ="$ ${taskDetailsExpensesModel.amount}"
         holder.expense_employee.text = taskDetailsExpensesModel.employeeName
         var parser = SimpleDateFormat("yyyy-MM-dd hh:mm a")
         var myFormat = "MMM dd, yyyy hh:mm a"
         val sdf = SimpleDateFormat(myFormat)
         val stDate = parser.parse(taskDetailsExpensesModel.time)
         var stDateToLocal = context?.dateFromUTC(context!!,stDate)
         var stDateF = sdf.format(stDateToLocal)
         holder.expense_date.text = stDateF.replace("am","AM").replace("pm","PM")
     }catch (e:Exception){
         print(e)
     }


        holder.expense_price.setOnClickListener{
            var mBottomSheetDialog: Dialog? =
                context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) };
            mBottomSheetDialog?.setContentView(R.layout.task_details_add_expenses_popup)
            var expense_name = mBottomSheetDialog?.findViewById<EditText>(R.id.product_name_et)
            var expense_price = mBottomSheetDialog?.findViewById<EditText>(R.id.product_price_et)
            var close: ImageView? = mBottomSheetDialog?.findViewById(R.id.close_iv)
            var addExpense : Button? = mBottomSheetDialog?.findViewById(R.id.add_expense)
            var remove_expense : Button? = mBottomSheetDialog?.findViewById(R.id.remove_expense)
            var addExpensesHead : TextView? = mBottomSheetDialog?.findViewById(R.id.addExpensesHead)
            addExpensesHead?.text = "Edit Expenses"


             remove_expense!!.visibility = View.VISIBLE

            //decimal
            expense_price?.setKeyListener(DigitsKeyListener.getInstance(false,true))

            expense_name?.setText(taskDetailsExpensesModel.description)
            expense_price?.setText(taskDetailsExpensesModel.amount)
            addExpense?.setText("Update")
            addExpense?.setOnClickListener{
                var desc = expense_name?.text.toString()
                var price = expense_price?.text.toString()
                if (desc.trim() == "" || desc.trim().isEmpty() || desc.trim().isBlank()){
                    context?.toast("Please enter expense details")
                }
                else if (price.trim() == ""){
                    context?.toast("Please enter price")
                }else if (price.toDouble() < 0.1){
                    context?.toast("Please enter a valid price")
                }
                else{
                    val roundOff = Math.round(price.toDouble() * 100.0) / 100.0

                    var params = HashMap<String,Any>()
                    params["task_id"] = task_id_edit_prod
                    params["description"] = desc
                    params["amount"] = roundOff.toString()
                    params["expensesId"] = taskDetailsExpensesModel.expense_id
                    context?.let {
                        apiRequest.postRequestBodyWithHeadersAny(it,webService.Task_expenses_add_edit_url,params){ response->
                            try {

                                var status = response.getString("status")
                                var msg = response.getString("msg")
                                if (status == "200"){
                                    mBottomSheetDialog?.dismiss()
                                    context?.toast(msg)
                                    refreshProductAndExpensesTaskDetails.refreshProductAndExpenses()
                                }
                                else{
                                    context?.toast(msg)
                                }

                            }catch (e:java.lang.Exception){
                                context?.toast(e.toString())
                            }
                        }
                    }
                }


            }

            remove_expense.setOnClickListener{
                val dialog = Dialog(context!!)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.setCancelable(false)
                dialog.setContentView(R.layout.yes_or_no_alert_view)
                dialog.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
                val title : TextView = dialog.findViewById(R.id.alert_title)
                title.text = "Alert"
                val msg: TextView = dialog.findViewById(R.id.alert_msg)
                msg.text = "Do you want to remove expense?"
                val yes = dialog.findViewById<TextView>(R.id.alert_yes)
                yes.setOnClickListener(View.OnClickListener {
                    dialog.dismiss()
                    var desc = expense_name?.text.toString()
                    var price = expense_price?.text.toString()
                    if (desc.trim() == "" || desc.trim().isEmpty() || desc.trim().isBlank()){
                        context?.toast("Please enter expense details")
                    }
                    else if (price.trim() == ""){
                        context?.toast("Please enter price")
                    }else if (price.toDouble() < 0.1){
                        context?.toast("Please enter a valid price")
                    }
                    else{
                        var params = HashMap<String,Any>()
                        params["task_id"] = task_id_edit_prod
                        params["description"] = desc
                        params["amount"] = price.toDouble()
                        params["expensesId"] = taskDetailsExpensesModel.expense_id
                        params["type"] = "delete"
                        context?.let {
                            apiRequest.postRequestBodyWithHeadersAny(it,webService.Task_expenses_add_edit_url,params){ response->
                                try {

                                    var status = response.getString("status")
                                    var msg = response.getString("msg")
                                    if (status == "200"){
                                        mBottomSheetDialog?.dismiss()
                                        context?.toast("Expense removed successfully")
                                        refreshProductAndExpensesTaskDetails.refreshProductAndExpenses()
                                    }
                                    else{
                                        context?.toast(msg)
                                    }

                                }catch (e:java.lang.Exception){
                                    context?.toast(e.toString())
                                }
                            }
                        }
                    }


                })
                val no = dialog.findViewById(R.id.alert_no) as TextView
                no.setOnClickListener {
                    dialog.dismiss()
                }
                dialog.show()
            }

            mBottomSheetDialog?.setCancelable(true)
            mBottomSheetDialog?.window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            );
            mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
            mBottomSheetDialog?.show()

            close?.setOnClickListener{
                mBottomSheetDialog?.dismiss()
            }

        }


    }
    class MyViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView) {
        var expense_employee: TextView = itemView.findViewById(R.id.expense_employee)
        var expense_price : TextView = itemView.findViewById(R.id.expense_price)
        var expense_date : TextView = itemView.findViewById(R.id.expense_date)
        var expense_name : TextView = itemView.findViewById(R.id.expense_name)
    }


}
package com.salesc2.data.network.api.response

data class ResponseUpdateTimeZone(
    val msg: String,
    val status: String
)
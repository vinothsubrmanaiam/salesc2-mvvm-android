package com.salesc2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.NotificationSettingsTasksModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.capFirstLetter
import com.salesc2.utils.toast
import java.util.regex.Matcher
import java.util.regex.Pattern

class NotificationSettingsTaskAdapter() : RecyclerView.Adapter<NotificationSettingsTaskAdapter.MyViewHolder>() {
    private var notificationSettingsItems = ArrayList<NotificationSettingsTasksModel>()
    var context : Context? = null
    private var settingsSwitch :Boolean = false
    var apiRequest = ApiRequest()
    var webService = WebService()

    constructor(context: Context?, notificationSettingsTasksItems: ArrayList<NotificationSettingsTasksModel>) : this(){
        this.context = context
        this.notificationSettingsItems = notificationSettingsTasksItems

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_notification_settings, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
       return notificationSettingsItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       var notificationSettingsTasksModel : NotificationSettingsTasksModel = notificationSettingsItems[position]
        holder.name_settings.text = context?.capFirstLetter(notificationSettingsTasksModel.name.replace("_", " "))
        holder.notify_switch.isChecked = notificationSettingsTasksModel.notification
        holder.email_switch.isChecked = notificationSettingsTasksModel.email
        if (notificationSettingsTasksModel.name == "task_create_territory_clinical"){
            holder.notify_switch.trackDrawable = ContextCompat.getDrawable(context!!,R.drawable.track_selector_blocked)
            holder.notify_switch.isClickable = false
            holder.notify_switch.clearFocus()

        }else{
            holder.notify_switch.isClickable = true
            holder.notify_switch.setOnCheckedChangeListener { buttonView, isChecked ->

                if (isChecked) {
                    holder.notify_switch.isChecked = true
                    editSwitchApiCall(
                        notificationSettingsTasksModel.email,
                        notificationSettingsTasksModel.name,
                        true
                    )
                    settingsSwitch = true
                } else {
                    holder.notify_switch.isChecked = false
                    settingsSwitch = false
                    editSwitchApiCall(
                        notificationSettingsTasksModel.email,
                        notificationSettingsTasksModel.name,
                        false
                    )

                }
            }
        }

        if (notificationSettingsTasksModel.name == "task_rejected"){
            holder.email_switch.trackDrawable = ContextCompat.getDrawable(context!!,R.drawable.track_selector_blocked)
            holder.email_switch.isClickable = false
            holder.email_switch.clearFocus()
        }else{
            holder.email_switch.isClickable = true
            holder.email_switch.setOnCheckedChangeListener { buttonV, isChecked ->
                if (isChecked) {
                    holder.email_switch.isChecked = true
                    editSwitchApiCall(
                        true,
                        notificationSettingsTasksModel.name,
                        notificationSettingsTasksModel.notification
                    )
                    settingsSwitch = true
                } else {
                    holder.email_switch.isChecked = false
                    settingsSwitch = false
                    editSwitchApiCall(
                        false,
                        notificationSettingsTasksModel.name,
                        notificationSettingsTasksModel.notification
                    )

                }


            }
        }

    }

    private fun editSwitchApiCall(email : Boolean,name:String,notification:Boolean){
        var params =  HashMap<String,Any>()
        params["email"] = email
        params["name"] = name
        params["notification"] = notification

        context?.let {
            apiRequest.putRequestBodyWithHeadersAny(it,webService.user_notification_settings,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
//                        context?.toast(msg)
                    }else{
                        context?.toast(msg)
                    }

                }catch (e:Exception){
                    context?.toast(e.toString())
                }
            }
        }

    }

    class MyViewHolder constructor(itemView : View):RecyclerView.ViewHolder(itemView) {
var name_settings : TextView = itemView.findViewById(R.id.name_settings)
var notify_switch : SwitchCompat = itemView.findViewById(R.id.notify_switch)
        var email_switch : SwitchCompat = itemView.findViewById(R.id.email_switch)
    }

}
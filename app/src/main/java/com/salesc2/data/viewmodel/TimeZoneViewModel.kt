package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.response.ResponseTeamList
import com.salesc2.data.network.api.response.ResponseTimeZone
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.TeamListRepository
import com.salesc2.data.repository.TimeZoneRepository
import kotlinx.coroutines.launch

class TimeZoneViewModel(
    private val timeZoneRepository: TimeZoneRepository,
    val context: Context
) : ViewModel() {

    fun reqTeamList(
        onSuccess: OnSuccess<ResponseTimeZone>,
        onFailure: OnFailure<ResponseTimeZone>

    ){
        viewModelScope.launch {
            timeZoneRepository.reqTimezone(onSuccess,onFailure)
        }
    }

}
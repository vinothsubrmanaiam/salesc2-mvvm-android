package com.salesc2.fragment

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.salesc2.R
import com.salesc2.adapters.TaskListAdapter
import com.salesc2.model.TaskListModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class TaskListFragment : Fragment(),RefreshTaskList {

    var webService = WebService()
    var apiRequest = ApiRequest()
    lateinit var tasklistRecyclerView : RecyclerView
    var taskListModel = ArrayList<TaskListModel>()

    lateinit var taskList_SearchView : SearchView
    lateinit var taskList_filter : ImageView
    lateinit var mView:View
    lateinit var noTaskFound_layout : ConstraintLayout

    private var filterAccountID = String()
    private var filterAssigneeID = String()
    private var filterStatusName = String()
    private var filterDateRangeStart = String()
    private var filterDateRangeEnd = String()
    private var filterTerritoryID = String()

    private var tasklistResp = String()

    private var currentPage = String()
    private var lastPage = String()
    private  var pageLoad:Int = 1
    private var isLoading = true
    var previousTotal : Int  = 0
    private var pageSize = String()
    private lateinit var tasklist_backArrow : ImageView

    private lateinit var taskListProgressbar: ProgressBar
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context?.progressBarShow(context!!)
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_task_list,container,false)
//        context?.progressBarDismiss(context!!)

//        filterAccountID = arguments?.getString("filter_account_id").toString()
//        filterAssigneeID = arguments?.getString("filter_assignee_id").toString()
//        filterStatusName  = arguments?.getString("filter_status_name").toString()
//        filterDateRangeStart  = arguments?.getString("filter_start_dt").toString()
//        filterDateRangeEnd  = arguments?.getString("filter_end_dt").toString()
//        filterTerritoryID  = arguments?.getString("filter_territory_id").toString()


        filterAccountID = context?.getSharedPref("filter_account_id", context!!).toString()
        filterAssigneeID = context?.getSharedPref("filter_assignee_id", context!!).toString()
        filterStatusName =  context?.getSharedPref("filter_status_name",context!!).toString()
        filterDateRangeStart = context?.getSharedPref("filter_start_dt",context!!).toString()
        filterDateRangeEnd = context?.getSharedPref("filter_end_dt",context!!).toString()
        filterTerritoryID = context?.getSharedPref("filter_territory_id",context!!).toString()

        tasklistRecyclerView = mView.findViewById(R.id.taskList_recyclerView)
        taskListModel = ArrayList()
        taskList_SearchView = mView.findViewById(R.id.taskList_SearchView)
        taskList_filter = mView.findViewById(R.id.taskList_filter)
        noTaskFound_layout = mView.findViewById(R.id.noTaskFound_layout)
        taskListProgressbar = mView.findViewById(R.id.taskListProgressbar)
        tasklist_backArrow = mView.findViewById(R.id.tasklist_backArrow)

        tasklist_backArrow.setOnClickListener{
            context?.loadFragment(context!!,MoreFragment())
        }

        if (filterAccountID != "null" || filterTerritoryID != "null" || filterDateRangeStart != "null" || filterDateRangeEnd != "null" ||filterStatusName != "null" || filterAssigneeID != "null"){
        taskList_filter.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_filter_icon_with_dot))
            taskList_filter.setPadding(0,0,0,0)
        }else{
            taskList_filter.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_filter_icon))
        }

        taskListSearchAction()
        taskListFilterAction()
        TaskListAsyncTask().execute()
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private fun taskListFilterAction(){
        taskList_filter.setOnClickListener{
            context?.setSharedPref(context!!,"territory_toolbar","no")
            context?.loadFragment(context!!,FilterTaskListFragment())
        }
    }

    var searchtext = String()
    private fun taskListSearchAction(){
        taskList_SearchView.setOnQueryTextListener(object :SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchtext = newText
                pageLoad = 1
                taskListModel.clear()
                TaskListAsyncTask().execute()
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchtext = query
                pageLoad = 1
                taskListModel.clear()
                TaskListAsyncTask().execute()
                return true
            }

            override fun equals(other: Any?): Boolean {
                return super.equals(other)

            }


        })
    }
    var acID = String()
    var assigneID = String()
    var statusName = String()
    var tertoryID = String()
    var startDate = String()
    var endDate = String()


    inner class TaskListAsyncTask(): AsyncTask<String,String,String>(){
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (pageLoad == 1){
                taskListProgressbar.visibility = View.VISIBLE
                taskListModel.clear()
                taskListModel = ArrayList()

            }


        }
        override fun doInBackground(vararg params: String?): String {

            var parser = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
            var myFormat ="yyyy-MM-dd HH:mm:ss"
            val sdf = SimpleDateFormat(myFormat)


            if (filterDateRangeStart != "null" && filterDateRangeEnd != "null") {
               val  startDateParse = parser.parse(filterDateRangeStart)
                val stDateUTC =context?.dateToUTC(context!!,startDateParse)
                startDate = sdf.format(stDateUTC)
                val endDateParse = parser.parse(filterDateRangeEnd)
                val edDateUTC =context?.dateToUTC(context!!,endDateParse)
                endDate = sdf.format(edDateUTC)
            }else{
                startDate = ""
                endDate = ""
            }


            if (filterAccountID != "null"){
                acID = filterAccountID.toString()
            }else{
                acID  = ""
            }
            if (filterAssigneeID != "null"){
                assigneID = filterAssigneeID.toString()
            }else{
                assigneID  = ""
            }
            if (filterStatusName != "null"){
                statusName = filterStatusName.toString()
            }else{
                statusName  = ""
            }

            if (filterTerritoryID != "null"){
                tertoryID = filterTerritoryID.toString()
            }else{
                tertoryID  = ""
            }


            var params = HashMap<String, Any>()
            params["search"] = searchtext
            params["nextpage"] = pageLoad
            params["pagesize"] = "10"
            params["taskStatus"] = statusName
            params["assignedTo"] = assigneID
            params["accountId"] = acID
            params["territoryId"] = tertoryID
            params["startDate"] = startDate
            params["endDate"] = endDate


            context?.let {
                apiRequest.postRequestBodyWithHeadersAny(it,webService.Task_list_url,params){ response->
                    try {
                    var status = response.getString("status")
                    if (status == "200"){
                        if (pageLoad == 1){
                            taskListProgressbar.visibility = View.VISIBLE
                            taskListModel.clear()
                            taskListModel = ArrayList()
                        }

                        var dataArray = response.getJSONArray("data")
                        for (i in 0 until dataArray.length()) {

                            var dataObj = dataArray.getJSONObject(i)
                            var _id = dataObj.getString("_id")
                            var taskStatus = dataObj.getString("taskStatus")
                            var taskName = dataObj.getString("taskName")
                            var assignedTo = dataObj.getJSONObject("assignedTo")
                            var assigneto_id = assignedTo.getString("_id")
                            var assigneName = assignedTo.getString("name")
                            var departmentName = String()
                            if (dataObj.has("departmentName")) {
                                departmentName = dataObj.getString("departmentName")
                            } else {
                                departmentName = ""
                            }
                            var startDate = dataObj.getString("startDate")
                            var endDate = dataObj.getString("endDate")

                            var locationObj = dataObj.getJSONObject("location")
                            var lat = locationObj.getString("lat")
                            var lng = locationObj.getString("lng")


                            context?.progressBarDismiss(context!!)
                            taskListModel.add(
                                TaskListModel(
                                    _id,
                                    taskName,
                                    departmentName,
                                    taskStatus,
                                    assigneName,
                                    assigneto_id,
                                    startDate,
                                    endDate,lat,lng
                                )
                            )
                        }
                        if(response.has("pagination")){
                            var paginationObj = response.getJSONObject("pagination")
                            currentPage = paginationObj.getString("currentPage")
                            lastPage = paginationObj.getString("endPage")
                            var totalItems = paginationObj.getString("totalItems")
                        }


                        taskListProgressbar.visibility = View.GONE
                            setupTaskListRecyclerView(taskListModel)
                        if (taskListModel.size == 0){
                            noTaskFound_layout.visibility = View.VISIBLE
                        }else{
                            noTaskFound_layout.visibility = View.GONE
                        }
                    }else{
                        taskListProgressbar.visibility = View.GONE
                        context?.progressBarDismiss(context!!)
                    }
                }catch (e:Exception){
                        println(e)
                        taskListProgressbar.visibility = View.GONE
                        context?.toast(e.toString())
                    }
                }
            }

            return ""
        }

        override fun onPreExecute() {
            super.onPreExecute()
            context?.progressBarDismiss(context!!)
        }

    }

    //    setup adapter
    private fun setupTaskListRecyclerView(taskListModel: ArrayList<TaskListModel>) {
        val adapter: RecyclerView.Adapter<*> = TaskListAdapter(this@TaskListFragment,context!!, taskListModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        tasklistRecyclerView.layoutManager = llm
        tasklistRecyclerView.adapter = adapter

        tasklistRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val visibleItemCount = llm.childCount
                val totalItemCount = llm.itemCount
                val firstVisibleItem = llm.findFirstVisibleItemPosition()


                if (isLoading) {
                    if (totalItemCount > previousTotal) {
                        previousTotal = totalItemCount
//                        var i = currentPage.toInt()
                        isLoading = false
                        Log.v("...", "Last Item Wow !");

                    }
                }
                if (!isLoading && (firstVisibleItem + visibleItemCount) >= totalItemCount && pageLoad < lastPage.toInt()) {
                    pageLoad++
                    isLoading = true
                    TaskListAsyncTask().execute()
                }
            }


        })



    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

    override fun onResume() {
        super.onResume()
        context?.progressBarDismiss(context!!)
    }

    override fun taskListApiCall() {
        taskListModel.clear()
        pageLoad = 1
        TaskListAsyncTask().execute()
    }


}
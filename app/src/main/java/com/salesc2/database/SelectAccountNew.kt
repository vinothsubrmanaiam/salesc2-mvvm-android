package com.salesc2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.json.JSONArray
import org.json.JSONObject





@Entity
data class SelectAccountNew(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "account_id") val account_id: String?,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "department") val department: String?
    )

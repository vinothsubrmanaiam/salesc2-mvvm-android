package com.salesc2.adapters

import android.app.Dialog
import android.content.Context
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.fragment.TaskDetailsFragment
import com.salesc2.model.DashSectionModel
import com.salesc2.model.DashTasksModel
import com.salesc2.model.MapTaskListModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.capFirstLetter
import com.salesc2.utils.dateFromUTC
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.setSharedPref
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MapTaskListAdapter():RecyclerView.Adapter<MapTaskListAdapter.MyViewHolder>() {


    var mapTaskListItems = ArrayList<MapTaskListModel>()

    var context : Context? = null
    var dash_tab_click = String()
    var apiRequest = ApiRequest()
    var webService = WebService()
    constructor(context: Context?, mapTaskListItems: ArrayList<MapTaskListModel>) : this(){
        this.context = context
        this.mapTaskListItems = mapTaskListItems

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_map_tasklist, parent, false)
        dash_tab_click = context?.getSharedPref("dash_tab_click",context!!).toString()
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
       return mapTaskListItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
      var mapTaskListModel : MapTaskListModel = mapTaskListItems[position]

        try {
            holder.map_task_account_name.text = context?.capFirstLetter(mapTaskListModel.accountName)
            holder.map_task_status.text = context?.capFirstLetter(mapTaskListModel.taskStatus)
            holder.map_task_name.text = context?.capFirstLetter(mapTaskListModel.taskName)

            holder.map_task_assignTo_name.text = context?.capFirstLetter(mapTaskListModel.assignTo_name)
        }catch (e:Exception){

        }

        if (mapTaskListModel.assignTo_name == "null" || mapTaskListModel.assignTo_name == ""){
            holder.map_assign_to.visibility = View.GONE
        }else{
            holder.map_assign_to.visibility = View.VISIBLE
        }


        holder.itemView.setOnClickListener {
            (context as MainActivity).closeMapsPopUp(context as MainActivity)
            context?.setSharedPref(context!!, "task_start_date", mapTaskListModel.startDate)
            context?.setSharedPref(context!!, "task_end_date", mapTaskListModel.endDate)
            context?.setSharedPref(context!!, "td_task_id", mapTaskListModel._id)
            context?.setSharedPref(context!!, "td_assignee_name", mapTaskListModel.assignTo_name)
            var bundle = Bundle()
            bundle.putString("task_id", mapTaskListModel._id)
            bundle.putString("task_start_date", mapTaskListModel.startDate)
            bundle.putString("task_end_date", mapTaskListModel.endDate)
            bundle.putString("task_assignto_id", "")
            bundle.putString("task_assignee_name", mapTaskListModel.assignTo_name)

            var fragment = TaskDetailsFragment()
            fragment.arguments = bundle
            val activity = context as AppCompatActivity
            val transaction: FragmentTransaction =
                activity.supportFragmentManager.beginTransaction()
            transaction.replace(R.id.main_fragmet_container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }




        try {
            var sdf:SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            var myFormat = SimpleDateFormat("hh:mm a")
            val time_parse: Date = sdf.parse(mapTaskListModel.startDate)
            val timeParseLocal = context?.dateFromUTC(context!!, time_parse)
            val startTime: String = myFormat.format(timeParseLocal)
            holder.map_task_time.text = startTime.replace("am","AM").replace("pm","PM")
        }catch (e:Exception){

        }


        if (mapTaskListModel.taskStatus.equals("Unassigned", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_text) }?.let {
                holder.map_task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_bg) }?.let {
                holder.map_task_status.setBackgroundColor(it)
            }
            setTextViewDrawableColor(holder.map_task_status, R.color.un_assigned_text)

        } else if (mapTaskListModel.taskStatus.equals("Completed", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.completed_text) }?.let {
                holder.map_task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.completed_bg) }?.let {
                holder.map_task_status.setBackgroundColor(it)
            }
            setTextViewDrawableColor(holder.map_task_status, R.color.completed_text)

        } else if (mapTaskListModel.taskStatus.equals("Uncovered", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.uncovered_text) }?.let {
                holder.map_task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.uncovered_bg) }?.let {
                holder.map_task_status.setBackgroundColor(it)
            }
//            val unwrappedDrawable = AppCompatResources.getDrawable(context!!, R.drawable.ic_dot)
//            val wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable!!)
//            DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(context!!, R.color.uncovered_text))
            setTextViewDrawableColor(holder.map_task_status, R.color.uncovered_text)

        } else if (mapTaskListModel.taskStatus.equals("Cancelled", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.cancelled_text) }?.let {
                holder.map_task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.cancelled_bg) }?.let {
                holder.map_task_status.setBackgroundColor(it)
            }
            setTextViewDrawableColor(holder.map_task_status, R.color.cancelled_text)

        } else if (mapTaskListModel.taskStatus.equals("In Progress", ignoreCase = true) ||mapTaskListModel.taskStatus.equals("inprogress", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.in_progress_text) }?.let {
                holder.map_task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.in_progress_bg) }?.let {
                holder.map_task_status.setBackgroundColor(it)
            }

            setTextViewDrawableColor(holder.map_task_status, R.color.in_progress_text)
        } else if (mapTaskListModel.taskStatus.equals("Pending", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.pending_text) }?.let {
                holder.map_task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_bg) }?.let {
                holder.map_task_status.setBackgroundColor(it)
            }
            setTextViewDrawableColor(holder.map_task_status, R.color.pending_text)

        } else if (mapTaskListModel.taskStatus.equals("Assigned", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.assigned_text) }?.let {
                holder.map_task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_bg) }?.let {
                holder.map_task_status.setBackgroundColor(it)
            }
            setTextViewDrawableColor(holder.map_task_status, R.color.assigned_text)
        }




    }

    private fun setTextViewDrawableColor(textView: TextView, color: Int) {
        for (drawable in textView.compoundDrawables) {
            if (drawable != null) {
                drawable.colorFilter = PorterDuffColorFilter(
                    ContextCompat.getColor(textView.context, color),
                    PorterDuff.Mode.SRC_IN
                )
            }
        }
    }

    class MyViewHolder constructor(itemView:View): RecyclerView.ViewHolder(itemView) {
      var map_task_account_name : TextView = itemView.findViewById(R.id.map_task_account_name)
      var map_taskStatus_cv : CardView = itemView.findViewById(R.id.map_taskStatus_cv)
      var map_task_status :TextView = itemView.findViewById(R.id.map_task_status_tv)
        var map_task_name : TextView = itemView.findViewById(R.id.map_task_name)
        var map_task_time : TextView = itemView.findViewById(R.id.map_task_time)
        var map_task_assignTo_name : TextView = itemView.findViewById(R.id.map_task_assignTo_name)
        var map_assign_to :TextView = itemView.findViewById(R.id.map_assign_to)

    }
}
package com.salesc2.service.imageupload

import net.simplifiedcoding.imageuploader.UploadResponse
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import okhttp3.logging.HttpLoggingInterceptor

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.Response
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Header
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part


interface ImageUploadAPI {


    @Multipart
    @POST("profile")
    fun uploadImage(
        @Header("Authorization") token: String,
        @Part image: MultipartBody.Part,
        @Part("phone") phone: RequestBody,
        @Part("address") address: RequestBody,
        @Part("lat") lat: RequestBody,
        @Part("lng") lng: RequestBody

    ): Call<UploadResponse>

    companion object {
        operator fun invoke(): ImageUploadAPI {
            val okHttpClient: OkHttpClient by lazy {
                OkHttpClient.Builder().apply {

                //    addNetworkInterceptor(StethoInterceptor())
                    addInterceptor(
                        HttpLoggingInterceptor().apply {
                            this.level = HttpLoggingInterceptor.Level.BODY

                        }
                    )

                }

                    .connectTimeout(30, java.util.concurrent.TimeUnit.SECONDS)
                    .readTimeout(30, java.util.concurrent.TimeUnit.SECONDS)
                    .writeTimeout(30, java.util.concurrent.TimeUnit.SECONDS)
// .callTimeout(25, TimeUnit.SECONDS)
                    .build()
            }

            return Retrofit.Builder()
                .baseUrl("http://3.219.91.255:9002/api/v1/")
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build()
                .create(ImageUploadAPI::class.java)
        }
    }

}
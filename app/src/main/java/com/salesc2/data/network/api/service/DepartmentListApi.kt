package com.salesc2.data.network.api.service



import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestDepartmentList
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseDepartmentList
import com.salesc2.data.network.api.response.ResponseTeamList
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface DepartmentListApi {

    @POST("ios-task/api/v1/departmentFilter")
    suspend fun reqGetDepartmentList(
        @Body requestDepartmentList: RequestDepartmentList
    ): Response<ResponseDepartmentList>
}
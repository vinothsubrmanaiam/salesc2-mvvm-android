package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestDashBoard
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseDashBoard
import com.salesc2.data.network.api.service.AccountListApi
import com.salesc2.data.network.api.service.DashBoardAPI
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface AccountListRepository {

    suspend fun reqGetAccountList(
      requestAccountList: RequestAccountList,
    onSuccess: OnSuccess<ResponseAccountList>,
    onFailure: OnFailure<ResponseAccountList>

    )

    companion object Factory{
        fun create(api : AccountListApi) : AccountListRepository = AccountListRepositoryImpl(api)
    }
}

class AccountListRepositoryImpl(var api: AccountListApi) : AccountListRepository {
    override suspend fun reqGetAccountList(
        requestAccountList: RequestAccountList,
        onSuccess: OnSuccess<ResponseAccountList>,
        onFailure: OnFailure<ResponseAccountList>
    ) {


        withContext(Dispatchers.IO) {
            try {
                val response = api.reqGetAccountList(requestAccountList = requestAccountList)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status==200) {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }
    }

}

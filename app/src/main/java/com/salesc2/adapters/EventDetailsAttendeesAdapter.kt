package com.salesc2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.EventAttendeesModel
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.lang.Exception

class EventDetailsAttendeesAdapter():RecyclerView.Adapter<EventDetailsAttendeesAdapter.MyViewHolder>() {
    private var attendeesItems = ArrayList<EventAttendeesModel>()
    var context : Context? = null

    constructor(context: Context?, attendeesItems: ArrayList<EventAttendeesModel>) : this(){
        this.context = context
        this.attendeesItems = attendeesItems

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_event_details_attendees, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return attendeesItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var eventAttendeesModel :EventAttendeesModel =  attendeesItems[position]
        holder.attendeeName.text = eventAttendeesModel.attendee_name
            try {
                when (eventAttendeesModel.type) {
                    "1" -> {
                        holder.attendeeStatusImg.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_round_tick_green))

                    }
                    "2" -> {
                        holder.attendeeStatusImg.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_cal_close_red))

                    }
                    "0" -> {
                        holder.attendeeStatusImg.setImageDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_cal_question))

                    }
                }
                Picasso.get().load(eventAttendeesModel.attendee_img)
                    .placeholder(R.drawable.ic_dummy_user_pic).into(holder.attendeeImg)
            } catch (e: Exception) {
                print(e)
            }

    }
    class MyViewHolder constructor(itemView:View):RecyclerView.ViewHolder(itemView){
    var attendeeImg : CircleImageView = itemView.findViewById(R.id.attendee_ed_img)
        var attendeeName : TextView = itemView.findViewById(R.id.attendee_ed_name)
        var attendeeStatusImg : ImageView = itemView.findViewById(R.id.attendeeStatusImg)
    }

}
package com.salesc2

import android.Manifest
import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.Icon
import android.location.Location
import android.os.*
import androidx.core.app.ActivityCompat
import androidx.core.util.PatternsCompat
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnSuccessListener
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.alertDialog
import com.salesc2.utils.progressBarDismiss
import com.salesc2.utils.progressBarShow
import com.salesc2.utils.toast
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit


class LocationUpdate : Service(), LocationListener {
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest

    private var serviceStartedFrom: String = ""
    private var serviceStartedTime: String? = ""
    private var serviceCreatedTime: String? = ""
    var currentLatitude = 0.0
    var currentLongtitude = 0.0
    var currentAccuracy = 0.0
    var speed = 0.0
    private var startID = 0

    private var timerRunnable: Runnable? = null
    private var mDateObject: Date? = null
    private var excecuter: ScheduledFuture<*>? = null
    private var notificationManager: NotificationManager? = null
    private val mTimer =
        Executors.newSingleThreadScheduledExecutor()

    private var UPDATE_INTERVAL: Long = 0
    private var TIMER_INTERVAL: Long = 300000

        //
    private var DELAY_DUE_TO_TRAFFIC: Long = 10000
    var STARTED_AT: Long = 0

    private val DistanceHandler: Handler? = null
    private  var mhandler:Handler? = null
    var instance: LocationUpdate? = null
    private var mLocationRequest: LocationRequest? = null

    var webService = WebService()
    var apiRequest = ApiRequest()

    override fun onBind(p0: Intent?): IBinder? {
        TODO("Not yet implemented")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null && intent.getStringExtra("started_from") != null)
            serviceStartedFrom = intent.getStringExtra("started_from")!!
        serviceStartedTime = GetUTCdatetimeAsString()
        startID = startId


        return super.onStartCommand(intent, flags, startId)
    }


    override fun onCreate() {
        super.onCreate()
        this.startForeground(10, getNotification())
        ISLocationUpdate = "True"
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        mDateObject = Date()
        STARTED_AT = mDateObject!!.getTime()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mhandler = Handler(Looper.getMainLooper())
     instance = this

        mLocationRequest = LocationRequest.create().apply {
            interval = 1000
            fastestInterval = 1000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        }
        timerRunnable = Runnable {
            if (excecuter != null && excecuter!!.getDelay(TimeUnit.SECONDS) >= 0 && excecuter!!.getDelay(
                    TimeUnit.SECONDS
                ) < 5
            ) {
                UPDATE_INTERVAL += TIMER_INTERVAL
                if (ActivityCompat.checkSelfPermission(
                        this@LocationUpdate,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this@LocationUpdate,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {

                    fusedLocationClient.lastLocation
                        .addOnSuccessListener { location: Location? ->
                            // Got last known location. In some rare situations this can be null.
currentLatitude = location!!.latitude

                            currentLongtitude = location!!.longitude
                            LoginRequestAsyncTask().execute()
                        }

                }
            }
        }

        excecuter = mTimer.scheduleAtFixedRate(
            timerRunnable,
            0,
            TIMER_INTERVAL,
            TimeUnit.MILLISECONDS
        )
        

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }

        createLocationRequest()
    }

    @SuppressLint("MissingPermission")
    fun createLocationRequest() {
        val locationRequest = LocationRequest.create()?.apply {

            locationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult ?: return
                    for (location in locationResult.locations) {

                    }
                }
            }
        }
        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }


    private fun getNotification(): Notification? {
        val activityPendingIntent = PendingIntent.getActivity(
            this, 0, Intent(
                this,
                SplashScreen::class.java
            ), 0
        )
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val NOTIFICATION_CHANNEL_ID = "my_channel_id_01"
        val notifyId = 10
        val notification: Notification
        var builder: Notification.Builder? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val action = Notification.Action.Builder(
                Icon.createWithResource(
                    this,
                    R.drawable.ic_launcher_foreground
                ), "Open App", activityPendingIntent
            ).build()
            val notificationChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                "My Notifications",
                NotificationManager.IMPORTANCE_HIGH
            )
            // Configure the notification channel.
            notificationChannel.description = "Channel description"
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationManager.createNotificationChannel(notificationChannel)
            builder = Notification.Builder(this, NOTIFICATION_CHANNEL_ID)
                .addAction(action)
                .setContentText("App is using location service")
                .setContentTitle(resources.getString(R.string.app_name))
                .setOngoing(true)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setWhen(System.currentTimeMillis())
        } else {
            builder = Notification.Builder(this)
                .addAction(
                    0, "Open App" + "",
                    activityPendingIntent
                )
                .setContentText("App is using location service")
                .setContentTitle(resources.getString(R.string.app_name))
                .setOngoing(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setWhen(System.currentTimeMillis())
        }
        notification = builder.build()
        notificationManager.notify(notifyId, notification)
        return notification
    }


    companion object {
        private const val TAG = "ForegroundOnlyLocationService"

        private const val PACKAGE_NAME = "com.salesc2"

        internal const val ACTION_FOREGROUND_ONLY_LOCATION_BROADCAST =
            "$PACKAGE_NAME.action.FOREGROUND_ONLY_LOCATION_BROADCAST"

        internal const val EXTRA_LOCATION = "$PACKAGE_NAME.extra.LOCATION"
        internal  var ISLocationUpdate = "False"

        private const val EXTRA_CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION =
            "$PACKAGE_NAME.extra.CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION"

        private const val NOTIFICATION_ID = 12345678

        private const val NOTIFICATION_CHANNEL_ID = "com.salesc2_01"
    }

    override fun onLocationChanged(p0: Location?) {

    }

    fun startLocationService(context: Context) {

        if (!serviceIsRunningInForeground(context)) {
            val serviceStartedFrom = context.javaClass.simpleName
            val pushIntent1 = Intent(context, LocationUpdate::class.java)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                pushIntent1.putExtra("started_from", serviceStartedFrom)
                context.startForegroundService(pushIntent1)
            } else {
                pushIntent1.putExtra("started_from", serviceStartedFrom)
                context.startService(pushIntent1)
            }
        }

    }

    fun serviceIsRunningInForeground(context: Context): Boolean {
        val manager = context.getSystemService(
            Context.ACTIVITY_SERVICE
        ) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (LocationUpdate::class.java.name == service.service.className) {
                if (service.foreground) {
                    return true
                }
            }
        }
        return false
    }

    fun GetUTCdatetimeAsString(): String? {
        return "" + System.currentTimeMillis()
    }

    inner class LoginRequestAsyncTask : AsyncTask<String, String, String>()
    {
        override fun onPreExecute() {

            super.onPreExecute()
        }
        @SuppressLint("WrongThread")
        override fun doInBackground(vararg params: String?): String {
            var LatLon = ArrayList<Double>()
            try {
                LatLon.add(currentLongtitude)
                LatLon.add(currentLatitude)
            }catch (e:Exception){

            }

            val params = HashMap<String, Any>()
            /*val jsonArray = JSONArray(listOf(currentLongtitude, currentLatitude))

            params.put("location", jsonArray.toString())*/
            params["location"] = LatLon


            apiRequest.postRequestBodyWithHeadersForLocationService(this@LocationUpdate,webService.location_update,params) { response ->
                try {
                    var status = response.getString("status")
                    if (status == "200")
                    {
                        var token = response.getString("token")
                        var data = response.getString("data")

                    } else if (status == "400")
                    {
                        var msg = response.getString("msg")

                    } else if (status == "401"){
                        var msg = response.getString("msg")

                    } else if (status == "403") {
                        var msg = response.getString("msg")

                    }
                    else if (status == "400") {
                        var msg = response.getString("msg")

                    }
                    else {

                    }
                } catch (e: Exception) {
                    print(e)

                }
            }
            return ""
        }
    }

}
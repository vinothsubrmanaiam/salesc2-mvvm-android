package com.salesc2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AddTask(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "accountId") val accountId: String?,
    @ColumnInfo(name = "departmentId") val departmentId: String?,
    @ColumnInfo(name = "serviceId") val serviceId: String?,
@ColumnInfo(name = "assignedTo") val assignedTo: String?,
@ColumnInfo(name = "startDate") val startDate: String?,
@ColumnInfo(name = "endDate") val endDate: String?,
@ColumnInfo(name = "taskDescription") val taskDescription: String?,
@ColumnInfo(name = "taskStatus") val taskStatus: String?,
@ColumnInfo(name = "products") val products: String?,
@ColumnInfo(name = "priority") val priority: String?

)
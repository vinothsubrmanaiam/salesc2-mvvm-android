package com.salesc2.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.adapters.FilterTerritoryAdapter
import com.salesc2.model.FilterTerritoryModel
import com.salesc2.model.SelectAccountModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.progressBarDismiss
import java.util.*
import kotlin.collections.ArrayList

//not in use

class FilterTerritoryFragment:Fragment() {

    private lateinit var territorySearch : SearchView
    private lateinit var territoryRecyclerView : RecyclerView
    private var filterTerritoryModel = ArrayList<FilterTerritoryModel>()
    var webService = WebService()
    var apiRequest = ApiRequest()
    var territory_toolbar = String()
    private lateinit var territory_toolbar_layout : ConstraintLayout
    private lateinit var mView : View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_filter_territory, container, false)
        territorySearch = mView.findViewById(R.id.filterTerritory_SearchView)
        territory_toolbar = context?.getSharedPref("territory_toolbar",context!!).toString()
        territoryRecyclerView = mView.findViewById(R.id.filterTerritory_recyclerView)
        territory_toolbar_layout = mView.findViewById(R.id.territory_toolbar)
        if (territory_toolbar == "no"){
            territory_toolbar_layout.visibility = View.GONE
        }else{
            territory_toolbar_layout.visibility = View.VISIBLE
        }
        filterTerritoryModel = ArrayList()
        filterTerritoryApiCall()
        searchTerritory()
        activity?.findViewById<ImageView>(R.id.app_icon_imageView)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        return mView
    }



    private var searchtext = String()
    private fun searchTerritory() {
        territorySearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchtext = newText
                filterTerritoryModel.clear()
                filterTerritoryModel = ArrayList()
                filterTerritoryApiCall()
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchtext = query
                filterTerritoryModel.clear()
                filterTerritoryModel = ArrayList()
                filterTerritoryApiCall()

                return true
            }

            override fun equals(other: Any?): Boolean {
                return super.equals(other)

            }


        })
    }
    private fun filterTerritoryApiCall(){
        filterTerritoryModel = ArrayList()
        var params = HashMap<String, String>()
        params.put("search",searchtext)
        context?.let {
            apiRequest.postRequestBodyWithHeaders(it,webService.Territory_filter_url,params){ response->
                println("select_territory_filter== $response")
                var status = response.getString("status")
                if (status == "200"){
                    var dataArray = response.getJSONArray("data")
                    for (i in 0 until dataArray.length()){
                        var dataObj = dataArray.getJSONObject(i)
                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")
                        filterTerritoryModel.add(FilterTerritoryModel(_id,name))
                        context?.progressBarDismiss(context!!)
                    }
                    setupFilterTerritoryRecyclerView(filterTerritoryModel)
                }else{
                    context?.progressBarDismiss(context!!)
                }
            }
        }
    }
    //    setup adapter
    private fun setupFilterTerritoryRecyclerView(filterTerritoryModel: ArrayList<FilterTerritoryModel>) {
        val adapter: RecyclerView.Adapter<*> = FilterTerritoryAdapter(context, filterTerritoryModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        territoryRecyclerView.layoutManager = llm
        territoryRecyclerView.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }
}
package com.salesc2.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.adapters.BillingHistoryAdapter
import com.salesc2.data.network.api.request.RequestBillingHistory
import com.salesc2.data.viewmodel.BillingHistoryViewModel
import com.salesc2.data.viewmodel.UserLoginViewModel
import com.salesc2.model.BillingHistoryModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.getSharedPref
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import org.koin.androidx.viewmodel.ext.android.viewModel


class BillingHistoryFragment : Fragment() {

    var webService = WebService()
    var apiRequest = ApiRequest()

    private lateinit var bh_loc_name : TextView
    private lateinit var bh_region_name : TextView
    private lateinit var bh_recyclerView : RecyclerView
    private lateinit var bh_nestedScroll:NestedScrollView
    private lateinit var bh_progressBar : ProgressBar
    private lateinit var bh_noHistory : TextView
    private lateinit var imageLoc_bh : CircleImageView
    private lateinit var billing_history_back_arrow : ImageView
    private var billingHistoryModel = ArrayList<BillingHistoryModel>()
    var pageLoad = 1
    var lastPage :Int? = null
    private val billingHistoryViewModel by viewModel<BillingHistoryViewModel>()

    private var subscriptionId = String()
    private lateinit var mView : View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_billing_history, container, false)
        subscriptionId = context?.getSharedPref("subscriptionId",context!!).toString()
        bh_loc_name = mView.findViewById(R.id.bh_loc_name)
        bh_region_name = mView.findViewById(R.id.bh_region_name)
        bh_recyclerView = mView.findViewById(R.id.bh_recyclerView)
        bh_nestedScroll = mView.findViewById(R.id.bh_nestedScroll)
        bh_progressBar = mView.findViewById(R.id.bh_progressBar)
        bh_noHistory = mView.findViewById(R.id.bh_noHistory)
        imageLoc_bh = mView.findViewById(R.id.imageLoc_bh)
        billing_history_back_arrow = mView.findViewById(R.id.billing_history_back_arrow)
        billingHistoryModel = ArrayList()
        bh_nestedScroll.smoothScrollTo(0, 0)
        bh_nestedScroll.isSmoothScrollingEnabled = true
        billingHistoryApiCall()
        billingHistoryApiCallwithMVVM()
        companySubDetailsApiCall()
        billing_history_back_arrow.setOnClickListener{
            fragmentManager?.popBackStack()
        }
        return mView
    }

    private fun companySubDetailsApiCall(){
        val params = HashMap<String,Any>()
        params["subscriptionId"] = subscriptionId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.company_subscription_details,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){
                        var dataobj = response.getJSONObject("data")
                        var name = dataobj.getString("name")
                        var logo = dataobj.getString("logo")
                        var type = dataobj.getString("type")
                        var level = dataobj.getString("level")
                        var role = dataobj.getString("role")
                        try {
                            Picasso.get().load(webService.imageBasePath+logo).placeholder(R.drawable.ic_bill_history).into(imageLoc_bh)
                        }catch (e:Exception){
                            println(e)
                        }
                        if (type == "1" || type == "3"){
                            bh_loc_name.text = name
                            if (role.equals("Sales Rep",ignoreCase = true)){
                                bh_region_name.text  = "Representative"
                            }else if (role.equals("Team Lead",ignoreCase = true)){
                                bh_region_name.text ="Regional Manager"
                            }else{
                                bh_region_name.text = role
                            }
                        }else{
                            bh_loc_name.text = name
                            bh_region_name.text = level
                        }

                    }
                }catch (e:Exception){
                    println(e)
                }

            }
        }
    }

    fun  billingHistoryApiCallwithMVVM()
    {
        billingHistoryViewModel.reqBillingHistory(RequestBillingHistory(
            subscriptionId =subscriptionId,
            page = pageLoad,
            pageSize = 20
        ),{
            println("billing_history"+" "+it)

        },{

        })
    }
    private fun billingHistoryApiCall(){
        val params = HashMap<String,Any>()
        params["subscriptionId"] = subscriptionId
        params["page"] = pageLoad
        params["pageSize"] = 20
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.billing_history_url,params){ response->

                println("billing_history"+" "+webService.billing_history_url)
                println("billing_history"+" "+params)
                try {
                    println("billing_history"+" "+response)

                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200") {
                        billingHistoryModel.clear()
                        billingHistoryModel = ArrayList()
                        var dataArray = response.getJSONArray("data")
                        if (dataArray.length() <= 0){
                            bh_noHistory.visibility = View.VISIBLE
                            bh_recyclerView.visibility = View.GONE
                        }
                        else{
                            bh_noHistory.visibility = View.GONE
                            bh_recyclerView.visibility = View.VISIBLE
                        }
                        for (i in 0 until dataArray.length()) {
                            var dataObj = dataArray.getJSONObject(i)

                            var _id = dataObj.getString("_id")
                            var amount = dataObj.getString("amount")
                            var startDate = dataObj.getString("startDate")
                            var createdAt = dataObj.getString("createdAt")
                            var invoiceNumber = dataObj.getString("invoiceNumber")
                            var status = dataObj.getString("status")
                            var subscription = dataObj.getString("subscription")
                            var billingCycle = dataObj.getString("billingCycle")
                            billingHistoryModel.add(BillingHistoryModel(_id, amount, startDate, createdAt, invoiceNumber, status, subscription, billingCycle))
                        }
                        var pagerObj = response.getJSONObject("pager")
                        var totalItems = pagerObj.getString("totalItems")
                        var currentPage = pagerObj.getString("currentPage")
                        var endPage = pagerObj.getString("endPage")
                        lastPage = endPage.toInt()
                        var totalPages = pagerObj.getString("totalPages")


                        setupBillingHistoryRecyclerView(billingHistoryModel)
                    }
                }catch (e:Exception){
                    println(e)
                }

            }
        }
    }
    private fun setupBillingHistoryRecyclerView(billingHistoryModel: ArrayList<BillingHistoryModel>) {
        val adapter: RecyclerView.Adapter<*> = BillingHistoryAdapter(context, billingHistoryModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        bh_recyclerView.layoutManager = llm
        bh_recyclerView.adapter = adapter
        bh_nestedScroll.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            if (v.getChildAt(0).bottom <= bh_nestedScroll.height + scrollY) {
                bh_progressBar.visibility = View.VISIBLE
                if (adapter.itemCount != 0) {
                    val lastVisibleItemPosition =
                        llm.findLastCompletelyVisibleItemPosition()
                    if (lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == adapter.itemCount - 1) {
                        bh_progressBar.visibility = View.VISIBLE
                        if (pageLoad != lastPage)
                        {
                            billingHistoryApiCall()
                            pageLoad++
                            adapter.notifyDataSetChanged()
                        } else {
                            bh_progressBar.visibility = View.GONE
                        }
                    }
                }

            }

        })
    }
}
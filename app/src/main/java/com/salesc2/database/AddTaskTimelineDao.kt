package com.salesc2.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface AddTaskTimelineDao {
    @Query("SELECT * FROM AddTaskTimeline")
    suspend fun getAll(): List<AddTaskTimeline>

    @Insert
    suspend fun insertAll(vararg addTaskTimeline: AddTaskTimeline)

    @Query("DELETE FROM AddTaskTimeline")
    fun deleteAll()

    @Query("UPDATE AddTaskTimeline SET type1=:timeline WHERE  timeline_id= :t_id")
    fun updateTimelineType1(timeline: String?, t_id: String)

    @Query("UPDATE AddTaskTimeline SET type2=:timeline WHERE  timeline_id= :t_id")
    fun updateTimelineType2(timeline: String?, t_id: String)

    @Query("UPDATE AddTaskTimeline SET type3=:timeline WHERE  timeline_id= :t_id")
    fun updateTimelineType3(timeline: String?, t_id: String)

    @Query("UPDATE AddTaskTimeline SET type4=:timeline WHERE  timeline_id= :t_id")
    fun updateTimelineType4(timeline: String?, t_id: String)

    @Query("UPDATE AddTaskTimeline SET type5=:timeline WHERE  timeline_id= :t_id")
    fun updateTimelineType5(timeline: String?, t_id: String)

    @Query("UPDATE AddTaskTimeline SET type6=:timeline WHERE  timeline_id= :t_id")
    fun updateTimelineType6(timeline: String?, t_id: String)

    @Query("UPDATE AddTaskTimeline SET type7=:timeline WHERE  timeline_id= :t_id")
    fun updateTimelineType7(timeline: String?, t_id: String)

    @Query("UPDATE AddTaskTimeline SET type8=:timeline WHERE  timeline_id= :t_id")
    fun updateTimelineType8(timeline: String?, t_id: String)

    @Query("UPDATE AddTaskTimeline SET type9=:timeline WHERE  timeline_id= :t_id")
    fun updateTimelineType9(timeline: String?, t_id: String)

    @Query("UPDATE AddTaskTimeline SET type10=:timeline WHERE  timeline_id= :t_id")
    fun updateTimelineType10(timeline: String?, t_id: String)

    @Query("UPDATE AddTaskTimeline SET type11=:timeline WHERE  timeline_id= :t_id")
    fun updateTimelineType11(timeline: String?, t_id: String)

    @Query("DELETE FROM AddTaskTimeline WHERE timeline_id = :tid")
    fun deleteById(tid: Int)
}
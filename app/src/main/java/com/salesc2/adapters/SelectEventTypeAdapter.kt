package com.salesc2.adapters

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.common.data.DataHolder
import com.salesc2.R
import com.salesc2.fragment.AddEventFragment
import com.salesc2.fragment.SelectEventDateTimeFragment
import com.salesc2.fragment.SelectEventLocationFragment
import com.salesc2.model.SelectEventTypeModel
import com.salesc2.model.TeamMemberListModel
import com.salesc2.utils.ShowNoData
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.loadFragment
import com.salesc2.utils.setSharedPref
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList


class SelectEventTypeAdapter(context: Context?, val showdata: ShowNoData, var eItems: ArrayList<SelectEventTypeModel>):RecyclerView.Adapter<SelectEventTypeAdapter.MyViewHolder>() ,Filterable{

     var eTypeItems=ArrayList<SelectEventTypeModel>()
    var context: Context? = null
  var eType_type = String()
   private var resultCount:Int = 0
    private var row_index = -1
  init{
        this.context = context
        eTypeItems = eItems

    }
    private var editAddEvent = String()
    private var selected_eType_position = String()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_select_list, parent, false)
        eType_type = context?.getSharedPref("eType_type",context!!).toString()
        editAddEvent =  context?.getSharedPref("edit_add_event",context!!).toString()
        selected_eType_position = context?.getSharedPref("selected_eType_position",context!!).toString()
        return MyViewHolder(view)
    }


    override fun getItemCount(): Int {
        return eTypeItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       var selectEventTypeModel : SelectEventTypeModel = eTypeItems[position]
        holder.list_name.text = selectEventTypeModel.et_name
        selected_eType_position = context?.getSharedPref("selected_eType_position",context!!).toString()

        if (selected_eType_position != "null"){
            row_index=selected_eType_position.toInt()
            if (row_index.equals(selected_eType_position)){
                holder.select_icon.visibility = View.VISIBLE
            }
        }

        holder.itemView.setOnClickListener{
            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("selected_eType_position")
            editor.commit()
            row_index=position
            notifyDataSetChanged()
            if (eType_type == "1"){
                holder.select_icon.visibility = View.VISIBLE
                context?.let { ContextCompat.getColor(it, R.color.offwhite2) }?.let {
                    holder.list_layout.setBackgroundColor(it)
                }
                context?.setSharedPref(context!!,"eType_name",selectEventTypeModel.et_name)
                context?.setSharedPref(context!!,"eType_id",selectEventTypeModel.et_id)

                if (editAddEvent == "101"){
                    context?.loadFragment(context!!,AddEventFragment())
                }else {
                    context?.loadFragment(context!!,SelectEventLocationFragment())
                }
            }
            else if(eType_type == "0"){
                holder.select_icon.visibility = View.VISIBLE
                context?.let { ContextCompat.getColor(it, R.color.offwhite2) }?.let {
                    holder.list_layout.setBackgroundColor(it)
                }
                context?.setSharedPref(context!!,"eType_name",selectEventTypeModel.et_name)
                context?.setSharedPref(context!!,"eType_id",selectEventTypeModel.et_id)
                if (editAddEvent == "101"){
                    context?.loadFragment(context!!,AddEventFragment())
                }else {
                    context?.loadFragment(context!!,SelectEventDateTimeFragment())
                }
            }


        }
        if(row_index==position){
            holder.select_icon.visibility = View.VISIBLE
            context?.setSharedPref(context!!,"selected_eType_position",position.toString())

        }else{
            holder.select_icon.visibility = View.GONE
        }


    }

    fun updateList(list: ArrayList<SelectEventTypeModel>) {
        list.clear()
        eTypeItems = list
        notifyDataSetChanged()
    }

    class MyViewHolder constructor(itemView:View):RecyclerView.ViewHolder(itemView){
        var list_name : TextView = itemView.findViewById(R.id.select_list_name)
        val list_layout : ConstraintLayout = itemView.findViewById(R.id.list_layout)
        var select_icon : ImageView = itemView.findViewById(R.id.select_icon)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    showdata.hideNoData()
                    eTypeItems = eItems

                } else {
                    val resultList = java.util.ArrayList<SelectEventTypeModel>()
                    for (row in eItems) {
                        if (row.et_name.toLowerCase(Locale.ROOT).contains(charSearch.toLowerCase(Locale.ROOT))) {
                            showdata.hideNoData()
                            resultList.add(row)
                        } else{
                            if (resultList.size==0)
                            {
                                showdata.showNoData()
                            }

                        }
                    }
                    eTypeItems = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = eTypeItems
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                try {
                    eTypeItems = results?.values as java.util.ArrayList<SelectEventTypeModel>
                    notifyDataSetChanged()
                }catch (e:Exception){
                }

            }

        }
    }
}
package com.salesc2.service

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.salesc2.R
import java.util.*

class PlacePickerDemoPage :Activity(){

    lateinit var mView : View
    private var PLACE_PICKER_REQUEST = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.place_picker)
        placeAutoComplete()
        super.onCreate(savedInstanceState)
    }

    private fun placeAutoComplete() {
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, "AIzaSyD6L3A5wCPVO2mt3KeLTZ-fnl2kIEAs690", Locale.US);
        }
        var fields=Arrays.asList(Place.Field.ID,Place.Field.NAME,Place.Field.LAT_LNG)
        var intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(this)
        startActivityForResult(intent, PLACE_PICKER_REQUEST)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                val place =Autocomplete.getPlaceFromIntent(data)
               var lat = place.latLng?.latitude
                var lng = place.latLng?.longitude
                println("latPicker== $lat \n lngPicker== $lng")
            }
            else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                var status = Autocomplete.getStatusFromIntent(data)
                Log.i("address", status.getStatusMessage().toString());
                print("addressPicker== ${status.statusMessage}")
            }
        }
    }
}
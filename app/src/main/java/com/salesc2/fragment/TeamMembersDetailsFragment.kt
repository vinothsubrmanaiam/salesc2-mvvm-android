package com.salesc2.fragment

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.ivbaranov.mli.MaterialLetterIcon
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.google.android.material.textfield.TextInputEditText
import com.kaopiz.kprogresshud.KProgressHUD
import com.salesc2.R
import com.salesc2.adapters.UpcomingTasksAdapter
import com.salesc2.model.UpcomingTasksModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_account_detail_information.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TeamMembersDetailsFragment : Fragment() {

    private lateinit var mView: View
    private var webService = WebService()
    private var apiRequest = ApiRequest()

    private lateinit var team_member_image :ImageView
    private lateinit var e_role : TextView
    private lateinit var e_name : TextView
    private lateinit var e_phone : TextInputEditText
    private lateinit var e_email : TextInputEditText
    private lateinit var e_region : TextInputEditText
    private lateinit var e_territory : TextInputEditText
    private lateinit var goback : ImageView
    private lateinit var noDataFound_layout : ConstraintLayout

    private lateinit var upcomingTaskRecyclerView: RecyclerView
    private var upcomingTasksModel = ArrayList<UpcomingTasksModel>()

    private var teamMember_id = String()
    var authToken = String()
    private lateinit var letterImage_userdetail : MaterialLetterIcon
    private lateinit var upcoming_noDataFound_layout : ConstraintLayout


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_team_member_details, container, false)
        teamMember_id = arguments?.getString("id").toString()

        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        authToken = context?.let { activity?.getSharedPref("token", it).toString() }.toString()

        team_member_image = mView.findViewById(R.id.team_member_detail_iv)
        e_role = mView.findViewById(R.id.team_member_detail_job_tv)
        e_name = mView.findViewById(R.id.team_member_name)
        e_phone = mView.findViewById(R.id.team_member_detail_phone)
        e_email = mView.findViewById(R.id.team_member_detail_email)
        e_region = mView.findViewById(R.id.team_member_detail_region)
        e_territory = mView.findViewById(R.id.team_member_detail_territory)
        goback = mView.findViewById(R.id.member_detail_back_arrow)
        letterImage_userdetail = mView.findViewById(R.id.letterImage_userdetail)
        noDataFound_layout = mView.findViewById(R.id.noDataFound_layout)

        upcoming_noDataFound_layout = mView.findViewById(R.id.upcoming_noDataFound_layout)
        upcomingTaskRecyclerView = mView.findViewById(R.id.upcoming_task_recyclerView)
        upcomingTasksModel = ArrayList()
        TeamMembersDetailsAsyncTask().execute()
        goBack_Action()
        callAndEmailClick()
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }
    private fun goBack_Action(){
        goback.setOnClickListener(View.OnClickListener {
            fragmentManager?.popBackStack()
        })

    }
    private fun callAndEmailClick(){
        e_phone.setOnClickListener{
           /* try {

                var callIntent: Intent =
                    Intent(Intent.ACTION_DIAL, Uri.parse("tel: ${e_phone.text.toString().toInt()}"))
                context?.startActivity(callIntent)
            } catch (activityException: ActivityNotFoundException) {
                Log.e("Calling a Phone Number", "Call failed", activityException)
            }catch (e:Exception){

            }*/
            try {
                var callIntent: Intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel: ${(e_phone.text.toString().trim().replace("+","").replace("-","").replace("\"","")).toLong()}"))
                context?.startActivity(callIntent)
            }catch (e:Exception){

            }
        }
        e_email.setOnClickListener{
            val TO = arrayOf(e_email.text.toString())
            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.data = Uri.parse("mailto:")
            emailIntent.type = "text/plain"
            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
// emailIntent.putExtra(Intent.EXTRA_CC, CC);
            // emailIntent.putExtra(Intent.EXTRA_CC, CC);
//            emailIntent.putExtra(Intent.EXTRA_SUBJECT,)

            emailIntent.setPackage("com.google.android.gm")


            try {
                context?.startActivity(Intent.createChooser(emailIntent, "Send mail..."))
            } catch (ex: ActivityNotFoundException) {
            }catch (e:Exception){

            }
        }
    }


    private var progressHuD : KProgressHUD? = null
    inner class TeamMembersDetailsAsyncTask : AsyncTask<String, String, String>() {

        override fun onPreExecute() {
            super.onPreExecute()
            progressHuD = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                //.setDetailsLabel("Downloading data")
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .setBackgroundColor(57000000)
                .show()
        }

        override fun doInBackground(vararg params: String?): String {
            val params = HashMap<String, String>()
            params.put("Authorization", "Bearer $authToken")
            context?.let {
                apiRequest.getRequest(
                    it,
                    webService.team_members_details_url + "/$teamMember_id",
                    params
                ) { response ->
                    var responseStatus = response.getString("status")
                    var msg = response.getString("msg")
                    if (responseStatus == "200") {

                        var viewData = response.getJSONArray("viewData")
                        for (i in 0 until viewData.length()) {
                            var dataObj = viewData.getJSONObject(i)
                            var eid = dataObj.getString("_id")
                            var name = dataObj.getString("name")
                            e_name.text = name

                            var companyId = dataObj.getString("companyId")
                            var level = dataObj.getString("level")
                            var levelId = dataObj.getString("levelId")
                            var levelName = dataObj.getString("levelName")
                            if (levelName != "null" && level == "region"){
                                e_region.setText(levelName)
                                e_territory.setText("NA")
                            }else{
                                e_territory.setText(levelName)
                                e_region.setText("NA")
                            }

                            var regionName = dataObj.getString("regionName")

                            var role = dataObj.getString("role")
                            if (role.equals("Sales Rep",ignoreCase = true)){
                                e_role.text  = context?.capFirstLetter("Representative")
                            }else if (role.equals("Team Lead",ignoreCase = true)){
                                e_role.text ="Regional Manager"
                            }else{
                                e_role.text = role
                            }


                            var email_id = dataObj.getString("email")
                            e_email.setText(email_id)

                            var phone = dataObj.getString("phone")
                            e_phone.setText(phone)
                            var profilePath = String()
                            if (dataObj.has("profilePath")) {
                                profilePath = dataObj.getString("profilePath")
                            }
                            else{
                                profilePath = ""
                            }
                            var imgWithBase = webService.imageBasePath+profilePath

                            var initials: String = e_name.text.toString()
                                .split(' ')
                                .mapNotNull { it.firstOrNull()?.toString() }
                                .reduce { acc, s -> acc + s }
                            if(profilePath == ""){
                               letterImage_userdetail.visibility = View.VISIBLE
                                letterImage_userdetail.letter = initials
                            }else{
                                letterImage_userdetail.visibility = View.GONE
                                Picasso.get().load(imgWithBase).placeholder(R.drawable.ic_dummy_user_pic).into(team_member_image)
                            }

                           var status = dataObj.getString("status")

                            var taskDetail = dataObj.getJSONArray("taskDetail")
                            if (taskDetail.length() <1){
                                upcoming_noDataFound_layout.visibility = View.VISIBLE
                            }else{
                                upcoming_noDataFound_layout.visibility = View.GONE
                            }
                            for (j in 0 until taskDetail.length()){
                                var taskDetailObj = taskDetail.getJSONObject(j)

                                var assignedBy = taskDetailObj.getString("assignedBy")

                                var taskName = taskDetailObj.getString("taskName")

                                var taskDescription = taskDetailObj.getString("taskDescription")

                                var taskStatus = taskDetailObj.getString("taskStatus")

                                var startDate = taskDetailObj.getString("startDate")
                                var sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                                val cal = Calendar.getInstance()
                                var tz:TimeZone = cal.timeZone
                                var format = SimpleDateFormat("dd-MM-yyyy")
                                format.timeZone = TimeZone.getTimeZone(tz.displayName)
                                val month_parse: Date = sdf.parse(startDate)
                                val month_date: String = format.format(month_parse)


                                var endDate = taskDetailObj.getString("endDate")

                                var _id = taskDetailObj.getString("_id")
                                upcomingTasksModel.add(UpcomingTasksModel(month_date,taskName))
                            }
                            setupUpcomingTaskRecyclerView(upcomingTasksModel)
                            progressHuD?.dismiss()



                        }

                    }else{
                        progressHuD?.dismiss()
                        activity?.alertDialog(context!!,"Alert",msg)
                    }

                }
            }

            return ""
        }

    }

    //    setup adapter
    private fun setupUpcomingTaskRecyclerView(upcomingTasksModel: ArrayList<UpcomingTasksModel>) {
        val adapter: RecyclerView.Adapter<*> =
            UpcomingTasksAdapter(
                context,
                upcomingTasksModel
            )
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        upcomingTaskRecyclerView.layoutManager = llm
        upcomingTaskRecyclerView.adapter = adapter

    }

    override fun onDestroy() {
        super.onDestroy()
        progressHuD?.dismiss()
    }

    override fun onDetach() {
        super.onDetach()
        progressHuD?.dismiss()
    }


}
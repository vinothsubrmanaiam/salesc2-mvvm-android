package com.salesc2.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.R
import com.salesc2.adapters.DashboardTerritoryFilterAdapter
import com.salesc2.adapters.FilterTerritoryAdapter
import com.salesc2.model.FilterTerritoryModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.loadFragment
import com.salesc2.utils.progressBarDismiss
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import java.util.HashMap

class DashboardTerritoryFilterFragment : Fragment() {
    private lateinit var territorySearch : SearchView
    private lateinit var territoryRecyclerView : RecyclerView
    private var filterTerritoryModel = ArrayList<FilterTerritoryModel>()
    var webService = WebService()
    var apiRequest = ApiRequest()
    private lateinit var goback: ImageView
    private lateinit var territoryHead: TextView
    private lateinit var close : ImageView
    private lateinit var mView : View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_filter_territory, container, false)
        territorySearch = mView.findViewById(R.id.filterTerritory_SearchView)
        territoryRecyclerView = mView.findViewById(R.id.filterTerritory_recyclerView)
        goback = mView.findViewById(R.id.territory_back_arrow)
        territoryHead = mView.findViewById(R.id.territoryHead)
        close = mView.findViewById(R.id.close_iv)

        filterTerritoryModel = ArrayList()
        filterTerritoryApiCall()
        searchTerritory()

        territoryHead.text = "Territory"
        activity?.findViewById<ImageView>(R.id.app_icon_imageView)?.visibility = View.GONE
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        closeAction()
        goBackAction()
        return mView
    }
    private fun closeAction(){
        close.setOnClickListener{
            context?.loadFragment(context!!, DashboardFragment())
        }
    }
    private fun goBackAction(){
        goback.setOnClickListener(View.OnClickListener {
            context?.loadFragment(context!!, DashboardFragment())
        })
    }


    private var searchtext = String()
    private fun searchTerritory() {
        territorySearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchtext = newText
                filterTerritoryModel.clear()
                filterTerritoryApiCall()
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchtext = query
                filterTerritoryModel.clear()
                filterTerritoryApiCall()

                return true
            }

            override fun equals(other: Any?): Boolean {
                return super.equals(other)

            }


        })
    }
    private fun filterTerritoryApiCall(){
        var params = HashMap<String, String>()
        params.put("search",searchtext)
        context?.let {
            apiRequest.postRequestBodyWithHeaders(it,webService.Territory_filter_url,params){ response->
                var status = response.getString("status")
                if (status == "200"){
                    filterTerritoryModel.clear()
                    filterTerritoryModel = ArrayList()
                    var dataArray = response.getJSONArray("data")
                    for (i in 0 until dataArray.length()){
                        var dataObj = dataArray.getJSONObject(i)
                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")
                        filterTerritoryModel.add(FilterTerritoryModel(_id,name))
                        context?.progressBarDismiss(context!!)
                    }
                    setupFilterTerritoryRecyclerView(filterTerritoryModel)
                }else{
                    context?.progressBarDismiss(context!!)
                }
            }
        }
    }
    //    setup adapter
    private fun setupFilterTerritoryRecyclerView(filterTerritoryModel: ArrayList<FilterTerritoryModel>) {
        val adapter: RecyclerView.Adapter<*> = DashboardTerritoryFilterAdapter(context, filterTerritoryModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        territoryRecyclerView.layoutManager = llm
        territoryRecyclerView.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }
}
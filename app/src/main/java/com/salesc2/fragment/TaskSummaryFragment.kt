package com.salesc2.fragment

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.R
import com.salesc2.adapters.TaskSummaryAdapter
import com.salesc2.data.network.api.request.RequestTaskSummary
import com.salesc2.data.viewmodel.TaskSummaryViewModel
import com.salesc2.model.TaskSummaryModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.dateToUTC
import com.salesc2.utils.getSharedPref
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import kotlinx.coroutines.delay
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class TaskSummaryFragment : Fragment() {


    private lateinit var ts_today : TextView
    private lateinit var ts_week : TextView
    private lateinit var ts_month : TextView
    private lateinit var ts_quarter : TextView
    private lateinit var ts_ack_arrow : ImageView
    private lateinit var task_summary_recyclerView : RecyclerView
    private var taskSummaryModel = ArrayList<TaskSummaryModel>()
    var myFilterDateFormat= SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    private val taskSummaryViewModel by viewModel<TaskSummaryViewModel>()

    var apiRequest = ApiRequest()
    var webService = WebService()

    private var set_timezone = String()

    lateinit var mView : View
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_task_summary, container, false)
        ts_today = mView.findViewById(R.id.ts_today)
        ts_week = mView.findViewById(R.id.ts_week)
        ts_month = mView.findViewById(R.id.ts_month)
        ts_quarter = mView.findViewById(R.id.ts_quarter)
        ts_ack_arrow = mView.findViewById(R.id.ts_ack_arrow)
        task_summary_recyclerView = mView.findViewById(R.id.task_summary_recyclerView)
        taskSummaryModel = ArrayList()
        set_timezone = context?.getSharedPref("set_timezone",context!!).toString()
        filterOnCliCkAction()
        var now= if (set_timezone != "null"){
            LocalDateTime.now(ZoneId.of(set_timezone))
        }else{
            LocalDateTime.now()
        }
        var todayDtFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        var dateToday = todayDtFormat.format(now)+" 00:00:00"
        var dateTodayStDate = dateToday.toDate()
        var dateTodayStUTC = context?.dateToUTC(context!!,dateTodayStDate)

        var stToday = myFilterDateFormat.format(dateTodayStUTC)


        var dateTodayEnd = todayDtFormat.format(now)+" 23:59:59"
        var dateTodayEndDate = dateTodayEnd.toDate()
        var dateTodayEndUTC = context?.dateToUTC(context!!,dateTodayEndDate)
        var edToday = myFilterDateFormat.format(dateTodayEndUTC)

        var Dparams = HashMap<String,Any>()
//            Dparams["today"] = dateToday
        Dparams["startDate"] = stToday
        Dparams["endDate"] =edToday
        taskSummaryModel.clear()
        if(webService.typeservice.equals("Stage"))
        {
            taskSummaryModel.clear()
            taskSummaryApiCallMVVM(stToday,edToday)
        }
        else
        {
            taskSummaryApiCall(Dparams)

        }


        context?.let { ContextCompat.getColor(it,R.color.green) }?.let {
            ts_today.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.sides_green_body_transparent_green_corner_shape) }?.let {
            ts_today.setBackgroundDrawable(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
            ts_week.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
            ts_week.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
            ts_month.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
            ts_month.setBackgroundDrawable(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
            ts_quarter.setTextColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
            ts_quarter.setBackgroundDrawable(it)
        }

        ts_ack_arrow.setOnClickListener{
            fragmentManager?.popBackStack()
        }

        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    @RequiresApi(Build.VERSION_CODES.O)
     fun filterOnCliCkAction(){
        ts_today.setOnClickListener{

            var now = if (set_timezone != "null"){
                LocalDateTime.now(ZoneId.of(set_timezone))
            }else{
                LocalDateTime.now()
            }
            var todayDtFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            var dateToday = todayDtFormat.format(now)+" 00:00:00"
            var dateTodayStDate = dateToday.toDate()
            var dateTodayStUTC = context?.dateToUTC(context!!,dateTodayStDate)
            var stToday = myFilterDateFormat.format(dateTodayStUTC)


            var dateTodayEnd = todayDtFormat.format(now)+" 23:59:59"
            var dateTodayEndDate = dateTodayEnd.toDate()
            var dateTodayEndUTC = context?.dateToUTC(context!!,dateTodayEndDate)
            var edToday = myFilterDateFormat.format(dateTodayEndUTC)

            var Dparams = HashMap<String,Any>()
//            Dparams["today"] = dateToday
            Dparams["startDate"] = stToday
            Dparams["endDate"] =edToday
            taskSummaryModel.clear()
            if(webService.typeservice.equals("Stage"))
            {
                taskSummaryApiCallMVVM(stToday,edToday)
            }
            else
            {
                taskSummaryApiCall(Dparams)
            }



            context?.let { ContextCompat.getColor(it,R.color.green) }?.let {
                ts_today.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_green_body_transparent_green_corner_shape) }?.let {
                ts_today.setBackgroundDrawable(it)
            }

            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                ts_week.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
                ts_week.setBackgroundDrawable(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                ts_month.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
                ts_month.setBackgroundDrawable(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                ts_quarter.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
                ts_quarter.setBackgroundDrawable(it)
            }
        }
        ts_week.setOnClickListener{
            var startWeek = getWeekStartDate()
            var endWeek = getWeekEndDate()
            var myDateformat = SimpleDateFormat("yyyy-MM-dd")
            var myWeekStartFormat = myDateformat.format(startWeek)+ " 00:00:00"
            var myWeekStartFormatToDate = myWeekStartFormat.toDate()
            var myWeekStUTC = context?.dateToUTC(context!!,myWeekStartFormatToDate)
            var myWeekStFormat = myFilterDateFormat.format(myWeekStUTC)

            var myWeekEndFormat = myDateformat.format(endWeek)+ " 23:59:59"
            var myWeekEndFormatToDate = myWeekEndFormat.toDate()
            var myWeekEdUTC = context?.dateToUTC(context!!,myWeekEndFormatToDate)
            var myWeekEdFormat = myFilterDateFormat.format(myWeekEdUTC)

            context?.let { ContextCompat.getColor(it,R.color.green) }?.let {
                ts_week.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_green_body_transparent_green_corner_shape) }?.let {
                ts_week.setBackgroundDrawable(it)
            }

            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                ts_today.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
                ts_today.setBackgroundDrawable(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                ts_month.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
                ts_month.setBackgroundDrawable(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                ts_quarter.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
                ts_quarter.setBackgroundDrawable(it)
            }


            var Wparams = HashMap<String,Any>()
            Wparams["startDate"] = myWeekStFormat
            Wparams["endDate"] = myWeekEdFormat
            taskSummaryModel.clear()

            if(webService.typeservice.equals("Stage"))
            {
                taskSummaryApiCallMVVM(myWeekStFormat,myWeekEdFormat)

            }
            else{
                taskSummaryApiCall(Wparams)
            }





        }
        ts_month.setOnClickListener{
       var dayOfMonth = getCurrentMonthFirstDate()
            var dayEndMonth = getCurrentMonthLastDate()
            var myDateformat = DateTimeFormatter.ofPattern("yyyy-MM-dd")
//            myDateformat.timeZone = TimeZone.getTimeZone("UTC")
            var dayofMonthFormat = myDateformat.format(dayOfMonth)+" 00:00:00"
            var dayOfMonthToDate= dayofMonthFormat.toDate()
           var dayOfMonthUTC= context?.dateToUTC(context!!,dayOfMonthToDate)
            var dayOfMonthUTCFormat= myFilterDateFormat.format(dayOfMonthUTC)
            var dayEndMonthFormat = myDateformat.format(dayEndMonth)+" 23:59:59"
            var dayEndMonthToDate = dayEndMonthFormat.toDate()
            var dayEndMonthUTC =context?.dateToUTC(context!!,dayEndMonthToDate)
            var dayEndMonthUTCFormat = myFilterDateFormat.format(dayEndMonthUTC)
            context?.let { ContextCompat.getColor(it,R.color.green) }?.let {
                ts_month.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_green_body_transparent_green_corner_shape) }?.let {
                ts_month.setBackgroundDrawable(it)
            }

            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                ts_today.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
                ts_today.setBackgroundDrawable(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                ts_week.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
                ts_week.setBackgroundDrawable(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                ts_quarter.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
                ts_quarter.setBackgroundDrawable(it)
            }


            var Mparams = HashMap<String,Any>()

            Mparams["startDate"] = dayOfMonthUTCFormat
            Mparams["endDate"] = dayEndMonthUTCFormat
            taskSummaryModel.clear()

            if(webService.typeservice.equals("Stage"))
            {
                taskSummaryApiCallMVVM(dayOfMonthUTCFormat,dayEndMonthUTCFormat)

            }
            else
            {
                taskSummaryApiCall(Mparams)

            }
        }
        ts_quarter.setOnClickListener{
            var now = if (set_timezone != "null"){
                LocalDateTime.now(ZoneId.of(set_timezone))
            }else{
                LocalDateTime.now()
            }
//        dateSelected = dtf.format(now)
            var todayDtFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
           var NowDate = todayDtFormat.format(now)
            var dtf = SimpleDateFormat("dd/MM/yyyy")
            var dateToday = dtf.parse(NowDate)

       var dayStartQuarter = getFirstDayOfQuarter(dateToday)
            var dayStartQFormat = SimpleDateFormat("yyyy-MM-dd")
            var dayStQ = dayStartQFormat.format(dayStartQuarter)+" 00:00:00"
            var dayStQToDate = dayStQ.toDate()
            var dayStQUTC = context?.dateToUTC(context!!,dayStQToDate)
            var dayStQUTCFormat = myFilterDateFormat.format(dayStQUTC)


            var dayEndQuarter = getLastDayOfQuarter(dateToday)
            var dayEdQ = dayStartQFormat.format(dayEndQuarter)+" 23:59:59"
            var dayEdQToDate = dayEdQ.toDate()
            var dayEdQUTC = context?.dateToUTC(context!!,dayEdQToDate)
            var dayEdQUTCFormat = myFilterDateFormat.format(dayEdQUTC)

            context?.let { ContextCompat.getColor(it,R.color.green) }?.let {
                ts_quarter.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_green_body_transparent_green_corner_shape) }?.let {
                ts_quarter.setBackgroundDrawable(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                ts_today.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
                ts_today.setBackgroundDrawable(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                ts_month.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
                ts_month.setBackgroundDrawable(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                ts_week.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.sides_grey_body_lightgrey_corner_shape) }?.let {
                ts_week.setBackgroundDrawable(it)
            }


            taskSummaryModel.clear()
            var Qparam = HashMap<String,Any>()
            Qparam["startDate"] = dayStQUTCFormat
            Qparam["endDate"] = dayEdQUTCFormat

            if(webService.typeservice.equals("Stage"))
            {
                taskSummaryApiCallMVVM(dayStQUTCFormat,dayEdQUTCFormat)

            }
            else
            {
                taskSummaryApiCall(Qparam)
            }



        }
    }

    private fun getWeekStartDate(): Date? {
        val calendar = Calendar.getInstance()
        while (calendar[Calendar.DAY_OF_WEEK] !== Calendar.SUNDAY) {
            calendar.add(Calendar.DATE, -1)
        }
        return calendar.time
    }
    private fun getMonthStartDate(): Date? {
        val calendar = Calendar.getInstance()
        while (calendar[Calendar.DAY_OF_MONTH] !== Calendar.SATURDAY) {
            calendar.add(Calendar.DATE, -1)
        }
        return calendar.time
    }

    private fun getWeekEndDate(): Date? {
        val calendar = Calendar.getInstance()
        while (calendar[Calendar.DAY_OF_WEEK] !== Calendar.SATURDAY) {
            calendar.add(Calendar.DATE, 1)
        }
        calendar.add(Calendar.DATE, -1)
        return calendar.time
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getCurrentMonthFirstDate():LocalDate {
        return LocalDate.ofEpochDay(System.currentTimeMillis() / (24 * 60 * 60 * 1000)).withDayOfMonth(1)
    }
    @RequiresApi(Build.VERSION_CODES.O)
    fun getCurrentMonthLastDate():LocalDate {
        return LocalDate.ofEpochDay(System.currentTimeMillis() / (24 * 60 * 60 * 1000)).plusMonths(1).withDayOfMonth(1).minusDays(1)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getFirstDayOfQuarter(date: Date?):Date {
        val cal = Calendar.getInstance()
        cal.setTime(date)
        cal.set(Calendar.DAY_OF_MONTH, 1)
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) / 3 * 3)
        return cal.getTime()
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun getLastDayOfQuarter(date: Date?):Date {
        val cal = Calendar.getInstance()
        cal.setTime(date)
        cal.set(Calendar.DAY_OF_MONTH, 1)
        cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) / 3 * 3 + 2)
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH))
        return cal.getTime()
    }

    fun taskSummaryApiCallMVVM(stToday: String, edToday: String)
    {
        taskSummaryModel.clear()
        taskSummaryViewModel.reqGetTaskSummary(
            RequestTaskSummary(
                startDate = stToday,
                endDate= edToday
            ),{


                for (i in 0 until it.taskSummary.size ){
                    println("Output_taskSummary"+" "+  it.taskSummary.get(i).status)
                    var ts_status = it.taskSummary.get(i).status
                    var count = it.taskSummary.get(i).count
                    taskSummaryModel.add(TaskSummaryModel(ts_status,count))
                }

                lifecycleScope.launchWhenResumed {
                    delay(500)
                    setupTaskSummaryRecyclerView(taskSummaryModel)
                }

            },{

            }
        )
    }

    private fun taskSummaryApiCall(param: HashMap<String,Any>){

//        var param = HashMap<String,Any>()
//        param["today"] = "2020-07-31"

        println("Task_summary"+" "+" "+ param)
        println("Task_summary"+" "+" "+ webService.TaskSummary_url)
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.TaskSummary_url,param){ response->
                println("Task_summary"+" "+" "+ response)

                try {
                    var status = response.getString("status")
                    if (status == "200"){
                        var taskSummaryArray = response.getJSONArray("taskSummary")
                        for (i in 0 until taskSummaryArray.length() ){
                            var taskSummaryObj = taskSummaryArray.getJSONObject(i)

                            var ts_status = taskSummaryObj.getString("status")
                            var count = taskSummaryObj.getString("count")
                            taskSummaryModel.add(TaskSummaryModel(ts_status,count))
                        }
                        setupTaskSummaryRecyclerView(taskSummaryModel)
                    }

                }catch (e:Exception){

                }
            }
        }

    }

    //    setup adapter
    private fun setupTaskSummaryRecyclerView(taskSummaryModel: ArrayList<TaskSummaryModel>) {
        val adapter: RecyclerView.Adapter<*> = TaskSummaryAdapter(context, taskSummaryModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        task_summary_recyclerView.layoutManager = llm
        task_summary_recyclerView.adapter = adapter
    }





    fun String.toDate(dateFormat: String = "yyyy-MM-dd HH:mm:ss", timeZone: TimeZone = TimeZone.getDefault()): Date {
        val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
        parser.timeZone = timeZone
        return parser.parse(this)
    }

}
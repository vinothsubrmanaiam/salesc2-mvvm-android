package com.salesc2.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query


@Dao
interface SalesDao {

   @Query("SELECT * FROM DashBoard")
    suspend fun getAll(): List<DashBoard>

    @Insert
    suspend fun insertAll(vararg dashBoard: DashBoard)


    @Query("DELETE FROM DashBoard")
    fun deleteAll()

 @Query("UPDATE DashBoard SET taskStatus=:taskStatus WHERE _id = :t_id")
 fun updateTaskStatus(taskStatus: String?, t_id: String)

 @Query("SELECT * FROM DashBoard WHERE date = :Date")
 fun findByDate(Date: String?): DashBoard?
}

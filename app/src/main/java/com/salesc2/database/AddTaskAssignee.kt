package com.salesc2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data  class AddTaskAssignee(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "_id") val _id: String?,
    @ColumnInfo(name = "name") val name: String?,
    @ColumnInfo(name = "profilePath") val profilePath: String?
)
package com.salesc2.data.network.api.response

data class ResponseAccountList(
    val `data`: List<Data>,
    val msg: String,
    val status: Int
) {
    data class Data(
        val _id: String,
        val name: String
    )
}
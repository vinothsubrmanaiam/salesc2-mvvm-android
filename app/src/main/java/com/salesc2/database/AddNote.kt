package com.salesc2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AddNote (
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "note_task_id") val note_task_id: String?,
    @ColumnInfo(name = "note") val note: String?
        )
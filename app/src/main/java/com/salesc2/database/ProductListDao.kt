package com.salesc2.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ProductListDao {
    @Query("SELECT * FROM ProductList")
    suspend fun getAll(): List<ProductList>

    @Insert
    suspend fun insertAll(vararg productList: ProductList)

    @Query("DELETE FROM ProductList")
    fun deleteAll()
}
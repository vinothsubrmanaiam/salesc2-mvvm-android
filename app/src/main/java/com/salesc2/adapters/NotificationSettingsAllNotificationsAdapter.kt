package com.salesc2.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.model.NotificationSettingsAllNotificationModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.capFirstLetter
import com.salesc2.utils.toast
import java.util.regex.Matcher
import java.util.regex.Pattern

class NotificationSettingsAllNotificationsAdapter () : RecyclerView.Adapter<NotificationSettingsAllNotificationsAdapter.MyViewHolder>() {
    private var notificationSettingsItems = ArrayList<NotificationSettingsAllNotificationModel>()
    var context : Context? = null
    private var settingsSwitch :Boolean = false
    var apiRequest = ApiRequest()
    var webService = WebService()

    constructor(context: Context?, notificationSettingsItems: ArrayList<NotificationSettingsAllNotificationModel>) : this(){
        this.context = context
        this.notificationSettingsItems = notificationSettingsItems

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_notification_settings, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return notificationSettingsItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var notificationSettingsAllNotificationModel: NotificationSettingsAllNotificationModel =
            notificationSettingsItems[position]

        if (notificationSettingsAllNotificationModel.name == "allNotification") {
            holder.name_settings.text = " All Notifications"
        } else {
            holder.name_settings.text =
                context?.capFirstLetter(notificationSettingsAllNotificationModel.name)
        }

        holder.name_settings.setTypeface(holder.name_settings.getTypeface(), Typeface.BOLD)
        holder.name_settings.textSize = 16F
        holder.notify_switch.isChecked = notificationSettingsAllNotificationModel.notification
        holder.email_switch.isChecked = notificationSettingsAllNotificationModel.email


        holder.notify_switch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                settingsSwitch = true
                holder.notify_switch.isChecked = true
                editSwitchApiCall(
                    notificationSettingsAllNotificationModel.email,
                    notificationSettingsAllNotificationModel.name,
                    true
                )

            } else if (!isChecked) {
                holder.notify_switch.isChecked = false
                settingsSwitch = false
                editSwitchApiCall(
                    notificationSettingsAllNotificationModel.email,
                    notificationSettingsAllNotificationModel.name,
                   false
                )

            }
        }

        holder.email_switch.setOnCheckedChangeListener { buttonV, isCheckedEmail ->
            if (isCheckedEmail) {
                holder.email_switch.isChecked = true
                editSwitchApiCallEmail(
                   true,
                    notificationSettingsAllNotificationModel.name,
                    notificationSettingsAllNotificationModel.notification
                )
                settingsSwitch = true
            } else if (!isCheckedEmail) {
                holder.email_switch.isChecked = false
                settingsSwitch = false
                editSwitchApiCallEmail(
                    false,
                    notificationSettingsAllNotificationModel.name,
                    notificationSettingsAllNotificationModel.notification
                )

            }


        }
    }

    private fun editSwitchApiCall(email : Boolean,name:String,notification:Boolean) {
        var params = HashMap<String,Any>()
        params["name"] = name
        params["notification"] = notification

        context?.let {
            apiRequest.putRequestBodyWithHeadersAny(
                it,
                webService.user_notification_settings,
                params
            ) { response ->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200") {
//                        context?.toast(msg)
                        (context as MainActivity).allNotifyRefresh(context as MainActivity)
                    } else {
                        context?.toast(msg)
                    }

                } catch (e: Exception) {
                    context?.toast(e.toString())
                }
            }
        }
    }

        private fun editSwitchApiCallEmail(email : Boolean,name:String,notification:Boolean) {
            var params = HashMap<String, Any>()
            params["email"] = email
            params["name"] = name
            context?.let {
                apiRequest.putRequestBodyWithHeadersAny(
                    it,
                    webService.user_notification_settings,
                    params
                ) { response ->
                    try {
                        var status = response.getString("status")
                        var msg = response.getString("msg")
                        if (status == "200") {
//                            context?.toast(msg)
                            (context as MainActivity).allNotifyRefresh(context as MainActivity)
                        } else {
                            context?.toast(msg)
                        }

                    } catch (e: Exception) {
                        context?.toast(e.toString())
                    }
                }
            }
        }




    class MyViewHolder constructor(itemView : View): RecyclerView.ViewHolder(itemView) {
        var name_settings : TextView = itemView.findViewById(R.id.name_settings)
        var notify_switch : SwitchCompat = itemView.findViewById(R.id.notify_switch)
        var email_switch : SwitchCompat = itemView.findViewById(R.id.email_switch)
    }

}
package com.salesc2.adapters

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.fragment.TaskDetailsFragment
import com.salesc2.model.SelectStatusModel
import com.salesc2.model.TaskListModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import me.thanel.swipeactionview.SwipeActionView
import me.thanel.swipeactionview.SwipeGestureListener
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Exception
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class TaskListAdapter(val refreshTaskList : RefreshTaskList,var context : Context, var taskListItems: ArrayList<TaskListModel>) : RecyclerView.Adapter<TaskListAdapter.MyViewHolder>() {

    var webService = WebService()
    var apiRequest = ApiRequest()
    var stDateBundle = String()
    var edDateBundle = String()



   init{
        this.context = context
        this.taskListItems = taskListItems
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_task_list, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return taskListItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var taskListModel: TaskListModel = taskListItems[position]
        try {
            holder.task_name.text = context?.capFirstLetter(taskListModel.task_name)
            holder.task_dept.text = context?.capFirstLetter(taskListModel.task_department)
            holder.task_status.text = context?.capFirstLetter(taskListModel.task_status)
            holder.task_assign.text = context?.capFirstLetter(taskListModel.task_assigneeTo)
        } catch (e: Exception) {

        }

        var start_date = taskListModel.task_startDate
        var end_date = taskListModel.task_endDate

        var sdf = SimpleDateFormat("yyyy-MM-dd HH:mm")
        //date
        var myFormat = SimpleDateFormat("MMM dd, yyyy")
//        format.timeZone = TimeZone.getTimeZone(tz.displayName)
        val month_parse: Date = sdf.parse(start_date)
        val monthParseLocal = context?.dateFromUTC(context!!, month_parse)
        val month_date: String = myFormat.format(monthParseLocal)


        //time start
        var sdf_time = SimpleDateFormat("hh:mm a")
//        sdf_time.timeZone = TimeZone.getTimeZone(tz.displayName)
        val time_parse: Date = sdf.parse(start_date)
        val timeParseLocal = context?.dateFromUTC(context!!, time_parse)
        val startTime: String = sdf_time.format(timeParseLocal)
        stDateBundle = "$month_date $startTime"

        //time end
        var sdf_time_end = SimpleDateFormat("hh:mm a")
        val time_parse_end: Date = sdf.parse(end_date)
        val timeParseEndLocal = context?.dateFromUTC(context!!, time_parse_end)
        val endTime: String = sdf_time_end.format(timeParseEndLocal)
        edDateBundle = "$month_date $endTime"
        holder.task_start_end_date.text =
            "$month_date at $startTime To $endTime".replace("am", "AM").replace("pm", "PM")

        holder.swipe_view_task_list.swipeGestureListener = object : SwipeGestureListener {
            override fun onSwipedLeft(swipeActionView: SwipeActionView): Boolean {
                holder.update_status_layout.visibility = View.VISIBLE
                return false
            }

            override fun onSwipedRight(swipeActionView: SwipeActionView): Boolean {
                return false
            }

        }


        if (taskListModel.task_status.equals("Unassigned", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_text) }?.let {
                holder.task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_bg) }?.let {
                holder.task_status.setBackgroundColor(it)
            }
            setTextViewDrawableColor(holder.task_status, R.color.un_assigned_text)

        } else if (taskListModel.task_status.equals("Completed", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.completed_text) }?.let {
                holder.task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.completed_bg) }?.let {
                holder.task_status.setBackgroundColor(it)
            }
            setTextViewDrawableColor(holder.task_status, R.color.completed_text)

        } else if (taskListModel.task_status.equals("Uncovered", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.uncovered_text) }?.let {
                holder.task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.uncovered_bg) }?.let {
                holder.task_status.setBackgroundColor(it)
            }
//            val unwrappedDrawable = AppCompatResources.getDrawable(context!!, R.drawable.ic_dot)
//            val wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable!!)
//            DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(context!!, R.color.uncovered_text))
            setTextViewDrawableColor(holder.task_status, R.color.uncovered_text)

        } else if (taskListModel.task_status.equals("Cancelled", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.cancelled_text) }?.let {
                holder.task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.cancelled_bg) }?.let {
                holder.task_status.setBackgroundColor(it)
            }
            setTextViewDrawableColor(holder.task_status, R.color.cancelled_text)

        } else if (taskListModel.task_status.equals("In Progress", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.in_progress_text) }?.let {
                holder.task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.in_progress_bg) }?.let {
                holder.task_status.setBackgroundColor(it)
            }

            setTextViewDrawableColor(holder.task_status, R.color.in_progress_text)
        } else if (taskListModel.task_status.equals("Pending", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.pending_text) }?.let {
                holder.task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_bg) }?.let {
                holder.task_status.setBackgroundColor(it)
            }
            setTextViewDrawableColor(holder.task_status, R.color.pending_text)

        } else if (taskListModel.task_status.equals("Assigned", ignoreCase = true)) {
            context?.let { ContextCompat.getColor(it, R.color.assigned_text) }?.let {
                holder.task_status.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.un_assigned_bg) }?.let {
                holder.task_status.setBackgroundColor(it)
            }
//            val unwrappedDrawable = AppCompatResources.getDrawable(context!!, R.drawable.ic_dot)
//            val wrappedDrawable = DrawableCompat.wrap(unwrappedDrawable!!)
//            DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(context!!, R.color.assigned_text))
            setTextViewDrawableColor(holder.task_status, R.color.assigned_text)
        }

        holder.t_mapTv.setOnClickListener {
            try {
                var uri: String =
                    "http://maps.google.com/maps?daddr=${taskListModel.task_lat},${taskListModel.task_lng}"
                var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                intent.setPackage("com.google.android.apps.maps")
                context?.startActivity(intent)
            } catch (e: Exception) {
                print(e)
                context?.toast(e.toString())
            }
        }



        holder.get_direction_layout.setOnClickListener {
            try {
                var uri: String =
                    "http://maps.google.com/maps?daddr=${taskListModel.task_lat},${taskListModel.task_lng}"
                var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                intent.setPackage("com.google.android.apps.maps")
                context?.startActivity(intent)
            } catch (e: Exception) {
                print(e)
                context?.toast(e.toString())
            }
        }


        holder.t_updatestatus_tv.setOnClickListener {
            holder.update_status_layout.performClick()
        }


        holder.update_status_layout.setOnClickListener {
            if (taskListModel.task_status == "Completed" || taskListModel.task_status == "Cancelled" || taskListModel.task_status == "Uncovered") {
                context?.toast(taskListModel.task_status)
            } else {
                var mBottomSheetDialog: Dialog? =
                    context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) };
                mBottomSheetDialog?.setContentView(R.layout.update_status_popup)
                var addNote = mBottomSheetDialog?.findViewById<EditText>(R.id.add_note_et)
                var cancelStatus = mBottomSheetDialog?.findViewById<TextView>(R.id.cancel_status)
                var updateStatus = mBottomSheetDialog?.findViewById<TextView>(R.id.update_status)
                var close: ImageView? = mBottomSheetDialog?.findViewById(R.id.close_img)
                var updateStatusRecyclerView =
                    mBottomSheetDialog?.findViewById<RecyclerView>(R.id.updateStatus_recyclerView)
                var selectStatusModel = ArrayList<SelectStatusModel>()

                var list = getStatusArray(taskListModel.task_status)
                for (i in 0 until list.size) {
                    selectStatusModel.add(SelectStatusModel(list[i]))
                }
                val adapter: RecyclerView.Adapter<*> =
                    SelectStatusAdapter(context, selectStatusModel)
                val llm = LinearLayoutManager(context)
                llm.orientation = LinearLayoutManager.VERTICAL
                updateStatusRecyclerView?.layoutManager = llm
                updateStatusRecyclerView?.adapter = adapter



                updateStatus?.setOnClickListener {
                    var taskStatusNew =
                        context?.getSharedPref("select_status_name", context).toString()
                    var statusTask = getStatus(taskStatusNew)
                    context?.progressBarShow(context!!)
                    var params = HashMap<String, String>()
                    params["taskStatus"] = statusTask
                    params["_id"] = taskListModel.task_id
                    params["note"] = addNote?.text.toString()
                    params["assignedTo"] = taskListModel.task_assigneeTo_id
                    context?.let { it1 ->
                        apiRequest.postRequestBodyWithHeaders(
                            it1,
                            webService.Update_status_url,
                            params
                        ) { response ->
                            var status = response.getString("status")
                            if (status == "200") {
                                var msg = response.getString("msg")
                                context?.alertDialog(context!!, "Success", msg)
                                context?.progressBarDismiss(context!!)
                                refreshTaskList.taskListApiCall()
                                mBottomSheetDialog?.dismiss()
                            } else {
                                var msg = response.getString("msg")
                                context?.toast(msg)
                                context?.progressBarDismiss(context!!)
                            }

                        }
                    }
                }
                cancelStatus?.setOnClickListener {
                    mBottomSheetDialog?.dismiss()
                }
                close?.setOnClickListener {
                    mBottomSheetDialog?.dismiss()
                }




                mBottomSheetDialog?.setCancelable(true)
                mBottomSheetDialog?.window?.setLayout(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT
                );
                mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
                mBottomSheetDialog?.show()
            }


        }


        holder.task_name.setOnClickListener {
            holder.topLayout.performClick()
        }
        holder.task_dept.setOnClickListener {
            holder.topLayout.performClick()
        }
        holder.task_status.setOnClickListener {
            holder.topLayout.performClick()
        }
        holder.task_assign.setOnClickListener {
            holder.topLayout.performClick()
        }

        //move to details
        holder.task_list_layout.setOnClickListener {
            holder.topLayout.performClick()
        }
        holder.topLayout.setOnClickListener {

            context?.setSharedPref(context!!, "task_start_date", taskListModel.task_startDate)
            context?.setSharedPref(context!!, "task_end_date", taskListModel.task_endDate)
            context?.setSharedPref(context!!, "task_id", taskListModel.task_id)
            context?.setSharedPref(context!!, "td_assignee_name", taskListModel.task_assigneeTo)
            var bundle = Bundle()
            bundle.putString("task_id", taskListModel.task_id)
            bundle.putString("task_start_date", taskListModel.task_startDate)
            bundle.putString("task_end_date", taskListModel.task_endDate)
            bundle.putString("task_assignto_id", taskListModel.task_assigneeTo_id)
            bundle.putString("task_assignee_name", taskListModel.task_assigneeTo)

            var fragment = TaskDetailsFragment()
            fragment.arguments = bundle
            val activity = context as AppCompatActivity
            val transaction: FragmentTransaction =
                activity.supportFragmentManager.beginTransaction()
            transaction.replace(R.id.main_fragmet_container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()
        }


    }


    private fun setTextViewDrawableColor(textView: TextView, color: Int) {
        for (drawable in textView.compoundDrawables) {
            if (drawable != null) {
                drawable.colorFilter = PorterDuffColorFilter(
                    ContextCompat.getColor(textView.context, color),
                    PorterDuff.Mode.SRC_IN
                )
            }
        }
    }


    private fun getStatusArray(Selected: String): Array<String> {
        if (Selected.equals("In Progress", ignoreCase = true)) {
            return arrayOf("Completed", "Cancelled")
        } else if (Selected.equals("Unassigned", ignoreCase = true)) {
            return arrayOf("Cancelled", "Uncovered")
        } else if (Selected.equals("Pending", ignoreCase = true)) {
            return arrayOf("Cancelled", "Uncovered")
        } else if (Selected.equals("Assigned", ignoreCase = true)) {
            return arrayOf("Unassigned", "Cancelled", "Uncovered")
        }
        return arrayOf("")
    }

    private fun getStatus(Selected: String): String {
        if (Selected.equals("Inprogress", ignoreCase = true) || Selected.equals("In Progress", ignoreCase = true)
        ) {
            return "inprogress"
        } else if (Selected == "Uncovered") {
            return "uncovered"
        } else if (Selected == "Cancelled" || Selected == "Canceled") {
            return "cancelled"
        } else if (Selected == "Completed") {
            return "completed"
        } else if (Selected == "Unassigned") {
            return "Unassigned"
        } else if (Selected == "Pending") {
            return "Pending"
        } else if (Selected == "Assigned" || Selected.equals("Scheduled", ignoreCase = true)) {
            return "assigned"
        }
        return ""
    }

    class MyViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var task_name: TextView = itemView.findViewById(R.id.task_name_tv)
        var task_dept: TextView = itemView.findViewById(R.id.task_dept_tv)
        var task_status: TextView = itemView.findViewById(R.id.task_status_tv)
        var task_assign: TextView = itemView.findViewById(R.id.task_assign_tv)
        var task_start_end_date: TextView = itemView.findViewById(R.id.task_start_end_date)
        var swipe_view_task_list: SwipeActionView = itemView.findViewById(R.id.swipe_view_task_list)
        var update_status_layout: ConstraintLayout =
            itemView.findViewById(R.id.update_status_layout)
        var task_list_layout: CardView = itemView.findViewById(R.id.task_list_layout)
        var get_direction_layout: ConstraintLayout =
            itemView.findViewById(R.id.get_direction_layout)
        var t_mapTv: TextView = itemView.findViewById(R.id.t_mapTv)
        var t_updatestatus_tv: TextView = itemView.findViewById(R.id.t_updatestatus_tv)
        var topLayout : ConstraintLayout = itemView.findViewById(R.id.topLayout)
    }


}

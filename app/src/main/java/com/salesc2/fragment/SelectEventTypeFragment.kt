package com.salesc2.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.adapters.SelectEventTypeAdapter
import com.salesc2.model.SelectEventTypeModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.ShowNoData
import com.salesc2.utils.alertDialog
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.loadFragment
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar


class SelectEventTypeFragment:Fragment(),ShowNoData {

   private lateinit var select_et_recyclerView: RecyclerView
    private var selectEventTypeModel = ArrayList<SelectEventTypeModel>()
    private lateinit var select_et_SearchView:SearchView
    private lateinit var select_et_back_arrow : ImageView
    private lateinit var close_iv : ImageView
    private lateinit var selectEventTypeAdapter: SelectEventTypeAdapter
    var webService = WebService()
    var apiRequest = ApiRequest()
    var eType = String()
    private var  editAddEvent = String()
    private lateinit var selectEventTypeHead : TextView
    private lateinit var ET_noDataFound_layout : ConstraintLayout
    private var edit_event = String()
    lateinit var mView: View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_select_event_type,container,false)
        select_et_recyclerView= mView.findViewById(R.id.select_et_recyclerView)
        select_et_recyclerView.layoutManager = LinearLayoutManager(select_et_recyclerView.context)
        select_et_recyclerView.setHasFixedSize(true)
        select_et_recyclerView.isNestedScrollingEnabled = true
        selectEventTypeModel = ArrayList()
        select_et_SearchView = mView.findViewById(R.id.select_et_SearchView)
        selectEventTypeHead = mView.findViewById(R.id.selectEventTypeHead)
        ET_noDataFound_layout = mView.findViewById(R.id.ET_noDataFound_layout)
        eType = context?.getSharedPref("eType_type",context!!).toString()
        edit_event = context?.getSharedPref("edit_event",context!!).toString()
        if (eType == "1"){
            if (edit_event == "yes"){
                selectEventTypeHead.text = "Edit work event"
            }else{
                selectEventTypeHead.text = "Add work event"
            }

        }else{
            if (edit_event == "yes"){
                selectEventTypeHead.text = "Edit personal event"
            }else{
                selectEventTypeHead.text = "Add personal event"
            }

        }
        select_et_back_arrow = mView.findViewById(R.id.select_et_back_arrow)
        close_iv = mView.findViewById(R.id.close_iv)
        editAddEvent =  context?.getSharedPref("edit_add_event",context!!).toString()
        selectEventTypeAdapter  = SelectEventTypeAdapter(context, this,selectEventTypeModel)
        eventTypeList()
        searchEventTypeList()
        goBackAction()
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }
    private fun goBackAction(){
        select_et_back_arrow.setOnClickListener{
            if (editAddEvent == "101"){
                context?.loadFragment(context!!,AddEventFragment())
            }else {
                startActivity(Intent(context,MainActivity::class.java))
            }

        }
        close_iv.setOnClickListener{
           startActivity(Intent(context,MainActivity::class.java))
        }
    }

    private var searchtext = String()
    private fun searchEventTypeList(){
        select_et_SearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchtext = newText
                if(newText?.trim()!!.length<1)
                {
                    eventTypeList()
                }
                else{
                    selectEventTypeAdapter.filter.filter(newText)
                }

                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchtext = query
               selectEventTypeAdapter.filter.filter(query)
                return false
            }

        })
    }


    private fun eventTypeList(){
        var params = HashMap<String,Any>()
        params["type"]= eType.toInt()
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.Event_type_list_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        selectEventTypeModel.clear()
                        selectEventTypeModel = ArrayList()
                        var dataArray = response.getJSONArray("data")
                        if (dataArray.length() <1){
                            ET_noDataFound_layout.visibility = View.VISIBLE
                            select_et_recyclerView.visibility = View.GONE
                        }else{
                            ET_noDataFound_layout.visibility = View.GONE
                            select_et_recyclerView.visibility = View.VISIBLE
                        }
                        for (i in 0 until dataArray.length()) {
                            var dataObj = dataArray.getJSONObject(i)
                            var _id = dataObj.getString("_id")
                            var type = dataObj.getString("type")
                            var name = dataObj.getString("name")
                                selectEventTypeModel.add(SelectEventTypeModel(_id, type, name))

                        }
//                        setupEventTypeRecyclerView(selectEventTypeModel)
                        selectEventTypeAdapter  = SelectEventTypeAdapter(context,this@SelectEventTypeFragment, selectEventTypeModel)
                        select_et_recyclerView.adapter = selectEventTypeAdapter
                    }else{
                        context?.alertDialog(context!!,"Alert",msg)
                    }
                }catch (e:Exception){
                    print(e)
                }
            }
        }
    }


    override fun showNoData() {
        ET_noDataFound_layout.visibility = View.VISIBLE
        select_et_recyclerView.visibility = View.GONE
    }

    override fun hideNoData() {
        ET_noDataFound_layout.visibility = View.GONE
        select_et_recyclerView.visibility = View.VISIBLE
    }
}
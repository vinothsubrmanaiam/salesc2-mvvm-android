package com.salesc2.data.network.api.service



import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestBillingHistory
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseBillingHistory
import com.salesc2.data.network.api.response.ResponseTeamList
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface BillingHistoryApi {

    @POST("subscription/api/v1/billing/history")
    suspend fun reqBillingHistory(
        @Body requestBillingHistory: RequestBillingHistory
    ): Response<ResponseBillingHistory>
}
package com.salesc2.fragment

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.browser.customtabs.CustomTabsIntent
import androidx.fragment.app.Fragment
import com.salesc2.R
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.loadFragment
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.text.SimpleDateFormat

class BillingEstimateFragment : Fragment() {

    private lateinit var be_loc_name : TextView
    private lateinit var be_area_name : TextView
    private lateinit var be_date : TextView
    private lateinit var be_manageSub : TextView
    private lateinit var be_plan_users : TextView
    private lateinit var be_plan_price : TextView
    private lateinit var be_total_due_date : TextView
    private lateinit var be_total_due_price : TextView
    private lateinit var be_current_plan_desc : TextView
    private lateinit var be_contact_us : TextView
    private lateinit var imageLoc_be : CircleImageView
    private lateinit var tax_amount : TextView
    private lateinit var billing_estimate_back_arrow : ImageView


    private var subscriptionId = String()
    var webService = WebService()
    var apiRequest = ApiRequest()

    private lateinit var mView : View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_billing_estimate, container, false)
        subscriptionId = context?.getSharedPref("subscriptionId",context!!).toString()
        be_loc_name = mView.findViewById(R.id.be_loc_name)
        be_area_name = mView.findViewById(R.id.be_area_name)
        be_date = mView.findViewById(R.id.be_date)
        be_manageSub = mView.findViewById(R.id.be_manageSub)
        be_plan_users = mView.findViewById(R.id.be_plan_users)
        be_plan_price = mView.findViewById(R.id.be_plan_price)
        be_total_due_date = mView.findViewById(R.id.be_total_due_date)
        be_total_due_price = mView.findViewById(R.id.be_total_due_price)
        be_current_plan_desc = mView.findViewById(R.id.be_current_plan_desc)
        be_contact_us = mView.findViewById(R.id.be_contact_us)
        imageLoc_be = mView.findViewById(R.id.imageLoc_be)
        tax_amount = mView.findViewById(R.id.tax_amount)
        billing_estimate_back_arrow = mView.findViewById(R.id.billing_estimate_back_arrow)
        billingEstimateApiCall()
        companySubDetailsApiCall()
        onclickActions()
        return mView
    }
    private fun onclickActions(){
        billing_estimate_back_arrow.setOnClickListener{
            fragmentManager?.popBackStack()
        }
        be_contact_us.setOnClickListener{
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(context!!, Uri.parse( "https://salesc2.com/contact"))
        }
        be_manageSub.setOnClickListener{
            context?.loadFragment(context!!,BillingManageSubscriptionFragment())
        }
    }
    private fun billingEstimateApiCall(){
        val params = HashMap<String,Any>()
        params["id"] = subscriptionId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.billing_estimate_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg  = response.getString("msg")
                    if (status == "200"){
                        var dataObj = response.getJSONObject("data")
                        var _id = dataObj.getString("_id")
                        var zipCode = dataObj.getString("zipCode")
                        var plan = dataObj.getString("plan")
                        var planCharge = dataObj.getString("planCharge")
                        var estimatedAmt = dataObj.getString("estimatedAmt")
                        be_total_due_price.text = "USD $estimatedAmt"
                        var usersCount = dataObj.getString("usersCount")
                        be_plan_price.text = "$ ${planCharge.toDouble()*usersCount.toInt()}"
                        if (usersCount.toInt() > 1){
                            be_plan_users.text = "$usersCount Users / Monthly"
                        }else{
                            be_plan_users.text = "$usersCount User / Monthly"
                        }

                        var taxAmount = dataObj.getString("taxAmount")
                        tax_amount.text = "USD $taxAmount "
                        var billingCycle = dataObj.getString("billingCycle")
                        var startDate = dataObj.getString("startDate")
                        var endDate = dataObj.getString("endDate")
                        var sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                        var myFormat = SimpleDateFormat("MMM dd, yyyy")
                        var stDate = sdf.parse(startDate)
                        var stDateF = myFormat.format(stDate)
                        var edDate = sdf.parse(endDate)
                        var edDateF = myFormat.format(edDate)
                        be_date.text = "$stDateF - $edDateF"
                        be_total_due_date.text = "Total due $edDateF"
                        be_current_plan_desc.text = "Based on your current plan, you are being automatically charged $$planCharge"
                    }
                }catch (e:Exception){
                    println(e)
                }
            }
        }

    }


    private fun companySubDetailsApiCall(){
        val params = HashMap<String,Any>()
        params["subscriptionId"] = subscriptionId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.company_subscription_details,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){
                        var dataobj = response.getJSONObject("data")
                        var name = dataobj.getString("name")
                        var logo = dataobj.getString("logo")
                        var type = dataobj.getString("type")
                        var level = dataobj.getString("level")
                        var role = dataobj.getString("role")
                        try {
                            Picasso.get().load(webService.imageBasePath+logo).placeholder(R.drawable.ic_be_location).into(imageLoc_be)
                        }catch (e:Exception){
                            println(e)
                        }
                        if (type == "1" || type == "3"){
                            be_loc_name.text = name
                            if (role.equals("Sales Rep",ignoreCase = true)){
                                be_area_name.text  = "Representative"
                            }else if (role.equals("Team Lead",ignoreCase = true)){
                                be_area_name.text ="Regional Manager"
                            }else{
                                be_area_name.text = role
                            }
                        }else{
                            be_loc_name.text = name
                            be_area_name.text = level
                        }

                    }
                }catch (e:Exception){
                    println(e)
                }

            }
        }
    }
}
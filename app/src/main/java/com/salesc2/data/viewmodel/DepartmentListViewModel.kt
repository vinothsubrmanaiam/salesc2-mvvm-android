package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestDepartmentList
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseDepartmentList
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.AccountListRepository
import com.salesc2.data.repository.DepartmentListRepository
import com.salesc2.data.repository.EmployeeLoginRepository

import kotlinx.coroutines.launch

class DepartmentListViewModel(
    private val repository: DepartmentListRepository,
    val context: Context
) : ViewModel() {
     fun reqGetAccountList(
         requestDepartmentList: RequestDepartmentList,
         onSuccess: OnSuccess<ResponseDepartmentList>,
         onFailure: OnFailure<ResponseDepartmentList>
    )
     {
         viewModelScope.launch {
             repository.reqGetDepartmentList(requestDepartmentList,onSuccess,onFailure)
         }
     }

}
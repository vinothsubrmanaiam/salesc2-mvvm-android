package com.salesc2.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.adapters.FilterAssigneeAdapter
import com.salesc2.adapters.FilterStatusAdapter
import com.salesc2.model.FilterAssigneeModel
import com.salesc2.model.FilterStatusModel
import com.salesc2.utils.progressBarDismiss

//not in use
class FilterStatusFragment : Fragment() {
    private lateinit var statusRecyclerView: RecyclerView
    var filterStatusModel = ArrayList<FilterStatusModel>()
    private lateinit var mView : View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_filter_status, container, false)
        statusRecyclerView = mView.findViewById(R.id.filterStatus_recyclerView)
        filterStatusModel = ArrayList()
        var statusNameList = arrayOf("In progress","Uncovered","Cancelled","Completed","Scheduled","Pending","Unassigned")
        for (i in 0 until statusNameList.size){
            filterStatusModel.add(FilterStatusModel(statusNameList[i]))
        }
        setupFilterStatusRecyclerView(filterStatusModel)

        activity?.findViewById<ImageView>(R.id.app_icon_imageView)?.visibility = View.GONE
        return mView
    }
    //    setup adapter
    private fun setupFilterStatusRecyclerView(filterStatusModel: ArrayList<FilterStatusModel>) {
        val adapter: RecyclerView.Adapter<*> = FilterStatusAdapter(context, filterStatusModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        statusRecyclerView.layoutManager = llm
        statusRecyclerView.adapter = adapter
    }


    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }
}
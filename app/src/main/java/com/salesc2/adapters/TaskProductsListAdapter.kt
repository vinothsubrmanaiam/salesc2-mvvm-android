package com.salesc2.adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import java.lang.Exception

class TaskProductsListAdapter(private val context: Activity, private val id:Array<String>, private val pname: Array<String>, private val price: Array<String>, private val qty: Array<String>)
    :RecyclerView.Adapter<TaskProductsListAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_add_products, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return id.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        try {
            holder.add_prod_name.text = pname[position]
            holder.add_prod_price.text = "$"+price[position]
            holder.add_prod_units.text = qty[position] + " Units"
            holder.add_prod_total.text = "Total: $" + (qty[position].toDouble()* price[position].toDouble()).toString()
        }catch (e: Exception){
            print(e)
        }
    }
    class MyViewHolder constructor(itemView:View) :RecyclerView.ViewHolder(itemView){
        var add_prod_name : TextView = itemView.findViewById(R.id.add_prod_name)
        var add_prod_price : TextView = itemView.findViewById(R.id.add_prod_price)
        var add_prod_units : TextView = itemView.findViewById(R.id.add_prod_units)
        var add_prod_total : TextView = itemView.findViewById(R.id.add_prod_total)
    }
}


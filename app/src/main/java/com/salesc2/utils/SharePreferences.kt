package com.salesc2.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class SharePreferences {
    fun setDefaults(
        key: String,
        value: String,
        context: Context
    ) {
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor: SharedPreferences.Editor = preferences.edit()
        editor.putString(key, value)
        editor.apply()
        editor.commit()

    }

    fun getDefaults(key: String?, context: Context?): String? {
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(key, null)
    }

    fun clearDefaults(context: Context){
        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor : SharedPreferences.Editor = preferences.edit()
        editor.clear()
        editor.apply()
    }

}
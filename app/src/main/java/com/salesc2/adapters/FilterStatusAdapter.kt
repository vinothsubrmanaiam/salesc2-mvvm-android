package com.salesc2.adapters

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.model.FilterStatusModel
import com.salesc2.model.FilterTerritoryModel
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.setSharedPref
import java.util.ArrayList

class FilterStatusAdapter(): RecyclerView.Adapter<FilterStatusAdapter.MyViewHolder>() {
    private lateinit var filterStatusItems: ArrayList<FilterStatusModel>
    var context: Context? = null
   var filter_status_name = String()
    var filter_name_position = String()
    constructor(context: Context?, filterStatusItems: ArrayList<FilterStatusModel>) : this(){
        this.context = context
        this.filterStatusItems = filterStatusItems

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_select_list, parent, false)
        filter_status_name = context?.getSharedPref("filter_status_name",context!!).toString()
        filter_name_position = context?.getSharedPref("status_position",context!!).toString()
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return filterStatusItems.size
    }

//    fun resetGraph(context: Context) {
//
//    }
    var row_index = -1
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var filterStatusModel : FilterStatusModel = filterStatusItems[position]
        holder.filter_name.text = filterStatusModel.filterStatus_name
        holder.filter_name.gravity = Gravity.CENTER
        filter_status_name = context?.getSharedPref("filter_status_name",context!!).toString()
        filter_name_position = context?.getSharedPref("status_position",context!!).toString()
        if (filter_name_position != "null"){
            row_index=filter_name_position.toInt()
        if (row_index.equals(filter_name_position)){
            context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                holder.filter_name.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
                holder.filter_name.setBackgroundDrawable(it)
            }
        }
        }

        holder.itemView.setOnClickListener(View.OnClickListener {

            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("status_position")
            editor.commit()
            row_index=position
            notifyDataSetChanged()
            var statusName = getStatus(filterStatusModel.filterStatus_name)
            context?.setSharedPref(context!!,"filter_status_name",statusName)
        })
        if(row_index==position){
            context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                holder.filter_name.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
                holder.filter_name.setBackgroundDrawable(it)
            }
            context?.setSharedPref(context!!,"status_position",position.toString())

        }
        else
        {
            context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
                holder.filter_name.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
                holder.filter_name.setBackgroundColor(it)
            }

        }
    }
    class MyViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView) {
        var filter_name : TextView = itemView.findViewById(R.id.select_list_name)
        val list_layout : ConstraintLayout = itemView.findViewById(R.id.list_layout)
    }

    private fun getStatus(Selected: String) : String {
        if (Selected.equals("Inprogress",ignoreCase = true) || Selected.equals("In Progress",ignoreCase = true) || Selected.equals("In progress")) {
            return "inprogress"
        }
        else if (Selected == "Uncovered") {
            return "uncovered"
        }
        else if (Selected == "Cancelled" || Selected == "Canceled"){
            return "cancelled"
        }
        else if (Selected == "Completed") {
            return "completed"
        }
        else if (Selected == "Unassigned") {
            return "Unassigned"
        }
        else if (Selected == "Pending") {
            return "Pending"
        }
        else if (Selected == "Assigned" ||Selected.equals("Scheduled",ignoreCase = true)) {
            return "assigned"
        }
        return ""
    }
}

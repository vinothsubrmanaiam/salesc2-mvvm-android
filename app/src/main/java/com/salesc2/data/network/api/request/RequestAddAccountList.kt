package com.salesc2.data.network.api.request

import com.google.gson.annotations.SerializedName

data class RequestAddAccountList(
    @SerializedName("search") val search: String? = ""
)

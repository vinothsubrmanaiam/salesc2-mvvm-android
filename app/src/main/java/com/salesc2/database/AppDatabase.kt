package com.salesc2.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.digi.store.util.database.DashBoardDateAddDao
import com.digi.store.util.database.SelectAccountDao
import com.digi.store.util.database.SelectAccountNewDao

@Database(entities = [DashBoard::class, SelectAccount::class, SelectService::class,
    SelectDepartment::class,AddTaskAssignee::class,AddTask::class,
    DashBoardDateAdd::class,SelectAccountNew::class,ProductList::class,Task::class,
    AddNote::class,AddTaskTimeline::class,TaskStatusTable::class],version = 24, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {


    abstract fun salesDao() : SalesDao
    abstract fun selectAccountDao() : SelectAccountDao
    abstract fun addTaskDao() : AddTaskDao
    abstract fun dashBoardDateAddDao() : DashBoardDateAddDao
    abstract fun productListDao() : ProductListDao
    abstract fun selectDepartmentDao() : SelectDepartmentDao
    abstract fun selectServiceDao() : SelectServiceDao
    abstract fun selectAccountNewDao() : SelectAccountNewDao

    abstract fun addTaskAssigneeDao() : AddTaskAssigneeDao
    abstract fun taskDao() : TaskDao
    abstract fun addNoteDao() : AddNoteDao
    abstract fun addTaskTimelineDao() : AddTaskTimelineDao
    abstract fun taskStatusDao() : TaskStatusDao

    companion object{
        @Volatile
        private var INSTANCE : AppDatabase? = null
        fun getDatabaseClient(context: Context): AppDatabase
        {
            if (INSTANCE != null) return INSTANCE!!
            synchronized(this)
            {
                INSTANCE = Room.databaseBuilder(context,AppDatabase::class.java,"Sales_Details").fallbackToDestructiveMigration().allowMainThreadQueries().build()
                return INSTANCE!!
            }
        }

        fun destroyDataBase(){
            INSTANCE = null
        }
    }
}

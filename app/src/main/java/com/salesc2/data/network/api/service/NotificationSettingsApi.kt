package com.salesc2.data.network.api.service

import com.salesc2.data.network.api.response.ResponseNotificationSettings
import com.salesc2.data.network.api.response.ResponseTeamList
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST

interface NotificationSettingsApi {
    @GET("ios-user/api/v1/employee/notificationSettings")
    suspend fun reqNotificationSettings(): Response<ResponseNotificationSettings>
}
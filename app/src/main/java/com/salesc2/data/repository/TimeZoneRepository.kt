package com.salesc2.data.repository

import com.salesc2.data.network.api.response.ResponseTeamList
import com.salesc2.data.network.api.response.ResponseTimeZone
import com.salesc2.data.network.api.service.TeamListApi
import com.salesc2.data.network.api.service.TimeZoneApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

interface TimeZoneRepository {
    suspend fun reqTimezone (
    onSuccess: OnSuccess<ResponseTimeZone>,
    onFailure: OnFailure<ResponseTimeZone>
)

    companion object Factory{
        fun create(api : TimeZoneApi) : TimeZoneRepository = TimeZoneRepositoryImpl(api)
    }


}
class TimeZoneRepositoryImpl(val api: TimeZoneApi) : TimeZoneRepository {
    override suspend fun reqTimezone (
        onSuccess: OnSuccess<ResponseTimeZone>,
        onFailure: OnFailure<ResponseTimeZone>
    ) {


        withContext(Dispatchers.IO)
        {
            try {

                val response = api.reqTimeZone()
                if(response.isSuccessful)
                {
                    response.body().let {
                        if(it?.status?.equals("200") == true)
                        {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it!!)
                            }
                        }
                        else
                        {
                            onFailure(it!!)
                        }
                    }
                }

            }catch (e: Exception)
            {

            }
        }
    }

}
package com.salesc2.fragment

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnChildAttachStateChangeListener
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.adapters.AccountlistAdapter
import com.salesc2.model.AccountlistModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.Keyboard
import com.salesc2.service.WebService
import com.salesc2.utils.ShowNoData
import com.salesc2.utils.alertDialog
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.progressBarDismiss
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList


class AccountFragment : Fragment(),ShowNoData {

    lateinit var accountListRecyclerView: RecyclerView
    lateinit var accountlistModel: ArrayList<AccountlistModel>
    lateinit var accountlistAdapter: AccountlistAdapter
    lateinit var mView: View
    var requestApi = ApiRequest()
    var searchView: SearchView? = null
    var webServices = WebService()
    var mainActivity = MainActivity()
    var authToken = String()
    var toolbar_title_tv: TextView? = null
    private lateinit var noDataFound_layout : ConstraintLayout

    private var currentPage = 1
    private var lastPage = 1
    private  var pageLoad:Int = 1
    private var isLoading = true
    var previousTotal : Int  = 0
    private var pageSize = String()

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_account, container, false)

        //initializing views
        initViews(mView)

        toolbar_title_tv?.setOnClickListener {
            fragmentManager?.popBackStack()
        }
        noDataFound_layout = mView.findViewById(R.id.noDataFound_layout)
        //calling account list API
        AccountListApiTask()


        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun initViews(mView: View?) {

        val activity = context as MainActivity
        activity.findViewById<ImageView>(R.id.app_icon_imageView)?.visibility = View.GONE
        activity.toolbar?.visibility = View.GONE
        activity.divider1?.visibility = View.GONE
        toolbar_title_tv = mView?.findViewById(R.id.toolbar_title_tv)

        authToken = context?.let { activity.getSharedPref("token", it).toString() }.toString()

        //account list Recycler view
        accountListRecyclerView = mView?.findViewById(R.id.accounts_list_rv)!!
        accountListRecyclerView.layoutManager = LinearLayoutManager(accountListRecyclerView.context)
        accountListRecyclerView.setHasFixedSize(true)
        accountListRecyclerView.isNestedScrollingEnabled = true
        accountlistModel = ArrayList()

        //search view
        searchView = mView.findViewById<View>(R.id.searchView) as SearchView?
        searchView?.visibility = View.VISIBLE
        searchView?.clearFocus()
        searchView?.isIconifiedByDefault = false
        searchView?.setQuery("", false)
        searchView?.isIconified = true
        val searchIcon = searchView?.findViewById<ImageView>(R.id.search_mag_icon)
        searchIcon?.setColorFilter(Color.MAGENTA)

        if (view !is SearchView) {
            view?.setOnTouchListener { v, event ->
                getActivity()?.let { Keyboard.hideSoftKeyboard(it) }
                false
            }
        }
        //search functionality

        /*   searchView!!.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
               override fun onQueryTextSubmit(query: String?): Boolean {

                   return false
               }

               override fun onQueryTextChange(newText: String?): Boolean {
                   accountlistAdapter.filter?.filter(newText)
                  accountlistAdapter.notifyDataSetChanged()
                   return false
               }
           })*/
        /*  searchView!!.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
                  override fun onQueryTextSubmit(query: String?): Boolean {
                      return false
                  }

                  override fun onQueryTextChange(newText: String?): Boolean {
                      filter(newText.toString())
                      return false
                  }

              })*/

        searchView?.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                accountlistAdapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                if(newText?.trim()!!.length<1)
                {
                    AccountListApiTask()
                }
                else{
                    accountlistAdapter.filter.filter(newText)
                }

//                if (accountlistAdapter.resultCount > 0){
//                    noDataFound_layout.visibility = View.GONE
//                }else{
//                    noDataFound_layout.visibility = View.VISIBLE
//                }


//                if (accountlistAdapter.acclist.isEmpty()){
//                    noDataFound_layout.visibility = View.VISIBLE
//                }else{
//                    noDataFound_layout.visibility = View.GONE
//                }
//                if (accountlistModel.size > 0){
//                    noDataFound_layout.visibility = View.GONE
//                }
//                if(accountlistAdapter.getItemCount()<1){
////                    accountListRecyclerView.setVisibility(View.GONE);
//                    noDataFound_layout.setVisibility(View.VISIBLE);
//                }else{
////                    accountListRecyclerView.setVisibility(View.VISIBLE);
//                    noDataFound_layout.setVisibility(View.GONE);
//                }
//                if(accountlistAdapter.itemCount >= 1){
//                    noDataFound_layout.setVisibility(View.GONE);
//                }
//                accountListRecyclerView.addOnChildAttachStateChangeListener(object :
//                    OnChildAttachStateChangeListener {
//                    override fun onChildViewAttachedToWindow(view: View) {
//                        noDataFound_layout.visibility = View.GONE
//                    }
//
//                    override fun onChildViewDetachedFromWindow(view: View) {
//                        noDataFound_layout.visibility = View.VISIBLE
//                    }
//                })
                return false
            }

        })


    }



    fun AccountListApiTask() {

        val params = HashMap<String, String>()
        params.put("Authorization", "Bearer $authToken")
        context?.let {
            requestApi.getRequest(
                it,
                webServices.Account_list_url + "/$pageLoad/10",
                params
            ) { response ->
                var info = response
                var responseStatus = info.getString("status")
                
                if (responseStatus == "200") {
                    if (pageLoad == 1){
                        accountlistModel.clear()
                        accountlistModel = ArrayList()
                    }


                    var pageOfItems = info.getJSONArray("pageOfItems")
                    if (pageOfItems.length() < 1){
                        try {
                            noDataFound_layout.visibility = View.VISIBLE
                            accountListRecyclerView.visibility = View.GONE
                        }catch (e:Exception){

                        }
                    }else{
                        try {
                            noDataFound_layout.visibility = View.GONE
                            accountListRecyclerView.visibility = View.VISIBLE
                        }catch (e:Exception){

                        }
                    }

                    for (i in 0 until pageOfItems.length()) {

                        val jsonObject = pageOfItems.getJSONObject(i)

                        if (jsonObject != null) {

                            var _id = jsonObject.getString("_id")
                            var accountName = jsonObject.getString("accountName")
                            var territory = jsonObject.getString("territory")
                            var lat = jsonObject.getString("lat")

                            var long = jsonObject.getString("long")

                            accountlistModel.add(
                                AccountlistModel(
                                    _id,
                                    accountName,
                                    territory,
                                    lat,
                                    long
                                )
                            )

                        }
                    }
                    var pager = info.getJSONObject("pager")
                    var totalItems = pager.getString("totalItems")
                    currentPage = pager.getString("currentPage").toInt()
                    lastPage = pager.getString("endPage").toInt()

                } else {
                    val msg = response.getString("msg")
                    activity?.alertDialog(requireContext(), "Alert", msg)
                }

                context?.let { activity?.progressBarDismiss(it) }
                //  setupAccountRecyclerView(accountlistModel)
                accountlistAdapter = AccountlistAdapter(context,this, accountlistModel)
                val llm = LinearLayoutManager(context)
                llm.orientation = LinearLayoutManager.VERTICAL
                accountListRecyclerView.layoutManager = llm
                accountListRecyclerView.adapter = accountlistAdapter
                accountListRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                        super.onScrollStateChanged(recyclerView, newState)
                    }

                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                        super.onScrolled(recyclerView, dx, dy)

                        val visibleItemCount = llm.childCount
                        val totalItemCount = llm.itemCount
                        val firstVisibleItem = llm.findFirstVisibleItemPosition()


                        if (isLoading) {
                            if (totalItemCount > previousTotal) {
                                previousTotal = totalItemCount
                                isLoading = false
                                Log.v("...", "Last Item Wow !");

                            }
                        }
                        if (!isLoading && (firstVisibleItem + visibleItemCount) >= totalItemCount && pageLoad < lastPage) {
                            isLoading = true
                            pageLoad++
                            AccountListApiTask()
                        }
                    }


                })




            }
        }

    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(requireContext())
        if (searchView != null) {
            searchView!!.setQuery("", false)
            searchView!!.clearFocus()
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(requireContext())
    }

    override fun showNoData() {
        try {
            noDataFound_layout.visibility = View.VISIBLE
            accountListRecyclerView.visibility = View.GONE
        }catch (e:Exception){

        }

    }

    override fun hideNoData() {
        try {
            noDataFound_layout.visibility = View.GONE
            accountListRecyclerView.visibility = View.VISIBLE
        }catch (e:Exception){

        }
    }


}
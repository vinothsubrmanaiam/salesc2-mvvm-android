package com.salesc2.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.fragment.DashboardFragment
import com.salesc2.model.ChangeTaskAssigneeModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.loadFragment
import com.salesc2.utils.setSharedPref
import com.salesc2.utils.toast
import com.squareup.picasso.Picasso
import java.util.ArrayList

class NotificationReassignAdapter(): RecyclerView.Adapter<NotificationReassignAdapter.MyViewHolder>() {

    private lateinit var filterAssigneeItems: ArrayList<ChangeTaskAssigneeModel>
    var context: Context? = null
    private var notify_re_assignee_position  = String()

     var apiRequest = ApiRequest()
    var webService = WebService()
    private var task_id_notify = String()

    constructor(context: Context?, filterAssigneeItems: ArrayList<ChangeTaskAssigneeModel>) : this(){
        this.context = context
        this.filterAssigneeItems = filterAssigneeItems

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_add_task_assignee, parent, false)
        notify_re_assignee_position = context?.getSharedPref("notify_re_assignee_position",context!!).toString()
        task_id_notify = context?.getSharedPref("task_id_notify",context!!).toString()

        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return filterAssigneeItems.size
    }

    var row_index = -1
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var filterAssigneeModel : ChangeTaskAssigneeModel = filterAssigneeItems[position]
        holder.taskAssigneeName.text = filterAssigneeModel.assignee_name
        notify_re_assignee_position = context?.getSharedPref("notify_re_assignee_position",context!!).toString()
        task_id_notify = context?.getSharedPref("task_id_notify",context!!).toString()
        if (!filterAssigneeModel.assignee_image.equals("")){
            Picasso.get().load(filterAssigneeModel.assignee_image)
                .placeholder(R.drawable.ic_dummy_user_pic).into(holder.taskAssigneeImage)
        }
        else{
            holder.taskAssigneeImage.setBackgroundResource(R.drawable.ic_dummy_user_pic)
        }
        if (notify_re_assignee_position != "null"){
            row_index=notify_re_assignee_position.toInt()
            if (row_index.equals(notify_re_assignee_position)){
                holder.select_icon.visibility= View.VISIBLE
            }
        }

        holder.itemView.setOnClickListener(View.OnClickListener {
            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("notify_re_assignee_position")
            editor.commit()
            row_index=position
            notifyDataSetChanged()
            context?.setSharedPref(context!!,"notify_re_assignee_id",filterAssigneeModel.assignee_id)
            val a = Activity()

            var params = HashMap<String, String>()
            params["_id"] = task_id_notify
            params["taskStatus"] = "Pending"
            params["assignedTo"] = filterAssigneeModel.assignee_id

            context?.let {
                apiRequest.postRequestBodyWithHeaders(
                    it,
                    webService.Update_status_url,
                    params
                ) { response ->
                    try {
                        var status = response.getString("status")
                        var msg = response.getString("msg")
                        if (status == "200") {
                            context?.toast(msg)
                            context?.startActivity(Intent(context,MainActivity::class.java))
                        }else{
                            context?.toast(msg)
                        }
                    }catch (e:java.lang.Exception){

                    }



                }
            }


//            context?.loadFragment(context!!, DashboardFragment())
//                (context as MainActivity).updateDetailsPage(context as MainActivity)
        })
        if(row_index==position){
//            context?.let { ContextCompat.getColor(it, R.color.white) }?.let {
//                holder.taskAssigneeName.setTextColor(it)
//            }
//            context?.let { ContextCompat.getDrawable(it, R.drawable.corner_shape_green_bg) }?.let {
//                holder.taskAssigneeName.setBackgroundDrawable(it)
//            }
            holder.select_icon.visibility= View.VISIBLE
            context?.setSharedPref(context!!,"notify_re_assignee_position",position.toString())

        }
        else
        {
            holder.select_icon.visibility= View.GONE
//            context?.let { ContextCompat.getColor(it, R.color.filter_left_menu_bg) }?.let {
//                holder.taskAssigneeName.setTextColor(it)
//            }
//            context?.let { ContextCompat.getColor(it, R.color.light_grey) }?.let {
//                holder.taskAssigneeName.setBackgroundColor(it)
//            }

        }
    }
    class MyViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView){
        var taskAssigneeName : TextView = itemView.findViewById(R.id.task_assignee_name)
        var taskAssigneeImage : ImageView = itemView.findViewById(R.id.task_assignee_image)
        var assignee_layout : ConstraintLayout = itemView.findViewById(R.id.assignee_layout)
        var select_icon : ImageView = itemView.findViewById(R.id.select_icon)
    }


}
package com.salesc2.fragment

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.data.network.api.request.RequestBillingSubScription
import com.salesc2.data.viewmodel.BillingSubscriptionViewModel
import com.salesc2.data.viewmodel.MorePageViewModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.toast
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat

class BillingManageSubscriptionFragment :Fragment(){

    var webService = WebService()
    var apiRequest = ApiRequest()
    private lateinit var mbs_loc_name : TextView
    private lateinit var mbs_region_name : TextView
    private lateinit var bms_users : TextView
    private lateinit var bms_nextbilldate : TextView
    private lateinit var bms_estimate : TextView
    private lateinit var bms_cancel_sub : TextView
    private lateinit var imageLoc_mbs : CircleImageView
    private lateinit var manage_sub_back_arrow : ImageView
    private val billingSubscriptionViewModel by viewModel<BillingSubscriptionViewModel>()

    private var subscriptionId = String()
    private lateinit var mView:View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_billing_manage_subscription, container, false)
        subscriptionId = context?.getSharedPref("subscriptionId",context!!).toString()
        mbs_loc_name = mView.findViewById(R.id.mbs_loc_name)
        mbs_region_name = mView.findViewById(R.id.mbs_region_name)
        bms_users = mView.findViewById(R.id.bms_users)
        bms_nextbilldate = mView.findViewById(R.id.bms_nextbilldate)
        bms_estimate = mView.findViewById(R.id.bms_estimate)
        bms_cancel_sub = mView.findViewById(R.id.bms_cancel_sub)
        imageLoc_mbs = mView.findViewById(R.id.imageLoc_mbs)
        manage_sub_back_arrow = mView.findViewById(R.id.manage_sub_back_arrow)
        onclickAction()
        if(webService.typeservice.equals("Stage"))
        {
            billingEstimateApiCallwithMVVM()

        }
        else
        {
            billingEstimateApiCall()

        }
        companySubDetailsApiCall()
        return mView
    }
    private fun companySubDetailsApiCall(){
        val params = HashMap<String,Any>()
        params["subscriptionId"] = subscriptionId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.company_subscription_details,params){ response->
                try {

                    var status = response.getString("status")
                    if (status == "200"){
                        var dataobj = response.getJSONObject("data")
                        var name = dataobj.getString("name")
                        var logo = dataobj.getString("logo")
                        var type = dataobj.getString("type")
                        var level = dataobj.getString("level")
                        var role = dataobj.getString("role")
                        try {
                            Picasso.get().load(webService.imageBasePath+logo).placeholder(R.drawable.ic_bill_history).into(imageLoc_mbs)
                        }catch (e:Exception){
                            println(e)
                        }
                        if (type == "1" || type == "3"){
                            mbs_loc_name.text = name
                            if (role.equals("Sales Rep",ignoreCase = true)){
                                mbs_region_name.text  = "Representative"
                            }else if (role.equals("Team Lead",ignoreCase = true)){
                                mbs_region_name.text ="Regional Manager"
                            }else{
                                mbs_region_name.text = role
                            }
                        }else{
                            mbs_loc_name.text = name
                            mbs_region_name.text = level
                        }

                    }
                }catch (e:Exception){
                    println(e)
                }

            }
        }
    }


    private fun onclickAction(){
        bms_cancel_sub.setOnClickListener{
            val dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.yes_or_no_alert_view)
            dialog.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
            val title : TextView = dialog.findViewById(R.id.alert_title)
            title.text = "Alert"
            val msg: TextView = dialog.findViewById(R.id.alert_msg)
            msg.text = "Do you want to cancel subscription?"
            val yes = dialog.findViewById<TextView>(R.id.alert_yes)
            yes.setOnClickListener(View.OnClickListener {
                cancelSubscriptionApiCall()
                dialog.dismiss()

            })
            val no = dialog.findViewById(R.id.alert_no) as TextView
            no.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }
        manage_sub_back_arrow.setOnClickListener{
            fragmentManager?.popBackStack()
        }
    }

    fun billingEstimateApiCallwithMVVM()
    {
        billingSubscriptionViewModel.reqSubscriptionDetails(
            RequestBillingSubScription(
                subscriptionId = subscriptionId
            ),{



                var _id = it.data._id
                var zipCode =it.data.zipCode
                var plan = it.data.plan
                var planCharge = it.data.planCharge
                var estimatedAmt = it.data.estimatedAmt
                bms_estimate.text = "$ $estimatedAmt"
                var usersCount =it.data.usersCount
                bms_users.text = "$usersCount Users"
                var taxAmount = it.data.taxAmount
                var billingCycle = it.data.billingCycle
                var startDate = it.data.startDate
                var endDate = it.data.endDate
                var sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                var myFormat = SimpleDateFormat("MMM dd, yyyy")
                var date = sdf.parse(endDate)
                var dateF = myFormat.format(date)
                bms_nextbilldate.text = dateF


            },{

            }
        )
    }
    private fun billingEstimateApiCall(){
        val params = HashMap<String,Any>()
        params["id"] = subscriptionId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.billing_estimate_url,params){ response->
                println("Billing_subscription"+" "+ webService.company_subscription_details)
                println("Billing_subscription"+" "+ params)

                try {
                    var status = response.getString("status")
                    var msg  = response.getString("msg")
                    println("Billing_subscription"+" "+ response)

                    if (status == "200"){
                        var dataObj = response.getJSONObject("data")
                        var _id = dataObj.getString("_id")
                        var zipCode = dataObj.getString("zipCode")
                        var plan = dataObj.getString("plan")
                        var planCharge = dataObj.getString("planCharge")
                        var estimatedAmt = dataObj.getString("estimatedAmt")
                        bms_estimate.text = "$ $estimatedAmt"
                        var usersCount = dataObj.getString("usersCount")
                        bms_users.text = "$usersCount Users"
                        var taxAmount = dataObj.getString("taxAmount")
                        var billingCycle = dataObj.getString("billingCycle")
                        var startDate = dataObj.getString("startDate")
                        var endDate = dataObj.getString("endDate")
                        var sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                        var myFormat = SimpleDateFormat("MMM dd, yyyy")
                        var date = sdf.parse(endDate)
                        var dateF = myFormat.format(date)
                        bms_nextbilldate.text = dateF

                    }
                }catch (e:Exception){
                    println(e)
                }
            }
        }

    }

    private fun cancelSubscriptionApiCall(){
        val params = HashMap<String,Any>()
        params["subscriptionId"] = subscriptionId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.cancel_subscription_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        context?.toast(msg)
//                        context?.loadFragment(context!!,MoreFragment())
                        context?.startActivity(Intent(context,MainActivity::class.java))
                    }else{
                        context?.toast(msg)
                    }

                }catch (e:Exception){

                }
            }
        }
    }
}
package com.salesc2.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface SelectServiceDao {
    @Query("SELECT * FROM SelectService")
    suspend fun getAll(): List<SelectService>



    @Insert
    suspend fun insertAll(vararg selectService: SelectService)


    @Query("DELETE FROM SelectService")
    fun deleteAll()

}
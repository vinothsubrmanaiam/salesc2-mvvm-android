package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestBillingHistory
import com.salesc2.data.network.api.request.RequestDashBoard
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseBillingHistory
import com.salesc2.data.network.api.response.ResponseDashBoard
import com.salesc2.data.network.api.service.AccountListApi
import com.salesc2.data.network.api.service.BillingHistoryApi
import com.salesc2.data.network.api.service.DashBoardAPI
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface BillingHistoryRepository {

    suspend fun reqBillingHistory(
        requestBillingHistory: RequestBillingHistory,
        onSuccess: OnSuccess<ResponseBillingHistory>,
        onFailure: OnFailure<ResponseBillingHistory>

    )

    companion object Factory{
        fun create(api : BillingHistoryApi) : BillingHistoryRepository = BillingHistoryRepositoryImpl(api)
    }
}

class BillingHistoryRepositoryImpl(var api: BillingHistoryApi) : BillingHistoryRepository {
    override suspend fun reqBillingHistory(
        requestBillingHistory: RequestBillingHistory,
        onSuccess: OnSuccess<ResponseBillingHistory>,
        onFailure: OnFailure<ResponseBillingHistory>
    ) {


        withContext(Dispatchers.IO) {
            try {
                val response = api.reqBillingHistory(requestBillingHistory = requestBillingHistory)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status==200) {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }
    }

}

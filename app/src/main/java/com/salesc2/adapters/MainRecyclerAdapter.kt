package com.salesc2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.database.AppDatabase
import com.salesc2.model.DashSectionModel
import com.salesc2.model.DashTasksModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.getSharedPref
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainRecyclerAdapter() : RecyclerView.Adapter<MainRecyclerAdapter.MyViewHolder>() {

    var dashSectionItems = ArrayList<DashSectionModel>()
    var context : Context? = null
    var dash_tab_click = String()
    var apiRequest = ApiRequest()
    var webService = WebService()

    constructor(context: Context?, dashSectionItems: ArrayList<DashSectionModel>) : this(){
        this.context = context
        this.dashSectionItems = dashSectionItems

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.dashboard_section_row, parent, false)
        dash_tab_click = context?.getSharedPref("dash_tab_click",context!!).toString()
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dashSectionItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var dashSectionModel : DashSectionModel = dashSectionItems[position]

        var sdf = SimpleDateFormat("yyyy-MM-dd")
        val format = SimpleDateFormat("EEEE")
        val day_parse: Date = sdf.parse(dashSectionModel.date)
        val day_name: String = format.format(day_parse)
        var month_format = SimpleDateFormat("MMM dd")
        var month_parse:Date  = sdf.parse(dashSectionModel.date)
        val month_name:String = month_format.format(month_parse)
        holder.sectionName.text = "${day_name.toUpperCase()}, ${month_name.toUpperCase()}"
        var items : ArrayList<DashTasksModel> = dashSectionModel.items
        var dashboardTasksAdapter = DashboardTasksAdapter(context,items)
        holder.section_recyclerView.adapter = dashboardTasksAdapter


    }
    class MyViewHolder constructor(itemView:View): RecyclerView.ViewHolder(itemView){
        var sectionName : TextView = itemView.findViewById(R.id.section_name)
        var section_recyclerView : RecyclerView = itemView.findViewById(R.id.section_recyclerView)
    }
}
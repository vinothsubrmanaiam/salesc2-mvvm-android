package com.salesc2.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.loadFragment
import com.salesc2.utils.setSharedPref
import com.salesc2.utils.toast
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import java.text.SimpleDateFormat
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class TimeZoneSettingFragment :Fragment(){

    private lateinit var your_tz : TextView
    private lateinit var your_tz_date : TextView
    private lateinit var your_tz_dropdown : AutoCompleteTextView
    private lateinit var set_default_tz : TextView

    private lateinit var tz_back_arrow : ImageView
    private lateinit var tz_done : TextView

    var apiRequest = ApiRequest()
    var webService = WebService()
    val cal = Calendar.getInstance()
    var tz: TimeZone = cal.timeZone
    var timeZoneName = String()
    private var timezoneArray : Array<String>? = null
    lateinit var mView: View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_timezone_settings, container, false)
        your_tz = mView.findViewById(R.id.your_tz)
        your_tz_date = mView.findViewById(R.id.your_tz_date)
        your_tz_dropdown = mView.findViewById(R.id.your_tz_dropdown)
        set_default_tz = mView.findViewById(R.id.set_default_tz)
        tz_back_arrow = mView.findViewById(R.id.tz_back_arrow)
        tz_back_arrow.setOnClickListener{
            fragmentManager?.popBackStack()
        }
        tz_done = mView.findViewById(R.id.tz_done)
        tz_done.setOnClickListener{
            timeZoneApiCall()
           context?.loadFragment(context!!,ManageAccountFragment())
        }
        timezoneArray = arrayOf(String())
        try {
            timezoneArray = TimeZone.getAvailableIDs()
        }catch (e:Exception){
            context?.toast(e.toString())
        }


        set_default_tz.text ="Set timezone same as device : ${tz.id}"
        searchProductName()
        defaultTimeZoneSet()
        timeZoneApiCall()

        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ScrollView>(R.id.noTaskScrollView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.no_task_layout)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private fun timeZoneApiCall(){
        var params = HashMap<String,Any>()
        context?.let {
            apiRequest.getRequestBodyWithHeaders(it,webService.Employee_timezone,params){ response->
             try {
                var status = response.getString("status")
                 var msg = response.getString("msg")
                 if (status == "200"){
                     var dataObj = response.getJSONObject("data")
                     var _id = dataObj.getString("_id")
                      timeZoneName = dataObj.getString("timezone")
                     if (timeZoneName == ""){
                         your_tz.text = "Your Time Zone is: NA"
                         your_tz_dropdown.hint = "NA"
                         your_tz_dropdown.text.clear()
                     }else{
                         your_tz.text = "Your Time Zone is: $timeZoneName"
                         your_tz_dropdown.hint = timeZoneName
                         your_tz_dropdown.text.clear()
                     }



                     if (timeZoneName != ""){
                         context?.setSharedPref(context!!,"set_timezone",timeZoneName)
                     }else{
                         context?.setSharedPref(context!!,"set_timezone","null")
                     }

//                     (context as MainActivity).timeZoneUpdateRefresh(context as MainActivity)

                     var DateTime = ZonedDateTime.now(ZoneId.of(timeZoneName))
                     var todayDtFormatNew: DateTimeFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy, hh:mm a")
                     var dtFormated = todayDtFormatNew.format(DateTime)
                     your_tz_date.text = dtFormated.replace("am","AM").replace("pm","PM")

                 }else{
                     context?.toast(msg)
                 }
             }catch (e:Exception){

             }
            }
        }
    }

    private fun defaultTimeZoneSet(){
        set_default_tz.setOnClickListener{
            updateTimeZoneApiCall(tz.id)
        }
    }


    private fun updateTimeZoneApiCall(timezone : String){
        var params = HashMap<String,Any>()
         params["timezone"] = timezone
        context?.let { apiRequest.putRequestBodyWithHeadersAny(it,webService.Employee_timezone,params){ response->

            try {
                var status = response.getString("status")
                var msg = response.getString("msg")
                if (status == "200"){
                    context?.toast(msg)
                    timeZoneApiCall()
                }
                else{
                    context?.toast(msg)
                }
            }catch (e:Exception){

                context?.toast(e.toString())
            }
        }
        }
    }

    fun String.toDate(dateFormat: String = "dd/MM/yyyy hh:mm a", timeZone: TimeZone = TimeZone.getDefault()): Date {
        val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
        parser.timeZone = timeZone
        return parser.parse(this)
    }


private fun searchProductName(){

    var arrayAdapter = timezoneArray?.let {
        ArrayAdapter<String>(context!!,R.layout.support_simple_spinner_dropdown_item,
            it
        )
    }
    your_tz_dropdown.setAdapter(arrayAdapter)
    your_tz_dropdown.threshold = 1
    your_tz_dropdown.setOnClickListener{
//        your_tz_dropdown.setHint(your_tz_dropdown.text.toString())
        your_tz_dropdown.showDropDown()



    }
    your_tz_dropdown.onFocusChangeListener = View.OnFocusChangeListener{
            view, b ->
        if(b){
//            your_tz_dropdown.setHint(your_tz_dropdown.text.toString())

            your_tz_dropdown.showDropDown()
//            your_tz_dropdown.text.clear()

        }

    }
    your_tz_dropdown.onItemClickListener = object : AdapterView.OnItemClickListener{
        override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            val selectedItem = parent?.getItemAtPosition(position).toString()
            your_tz_dropdown.hint = selectedItem
            updateTimeZoneApiCall(selectedItem)
            mView.hideKeyboard()
            your_tz_dropdown.clearFocus()

        }


    }


}

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }
}
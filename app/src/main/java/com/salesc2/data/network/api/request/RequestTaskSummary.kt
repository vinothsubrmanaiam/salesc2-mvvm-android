package com.salesc2.data.network.api.request

import com.google.gson.annotations.SerializedName

data class RequestTaskSummary(
    @SerializedName("endDate") val endDate: String? = "",
    @SerializedName("startDate") val startDate: String? = ""
)

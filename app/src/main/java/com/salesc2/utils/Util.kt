 package com.salesc2.utils

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextWatcher
import android.view.Gravity
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.kaopiz.kprogresshud.KProgressHUD
import com.salesc2.R
import java.sql.Time
import java.text.SimpleDateFormat
import java.util.*


 fun Context.isNetworkAvailable(context: Context): Boolean {
     val conMan =
         context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
     if (conMan.activeNetworkInfo != null && conMan.activeNetworkInfo!!.isConnected) {
         return true
     } else {
//          context.toast("No internet connection")
     }
     return false
 }





fun Context.toast(message: String){
  var toast : Toast =  Toast.makeText(this,message,Toast.LENGTH_SHORT)
    toast.setGravity(Gravity.CENTER, 0, 0)
    toast.show()
}

 fun Context.capFirstLetter(text: String): String? {
     return if (text.length >=2){
         text.substring(0, 1).toUpperCase() + text.substring(1)
     }else{
         text.toUpperCase()
     }

}

fun String.toDate(dateFormat: String = "dd/MM/yyyy hh:mm a", timeZone: TimeZone = TimeZone.getDefault()): Date {
    val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
    parser.timeZone = timeZone
    return parser.parse(this)
}



fun String.toDateF(dateFormat: String , timeZone: TimeZone): Date {
    val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
    parser.timeZone = timeZone
    return parser.parse(this)
}

fun Date.formatToF(dateFormat: String, timeZone: TimeZone): String {
    val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
    formatter.timeZone = timeZone
    return formatter.format(this)
}


fun Context.dateFromUTC(context: Context,date: Date): Date? {
   var set_timezone = context.getSharedPref("set_timezone",context).toString()
   // println("userSelectedTimezone == $set_timezone")
    return if (set_timezone != "null"){
        Date(date.time + TimeZone.getTimeZone(set_timezone).getOffset(date.time))
    }else{
        Date(date.time +  Calendar.getInstance().timeZone.getOffset(date.time))
    }
}

fun Context.dateToUTC(context: Context,date: Date): Date? {
    var set_timezone = context.getSharedPref("set_timezone",context).toString()
   // println("userSelectedTimezone == $set_timezone")
    return if (set_timezone != "null"){
        Date(date.time - TimeZone.getTimeZone(set_timezone).getOffset(date.time))
    }else{
        Date(date.time -  Calendar.getInstance().timeZone.getOffset(date.time))
    }
}

fun Context.alertDialog(context: Context, title: String, msg: String) {
    /* val dialog = Dialog(context)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.setCancelable(false)
    dialog.setContentView(R.layout.alert_dialog_view)
    dialog.window?.setBackgroundDrawableResource(R.drawable.alert_dialog_view_bg)
    val body = dialog.findViewById(R.id.dialogtitle) as TextView
    body.text = title
    val msg1 = dialog.findViewById(R.id.text_dialog) as TextView
    msg1.text = msg
    val dialogButton = dialog.findViewById(R.id.cancel_btn_dialog) as TextView
    dialogButton.setOnClickListener {
        dialog.dismiss()
    }
    dialog.show()*/
}
private  var progressHuD : KProgressHUD? = null


fun Context.progressBarShow(context: Context){
       progressHuD= KProgressHUD.create(context)
        .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
        .setLabel("Please wait")
        //.setDetailsLabel("Downloading data")
        .setCancellable(true)
        .setAnimationSpeed(2)
        .setDimAmount(0.5f)
        .setBackgroundColor(57000000)
        .show()
}

fun Context.progressBarDismiss(it: Context) {
    progressHuD?.dismiss()
}


fun Context.setSharedPref( context: Context, key: String, value: String){
    val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    val editor: SharedPreferences.Editor = preferences.edit()
    editor.putString(key, value)
    editor.apply()
//    editor.commit()
}

fun Context.getSharedPref(key: String, context: Context): String{
    val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    return preferences.getString(key, null).toString()
}

fun Context.clearSharedPref(context: Context){
    val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    val editor : SharedPreferences.Editor = preferences.edit()
    editor.clear()
    editor.apply()
}


fun Context.loadFragment(context: Context,fragment : Fragment){
    //replaceing Fragment
    val activity = context as AppCompatActivity
    val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
    transaction.replace(R.id.main_fragmet_container, fragment)
    transaction.addToBackStack(null)
    transaction.commit()
}

fun Context.loadFilterFragment(context: Context,fragment : Fragment){
    //replaceing Fragment
    val activity = context as AppCompatActivity
    val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
    transaction.replace(R.id.filter_frameLayout, fragment)
    transaction.addToBackStack(null)
    transaction.commit()
}

fun dateFromUTC_Timezone(date: Date): Date? {
    return Date(date.time + Calendar.getInstance().timeZone.getOffset(date.time))
}


 fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
     this.addTextChangedListener(object : TextWatcher {
         override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
         }

         override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
         }

         override fun afterTextChanged(editable: Editable?) {
             afterTextChanged.invoke(editable.toString())
         }
     })
 }
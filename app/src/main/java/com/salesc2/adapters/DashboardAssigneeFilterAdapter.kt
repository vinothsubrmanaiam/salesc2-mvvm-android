package com.salesc2.adapters

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.fragment.DashboardFragment
import com.salesc2.fragment.TaskDetailsFragment
import com.salesc2.model.ChangeTaskAssigneeModel
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.loadFragment
import com.salesc2.utils.setSharedPref
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.squareup.picasso.Picasso
import java.util.ArrayList

class DashboardAssigneeFilterAdapter(): RecyclerView.Adapter<DashboardAssigneeFilterAdapter.MyViewHolder>() {

    private lateinit var filterAssigneeItems: ArrayList<ChangeTaskAssigneeModel>
    var context: Context? = null
    private var dash_filter_assignee_position  = String()

    private var reassign_dash_assignee = String()

    constructor(context: Context?, filterAssigneeItems: ArrayList<ChangeTaskAssigneeModel>) : this(){
        this.context = context
        this.filterAssigneeItems = filterAssigneeItems

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_add_task_assignee, parent, false)
        dash_filter_assignee_position = context?.getSharedPref("dash_filter_assignee_position",context!!).toString()
        reassign_dash_assignee = context?.getSharedPref("reassign_dash_assignee", context!!).toString()
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return filterAssigneeItems.size
    }

    var row_index = -1
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var filterAssigneeModel : ChangeTaskAssigneeModel = filterAssigneeItems[position]
        holder.taskAssigneeName.text = filterAssigneeModel.assignee_name
        dash_filter_assignee_position = context?.getSharedPref("dash_filter_assignee_position",context!!).toString()
        reassign_dash_assignee = context?.getSharedPref("reassign_dash_assignee", context!!).toString()
        if (!filterAssigneeModel.assignee_image.equals("")){
            Picasso.get().load(filterAssigneeModel.assignee_image)
                .placeholder(R.drawable.ic_dummy_user_pic).into(holder.taskAssigneeImage)
        }
        else{
            holder.taskAssigneeImage.setBackgroundResource(R.drawable.ic_dummy_user_pic)
        }
        if (dash_filter_assignee_position != "null"){
            row_index=dash_filter_assignee_position.toInt()
            if (row_index.equals(dash_filter_assignee_position)){
                holder.select_icon.visibility= View.VISIBLE
            }
        }

        holder.itemView.setOnClickListener(View.OnClickListener {

            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("dash_filter_assignee_position")
            editor.commit()
            row_index=position
            notifyDataSetChanged()
            context?.setSharedPref(context!!,"dash_assignee_id",filterAssigneeModel.assignee_id)
            val a = AppCompatActivity()
            context?.loadFragment(context!!, DashboardFragment())
            if (reassign_dash_assignee == "yes"){
                (context as MainActivity).updateDetailsPage(context as MainActivity)
            }else{
                (context as MainActivity).resetAssigneeOnClick(context as MainActivity)
            }


        })
        if(row_index==position){
            holder.select_icon.visibility= View.VISIBLE
            context?.setSharedPref(context!!,"dash_filter_assignee_position",position.toString())

        }
        else
        {
            holder.select_icon.visibility= View.GONE
        }
    }
    class MyViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView){
        var taskAssigneeName : TextView = itemView.findViewById(R.id.task_assignee_name)
        var taskAssigneeImage : ImageView = itemView.findViewById(R.id.task_assignee_image)
        var assignee_layout : ConstraintLayout = itemView.findViewById(R.id.assignee_layout)
        var select_icon : ImageView = itemView.findViewById(R.id.select_icon)
    }
}
package com.salesc2.service

import android.app.Activity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import com.salesc2.R

class ShowPopUp: Activity() {
    internal lateinit var popUp: PopupWindow
    internal var click = true
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        popUp = PopupWindow(this)
        val layout = LinearLayout(this)
        val mainLayout = LinearLayout(this)
        val tv = TextView(this)
        val but = Button(this)
        but.setText("Click Me")
        but.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v:View) {
                if (click)
                {
                    popUp.showAtLocation(layout, Gravity.BOTTOM, 10, 10)
                    popUp.update(50, 50, 300, 80)
                    click = false
                }
                else
                {
                    popUp.dismiss()
                    click = true
                }
            }
        })
        val params = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layout.setOrientation(LinearLayout.VERTICAL)
        tv.setText("Hi this is a sample text for popup window")
        layout.addView(tv, params)
        popUp.setContentView(layout)
        // popUp.showAtLocation(layout, Gravity.BOTTOM, 10, 10);
        mainLayout.addView(but, params)
        setContentView(R.layout.nav_mid_popup)
    }
}
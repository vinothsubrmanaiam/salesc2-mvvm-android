package com.salesc2.data.network.api.request

import com.google.gson.annotations.SerializedName

data class RequestBillingHistory(
    @SerializedName("pageSize") val pageSize: Int,
    @SerializedName("page") val page: Int? ,
    @SerializedName("subscriptionId") val subscriptionId: String? = ""
)

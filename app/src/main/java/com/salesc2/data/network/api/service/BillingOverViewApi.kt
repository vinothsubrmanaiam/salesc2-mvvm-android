package com.salesc2.data.network.api.service



import com.salesc2.data.network.api.request.RequestBillingOverView
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseBillingOverview
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface BillingOverViewApi {

    @POST("company/api/v1/company/subscription/details")
    suspend fun reqGetSubscription(
        @Body requestBillingOverView: RequestBillingOverView
    ): Response<ResponseBillingOverview>
}
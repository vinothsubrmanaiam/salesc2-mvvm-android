package com.salesc2.fragment

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.CalendarContract
import android.view.*
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.adapters.EventDetailsAttendeesAdapter
import com.salesc2.model.AddAttendeesModel
import com.salesc2.model.EventAttendeesModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.DatabaseHandler
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class EventDetailsFragment : Fragment(), OnMapReadyCallback {
    private lateinit var ed_title: TextView
    private lateinit var ed_datetime : TextView
    private lateinit var ed_type :TextView
    private lateinit var ed_typeName :TextView
    private lateinit var ed_desc : TextView
    private lateinit var ed_attendees_recyclerView : RecyclerView
    private lateinit var ed_location_name : TextView
    private lateinit var ed_mapView : MapView
    private lateinit var edit_event : ImageView
    private lateinit var event_details_back_arrow : ImageView

    var eventAttendeesModel = ArrayList<EventAttendeesModel>()


    var webService = WebService()
    var apiRequest = ApiRequest()
    var mGoogleMap: GoogleMap? = null
    var latitude = String()
    var longitude = String()
    var eventAddress = String()

    private var editSt_date = String()
    private var editSt_time = String()
    private var editEd_date = String()
    private var editEd_time = String()
    private var eType_id = String()
    private var eAttendee_name = String()
    private var eAttendee_id = String()
   private var eAttendee_img = String()
    private var eventId =  String()
    private lateinit var ed_getDirection_iv : CircleImageView
    private lateinit var descText_ed : TextView
    private lateinit var locationText_ed : TextView
    private lateinit var map_cv : CardView
    private lateinit var attendeesText_ed : TextView
    private lateinit var eventDetailHead : TextView
    private lateinit var organizer_image : CircleImageView
    private lateinit var organizer_name : TextView
    private lateinit var ccEmails : TextView
    private lateinit var emailsText_ed : TextView
    lateinit var mView:View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_event_details,container,false)
        eventId = arguments?.getString("event_id").toString()
        latitude = arguments?.getString("event_lat").toString()
        longitude = arguments?.getString("event_lng").toString()
        ccEmails = mView.findViewById(R.id.ccEmails)
        emailsText_ed = mView.findViewById(R.id.emailsText_ed)
        eventDetailHead = mView.findViewById(R.id.eventDetailHead)
        ed_title = mView.findViewById(R.id.ed_title)
        ed_datetime = mView.findViewById(R.id.ed_datetime)
        ed_type= mView.findViewById(R.id.ed_type)
        ed_typeName = mView.findViewById(R.id.ed_typeName)
        ed_desc = mView.findViewById(R.id.ed_desc)
        ed_attendees_recyclerView = mView.findViewById(R.id.ed_attendees_recyclerView)
        ed_location_name = mView.findViewById(R.id.ed_location_name)
        event_details_back_arrow = mView.findViewById(R.id.event_details_back_arrow)
        ed_getDirection_iv = mView.findViewById(R.id.ed_getDirection_iv)
        edit_event = mView.findViewById(R.id.edit_event)
        ed_mapView = mView.findViewById(R.id.ed_mapView)
        descText_ed = mView.findViewById(R.id.descText_ed)
        organizer_image= mView.findViewById(R.id.organizer_image)
        organizer_name = mView.findViewById(R.id.organizer_name)
        attendeesText_ed = mView.findViewById(R.id.attendeesText_ed)

        locationText_ed = mView.findViewById(R.id.locationText_ed)
        map_cv = mView.findViewById(R.id.map_cv)

        ed_mapView.onCreate(null)
        ed_mapView.onResume()
        ed_mapView.getMapAsync(this)
        eventAttendeesModel = ArrayList()


        editEventAction()
        goBackAction()
        getDirectionOnclick()

        if (latitude == "" ||latitude == "null" || longitude == "" || longitude == "null" || ed_location_name.text.toString() == "NA"){
            locationText_ed.visibility = View.GONE
            ed_location_name.visibility = View.GONE
            map_cv.visibility = View.GONE
        }



        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }
    private fun goBackAction(){
        event_details_back_arrow.setOnClickListener{
            fragmentManager?.popBackStack()
        }
    }
    private fun getDirectionOnclick(){
        ed_getDirection_iv.setOnClickListener{
            try {
                var uri : String  = "http://maps.google.com/maps?daddr=$latitude,$longitude"
                var intent : Intent =  Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                intent.setPackage("com.google.android.apps.maps");
                context?.startActivity(intent)
            }catch (e:Exception){
                context?.toast(e.toString())
            }

        }
    }

    private fun editEventAction(){
        edit_event.setOnClickListener{
            val popup = PopupMenu(context, edit_event)
            popup.menu.add("  Add Reminder")
            popup.menu.add("  Edit")
            popup.menu.add("  Cancel Event")

            popup.show()
            popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
                override fun onMenuItemClick(item: MenuItem): Boolean {
//                    context?.toast(item.title as String)
                    if (item.title == "  Edit"){
                        context?.setSharedPref(context!!,"event_id",eventId)
                        context?.setSharedPref(context!!,"edit_event","yes")
                        context?.setSharedPref(context!!,"eType_name",ed_typeName.text.toString())
                        context?.setSharedPref(context!!,"eType_id",eType_id)
                        context?.setSharedPref(context!!,"eLocation_name",ed_location_name.text.toString())
                        if (ed_type.text.toString().equals( "meeting",ignoreCase = true)){
                            context?.setSharedPref(context!!,"eType_type","1")
                        }else{
                            context?.setSharedPref(context!!,"eType_type","0")
                        }

                        context?.setSharedPref(context!!,"latitude",latitude)
                        context?.setSharedPref(context!!,"longitude",longitude)
                        context?.setSharedPref(context!!,"event_start_date",editSt_date)
                        context?.setSharedPref(context!!,"event_start_time", editSt_time)
                        context?.setSharedPref(context!!,"event_end_time",editEd_time)
                        context?.setSharedPref(context!!,"event_end_date",editEd_date)
                        context?.setSharedPref(context!!,"eAttendee_name",eAttendee_name)
                        context?.setSharedPref(context!!,"eAttendee_id",eAttendee_id)
                        context?.setSharedPref(context!!,"eAttendee_img",eAttendee_img)
                        context?.setSharedPref(context!!,"event_desc_edit",ed_desc.text.toString())
                        context?.setSharedPref(context!!,"event_title_edit",ed_title.text.toString())
                        context?.loadFragment(context!!,AddEventFragment())
                    }else if (item.title == "  Add Reminder"){
                        var intent: Intent = Intent(Intent.ACTION_INSERT);
                        intent.type = "vnd.android.cursor.item/event";

                        var cal: Calendar  = Calendar.getInstance();
                        var startTime = "$editSt_date $editSt_time"
                        var endTime = "$editEd_date $editEd_time"

                        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startTime)
                        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime)
                        intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, false)

                        intent.putExtra(CalendarContract.Events.TITLE, ed_title.text.toString())
                        intent.putExtra(CalendarContract.Events.DESCRIPTION, ed_desc.text.toString())
                        intent.putExtra(CalendarContract.Events.EVENT_LOCATION, ed_location_name.text.toString())
//                        intent.putExtra(CalendarContract.Events.RRULE, "FREQ=YEARLY");
                        startActivity(intent);
                    }else if (item.title == "  Cancel Event"){
                        val dialog = Dialog(context!!)
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog.setCancelable(false)
                        dialog.setContentView(R.layout.yes_or_no_alert_view)
                        dialog.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
                        val title : TextView = dialog.findViewById(R.id.alert_title)
                        title.text = "Alert"
                        val msg: TextView = dialog.findViewById(R.id.alert_msg)
                        msg.text = "Do you want to cancel event?"
                        val yes = dialog.findViewById<TextView>(R.id.alert_yes)
                        yes.setOnClickListener(View.OnClickListener {
                            cancelEventApiCall()
//                            context?.startActivity(Intent(context, MainActivity::class.java))
                            dialog.dismiss()

                        })
                        val no = dialog.findViewById(R.id.alert_no) as TextView
                        no.setOnClickListener {
                            dialog.dismiss()
                        }
                        dialog.show()



                    }
                    return true
                }
            })
        }
    }
private fun eventDetailsApiCall(googleMap: GoogleMap){
    eventAttendeesModel.clear()
    var params = HashMap<String,Any>()
    context?.let {
        apiRequest.getRequestBodyWithHeaders(it,webService.Event_details_url+"/$eventId",params){ response->
            try {
            var status = response.getString("status")

            var msg = response.getString("msg")
            if (status == "200"){

                var dataObj = response.getJSONObject("data")

                if (dataObj.has("_id")){
                    var _id = dataObj.getString("_id")
                }

                    var title = dataObj.getString("title")
                    ed_title.setText(context?.capFirstLetter(title))
                    var description = dataObj.getString("description")
                if (description == "" || description == "null"){
                    ed_desc.visibility = View.GONE
                    descText_ed.visibility = View.GONE
                }else{
                    ed_desc.visibility = View.VISIBLE
                    descText_ed.visibility = View.VISIBLE
                }
                    ed_desc.setText(context?.capFirstLetter(description))


                    var isEmergency = dataObj.getString("isEmergency")
                    var address = dataObj.getString("address")
                eventAddress = address
                if (address == "null"){
                    ed_location_name.setText("NA")
                    locationText_ed.visibility = View.GONE
                    ed_location_name.visibility = View.GONE
                    map_cv.visibility = View.GONE
                }else{
                    ed_location_name.setText(address)
                }

                    var startDate = dataObj.getString("startDate")
                    var endDate = dataObj.getString("endDate")
                    try {
                        var parser = SimpleDateFormat("yyyy-MM-dd HH:mm")
                        val myFormatDate = "MMM dd, yyyy"
                        val sdf = SimpleDateFormat(myFormatDate)
                        val stDate = parser.parse(startDate)
                        var stDtToLocal = context?.dateFromUTC(context!!,stDate)
                        var stDateF = sdf.format(stDtToLocal)
                        val myFormatTime = "hh:mm a"
                        val sdfTime = SimpleDateFormat(myFormatTime)
                        val startTime =sdfTime.format(stDtToLocal)
                        val endTime = parser.parse(endDate)
                        var edTimeToLocal = context?.dateFromUTC(context!!,endTime)
                        var endTimeF = sdfTime.format(edTimeToLocal)
                        ed_datetime.text = "$stDateF at $startTime to $endTimeF".replace("am","AM").replace("pm","PM")

                        val dateFormat = "MM/dd/yyyy"
                        var sdf_date = SimpleDateFormat(dateFormat)
                        var stDateEdit = parser.parse(startDate)
                        var stDateEditToLocal = context?.dateFromUTC(context!!,stDateEdit)
                        editSt_date = sdf_date.format(stDateEditToLocal)
                        var edDateEdit = parser.parse(endDate)
                        var edDateEditToLocal = context?.dateFromUTC(context!!,edDateEdit)
                        editEd_date = sdf_date.format(edDateEditToLocal)
                        var stTimeEdit = parser.parse(startDate)
                        var stTimeToLocal = context?.dateFromUTC(context!!,stTimeEdit)
                        editSt_time = sdfTime.format(stTimeToLocal)
                        var edTimeEdit = parser.parse(endDate)
                        var edTimToLocal = context?.dateFromUTC(context!!,edTimeEdit)
                        editEd_time = sdfTime.format(edTimToLocal)

                    }catch (e:Exception){
                        print("td_date parse== $e")
                    }

                    var eventTypeName = dataObj.getString("eventTypeName")
                    ed_typeName.text = context?.capFirstLetter(eventTypeName)
                    var eventTypeId = dataObj.getString("eventTypeId")
                eType_id = eventTypeId
                    var type = dataObj.getString("type")
                    ed_type.text = context?.capFirstLetter(type)
                if (type == "meeting"){
                    eventDetailHead.text = "Work event details"
                }else{
                    eventDetailHead.text = "Personal event details"
                }

                    var locationsObj = dataObj.getJSONObject("locations")
                if (locationsObj.has("lat")){
                    var lat = locationsObj.getString("lat")
                    latitude = lat
                }
                 if (locationsObj.has("lng")) {
                     var lng = locationsObj.getString("lng")
                     longitude = lng
                 }





                var attendee_id = String()
                var attendeeName = String()
                var attendeeImage = String()
                var attendee_type = String()
                    var attendeesArray = dataObj.getJSONArray("attendees")

                   if (attendeesArray.length() >= 1) {
                    eventAttendeesModel.clear()
                    eventAttendeesModel = ArrayList()
                    attendeesText_ed.visibility = View.VISIBLE
                    ed_attendees_recyclerView.visibility = View.VISIBLE
                    for (a in 0 until attendeesArray.length()) {
                        var attendeeObj = attendeesArray.getJSONObject(a)
                        if (attendeeObj.has("_id")) {
                            attendee_id = attendeeObj.getString("_id")
                        }

                        if (attendeeObj.has("name")) {
                            attendeeName = attendeeObj.getString("name")
                        }

                        if (attendeeObj.has("profilePath")) {
                            attendeeImage = attendeeObj.getString("profilePath")
                        }
                        if(attendeeObj.has("type")){
                            attendee_type = attendeeObj.getString("type")
                        }

                        var imgWithBase = webService.imageBasePath + attendeeImage
                        eventAttendeesModel.add(
                            EventAttendeesModel(
                                attendeeName,
                                attendee_id,
                                imgWithBase,attendee_type
                            )
                        )
                        eAttendee_id = attendee_id
                        eAttendee_name = attendeeName
                        eAttendee_img = imgWithBase


                        val a_id = attendee_id
                        val a_name = attendeeName
                        val a_img = imgWithBase


                        val databaseHandler: DatabaseHandler = DatabaseHandler(context!!)
                        if (a_name.trim() != "" && a_id.trim() != "") {
                            val status =
                                databaseHandler.addAttendees(AddAttendeesModel(a_id, a_name, a_img))
                            if (status > -1) {

                                //creating the instance of DatabaseHandler class
                                var databaseHandler: DatabaseHandler? = null
                                //calling the viewEmployee method of DatabaseHandler class to read the records
                                var attendee: List<AddAttendeesModel>? = null
                                var multAttendeeId = String()
                                databaseHandler = DatabaseHandler(context!!)
                                attendee = databaseHandler?.viewAttendees()
                                val a_Id = Array<String>(attendee!!.size) { "" }
                                val a_Name = Array<String>(attendee!!.size) { "null" }
                                val a_img = Array<String>(attendee!!.size) { "null" }

                                var index = 0

                                for (a in attendee!!) {
                                    a_Id[index] = a.at_id
                                    a_Name[index] = a.at_name
                                    a_img[index] = a.at_img
                                    index++


                                }
                            } else {
                                Toast.makeText(
                                    context,
                                    "Id or name or price or quantity cannot be blank",
                                    Toast.LENGTH_LONG
                                ).show()
                            }


                        }
                    }
                }else{
                    attendeesText_ed.visibility = View.GONE
                    ed_attendees_recyclerView.visibility = View.GONE
                    }
                setupEdAttendeeRecyclerView(eventAttendeesModel)

                var organizerId = String()
                var organizerName = String()
                var organizerImage = String()
                if (dataObj.has("organizerId")){
                    var organizerIdObj = dataObj.getJSONObject("organizerId")
                    organizerId = organizerIdObj.getString("_id")
                    organizerName = organizerIdObj.getString("name")
                    var profilePath = organizerIdObj.getString("profilePath")
                    organizerImage = webService.imageBasePath+profilePath
                    organizer_name.text = organizerName
                    Picasso.get().load(organizerImage).placeholder(R.drawable.ic_dummy_user_pic).into(organizer_image)
                }

                var ccEmailsArr = dataObj.getJSONArray("ccEmails")
                if (ccEmailsArr.length()<1){
                    ccEmails.visibility = View.GONE
                    emailsText_ed.visibility = View.GONE
                }else{
                    ccEmails.visibility = View.VISIBLE
                    emailsText_ed.visibility = View.VISIBLE
                    ccEmails.text = ccEmailsArr.toString().replace("[","").replace("]","").replace("\"","")
                }




                if( latitude != "null" || longitude != "null") {
                    try {
                        locationText_ed.visibility = View.VISIBLE
                        ed_location_name.visibility = View.VISIBLE
                        map_cv.visibility = View.VISIBLE
                        MapsInitializer.initialize(context)
                        mGoogleMap = googleMap
                        googleMap.addMarker(MarkerOptions().position(LatLng(latitude.toDouble(), longitude.toDouble())))
                        var Liberty: CameraPosition =
                            CameraPosition.builder()
                                .target(LatLng(latitude.toDouble(), longitude.toDouble())).zoom(14F)
                                .bearing(0F)
                                .build()
                        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(Liberty))
                    }catch (e:Exception){
                        print(e)
                    }
                }else{
                    locationText_ed.visibility = View.GONE
                    ed_location_name.visibility = View.GONE
                    map_cv.visibility = View.GONE
                }



            }else if (status == "400"){
                context?.alertDialog(context!!,"Alert",msg)
            }
            else{
                context?.toast(msg)
            }
            }catch (e:Exception){
                print(e)
                context?.toast(e.toString())
            }
        }
    }
}

    //    setup adapter
    private fun setupEdAttendeeRecyclerView(eventAttendeesModel: ArrayList<EventAttendeesModel>) {
        val adapter: RecyclerView.Adapter<*> = EventDetailsAttendeesAdapter(context, eventAttendeesModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.HORIZONTAL
        ed_attendees_recyclerView.layoutManager = llm
        ed_attendees_recyclerView.adapter = adapter
    }

    override fun onMapReady(googleMap: GoogleMap) {
        eventId = arguments?.getString("event_id").toString()
        latitude = arguments?.getString("event_lat").toString()
        longitude = arguments?.getString("event_lng").toString()
        eventDetailsApiCall(googleMap)
    }

    private fun cancelEventApiCall(){
      var  fromClass = context?.getSharedPref("fromClass",context!!).toString()
        val params = HashMap<String,Any>()
        params["id"]= eventId
        context?.let {
            apiRequest.postRequestBodyWithHeadersAny(it,webService.Event_cancel_url,params){ response->
             try {
                 var status = response.getString("status")
                 var msg = response.getString("msg")
                 if (status == "200"){
                     context?.toast(msg)
                     if (fromClass == "Dash"){
                         context?.startActivity(Intent(context,MainActivity::class.java))
                     }else{
                         fragmentManager?.popBackStack()
                     }

                 }else if (status == "400"){
                     context?.toast(msg)
                 }else{
                     context?.toast(msg)
                 }
             }catch (e:Exception){
                 context?.toast(e.toString())
             }
            }
        }
    }

}
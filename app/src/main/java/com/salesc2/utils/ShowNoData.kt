package com.salesc2.utils

interface ShowNoData {
    fun showNoData()
    fun hideNoData()
}
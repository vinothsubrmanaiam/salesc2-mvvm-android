package com.salesc2.data.network.api.service



import com.salesc2.data.network.api.request.RequestTaskSummary
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.api.response.ResponseTaskSummary
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface TaskSummaryApi {

    @POST("ios-task/api/v1/taskSummary")
    suspend fun reqGetTaskDetails(
        @Body requestTaskSummary: RequestTaskSummary
    ): Response<ResponseTaskSummary>
}
package com.salesc2.data.network.api.request

import com.google.gson.annotations.SerializedName

data class RequestMorePage(
    @SerializedName("Authorization") val Authorization: String? = ""
)

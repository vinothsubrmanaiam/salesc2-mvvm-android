package com.salesc2.adapters

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.AccountTimeLinelistModel
import com.salesc2.utils.capFirstLetter
import com.salesc2.utils.dateFromUTC
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


class AccountTimeLinelistAdapter() : RecyclerView.Adapter<AccountTimeLinelistAdapter.viewHolder>() {

     private lateinit var accountTimeLinelistModel: ArrayList<AccountTimeLinelistModel>
     var context: Context? = null

     constructor(
         context: Context?,
         accountTimeLinelistModel: ArrayList<AccountTimeLinelistModel>
     ) : this() {
         this.context = context
         this.accountTimeLinelistModel = accountTimeLinelistModel
     }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AccountTimeLinelistAdapter.viewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.accounts_detail_timeline, parent, false)
        return viewHolder(view)
    }

    override fun getItemCount(): Int {
        return accountTimeLinelistModel.size
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {

        val accountTimeLinelistModel: AccountTimeLinelistModel =
            accountTimeLinelistModel.get(position)
        if (accountTimeLinelistModel.status.contains("Unassigned")) {
            holder.status_tv?.setTextColor(Color.parseColor("#617FFF"))
            holder.color_constraintLayout?.setBackgroundResource(R.drawable.blue_layout)
             holder.status_tv?.let { setTextViewDrawableColor(it, Color.parseColor("#617FFF")) }
         } else if (accountTimeLinelistModel.status.contains("completed")) {
            holder.status_tv?.setTextColor(Color.parseColor("#21BF73"))
            holder.color_constraintLayout?.setBackgroundResource(R.drawable.green_layout)
            holder.status_tv?.let { setTextViewDrawableColor(it, Color.parseColor("#21BF73")) }
        } else if (accountTimeLinelistModel.status.contains("Pending")) {
            holder.status_tv?.setTextColor(Color.parseColor("#617FFF"))
            holder.color_constraintLayout?.setBackgroundResource(R.drawable.blue_layout)
            holder.status_tv?.let { setTextViewDrawableColor(it, Color.parseColor("#617FFF")) }
        } else if (accountTimeLinelistModel.status.contains("assigned")) {
            holder.status_tv?.setTextColor(Color.parseColor("#303960"))
            holder.color_constraintLayout?.setBackgroundResource(R.drawable.dark_blue_layout)
            holder.status_tv?.let { setTextViewDrawableColor(it, Color.parseColor("#303960")) }
        } else if (accountTimeLinelistModel.status.contains("Inprogress")) {
            holder.status_tv?.setTextColor(Color.parseColor("#69F3B1"))
            holder.color_constraintLayout?.setBackgroundResource(R.drawable.lightgreen_layout)
            holder.status_tv?.let { setTextViewDrawableColor(it, Color.parseColor("#69F3B1")) }
        } else if (accountTimeLinelistModel.status.contains("uncovered")) {
            holder.status_tv?.setTextColor(Color.parseColor("#D24856"))
            holder.color_constraintLayout?.setBackgroundResource(R.drawable.red_layout)
            holder.status_tv?.let { setTextViewDrawableColor(it, Color.parseColor("#D24856")) }
        } else if (accountTimeLinelistModel.status.contains("cancelled")) {
            holder.status_tv?.setTextColor(Color.parseColor("#888888"))
            holder.color_constraintLayout?.setBackgroundResource(R.drawable.gray_layout)
            holder.status_tv?.let { setTextViewDrawableColor(it, Color.parseColor("#888888")) }

        }


         holder.status_tv?.text = context?.capFirstLetter(accountTimeLinelistModel.status)
         holder.taskname?.text = context?.capFirstLetter(accountTimeLinelistModel.taskname)
        val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm a")
        val mySdf = SimpleDateFormat("yyyy-MM-dd hh:mm a")
        var dateP = sdf.parse(accountTimeLinelistModel.timestamp)
        var  dateToLocal = context?.dateFromUTC(context!!, dateP)
        var dateF = mySdf.format(dateToLocal)
        holder.time_tv?.text = dateF.replace("am","AM").replace("pm","PM")
         holder.status_tv?.text = context?.capFirstLetter(accountTimeLinelistModel.status)
         holder.assign_name_tv?.text = context?.capFirstLetter(accountTimeLinelistModel.assignee_name)
     }


    private fun setTextViewDrawableColor(textView: TextView, color: Int) {
        for (drawable in textView.compoundDrawables) {
            if (drawable != null) {
                drawable.colorFilter = PorterDuffColorFilter(
                    color,
                    PorterDuff.Mode.SRC_IN
                )
            }
        }
    }

     class viewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {

         var status_tv: TextView?  = itemView.findViewById(R.id.status_tv)
         var taskname: TextView?  = itemView.findViewById(R.id.taskname)
         var date_tv: TextView?  = itemView.findViewById(R.id.date_tv)
         var time_tv: TextView?  = itemView.findViewById(R.id.time_tv)
         var assign_name_tv: TextView?  = itemView.findViewById(R.id.assign_name_tv)
         var color_constraintLayout:ConstraintLayout?=itemView.findViewById(R.id.color_constraintLayout)
     }

 }
package com.salesc2.utils

interface HideShowRecyclerListCardSelection {
    fun showRecyclerSelection()
    fun hideRecyclerSelection()
}
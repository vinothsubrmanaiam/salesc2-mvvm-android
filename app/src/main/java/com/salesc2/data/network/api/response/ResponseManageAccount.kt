package com.salesc2.data.network.api.response

data class ResponseManageAccount(
    val `data`: Data,
    val msg: String,
    val status: String
) {
    data class Data(
        val _id: String,
        val address: String,
        val area: String,
        val comapanyName: String,
        val division: String,
        val email: String,
        val location: Location,
        val name: String,
        val phone: String,
        val profilePath: String,
        val region: String,
        val role: String
    ) {
        data class Location(
            val lat: Double,
            val lng: Double
        )
    }
}
package com.salesc2.fragment

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.adapters.AccountTimeLinelistAdapter
import com.salesc2.model.AccountTimeLinelistModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.alertDialog
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.progressBarDismiss
import com.salesc2.utils.progressBarShow
import java.util.*
import kotlin.collections.ArrayList

class AccountDetailTimelineFragment : Fragment() {

    lateinit var mView: View
    var accountTimelineListRecyclerView: RecyclerView? = null
    val accountTimeLinelistModel = ArrayList<AccountTimeLinelistModel>()
    var accountTimeLinelistAdapter: AccountTimeLinelistAdapter = AccountTimeLinelistAdapter()
    var webServices = WebService()
    var requestApi = ApiRequest()
    private lateinit var noDataFound_layout_acTimeline : ConstraintLayout
    private var currentPage = String()
    private var lastPage = String()
    private  var pageLoad:Int = 1
    private var isLoading = true
    var previousTotal : Int  = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_account_detail_timeline, container, false)

        //initializing views
        initViews(mView)
        noDataFound_layout_acTimeline = mView.findViewById(R.id.noDataFound_layout_acTimeline)

        AccouuntTimeListAsyncTask().execute()

        return mView
    }

    //initializing views
    private fun initViews(mView: View?) {
        accountTimelineListRecyclerView = mView?.findViewById(R.id.accounts_detail_timeline_rv)
        accountTimelineListRecyclerView?.setHasFixedSize(true)
    }



    inner class AccouuntTimeListAsyncTask : AsyncTask<String, String, String>() {
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            context?.progressBarShow(context!!)
        }

        override fun doInBackground(vararg params: String?): String {
            var accountID = String()
            accountID = arguments?.getString("account_id").toString()

            var authToken = String()
            authToken = context?.let { activity?.getSharedPref("token", it).toString() }.toString()
            val params = HashMap<String, Any>()

            params.put("id", accountID)
            params.put("page", pageLoad)

            context?.let {
                requestApi.postRequestBodyWithHeadersAny(
                    it,
                    webServices.Account_timelinedetail_url,
                    params
                ) { response ->
                    var responseStatus = response.getString("status")
                    var msg = response.getString("msg")
                    if (responseStatus == "200") {

                        val data = response.getString("data")

                        var dataArray = response.getJSONArray("data")

                        if (dataArray.length() <=0){
                            noDataFound_layout_acTimeline.visibility = View.VISIBLE
                        }else{
                            noDataFound_layout_acTimeline.visibility = View.GONE
                        }

                        for (i in 0 until dataArray.length()) {

                            val jsonData = dataArray.getJSONObject(i)
                            var _id = jsonData.getString("_id")
                            var timeline_id = jsonData.getString("id")

                            var taskname = jsonData.getString("taskname")
                            var status = jsonData.getString("status")
                            var assignee = jsonData.getJSONObject("assignee")
                            var assignee_id = assignee.getString("id")
                            var assignee_name = assignee.getString("name")
                            var timestamp = String()
                            if (jsonData.has("timestamp")){
                                timestamp = jsonData.getString("timestamp")
                            }


                            accountTimeLinelistModel.add(
                                AccountTimeLinelistModel(
                                    _id,
                                    timeline_id,
                                    timestamp,
                                    taskname,
                                    status,
                                    assignee_id,
                                    assignee_name
                                )
                            )

                        }
                        var pagination = response.getJSONObject("pagination")
                        currentPage = pagination.getString("currentPage")
                        lastPage = pagination.getString("endPage")


                        context?.let { activity?.progressBarDismiss(it) }
                        setupAccountTimeLineRecyclerView(accountTimeLinelistModel)

                    } else {
                        activity?.alertDialog(requireContext(), "Alert", msg)
                    }

                }
            }
            context?.let { activity?.progressBarDismiss(it) }
            return ""
        }

        override fun onPreExecute() {
            super.onPreExecute()
            context?.progressBarDismiss(context!!)
        }

    }

    ///////////////////////////////////////////
    private fun setupAccountTimeLineRecyclerView(accountTimeLinelistModel: ArrayList<AccountTimeLinelistModel>) {
        val adapter: AccountTimeLinelistAdapter =
            AccountTimeLinelistAdapter(
                context,
                accountTimeLinelistModel
            )
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        accountTimelineListRecyclerView?.layoutManager = llm
        accountTimelineListRecyclerView?.adapter = adapter
        context?.let { activity?.progressBarDismiss(it) }

        accountTimelineListRecyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)

                val visibleItemCount = llm.childCount
                val totalItemCount = llm.itemCount
                val firstVisibleItem = llm.findFirstVisibleItemPosition()


                if (isLoading) {
                    if (totalItemCount > previousTotal) {
                        previousTotal = totalItemCount
//                        var i = currentPage.toInt()
                        pageLoad++
                        isLoading = false
                        Log.v("...", "Last Item Wow !");

                    }
                }
                if (!isLoading && (firstVisibleItem + visibleItemCount) >= totalItemCount && pageLoad != lastPage.toInt()+1) {
                    isLoading = true
                    AccouuntTimeListAsyncTask().execute()

                }
            }


        })
    }
    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(requireContext())
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(requireContext())
    }

}






package com.salesc2.adapters

import android.app.Dialog
import android.content.Context
import android.text.method.DigitsKeyListener
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.TaskDetailsProductsModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.RefreshProductAndExpensesTaskDetails
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.progressBarDismiss
import com.salesc2.utils.toast
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class TaskDetailsProductAdapter(var context: Context,
                                var addProductsItems: ArrayList<TaskDetailsProductsModel>,
                                var productIdArr: ArrayList<String> ,
                                var productNameArr : ArrayList<String>,
                                var refreshProductAndExpensesTaskDetails: RefreshProductAndExpensesTaskDetails) : RecyclerView.Adapter<TaskDetailsProductAdapter.MyViewHolder>() {

    private var count = 1
    var webService = WebService()
    var apiRequest = ApiRequest()
    var task_id_edit_prod = String()
    private var prodId = String()
   private var prodIdList = ArrayList<String>()
  private  var prodNameList = ArrayList<String>()
    private  var prodIDLIST = ArrayList<String>()


    init {
        this.context = context
        this.addProductsItems = addProductsItems
        this.productIdArr = productIdArr
        this.productNameArr = productNameArr
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.adapter_task_details_product, parent, false)
        task_id_edit_prod = context?.getSharedPref("task_id_edit_prod", context!!).toString()
        prodNameList = ArrayList()
        prodIdList = ArrayList()
        prodIDLIST= ArrayList()
        addProductApiCallForEdit()
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return addProductsItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var addProductsModel: TaskDetailsProductsModel = addProductsItems[position]
        task_id_edit_prod = context.getSharedPref("task_id_edit_prod", context!!).toString()
        try {
            prodIDLIST.add(addProductsModel.prod_id)
            holder.add_prod_name.text = addProductsModel.prod_name
            holder.add_prod_price.text = "$" + addProductsModel.prod_price
            holder.add_prod_units.text = addProductsModel.prod_qty + " Units"
            holder.add_prod_total.text =
                "Total: $" + (addProductsModel.prod_qty.toInt() * addProductsModel.prod_price.toDouble()).toString()
        } catch (e: Exception) {
            print(e)
        }


        holder.add_prod_price.setOnClickListener {
            val mBottomSheetDialog: Dialog? =
                context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) }
            mBottomSheetDialog?.setContentView(R.layout.task_details_add_products_popup)
            val product_name =
                mBottomSheetDialog?.findViewById<AutoCompleteTextView>(R.id.product_name_et)
            var product_price = mBottomSheetDialog?.findViewById<TextView>(R.id.product_price_et)
            var q_count = mBottomSheetDialog?.findViewById<TextView>(R.id.q_count)
            var close: ImageView? = mBottomSheetDialog?.findViewById(R.id.close_iv)
            var q_minus: ImageView? = mBottomSheetDialog?.findViewById(R.id.q_minus)
            var q_plus: ImageView? = mBottomSheetDialog?.findViewById(R.id.q_plus)
            var add_product_td: Button? = mBottomSheetDialog?.findViewById(R.id.add_product_td)
            var remove_product_td: Button? = mBottomSheetDialog?.findViewById(R.id.remove_product_td)

            remove_product_td!!.visibility = View.VISIBLE

            var addProductHead : TextView? = mBottomSheetDialog?.findViewById(R.id.addProductHead)
            addProductHead?.text = "Edit Product"
            product_price?.setKeyListener(DigitsKeyListener.getInstance(false, true))
            count = addProductsModel.prod_qty.toString().toInt()
            q_plus?.setOnClickListener {
                count++
                q_count?.text = count.toString()
            }
            q_minus?.setOnClickListener {
                if (q_count?.text.toString().toInt() > 1) {
                    count--
                }
                if (count == 0){
                    q_count?.text = "1"
                }else{
                    q_count?.text = count.toString()
                }

            }


            val arrayAdapter = ArrayAdapter<String>(context!!,R.layout.support_simple_spinner_dropdown_item,prodNameList)
    product_name?.setAdapter(arrayAdapter)
    product_name?.threshold = 1
    product_name?.setOnClickListener{
        product_name?.showDropDown()

    }
    product_name?.setDropDownBackgroundDrawable(ContextCompat.getDrawable(context!!,R.drawable.corner_shape_white_bg))
    product_name?.onFocusChangeListener = View.OnFocusChangeListener{
            view, b ->
//                view.setBackgroundColor(ContextCompat.getColor(context!!,R.color.white))
        if(b){
            // Display the suggestion dropdown on focus
            product_name?.showDropDown()
        }
    }
    product_name?.onItemClickListener =
        AdapterView.OnItemClickListener { parent, view, position, id ->
            val selectedItem = parent?.getItemAtPosition(position).toString()
            product_name?.setText(selectedItem)
           product_name?.clearFocus()
            val selectedItemId = parent?.getItemIdAtPosition(position)
            prodId = prodIdList.get(selectedItemId!!.toInt())

        }
            product_name?.setText(addProductsModel.prod_name)
            product_price?.setText(addProductsModel.prod_price)
            q_count?.setText(addProductsModel.prod_qty)


            mBottomSheetDialog?.setCancelable(true)
            mBottomSheetDialog?.window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT)
            mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
            mBottomSheetDialog?.show()

            add_product_td?.setText("Update")
            add_product_td?.setOnClickListener {
                if(!addProductsModel.prod_name.equals(product_name?.text.toString()))
                {

                    if (productIdArr.contains(prodId)||productNameArr.contains(product_name?.text.toString()))
                    {
                        context?.toast("Product already exist")
                    }

                    else
                    {
                        if (product_price?.text.toString().toDouble() < 0.1) {
                            context?.toast("Please enter a valid price")
                        } else {
                            val roundOff = Math.round(product_price?.text.toString().toDouble() * 100.0) / 100.0
                            var jsonObj = JSONObject()
                            jsonObj.put("item", product_name?.text.toString())
                            jsonObj.put("price", roundOff)
                            jsonObj.put("productId", addProductsModel.prod_id)
                            jsonObj.put("quantity", q_count?.text.toString())
                            var arrayOfParams = JSONArray()
                            arrayOfParams.put(jsonObj)
                            val params = HashMap<String, Any>()
                            params.put("id", task_id_edit_prod)
                            params.put("products", arrayOfParams)
                            apiRequest.postRequestBodyWithHeadersAny(
                                context!!,
                                webService.Task_product_add_edit_url,
                                params
                            ) { response ->
                                try {
                                    var status = response.getString("status")
                                    if (status == "202") {
                                        var msg = response.getString("msg")
                                        context?.toast(msg)
                                        mBottomSheetDialog?.dismiss()
                                        refreshProductAndExpensesTaskDetails.refreshProductAndExpenses()
                                        prodId = ""
                                    } else {
                                        var msg = response.getString("msg")
                                        context?.toast(msg)
                                    }
                                } catch (e: Exception) {

                                }


                            }
                        }
                    }
                   // context?.toast("Different product")
                }

               else if (product_name?.text.toString().trim() == "" || product_name?.text.toString().trim().isEmpty() ||product_name?.text.toString().trim().isBlank()) {
                    context?.toast("Please enter product name")
                } else if (product_price?.text.toString().trim() == ""||product_price?.text.toString().trim().isEmpty() || product_price?.text.toString().trim().isBlank()) {
                    context?.toast("Please enter product price")
                } else if (product_price?.text.toString().toDouble() < 0.1) {
                    context?.toast("Please enter a valid price")
                } else {
                    val roundOff = Math.round(product_price?.text.toString().toDouble() * 100.0) / 100.0
                    var jsonObj = JSONObject()
                    jsonObj.put("item", product_name?.text.toString())
                    jsonObj.put("price", roundOff)
                    jsonObj.put("productId", addProductsModel.prod_id)
                    jsonObj.put("quantity", q_count?.text.toString())
                    var arrayOfParams = JSONArray()
                    arrayOfParams.put(jsonObj)
                    val params = HashMap<String, Any>()
                    params.put("id", task_id_edit_prod)
                    params.put("products", arrayOfParams)
                    apiRequest.postRequestBodyWithHeadersAny(
                        context,
                        webService.Task_product_add_edit_url,
                        params
                    ) { response ->
                        try {
                            var status = response.getString("status")
                            if (status == "202") {
                                var msg = response.getString("msg")
                                context?.toast(msg)
                                mBottomSheetDialog?.dismiss()
                                refreshProductAndExpensesTaskDetails.refreshProductAndExpenses()
                            } else {
                                var msg = response.getString("msg")
                                context?.toast(msg)
                            }
                        } catch (e: Exception) {

                        }


                    }
                }

            }



            remove_product_td?.setOnClickListener {
                val dialog = Dialog(context!!)
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                dialog.setCancelable(false)
                dialog.setContentView(R.layout.yes_or_no_alert_view)
                dialog.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
                val title : TextView = dialog.findViewById(R.id.alert_title)
                title.text = "Alert"
                val msg: TextView = dialog.findViewById(R.id.alert_msg)
                msg.text = "Do you want to remove product?"
                val yes = dialog.findViewById<TextView>(R.id.alert_yes)
                yes.setOnClickListener(View.OnClickListener {
                    dialog.dismiss()
                    val params = HashMap<String, Any>()
                    params.put("taskId", task_id_edit_prod)
                    params.put("productId", addProductsModel.prod_id)
                    apiRequest.postRequestBodyWithHeadersAny(
                        context!!,
                        webService.remome_product_api,
                        params
                    ) { response ->
                        try {
                            var status = response.getString("status")
                            if (status == "200") {
                                var msg = response.getString("msg")
                                context?.toast("Product removed successfully")
                                mBottomSheetDialog?.dismiss()
                                refreshProductAndExpensesTaskDetails.refreshProductAndExpenses()
                            }else{
                                var msg = response.getString("msg")
                                context?.toast(msg)
                            }
                        } catch (e: Exception) {

                        }


                    }

                })
                val no = dialog.findViewById(R.id.alert_no) as TextView
                no.setOnClickListener {
                    dialog.dismiss()
                }
                dialog.show()

            }

            mBottomSheetDialog?.setCancelable(true)
            mBottomSheetDialog?.window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            );
            mBottomSheetDialog?.window?.setGravity(Gravity.BOTTOM)
            mBottomSheetDialog?.show()

            close?.setOnClickListener {
                mBottomSheetDialog?.dismiss()
            }

        }

    }

    private fun addProductApiCallForEdit(){
        var param = HashMap<String,String>()
        param.put("id",task_id_edit_prod)
        context?.let {
            apiRequest.postRequestBodyWithHeaders(it,webService.Add_products_url,param){ response ->

                try {
                    var status = response.getString("status")
                    if (status == "200"){
                        prodIdList.clear()
                        prodNameList.clear()
                        prodIdList = ArrayList()
                        prodNameList = ArrayList()
                        var dataArray = response.getJSONArray("data")
                        for (i in 0 until dataArray.length()){
                            var dataObj = dataArray.getJSONObject(i)
                            var id = dataObj.getString("_id")
                            prodIdList.add(id)
                            var name = dataObj.getString("name")
                            prodNameList.add(name)

                            context?.progressBarDismiss(context!!)
                        }
                    }else{
                        context?.progressBarDismiss(context!!)
                    }
                }catch (e:java.lang.Exception)
                {
                    context?.toast(e.toString())
                }
            }

        }
    }


    class MyViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var add_prod_name: TextView = itemView.findViewById(R.id.add_prod_name)
        var add_prod_price: TextView = itemView.findViewById(R.id.add_prod_price)
        var add_prod_units: TextView = itemView.findViewById(R.id.add_prod_units)
        var add_prod_total: TextView = itemView.findViewById(R.id.add_prod_total)
    }

}






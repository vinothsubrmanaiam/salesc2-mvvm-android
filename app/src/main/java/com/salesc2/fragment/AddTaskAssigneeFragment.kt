package com.salesc2.fragment

import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import com.salesc2.R
import com.salesc2.adapters.AddTaskAssigneeAdapter
import com.salesc2.database.AddTaskAssignee
import com.salesc2.database.AppDatabase
import com.salesc2.model.AddTaskAssigneeModel
import com.salesc2.model.SelectAccountModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import kotlinx.coroutines.*
import org.json.JSONObject
import androidx.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread




class AddTaskAssigneeFragment: Fragment(),ShowNoData {


    private var db: AppDatabase? = null
    var webService = WebService()
    var apiRequest = ApiRequest()
    lateinit var assigneeSearch : SearchView
    lateinit var assigneeListRecyclerView: RecyclerView
    lateinit var addTaskAssigneeModel: ArrayList<AddTaskAssigneeModel>
    lateinit var goback : ImageView
    private lateinit var close : ImageView
    lateinit var addTaskAssigneeAdapter: AddTaskAssigneeAdapter

    private var searchtext = String()
    var accId = String()

    private var task_start_date = String()
    private  var task_end_date = String()
    private lateinit var noDataFound_layout : ConstraintLayout

    lateinit var mView: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_add_task_assignee, container, false)
        accId = context?.getSharedPref("acc_filter_id", context!!).toString()
        task_start_date = context?.getSharedPref("task_start_date",context!!).toString()
        task_end_date = context?.getSharedPref("task_end_date",context!!).toString()

        noDataFound_layout = mView.findViewById(R.id.noDataFound_layout)
        assigneeSearch = mView.findViewById(R.id.task_assignee_SearchView)
        assigneeListRecyclerView = mView.findViewById(R.id.task_assignee_recyclerView)
        goback = mView.findViewById(R.id.task_assignee_back_arrow)
        addTaskAssigneeModel = ArrayList()
        close = mView.findViewById(R.id.close_iv)

        if ( requireContext().isNetworkAvailable(requireContext()))
        {
            lifecycleScope.launch {
                AddTaskAssigneeAsyncTask().execute()
            }
        }
        else
        {
            showLocalResponse()
        }




        goBackAction()
        searchAssignee()
        closeAction()

        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE
//        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

   private fun closeAction(){
        close.setOnClickListener{
            context?.loadFragment(context!!,AddTaskFragment())
        }
    }
    private fun goBackAction(){
        goback.setOnClickListener(View.OnClickListener {
            fragmentManager?.popBackStack()
        })
    }
    private fun searchAssignee(){
        assigneeSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchtext = newText
                if (context?.isNetworkAvailable(context!!)==true){
                    AddTaskAssigneeAsyncTask().execute()
                }else{
                    if(newText?.trim().isEmpty())
                    {
                        showLocalResponse()
                    }
                    else{
                        addTaskAssigneeAdapter.filter.filter(newText)
                    }
                }
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchtext = query
                if (context?.isNetworkAvailable(context!!)==true){
                    AddTaskAssigneeAsyncTask().execute()
                } else{
                    addTaskAssigneeAdapter.filter.filter(query)
                }

                return true
            }

            override fun equals(other: Any?): Boolean {
                return super.equals(other)

            }


        })
    }
    inner class AddTaskAssigneeAsyncTask(): AsyncTask<String,String,String>(){
        override fun onPreExecute() {
            super.onPreExecute()
            addTaskAssigneeModel = ArrayList()
//            context?.progressBarShow(context!!)
        }
        override fun doInBackground(vararg params: String?): String {
            var param = HashMap<String,String>()
            param.put("accountId",accId)
            if(task_start_date != "null"){
                param.put("StartDate",task_start_date)
            }else{
                param.put("StartDate","")
            }
            if(task_end_date != "null") {
                param.put("toDate", task_end_date)
            }
            else{
                param.put("toDate", "")
            }
            param.put("search",searchtext)
            context?.let {
                apiRequest.postRequestBodyWithHeaders(it,webService.Add_task_assignee_url,param){ response->


                    var status = response.getString("status")
                    if (status == "200"){
                        addTaskAssigneeModel.clear()
                        var dataArray = response.getJSONArray("data")
                        for (i in 0 until dataArray.length()){
                            var dataObj = dataArray.getJSONObject(i)
                            var _id = dataObj.getString("_id")
                            var name = dataObj.getString("name")
                            var profilePath = String()
                            if (dataObj.has("profilePath")) {
                                profilePath = dataObj.getString("profilePath")
                            }else{
                                profilePath = ""
                            }
                            var imgWithBase = webService.imageBasePath+profilePath
                            addTaskAssigneeModel.add(AddTaskAssigneeModel(_id,imgWithBase,name))
                            context?.progressBarDismiss(context!!)
                        }
                        if (dataArray.length() == 0){
                            noDataFound_layout.visibility =  View.VISIBLE
                        }else{
                            noDataFound_layout.visibility =  View.GONE
                        }
                        setupAddTaskAssigneeRecyclerView(addTaskAssigneeModel)
                    }else{
                        context?.progressBarDismiss(context!!)
                    }

                }
            }

            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            context?.progressBarDismiss(context!!)

        }

    }

    //    setup adapter
    private fun setupAddTaskAssigneeRecyclerView(addTaskAssigneeModel: ArrayList<AddTaskAssigneeModel>) {

        addTaskAssigneeAdapter = AddTaskAssigneeAdapter(context, this,addTaskAssigneeModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        assigneeListRecyclerView.layoutManager = llm
        assigneeListRecyclerView.adapter = addTaskAssigneeAdapter
        addTaskAssigneeAdapter.notifyDataSetChanged()
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

    private fun showLocalResponse(){
            addTaskAssigneeModel.clear()
            CoroutineScope(Dispatchers.Main).launch {
                db = AppDatabase.getDatabaseClient(requireContext())
                val accountData = db!!.addTaskAssigneeDao().getAll()

                for (a in 0 until accountData.size) {
                    var id = accountData[a]._id
                    var name = accountData[a].name
                    var profilePath = accountData[a].profilePath

                    addTaskAssigneeModel.add(
                        AddTaskAssigneeModel(
                            id!!,
                            profilePath!!,
                            name!!,
                            false
                        )
                    )
                }
                setupAddTaskAssigneeRecyclerView(addTaskAssigneeModel)
                Log.i("accountDataMain", accountData.toString())
            }







        }

    override fun showNoData() {
        noDataFound_layout.visibility =  View.VISIBLE
    }

    override fun hideNoData() {
        noDataFound_layout.visibility =  View.GONE
    }
}

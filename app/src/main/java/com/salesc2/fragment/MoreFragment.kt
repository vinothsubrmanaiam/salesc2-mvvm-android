package com.salesc2.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.appcompat.widget.Toolbar
import androidx.browser.customtabs.CustomTabsIntent
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.BuildConfig
import com.salesc2.LoginActivity
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.data.network.api.request.RequestMorePage
import com.salesc2.data.viewmodel.MorePageViewModel
import com.salesc2.data.viewmodel.UserLoginViewModel
import com.salesc2.database.AppDatabase
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class MoreFragment:Fragment() {

    lateinit var mView: View
    var webService = WebService()
    var apiRequest = ApiRequest()
    var authToken = String()
    lateinit var menu_account_tv: TextView
    lateinit var menu_team_tv : TextView
    private lateinit var profile_name : TextView
    private lateinit var profile_email : TextView
    private lateinit var profile_image : CircleImageView
    private lateinit var menu_products: TextView
    private lateinit var menu_ps_tv : TextView
    private lateinit var menu_billing_tv : TextView
    private lateinit var menu_privacy_policy_tv : TextView
    private lateinit var menu_terms_conditions_tv : TextView
    private lateinit var info_help_tv : TextView

    private lateinit var menu_tz_tv : TextView

    private lateinit var info_version_tv :TextView
    private lateinit var manage_account_tv : TextView
    private lateinit var menu_task_summary_tv : TextView
    private lateinit var manage_account_layout : ConstraintLayout

    lateinit var logout : TextView
    lateinit var auto_switch : SwitchCompat
    lateinit var lastSync : TextView
    lateinit var syncNow : Button
    lateinit var removeData : Button

    var db: AppDatabase? = null


    var offline_auto_pref = String()
    var offline_manual_pref = String()


    var profileName = String()
    var profileEmail = String()
    var profileImage = String()
    private val morePageViewModel by viewModel<MorePageViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_more, container, false)
        authToken = context?.let { activity?.getSharedPref("token", it) }.toString()
        profileName = context?.let { activity?.getSharedPref("username", it) }.toString()
        profileEmail =  context?.let { activity?.getSharedPref("email", it) }.toString()
        profileImage = context?.getSharedPref("profile_image", context!!).toString()
        offline_auto_pref = context?.getSharedPref("isChecked_offline_auto", context!!).toString()
        offline_manual_pref = context?.getSharedPref("isChecked_offline_manual", context!!).toString()


        setupUI()
        copyEmailAction()
        employeeDetailsApiCall()
        profile_name.text = profileName
        profile_email.text = profileEmail
        Picasso.get().load(profileImage).placeholder(R.drawable.ic_dummy_user_pic).into(profile_image)

        logoutAction()
        menuOnClickAction()

        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE


        showBillingApiCall()
        syncNowAndRemoveActions()
        autoSyncSwitchAction()
        if(offline_auto_pref=="yes"){
            auto_switch.isChecked = true
        }



        val pre_time = context?.getSharedPref("pre_date", context!!)

        println("gfhfgjthhfhf" + pre_time)
        if (pre_time != "null") {






            val originalFormat: DateFormat =
                SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH)
            val targetFormat: DateFormat = SimpleDateFormat("dd MMMM yyyy hh:mm a")
            val date: Date = originalFormat.parse(pre_time)
            val formattedDate: String = targetFormat.format(date) // 20120821




            lastSync.text = "Last synced on $formattedDate"


        }else{
            lastSync.text = "Last Sync: Never"
        }
 /*       when {
            offline_auto_pref=="yes" -> {
                offline_auto.isChecked = true
            }
            offline_manual_pref=="yes" -> {
                offline_manual.isChecked = true
            }
            else -> {
                offline_auto.isChecked = false
                offline_manual.isChecked = false
            }
        }*/
//        handleOfflineButtons()
        return mView
    }

    private fun showBillingApiCall(){
        var params = HashMap<String,Any>()
        context?.let {
            apiRequest.getRequestBodyWithHeaders(it,webService.show_billing_url,params){ response->
                try {
                   var status = response.getString("status")
                    if (status=="200"){
                            var dataObj = response.getJSONObject("data")
                            var showBilling = dataObj.getString("showBilling")
                            println("showBilling=== $showBilling")
                            if (showBilling == "true"){
                                configureBillingApiCall()
                                menu_billing_tv.visibility = View.VISIBLE
                            }else{
                                menu_billing_tv.visibility = View.GONE
                            }
                    }else{

                    }
                }catch (e:Exception){
                    println(e)
                }
            }
        }
    }

    private fun configureBillingApiCall(){
        var params = HashMap<String,Any>()

        context?.let {
            apiRequest.getRequestBodyWithHeaders(it,webService.configure_billing_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (response.has("msg")){
                        var msg = response.getString("msg")
                    }
                    if (status == "200"){
                        var dataObj = response.getJSONObject("data")
                        var showConfigure = dataObj.getString("showConfigure")
                        context?.setSharedPref(context!!,"showConfigure",showConfigure)
                        var subscribeSettingsId = dataObj.getString("subscribeSettingsId")
                        context?.setSharedPref(context!!,"subscriptionId",subscribeSettingsId)
                        var billingId = dataObj.getString("billingId")
                        context?.setSharedPref(context!!,"billingId",billingId)
                        var pricePerUser = dataObj.getString("pricePerUser")
                        context?.setSharedPref(context!!,"unit_amount",pricePerUser)
                    }
                }catch (e:Exception){
                    println(e)
                }
            }
        }
    }

    private fun setupUI(){
        context?.progressBarDismiss(context!!)
        logout = mView.findViewById(R.id.logout_tv)
        menu_account_tv = mView.findViewById(R.id.menu_account_tv)
        menu_team_tv = mView.findViewById(R.id.menu_team_tv)
        profile_name = mView.findViewById(R.id.profile_name)
        profile_email = mView.findViewById(R.id.profile_email)
        profile_image = mView.findViewById(R.id.profile_image)
        menu_products = mView.findViewById(R.id.menu_products_tv)
        info_version_tv = mView.findViewById(R.id.info_version_tv)
        var versionCode = BuildConfig.VERSION_NAME
        info_version_tv.text = "Version $versionCode"

        menu_ps_tv = mView.findViewById(R.id.menu_ps_tv)
        manage_account_tv = mView.findViewById(R.id.manage_account_tv)
        menu_task_summary_tv = mView.findViewById(R.id.menu_task_summary_tv)
        manage_account_layout = mView.findViewById(R.id.manage_account_layout)
        menu_billing_tv = mView.findViewById(R.id.menu_billing_tv)
        menu_privacy_policy_tv = mView.findViewById(R.id.menu_privacy_policy_tv)
        menu_terms_conditions_tv = mView.findViewById(R.id.menu_terms_conditions_tv)
        info_help_tv = mView.findViewById(R.id.info_help_tv)
        menu_tz_tv = mView.findViewById(R.id.menu_tz_tv)

        auto_switch = mView.findViewById(R.id.auto_switch)
        lastSync = mView.findViewById(R.id.lastSync_tv)
        syncNow = mView.findViewById(R.id.syncNow_btn)
        removeData = mView.findViewById(R.id.removeData_btn)

    }

    private fun menuOnClickAction(){
        menu_team_tv.setOnClickListener{
            context?.loadFragment(context!!,TaskListFragment())
        }


        manage_account_layout.setOnClickListener{
            manage_account_tv.performClick()
        }
        manage_account_tv.setOnClickListener{
            manage_account_tv.isEnabled = false

//            profileDetailsApiCall()
            context?.loadFragment(context!!,ManageAccountFragment())
        }

        menu_account_tv.setOnClickListener(View.OnClickListener {
            var fragment = AccountFragment()
            val activity = context as AppCompatActivity
            activity.supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragmet_container, fragment).addToBackStack(null).commit()
        })
//        menu_team_tv.setOnClickListener(View.OnClickListener {
//            var fragment = TeamMembersListFragment()
//            val activity = context as AppCompatActivity
//            activity.supportFragmentManager.beginTransaction()
//                .replace(R.id.main_fragmet_container, fragment).addToBackStack(null).commit()
//        })


        menu_task_summary_tv.setOnClickListener{
            context?.loadFragment(context!!,TaskSummaryFragment())
        }

        menu_billing_tv.text = "Billing"
        menu_billing_tv.setOnClickListener{
            var showConfigure = context?.getSharedPref("showConfigure",context!!).toString()
            if (showConfigure == "true"){
                context?.loadFragment(context!!,ConfigureBillingFragment())
            }else{
                context?.loadFragment(context!!,BillingOverviewFragment())
            }

        }


      menu_privacy_policy_tv.setOnClickListener {
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(context!!, Uri.parse("https://salesc2.com/privacy-policy"))
        }
        menu_terms_conditions_tv.setOnClickListener{
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(context!!, Uri.parse("https://salesc2.com/terms-of-service"))
        }

        menu_ps_tv.setOnClickListener{
            context?.loadFragment(context!!,PasswordAndSecurityFragment())
        }


        info_help_tv.setOnClickListener{
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()
            customTabsIntent.launchUrl(context!!, Uri.parse( "https://salesc2.com/contact"))
        }

        menu_tz_tv.setOnClickListener{
            context?.loadFragment(context!!,TimeZoneSettingFragment())
        }

    }

    private fun profileDetailsApiCall(){
        var params = HashMap<String,Any>()
        context?.let {
            apiRequest.getRequestBodyWithHeaders(it,webService.Profile_update_url,params){ response->
             try {
                var status = response.getString("status")
                 if (status == "200"){
                     var dataObj = response.getJSONObject("data")

                     var _id = dataObj.getString("_id")
                     var name = dataObj.getString("name")
                     var role = dataObj.getString("role")
                     var email = dataObj.getString("email")
                     var phone = dataObj.getString("phone")
                     var profilePath = dataObj.getString("profilePath")
                     var imgwithBase = webService.imageBasePath+profilePath
                     var address = dataObj.getString("address")
                     var comapanyName = dataObj.getString("comapanyName")
                     var division = dataObj.getString("division")
                     var area = dataObj.getString("area")
                     var region = dataObj.getString("region")
                     var territory = dataObj.getString("territory")

                     var locationObj = dataObj.getJSONObject("location")
                     var lng = locationObj.getString("lng")
                     var lat = locationObj.getString("lat")

                     var bundle = Bundle()
                     bundle.putString("user_address",address)
                     bundle.putString("user_phone",phone)
                     bundle.putString("user_name",name)
                     bundle.putString("user_role",role)
                     bundle.putString("user_image",imgwithBase)
                     bundle.putString("user_division",division)
                     bundle.putString("user_area",area)
                     bundle.putString("user_region",region)
                     bundle.putString("user_email",email)
                     bundle.putString("user_territory",territory)
                     var fragment = ManageAccountFragment()
                     fragment.arguments = bundle
                     val activity = context as AppCompatActivity
                     activity.supportFragmentManager.beginTransaction()
                         .replace(R.id.main_fragmet_container, fragment).addToBackStack(null).commit()

                 }
                 else{
                     context?.toast("Something went wrong, Try again later")
                 }
             }catch (e:Exception){
                 println(e)
             }
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun copyEmailAction(){
        profile_email.setOnTouchListener(object: View.OnTouchListener {

            override fun onTouch(v:View, event: MotionEvent):Boolean {
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action === MotionEvent.ACTION_DOWN)
                {
                    if (event.rawX >= (profile_email.right - profile_email.compoundDrawables[DRAWABLE_RIGHT].bounds.width()))
                    {
                        // your action here
                        val cm =
                            context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                        cm.text = profile_email.text
                        context?.toast("Copied")
                        return true
                    }
                }
                return false
            }
        })
    }

    private fun logoutAction()
    {
        logout.setOnClickListener(View.OnClickListener {

            val dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.yes_or_no_alert_view)
            dialog.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
            val title : TextView = dialog.findViewById(R.id.alert_title)
            title.text = "Logout"
            val msg: TextView = dialog.findViewById(R.id.alert_msg)
             msg.text = "Do you want to logout?"
            val yes = dialog.findViewById<TextView>(R.id.alert_yes)
            yes.setOnClickListener(View.OnClickListener {
                dialog.dismiss()
                if(webService.typeservice.equals("Stage"))
                {
                    logoutWithMVVM()
                }
                else{
                    LogoutRequestAsyncTask().execute()
                }


            })
            val no = dialog.findViewById(R.id.alert_no) as TextView
            no.setOnClickListener {
               dialog.dismiss()
            }
            dialog.show()

        })
    }



    fun logoutWithMVVM()
    {
        morePageViewModel.reqLogOut(RequestMorePage(
            Authorization = "Bearer $authToken"
        ),{
            println("Response"+" "+ it.status)
            requireContext()?.toast(it.msg)
            requireContext()?.clearSharedPref(requireContext()!!)
            startActivity(Intent( requireContext(), LoginActivity::class.java))
            requireActivity()?.let { requireContext()?.progressBarDismiss(it) }
        },{

            requireContext()?.alertDialog( requireContext()!!, "Error", it.msg)
            requireContext()?.let {  requireContext()?.progressBarDismiss(it) }
        })
    }
    inner class LogoutRequestAsyncTask : AsyncTask<String,String,String>(){
        override fun onPreExecute() {
            super.onPreExecute()
            context?.let { activity?.progressBarShow(it) }
        }
        override fun doInBackground(vararg params: String?): String {
            var params = HashMap<String,String>()
            params.put("Authorization","Bearer $authToken")
            context?.let { it1 ->
                apiRequest.postRequestHeaders(it1,webService.employee_logout_url,params){ response->

                    println("More_Fragment"+ " "+webService.employee_logout_url )
                    var status = response.getString("status")
                    println("More_Fragment"+ " "+response )

                    if (status == "204"){
                        val msg = response.getString("msg")

                        activity?.toast(msg)
                        activity?.clearSharedPref(context!!)
                        startActivity(Intent(context, LoginActivity::class.java))
                        context?.let { activity?.progressBarDismiss(it) }
                    }
                    else if (status == "400") {
                        val msg = response.getString("msg")
                        activity?.alertDialog(context!!, "Error", msg)
                        context?.let { activity?.progressBarDismiss(it) }
                    }
                    else if (status == "403") {
                        val msg = response.getString("msg")
                        activity?.alertDialog(context!!, "Error", msg)
                        context?.let { activity?.progressBarDismiss(it) }
                    }

                }
            }

            return ""
        }

    }



    private fun employeeDetailsApiCall()
    {
        val params = HashMap<String,Any>()
        context?.let {
            apiRequest.getRequestBodyWithHeaders(it,webService.billing_employee_details_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){
                       var dataObj = response.getJSONObject("data")
                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")
                        var email = dataObj.getString("email")
                        var phone = dataObj.getString("phone")
                        var companyName = dataObj.getString("companyName")
                        context?.setSharedPref(context!!,"",name)
                        context?.setSharedPref(context!!,"",email)
                        context?.setSharedPref(context!!,"empPhone",phone)
                        context?.setSharedPref(context!!,"empCompany",companyName)
                        context?.setSharedPref(context!!,"",_id)
                    }

                }catch (e:Exception){
                    println(e)
                }
            }
        }
    }
    private fun autoSyncSwitchAction(){
        auto_switch.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                auto_switch.isChecked = true
                context?.setSharedPref(context!!, "isChecked_offline_auto", "yes")
                (context as MainActivity).callOfflineApi(context as MainActivity)
                val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                val currentDateandTime = sdf.format(Date())
                context?.setSharedPref(context!!, "pre_date", currentDateandTime.toString())
                val pre_time = context?.getSharedPref("pre_date", context!!)


                val originalFormat: DateFormat =
                    SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH)
                val targetFormat: DateFormat = SimpleDateFormat("dd MMMM yyyy hh:mm a")
                val date: Date = originalFormat.parse(pre_time)
                val formattedDate: String = targetFormat.format(date) // 20120821




                lastSync.text = "Last synced on $formattedDate"

            } else if (!isChecked) {
                auto_switch.isChecked = false
                context?.setSharedPref(context!!, "isChecked_offline_auto", "no")
            }
        }
    }
    private fun syncNowAndRemoveActions(){
        syncNow.setOnClickListener {
            (context as MainActivity).callOfflineApi(context as MainActivity)
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val currentDateandTime = sdf.format(Date())

            val sdf1 = SimpleDateFormat("dd MMMM yyyy HH:mm a" , Locale.getDefault())
            val currentDateandTime1 = sdf1.format(Date())
            lastSync.text = "Last synced on $currentDateandTime1"
            context?.setSharedPref(context!!, "pre_date", currentDateandTime.toString())
        }
        removeData.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                db = AppDatabase.getDatabaseClient(requireContext())
                db!!.taskDao().deleteAll()
                db!!.productListDao().deleteAll()
                db!!.selectServiceDao().deleteAll()
                db!!.selectAccountDao().deleteAll()
                db!!.selectDepartmentDao().deleteAll()
                val preferences: SharedPreferences =
                    PreferenceManager.getDefaultSharedPreferences(context)
                val editor: SharedPreferences.Editor = preferences.edit()
                editor.remove("pre_date")
                editor.commit()
                lastSync.text = "Last Sync: Data Removed"
            }
        }
    }

/*private fun offlineModeActions(){
    radioGrp.setOnCheckedChangeListener { radioGroup, optionId ->
        run {
            when (optionId) {
                R.id.offline_auto_tv -> {
                    // do something when radio button 1 is selected
                    offline_manual.isChecked = false
                    context?.setSharedPref(context!!, "isChecked_offline_auto", "yes")
                    context?.setSharedPref(context!!, "isChecked_offline_manual", "no")
                    handleOfflineButtons()
                }
                R.id.offline_manual_tv -> {
                    // do something when radio button 2 is selected
                    offline_auto.isChecked = false
                    context?.setSharedPref(context!!, "isChecked_offline_auto", "no")
                    context?.setSharedPref(context!!, "isChecked_offline_manual", "yes")
                    handleOfflineButtons()
                }else ->{
                context?.setSharedPref(context!!, "isChecked_offline_auto", "no")
                context?.setSharedPref(context!!, "isChecked_offline_manual", "no")
                }
            }
        }
    }

}
    private fun handleOfflineButtons(){
        offline_auto_pref = context?.getSharedPref("isChecked_offline_auto", context!!).toString()
        offline_manual_pref = context?.getSharedPref("isChecked_offline_manual", context!!).toString()
        if (offline_auto_pref == "yes"){
            offline_auto.isChecked = true
                    (context as MainActivity).callOfflineApi(context as MainActivity)
        }
        else if (offline_manual_pref == "yes"){
            offline_manual.isChecked = true
            (context as MainActivity).callOfflineApi(context as MainActivity)
        }
    }*/

}



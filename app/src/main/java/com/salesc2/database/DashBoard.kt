package com.salesc2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.json.JSONArray
import org.json.JSONObject





@Entity
data class DashBoard(

    @PrimaryKey(autoGenerate = true) val id: Int,

    @ColumnInfo(name = "date") val date: String?,

    @ColumnInfo(name = "_id") val _id: String?,

    @ColumnInfo(name = "taskStatus") val taskStatus: String?,
    @ColumnInfo(name = "title") val title: String?,
    @ColumnInfo(name = "account_address") val account_address: String?,
    @ColumnInfo(name = "lat") val lat: String?,
    @ColumnInfo(name = "lng") val lng: String?,
    @ColumnInfo(name = "isEmergency") val isEmergency: String?,
    @ColumnInfo(name = "assignTo_id") val assignTo_id: String?,
    @ColumnInfo(name = "assignTo_name") val assignTo_name: String?,
    @ColumnInfo(name = "assignTo_img") val assignTo_img: String?,
    @ColumnInfo(name = "department") val department: String?,
    @ColumnInfo(name = "status") val status: String?,
    @ColumnInfo(name = "startDate") val startDate: String?,
    @ColumnInfo(name = "endDate") val endDate: String?,
    @ColumnInfo(name = "type") val type: String?,
    @ColumnInfo(name = "event_eventType") val event_eventType: String?,
    @ColumnInfo(name = "event_eventCategory") val event_eventCategory: String?,
    @ColumnInfo(name = "event_address") val event_address: String?,
    @ColumnInfo(name = "event_description") val event_description: String?,
    @ColumnInfo(name = "attendee_name") val attendee_name: String?,
    @ColumnInfo(name = "attendee_img") val attendee_img: String?,
    @ColumnInfo(name = "attendee_id") val attendee_id: String?,
    @ColumnInfo(name = "attendee_length") val attendee_length: String?,
    @ColumnInfo(name = "organizerId") val organizerId: String?,
    @ColumnInfo(name = "organizerName") val organizerName: String?,
    @ColumnInfo(name = "organizerImage") val organizerImage: String?


    )

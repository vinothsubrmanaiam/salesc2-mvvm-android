package com.salesc2.data.network.api.service

import com.salesc2.data.network.api.request.RequestPasswordAndSecurity
import com.salesc2.data.network.api.request.RequestUpdateTimeZone
import com.salesc2.data.network.api.response.ResponsePasswordAndSecurity
import com.salesc2.data.network.api.response.ResponseUpdateTimeZone
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.PUT

interface UpdateTimeZoneApi {
    @PUT("ios-task/api/v1/employee/timezone")
    suspend fun reqUpdateTimeZone(
        @Body reqUpdateTimeZone: RequestUpdateTimeZone
    ): Response<ResponseUpdateTimeZone>
}
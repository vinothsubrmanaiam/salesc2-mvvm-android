package com.salesc2.fragment

import android.app.Dialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.adapters.SelectDepartmentAdapter
import com.salesc2.database.AppDatabase
import com.salesc2.database.SelectAccount
import com.salesc2.database.SelectDepartment
import com.salesc2.model.SelectAccountModel
import com.salesc2.model.SelectDeptModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.json.JSONArray
import java.util.HashMap

class SelectDepartmentFragment: Fragment(),ShowNoData {

    private var db: AppDatabase? = null
    lateinit var deptSearch : SearchView
    lateinit var  deptListRecyclerView: RecyclerView
    lateinit var selectDeptAdapter: SelectDepartmentAdapter
    lateinit var selectDeptModel: ArrayList<SelectDeptModel>
    var webService = WebService()
    var apiRequest = ApiRequest()
    var searchtext = String()
    lateinit var mView: View
    lateinit var goback : ImageView
    private lateinit var close : ImageView
    private lateinit var noDataFound_layout : ConstraintLayout
    var accountID = String()
    private var accIdPref = String()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_select_department, container, false)
        accountID = arguments?.getString("account_id").toString()
        accIdPref = context?.getSharedPref("acc_filter_id",context!!).toString()
        deptSearch  = mView.findViewById(R.id.selectDept_SearchView)
        deptListRecyclerView = mView.findViewById(R.id.selectDept_recyclerView)
        selectDeptModel = ArrayList()
        goback = mView.findViewById(R.id.select_dept_back_arrow)
        close = mView.findViewById(R.id.close_iv)
        noDataFound_layout = mView.findViewById(R.id.noDataFound_layout)

        if ( requireContext().isNetworkAvailable(requireContext()))
        {
            lifecycleScope.launch {
//                delay(100)
                SelectDeptAsyncTask().execute()
            } }
        else
        {
            deptOfflineData()
        }


        goBackAction()
        closeAction()
        searchDept()
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }
    private fun deptOfflineData(){
        selectDeptModel = ArrayList()
        CoroutineScope(Dispatchers.Main).launch {
            db = AppDatabase.getDatabaseClient(requireContext())
            db = AppDatabase.getDatabaseClient(requireContext())
            val getName = db!!.selectAccountNewDao().getDeparment(accountID)
            //string to json aray
            var response = JSONArray(getName[0].department)

            if (response.length() <=0){
                noDataFound_layout.visibility = View.VISIBLE
            }else{
                noDataFound_layout.visibility = View.GONE
            }
            println("getDepartment"+" "+response)
            for (i in 0 until response.length()) {
                var dataObj = response.getJSONObject(i)
                var id = dataObj.getString("_id")
                var name = dataObj.getString("name")
                selectDeptModel.add(SelectDeptModel(id,name))
            }
            setupSelectDeptRecyclerView(selectDeptModel)

        }
    }

    private fun closeAction(){
        close.setOnClickListener{
            val dialog = Dialog(context!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.yes_or_no_alert_view)
            dialog.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
            val title : TextView = dialog.findViewById(R.id.alert_title)
            title.text = "Alert"
            val msg: TextView = dialog.findViewById(R.id.alert_msg)
            msg.text = "Do you want to cancel adding task?"
            val yes = dialog.findViewById<TextView>(R.id.alert_yes)
            yes.setOnClickListener(View.OnClickListener {
                context?.startActivity(Intent(context,MainActivity::class.java))
                dialog.dismiss()
            })
            val no = dialog.findViewById(R.id.alert_no) as TextView
            no.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }
    }
    private fun goBackAction(){
        goback.setOnClickListener(View.OnClickListener {
            fragmentManager?.popBackStack()
//            context?.loadFragment(context!!,SelectAccountFragment())
        })
    }

    private fun searchDept(){

        deptSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchtext = newText
                selectDeptModel = ArrayList()
                if(context?.isNetworkAvailable(context!!) == true){

                SelectDeptAsyncTask().execute()
                }else{
                    if(newText.length!=0)
                    {
                        if(newText?.length<2)
                        {
                            deptOfflineData()
                        }
                        else{
                            selectDeptAdapter.filter.filter(newText)
                        }
                    }
                }

                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchtext = query
                selectDeptModel = ArrayList()
                if(context?.isNetworkAvailable(context!!) == true){
                SelectDeptAsyncTask().execute()
                }else{
                    selectDeptAdapter.filter.filter(query)
                }
//

                return false
            }
            override fun equals(other: Any?): Boolean {
                return super.equals(other)
            }
        })
    }


    inner class SelectDeptAsyncTask(): AsyncTask<String,String,String>(){
        override fun onPreExecute() {
            super.onPreExecute()
            selectDeptModel = ArrayList()
//            context?.progressBarShow(context!!)
        }
        override fun doInBackground(vararg params: String?): String {
            var params = HashMap<String, String>()
            params.put("search",searchtext)
            if (accountID == "null" || accountID == "" ){
                params.put("accountId",accIdPref)
            }else{
                params.put("accountId",accountID)
            }

            context?.let {
                apiRequest.postRequestBodyWithHeaders(it,webService.Select_dept_list_url,params){ response->
                    selectDeptModel.clear()
                    var status = response.getString("status")
                    if (status == "200"){
                        var dataArray = response.getJSONArray("data")
                        if (dataArray.length() <=0){
                            noDataFound_layout.visibility = View.VISIBLE
                        }else{
                            noDataFound_layout.visibility = View.GONE
                        }
                        for (i in 0 until dataArray.length()){
                            var dataObj = dataArray.getJSONObject(i)
                            var id = dataObj.getString("id")
                            var name = dataObj.getString("name")
                            selectDeptModel.add(SelectDeptModel(id,name))
                            context?.progressBarDismiss(context!!)
                        }
                        setupSelectDeptRecyclerView(selectDeptModel)
                    }else{
                        context?.progressBarDismiss(context!!)
                    }

                }
            }
            return ""
        }
    }

    //    setup adapter
    private fun setupSelectDeptRecyclerView(selectDeptModel: ArrayList<SelectDeptModel>) {
           selectDeptAdapter = SelectDepartmentAdapter(context,this@SelectDepartmentFragment ,selectDeptModel)
            val llm = LinearLayoutManager(context)
            llm.orientation = LinearLayoutManager.VERTICAL
            deptListRecyclerView.layoutManager = llm
            deptListRecyclerView.adapter = selectDeptAdapter

    }
    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

    override fun showNoData() {
        noDataFound_layout.visibility = View.VISIBLE
    }

    override fun hideNoData() {
        noDataFound_layout.visibility = View.GONE
    }
}
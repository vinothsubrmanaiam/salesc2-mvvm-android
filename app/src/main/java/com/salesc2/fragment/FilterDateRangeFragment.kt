package com.salesc2.fragment

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.salesc2.R
import com.salesc2.utils.progressBarDismiss
import com.salesc2.utils.toast
import java.text.SimpleDateFormat
import java.util.*


//not in use
class FilterDateRangeFragment : Fragment() {

    private lateinit var from_date :TextView
   private lateinit var to_date : TextView
    private lateinit var mView : View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_filter_date_range, container, false)
        from_date = mView.findViewById(R.id.from_date_et)
        to_date = mView.findViewById(R.id.to_date_et)
        dateAction()
        activity?.findViewById<ImageView>(R.id.app_icon_imageView)?.visibility = View.GONE
        return mView
    }
    private val myCalendar = Calendar.getInstance()
    private fun dateAction(){
        from_date.setOnClickListener(View.OnClickListener {
            var date =
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    myCalendar[Calendar.YEAR] = year
                    myCalendar[Calendar.MONTH] = monthOfYear
                    myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                    updateLabel()
                }
//            var datePickerDialog: DatePickerDialog? = context?.let { it1 -> DatePickerDialog(it1, date, Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH) } //date is dateSetListener as per your code in question
            var datePickerDialog: DatePickerDialog? = context?.let { it1 -> DatePickerDialog(it1, date,myCalendar.get(
                Calendar.YEAR
            ), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)) } //date is dateSetListener as per your code in question
//            datePickerDialog?.datePicker?.maxDate = System.currentTimeMillis()
            datePickerDialog?.show()
        })

    to_date.setOnClickListener(View.OnClickListener {
        if (from_date.text == ""){
            context?.toast("Please select start date")
        }else{
            var date =
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    myCalendar[Calendar.YEAR] = year
                    myCalendar[Calendar.MONTH] = monthOfYear
                    myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                    updateLabelToDate()
                }
//            var datePickerDialog: DatePickerDialog? = context?.let { it1 -> DatePickerDialog(it1, date, Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH) } //date is dateSetListener as per your code in question
            var datePickerDialog: DatePickerDialog? = context?.let { it1 -> DatePickerDialog(it1, date,myCalendar.get(
                Calendar.YEAR
            ), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)) } //date is dateSetListener as per your code in question
//            datePickerDialog?.datePicker?.maxDate = System.currentTimeMillis()
            datePickerDialog?.show()
        }

    })
}
    var myFormat = String()
    var sdf = SimpleDateFormat()
    private fun updateLabel(){
        myFormat = "MM/dd/yyyy" //In which you need put here yyyy-MM-dd
        sdf = SimpleDateFormat(myFormat, Locale.US)
        from_date.text = sdf.format(myCalendar.time)

    }
    private fun updateLabelToDate(){
        myFormat = "MM/dd/yyyy" //In which you need put here yyyy-MM-dd
        sdf = SimpleDateFormat(myFormat, Locale.US)
        to_date.text = sdf.format(myCalendar.time)

    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

}
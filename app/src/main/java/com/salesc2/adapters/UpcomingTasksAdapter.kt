package com.salesc2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.TeamMemberListModel
import com.salesc2.model.UpcomingTasksModel
import java.text.SimpleDateFormat
import java.util.ArrayList

class UpcomingTasksAdapter() : RecyclerView.Adapter<UpcomingTasksAdapter.viewHolder>() {

    private lateinit var upcomingTaskItems: ArrayList<UpcomingTasksModel>
    var context: Context? = null

    constructor(context: Context?, upcomingTaskItems : ArrayList<UpcomingTasksModel>) : this(){
        this.context = context
        this.upcomingTaskItems = upcomingTaskItems

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): viewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_upcoming_tasks, parent, false)
        return viewHolder(view)
    }

    override fun getItemCount(): Int {
       return upcomingTaskItems.size
    }

    override fun onBindViewHolder(holder: viewHolder, position: Int) {
        var upcomingTasksModel : UpcomingTasksModel = upcomingTaskItems.get(position)
        holder.taskName.text = upcomingTasksModel.taskName
        try {
            var sdf = SimpleDateFormat("dd-MM-yyyy")
            var myF = SimpleDateFormat("MM-dd-yyyy")
            var date = sdf.parse(upcomingTasksModel.taskDate)
            var dateF = myF.format(date)
            holder.taskDate.text = dateF

        }catch (e:Exception){

        }



    }

    class viewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView){

        var taskDate : TextView = itemView.findViewById(R.id.upcomingTask_date)
        var taskName : TextView = itemView.findViewById(R.id.upcomingTask_title)
    }
}
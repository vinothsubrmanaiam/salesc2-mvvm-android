package com.salesc2.data.network.api.service



import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestBillingHistory
import com.salesc2.data.network.api.request.RequestBillingSubScription
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseBillingHistory
import com.salesc2.data.network.api.response.ResponseBillingSubscription
import com.salesc2.data.network.api.response.ResponseTeamList
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface BillingSubScriptionApi {

    @POST("company/api/v1/company/subscription/details")
    suspend fun reqSubscriptionDetails(
        @Body requestBillingSubScription: RequestBillingSubScription
    ): Response<ResponseBillingSubscription>
}
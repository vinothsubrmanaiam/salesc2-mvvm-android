package com.salesc2.data.network.api.response

data class ResponsePasswordAndSecurity(
    val status: String,
    val msg: String
)
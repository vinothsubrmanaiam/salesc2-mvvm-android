package com.salesc2.service

import android.content.Context
import android.util.Log
import com.android.volley.*
import com.android.volley.toolbox.*
import com.salesc2.utils.getSharedPref
import org.json.JSONObject


class ApiRequest {

   var webService = WebService()

    /**
     * POST Request function
     * passing body params
     */
    fun postRequest(context: Context, path: String, param: HashMap<String,String>, completion: (JSONObject) -> Unit) {

        var mStringRequest: StringRequest
        var mRequestQueue: RequestQueue = Volley.newRequestQueue(context)
        mStringRequest = object : StringRequest(Method.POST,path,
            Response.Listener<String> { response->
                if (response != null) {
                    var info = JSONObject(response)

                    completion(info)
                }
            },
            Response.ErrorListener { volleyError ->

                var message= String()
                if (volleyError is NetworkError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ServerError)
                {
                    message = "The server could not be found. Please try again after some time!!"
                }
                else if (volleyError is AuthFailureError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ParseError)
                {
                    message = "Parsing error! Please try again after some time"
                }
                else if (volleyError is NoConnectionError)
                {
                    message = "Failed to connect server. Please try again after some time"
                }
                else if (volleyError is TimeoutError)
                {
                    message = "Connection TimeOut! Please check your internet connection."
                }
//                context.toast(message)
             //   context.alertDialog(context,"Alert",message)

            }) {
            override fun getBodyContentType(): String {
                return "application/json"
            }
            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray {
                return JSONObject(param as Map<*, *>).toString().toByteArray()
            }
        }
        mRequestQueue.add(mStringRequest)
    }

    /**
     * POST Request function
     * passing headers
     */
    fun postRequestHeaders(
        context: Context,
        path: String,
        param: HashMap<String, String>,
        completion: (JSONObject) -> Unit
    ) {

        var mStringRequest: StringRequest
//        var mRequestQueue: RequestQueue = Volley.newRequestQueue(context)
        val cache = DiskBasedCache(context.cacheDir, 2048 * 2048)
        val network = BasicNetwork(HurlStack())

        val mRequestQueue = RequestQueue(cache, network).apply {
            start()
        }
        mStringRequest = object : StringRequest(
            Method.POST, path,
            Response.Listener<String> { response ->
                if (response != null) {
                    var info = JSONObject(response)

                    completion(info)
                }
            },
            Response.ErrorListener { volleyError ->

                var message= String()
                if (volleyError is NetworkError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ServerError)
                {
                    message = "The server could not be found. Please try again after some time!!"
                }
                else if (volleyError is AuthFailureError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ParseError)
                {
                    message = "Parsing error! Please try again after some time!!"
                }
                else if (volleyError is NoConnectionError)
                {
                    message = "Failed to connect server. Please try again after some time"
                }
                else if (volleyError is TimeoutError)
                {
                    message = "Connection TimeOut! Please check your internet connection."
                }
//                context.alertDialog(context,"Alert",message)
//                context.toast(message)
            }) {
            override fun getBodyContentType(): String {
                return "application/json"
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                var authToken = context.getSharedPref("token",context).toString()
                var header = HashMap<String,String>()
                header["Authorization"] = "Bearer $authToken"

                return header
            }
        }
        mRequestQueue.add(mStringRequest)
    }


    /**
     * POST Request function
     * passing body params
     * headers
     */
    var resp = String()
    fun postRequestBodyWithHeaders(context: Context, path: String, param: HashMap<String, String>, completion: (JSONObject) -> Unit) {

        var mStringRequest: StringRequest

        val cache = DiskBasedCache(context.cacheDir, 1024 * 1024)
        val network = BasicNetwork(HurlStack())


        mStringRequest = object : StringRequest(Method.POST,path,
            Response.Listener<String> { response->
                resp = response

                if (response != null) {
                    var info = JSONObject(response)

                    completion(info)
                }
            },
            Response.ErrorListener { volleyError ->

                var message= String()
                if (volleyError is NetworkError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ServerError)
                {

                  /*  try {
                        val res = String(
                            resp.data,
                            HttpHeaderParser.parseCharset(resp.headers, "utf-8")
                        )
                        // Now you can use any deserializer to make sense of data
                        val obj = JSONObject(res)
                    } catch (e1: UnsupportedEncodingException) {
                        // Couldn't properly decode data to string
                        e1.printStackTrace()
                    } catch (e2: JSONException) {
                        // returned data is not JSONObject?
                        e2.printStackTrace()
                    }*/
                    message = "The server could not be found. Please try again after some time!!"
                }
                else if (volleyError is AuthFailureError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ParseError)
                {
                    message = "Parsing error! Please try again after some time!!"
                }
                else if (volleyError is NoConnectionError)
                {
                 cache.clear()
                    message = "Failed to connect server. Please try again after some time"
                }
                else if (volleyError is TimeoutError)
                {
                    message = "Connection TimeOut! Please check your internet connection."
                }
//                context.alertDialog(context,"Alert",message)
//                context.toast(message)
            }) {
            override fun getBodyContentType(): String {
                return "application/json"
            }
            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                var authToken = context.getSharedPref("token",context).toString()
                var header = HashMap<String,String>()

//                header.put("Content-Type","application/x-www-form-urlencoded");
                header.put("Authorization","Bearer $authToken")
//                header.put("Content-Type", "application/json; charset=utf-8")

                return header
            }
            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray {
                return JSONObject(param as Map<*, *>).toString().toByteArray()
            }
        }
        val mRequestQueue = RequestQueue(cache, network).apply {
            start()
        }
        mRequestQueue.add(mStringRequest)
    }



    fun postRequestBodyWithHeadersAny(context: Context, path: String, param: HashMap<String, Any>, completion: (JSONObject) -> Unit) {

        var mStringRequest: StringRequest

        val cache = DiskBasedCache(context.cacheDir, 1024 * 1024)
        val network = BasicNetwork(HurlStack())


        mStringRequest = object : StringRequest(Method.POST,path,
            Response.Listener<String> { response->
                if (response != null) {
                    var info = JSONObject(response)

                    completion(info)
                }
            },
            Response.ErrorListener { volleyError ->

                var message= String()
                if (volleyError is NetworkError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ServerError)
                {
                    message = "The server could not be found. Please try again after some time!!"
                }
                else if (volleyError is AuthFailureError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ParseError)
                {
                    message = "Parsing error! Please try again after some time!!"
                }
                else if (volleyError is NoConnectionError)
                {
                    message = "Failed to connect server. Please try again after some time"
                }
                else if (volleyError is TimeoutError)
                {
                    message = "Connection TimeOut! Please check your internet connection."
                }
//                context.alertDialog(context,"Alert",message)
//                context.toast(message)

            }) {
            override fun getBodyContentType(): String {
                return "application/json"
            }
            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                var authToken = context.getSharedPref("token",context).toString()
                var header = HashMap<String,String>()
                header.put("Authorization","Bearer $authToken")

                return header
            }
            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray {
                return JSONObject(param as Map<*, *>).toString().toByteArray()
            }
        }
        val mRequestQueue = RequestQueue(cache, network).apply {
            start()
        }
        mRequestQueue.add(mStringRequest)
    }


    fun getRequestBodyWithHeaders(context: Context, path: String, param: HashMap<String, Any>, completion: (JSONObject) -> Unit) {

        var mStringRequest: StringRequest

        val cache = DiskBasedCache(context.cacheDir, 1024 * 1024)
        val network = BasicNetwork(HurlStack())


        mStringRequest = object : StringRequest(Method.GET,"$path?$param",
            Response.Listener<String> { response->
                if (response != null) {
                    var info = JSONObject(response)

                    completion(info)
                }
            },
            Response.ErrorListener { volleyError ->

                var message= String()
                if (volleyError is NetworkError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ServerError)
                {
                    message = "The server could not be found. Please try again after some time!!"
                }
                else if (volleyError is AuthFailureError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ParseError)
                {
                    message = "Parsing error! Please try again after some time!!"
                }
                else if (volleyError is NoConnectionError)
                {
                    message = "Failed to connect server. Please try again after some time"
                }
                else if (volleyError is TimeoutError)
                {
                    message = "Connection TimeOut! Please check your internet connection."
                }
//                context.alertDialog(context,"Alert",message)
//                context.toast(message)

            }) {
            override fun getBodyContentType(): String {
                return "application/json"
            }
            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                var authToken = context.getSharedPref("token",context).toString()
                var header = HashMap<String,String>()
                header.put("Authorization","Bearer $authToken")
                Log.i("Authorization","Bearer $authToken")

                return header
            }

        }
        val mRequestQueue = RequestQueue(cache, network).apply {
            start()
        }
        mRequestQueue.add(mStringRequest)
    }




    /**
     * GET Request function
     */

    fun getRequest(
        context: Context,
        path: String,
        params: HashMap<String, String>,
        completion: (response: JSONObject) -> Unit
    ) {
        val mStringRequest: StringRequest
        val mRequestQueue: RequestQueue = Volley.newRequestQueue(context)
        mStringRequest = object : StringRequest(Method.GET, path,
            Response.Listener { response ->
                if (response != null) {
                    val info = JSONObject(response)

                    completion(info)
                }
            },
            Response.ErrorListener { error ->

            }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                return params
            }
        }
        mStringRequest.setShouldCache(false)
        mRequestQueue.add(mStringRequest)
    }



    fun postRequestBodyWithHeadersForLocationService(context: Context, path: String, param: HashMap<String, Any>, completion: (JSONObject) -> Unit) {

        var mStringRequest: StringRequest

        val cache = DiskBasedCache(context.cacheDir, 1024 * 1024)
        val network = BasicNetwork(HurlStack())


        mStringRequest = object : StringRequest(Method.POST,path,
            Response.Listener<String> { response->
                resp = response

                if (response != null) {
                    var info = JSONObject(response)

                    completion(info)
                }
            },
            Response.ErrorListener { volleyError ->

                var message= String()
                if (volleyError is NetworkError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ServerError)
                {

                    /*  try {
                          val res = String(
                              resp.data,
                              HttpHeaderParser.parseCharset(resp.headers, "utf-8")
                          )
                          // Now you can use any deserializer to make sense of data
                          val obj = JSONObject(res)
                      } catch (e1: UnsupportedEncodingException) {
                          // Couldn't properly decode data to string
                          e1.printStackTrace()
                      } catch (e2: JSONException) {
                          // returned data is not JSONObject?
                          e2.printStackTrace()
                      }*/
                    message = "The server could not be found. Please try again after some time!!"
                }
                else if (volleyError is AuthFailureError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ParseError)
                {
                    message = "Parsing error! Please try again after some time!!"
                }
                else if (volleyError is NoConnectionError)
                {
                    cache.clear()
                    message = "Failed to connect server. Please try again after some time"
                }
                else if (volleyError is TimeoutError)
                {
                    message = "Connection TimeOut! Please check your internet connection."
                }


            }) {
            override fun getBodyContentType(): String {
                return "application/json"
            }
            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                var authToken = context.getSharedPref("token",context).toString()
                var header = HashMap<String,String>()

//                header.put("Content-Type","application/x-www-form-urlencoded");
                header.put("Authorization","Bearer $authToken")
//                header.put("Content-Type", "application/json; charset=utf-8")

                return header
            }
            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray {
                return JSONObject(param as Map<*, *>).toString().toByteArray()
            }
        }
        val mRequestQueue = RequestQueue(cache, network).apply {
            start()
        }
        mRequestQueue.add(mStringRequest)
    }







    fun putRequestBodyWithHeaders(context: Context, path: String, param: String, completion: (JSONObject) -> Unit) {

        var mStringRequest: StringRequest

        val cache = DiskBasedCache(context.cacheDir, 1024 * 1024)
        val network = BasicNetwork(HurlStack())


        mStringRequest = object : StringRequest(Method.PUT, "$path",
            Response.Listener<String> { response->
                if (response != null) {
                    var info = JSONObject(response)

                    completion(info)
                }
            },
            Response.ErrorListener { volleyError ->

                var message= String()
                if (volleyError is NetworkError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ServerError)
                {
                    message = "The server could not be found. Please try again after some time!!"
                }
                else if (volleyError is AuthFailureError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ParseError)
                {
                    message = "Parsing error! Please try again after some time!!"
                }
                else if (volleyError is NoConnectionError)
                {
                    message = "Failed to connect server. Please try again after some time"
                }
                else if (volleyError is TimeoutError)
                {
                    message = "Connection TimeOut! Please check your internet connection."
                }
//                context.alertDialog(context,"Alert",message)
//                context.toast(message)

            }){
            override fun getBodyContentType(): String {
                return "application/json"
            }
            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                var authToken = context.getSharedPref("token",context).toString()
                var header = HashMap<String,String>()
                header.put("Authorization","Bearer $authToken")

                return header
            }

//            @Throws(AuthFailureError::class)
//            protected override fun getParams(): MutableMap<String, String> {
//                return param
//            }
        }
        val mRequestQueue = RequestQueue(cache, network).apply {
            start()
        }
        mRequestQueue.add(mStringRequest)
    }



    fun putRequestBodyWithHeadersAny(context: Context, path: String, param: HashMap<String, Any>, completion: (JSONObject) -> Unit){

        var mStringRequest: StringRequest

        val cache = DiskBasedCache(context.cacheDir, 1024 * 1024)
        val network = BasicNetwork(HurlStack())


        mStringRequest = object : StringRequest(Method.PUT, path,
            Response.Listener<String> { response->
                if (response != null) {
                    var info = JSONObject(response)

                    completion(info)
                }
            },
            Response.ErrorListener { volleyError ->

                var message= String()
                if (volleyError is NetworkError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ServerError)
                {
                    message = "The server could not be found. Please try again after some time!!"
                }
                else if (volleyError is AuthFailureError)
                {
                    message = "Cannot connect to Internet. Please check your connection!"
                }
                else if (volleyError is ParseError)
                {
                    message = "Parsing error! Please try again after some time!!"
                }
                else if (volleyError is NoConnectionError)
                {
                    message = "Failed to connect server. Please try again after some time"
                }
                else if (volleyError is TimeoutError)
                {
                    message = "Connection TimeOut! Please check your internet connection."
                }
//                context.alertDialog(context,"Alert",message)
//                context.toast(message)

            }){
            override fun getBodyContentType(): String {
                return "application/json"
            }
            @Throws(AuthFailureError::class)
            override fun getHeaders(): MutableMap<String, String> {
                var authToken = context.getSharedPref("token",context).toString()
                var header = HashMap<String,String>()
                header.put("Authorization","Bearer $authToken")

                return header
            }

            @Throws(AuthFailureError::class)
            override fun getBody(): ByteArray {

                return JSONObject(param as Map<*, *>).toString().toByteArray()
            }
        }
        val mRequestQueue = RequestQueue(cache, network).apply {
            start()
        }
        mRequestQueue.add(mStringRequest)
    }

}
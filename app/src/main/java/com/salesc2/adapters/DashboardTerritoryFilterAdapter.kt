package com.salesc2.adapters

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.fragment.DashboardFragment
import com.salesc2.model.FilterTerritoryModel
import com.salesc2.utils.capFirstLetter
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.loadFragment
import com.salesc2.utils.setSharedPref
import java.util.ArrayList

class DashboardTerritoryFilterAdapter() : RecyclerView.Adapter<DashboardTerritoryFilterAdapter.MyViewHolder>() {

    private lateinit var filterTerritoryItems: ArrayList<FilterTerritoryModel>
    var context: Context? = null
    var dash_filter_territory_position = String()
    constructor(context: Context?, filterTerritoryItems: ArrayList<FilterTerritoryModel>) : this(){
        this.context = context
        this.filterTerritoryItems = filterTerritoryItems

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_select_list, parent, false)
        dash_filter_territory_position = context?.getSharedPref("dash_filter_territory_position",context!!).toString()
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return filterTerritoryItems.size
    }

    var row_index = -1
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var filterTerritoryModel : FilterTerritoryModel = filterTerritoryItems[position]
        holder.filter_name.text = context?.capFirstLetter(filterTerritoryModel.territory_name)
        dash_filter_territory_position = context?.getSharedPref("dash_filter_territory_position",context!!).toString()

        if (dash_filter_territory_position != "null"){
            row_index=dash_filter_territory_position.toInt()
            if (row_index.equals(dash_filter_territory_position)){
                context?.let { ContextCompat.getColor(it, R.color.white) }?.let {
                    holder.filter_name.setTextColor(it)
                }
                context?.let { ContextCompat.getDrawable(it, R.drawable.corner_shape_green_bg) }?.let {
                    holder.filter_name.setBackgroundDrawable(it)
                }
            }
        }

        holder.itemView.setOnClickListener(View.OnClickListener {

            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("dash_filter_territory_position")
            editor.commit()
            context?.setSharedPref(context!!,"dash_territory_id",filterTerritoryModel.territory_id)
            context?.setSharedPref(context!!,"dash_territory_name",filterTerritoryModel.territory_name)
            context?.loadFragment(context!!,DashboardFragment())
            row_index=position
            notifyDataSetChanged()
            (context as MainActivity).resetTerritoryOnClick(context as MainActivity)
        })
        if(row_index==position){
            context?.setSharedPref(context!!,"dash_territory_id",filterTerritoryModel.territory_id)
            context?.setSharedPref(context!!,"dash_territory_name",filterTerritoryModel.territory_name)
            context?.setSharedPref(context!!,"dash_filter_territory_position",position.toString())
            context?.let { ContextCompat.getColor(it, R.color.white) }?.let {
                holder.filter_name.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it, R.drawable.corner_shape_green_bg) }?.let {
                holder.filter_name.setBackgroundDrawable(it)
            }

        }
        else
        {
            context?.let { ContextCompat.getColor(it, R.color.filter_left_menu_bg) }?.let {
                holder.filter_name.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.light_grey) }?.let {
                holder.filter_name.setBackgroundColor(it)
            }

        }
    }
    class MyViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView) {
        var filter_name : TextView = itemView.findViewById(R.id.select_list_name)
        val list_layout : ConstraintLayout = itemView.findViewById(R.id.list_layout)
    }
}
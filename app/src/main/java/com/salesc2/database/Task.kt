package com.salesc2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Task (
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "task_id") val task_id: String?,
    @ColumnInfo(name = "startDate") val startDate: String?,
    @ColumnInfo(name = "endDate") val endDate: String?,
    @ColumnInfo(name = "taskStatus") val taskStatus: String?,
    @ColumnInfo(name = "taskName") val taskName: String?,
    @ColumnInfo(name = "lat") val lat: String?,
    @ColumnInfo(name = "lng") val lng: String?,
    @ColumnInfo(name = "timeline") val timeline: String?,
    @ColumnInfo(name = "assignTo_id") val assignTo_id: String?,
    @ColumnInfo(name = "assignTo_name") val assignTo_name: String?,
    @ColumnInfo(name = "departmentName") val departmentName: String?,
    @ColumnInfo(name = "department_contacts") val department_contacts: String?,
    @ColumnInfo(name = "expenses") val expenses: String?,
    @ColumnInfo(name = "products") val products: String?,
    @ColumnInfo(name = "service_id") val service_id: String?,
@ColumnInfo(name = "service_name") val service_name: String?,
@ColumnInfo(name = "dept_id") val dept_id: String?,
@ColumnInfo(name = "dept_name") val dept_name: String?,
    @ColumnInfo(name = "account_id") val account_id: String?,
    @ColumnInfo(name = "account_name") val account_name: String?

    )
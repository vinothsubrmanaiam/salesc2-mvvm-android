package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.response.ResponseTeamList
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.TeamListRepository

import kotlinx.coroutines.launch

class TeamListViewModel(
    private val teamListRepository: TeamListRepository,
    val context: Context
) : ViewModel() {

fun reqTeamList(
    onSuccess: OnSuccess<ResponseTeamList>,
    onFailure: OnFailure<ResponseTeamList>

){
    viewModelScope.launch {
        teamListRepository.reqTeamList(onSuccess,onFailure)
    }
}

}
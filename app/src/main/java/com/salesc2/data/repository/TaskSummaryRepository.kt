package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestDashBoard
import com.salesc2.data.network.api.request.RequestTaskSummary
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseDashBoard
import com.salesc2.data.network.api.response.ResponseTaskSummary
import com.salesc2.data.network.api.service.AccountListApi
import com.salesc2.data.network.api.service.DashBoardAPI
import com.salesc2.data.network.api.service.TaskSummaryApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface TaskSummaryRepository {

    suspend fun reqGetTaskDetails(
      requestTaskSummary: RequestTaskSummary,
    onSuccess: OnSuccess<ResponseTaskSummary>,
    onFailure: OnFailure<ResponseTaskSummary>

    )

    companion object Factory{
        fun create(api : TaskSummaryApi) : TaskSummaryRepository = TaskSummaryRepositoryImpl(api)
    }
}

class TaskSummaryRepositoryImpl(var api: TaskSummaryApi) : TaskSummaryRepository {
    override suspend fun reqGetTaskDetails(
        requestTaskSummary: RequestTaskSummary,
        onSuccess: OnSuccess<ResponseTaskSummary>,
        onFailure: OnFailure<ResponseTaskSummary>
    ) {


        withContext(Dispatchers.IO) {
            try {
                val response = api.reqGetTaskDetails(requestTaskSummary = requestTaskSummary)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status==200) {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }
    }

}

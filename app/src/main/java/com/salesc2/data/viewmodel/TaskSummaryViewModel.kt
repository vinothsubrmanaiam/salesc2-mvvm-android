package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestTaskSummary
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.api.response.ResponseTaskSummary
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.AccountListRepository
import com.salesc2.data.repository.EmployeeLoginRepository
import com.salesc2.data.repository.TaskSummaryRepository

import kotlinx.coroutines.launch

class TaskSummaryViewModel(
    private val repository: TaskSummaryRepository,
    val context: Context
) : ViewModel() {
     fun reqGetTaskSummary(
         requestTaskSummary: RequestTaskSummary,
         onSuccess: OnSuccess<ResponseTaskSummary>,
         onFailure: OnFailure<ResponseTaskSummary>
    )
     {
         viewModelScope.launch {
             repository.reqGetTaskDetails(requestTaskSummary,onSuccess,onFailure)
         }
     }

}
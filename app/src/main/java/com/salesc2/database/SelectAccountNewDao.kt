package com.digi.store.util.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.salesc2.database.DashBoard
import com.salesc2.database.SelectAccount
import com.salesc2.database.SelectAccountNew


@Dao
interface SelectAccountNewDao {

   @Query("SELECT * FROM SelectAccountNew")
    suspend fun getAll(): List<SelectAccountNew>



    @Insert
    suspend fun insertAll(vararg dashBoard: SelectAccountNew)


    @Query("DELETE FROM SelectAccountNew")
    fun deleteAll()

 @Query("SELECT * FROM SelectAccountNew WHERE account_id = :id")
 fun getDeparment(id: String): List<SelectAccountNew>


}

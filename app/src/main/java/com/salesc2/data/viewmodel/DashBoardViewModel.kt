package com.salesc2.data.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.salesc2.data.network.api.request.RequestDashBoard
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseDashBoard
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import com.salesc2.data.repository.DashBoardRepository
import com.salesc2.data.repository.EmployeeLoginRepository

import kotlinx.coroutines.launch

class DashBoardViewModel(
    private val repository: DashBoardRepository,
    val context: Context
) : ViewModel() {
     fun reqDashboardData(
         requestDashBoard: RequestDashBoard,
         onSuccess: OnSuccess<ResponseDashBoard>,
         onFailure: OnFailure<ResponseDashBoard>
    )
     {
         viewModelScope.launch {
             repository.reqDashboardData(requestDashBoard,onSuccess,onFailure)
         }
     }

}
package com.salesc2.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.fragment.SelectAccountFragment
import com.salesc2.model.AccountDepartmentContactModel
import com.salesc2.model.AccountDepartmentDetailModel
import com.salesc2.model.AccountDepartmentEmailModel
import com.salesc2.model.AccountDepartmentPhoneModel
import com.salesc2.utils.capFirstLetter
import com.salesc2.utils.loadFragment
import java.util.*

class AccountDetailDepartmentListAdapter() :
    RecyclerView.Adapter<AccountDetailDepartmentListAdapter.viewHolder>(),
    Parcelable,
    Filterable {

    private lateinit var account_dept_list: ArrayList<AccountDepartmentDetailModel>
    var context: Context? = null

    constructor(
        context: Context?,
        account_dept_list: ArrayList<AccountDepartmentDetailModel>
    ) : this() {
        this.context = context
        this.account_dept_list = account_dept_list
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): viewHolder {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.accounts_detail_department, parent, false)
        return viewHolder(view)
    }


    @SuppressLint("WrongConstant")
    override fun onBindViewHolder(holder: viewHolder, position: Int) {

        //holder.itemView.tag = position
        val accountDepartmentDetailModel: AccountDepartmentDetailModel =
            account_dept_list.get(position)

        holder.department_name_tv?.text = context?.capFirstLetter(accountDepartmentDetailModel.departmentName)
        var contactSize = accountDepartmentDetailModel.accountDepartmentContactModel.size

        val accountDepartmentContactlist = ArrayList<AccountDepartmentContactModel>()
        for (i in 0 until contactSize) {
            var accountDepartmentContactModel =
                accountDepartmentDetailModel.accountDepartmentContactModel[i]
            var contactname = accountDepartmentContactModel.contactname
            var phoneSize = accountDepartmentContactModel.accountDepartmentPhoneModel.size
            val accountDepartmentPhoneList = ArrayList<AccountDepartmentPhoneModel>()
            for (j in 0 until phoneSize) {
                var accountDepartmentPhoneModel =
                    accountDepartmentContactModel.accountDepartmentPhoneModel[j]

                var phone_id = accountDepartmentPhoneModel.phone_id
                var phoneNo = accountDepartmentPhoneModel.phoneNo
                var phoneType = accountDepartmentPhoneModel.phoneType

                accountDepartmentPhoneList.add(
                    AccountDepartmentPhoneModel(
                        phone_id,
                        phoneNo,
                        phoneType
                    )
                )
            }
            var emailSize = accountDepartmentContactModel.accountDepartmentEmailModel.size
            val accountDepartmentEmailList = ArrayList<AccountDepartmentEmailModel>()
            for (k in 0 until emailSize) {
                var accountDepartmentEmailModel =
                    accountDepartmentContactModel.accountDepartmentEmailModel[k]
                var email_id = accountDepartmentEmailModel.email_id
                var emailid = accountDepartmentEmailModel.emailid
                var emailType = accountDepartmentEmailModel.emailType
                accountDepartmentEmailList.add(
                    AccountDepartmentEmailModel(
                        emailid,
                        email_id,
                        emailType
                    )
                )
            }
            //holder.email_tv?.text = accountDepartmentDetailModel.emailid
            // holder.email_type_tv?.text = "(" + accountDepartmentDetailModel.emailType + ")"
            // holder.phone_type_tv?.text = "(" + accountDepartmentDetailModel.phoneType + ")"
            /*  val childLayoutManager = LinearLayoutManager(
                  holder.itemView.customername_rv.context,
                  LinearLayout.VERTICAL,
                  false
              )

              holder.itemView.customername_rv.apply {
                  layoutManager = childLayoutManager
                  adapter = AccountDepartmentCustomerListAdapter(accountDepartmentDetailModel)
                  setRecycledViewPool(viewPool)
              }*/

            accountDepartmentContactlist.add(
                AccountDepartmentContactModel(
                    contactname,
                    accountDepartmentPhoneList,
                    accountDepartmentEmailList
                )
            )
        }

        //setup adapter
        val adapter: RecyclerView.Adapter<*> =
            AccountDepartmentCustomerListAdapter(
                context,
                accountDepartmentContactlist
            )
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        holder.customername_rv?.layoutManager = llm
        holder.customername_rv?.adapter = adapter

        holder.add_task.setOnClickListener{
            context?.loadFragment(context!!,SelectAccountFragment())
        }
    }




    class viewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var department_name_tv: TextView? = itemView.findViewById(R.id.department_name_tv)
        var customername_rv: RecyclerView? = itemView.findViewById(R.id.customername_rv)

        var add_task : TextView = itemView.findViewById(R.id.add_task_tv)
        /*var phone_tv: TextView?  = itemView.findViewById(R.id.phone_tv)
        var phone_type_tv: TextView?  = itemView.findViewById(R.id.phone_type_tv)
        var second_phone_tv: TextView?  = itemView.findViewById(R.id.second_phone_tv)
        var email_tv: TextView?  = itemView.findViewById(R.id.email_tv)
        var email_type_tv: TextView?  = itemView.findViewById(R.id.email_type_tv)*/
    }

    override fun getItemCount(): Int {
        return account_dept_list.size
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AccountDetailDepartmentListAdapter> {
        override fun createFromParcel(parcel: Parcel): AccountDetailDepartmentListAdapter {
            return AccountDetailDepartmentListAdapter()
        }

        override fun newArray(size: Int): Array<AccountDetailDepartmentListAdapter?> {
            return arrayOfNulls(size)
        }
    }


    override fun getFilter(): Filter {
        TODO("Not yet implemented")
    }

}
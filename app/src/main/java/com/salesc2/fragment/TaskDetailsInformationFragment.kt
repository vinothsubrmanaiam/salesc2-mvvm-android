package com.salesc2.fragment

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.utils.toast

class TaskDetailsInformationFragment: Fragment() {
    private lateinit var serviceName: TextView
    private lateinit var accountName : TextView
    private lateinit var tdi_phone : TextView
    private lateinit var tdi_email : TextView
    private lateinit var tdi_location : TextView
    private lateinit var tdi_desc : TextView
    private lateinit var tdi_getDirection: TextView
    private lateinit var descLayout : ConstraintLayout

    private var service_name = String()
    private var account_name = String()
    private var phone = String()
    private var email = String()
    private var location = String()
    private var latt = String()
    private var long = String()
    private var desc = String()

    lateinit var mView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance=true
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_task_details_information,container,false)
        service_name = arguments?.getString("tdi_service_name").toString()
        account_name = arguments?.getString("tdi_account_name").toString()
        phone = arguments?.getString("tdi_phone").toString()
        email = arguments?.getString("tdi_email").toString()
        location = arguments?.getString("tdi_location_name").toString()
        latt = arguments?.getString("tdi_lat").toString()
        long = arguments?.getString("tdi_lon").toString()
        desc = arguments?.getString("tdi_desc").toString()

        serviceName = mView.findViewById(R.id.serviceName_tv)
        accountName = mView.findViewById(R.id.tdi_accountName_tv)
        tdi_phone = mView.findViewById(R.id.tdi_phone_tv)
        tdi_email = mView.findViewById(R.id.tdi_email_tv)
        tdi_location = mView.findViewById(R.id.tdi_location_tv)
        tdi_desc = mView.findViewById(R.id.tdi_desc_tv)
        tdi_getDirection = mView.findViewById(R.id.tdi_getDirection_tv)
        descLayout = mView.findViewById(R.id.descLayout)
        serviceName.text = service_name
        accountName.text = account_name

        tdi_phone.text = phone
        tdi_email.text = email
        tdi_location.text= location
        tdi_desc.text = desc
        if (tdi_desc.text == ""){
            descLayout.visibility = View.GONE
        }
        if (tdi_email.text == ""){
            tdi_email.visibility = View.GONE
        }
        if (tdi_phone.text == ""){
            tdi_phone.visibility = View.GONE
        }
        if (tdi_location.text == "")
        {
            tdi_location.visibility = View.GONE
        }
        getDirectionAction()
        phoneEmailClick()
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<ImageView>(R.id.app_icon_imageView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }
    private fun getDirectionAction(){
        tdi_getDirection.setOnClickListener{
        try {
            var uri: String = "http://maps.google.com/maps?daddr=${latt},${long}"
            var intent: Intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
            intent.setPackage("com.google.android.apps.maps")
            context?.startActivity(intent)
        }
        catch (e: Exception){
            print(e)
        }
        }
    }

    private fun phoneEmailClick(){
        tdi_phone.setOnClickListener{
            try {
                var callIntent: Intent =
                    Intent(Intent.ACTION_DIAL, Uri.parse("tel: ${tdi_phone.text.toString()}"))
                context?.startActivity(callIntent)
            }catch (e:Exception){
                context?.toast(e.toString())
            }
            catch (activityException: ActivityNotFoundException) {
                Log.e("Calling a Phone Number", "Call failed", activityException)
            }
        }
        tdi_email.setOnClickListener{


            val TO = arrayOf(tdi_email.text.toString())
            val emailIntent = Intent(Intent.ACTION_SEND)
            emailIntent.data = Uri.parse("mailto:")
            emailIntent.type = "text/plain"
            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO)
            emailIntent.setPackage("com.google.android.gm")


            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."))
            } catch (ex: ActivityNotFoundException) {
            }catch (e:Exception){
                println(e)
            }
        }
    }
}
package com.salesc2.data.network.api.service

import com.salesc2.data.network.api.request.RequestPasswordAndSecurity
import com.salesc2.data.network.api.response.ResponsePasswordAndSecurity
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface PasswordAndSecurityApi {
    @POST("ios-task/api/v1/employee/changePassword")
    suspend fun reqPwSec(
        @Body reqPwSec: RequestPasswordAndSecurity
    ): Response<ResponsePasswordAndSecurity>
}
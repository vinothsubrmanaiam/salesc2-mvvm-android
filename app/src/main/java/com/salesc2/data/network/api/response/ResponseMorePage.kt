package com.salesc2.data.network.api.response

data class ResponseMorePage(
    val msg: String,
    val status: String
)
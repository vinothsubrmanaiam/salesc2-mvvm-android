package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestPasswordAndSecurity
import com.salesc2.data.network.api.request.RequestUpdateTimeZone
import com.salesc2.data.network.api.response.ResponsePasswordAndSecurity
import com.salesc2.data.network.api.response.ResponseUpdateTimeZone
import com.salesc2.data.network.api.service.PasswordAndSecurityApi
import com.salesc2.data.network.api.service.UpdateTimeZoneApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface UpdateTimeZoneRepository {
    suspend fun reqUpdateTZ(
        requestUpdateTZ: RequestUpdateTimeZone,
        onSuccess: OnSuccess<ResponseUpdateTimeZone>,
        onFailure: OnFailure<ResponseUpdateTimeZone>

    )

    companion object Factory{
        fun create(api : UpdateTimeZoneApi) : UpdateTimeZoneRepository = UpdateTimeZoneRepositoryImpl(api)
    }
}

class UpdateTimeZoneRepositoryImpl(var api: UpdateTimeZoneApi) : UpdateTimeZoneRepository {
    override   suspend fun reqUpdateTZ(
        requestUpdateTZ: RequestUpdateTimeZone,
        onSuccess: OnSuccess<ResponseUpdateTimeZone>,
        onFailure: OnFailure<ResponseUpdateTimeZone>
    ){


        withContext(Dispatchers.IO) {
            try {
                val response = api.reqUpdateTimeZone(reqUpdateTimeZone = requestUpdateTZ)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status.toInt()==200) {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }
    }
}
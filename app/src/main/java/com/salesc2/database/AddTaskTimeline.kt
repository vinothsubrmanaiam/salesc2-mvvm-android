package com.salesc2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class AddTaskTimeline (
    @PrimaryKey(autoGenerate = true) val timeline_id: Int,
    @ColumnInfo(name = "companyId") val companyId: String?,
    @ColumnInfo(name = "accountId") val accountId: String?,
    @ColumnInfo(name = "departmentId") val departmentId: String?,
    @ColumnInfo(name = "serviceId") val serviceId: String?,
    @ColumnInfo(name = "assignedBy") val assignedBy: String?,
    @ColumnInfo(name = "createdBy") val createdBy: String?,
    @ColumnInfo(name = "assignedTo") val assignedTo: String?,
    @ColumnInfo(name = "recurringOption") val recurringOption: String?,
    @ColumnInfo(name = "startDate") val startDate: String?,
    @ColumnInfo(name = "endDate") val endDate: String?,
    @ColumnInfo(name = "taskDescription") val taskDescription: String?,
    @ColumnInfo(name = "taskStatus") val taskStatus: String?,
    @ColumnInfo(name = "products") val products: String?,
    @ColumnInfo(name = "timezone") val timezone: String?,
    @ColumnInfo(name = "createdAt") val createdAt: String?,
    @ColumnInfo(name = "updatedAt") val updatedAt: String?,
    @ColumnInfo(name = "type1") val type1: String?,
    @ColumnInfo(name = "type2") val type2: String?,
    @ColumnInfo(name = "type3") val type3: String?,
    @ColumnInfo(name = "type4") val type4: String?,
    @ColumnInfo(name = "type5") val type5: String?,
    @ColumnInfo(name = "type6") val type6: String?,
    @ColumnInfo(name = "type7") val type7: String?,
    @ColumnInfo(name = "type8") val type8: String?,
    @ColumnInfo(name = "type9") val type9: String?,
    @ColumnInfo(name = "type10") val type10: String?,
    @ColumnInfo(name = "type11") val type11: String?
    )
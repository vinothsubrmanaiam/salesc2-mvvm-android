package com.salesc2.data.network.api.response

data class ResponseBillingHistory(
    val `data`: List<Data>,
    val msg: String,
    val pager: Pager,
    val status: Int
) {
    data class Data(
        val _id: String,
        val amount: Double,
        val billingCycle: String,
        val createdAt: String,
        val invoiceNumber: String,
        val startDate: String,
        val status: Int,
        val subscription: String
    )

    data class Pager(
        val currentPage: Int,
        val endPage: Int,
        val totalItems: Int,
        val totalPages: Int
    )
}
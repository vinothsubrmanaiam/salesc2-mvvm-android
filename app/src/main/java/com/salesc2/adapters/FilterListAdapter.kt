package com.salesc2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.fragment.*
import com.salesc2.model.FilterListModel
import com.salesc2.utils.loadFilterFragment
import java.util.ArrayList

class FilterListAdapter() : RecyclerView.Adapter<FilterListAdapter.MyViewHolder>() {
    private lateinit var filterListItems: ArrayList<FilterListModel>
    var context: Context? = null
    var row_index =  -1

    constructor(context: Context?, filterListItems: ArrayList<FilterListModel>) : this(){
        this.context = context
        this.filterListItems = filterListItems

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_filter_list, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return filterListItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var filterListModel:FilterListModel = filterListItems[position]
        holder.filter_name.text = filterListModel.filter_name

        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            holder.filter_name.setBackgroundColor(it)
        }


        holder.itemView.setOnClickListener{
            row_index=position
            notifyDataSetChanged()
            if (filterListModel.filter_name == "Status"){
                context?.loadFilterFragment(context!!,FilterStatusFragment())
            }
            else if (filterListModel.filter_name == "Account"){
                context?.loadFilterFragment(context!!,FilterAccountFragment())
            }
            else if (filterListModel.filter_name == "Date range"){
                context?.loadFilterFragment(context!!,FilterDateRangeFragment())
            }
            else if (filterListModel.filter_name == "Assignee"){
                context?.loadFilterFragment(context!!,FilterAssigneeFragment())
            }
            else if (filterListModel.filter_name == "Territory"){
                context?.loadFilterFragment(context!!,FilterTerritoryFragment())
            }

        }
        if(row_index==position){
            context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                holder.filter_name.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
                holder.filter_name.setBackgroundColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_filter_black_bg) }?.let {
                holder.filter_name.setBackgroundDrawable(it)
            }
        }
        else
        {
            context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
                holder.filter_name.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
                holder.filter_name.setBackgroundColor(it)
            }

        }


    }
    class MyViewHolder constructor(itemView : View): RecyclerView.ViewHolder(itemView) {
      var filter_name : TextView = itemView.findViewById(R.id.filter_name)
    }
}
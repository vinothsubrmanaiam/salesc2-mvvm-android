package com.salesc2.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AppComponentFactory
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Criteria
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import java.io.IOException
import java.util.*

class SelectEventLocationFragment:Fragment() {
    lateinit var eventLocation: TextView
    private lateinit var Loc_Continue: Button
    private lateinit var select_loc_back_arrow :ImageView
    private lateinit var close_iv : ImageView

    private var locationManager: LocationManager? = null
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    var PERMISSION_ID = 101
    private var lattitude = String()
    private var longitude = String()
    lateinit var mView: View
    private var addEventEdit = String()
    private val PLACE_PICKER_REQUEST = 1
    private var edit_event = String()
    private lateinit var currentLocation_tv: TextView
    private lateinit var eventLocHead : TextView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_select_event_location,container,false)
        addEventEdit = context?.getSharedPref("edit_add_event",context!!).toString()
        edit_event = context?.getSharedPref("edit_event",context!!).toString()
        eventLocation = mView.findViewById(R.id.eventLocation)
        Loc_Continue = mView.findViewById(R.id.Loc_Continue)
        select_loc_back_arrow = mView.findViewById(R.id.select_loc_back_arrow)
        currentLocation_tv = mView.findViewById(R.id.currentLocation_tv)
        close_iv = mView.findViewById(R.id.close_iv)
        eventLocHead = mView.findViewById(R.id.eventLocHead)
        if (edit_event == "yes"){
            eventLocHead.text = "Edit work event"
        }
        mFusedLocationClient = context?.let { LocationServices.getFusedLocationProviderClient(it) }!!
        // currentLocationName()
        locationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        getLastLocation()
        locationOnClickAction()
        gotoDateTime()
        eventLocationText()
        goBackAction()
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE


        return mView
    }

    private fun goBackAction(){
        select_loc_back_arrow.setOnClickListener{
//            fragmentManager?.popBackStack()
            context?.loadFragment(context!!,SelectEventTypeFragment())
        }
        close_iv.setOnClickListener{
            startActivity(Intent(context, MainActivity::class.java))
        }
    }


    private fun placeAutoComplete() {

        if (!Places.isInitialized()) {
            Places.initialize(context!!, "AIzaSyD6L3A5wCPVO2mt3KeLTZ-fnl2kIEAs690", Locale.US);
        }
        var fields=Arrays.asList(Place.Field.ID,Place.Field.NAME,Place.Field.LAT_LNG)
        var intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(context!!)
        startActivityForResult(intent, PLACE_PICKER_REQUEST)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                val place =Autocomplete.getPlaceFromIntent(data!!);

                var lat = place.latLng?.latitude
                lattitude = lat.toString()
                var lng = place.latLng?.longitude
                longitude = lng.toString()
                eventLocation.setText(place.name)
            }
            else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                var status = Autocomplete.getStatusFromIntent(data!!)
                Log.i("address", status.getStatusMessage().toString())
            }
        }
    }


    private fun eventLocationText(){

        eventLocation.setOnClickListener{
            placeAutoComplete()
        }

    }

    private fun gotoDateTime(){
        Loc_Continue.setOnClickListener{
                if (addEventEdit == "101"){
                    context?.setSharedPref(context!!,"eLocation_name", eventLocation.text.toString())
                    context?.setSharedPref(context!!,"latitude", lattitude)
                    context?.setSharedPref(context!!,"longitude", longitude)
                    context?.loadFragment(context!!, AddEventFragment())
                }else {
                    context?.setSharedPref(context!!,"eLocation_name", eventLocation.text.toString())
                    context?.setSharedPref(context!!,"latitude", lattitude)
                    context?.setSharedPref(context!!,"longitude", longitude)
                    context?.loadFragment(context!!, SelectEventDateTimeFragment())
                }


        }
    }
    @SuppressLint("MissingPermission")
    private fun getLastLocation()  {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                activity?.let {
                    mFusedLocationClient.lastLocation.addOnCompleteListener(it) { task ->
                        var location: Location? = task.result
                        if (location == null) {
                            requestNewLocationData()
                        } else {
                            lattitude  = location.latitude.toString()
                            longitude = location.longitude.toString()
                        }
                    }
                }
            } else {
                Toast.makeText(context, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = context?.let { LocationServices.getFusedLocationProviderClient(it) }!!
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            lattitude= mLastLocation.latitude.toString()
            longitude = mLastLocation.longitude.toString()
        }
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (context?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            } == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        activity?.let {
            ActivityCompat.requestPermissions(
                it,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSION_ID
            )
        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
    }





    private fun locationOnClickAction(){
        currentLocation_tv.setOnClickListener{
            currentLocationName()
        }

        eventLocation.setOnTouchListener(object: View.OnTouchListener {

            override fun onTouch(v:View, event: MotionEvent):Boolean {
                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3
                if (event.action === MotionEvent.ACTION_DOWN)
                {
                    if (event.rawX >= (eventLocation.right - eventLocation.compoundDrawables[DRAWABLE_RIGHT].bounds.width()))
                    {
                        currentLocationName()
                        return true
                    }
                }
                return false
            }
        })
    }


    private fun currentLocationName(){

        val geoCoder = Geocoder(context, Locale.getDefault()) //it is Geocoder
        val builder = StringBuilder()
        try
        {
            val location: Location? = null
            val lat: String = lattitude
            val lng: String = longitude

            val address = lat.toDouble().let { lng.toDouble().let { it1 -> geoCoder.getFromLocation(it, it1, 1) } }
            val maxLines = address?.get(0)?.maxAddressLineIndex
            eventLocation.setText(address?.get(0)?.locality)

        }
        catch (e: Exception) {
            print(e)
        }
        catch (e:NullPointerException) {
        }
    }

}
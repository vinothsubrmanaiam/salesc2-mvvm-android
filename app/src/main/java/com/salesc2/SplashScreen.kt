package com.salesc2

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.preference.PreferenceManager
import android.provider.Settings
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.setSharedPref
import java.util.*

class SplashScreen : AppCompatActivity() {

    var bottomtext_tv : TextView? = null
    var custom_font: Typeface? = null
    var loginEmail = String()
    private val READ_LOCATION_PERMISSION = 1001
    private val REQUEST_LOCATION_PERMISSION = 1
    private var REQUEST_CODE_LOCATION = 1111
    var PERMISSION_ID = 143
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        this.setSharedPref(this,"user_wish_show","yes")
        this.setSharedPref(this,"dash_tab_click","taskOnClick")
        mFusedLocationClient = this.let { LocationServices.getFusedLocationProviderClient(it) }!!
      //  FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
        FirebaseApp.initializeApp(applicationContext)
        var firebaseAnalytics : FirebaseAnalytics = FirebaseAnalytics.getInstance(applicationContext)
        firebaseAnalytics.setAnalyticsCollectionEnabled(true)

        getLastLocation()
        //inintializeing views

        val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        val editor : SharedPreferences.Editor = preferences.edit()
        //filter prefs
                    editor.remove("filter_account_id")
                    editor.remove("filter_assignee_id")
                    editor.remove("filter_status_name")
                    editor.remove("filter_start_dt")
                    editor.remove("filter_end_dt")
                    editor.remove("filter_territory_id")
                    //filter items
                    editor.remove("status_position")
                    editor.remove("filter_assignee_position")
                    editor.remove("filter_account_position")
                    editor.remove("filter_territory_position")
        editor.commit()


        //status bar color
        window.statusBarColor = ContextCompat.getColor(
            applicationContext,
            R.color.blacktext
        )
        custom_font = Typeface.createFromAsset(assets, "fonts/SFProDisplay-Regular.ttf")
        bottomtext_tv = findViewById(R.id.bottomtext_tv)
        bottomtext_tv?.typeface = custom_font



        if (!checkRuntimePermission()) {
            requestRuntimePermission()
        }
        else {
          movetonextscreen()
        }



    }

    private fun movetonextscreen() {
        var userToken = getSharedPref("token", this)
        val timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                if (!userToken.equals("null", ignoreCase = true)) {
                    startActivity(Intent(this@SplashScreen, MainActivity::class.java))
                    finish()
                } else {
                    startActivity(Intent(this@SplashScreen, LoginActivity::class.java))
                    finish()
                }
            }
        }, 2000)
    }


    private fun requestRuntimePermission() {
        requestPermissions(
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE
            ), READ_LOCATION_PERMISSION
        )
        movetonextscreen()
    }


    private fun checkRuntimePermission(): Boolean {

        val writeablePermission =
            ContextCompat.checkSelfPermission(
              this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
        if (writeablePermission != PackageManager.PERMISSION_GRANTED) {
            getLastLocation()
            return false
        }
        return true
    }



    /**
     * get lat lon
     */

    @SuppressLint("MissingPermission")
    fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                       var lati = location.latitude.toString()
                      var  long = location.longitude.toString()
                        setSharedPref(this, "current_lat", lati)
                        setSharedPref(this, "current_lon", long)

                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }


    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
            PERMISSION_ID
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                enableMyLocation()
            }
        }
        if (requestCode == REQUEST_CODE_LOCATION && grantResults.isNotEmpty()
            && grantResults[0] != PackageManager.PERMISSION_GRANTED
        ) {
            throw RuntimeException("Location services are required in order to " + "connect to a reader.")
        }
        if (requestCode == READ_LOCATION_PERMISSION){
            val granted = grantResults.isNotEmpty()
                    && permissions.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && !ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                permissions[0]
            )
            when (granted) {
                true -> movetonextscreen()  //  openImagePicker()
                else -> Toast.makeText(this, "Check Permission", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
           var lati = mLastLocation.latitude.toString()

           var long = mLastLocation.longitude.toString()


            setSharedPref(this@SplashScreen, "current_lat", lati)
            setSharedPref(this@SplashScreen, "current_lon", long)


        }
    }
    private fun enableMyLocation() {
        if (isPermissionGranted()) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
        } else {
            this.let {
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION_PERMISSION
                )
            }
        }
    }

    private fun isPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }
}
package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestMorePage
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.api.response.ResponseMorePage
import com.salesc2.data.network.api.service.MorePageApi
import com.salesc2.data.network.api.service.UserLoginApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface MorePageRepository {

    suspend fun reqLogOut(
        requestMorePage: RequestMorePage,
        onSuccess: OnSuccess<ResponseMorePage>,
        onFailure: OnFailure<ResponseMorePage>
    )

    companion object Factory {
        fun create(api: MorePageApi): MorePageRepository = MorePageRepositoryImpl(api)
    }
}

private class MorePageRepositoryImpl(private val api: MorePageApi) : MorePageRepository {
    override suspend fun reqLogOut(
        requestMorePage: RequestMorePage,
        onSuccess: OnSuccess<ResponseMorePage>,
        onFailure: OnFailure<ResponseMorePage>
    ) {
        withContext(Dispatchers.IO) {
            try {
                val response = api.reqMorePage(requestMorePage = requestMorePage)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status.equals("204")) {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }

    }

}

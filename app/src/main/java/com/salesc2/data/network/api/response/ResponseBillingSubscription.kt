package com.salesc2.data.network.api.response

data class ResponseBillingSubscription(
    val `data`: Data,
    val msg: String,
    val status: Int
) {
    data class Data(
        val _id: String,
        val billingCycle: Int,
        val endDate: String,
        val estimatedAmt: Double,
        val plan: String,
        val planCharge: Double,
        val startDate: String,
        val taxAmount: Int,
        val taxPercent: Int,
        val usersCount: Int,
        val zipCode: String
    )
}
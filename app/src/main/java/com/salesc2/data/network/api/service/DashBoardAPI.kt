package com.salesc2.data.network.api.service

import com.salesc2.data.network.api.request.RequestDashBoard
import com.salesc2.data.network.api.response.ResponseDashBoard
import com.salesc2.data.network.api.response.ResponseTeamList
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface DashBoardAPI {
    @POST("ios-task/api/v1/dashboard")
    suspend fun reqDashBoardData(
@Body requestDashBoard: RequestDashBoard
    ): Response<ResponseDashBoard>
}
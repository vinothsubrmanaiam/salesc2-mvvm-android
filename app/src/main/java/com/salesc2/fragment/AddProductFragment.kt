package com.salesc2.fragment

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.DigitsKeyListener
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.MainActivity
import com.salesc2.R
import com.salesc2.database.AppDatabase
import com.salesc2.model.AddProductsModel
import com.salesc2.model.ProductsModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.DatabaseHandler
import com.salesc2.service.WebService
import com.salesc2.utils.isNetworkAvailable
import com.salesc2.utils.loadFragment
import com.salesc2.utils.progressBarDismiss
import com.salesc2.utils.toast
import androidx.lifecycle.lifecycleScope
import com.salesc2.database.ProductList
import kotlinx.coroutines.*
import org.json.JSONObject


class AddProductFragment : Fragment() {

    private var db: AppDatabase? = null
    var webService = WebService()
    var apiRequest = ApiRequest()
    lateinit var product_name: AutoCompleteTextView
    lateinit var product_price: EditText
    lateinit var q_plus: ImageView
    lateinit var q_minus: ImageView
    lateinit var q_count: TextView
    private lateinit var close: ImageView
    var searchText = String()
    lateinit var add_product: Button
    lateinit var add_product_back_arrow: ImageView


    lateinit var mView: View
    private var count = 1
    var prodNameList = ArrayList<String>()
    private var prodIdList = ArrayList<String>()
    var productsModel = ArrayList<ProductsModel>()
    private var prodId: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_add_product, container, false)
        product_name = mView.findViewById(R.id.product_name_et)
        product_price = mView.findViewById(R.id.product_price_et)
        q_plus = mView.findViewById(R.id.q_plus)
        q_minus = mView.findViewById(R.id.q_minus)
        q_count = mView.findViewById(R.id.q_count)
        close = mView.findViewById(R.id.close_iv)
        add_product_back_arrow = mView.findViewById(R.id.add_product_back_arrow)
        add_product = mView.findViewById(R.id.add_product)
        prodNameList = ArrayList()
        prodIdList = ArrayList()
        productsModel = ArrayList()


        if ( requireContext().isNetworkAvailable(requireContext()))
        {
            lifecycleScope.launch {
                delay(100)
                ProductListAsyncTask().execute()
            }
        }
        else
        {
            showLocalResponse()
        }

        productQuantity()

        //println("product_dsk"+" "+"agjhavhvahgcvhgc")
        closeAction()
        addProductsAction()
        searchProductName()
        viewRecord()

        product_price.setKeyListener(DigitsKeyListener.getInstance(false, true))
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility =
            View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
//        activity?.findViewById<BottomNav
//        igationView>(R.id.navigationView)?.visibility = View.GONE
        return mView
    }

    /*  private fun spinnerAction(){
          spinner?.isSelected = false;  // must
          spinner?.setSelection(0,true);  //must

          var productNameList : ArrayList<String> = ArrayList()
          productNameList.add(prodNames)
          spinner?.adapter = object : ArrayAdapter<String>(context!!,R.layout.support_simple_spinner_dropdown_item,productNameList)
                 spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
              override fun onNothingSelected(parent: AdapterView<*>?) {

              }

              override fun onItemSelected(
                  parent: AdapterView<*>?,
                  view: View?,
                  position: Int,
                  id: Long
              ) {

              }

                  }
      }*/


    private fun addProductsAction() {
        add_product.setOnClickListener {

            if (!prodId.equals("")) {
                if (productNameArray.contains(product_name.text.toString()) || productIdArray.contains(
                        prodId
                    )
                ) {
                    context?.toast("Product already exist")
                } else {


                    if (product_name.text.toString() == "") {
                        val toast =
                            Toast.makeText(context, "Please enter product name", Toast.LENGTH_SHORT)
//                var toast : Toast = Toast.makeText(context,"Product saved",Toast.LENGTH_LONG)
                        toast.setGravity(Gravity.CENTER, 0, 0);
                        toast.show()
//                val view = toast.view.findViewById<TextView>(android.R.id.message)
//                view?.let {
//                    view.gravity = Gravity.CENTER
//                }
                        toast.show()
//                context?.toast("Please enter product name")
                    } else if (product_price.text.toString() == "") {
                        context?.toast("Please enter product price")
                    } else {

                        val n2Var: Double = product_price!!.getText().toString().toDouble()

                        if (n2Var < 1) {
                            context?.toast("Please enter valid product price")
                        } else {
                            saveRecord()
                        }


                    }
                }
            } else {
                context?.toast("Kindly select the valid product")
            }


        }
    }

    //method for saving records in database
    private fun saveRecord() {
        val n2Var: Double = product_price!!.getText().toString().toDouble()
        val roundOff = Math.round(n2Var.toDouble() * 100.0) / 100.0

        val pId = prodId
        val pName = product_name.text.toString()
        val pPrice = roundOff
        val pQty = q_count.text.toString()

        val databaseHandler: DatabaseHandler = DatabaseHandler(context!!)
        if (pName.trim() != "" && pPrice.toString().trim() != "") {
            val status =
                databaseHandler.addProducts(AddProductsModel(pId, pName, pPrice.toString(), pQty))
            if (status > -1) {
                product_name.text.clear()
                product_price.text.clear()
                var toast: Toast = Toast.makeText(context, "Product saved", Toast.LENGTH_LONG)
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show()

                var bundle = Bundle()
                bundle.putString("prod_name", product_name.text.toString())
                bundle.putString("prod_price", roundOff.toString())
                bundle.putString("prod_qty", q_count.text.toString())
                var fragment = AddTaskFragment()
                fragment.arguments = bundle
                val transaction: FragmentTransaction? =
                    activity?.supportFragmentManager?.beginTransaction()
                transaction?.replace(R.id.main_fragmet_container, fragment)
                transaction?.addToBackStack(null)
                transaction?.commit()
            }
        } else {
            Toast.makeText(
                context,
                "Id or name or price or quantity cannot be blank",
                Toast.LENGTH_LONG
            ).show()
        }
    }


    var productNameArray = ArrayList<String>()
    var productIdArray = ArrayList<String>()

    private fun viewRecord() {
        //creating the instance of DatabaseHandler class
        val databaseHandler: DatabaseHandler = DatabaseHandler(context!!)
        //calling the viewEmployee method of DatabaseHandler class to read the records
        val prod: List<AddProductsModel> = databaseHandler.viewProducts()
        val prodArrayId = Array<String>(prod.size) { "" }
        val prodArrayName = Array<String>(prod.size) { "null" }
        val prodArrayPrice = Array<String>(prod.size) { "null" }
        val prodArrayQty = Array<String>(prod.size) { "null" }
        var index = 0

        for (e in prod) {
            prodArrayId[index] = e.prod_id
            prodArrayName[index] = e.prod_name
            prodArrayPrice[index] = e.prod_price
            prodArrayQty[index] = e.prod_qty
            index++
            productNameArray.add("${e.prod_name}")
            productIdArray.add("${e.prod_id}")
        }

    }

    private fun closeAction() {
        close.setOnClickListener {
            startActivity(Intent(context, MainActivity::class.java))
        }
        add_product_back_arrow.setOnClickListener {
//            fragmentManager?.popBackStack()
            context?.loadFragment(context!!, AddTaskFragment())
        }
    }


    private fun searchProductName() {

        var arrayAdapter = ArrayAdapter<String>(
            context!!,
            R.layout.support_simple_spinner_dropdown_item,
            prodNameList
        )
        product_name.setAdapter(arrayAdapter)
        product_name.threshold = 1
        product_name.setOnClickListener {
            product_name.showDropDown()

        }
        product_name.onFocusChangeListener = View.OnFocusChangeListener { view, b ->
            if (b) {
                product_name.showDropDown()
            }
        }
        product_name.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val selectedItem = parent?.getItemAtPosition(position).toString()


                val selectedItemId = parent?.getItemIdAtPosition(position)
                prodId = prodIdList.get(selectedItemId!!.toInt())

                if (prodId == "") {
                    Toast.makeText(context, "Closed.", Toast.LENGTH_SHORT).show()
                    prodId = ""
                } else {
                    product_name.setText(selectedItem)
                }

            }


        }
        product_name.setOnDismissListener {
//        Toast.makeText(context,"Closed.",Toast.LENGTH_SHORT).show()
        }
        product_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.length == 1) {
                    prodId = ""
                }
            }
        })

    }

    private fun productQuantity() {
        q_plus.setOnClickListener {
            count++
            q_count.text = count.toString()
        }
        q_minus.setOnClickListener {
            if (q_count.text.toString().toInt() > 1) {
                count--
            }
            q_count.text = count.toString()
        }
    }



    inner class ProductListAsyncTask : AsyncTask<String, String, String>() {
        override fun doInBackground(vararg params: String?): String {
            /*prodIdList = ArrayList()
            prodNameList = ArrayList()*/
            var param = HashMap<String, String>()
            param.put("search", searchText)
            context?.let {
                apiRequest.postRequestBodyWithHeaders(
                    it,
                    webService.Add_products_url,
                    param
                ) { response ->
                    try {
                        var status = response.getString("status")
                        println("ahbbasb"+"hbs")
                        if (status == "200") {
                            var dataArray = response.getJSONArray("data")
                            Log.i("dataArrayOfProds",dataArray.toString())
                            for (i in 0 until dataArray.length()) {
                                var dataObj = dataArray.getJSONObject(i)
                                var id = dataObj.getString("_id")
                                prodIdList.add(id)

                                var name = dataObj.getString("name")
                                prodNameList.add(name)

                                context?.progressBarDismiss(context!!)
                            }
                        } else {
                            context?.progressBarDismiss(context!!)
                        }
                    } catch (e: Exception) {

                    }
                }
            }
            return ""
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

    private fun showLocalResponse(){
        lifecycleScope.launch {
            delay(500)
            CoroutineScope(Dispatchers.Main).launch {
                db = AppDatabase.getDatabaseClient(requireContext())
                val productData = db!!.productListDao().getAll()
                for (p in 0 until productData.size){
                    var prod_id = productData[p].prod_id
                    var prod_name = productData[p].prod_name
                    prodIdList.add(prod_id.toString())
                    prodNameList.add(prod_name.toString())
                }
                Log.i("productDataDataString", productData.toString())
            }
        }
    }

}
package com.salesc2

import android.app.Application
import com.salesc2.data.network.di.NETWORKING_MODULE
import com.salesc2.data.network.di.REPOSITORY_MODULE
import com.salesc2.data.network.di.VIEW_MODEL_MODULE

import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class ServiceApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@ServiceApplication)
            loadKoinModules(REQUIRED_MODULE)
        }
    }

    companion object{
        val REQUIRED_MODULE = listOf(
            NETWORKING_MODULE,
            REPOSITORY_MODULE,
            VIEW_MODEL_MODULE
        )
    }
}
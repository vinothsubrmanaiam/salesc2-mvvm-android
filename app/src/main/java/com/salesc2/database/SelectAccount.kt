package com.salesc2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.json.JSONArray
import org.json.JSONObject





@Entity
data class SelectAccount(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "_id") val _id: String?,
    @ColumnInfo(name = "name") val name: String?
    )

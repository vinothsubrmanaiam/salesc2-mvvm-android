package com.salesc2.utils

import android.content.Context
import android.widget.ImageView

interface ShowOption {
    fun showOption(selectCard1: Context, selectCard: ImageView)

}
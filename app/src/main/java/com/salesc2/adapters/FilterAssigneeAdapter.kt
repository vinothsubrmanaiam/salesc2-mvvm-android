package com.salesc2.adapters

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.fragment.FilterTaskListFragment
import com.salesc2.model.FilterAssigneeModel
import com.salesc2.model.SelectAccountModel
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.setSharedPref
import java.util.ArrayList

class FilterAssigneeAdapter(): RecyclerView.Adapter<FilterAssigneeAdapter.MyViewHolder>() {

    private lateinit var filterAssigneeItems: ArrayList<FilterAssigneeModel>
    var context: Context? = null

    var filter_assignee_position = String()

    constructor(context: Context?, filterAssigneeItems: ArrayList<FilterAssigneeModel>) : this(){
        this.context = context
        this.filterAssigneeItems = filterAssigneeItems

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_select_list, parent, false)
        filter_assignee_position = context?.getSharedPref("filter_assignee_position",context!!).toString()
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return filterAssigneeItems.size
    }

    var row_index = -1
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var filterAssigneeModel : FilterAssigneeModel = filterAssigneeItems[position]
        holder.filte_name.text = filterAssigneeModel.assignee_name
        filter_assignee_position = context?.getSharedPref("filter_assignee_position",context!!).toString()
            if (filter_assignee_position != "null"){
            row_index=filter_assignee_position.toInt()
                if (row_index.equals(filter_assignee_position)){
                    context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                        holder.filte_name.setTextColor(it)
                    }
                    context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
                        holder.filte_name.setBackgroundDrawable(it)
                    }
                }
        }

        holder.list_layout.setOnClickListener(View.OnClickListener {
            context?.setSharedPref(context!!,"filter_assignee_id",filterAssigneeModel.assignee_id)
            context?.setSharedPref(context!!,"filter_assignee_name",filterAssigneeModel.assignee_name)

            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("filter_assignee_position")
            editor.commit()

            row_index=position
            notifyDataSetChanged()
        })
        if(row_index==position){
            context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                holder.filte_name.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
                holder.filte_name.setBackgroundDrawable(it)
            }
            context?.setSharedPref(context!!,"filter_assignee_position",position.toString())

        }
        else
        {
            context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
                holder.filte_name.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
                holder.filte_name.setBackgroundColor(it)
            }

        }
    }
    class MyViewHolder constructor(itemView:View):RecyclerView.ViewHolder(itemView){
        var filte_name : TextView = itemView.findViewById(R.id.select_list_name)
        val list_layout : ConstraintLayout = itemView.findViewById(R.id.list_layout)
    }
}
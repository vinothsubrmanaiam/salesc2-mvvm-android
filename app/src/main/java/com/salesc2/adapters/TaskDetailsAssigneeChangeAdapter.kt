package com.salesc2.adapters

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.fragment.TaskDetailsAssigneeChangeFragment
import com.salesc2.fragment.TaskDetailsFragment
import com.salesc2.model.AddTaskAssigneeModel
import com.salesc2.model.ChangeTaskAssigneeModel
import com.salesc2.model.FilterAssigneeModel
import com.salesc2.utils.getSharedPref
import com.salesc2.utils.setSharedPref
import com.squareup.picasso.Picasso
import java.util.ArrayList

class TaskDetailsAssigneeChangeAdapter(): RecyclerView.Adapter<TaskDetailsAssigneeChangeAdapter.MyViewHolder>() {

    private lateinit var filterAssigneeItems: ArrayList<ChangeTaskAssigneeModel>
    var context: Context? = null
    private var task_details_assignee_position = String()

    constructor(context: Context?, filterAssigneeItems: ArrayList<ChangeTaskAssigneeModel>) : this(){
        this.context = context
        this.filterAssigneeItems = filterAssigneeItems

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_add_task_assignee, parent, false)
        task_details_assignee_position = context?.getSharedPref("task_details_assignee_position",context!!).toString()
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return filterAssigneeItems.size
    }

    var row_index = -1
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var filterAssigneeModel : ChangeTaskAssigneeModel = filterAssigneeItems[position]
        task_details_assignee_position = context?.getSharedPref("task_details_assignee_position",context!!).toString()
        holder.taskAssigneeName.text = filterAssigneeModel.assignee_name
        if (!filterAssigneeModel.assignee_image.equals("")){
            Picasso.get().load(filterAssigneeModel.assignee_image)
                .placeholder(R.drawable.ic_dummy_user_pic).into(holder.taskAssigneeImage)
        }
        else{
            holder.taskAssigneeImage.setBackgroundResource(R.drawable.ic_dummy_user_pic)
        }
        if (task_details_assignee_position != "null"){
            row_index=task_details_assignee_position.toInt()
            if (row_index.equals(task_details_assignee_position)){
                holder.select_icon.visibility= View.VISIBLE
            }
        }

        holder.itemView.setOnClickListener(View.OnClickListener {

            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("task_details_assignee_position")
            editor.commit()
            context?.setSharedPref(context!!,"update_api_call","yes")
            row_index=position
            notifyDataSetChanged()
            var bundle = Bundle()
            bundle.putString("task_assignee_name",filterAssigneeModel.assignee_name)
            bundle.putString("task_assignto_id_change",filterAssigneeModel.assignee_id)
            context?.setSharedPref(context!!,"task_assignto_id_change",filterAssigneeModel.assignee_id)
            var taskid = context?.getSharedPref("task_detail_id",context!!)
            context?.setSharedPref(context!!,"td_assignee_name",filterAssigneeModel.assignee_name)
            bundle.putString("task_id",taskid)
            var fragment = TaskDetailsFragment()
            fragment.arguments = bundle
            val activity = context as AppCompatActivity
            val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
            transaction.replace(R.id.main_fragmet_container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()


        })
        if(row_index==position){
            holder.select_icon.visibility= View.VISIBLE
            context?.setSharedPref(context!!,"task_details_assignee_position",position.toString())

        }
        else
        {
            holder.select_icon.visibility= View.GONE
            context?.let { ContextCompat.getColor(it, R.color.filter_left_menu_bg) }?.let {
                holder.taskAssigneeName.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it, R.color.light_grey) }?.let {
                holder.taskAssigneeName.setBackgroundColor(it)
            }

        }
    }
    class MyViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView){
        var taskAssigneeName : TextView = itemView.findViewById(R.id.task_assignee_name)
        var taskAssigneeImage : ImageView = itemView.findViewById(R.id.task_assignee_image)
        var assignee_layout : ConstraintLayout = itemView.findViewById(R.id.assignee_layout)
        var select_icon : ImageView = itemView.findViewById(R.id.select_icon)
    }
}
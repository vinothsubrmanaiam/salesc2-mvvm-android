package com.salesc2.fragment

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.SearchView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.R
import com.salesc2.adapters.*
import com.salesc2.model.*
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class FilterTaskListFragment : Fragment() {

    lateinit var filterListRecyclerView : RecyclerView
    var filterListModel = ArrayList<FilterListModel>()



    lateinit var mView: View
    private lateinit var viewPagerFilter :ViewPager
    private lateinit var tabLayoutFilter : TabLayout
    lateinit var filter_frameLayout : FrameLayout
    lateinit var filter_close : ImageView
    lateinit var cancle_filter : TextView
    lateinit var apply_filter : TextView

    var webService = WebService()

    private var filterAccountID = String()
    private var filterAssigneeID = String()
    private var filterStatusName = String()
    private var filterDateRangeStart = String()
    private var filterDateRangeEnd = String()
    private var filterTerritoryID = String()

    private lateinit var status_filter : TextView
    private lateinit var assignee_filter : TextView
    private lateinit var date_range_filter : TextView
    private lateinit var account_filter : TextView
    private lateinit var territory_filter : TextView

    private lateinit var filterRecyclerView : RecyclerView
    private lateinit var filterSearchView : SearchView


   private var filterStatusModel = ArrayList<FilterStatusModel>()
    private var filterAssigneeModel= ArrayList<FilterAssigneeModel>()
   private var selectAccountModel = ArrayList<SelectAccountModel>()
    private var filterTerritoryModel = ArrayList<FilterTerritoryModel>()

    var apiRequest = ApiRequest()
    private lateinit var dateRangeLayout : ConstraintLayout
    private lateinit var from_date :TextView
    private lateinit var to_date : TextView

    private var filterName = String()
    @SuppressLint("ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_task_list_filter,container,false)
        filterAccountID = context?.getSharedPref("filter_account_id", context!!).toString()
        filterAssigneeID = context?.getSharedPref("filter_assignee_id", context!!).toString()
        filterStatusName =  context?.getSharedPref("filter_status_name",context!!).toString()
        filterDateRangeStart = context?.getSharedPref("filter_start_dt",context!!).toString()
        filterDateRangeEnd = context?.getSharedPref("filter_end_dt",context!!).toString()
        filterTerritoryID = context?.getSharedPref("filter_territory_id",context!!).toString()
        status_filter = mView.findViewById(R.id.status_filter)
        assignee_filter = mView.findViewById(R.id.assignee_filter)
        date_range_filter = mView.findViewById(R.id.date_range_filter)
        account_filter = mView.findViewById(R.id.account_filter)
        territory_filter = mView.findViewById(R.id.territory_filter)



        filterRecyclerView = mView.findViewById(R.id.filterRecyclerView)
        filterSearchView = mView.findViewById(R.id.filterSearchView)

        filterListRecyclerView = mView.findViewById(R.id.filterList_recyclerView)
        filterListModel = ArrayList()

        dateRangeLayout = mView.findViewById(R.id.dateRangeLayout)
        from_date = mView.findViewById(R.id.from_date_et)
        to_date = mView.findViewById(R.id.to_date_et)

        viewPagerFilter = mView.findViewById(R.id.filter_viewpager)
        tabLayoutFilter = mView.findViewById(R.id.filter_tabLayout)
        filter_frameLayout = mView.findViewById(R.id.filter_frameLayout)
        filter_close = mView.findViewById(R.id.filter_close_iv)
        cancle_filter = mView.findViewById(R.id.cancle_filter)
        apply_filter = mView.findViewById(R.id.apply_filter)


        filterStatusModel = ArrayList()
        filterAssigneeModel = ArrayList()
        selectAccountModel = ArrayList()
        filterTerritoryModel = ArrayList()

        var listFilter: Array<String> = arrayOf<String>("Status","Assignee","Date range","Account","Territory")
        for (i in 0 until listFilter.size) {
            filterListModel.add(FilterListModel(listFilter[i]))
        }

        bgStatus()
        setupFilterListRecyclerView(filterListModel)
        closeAction()
        filterClickAction()
        searchViewAction()
        filterDrawable()
        filterSearchView.visibility = View.GONE
        dateRangeLayout.visibility = View.GONE
        filterStatusModel = ArrayList()
        var statusNameList = arrayOf("In progress","Uncovered","Cancelled","Completed","Scheduled","Pending","Unassigned")
        for (i in 0 until statusNameList.size){
            filterStatusModel.add(FilterStatusModel(statusNameList[i]))
        }
        setupFilterStatusRecyclerView(filterStatusModel)

        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    private fun filterDrawable(){
        if (filterStatusName != "null"){
            status_filter.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_dot_icon,0)
        }
        if (filterAssigneeID != "null"){
            assignee_filter.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_dot_icon,0)
        }
        if (filterDateRangeEnd != "null" && filterDateRangeStart != "null"){
            date_range_filter.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_dot_icon,0)
        }
        if (filterAccountID != "null"){
            account_filter.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_dot_icon,0)
        }
        if (filterTerritoryID != "null"){
            territory_filter.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_dot_icon,0)
        }
    }
    var searchtext = String()
    private fun searchViewAction(){
        filterSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                searchtext = newText
                if (filterName == "assignee_filter"){
                    filterAssigneeModel = ArrayList()
                    filterAssigneeApiCall()
                }else if (filterName == "account_filter"){
                    selectAccountModel = ArrayList()
                    filterAccountApiCall()
                }else if (filterName == "territory_filter"){
                    filterTerritoryModel = ArrayList()
                    filterTerritoryApiCall()
                }
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                searchtext = query
                if (filterName == "assignee_filter"){
                    filterAssigneeModel = ArrayList()
                    filterAssigneeApiCall()
                }else if (filterName == "account_filter"){
                    selectAccountModel = ArrayList()
                    filterAccountApiCall()
                }else if (filterName == "territory_filter"){
                    filterTerritoryModel = ArrayList()
                    filterTerritoryApiCall()
                }


                return true
            }

            override fun equals(other: Any?): Boolean {
                return super.equals(other)

            }


        })
    }

    private fun filterClickAction(){
        status_filter.setOnClickListener{
            bgStatus()
            filterSearchView.visibility = View.GONE
            dateRangeLayout.visibility = View.GONE
            filterRecyclerView.visibility = View.VISIBLE
            filterStatusModel = ArrayList()
            var statusNameList = arrayOf("In progress","Uncovered","Cancelled","Completed","Scheduled","Pending","Unassigned")
            for (i in 0 until statusNameList.size){
                filterStatusModel.add(FilterStatusModel(statusNameList[i]))
            }
            setupFilterStatusRecyclerView(filterStatusModel)


            filterAccountID = context?.getSharedPref("filter_account_id", context!!).toString()
            filterAssigneeID = context?.getSharedPref("filter_assignee_id", context!!).toString()
            filterStatusName =  context?.getSharedPref("filter_status_name",context!!).toString()
            filterDateRangeStart = context?.getSharedPref("filter_start_dt",context!!).toString()
            filterDateRangeEnd = context?.getSharedPref("filter_end_dt",context!!).toString()
            filterTerritoryID = context?.getSharedPref("filter_territory_id",context!!).toString()
            filterDrawable()
        }
        assignee_filter.setOnClickListener{
            context?.setSharedPref(context!!,"filter","assignee_filter")
            bgAssigneeFilter()
            filterStatusModel.clear()
            filterAssigneeModel.clear()
            filterTerritoryModel.clear()
            selectAccountModel.clear()
            filterAssigneeModel = ArrayList()
            filterSearchView.visibility = View.VISIBLE
            dateRangeLayout.visibility = View.GONE
            filterRecyclerView.visibility = View.VISIBLE
            filterAssigneeApiCall()
            filterName = context?.getSharedPref("filter",context!!).toString()


            filterAccountID = context?.getSharedPref("filter_account_id", context!!).toString()
            filterAssigneeID = context?.getSharedPref("filter_assignee_id", context!!).toString()
            filterStatusName =  context?.getSharedPref("filter_status_name",context!!).toString()
            filterDateRangeStart = context?.getSharedPref("filter_start_dt",context!!).toString()
            filterDateRangeEnd = context?.getSharedPref("filter_end_dt",context!!).toString()
            filterTerritoryID = context?.getSharedPref("filter_territory_id",context!!).toString()
            filterDrawable()
        }
        date_range_filter.setOnClickListener{
            bgDateRangeFilter()
            filterSearchView.visibility = View.GONE
            dateRangeLayout.visibility = View.VISIBLE
            filterRecyclerView.visibility = View.GONE
            dateAction()
            filterAccountID = context?.getSharedPref("filter_account_id", context!!).toString()
            filterAssigneeID = context?.getSharedPref("filter_assignee_id", context!!).toString()
            filterStatusName =  context?.getSharedPref("filter_status_name",context!!).toString()
            filterDateRangeStart = context?.getSharedPref("filter_start_dt",context!!).toString()
            filterDateRangeEnd = context?.getSharedPref("filter_end_dt",context!!).toString()
            filterTerritoryID = context?.getSharedPref("filter_territory_id",context!!).toString()
            filterDrawable()
        }
        account_filter.setOnClickListener{
            context?.setSharedPref(context!!,"filter","account_filter")
            bgAccountFilter()
            filterStatusModel.clear()
            filterAssigneeModel.clear()
            filterTerritoryModel.clear()
            selectAccountModel.clear()
            selectAccountModel = ArrayList()
            filterSearchView.visibility = View.VISIBLE
            dateRangeLayout.visibility = View.GONE
            filterRecyclerView.visibility = View.VISIBLE
            filterAccountApiCall()
            filterName = context?.getSharedPref("filter",context!!).toString()

            filterAccountID = context?.getSharedPref("filter_account_id", context!!).toString()
            filterAssigneeID = context?.getSharedPref("filter_assignee_id", context!!).toString()
            filterStatusName =  context?.getSharedPref("filter_status_name",context!!).toString()
            filterDateRangeStart = context?.getSharedPref("filter_start_dt",context!!).toString()
            filterDateRangeEnd = context?.getSharedPref("filter_end_dt",context!!).toString()
            filterTerritoryID = context?.getSharedPref("filter_territory_id",context!!).toString()
            filterDrawable()
        }
        territory_filter.setOnClickListener{
            context?.setSharedPref(context!!,"filter","territory_filter")
            bgTerritoryFilter()
            filterStatusModel.clear()
            filterAssigneeModel.clear()
            filterTerritoryModel.clear()
            selectAccountModel.clear()
            filterTerritoryModel = ArrayList()
            filterSearchView.visibility = View.VISIBLE
            dateRangeLayout.visibility = View.GONE
            filterRecyclerView.visibility = View.VISIBLE
            filterTerritoryApiCall()
            filterName = context?.getSharedPref("filter",context!!).toString()

            filterAccountID = context?.getSharedPref("filter_account_id", context!!).toString()
            filterAssigneeID = context?.getSharedPref("filter_assignee_id", context!!).toString()
            filterStatusName =  context?.getSharedPref("filter_status_name",context!!).toString()
            filterDateRangeStart = context?.getSharedPref("filter_start_dt",context!!).toString()
            filterDateRangeEnd = context?.getSharedPref("filter_end_dt",context!!).toString()
            filterTerritoryID = context?.getSharedPref("filter_territory_id",context!!).toString()
            filterDrawable()
        }
    }
    private fun setupFilterStatusRecyclerView(filterStatusModel: ArrayList<FilterStatusModel>) {
        val adapter: RecyclerView.Adapter<*> = FilterStatusAdapter(context, filterStatusModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        filterRecyclerView.layoutManager = llm
        filterRecyclerView.adapter = adapter
    }


    private fun filterAssigneeApiCall(){
        var param = HashMap<String,String>()
//        param.put("accountId",accId)
//        param.put("StartDate","")
//        param.put("toDate","")
        param.put("search",searchtext)
        context?.let {
            apiRequest.postRequestBodyWithHeaders(it,webService.Add_task_assignee_url,param){ response->
                filterAssigneeModel.clear()
                var status = response.getString("status")
                if (status == "200"){
                    var dataArray = response.getJSONArray("data")
                    for (i in 0 until dataArray.length()){
                        var dataObj = dataArray.getJSONObject(i)
                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")
                        filterAssigneeModel.add(FilterAssigneeModel(_id,name))
                        context?.progressBarDismiss(context!!)
                    }
                    setupFilterAssigneeRecyclerView(filterAssigneeModel)
                }else{
                    context?.progressBarDismiss(context!!)
                }

            }
        }

    }
    //    setup adapter
    private fun setupFilterAssigneeRecyclerView(filterAssigneeModel: ArrayList<FilterAssigneeModel>) {
        val adapter: RecyclerView.Adapter<*> = FilterAssigneeAdapter(context, filterAssigneeModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        filterRecyclerView.layoutManager = llm
        filterRecyclerView.adapter = adapter
    }


    val myCalendar = Calendar.getInstance()
    val myCalendarEnd = Calendar.getInstance()

    private fun dateAction(){
        filterDateRangeStart = context?.getSharedPref("filter_start_dt",context!!).toString()
        filterDateRangeEnd = context?.getSharedPref("filter_end_dt",context!!).toString()

        if (filterDateRangeStart != "null"){
            var parser = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
            var formatDate = SimpleDateFormat("MM/dd/yyyy")
            var dateSt = formatDate.format(parser.parse(filterDateRangeStart))
            from_date.setText(dateSt)
        }
        if (filterDateRangeEnd != "null"){
            var parser = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
            var formatDate = SimpleDateFormat("MM/dd/yyyy")
            var dateEd = formatDate.format(parser.parse(filterDateRangeEnd))
            to_date.setText(dateEd)
        }
        from_date.setOnClickListener(View.OnClickListener {
            var date =
                DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
                    myCalendar[Calendar.YEAR] = year
                    myCalendar[Calendar.MONTH] = monthOfYear
                    myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
                    updateLabel()
                }
            if (from_date.text.toString() != ""){
                var edYr = String()
                var edMonth = String()
                var edDay = String()
                try {
                    val parserEnd = SimpleDateFormat("MM/dd/yyyy")
                    var parseEnd = parserEnd.parse(from_date.text.toString())
                    var getYrFormatEnd = SimpleDateFormat("yyyy")
                    var getMonthFormatEnd = SimpleDateFormat("MM")
                    var getDayFormatEnd = SimpleDateFormat("dd")

                    edYr = getYrFormatEnd.format(parseEnd)
                    edMonth = getMonthFormatEnd.format(parseEnd)
                    edDay = getDayFormatEnd.format(parseEnd)

                 var datePickerDialog: DatePickerDialog? = context?.let { it1 ->
                DatePickerDialog(
                    it1,
                    date,
                    edYr.toInt(),
                    edMonth.toInt() - 1,
                    edDay.toInt()
                )
            } //date is dateSetListener as per your code in question
//            datePickerDialog?.datePicker?.maxDate = System.currentTimeMillis()
            datePickerDialog?.updateDate(edYr.toInt(), edMonth.toInt() - 1, edDay.toInt())
                datePickerDialog?.show()
                     } catch (e: Exception) {

                }
            }else{
                //            var datePickerDialog: DatePickerDialog? = context?.let { it1 -> DatePickerDialog(it1, date, Calendar.YEAR, Calendar.MONTH, Calendar.DAY_OF_MONTH) } //date is dateSetListener as per your code in question
                var datePickerDialog: DatePickerDialog? = context?.let { it1 -> DatePickerDialog(it1, date,myCalendar.get(
                    Calendar.YEAR
                ), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)) } //date is dateSetListener as per your code in question
                datePickerDialog?.show()
            }


        })

        to_date.setOnClickListener(View.OnClickListener {
            if (from_date.text == ""){
                context?.toast("Please select start date")
            }else{
                var dateEnd =
                    DatePickerDialog.OnDateSetListener { dpd, year, monthOfYear, dayOfMonth ->
                        val sdf = SimpleDateFormat("MM/dd/yyyy HH:mm:ss")
                        val newDate = sdf.parse(from_date.text.toString()+" 23:59:59")
                        var newdate = sdf.format(newDate)
                        dpd.setMinDate(newdate.toDateNew().getTime())
                        myCalendarEnd[Calendar.YEAR] = year
                        myCalendarEnd[Calendar.MONTH] = monthOfYear
                        myCalendarEnd[Calendar.DAY_OF_MONTH] = dayOfMonth
                        updateLabelToDate()
                    }




                if (to_date.text.toString() != ""){
                    var edYr = String()
                    var edMonth = String()
                    var edDay = String()
                    try {
                        val parserEnd = SimpleDateFormat("MM/dd/yyyy")
                        var parseEnd = parserEnd.parse(to_date.text.toString())
                        var getYrFormatEnd = SimpleDateFormat("yyyy")
                        var getMonthFormatEnd = SimpleDateFormat("MM")
                        var getDayFormatEnd = SimpleDateFormat("dd")

                        edYr = getYrFormatEnd.format(parseEnd)
                        edMonth = getMonthFormatEnd.format(parseEnd)
                        edDay = getDayFormatEnd.format(parseEnd)

                    var datePickerDialogEnd: DatePickerDialog? = context?.let { it1 ->
                        DatePickerDialog(
                            it1,
                            dateEnd,
                            edYr.toInt(),
                            edMonth.toInt() - 1,
                            edDay.toInt()
                        )
                    } //date is dateSetListener as per your code in question
//            datePickerDialog?.datePicker?.maxDate = System.currentTimeMillis()
                    datePickerDialogEnd?.updateDate(edYr.toInt(), edMonth.toInt() - 1, edDay.toInt())
                    datePickerDialogEnd?.show()
                         } catch (e: Exception) {

                    }
                }else{
                    var datePickerDialogEnd: DatePickerDialog? = context?.let { it1 -> DatePickerDialog(it1, dateEnd,myCalendarEnd.get(
                        Calendar.YEAR
                    ), myCalendarEnd.get(Calendar.MONTH), myCalendarEnd.get(Calendar.DAY_OF_MONTH)) } //date is dateSetListener as per your code in question
                    datePickerDialogEnd?.show()
                }


            }

        })
    }


    var myFormat = String()
    var sdf = SimpleDateFormat()
    private fun updateLabel(){
        myFormat = "MM/dd/yyyy" //In which you need put here yyyy-MM-dd
        sdf = SimpleDateFormat(myFormat, Locale.ENGLISH)
        from_date.text = sdf.format(myCalendar.time)


        var stDate =sdf.format(myCalendar.time)+" 00:00:00"

        context?.setSharedPref(context!!,"filter_start_dt",stDate)

    }
    private fun updateLabelToDate(){
        myFormat = "MM/dd/yyyy" //In which you need put here yyyy-MM-dd
        sdf = SimpleDateFormat(myFormat, Locale.ENGLISH)
        to_date.text = sdf.format(myCalendarEnd.time)
        var edDate =sdf.format(myCalendarEnd.time)+" 23:59:59"
        context?.setSharedPref(context!!,"filter_end_dt",edDate)
    }

    private var toDate = Date()
    private var fromDate = Date()
    private fun getCurrentAndMyTime(){
        try {
            var parser = SimpleDateFormat("MM/dd/yyyy")
            val parseDate = parser.parse("${from_date.text.toString()}")
            val myFormat = "MM/dd/yyyy"
            val sdf = SimpleDateFormat(myFormat)
            val stDateFormat = sdf.format(parseDate)
            fromDate = stDateFormat.toDateNew1()
            val parseDateEnd = parser.parse("${to_date.text.toString()}")
            val edDateFormat = sdf.format(parseDateEnd)
            toDate = edDateFormat.toDateNew1()
            compareDateWithEndDate(fromDate,toDate)

        }catch (e:Exception){

        }
    }
    private fun compareDateWithEndDate(startDate: Date, endDate: Date?) {
        if (startDate.after(endDate)) {
            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("filter_start_dt")
            editor.remove("filter_end_dt")
            editor.commit()
            context?.toast("Date Range: End date should be greater than start date ")
        }

        if (startDate.before(endDate)) {
            filterAccountID = context?.getSharedPref("filter_account_id", context!!).toString()
            filterAssigneeID = context?.getSharedPref("filter_assignee_id", context!!).toString()
            filterStatusName =  context?.getSharedPref("filter_status_name",context!!).toString()
            filterDateRangeStart = context?.getSharedPref("filter_start_dt",context!!).toString()
            filterDateRangeEnd = context?.getSharedPref("filter_end_dt",context!!).toString()
            filterTerritoryID = context?.getSharedPref("filter_territory_id",context!!).toString()
            var filterStatus = getStatus(filterStatusName)
            var bundle = Bundle()
            bundle.putString("filter_account_id",filterAccountID)
            bundle.putString("filter_assignee_id",filterAssigneeID)
            bundle.putString("filter_status_name",filterStatus)
            bundle.putString("filter_start_dt",filterDateRangeStart)
            bundle.putString("filter_end_dt",filterDateRangeEnd)
            bundle.putString("filter_territory_id",filterTerritoryID)
            var fragment = TaskListFragment()
            fragment.arguments = bundle
            val activity = context as AppCompatActivity
            val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
            transaction.replace(R.id.main_fragmet_container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()


        }

        //equals() returns true if both the dates are equal
        if (startDate.equals(endDate)) {
            filterAccountID = context?.getSharedPref("filter_account_id", context!!).toString()
            filterAssigneeID = context?.getSharedPref("filter_assignee_id", context!!).toString()
            filterStatusName =  context?.getSharedPref("filter_status_name",context!!).toString()
            filterDateRangeStart = context?.getSharedPref("filter_start_dt",context!!).toString()
            filterDateRangeEnd = context?.getSharedPref("filter_end_dt",context!!).toString()
            filterTerritoryID = context?.getSharedPref("filter_territory_id",context!!).toString()
            var filterStatus = getStatus(filterStatusName)
            var bundle = Bundle()
            bundle.putString("filter_account_id",filterAccountID)
            bundle.putString("filter_assignee_id",filterAssigneeID)
            bundle.putString("filter_status_name",filterStatus)
            bundle.putString("filter_start_dt",filterDateRangeStart)
            bundle.putString("filter_end_dt",filterDateRangeEnd)
            bundle.putString("filter_territory_id",filterTerritoryID)
            var fragment = TaskListFragment()
            fragment.arguments = bundle
            val activity = context as AppCompatActivity
            val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
            transaction.replace(R.id.main_fragmet_container, fragment)
            transaction.addToBackStack(null)
            transaction.commit()

        }
    }
    fun String.toDateNew(dateFormat: String = "dd/MM/yyyy HH:mm:ss", timeZone: TimeZone = TimeZone.getDefault()): Date {
        val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
        parser.timeZone = timeZone
        return parser.parse(this)
    }
    fun String.toDateNew1(dateFormat: String = "dd/MM/yyyy", timeZone: TimeZone = TimeZone.getDefault()): Date {
        val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
        parser.timeZone = timeZone
        return parser.parse(this)
    }




    ////....Account filter
    private fun filterAccountApiCall(){
        var params = java.util.HashMap<String, String>()
        params.put("search",searchtext)
        context?.let {
            apiRequest.postRequestBodyWithHeaders(it,webService.Select_account_list_url,params){ response->
                selectAccountModel.clear()
                var status = response.getString("status")
                if (status == "200"){
                    var dataArray = response.getJSONArray("data")
                    for (i in 0 until dataArray.length()){
                        var dataObj = dataArray.getJSONObject(i)
                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")
                        selectAccountModel.add(SelectAccountModel(_id,name))
                        context?.progressBarDismiss(context!!)
                    }
                    setupFilterAccountRecyclerView(selectAccountModel)
                }else{
                    context?.progressBarDismiss(context!!)
                }
            }
        }
    }
    //    setup adapter
    private fun setupFilterAccountRecyclerView(selectAccountModel: ArrayList< SelectAccountModel>) {
        val adapter: RecyclerView.Adapter<*> = FilterAccountAdapter(context, selectAccountModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        filterRecyclerView.layoutManager = llm
        filterRecyclerView.adapter = adapter
    }


    /////////.... Filter Territory

    private fun filterTerritoryApiCall(){
        filterTerritoryModel = ArrayList()
        var params = java.util.HashMap<String, String>()
        params.put("search",searchtext)
        context?.let {
            apiRequest.postRequestBodyWithHeaders(it,webService.Territory_filter_url,params){ response->
                filterTerritoryModel.clear()
                var status = response.getString("status")
                if (status == "200"){
                    var dataArray = response.getJSONArray("data")
                    for (i in 0 until dataArray.length()){
                        var dataObj = dataArray.getJSONObject(i)
                        var _id = dataObj.getString("_id")
                        var name = dataObj.getString("name")
                        filterTerritoryModel.add(FilterTerritoryModel(_id,name))
                        context?.progressBarDismiss(context!!)
                    }
                    setupFilterTerritoryRecyclerView(filterTerritoryModel)
                }else{
                    context?.progressBarDismiss(context!!)
                }
            }
        }
    }
    //    setup adapter
    private fun setupFilterTerritoryRecyclerView(filterTerritoryModel: ArrayList<FilterTerritoryModel>) {
        val adapter: RecyclerView.Adapter<*> = FilterTerritoryAdapter(context, filterTerritoryModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        filterRecyclerView.layoutManager = llm
        filterRecyclerView.adapter = adapter
    }


    private fun closeAction(){
        filter_close.setOnClickListener{
            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("filter_account_id")
            editor.remove("filter_assignee_id")
            editor.remove("filter_status_name")
            editor.remove("filter_start_dt")
            editor.remove("filter_end_dt")
            editor.remove("filter_territory_id")
            editor.commit()
           context?.loadFragment(context!!,TaskListFragment())
        }
        cancle_filter.setOnClickListener{
            val preferences : SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
            val editor : SharedPreferences.Editor = preferences.edit()
            editor.remove("filter_account_id")
            editor.remove("filter_assignee_id")
            editor.remove("filter_status_name")
            editor.remove("filter_start_dt")
            editor.remove("filter_end_dt")
            editor.remove("filter_territory_id")
            //filter items
            editor.remove("status_position")
            editor.remove("filter_assignee_position")
            editor.remove("filter_account_position")
            editor.remove("filter_territory_position")
            editor.commit()
            context?.loadFragment(context!!,TaskListFragment())
        }
        apply_filter.setOnClickListener{


            filterAccountID = context?.getSharedPref("filter_account_id", context!!).toString()
            filterAssigneeID = context?.getSharedPref("filter_assignee_id", context!!).toString()
            filterStatusName =  context?.getSharedPref("filter_status_name",context!!).toString()
            filterDateRangeStart = context?.getSharedPref("filter_start_dt",context!!).toString()
            filterDateRangeEnd = context?.getSharedPref("filter_end_dt",context!!).toString()
            filterTerritoryID = context?.getSharedPref("filter_territory_id",context!!).toString()
            var filterStatus = getStatus(filterStatusName)
            if(filterDateRangeStart != "null" && filterDateRangeEnd == "null"){
                context?.toast("Please select end date")
                date_range_filter.performClick()
            }else if (from_date.text.toString() != ""){
                getCurrentAndMyTime()
            }else{
                var bundle = Bundle()
                bundle.putString("filter_account_id",filterAccountID)
                bundle.putString("filter_assignee_id",filterAssigneeID)
                bundle.putString("filter_status_name",filterStatus)
                bundle.putString("filter_start_dt",filterDateRangeStart)
                bundle.putString("filter_end_dt",filterDateRangeEnd)
                bundle.putString("filter_territory_id",filterTerritoryID)
                var fragment = TaskListFragment()
                fragment.arguments = bundle
                val activity = context as AppCompatActivity
                val transaction: FragmentTransaction = activity.supportFragmentManager.beginTransaction()
                transaction.replace(R.id.main_fragmet_container, fragment)
                transaction.addToBackStack(null)
                transaction.commit()
            }

        }

    }

    private fun getStatus(Selected: String) : String {
        if (Selected.equals("Inprogress",ignoreCase = true) || Selected.equals("In Progress",ignoreCase = true) || Selected.equals("In progress")) {
            return "inprogress"
        }
        else if (Selected == "Uncovered") {
            return "uncovered"
        }
        else if (Selected == "Cancelled" || Selected == "Canceled"){
            return "cancelled"
        }
        else if (Selected == "Completed") {
            return "completed"
        }
        else if (Selected == "Unassigned") {
            return "Unassigned"
        }
        else if (Selected == "Pending") {
            return "Pending"
        }
        else if (Selected == "Assigned" ||Selected.equals("Scheduled",ignoreCase = true)) {
            return "assigned"
        }
        return ""
    }


    //    setup adapter
    private fun setupFilterListRecyclerView(filterListModel: ArrayList<FilterListModel>) {
        val adapter: RecyclerView.Adapter<*> = FilterListAdapter(context, filterListModel)
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL
        filterListRecyclerView.layoutManager = llm
        filterListRecyclerView.adapter = adapter
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }
    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

    override fun onResume() {
        super.onResume()
        filterAccountID = context?.getSharedPref("filter_account_id", context!!).toString()
        filterAssigneeID = context?.getSharedPref("filter_assignee_id", context!!).toString()
        filterStatusName =  context?.getSharedPref("filter_status_name",context!!).toString()
        filterDateRangeStart = context?.getSharedPref("filter_start_dt",context!!).toString()
        filterDateRangeEnd = context?.getSharedPref("filter_end_dt",context!!).toString()
        filterTerritoryID = context?.getSharedPref("filter_territory_id",context!!).toString()
    }

    private fun bgStatus(){
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            status_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
            status_filter.setBackgroundColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_filter_black_bg) }?.let {
            status_filter.setBackgroundDrawable(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
           assignee_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            assignee_filter.setBackgroundColor(it)
        }


        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            date_range_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            date_range_filter.setBackgroundColor(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            account_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            account_filter.setBackgroundColor(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            territory_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            territory_filter.setBackgroundColor(it)
        }
    }

    private fun bgAssigneeFilter(){
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            assignee_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
            assignee_filter.setBackgroundColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_filter_black_bg) }?.let {
            assignee_filter.setBackgroundDrawable(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            status_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            status_filter.setBackgroundColor(it)
        }


        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            date_range_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            date_range_filter.setBackgroundColor(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            account_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            account_filter.setBackgroundColor(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            territory_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            territory_filter.setBackgroundColor(it)
        }
    }

    private fun bgDateRangeFilter(){
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            date_range_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
            date_range_filter.setBackgroundColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_filter_black_bg) }?.let {
            date_range_filter.setBackgroundDrawable(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            status_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            status_filter.setBackgroundColor(it)
        }


        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            assignee_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            assignee_filter.setBackgroundColor(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            account_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            account_filter.setBackgroundColor(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            territory_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            territory_filter.setBackgroundColor(it)
        }
    }


    private fun bgAccountFilter(){
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            account_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
            account_filter.setBackgroundColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_filter_black_bg) }?.let {
            account_filter.setBackgroundDrawable(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            status_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            status_filter.setBackgroundColor(it)
        }


        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            date_range_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            date_range_filter.setBackgroundColor(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            assignee_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            assignee_filter.setBackgroundColor(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            territory_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            territory_filter.setBackgroundColor(it)
        }
    }

    private fun bgTerritoryFilter(){
        context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
            territory_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.black) }?.let {
            territory_filter.setBackgroundColor(it)
        }
        context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_filter_black_bg) }?.let {
            territory_filter.setBackgroundDrawable(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            status_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            status_filter.setBackgroundColor(it)
        }


        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            date_range_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            date_range_filter.setBackgroundColor(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            account_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            account_filter.setBackgroundColor(it)
        }

        context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
            assignee_filter.setTextColor(it)
        }
        context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
            assignee_filter.setBackgroundColor(it)
        }
    }

}
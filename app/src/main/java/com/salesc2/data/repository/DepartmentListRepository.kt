package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestDashBoard
import com.salesc2.data.network.api.request.RequestDepartmentList
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseDashBoard
import com.salesc2.data.network.api.response.ResponseDepartmentList
import com.salesc2.data.network.api.service.AccountListApi
import com.salesc2.data.network.api.service.DashBoardAPI
import com.salesc2.data.network.api.service.DepartmentListApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface DepartmentListRepository {

    suspend fun reqGetDepartmentList(
      requestDepartmentList: RequestDepartmentList,
    onSuccess: OnSuccess<ResponseDepartmentList>,
    onFailure: OnFailure<ResponseDepartmentList>

    )

    companion object Factory{
        fun create(api : DepartmentListApi) : DepartmentListRepository = DepartmentListRepositoryImpl(api)
    }
}

class DepartmentListRepositoryImpl(var api: DepartmentListApi) : DepartmentListRepository {
    override suspend fun reqGetDepartmentList(
        requestDepartmentList: RequestDepartmentList,
        onSuccess: OnSuccess<ResponseDepartmentList>,
        onFailure: OnFailure<ResponseDepartmentList>
    ) {


        withContext(Dispatchers.IO) {
            try {
                val response = api.reqGetDepartmentList(requestDepartmentList = requestDepartmentList)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status==200) {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }
    }

}

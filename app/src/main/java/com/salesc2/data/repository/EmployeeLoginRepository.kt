package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import com.salesc2.data.network.api.service.UserLoginApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface EmployeeLoginRepository {

    suspend fun reqEmployeeLogin(
        requestUserLogin: RequestUserLogin,
        onSuccess: OnSuccess<ResponseEmployeeLogin>,
        onFailure: OnFailure<ResponseEmployeeLogin>
    )

    companion object Factory {
        fun create(api: UserLoginApi): EmployeeLoginRepository = EmployeeLoginRepositoryImpl(api)
    }
}

private class EmployeeLoginRepositoryImpl(private val api: UserLoginApi) : EmployeeLoginRepository {
    override suspend fun reqEmployeeLogin(
        requestUserLogin: RequestUserLogin,
        onSuccess: OnSuccess<ResponseEmployeeLogin>,
        onFailure: OnFailure<ResponseEmployeeLogin>
    ) {
        withContext(Dispatchers.IO) {
            try {
                val response = api.reqUserLogin(requestUserLogin = requestUserLogin)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status.equals("200")) {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }

    }

}

package com.salesc2.fragment

import android.app.Dialog
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.salesc2.R
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.data.Day
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import java.lang.Exception

class DashboardFragment : Fragment() {

    var webService = WebService()
    var apiRequest = ApiRequest()
    var userWishShow = String()

    private lateinit var mView : View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        webService.currentFrag = "DashboardFragment"
        mView = inflater.inflate(R.layout.fragment_dashboard, container, false)
        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.VISIBLE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.VISIBLE
        activity?.findViewById<ImageView>(R.id.app_icon_imageView)?.visibility = View.GONE
        userWishShow = context?.getSharedPref("user_wish_show",context!!).toString()
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.VISIBLE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.VISIBLE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.VISIBLE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE

        //webService.currentFrag = "Dashboard"
        if(userWishShow == "no"){
            activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        }else{
            activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.VISIBLE
        }

        billingStatusApiCall()
        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }
    private fun billingStatusApiCall(){
        val params = HashMap<String,Any>()
        context?.let {
            apiRequest.getRequestBodyWithHeaders(it,webService.billing_status_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (status == "200"){
                       var billStatus = response.getString("billStatus")
                        if (billStatus == "1"){
                            billingStatusPopUp("You are not subscribed to SalesC2. Please enable subscription to continue accessing the modules.")
                        }else if (billStatus == "2"){
                            billingStatusPopUp("Your payment for subscription is failed. Kindly add another card and retry payment.")
                        }else if (billStatus == "3"){
                            billingStatusPopUp("Payment is under process...")
                        }

                    }
                }catch (e:Exception){
                    println(e)
                }
            }
        }
    }

    var mBottomSheetDialog: Dialog? = null
    private fun billingStatusPopUp(Desc:String){
        mBottomSheetDialog =
            context?.let { it1 -> Dialog(it1, R.style.MaterialDialogSheet) }
        mBottomSheetDialog?.setContentView(R.layout.popup_billing_status)
        var bDesc = mBottomSheetDialog?.findViewById<TextView>(R.id.bDesc)
        var viewBillingPage = mBottomSheetDialog?.findViewById<TextView>(R.id.viewBillingPage)
        var bClose: ImageView? = mBottomSheetDialog?.findViewById(R.id.bClose)

        bClose?.visibility = View.GONE
        bDesc?.text = Desc
        bClose?.setOnClickListener{
            mBottomSheetDialog?.dismiss()
        }
        viewBillingPage?.setOnClickListener{
            configureBillingApiCall()
        }

        activity?.findViewById<RelativeLayout>(R.id.coordinatorlayout)?.visibility = View.GONE
        mBottomSheetDialog?.setCancelable(false)
        mBottomSheetDialog?.window?.setLayout(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT)
        mBottomSheetDialog?.window?.setGravity(Gravity.CENTER)
        mBottomSheetDialog?.show()
    }

    private fun configureBillingApiCall(){
        var params = HashMap<String,Any>()

        context?.let {
            apiRequest.getRequestBodyWithHeaders(it,webService.configure_billing_url,params){ response->
                try {
                    var status = response.getString("status")
                    if (response.has("msg")){
                        var msg = response.getString("msg")
                    }
                    if (status == "200"){
                        mBottomSheetDialog?.dismiss()
                        context?.setSharedPref(context!!,"fromPage","DashBoard")
                        var dataObj = response.getJSONObject("data")
                        var showConfigure = dataObj.getString("showConfigure")
                        context?.setSharedPref(context!!,"showConfigure",showConfigure)
                        if (showConfigure == "true"){
                            context?.loadFragment(context!!,ConfigureBillingFragment())
                        }else{
                            context?.loadFragment(context!!,BillingOverviewFragment())
                        }
                        var subscribeSettingsId = dataObj.getString("subscribeSettingsId")
                        context?.setSharedPref(context!!,"subscriptionId",subscribeSettingsId)
                        var billingId = dataObj.getString("billingId")
                        context?.setSharedPref(context!!,"billingId",billingId)
                        var pricePerUser = dataObj.getString("pricePerUser")
                        context?.setSharedPref(context!!,"unit_amount",pricePerUser)
                    }
                }catch (e:Exception){
                    println(e)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Common.className = "DashboardFragment"

    }

    override fun onDestroy() {
        super.onDestroy()
        Common.className = ""
    }

    override fun onStop() {
        super.onStop()
        Common.className = ""
    }

}
package com.salesc2

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.AsyncTask
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.util.PatternsCompat
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.preferences.SessionManager
import com.salesc2.data.viewmodel.UserLoginViewModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import org.json.JSONObject
import org.koin.androidx.viewmodel.ext.android.viewModel


class LoginActivity : AppCompatActivity() {
    private val userLoginViewModel by viewModel<UserLoginViewModel>()


    var webService = WebService()
    var apiRequest = ApiRequest()
    var token = ""

    lateinit var email: EditText
    lateinit var password: EditText
    lateinit var loginBtn: Button
    lateinit var forgotPassword: TextView
    lateinit var rememberMeCheckbox: CheckBox
    var sharedPreferences: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null

    var passwordInString = String()
    var userEmailInString = String()
    private lateinit var mFusedLocationClient: FusedLocationProviderClient
    private val READ_LOCATION_PERMISSION = 1001
    private val REQUEST_LOCATION_PERMISSION = 1
    private var REQUEST_CODE_LOCATION = 1111
    var PERMISSION_ID = 143
    private var deviceToken = String()

    @SuppressLint("CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        email = findViewById(R.id.editTextTextEmailAddress)
        password = findViewById(R.id.editTextTextPassword)
        loginBtn = findViewById(R.id.button)
        forgotPassword = findViewById(R.id.forgot_password)
        rememberMeCheckbox = findViewById(R.id.checkbox_remember)
        deviceToken = this.getSharedPref("reg_device_token", this)
        mFusedLocationClient = this.let { LocationServices.getFusedLocationProviderClient(it) }!!
        //remember me
        sharedPreferences = getSharedPreferences("LoginPrefs", MODE_PRIVATE)
        editor = sharedPreferences?.edit()
        passwordInString = password.text.toString()
        userEmailInString = email.text.toString()
        val mail = sharedPreferences?.getString("email", "")
        val pass = sharedPreferences?.getString("passowrd", "")
        email.setText(mail)
        password.setText(pass)
        email.requestFocus()
        loginRequest()
        resetPassword()
        getLastLocation()

        SessionManager.saveSession(
            "email",
            "william94thomasrobert+023@gmail.com",
            this@LoginActivity
        )
        SessionManager.saveSession("password", "welcome", this@LoginActivity)

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("FCM", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            token = task.result!!

            // Log and toast


        })

    }


    private fun loginRequest() {
        rememberMeCheckbox.isChecked =
            !(email.text.toString() == "" || email.text.toString() == "null")

        loginBtn.setOnClickListener(View.OnClickListener {
            if (rememberMeCheckbox.isChecked) {
                email.requestFocus()
                editor?.putString("email", email.text.toString())
                editor?.putString("passowrd", password.text.toString())
                editor?.commit()
            } else {
                email.requestFocus()
                editor?.putString("email", "")
                editor?.putString("passowrd", "")
                editor?.commit()
            }
            if (webService.typeservice.equals("Stage")) {
                LoginRequestWithMVVM()
            } else {
                LoginRequestAsyncTask()
            }


        })
        rememberMeCheckbox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                email.requestFocus()
            } else {
                email.requestFocus()
            }
        }


    }


    fun LoginRequestWithMVVM() {
        userLoginViewModel.reqEmployeeLogin(
            RequestUserLogin(
                password = password.text.toString().replace(" ", ""),
                device_token = token,
                email = email.text.toString().replace(" ", "")
            ),
            {
                var token = it.token
                var data = it.data
                var userId = it.data.userId

                var username = it.data.username
                var companyId = it.data.companyId
                var role = it.data.role


                var email = it.data.email
                var profilePath = it.data.profilePath

                var imageWithBase = webService.imageBasePath + profilePath
                setSharedPref(this@LoginActivity, "userId", userId)
                setSharedPref(this@LoginActivity, "token", token)
                        SessionManager.saveSession("token", token, this@LoginActivity)

                if (role.equals("Sales Rep", ignoreCase = true)) {
                    setSharedPref(this@LoginActivity, "role", "Representative")
                } else if (role.equals("Team Lead", ignoreCase = true)) {
                    setSharedPref(this@LoginActivity, "role", "Regional Manager")
                } else {
                    setSharedPref(this@LoginActivity, "role", role)
                }
                setSharedPref(this@LoginActivity, "userId", userId)
                setSharedPref(this@LoginActivity, "email", email)
                setSharedPref(this@LoginActivity, "username", username)
                setSharedPref(this@LoginActivity, "companyId", companyId)
                setSharedPref(this@LoginActivity, "profile_image", imageWithBase)

                var msg = it.msg

                setSharedPref(this@LoginActivity, "LoggedInFirst", "yes")
                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                progressBarDismiss(this@LoginActivity)

            }, {
                var msg = it.msg
                alertDialog(this@LoginActivity, "Alert", msg)

            }

        )
    }

    fun LoginRequestAsyncTask() {
        progressBarShow(this@LoginActivity)
        var emailString = email.text.toString().replace(" ", "")
        var passwordString = password.text.toString().replace(" ", "")
        val params = HashMap<String, String>()
        params.put("email", emailString)
        params.put("password", passwordString)
        params["device_token"] = deviceToken

        Log.i("paramsLogin", params.toString())
        apiRequest.postRequest(
            this@LoginActivity,
            webService.employee_login_url,
            params
        ) { response ->
            Log.i("ResponseLogin", response.toString())
            try {
                var status = response.getString("status")

                if (status == "200") {
                    var token = response.getString("token")

                    var data = response.getString("data")

                    var dataObject = JSONObject(data)
                    var userId = dataObject.getString("userId")
                    var username = dataObject.getString("username")
                    var companyId = dataObject.getString("companyId")
                    var role = dataObject.getString("role")
                    var email = dataObject.getString("email")
                    var profilePath = dataObject.getString("profilePath")
                    var imageWithBase = webService.imageBasePath + profilePath
                    setSharedPref(this@LoginActivity, "userId", userId)
                    setSharedPref(this@LoginActivity, "token", token)
                    if (role.equals("Sales Rep", ignoreCase = true)) {
                        setSharedPref(this@LoginActivity, "role", "Representative")
                    } else if (role.equals("Team Lead", ignoreCase = true)) {
                        setSharedPref(this@LoginActivity, "role", "Regional Manager")
                    } else {
                        setSharedPref(this@LoginActivity, "role", role)
                    }
                    setSharedPref(this@LoginActivity, "userId", userId)
                    setSharedPref(this@LoginActivity, "email", email)
                    setSharedPref(this@LoginActivity, "username", username)
                    setSharedPref(this@LoginActivity, "companyId", companyId)
                    setSharedPref(this@LoginActivity, "profile_image", imageWithBase)

                    var msg = response.getString("msg")

                    setSharedPref(this@LoginActivity, "LoggedInFirst", "yes")
                    startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                    progressBarDismiss(this@LoginActivity)
                } else if (status == "400") {
                    var msg = response.getString("msg")

                    var msgObj = JSONObject(msg)

                    if (email.text.isNullOrEmpty() && password.text.isNullOrEmpty()) {
                        alertDialog(
                            this@LoginActivity,
                            "Alert",
                            "Please enter email and password"
                        )
                    } else if (email.text.isNullOrEmpty()) {
                        var emailMsg = msgObj.getString("email")
                        alertDialog(this@LoginActivity, "Alert", emailMsg)
                    } else if (password.text.isNullOrEmpty()) {
                        var passMsg = msgObj.getString("password")
                        alertDialog(this@LoginActivity, "Alert", passMsg)
                    } else if (!PatternsCompat.EMAIL_ADDRESS.matcher(email.text.toString())
                            .matches()
                    ) {
                        var emailMsg = msgObj.getString("email")
                        alertDialog(this@LoginActivity, "Alert", emailMsg)
                    }
                    progressBarDismiss(this@LoginActivity)
                } else if (status == "401") {
                    var msg = response.getString("msg")

                    alertDialog(this@LoginActivity, "Alert", msg)
                    progressBarDismiss(this@LoginActivity)
                } else if (status == "403") {
                    var msg = response.getString("msg")

                    alertDialog(this@LoginActivity, "Alert", msg)
                    progressBarDismiss(this@LoginActivity)
                } else if (status == "400") {
                    var msg = response.getString("msg")

                    alertDialog(this@LoginActivity, "Alert", msg)
                    progressBarDismiss(this@LoginActivity)
                } else {
                    progressBarDismiss(this@LoginActivity)
                }
            } catch (e: Exception) {
                print(e)
                this@LoginActivity.toast(e.toString())
                progressBarDismiss(this@LoginActivity)
            }
        }

    }


    var dialog: Dialog? = null

    private fun resetPassword() {
        forgotPassword.setOnClickListener(View.OnClickListener {
            dialog = Dialog(this)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog?.setCancelable(false)
            dialog?.setContentView(R.layout.forgot_password_popup)
            dialog?.window!!.setBackgroundDrawableResource(R.drawable.corner_shape_white_bg)
            val title: TextView? = dialog?.findViewById(R.id.forgot_alert_title)
            title?.text = "Forgot Password?"
            val msg: TextView? = dialog?.findViewById(R.id.forgot_alert_msg)
            msg?.text = "Please enter your email id to reset your password"
            var email_id: EditText? = dialog?.findViewById(R.id.forgot_enter_email_tv)
            email_id?.requestFocus()

            if (email_id?.requestFocus() == true) {
                dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
            }

            val yes = dialog?.findViewById<TextView>(R.id.forgot_alert_send)
            yes?.setOnClickListener(View.OnClickListener {
                if (email_id?.text.isNullOrEmpty()) {
                    alertDialog(this, "Alert", "Please enter your email id")

                } else {
                    var emailString = email_id?.text.toString().replace(" ", "")
                    ForgotPasswordAsyncTask(emailString).execute()

                }

            })
            val no = dialog?.findViewById(R.id.forgot_alert_cancel) as TextView?
            no?.setOnClickListener {
                dialog?.dismiss()
            }

            dialog?.show()
        })

    }

    inner class ForgotPasswordAsyncTask(emailID: String) : AsyncTask<String, String, Any>() {
        var mailID = emailID
        override fun onPreExecute() {
            super.onPreExecute()
            progressBarShow(this@LoginActivity)
        }

        @SuppressLint("WrongThread")
        override fun doInBackground(vararg params: String?): Boolean {
            val params = HashMap<String, String>()
            params.put("email", mailID)
            apiRequest.postRequest(
                this@LoginActivity,
                webService.reset_password_url,
                params
            ) { response ->
                var responseCode = response.getString("status")

                if (responseCode == "201") {
                    var message = response.getString("msg")
                    alertDialog(this@LoginActivity, "Success", message)
                    progressBarDismiss(this@LoginActivity)
                    dialog?.dismiss()
                } else if (responseCode == "403") {
                    var message = response.getString("msg")
                    alertDialog(this@LoginActivity, "Alert", message)
                    progressBarDismiss(this@LoginActivity)
                } else if (responseCode == "400") {
                    var message = response.getString("msg")
                    var msgObj = JSONObject(message)
                    var mailInfo = msgObj.getString("email")
                    alertDialog(this@LoginActivity, "Alert", mailInfo)
                    progressBarDismiss(this@LoginActivity)
                } else {
                    progressBarDismiss(this@LoginActivity)
                }

            }

            return true
        }

    }


    override fun onBackPressed() {
        finishAffinity()
    }


    /**
     * get lat lon
     */

    @SuppressLint("MissingPermission")
    fun getLastLocation() {
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.lastLocation.addOnCompleteListener(this) { task ->
                    var location: Location? = task.result
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        var lati = location.latitude.toString()
                        var long = location.longitude.toString()
                        setSharedPref(this, "current_lat", lati)
                        setSharedPref(this, "current_lon", long)

                    }
                }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }


    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ),
            PERMISSION_ID
        )
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_ID) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                getLastLocation()
            }
        }
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
                enableMyLocation()
            }
        }
        if (requestCode == REQUEST_CODE_LOCATION && grantResults.isNotEmpty()
            && grantResults[0] != PackageManager.PERMISSION_GRANTED
        ) {
            throw RuntimeException("Location services are required in order to " + "connect to a reader.")
        }

    }

    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        mFusedLocationClient.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            var mLastLocation: Location = locationResult.lastLocation
            var lati = mLastLocation.latitude.toString()
            var long = mLastLocation.longitude.toString()

            setSharedPref(this@LoginActivity, "current_lat", lati)
            setSharedPref(this@LoginActivity, "current_lon", long)


        }
    }

    private fun enableMyLocation() {
        if (isPermissionGranted()) {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
        } else {
            this.let {
                ActivityCompat.requestPermissions(
                    it,
                    arrayOf<String>(Manifest.permission.ACCESS_FINE_LOCATION),
                    REQUEST_LOCATION_PERMISSION
                )
            }
        }
    }

    private fun isPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }


}
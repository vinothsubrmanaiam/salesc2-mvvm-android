package com.digi.store.util.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.salesc2.database.DashBoardDateAdd


@Dao
interface DashBoardDateAddDao {

   @Query("SELECT * FROM DashBoardDateAdd")
    suspend fun getAll(): List<DashBoardDateAdd>



    @Insert
    suspend fun insertAll(vararg dashBoard: DashBoardDateAdd)


    @Query("DELETE FROM DashBoardDateAdd")
    fun deleteAll()

 @Query("SELECT DISTINCT * FROM DashBoardDateAdd")
 fun fetchDistinctData(): List<DashBoardDateAdd?>

 @Query("SELECT * FROM DashBoardDateAdd  WHERE date =:item_name")
 fun showDateList(item_name : String): List<DashBoardDateAdd>

}

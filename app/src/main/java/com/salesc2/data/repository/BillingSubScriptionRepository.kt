package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestAccountList
import com.salesc2.data.network.api.request.RequestBillingSubScription
import com.salesc2.data.network.api.response.ResponseAccountList
import com.salesc2.data.network.api.response.ResponseBillingSubscription
import com.salesc2.data.network.api.service.AccountListApi
import com.salesc2.data.network.api.service.BillingSubScriptionApi
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface BillingSubScriptionRepository {

    suspend fun reqSubscriptionDetails(
        requestBillingSubScription: RequestBillingSubScription,
        onSuccess: OnSuccess<ResponseBillingSubscription>,
        onFailure: OnFailure<ResponseBillingSubscription>

    )

    companion object Factory{
        fun create(api : BillingSubScriptionApi) : BillingSubScriptionRepository = BillingSubScriptionRepositoryImpl(api)
    }
}

class BillingSubScriptionRepositoryImpl(var api: BillingSubScriptionApi) : BillingSubScriptionRepository {
    override suspend fun reqSubscriptionDetails(
        requestBillingSubScription: RequestBillingSubScription,
        onSuccess: OnSuccess<ResponseBillingSubscription>,
        onFailure: OnFailure<ResponseBillingSubscription>
    ) {


        withContext(Dispatchers.IO) {
            try {
                val response = api.reqSubscriptionDetails(requestBillingSubScription = requestBillingSubScription)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status==200) {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }
    }

}

package com.salesc2.data.network.api.response

data class ResponseDepartmentList(
    val `data`: List<Data>,
    val status: Int
) {
    data class Data(
        val id: String,
        val name: String
    )
}
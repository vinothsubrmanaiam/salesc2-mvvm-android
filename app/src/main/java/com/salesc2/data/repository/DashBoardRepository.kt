package com.salesc2.data.repository

import com.salesc2.data.network.api.request.RequestDashBoard
import com.salesc2.data.network.api.response.ResponseDashBoard
import com.salesc2.data.network.api.service.DashBoardAPI
import com.salesc2.data.network.di.OnFailure
import com.salesc2.data.network.di.OnSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface DashBoardRepository {

    suspend fun reqDashboardData(
        requestDashBoard: RequestDashBoard,
    onSuccess: OnSuccess<ResponseDashBoard>,
    onFailure: OnFailure<ResponseDashBoard>

    )

    companion object Factory{
        fun create(api : DashBoardAPI) : DashBoardRepository = DashBoardRepositoryImpl(api)
    }
}

class DashBoardRepositoryImpl(var api: DashBoardAPI) : DashBoardRepository {
    override suspend fun reqDashboardData(
        requestDashBoard: RequestDashBoard,
        onSuccess: OnSuccess<ResponseDashBoard>,
        onFailure: OnFailure<ResponseDashBoard>
    ) {


        withContext(Dispatchers.IO) {
            try {
                val response = api.reqDashBoardData(requestDashBoard = requestDashBoard)
                if (response.isSuccessful) {
                    response.body().let {
                        if (it!!.status==200) {
                            withContext(Dispatchers.IO)
                            {
                                onSuccess(it)
                            }
                        } else {
                            withContext(Dispatchers.IO)
                            {
                                onFailure(it)
                            }
                        }
                    }
                }
            } catch (e: Exception) {
            }


        }
    }

}

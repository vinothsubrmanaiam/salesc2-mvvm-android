package com.salesc2.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.AccountDepartmentEmailModel
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class AccountDepartmentCustomerEmailListAdapter() : RecyclerView.Adapter<AccountDepartmentCustomerEmailListAdapter.viewHolder>()  {

    private lateinit var accountemailList: ArrayList<AccountDepartmentEmailModel>
    var context: Context? = null

    constructor(context: Context?, accountemailList: ArrayList<AccountDepartmentEmailModel>) : this() {
        this.context = context
        this.accountemailList = accountemailList
    }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AccountDepartmentCustomerEmailListAdapter.viewHolder {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.account_detail_dept_email, parent, false)
        return AccountDepartmentCustomerEmailListAdapter.viewHolder(view)
    }

    override fun getItemCount(): Int {
        return accountemailList.size
    }

    override fun onBindViewHolder(
        holder: AccountDepartmentCustomerEmailListAdapter.viewHolder,
        position: Int
    ) {
        val accountDepartmentEmailModel: AccountDepartmentEmailModel =
            accountemailList.get(position)
        holder.email_tv?.text = accountDepartmentEmailModel.emailid
        holder.email_type_tv?.text = "(" + accountDepartmentEmailModel.emailType + ")"

        holder.email_layout?.setOnClickListener(View.OnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto: ${accountDepartmentEmailModel.emailid}")
            intent.putExtra(Intent.EXTRA_EMAIL, "test")
            context?.startActivity(intent)

        })
        holder.email_tv?.setOnClickListener(View.OnClickListener {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.data = Uri.parse("mailto: ${accountDepartmentEmailModel.emailid}")
            intent.putExtra(Intent.EXTRA_EMAIL, "test")
            context?.startActivity(intent)

        })

    }



    class viewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var email_tv: TextView? = itemView.findViewById(R.id.email_tv)
        var email_type_tv: TextView? = itemView.findViewById(R.id.email_type_tv)
        var email_layout: ConstraintLayout? = itemView.findViewById(R.id.email_layout)


    }
    private fun capitalize(capString: String): String? {
        val capBuffer = StringBuffer()
        val capMatcher: Matcher =
            Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(capString)
        while (capMatcher.find()) {
            capMatcher.appendReplacement(
                capBuffer,
                capMatcher.group(1).toUpperCase() + capMatcher.group(2).toLowerCase()
            )
        }
        return capMatcher.appendTail(capBuffer).toString()
    }

}
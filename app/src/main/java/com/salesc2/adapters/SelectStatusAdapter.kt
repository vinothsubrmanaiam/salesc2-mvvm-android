package com.salesc2.adapters

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.fragment.FilterTaskListFragment
import com.salesc2.model.SelectAccountModel
import com.salesc2.model.SelectStatusModel
import com.salesc2.utils.setSharedPref
import java.util.ArrayList

class SelectStatusAdapter() : RecyclerView.Adapter<SelectStatusAdapter.MyViewHolder>() {
    private lateinit var ssItems: ArrayList<SelectStatusModel>
    var context: Context? = null

    constructor(context: Context?, ssItems: ArrayList<SelectStatusModel>) : this(){
        this.context = context
        this.ssItems = ssItems

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_select_list, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
      return ssItems.size
    }

    var row_index = -1
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var selectStatusModel : SelectStatusModel = ssItems[position]
        holder.filte_name.text = selectStatusModel.ss_name
        holder.filte_name.gravity = Gravity.CENTER
        holder.itemView.setOnClickListener(View.OnClickListener {
            context?.setSharedPref(context!!,"select_status_name",selectStatusModel.ss_name)

            row_index=position
            notifyDataSetChanged()
        })
        if(row_index==position){
            context?.setSharedPref(context!!,"select_status_name",selectStatusModel.ss_name)
            context?.let { ContextCompat.getColor(it,R.color.white) }?.let {
                holder.filte_name.setTextColor(it)
            }
            context?.let { ContextCompat.getDrawable(it,R.drawable.corner_shape_green_bg) }?.let {
                holder.filte_name.setBackgroundDrawable(it)
            }

        }
        else
        {
            context?.let { ContextCompat.getColor(it,R.color.filter_left_menu_bg) }?.let {
                holder.filte_name.setTextColor(it)
            }
            context?.let { ContextCompat.getColor(it,R.color.light_grey) }?.let {
                holder.filte_name.setBackgroundColor(it)
            }

        }
    }
    class MyViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView){
        var filte_name : TextView = itemView.findViewById(R.id.select_list_name)
        val list_layout : ConstraintLayout = itemView.findViewById(R.id.list_layout)
    }
}
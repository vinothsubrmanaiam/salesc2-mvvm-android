package com.salesc2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.AddTaskAssigneeModel
import com.salesc2.model.BillingHistoryModel
import java.text.SimpleDateFormat

class BillingHistoryAdapter():RecyclerView.Adapter<BillingHistoryAdapter.MyViewHolder>() {
        private var billingHistoryItems = ArrayList<BillingHistoryModel>()
    var context : Context? = null

    constructor( context: Context?, billingHistoryItems: java.util.ArrayList<BillingHistoryModel>) : this(){
        this.context = context
        this.billingHistoryItems = billingHistoryItems

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_billing_history, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
       return billingHistoryItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var billingHistoryModel : BillingHistoryModel = billingHistoryItems[position]
        try {
            var sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            var date = sdf.parse(billingHistoryModel.startDate)
            var myF = SimpleDateFormat("MMM dd, yyyy")
            var dateF = myF.format(date)
            holder.BH_date.text = dateF
            if(billingHistoryModel.amount.contains(".")){
                holder.BH_price.text = "$ ${billingHistoryModel.amount}"
            }else{
                holder.BH_price.text = "$ ${billingHistoryModel.amount}.00"
            }

        }catch (e:Exception){

        }

    }
    class MyViewHolder constructor(itemView:View):RecyclerView.ViewHolder(itemView) {
     var BH_date : TextView = itemView.findViewById(R.id.BH_date)
        var BH_price : TextView = itemView.findViewById(R.id.BH_price)
    }
}
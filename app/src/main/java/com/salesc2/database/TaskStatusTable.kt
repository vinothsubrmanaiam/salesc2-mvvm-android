package com.salesc2.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TaskStatusTable(@PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo(name = "dash_task_id") val dash_task_id: String?,
    @ColumnInfo(name = "t_status") val t_status: String?)
package com.salesc2.data.network.api.service



import com.salesc2.data.network.api.request.RequestUserLogin
import com.salesc2.data.network.api.response.ResponseEmployeeLogin
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface UserLoginApi {

    @POST("ios-login/api/v1/employee")
    suspend fun reqUserLogin(
        @Body requestUserLogin: RequestUserLogin
    ): Response<ResponseEmployeeLogin>
}
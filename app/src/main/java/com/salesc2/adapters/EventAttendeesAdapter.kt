package com.salesc2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.salesc2.R
import com.salesc2.model.AddTaskAssigneeModel
import com.salesc2.model.EventAttendeesModel
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import java.lang.Exception


class EventAttendeesAdapter():RecyclerView.Adapter<EventAttendeesAdapter.MyViewHolder>() {
  private var attendeesItems = ArrayList<EventAttendeesModel>()
    var context :Context? = null

    constructor( context: Context?, attendeesItems: ArrayList<EventAttendeesModel>) : this(){
        this.context = context
        this.attendeesItems = attendeesItems

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.adapter_event_attendees, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
       return attendeesItems.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       var eventAttendeesModel :EventAttendeesModel =  attendeesItems[position]
        if (eventAttendeesModel.attendee_name != "null") {
            holder.attendee_name.text = eventAttendeesModel.attendee_name
            try {
                Picasso.get().load(eventAttendeesModel.attendee_img)
                    .placeholder(R.drawable.ic_dummy_user_pic).into(holder.attendee_image)
            } catch (e: Exception) {
                print(e)
            }
        }
    }
    class MyViewHolder constructor(itemView:View):RecyclerView.ViewHolder(itemView) {
  var attendee_image : CircleImageView = itemView.findViewById(R.id.attendee_image)
        var attendee_name : TextView = itemView.findViewById(R.id.attendee_name)
    }
}
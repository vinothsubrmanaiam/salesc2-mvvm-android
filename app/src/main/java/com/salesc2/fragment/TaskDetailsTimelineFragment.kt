package com.salesc2.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.tabs.TabLayout
import com.salesc2.R
import com.salesc2.adapters.TaskDetailsTimelineAdapter
import com.salesc2.adapters.TeamMembersListAdapter
import com.salesc2.database.AppDatabase
import com.salesc2.model.TaskDetailsTimelineModel
import com.salesc2.model.TeamMemberListModel
import com.salesc2.service.ApiRequest
import com.salesc2.service.WebService
import com.salesc2.utils.*
import com.shrikanthravi.collapsiblecalendarview.widget.CollapsibleCalendar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray
import java.lang.Exception
import java.text.SimpleDateFormat

class TaskDetailsTimelineFragment : Fragment() {

    lateinit var taskDetailsTimeline_recyclerView : RecyclerView
    var taskDetailsTimelineModel = ArrayList<TaskDetailsTimelineModel>()

    var apiRequest = ApiRequest()
    var webService = WebService()
    lateinit var mView :View
    private var task_ID = String()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance=true
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_task_details_timeline,container,false)
        task_ID = context?.getSharedPref("task_id",context!!).toString()
        taskDetailsTimeline_recyclerView = mView.findViewById(R.id.taskDetailsTimeline_recyclerView)
        taskDetailsTimelineModel = ArrayList()

        if (context?.isNetworkAvailable(context!!) == true){
            taskDetailsTimelineApiCall()
        }else{
            showTimelineOfflineData()
        }

        activity?.findViewById<Toolbar>(R.id.toolbar)?.visibility = View.GONE
        activity?.findViewById<View>(R.id.divider1)?.visibility = View.GONE
        activity?.findViewById<BottomNavigationView>(R.id.navigationView)?.visibility = View.VISIBLE
        activity?.findViewById<TabLayout>(R.id.dashbord_tabLayout)?.visibility = View.GONE
        activity?.findViewById<CollapsibleCalendar>(R.id.dashboard_calendarView)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.cal_bottom_layout)?.visibility = View.GONE
        activity?.findViewById<FrameLayout>(R.id.main_fragmet_container)?.visibility = View.VISIBLE
        activity?.findViewById<ConstraintLayout>(R.id.filter_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.dashboard_tabs_Layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.user_wish_layout)?.visibility = View.GONE
        activity?.findViewById<RecyclerView>(R.id.dashboard_recyclerView)?.visibility = View.GONE

        activity?.findViewById<ConstraintLayout>(R.id.notificationListLayout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.notificationEmailSetting_layout)?.visibility = View.GONE
        activity?.findViewById<ConstraintLayout>(R.id.maps_layout)?.visibility = View.GONE
        return mView
    }

    fun taskDetailsTimelineApiCall(){
        val params = HashMap<String,String>()
        params["id"] = task_ID
        print("taskID_timeline== $task_ID")
        context?.let {
            apiRequest.postRequestBodyWithHeaders(it,webService.Task_details_timeline_url,params){ response->
                try {
                    var status = response.getString("status")
                    var msg = response.getString("msg")
                    if (status == "200"){
                        taskDetailsTimelineModel.clear()
                        taskDetailsTimelineModel = ArrayList()
                        var dataArray = response.getJSONArray("data")
                        for (i in 0 until dataArray.length()){
                            var dataObj = dataArray.getJSONObject(i)
                            var _id = dataObj.getString("_id")
                            var type = dataObj.getString("type")
                            var description= String()
                            if (dataObj.has("description")) {
                                description = dataObj.getString("description")
                            }
                            else{
                               description = ""
                            }
                            var createdAt = dataObj.getString("createdAt")
                            var createdBy = dataObj.getString("createdBy")
                            var createdById = dataObj.getString("createdById")

                            var assignedTo = String()
                            var assignedToId = String()
                            if (dataObj.has("assignedTo")){
                                 assignedTo = dataObj.getString("assignedTo")
                            }
                            if (dataObj.has("assignedToId")){
                                 assignedToId = dataObj.getString("assignedToId")
                            }

                            taskDetailsTimelineModel.add(TaskDetailsTimelineModel(_id,type,description, createdAt, createdBy, createdById, assignedTo, assignedToId))
                        }
                        setupTimelineRecyclerView(taskDetailsTimelineModel)
                    }

                }catch (e:Exception){
                    println(e)
                    context?.toast(e.toString())
                }

            }
        }
    }

    //    setup adapter
    private fun setupTimelineRecyclerView(taskDetailsTimelineModel: ArrayList<TaskDetailsTimelineModel>) {
        val adapter: RecyclerView.Adapter<*> =
            TaskDetailsTimelineAdapter(
                context,
                taskDetailsTimelineModel
            )
        val llm = LinearLayoutManager(context)
        llm.orientation = LinearLayoutManager.VERTICAL

        taskDetailsTimeline_recyclerView.layoutManager = llm
        taskDetailsTimeline_recyclerView.adapter = adapter

    }
    override fun onDestroy() {
        super.onDestroy()
        context?.progressBarDismiss(context!!)
    }

    override fun onDetach() {
        super.onDetach()
        context?.progressBarDismiss(context!!)
    }

    var db: AppDatabase? = null
    //offline
    private fun showTimelineOfflineData(){
        CoroutineScope(Dispatchers.Main).launch {
            db = AppDatabase.getDatabaseClient(requireContext())
            val taskData = db!!.taskDao().getTaskDetails(task_ID)
            Log.i("taskId=====",task_ID)
            Log.i("taskData",taskData.toString())
            var timelineArr = taskData[0].timeline
            Log.i("timelineOfflineData",timelineArr.toString())
//            var assignToId = taskData[0].assignTo_id
//            var assignToName = taskData[0].assignTo_name
            //string to json aray
            var dataArray = JSONArray(taskData[0].timeline)
            for (i in 0 until dataArray.length()){
                var dataObj = dataArray.getJSONObject(i)
                var _id = dataObj.getString("_id")
                var type = dataObj.getString("type")
                var description= String()
                if (dataObj.has("description")) {
                    description = dataObj.getString("description")
                }
                else{
                    description = ""
                }
                var createdAt = dataObj.getString("createdAt")
                var createdBy = dataObj.getString("createdBy")
                var createdById = dataObj.getString("createdById")

                var assignedTo = String()
                var assignedToId = String()
                if (dataObj.has("assignedTo")){
                    assignedTo = dataObj.getString("assignedTo")
                }
                if (dataObj.has("assignedToId")){
                    assignedToId = dataObj.getString("assignedToId")
                }
                taskDetailsTimelineModel.add(TaskDetailsTimelineModel(_id,type,description, createdAt, createdBy, createdById, assignedTo, assignedToId))
            }
            setupTimelineRecyclerView(taskDetailsTimelineModel)

        }
    }

}